.class Lcom/sec/android/app/hwmoduletest/SensorTest$5;
.super Ljava/lang/Object;
.source "SensorTest.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/SensorTest;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V
    .locals 0

    .prologue
    .line 1274
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 1277
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 1283
    :goto_0
    return-void

    .line 1281
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 1286
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 1289
    return-void
.end method
