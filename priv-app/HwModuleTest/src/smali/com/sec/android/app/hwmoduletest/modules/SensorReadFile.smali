.class public Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;
.super Ljava/lang/Object;
.source "SensorReadFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;,
        Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    }
.end annotation


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private final INFO_ARRAY_INDEX_ACCELEROMETER:I

.field private final INFO_ARRAY_INDEX_ACCELEROMETER_CAL:I

.field private final INFO_ARRAY_INDEX_ACCELEROMETER_INTPIN:I

.field private final INFO_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

.field private final INFO_ARRAY_INDEX_ACCELEROMETER_SELF:I

.field private final INFO_ARRAY_INDEX_BAROMETER_EEPROM:I

.field private final INFO_ARRAY_INDEX_GYRO_POWER:I

.field private final INFO_ARRAY_INDEX_GYRO_SELFTEST:I

.field private final INFO_ARRAY_INDEX_GYRO_TEMPERATURE:I

.field private final INFO_ARRAY_INDEX_LIGHT:I

.field private final INFO_ARRAY_INDEX_LIGHT_ADC:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_ADC:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_DAC:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_POWER_ON:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_SELF:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_STATUS:I

.field private final INFO_ARRAY_INDEX_MAGNETIC_TEMP:I

.field private final INFO_ARRAY_INDEX_PROXIMITY_ADC:I

.field private final INFO_ARRAY_INDEX_PROXIMITY_AVG:I

.field private final INFO_ARRAY_INDEX_PROXIMITY_OFFSET:I

.field private final INFO_ARRAY_LENGTH:I

.field private LOOP_DELAY:I

.field private dummy:I

.field private mFeature_Magnetic:Ljava/lang/String;

.field private mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

.field private mInterrupted:Z

.field private mIsLoop:Z

.field private mModuleSensorIDArray:[I

.field private mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

.field private mTemp_String:[Ljava/lang/String;


# direct methods
.method public constructor <init>([I)V
    .locals 4
    .param p1, "moduleSensorID"    # [I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, "SensorReadFile"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->CLASS_NAME:Ljava/lang/String;

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->DEBUG:Z

    .line 26
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    .line 27
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER:I

    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

    .line 29
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_SELF:I

    .line 30
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_CAL:I

    .line 31
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_INTPIN:I

    .line 32
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_BAROMETER_EEPROM:I

    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_GYRO_POWER:I

    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_GYRO_TEMPERATURE:I

    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_GYRO_SELFTEST:I

    .line 36
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_LIGHT:I

    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_LIGHT_ADC:I

    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_POWER_ON:I

    .line 39
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

    .line 40
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_STATUS:I

    .line 41
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_TEMP:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_DAC:I

    .line 43
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_ADC:I

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_SELF:I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_PROXIMITY_ADC:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_PROXIMITY_AVG:I

    .line 47
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_PROXIMITY_OFFSET:I

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dummy:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_LENGTH:I

    .line 49
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_LENGTH:I

    new-array v0, v0, [Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    .line 55
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInterrupted:Z

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mIsLoop:Z

    .line 58
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I

    .line 60
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    .line 62
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 67
    const-string v0, "SensorReadFile"

    const-string v1, "SensorReadFile"

    const-string v2, "Sensor On"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->init()V

    .line 70
    return-void
.end method

.method private ConverterID(I)I
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 486
    const/4 v0, -0x1

    .line 488
    .local v0, "infoArrayIndex":I
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    if-ne p1, v1, :cond_1

    .line 489
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER:I

    .line 532
    :cond_0
    :goto_0
    return v0

    .line 490
    :cond_1
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    if-ne p1, v1, :cond_2

    .line 491
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

    goto :goto_0

    .line 492
    :cond_2
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    if-ne p1, v1, :cond_3

    .line 493
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_SELF:I

    goto :goto_0

    .line 494
    :cond_3
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    if-ne p1, v1, :cond_4

    .line 495
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_CAL:I

    goto :goto_0

    .line 496
    :cond_4
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    if-ne p1, v1, :cond_5

    .line 497
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_ACCELEROMETER_INTPIN:I

    goto :goto_0

    .line 498
    :cond_5
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    if-ne p1, v1, :cond_6

    .line 499
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_BAROMETER_EEPROM:I

    goto :goto_0

    .line 500
    :cond_6
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    if-ne p1, v1, :cond_7

    .line 501
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_GYRO_POWER:I

    goto :goto_0

    .line 502
    :cond_7
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    if-ne p1, v1, :cond_8

    .line 503
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_GYRO_TEMPERATURE:I

    goto :goto_0

    .line 504
    :cond_8
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    if-ne p1, v1, :cond_9

    .line 505
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_GYRO_SELFTEST:I

    goto :goto_0

    .line 506
    :cond_9
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    if-ne p1, v1, :cond_a

    .line 507
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_LIGHT:I

    goto :goto_0

    .line 508
    :cond_a
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    if-ne p1, v1, :cond_b

    .line 509
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_LIGHT_ADC:I

    goto :goto_0

    .line 510
    :cond_b
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    if-ne p1, v1, :cond_c

    .line 511
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_POWER_ON:I

    goto :goto_0

    .line 512
    :cond_c
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    if-ne p1, v1, :cond_d

    .line 513
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

    goto :goto_0

    .line 514
    :cond_d
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    if-ne p1, v1, :cond_e

    .line 515
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_STATUS:I

    goto :goto_0

    .line 516
    :cond_e
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    if-ne p1, v1, :cond_f

    .line 517
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_TEMP:I

    goto :goto_0

    .line 518
    :cond_f
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p1, v1, :cond_10

    .line 519
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_DAC:I

    goto :goto_0

    .line 520
    :cond_10
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p1, v1, :cond_11

    .line 521
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_ADC:I

    goto :goto_0

    .line 522
    :cond_11
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v1, :cond_12

    .line 523
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_MAGNETIC_SELF:I

    goto :goto_0

    .line 524
    :cond_12
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    if-ne p1, v1, :cond_13

    .line 525
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_PROXIMITY_ADC:I

    goto :goto_0

    .line 526
    :cond_13
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    if-ne p1, v1, :cond_14

    .line 527
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_PROXIMITY_AVG:I

    goto/16 :goto_0

    .line 528
    :cond_14
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    if-ne p1, v1, :cond_0

    .line 529
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_INDEX_PROXIMITY_OFFSET:I

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInterrupted:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .prologue
    .line 7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;
    .param p1, "x1"    # I

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .prologue
    .line 7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .prologue
    .line 7
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I

    return v0
.end method

.method private checkSpec(II)V
    .locals 11
    .param p1, "moduleSensorID"    # I
    .param p2, "infoArrayIndex"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 369
    const-string v0, ""

    .line 370
    .local v0, "specResult":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    .line 371
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    if-nez v4, :cond_1

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p1, v4, :cond_6

    .line 376
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    array-length v4, v4

    if-ne v4, v6, :cond_0

    .line 377
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v0, v4, v7

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v8

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 379
    .local v1, "x":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v9

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 380
    .local v2, "y":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v10

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 382
    .local v3, "z":I
    const-string v4, "1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 383
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v7

    .line 384
    :cond_2
    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 385
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v7

    .line 387
    :cond_3
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->checkSpecMagneticDAC(III)Ljava/lang/String;

    move-result-object v0

    .line 390
    const-string v4, "OK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "NG"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 392
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aput-object v0, v4, v7

    .line 393
    const-string v4, "SensorReadFile"

    const-string v5, "checkSpec"

    const-string v6, "<Write> specResult"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 395
    :cond_5
    const-string v4, "SensorReadFile"

    const-string v5, "checkSpec"

    const-string v6, "<Write> default"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 401
    .end local v1    # "x":I
    .end local v2    # "y":I
    .end local v3    # "z":I
    :cond_6
    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p1, v4, :cond_d

    .line 402
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BOSCH_BMC150"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BMC150_COMBINATION"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 406
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 407
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    new-array v5, v6, [Ljava/lang/String;

    iput-object v5, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    .line 408
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 409
    .restart local v1    # "x":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 410
    .restart local v2    # "y":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 411
    .restart local v3    # "z":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->checkSpecMagneticADC(III)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 412
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 413
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 414
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    goto/16 :goto_0

    .line 416
    .end local v1    # "x":I
    .end local v2    # "y":I
    .end local v3    # "z":I
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    array-length v4, v4

    if-ne v4, v6, :cond_0

    .line 418
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v0, v4, v7

    .line 419
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v8

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 420
    .restart local v1    # "x":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v9

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 421
    .restart local v2    # "y":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v10

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 423
    .restart local v3    # "z":I
    const-string v4, "1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v7

    .line 425
    :cond_9
    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 426
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v7

    .line 428
    :cond_a
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->checkSpecMagneticADC(III)Ljava/lang/String;

    move-result-object v0

    .line 431
    const-string v4, "OK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    const-string v4, "NG"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 433
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aput-object v0, v4, v7

    .line 434
    const-string v4, "SensorReadFile"

    const-string v5, "checkSpec"

    const-string v6, "<Write> specResult"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 436
    :cond_c
    const-string v4, "SensorReadFile"

    const-string v5, "checkSpec"

    const-string v6, "<Write> default"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 443
    .end local v1    # "x":I
    .end local v2    # "y":I
    .end local v3    # "z":I
    :cond_d
    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v4, :cond_0

    .line 444
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BOSCH_BMC150"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BMC150_COMBINATION"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 448
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 449
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    new-array v5, v9, [Ljava/lang/String;

    iput-object v5, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    .line 450
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 451
    .restart local v3    # "z":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    invoke-static {v7, v7, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->checkSpecMagneticSelf(III)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 452
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    goto/16 :goto_0

    .line 453
    .end local v3    # "z":I
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BMC150_NEWEST"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 456
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    array-length v4, v4

    if-ne v4, v6, :cond_0

    .line 458
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v0, v4, v7

    .line 459
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v8

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 460
    .restart local v1    # "x":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v9

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 461
    .restart local v2    # "y":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aget-object v4, v4, v10

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 463
    .restart local v3    # "z":I
    const-string v4, "1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 464
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v7

    .line 465
    :cond_10
    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 466
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v7

    .line 468
    :cond_11
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->checkSpecMagneticSelf(III)Ljava/lang/String;

    move-result-object v0

    .line 471
    const-string v4, "OK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    const-string v4, "NG"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 473
    :cond_12
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    aput-object v0, v4, v7

    .line 474
    const-string v4, "SensorReadFile"

    const-string v5, "checkSpec"

    const-string v6, "<Write> specResult"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 476
    :cond_13
    const-string v4, "SensorReadFile"

    const-string v5, "checkSpec"

    const-string v6, "<Write> default"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private dataCheck([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 538
    const-string v1, ""

    .line 540
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 541
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 542
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 544
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 545
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 541
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 549
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 552
    :cond_2
    return-object v1
.end method

.method private init()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 99
    const-string v2, "SensorReadFile"

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "INFO_ARRAY_LENGTH : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_LENGTH:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v1, -0x1

    .line 102
    .local v1, "infoArrayIndex":I
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    if-eqz v2, :cond_15

    .line 103
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    array-length v2, v2

    if-ge v0, v2, :cond_16

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    if-ne v2, v3, :cond_1

    .line 106
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 108
    if-ge v6, v1, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____ACCELEROMETER"

    const-string v5, "ACCEL_SENSOR_RAW"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    .line 237
    :cond_0
    :goto_1
    if-ge v6, v1, :cond_14

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->INFO_ARRAY_LENGTH:I

    if-ge v1, v2, :cond_14

    .line 238
    const-string v2, "SensorReadFile"

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mInfo - mName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v2, "SensorReadFile"

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mInfo - mFileID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mFileID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-string v2, "SensorReadFile"

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mInfo - mIsExistFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v5, v5, v1

    iget-boolean v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mIsExistFile:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 111
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    if-ne v2, v3, :cond_2

    .line 112
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 114
    if-ge v6, v1, :cond_0

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____ACCELEROMETER_N_ANGLE"

    const-string v5, "ACCEL_SENSOR_RAW"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 117
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    if-ne v2, v3, :cond_3

    .line 118
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 120
    if-ge v6, v1, :cond_0

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____ACCELEROMETER_SELF"

    const-string v5, "ACCEL_SENSOR_RAW"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 123
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    if-ne v2, v3, :cond_4

    .line 124
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 126
    if-ge v6, v1, :cond_0

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____ACCELEROMETER_CAL"

    const-string v5, "ACCEL_SENSOR_CAL"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 129
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    if-ne v2, v3, :cond_5

    .line 130
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 132
    if-ge v6, v1, :cond_0

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____ACCELEROMETER_INTPIN"

    const-string v5, "ACCEL_SENSOR_INTPIN"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 137
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    if-ne v2, v3, :cond_6

    .line 138
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 140
    if-ge v6, v1, :cond_0

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____BAROMETER_EEPROM"

    const-string v5, "BAROME_EEPROM"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 145
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    if-ne v2, v3, :cond_7

    .line 146
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 148
    if-ge v6, v1, :cond_0

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____GYRO_POWER"

    const-string v5, "GYRO_SENSOR_POWER_ON"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 151
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    if-ne v2, v3, :cond_8

    .line 152
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 154
    if-ge v6, v1, :cond_0

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____GYRO_TEMPERATURE"

    const-string v5, "GYRO_SENSOR_TEMP"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 157
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    if-ne v2, v3, :cond_9

    .line 158
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 160
    if-ge v6, v1, :cond_0

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____GYRO_SELF"

    const-string v5, "GYRO_SENSOR_SELFTEST"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 165
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    if-ne v2, v3, :cond_a

    .line 166
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 168
    if-ge v6, v1, :cond_0

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____LIGHT"

    const-string v5, "LIGHT_SENSOR_LUX"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 171
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    if-ne v2, v3, :cond_b

    .line 172
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 174
    if-ge v6, v1, :cond_0

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____LIGHT_ADC"

    const-string v5, "LIGHT_SENSOR_ADC"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 179
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    if-ne v2, v3, :cond_c

    .line 180
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 182
    if-ge v6, v1, :cond_0

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____MAGNETIC_POWER_ON"

    const-string v5, "GEOMAGNETIC_SENSOR_POWER"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 185
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    if-ne v2, v3, :cond_d

    .line 186
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 188
    if-ge v6, v1, :cond_0

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____MAGNETIC_STATUS"

    const-string v5, "GEOMAGNETIC_SENSOR_STATUS"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 191
    :cond_d
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    if-ne v2, v3, :cond_e

    .line 192
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 194
    if-ge v6, v1, :cond_0

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____MAGNETIC_TEMPERATURE"

    const-string v5, "GEOMAGNETIC_SENSOR_TEMP"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 197
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne v2, v3, :cond_f

    .line 198
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 200
    if-ge v6, v1, :cond_0

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____MAGNETIC_DAC"

    const-string v5, "GEOMAGNETIC_SENSOR_DAC"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 203
    :cond_f
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne v2, v3, :cond_10

    .line 204
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 206
    if-ge v6, v1, :cond_0

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____MAGNETIC_ADC"

    const-string v5, "GEOMAGNETIC_SENSOR_ADC"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 209
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne v2, v3, :cond_11

    .line 210
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 212
    if-ge v6, v1, :cond_0

    .line 213
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____MAGNETIC_SELF"

    const-string v5, "GEOMAGNETIC_SENSOR_SELFTEST"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 217
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    if-ne v2, v3, :cond_12

    .line 218
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 220
    if-ge v6, v1, :cond_0

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____PROXIMITY_ADC"

    const-string v5, "PROXI_SENSOR_ADC"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 223
    :cond_12
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    if-ne v2, v3, :cond_13

    .line 224
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 226
    if-ge v6, v1, :cond_0

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____PROXIMITY_AVG"

    const-string v5, "PROXI_SENSOR_ADC_AVG"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 229
    :cond_13
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    if-ne v2, v3, :cond_0

    .line 230
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v1

    .line 232
    if-ge v6, v1, :cond_0

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    const-string v4, "ID_FILE____PROXIMITY_OFFSET"

    const-string v5, "PROXI_SENSOR_OFFSET"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    goto/16 :goto_1

    .line 245
    :cond_14
    const-string v2, "SensorReadFile"

    const-string v3, "init"

    const-string v4, "ID : Unknown"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 249
    .end local v0    # "i":I
    :cond_15
    const-string v2, "SensorReadFile"

    const-string v3, "init"

    const-string v4, "mModuleSensorIDArray null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_16
    return-void
.end method


# virtual methods
.method public isSensorOn(I)Z
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v0

    .line 256
    .local v0, "infoArrayIndex":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mIsExistFile:Z

    .line 259
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public returnData(I)[Ljava/lang/String;
    .locals 6
    .param p1, "moduleSensorID"    # I

    .prologue
    const/4 v3, -0x1

    .line 325
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I

    move-result v0

    .line 327
    .local v0, "infoArrayIndex":I
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mIsLoop:Z

    if-eqz v2, :cond_1

    .line 362
    :cond_0
    :goto_0
    const-string v2, "SensorReadFile"

    const-string v3, "returnData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " => "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    return-object v2

    .line 330
    :cond_1
    const-string v1, ""

    .line 332
    .local v1, "mTemp":Ljava/lang/String;
    if-ge v3, v0, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-boolean v2, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mIsExistFile:Z

    if-eqz v2, :cond_3

    .line 334
    const-string v2, "SensorReadFile"

    const-string v3, "returnData"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mFileID:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 339
    if-eqz v1, :cond_0

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    .line 342
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-eq p1, v2, :cond_2

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-eq p1, v2, :cond_2

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v2, :cond_0

    .line 345
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->checkSpec(II)V

    goto :goto_0

    .line 348
    :cond_3
    if-ge v3, v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-boolean v2, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mIsExistFile:Z

    if-nez v2, :cond_0

    .line 350
    const-string v2, "SensorReadFile"

    const-string v3, "read no file"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const-string v1, "0"

    .line 355
    if-eqz v1, :cond_0

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public sensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 73
    const-string v0, "SensorReadFile"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInterrupted:Z

    .line 84
    const-string v0, "SensorReadFile"

    const-string v1, "sensorOff"

    const-string v2, "Loop Stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "SensorReadFile"

    const-string v1, "sensorOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mInterrupted : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInterrupted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    .line 93
    :cond_0
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    .line 94
    return-void
.end method

.method public startLoop(I)V
    .locals 2
    .param p1, "loopDelay_millisecond"    # I

    .prologue
    const/4 v1, 0x1

    .line 317
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mIsLoop:Z

    .line 318
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;-><init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    .line 319
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->setDaemon(Z)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mSensorReadThread:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->start()V

    .line 322
    return-void
.end method
