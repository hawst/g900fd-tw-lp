.class public Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
.super Ljava/lang/Object;
.source "SensorReadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$1;,
        Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;,
        Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;,
        Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;
    }
.end annotation


# static fields
.field public static final MAGNETIC_DATA_NUMBER_ADC:I = 0x5

.field public static final MAGNETIC_DATA_NUMBER_ALL:I = 0x8

.field public static final MAGNETIC_DATA_NUMBER_DAC:I = 0x4

.field public static final MAGNETIC_DATA_NUMBER_OFFSET_H:I = 0x7

.field public static final MAGNETIC_DATA_NUMBER_POWER_OFF:I = 0x2

.field public static final MAGNETIC_DATA_NUMBER_POWER_ON:I = 0x1

.field public static final MAGNETIC_DATA_NUMBER_SELF:I = 0x6

.field public static final MAGNETIC_DATA_NUMBER_STATUS:I = 0x3

.field public static final MAGNETIC_DATA_NUMBER_TEMP:I = 0x2


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private DUMMY:I

.field private final RETURN_DATA_ARRAY_SIZE_MAX:I

.field private final SENSOR_ON_ARRAY_INDEX_ACCELEROMETER:I

.field private final SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

.field private final SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_SELF:I

.field private final SENSOR_ON_ARRAY_INDEX_BAROMETER:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_EXPANSION:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_POWER:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_SELF:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_TEMPERATURE:I

.field private final SENSOR_ON_ARRAY_INDEX_LIGHT:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_ADC:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_DAC:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_OFFSETH:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_ON:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_SELF:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_STATUS:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_TEMPERATURE:I

.field private final SENSOR_ON_ARRAY_INDEX_PROXIMITY:I

.field private final SENSOR_ON_ARRAY_LENGTH:I

.field private mAccelerometerSensor:Landroid/hardware/Sensor;

.field private mBarometerSensor:Landroid/hardware/Sensor;

.field private mGyroscopeSensor:Landroid/hardware/Sensor;

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mMagneticSensor:Landroid/hardware/Sensor;

.field private mOriginalData_Accelerometer:[F

.field private mOriginalData_Barometer:[F

.field private mOriginalData_Gyro:[F

.field private mOriginalData_Light:[F

.field private mOriginalData_Magnetic:[F

.field private mOriginalData_Proximity:[F

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mReturnData:[Ljava/lang/String;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorOn:[Z


# direct methods
.method public constructor <init>([ILandroid/hardware/SensorManager;)V
    .locals 9
    .param p1, "sensorID"    # [I
    .param p2, "sm"    # Landroid/hardware/SensorManager;

    .prologue
    const/16 v4, 0x10

    const/4 v5, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v2, "SensorReadManager"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->CLASS_NAME:Ljava/lang/String;

    .line 13
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DEBUG:Z

    .line 15
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    .line 16
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER:I

    .line 17
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

    .line 18
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_SELF:I

    .line 19
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BAROMETER:I

    .line 20
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO:I

    .line 21
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_EXPANSION:I

    .line 22
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_POWER:I

    .line 23
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_SELF:I

    .line 24
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_TEMPERATURE:I

    .line 25
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_LIGHT:I

    .line 26
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC:I

    .line 27
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_ON:I

    .line 28
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_STATUS:I

    .line 29
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_TEMPERATURE:I

    .line 30
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_DAC:I

    .line 31
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_ADC:I

    .line 32
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_SELF:I

    .line 33
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_OFFSETH:I

    .line 34
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

    .line 35
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_PROXIMITY:I

    .line 36
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    .line 37
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    .line 49
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 50
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    .line 51
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 52
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 53
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 54
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 55
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 56
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 58
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Accelerometer:[F

    .line 59
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Barometer:[F

    .line 60
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Gyro:[F

    .line 61
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Light:[F

    .line 62
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Magnetic:[F

    .line 63
    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Proximity:[F

    .line 65
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->RETURN_DATA_ARRAY_SIZE_MAX:I

    .line 66
    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mReturnData:[Ljava/lang/String;

    .line 71
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "Sensor On"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v1, -0x1

    .line 74
    .local v1, "sensorOnArrayIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v2, :cond_0

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v5, v2, v0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v2, :cond_1e

    .line 81
    const/4 v0, 0x0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_1f

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    if-nez v2, :cond_1

    .line 83
    new-instance v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    invoke-direct {v2, p0, v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;-><init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$1;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    .line 86
    :cond_1
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-eq v2, v3, :cond_2

    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-eq v2, v3, :cond_2

    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne v2, v3, :cond_7

    .line 89
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_3

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v2, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 94
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "register-AccelerometerSensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_4

    .line 99
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne v2, v3, :cond_5

    .line 100
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    .line 81
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 102
    :cond_5
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne v2, v3, :cond_6

    .line 103
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto :goto_2

    .line 105
    :cond_6
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne v2, v3, :cond_4

    .line 106
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto :goto_2

    .line 110
    :cond_7
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne v2, v3, :cond_9

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_8

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 115
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "register-BarometerSensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_4

    .line 119
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto :goto_2

    .line 122
    :cond_9
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne v2, v3, :cond_b

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_a

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 127
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "register-GyroscopeSensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_4

    .line 131
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 134
    :cond_b
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne v2, v3, :cond_d

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_c

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 139
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "register-LightSensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_4

    .line 143
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 146
    :cond_d
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne v2, v3, :cond_f

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_e

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v2, v8}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 152
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "register-MagneticSensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_4

    .line 156
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 159
    :cond_f
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne v2, v3, :cond_11

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_10

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 164
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "register-ProximitySensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_4

    .line 168
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 173
    :cond_11
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne v2, v3, :cond_12

    .line 174
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 176
    :cond_12
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    if-ne v2, v3, :cond_13

    .line 177
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 179
    :cond_13
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne v2, v3, :cond_14

    .line 180
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 182
    :cond_14
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne v2, v3, :cond_15

    .line 183
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 187
    :cond_15
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne v2, v3, :cond_16

    .line 188
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 190
    :cond_16
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne v2, v3, :cond_17

    .line 191
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 193
    :cond_17
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne v2, v3, :cond_18

    .line 194
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 196
    :cond_18
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne v2, v3, :cond_19

    .line 197
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 199
    :cond_19
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne v2, v3, :cond_1a

    .line 200
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 202
    :cond_1a
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne v2, v3, :cond_1b

    .line 203
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 205
    :cond_1b
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne v2, v3, :cond_1c

    .line 206
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 208
    :cond_1c
    aget v2, p1, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne v2, v3, :cond_1d

    .line 209
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v1

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v6, v2, v1

    goto/16 :goto_2

    .line 212
    :cond_1d
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unregistered-ETC(ModuleSensor ID) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 217
    :cond_1e
    const-string v2, "SensorReadManager"

    const-string v3, "SensorReadManager"

    const-string v4, "SensorManager null !!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_1f
    return-void
.end method

.method private ConverterID(I)I
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 1127
    const/4 v0, -0x1

    .line 1129
    .local v0, "sensorOnArrayIndex":I
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p1, v1, :cond_1

    .line 1130
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER:I

    .line 1171
    :cond_0
    :goto_0
    return v0

    .line 1131
    :cond_1
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne p1, v1, :cond_2

    .line 1132
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

    goto :goto_0

    .line 1133
    :cond_2
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne p1, v1, :cond_3

    .line 1134
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_SELF:I

    goto :goto_0

    .line 1135
    :cond_3
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p1, v1, :cond_4

    .line 1136
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BAROMETER:I

    goto :goto_0

    .line 1137
    :cond_4
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne p1, v1, :cond_5

    .line 1138
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO:I

    goto :goto_0

    .line 1139
    :cond_5
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    if-ne p1, v1, :cond_6

    .line 1140
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_POWER:I

    goto :goto_0

    .line 1141
    :cond_6
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne p1, v1, :cond_7

    .line 1142
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_EXPANSION:I

    goto :goto_0

    .line 1143
    :cond_7
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne p1, v1, :cond_8

    .line 1144
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_SELF:I

    goto :goto_0

    .line 1145
    :cond_8
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne p1, v1, :cond_9

    .line 1146
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_TEMPERATURE:I

    goto :goto_0

    .line 1147
    :cond_9
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne p1, v1, :cond_a

    .line 1148
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_LIGHT:I

    goto :goto_0

    .line 1149
    :cond_a
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne p1, v1, :cond_b

    .line 1150
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC:I

    goto :goto_0

    .line 1151
    :cond_b
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne p1, v1, :cond_c

    .line 1152
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_ON:I

    goto :goto_0

    .line 1153
    :cond_c
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne p1, v1, :cond_d

    .line 1154
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

    goto :goto_0

    .line 1155
    :cond_d
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne p1, v1, :cond_e

    .line 1156
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_STATUS:I

    goto :goto_0

    .line 1157
    :cond_e
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne p1, v1, :cond_f

    .line 1158
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_TEMPERATURE:I

    goto :goto_0

    .line 1159
    :cond_f
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne p1, v1, :cond_10

    .line 1160
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_DAC:I

    goto :goto_0

    .line 1161
    :cond_10
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne p1, v1, :cond_11

    .line 1162
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_ADC:I

    goto :goto_0

    .line 1163
    :cond_11
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne p1, v1, :cond_12

    .line 1164
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_SELF:I

    goto :goto_0

    .line 1165
    :cond_12
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne p1, v1, :cond_13

    .line 1166
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_OFFSETH:I

    goto :goto_0

    .line 1167
    :cond_13
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne p1, v1, :cond_0

    .line 1168
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_PROXIMITY:I

    goto/16 :goto_0
.end method

.method static synthetic access$102(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p1, "x1"    # [F

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Accelerometer:[F

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p1, "x1"    # [F

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Barometer:[F

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p1, "x1"    # [F

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Gyro:[F

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p1, "x1"    # [F

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Light:[F

    return-object p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p1, "x1"    # [F

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Magnetic:[F

    return-object p1
.end method

.method static synthetic access$602(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p1, "x1"    # [F

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Proximity:[F

    return-object p1
.end method

.method private dataCheck([F)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [F

    .prologue
    .line 1177
    const-string v1, ""

    .line 1179
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 1180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 1181
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1183
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 1184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1180
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1188
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 1191
    :cond_2
    return-object v1
.end method


# virtual methods
.method public isSensorOn(I)Z
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->ConverterID(I)I

    move-result v0

    .line 252
    .local v0, "sensorOnArrayIndex":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    aget-boolean v1, v1, v0

    return v1
.end method

.method public returnAccelermeter()[F
    .locals 3

    .prologue
    .line 291
    const-string v0, "SensorReadManager"

    const-string v1, "returnAccelermeter"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Accelerometer:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Accelerometer:[F

    return-object v0
.end method

.method public returnBarometer()[F
    .locals 3

    .prologue
    .line 299
    const-string v0, "SensorReadManager"

    const-string v1, "returnBarometer"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Barometer:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Barometer:[F

    return-object v0
.end method

.method public returnGyro()[F
    .locals 3

    .prologue
    .line 307
    const-string v0, "SensorReadManager"

    const-string v1, "returnGyro"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Gyro:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Gyro:[F

    return-object v0
.end method

.method public returnGyroExpansion()Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnGyroPower()I
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x1

    return v0
.end method

.method public returnLight()[F
    .locals 3

    .prologue
    .line 315
    const-string v0, "SensorReadManager"

    const-string v1, "returnLight"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Light:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Light:[F

    return-object v0
.end method

.method public returnMagnetic()[F
    .locals 3

    .prologue
    .line 323
    const-string v0, "SensorReadManager"

    const-string v1, "returnMagnetic"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Magnetic:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Magnetic:[F

    return-object v0
.end method

.method public returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    .locals 10
    .param p1, "testNo"    # I
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 786
    new-array v0, v9, [I

    .line 787
    .local v0, "parameter1":[I
    new-array v1, v9, [I

    .line 788
    .local v1, "parameter2":[I
    new-array v2, v9, [I

    .line 789
    .local v2, "parameter3":[I
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    .line 791
    .local v3, "returnData":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    if-ne p1, v7, :cond_3

    .line 792
    aput v7, v0, v6

    .line 794
    if-eqz v1, :cond_0

    .line 795
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    .line 797
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_2

    .line 798
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 799
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    .line 901
    :cond_0
    :goto_0
    if-ne p1, v8, :cond_1

    .line 902
    aput v8, v0, v6

    .line 904
    if-eqz v1, :cond_1

    .line 905
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    .line 907
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_d

    .line 908
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 909
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    .line 917
    :cond_1
    :goto_1
    return-object v3

    .line 801
    :cond_2
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 802
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto :goto_0

    .line 805
    :cond_3
    if-ne p1, v9, :cond_5

    .line 806
    aput v7, v0, v6

    .line 807
    aput v9, v0, v6

    .line 809
    if-eqz v1, :cond_0

    .line 810
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    .line 812
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_4

    .line 813
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 818
    :goto_2
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    goto :goto_0

    .line 815
    :cond_4
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_2

    .line 820
    :cond_5
    if-eq p1, v8, :cond_0

    .line 821
    if-ne p1, v5, :cond_7

    .line 822
    aput v7, v0, v6

    .line 823
    aput v9, v0, v6

    .line 824
    aput v5, v0, v6

    .line 826
    if-eqz v1, :cond_0

    .line 827
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    .line 829
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_6

    .line 830
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 835
    :goto_3
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 836
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 837
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 832
    :cond_6
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_3

    .line 839
    :cond_7
    const/4 v4, 0x5

    if-ne p1, v4, :cond_9

    .line 840
    aput v7, v0, v6

    .line 841
    aput v9, v0, v6

    .line 842
    aput v5, v0, v6

    .line 843
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 845
    if-eqz v1, :cond_0

    .line 846
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    .line 848
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_8

    .line 849
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 854
    :goto_4
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 855
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v8

    .line 856
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 851
    :cond_8
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_4

    .line 858
    :cond_9
    const/4 v4, 0x6

    if-ne p1, v4, :cond_b

    .line 859
    aput v7, v0, v6

    .line 860
    aput v9, v0, v6

    .line 861
    aput v5, v0, v6

    .line 862
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 863
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 865
    if-eqz v1, :cond_0

    .line 866
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    .line 868
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_a

    .line 869
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 874
    :goto_5
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 875
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 876
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 871
    :cond_a
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_5

    .line 878
    :cond_b
    const/4 v4, 0x7

    if-ne p1, v4, :cond_0

    .line 879
    aput v7, v0, v6

    .line 880
    aput v9, v0, v6

    .line 881
    aput v5, v0, v6

    .line 882
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 883
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 884
    const/4 v4, 0x7

    aput v4, v0, v6

    .line 886
    if-eqz v1, :cond_0

    .line 887
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    .line 889
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_c

    .line 890
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 895
    :goto_6
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 896
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 897
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 892
    :cond_c
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_6

    .line 911
    :cond_d
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 912
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto/16 :goto_1
.end method

.method public returnMagneticExpansion_Asahi(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    .locals 1
    .param p1, "testNo"    # I
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 583
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnMagneticExpansion_Bosch(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    .locals 1
    .param p1, "testNo"    # I
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 986
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnMagneticExpansion_STMicro(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    .locals 10
    .param p1, "testNo"    # I
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 990
    new-array v0, v9, [I

    .line 991
    .local v0, "parameter1":[I
    new-array v1, v9, [I

    .line 992
    .local v1, "parameter2":[I
    new-array v2, v9, [I

    .line 993
    .local v2, "parameter3":[I
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    .line 995
    .local v3, "returnData":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    if-ne p1, v7, :cond_3

    .line 996
    aput v7, v0, v6

    .line 998
    if-eqz v1, :cond_0

    .line 999
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    .line 1001
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_2

    .line 1002
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1003
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    .line 1105
    :cond_0
    :goto_0
    if-ne p1, v8, :cond_1

    .line 1106
    aput v8, v0, v6

    .line 1108
    if-eqz v1, :cond_1

    .line 1109
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    .line 1111
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_d

    .line 1112
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1113
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    .line 1121
    :cond_1
    :goto_1
    return-object v3

    .line 1005
    :cond_2
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 1006
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto :goto_0

    .line 1009
    :cond_3
    if-ne p1, v9, :cond_5

    .line 1010
    aput v7, v0, v6

    .line 1011
    aput v9, v0, v6

    .line 1013
    if-eqz v1, :cond_0

    .line 1014
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    .line 1016
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_4

    .line 1017
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1022
    :goto_2
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    goto :goto_0

    .line 1019
    :cond_4
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_2

    .line 1024
    :cond_5
    if-eq p1, v8, :cond_0

    .line 1025
    if-ne p1, v5, :cond_7

    .line 1026
    aput v7, v0, v6

    .line 1027
    aput v9, v0, v6

    .line 1028
    aput v5, v0, v6

    .line 1030
    if-eqz v1, :cond_0

    .line 1031
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    .line 1033
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_6

    .line 1034
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1039
    :goto_3
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1040
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1041
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1036
    :cond_6
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_3

    .line 1043
    :cond_7
    const/4 v4, 0x5

    if-ne p1, v4, :cond_9

    .line 1044
    aput v7, v0, v6

    .line 1045
    aput v9, v0, v6

    .line 1046
    aput v5, v0, v6

    .line 1047
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1049
    if-eqz v1, :cond_0

    .line 1050
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    .line 1052
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_8

    .line 1053
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1058
    :goto_4
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1059
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v8

    .line 1060
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1055
    :cond_8
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_4

    .line 1062
    :cond_9
    const/4 v4, 0x6

    if-ne p1, v4, :cond_b

    .line 1063
    aput v7, v0, v6

    .line 1064
    aput v9, v0, v6

    .line 1065
    aput v5, v0, v6

    .line 1066
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1067
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 1069
    if-eqz v1, :cond_0

    .line 1070
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    .line 1072
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_a

    .line 1073
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1078
    :goto_5
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1079
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1080
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1075
    :cond_a
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_5

    .line 1082
    :cond_b
    const/4 v4, 0x7

    if-ne p1, v4, :cond_0

    .line 1083
    aput v7, v0, v6

    .line 1084
    aput v9, v0, v6

    .line 1085
    aput v5, v0, v6

    .line 1086
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1087
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 1088
    const/4 v4, 0x7

    aput v4, v0, v6

    .line 1090
    if-eqz v1, :cond_0

    .line 1091
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    .line 1093
    iget v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_c

    .line 1094
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1099
    :goto_6
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1100
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1101
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1096
    :cond_c
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_6

    .line 1115
    :cond_d
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 1116
    iget-object v4, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto/16 :goto_1
.end method

.method public returnMagneticExpansion_Yamaha(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;
    .locals 1
    .param p1, "testNo"    # I
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 782
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnProximity()[F
    .locals 3

    .prologue
    .line 331
    const-string v0, "SensorReadManager"

    const-string v1, "returnProximity"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Proximity:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Proximity:[F

    return-object v0
.end method

.method public sensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 222
    const-string v1, "SensorReadManager"

    const-string v2, "sensorOff"

    const-string v3, "Sensor Off"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 228
    :cond_0
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 229
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorListener:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;

    .line 230
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 231
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 232
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 233
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 234
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 235
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 236
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Accelerometer:[F

    .line 237
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Barometer:[F

    .line 238
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Gyro:[F

    .line 239
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Light:[F

    .line 240
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Magnetic:[F

    .line 241
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Proximity:[F

    .line 243
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v1, :cond_1

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_1
    return-void
.end method
