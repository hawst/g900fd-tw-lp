.class public Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
.super Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;
.source "SensorUV.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;
    }
.end annotation


# static fields
.field private static final INTERVAL:I = 0x1f4

.field public static final UVA:I = 0x1

.field public static final UVADC:I = 0x1

.field public static final UVB:I = 0x2

.field public static final UVIndex:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private isUVMGROn:Z

.field private mBuffer:[Ljava/lang/String;

.field private mBuffer_SensorValue_Cloud:[I

.field private mBuffer_SensorValue_UV:[F

.field private mContext:Landroid/content/Context;

.field private mMaximSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTimer:Ljava/util/Timer;

.field private mUVSensor:Landroid/hardware/Sensor;

.field private mVendor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;-><init>()V

    .line 25
    const-string v0, "SensorUV"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->CLASS_NAME:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    .line 50
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->CheckVendorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    .line 53
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV Sensor Vendor is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;Landroid/content/Context;)V
    .locals 4
    .param p1, "mSensorListener"    # Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;-><init>()V

    .line 25
    const-string v0, "SensorUV"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->CLASS_NAME:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    .line 57
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    .line 59
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->CheckVendorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    .line 61
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV Sensor Vendor is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method private CheckVendorName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 250
    const-string v1, ""

    .line 253
    .local v1, "mVendor":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    const v3, 0x1001d

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 260
    :goto_0
    return-object v1

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 257
    const-string v2, "UV"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private SensorOn(II)V
    .locals 6
    .param p1, "sensorID"    # I
    .param p2, "interval"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isSensorOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1001d

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 80
    const-string v0, "SensorUV"

    const-string v1, "SensorOn"

    const-string v2, "register-UVSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    :goto_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$1;-><init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 92
    return-void

    .line 83
    :cond_1
    const-string v0, "SensorUV"

    const-string v1, "SensorOn"

    const-string v2, "SensorManager null !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    .param p1, "x1"    # [I

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public SensorOff()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 95
    const-string v0, "SensorUV"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mMaximSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->SensorOff()V

    .line 111
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    .line 112
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 113
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 114
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    .line 115
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 102
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 103
    const-string v0, "SensorUV"

    const-string v1, "mTimer canceled"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method

.method public SensorOn()V
    .locals 2

    .prologue
    .line 65
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;-><init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mMaximSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mMaximSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->SensorOn()Z

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    const v0, 0x1001d

    const/16 v1, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOn(II)V

    goto :goto_0
.end method

.method public getADC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 134
    const-string v0, "0"

    .line 136
    .local v0, "res":Ljava/lang/String;
    const-string v1, "MAXIM"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    array-length v1, v1

    if-le v1, v3, :cond_0

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    aget-object v0, v1, v3

    .line 145
    :cond_0
    :goto_0
    return-object v0

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v1, v1

    if-le v1, v3, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUVA()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 149
    const-string v0, "0"

    .line 151
    .local v0, "res":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v1, v1

    if-le v1, v2, :cond_0

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    .line 155
    :cond_0
    return-object v0
.end method

.method public getUVB()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 159
    const-string v0, "0"

    .line 161
    .local v0, "res":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v1, v1

    if-le v1, v2, :cond_0

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    .line 165
    :cond_0
    return-object v0
.end method

.method public getUVIndex()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 169
    const-string v0, "0"

    .line 171
    .local v0, "res":Ljava/lang/String;
    const-string v1, "MAXIM"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    aget-object v0, v1, v4

    .line 181
    :cond_0
    :goto_0
    return-object v0

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v1, v1

    if-lez v1, :cond_0

    .line 177
    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    return-object v0
.end method

.method public isSensorOn()Z
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 120
    .local v0, "res":Z
    const-string v1, "MAXIM"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mMaximSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->isSensorOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    const/4 v0, 0x1

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    .line 126
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 230
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 234
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 242
    :goto_0
    return-void

    .line 236
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    .line 237
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    goto :goto_0

    .line 234
    :pswitch_data_0
    .packed-switch 0x1001d
        :pswitch_0
    .end packed-switch
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 0
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 247
    return-void
.end method

.method public returnCloudValues([I)V
    .locals 6
    .param p1, "buffer_VB"    # [I

    .prologue
    const/16 v5, 0xe

    const/4 v4, 0x0

    .line 211
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 212
    if-eqz p1, :cond_2

    .line 213
    const-string v1, ""

    .line 214
    .local v1, "res":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    invoke-interface {v2, v5, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 224
    .end local v0    # "i":I
    .end local v1    # "res":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    const-string v3, ""

    invoke-interface {v2, v4, v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 225
    return-void

    .line 220
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    const-string v3, "null"

    invoke-interface {v2, v5, v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public returnSensorValues()V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x0

    .line 185
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 187
    :cond_0
    const-string v1, ""

    .line 188
    .local v1, "res":Ljava/lang/String;
    const-string v2, "MAXIM"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 190
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v2, v2

    if-lez v2, :cond_2

    .line 194
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 195
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 200
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    invoke-interface {v2, v5, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 207
    .end local v1    # "res":Ljava/lang/String;
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    const-string v3, ""

    invoke-interface {v2, v4, v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 208
    return-void

    .line 203
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    const-string v3, "null"

    invoke-interface {v2, v5, v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_2
.end method
