.class public Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "MagneticSTMicro.java"


# instance fields
.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mSensorID_ADC:I

.field private mSensorID_DAC:I

.field private mSensorID_Initialized:I

.field private mSensorID_None:I

.field private mSensorID_Released:I

.field private mSensorID_Self:I

.field private mSensorID_Status:I

.field private mTableRow_ADC:Landroid/widget/TableRow;

.field private mTableRow_DAC:Landroid/widget/TableRow;

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTableRow_Offset_H:Landroid/widget/TableRow;

.field private mTableRow_SX:Landroid/widget/TableRow;

.field private mTableRow_SY:Landroid/widget/TableRow;

.field private mTableRow_SZ:Landroid/widget/TableRow;

.field private mTableRow_Status:Landroid/widget/TableRow;

.field private mTableRow_Temp:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_ADC_X:Landroid/widget/TextView;

.field private mText_ADC_Y:Landroid/widget/TextView;

.field private mText_ADC_Z:Landroid/widget/TextView;

.field private mText_DAC_X:Landroid/widget/TextView;

.field private mText_DAC_Y:Landroid/widget/TextView;

.field private mText_DAC_Z:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Offset_H_X:Landroid/widget/TextView;

.field private mText_Offset_H_Y:Landroid/widget/TextView;

.field private mText_Offset_H_Z:Landroid/widget/TextView;

.field private mText_SX:Landroid/widget/TextView;

.field private mText_SY:Landroid/widget/TextView;

.field private mText_SZ:Landroid/widget/TextView;

.field private mText_Status:Landroid/widget/TextView;

.field private mText_Temp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "MagneticSTMicro"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSenserID:[I

    .line 49
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    .line 50
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Initialized:I

    .line 51
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Status:I

    .line 52
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_DAC:I

    .line 53
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_ADC:I

    .line 54
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Self:I

    .line 56
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Released:I

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->WHAT_UPDATE:I

    .line 98
    new-instance v0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro$1;-><init>(Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mHandler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->update()V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 108
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 109
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Initialized:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0b0137

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_Status:Landroid/widget/TableRow;

    .line 112
    const v0, 0x7f0b0169

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Status:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f0b016a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_SX:Landroid/widget/TableRow;

    .line 119
    const v0, 0x7f0b016b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SX:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f0b016c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_SY:Landroid/widget/TableRow;

    .line 122
    const v0, 0x7f0b016d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SY:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f0b0182

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_SZ:Landroid/widget/TableRow;

    .line 125
    const v0, 0x7f0b0183

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SZ:Landroid/widget/TextView;

    .line 127
    const v0, 0x7f0b016e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_DAC:Landroid/widget/TableRow;

    .line 128
    const v0, 0x7f0b016f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_X:Landroid/widget/TextView;

    .line 129
    const v0, 0x7f0b0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_Y:Landroid/widget/TextView;

    .line 130
    const v0, 0x7f0b0171

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_Z:Landroid/widget/TextView;

    .line 132
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_ADC:Landroid/widget/TableRow;

    .line 133
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_X:Landroid/widget/TextView;

    .line 134
    const v0, 0x7f0b0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_Y:Landroid/widget/TextView;

    .line 135
    const v0, 0x7f0b0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_Z:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f0b0176

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_Offset_H:Landroid/widget/TableRow;

    .line 138
    const v0, 0x7f0b0177

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Offset_H_X:Landroid/widget/TextView;

    .line 139
    const v0, 0x7f0b0178

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Offset_H_Y:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f0b0179

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Offset_H_Z:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTextResult:Landroid/widget/TextView;

    .line 143
    return-void
.end method

.method private update()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "data":[Ljava/lang/String;
    const/4 v1, 0x1

    .line 151
    .local v1, "isPass":Z
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Initialized:I

    if-ge v2, v3, :cond_0

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_Initialized:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Initialized:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_7

    .line 156
    aget-object v2, v0, v6

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Initialized:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Initialized Return : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    :goto_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Status:I

    if-ge v2, v3, :cond_1

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_Status:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Status:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_9

    .line 178
    aget-object v2, v0, v7

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Status - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Status:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    :goto_3
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_DAC:I

    if-ge v2, v3, :cond_2

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_DAC:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_DAC:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_b

    .line 201
    aget-object v2, v0, v7

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 202
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "DAC - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :goto_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_X:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_Y:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_Z:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [DAC]X:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Z:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_2
    :goto_5
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_ADC:I

    if-ge v2, v3, :cond_3

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_ADC:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_ADC:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_d

    .line 228
    aget-object v2, v0, v7

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 229
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :goto_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_X:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_Y:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_Z:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [ADC]X:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Z:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_3
    :goto_7
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Self:I

    if-ge v2, v3, :cond_4

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_SX:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_SY:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTableRow_SZ:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Self:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_f

    .line 257
    aget-object v2, v0, v7

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Self - Pass (data[0] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :goto_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SX:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SY:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SZ:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SX:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SY:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SZ:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_4
    :goto_9
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Released:I

    if-ge v2, v3, :cond_5

    .line 292
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Released:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_11

    .line 295
    aget-object v2, v0, v6

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Released - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :goto_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Released Return : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_5
    :goto_b
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_12

    const-string v2, "PASS"

    :goto_c
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_13

    const v2, -0xffff01

    :goto_d
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "update"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Result:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_14

    const-string v2, "PASS"

    :goto_e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    return-void

    .line 159
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 166
    :cond_7
    const/4 v1, 0x0

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Initialized:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 181
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Status - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 188
    :cond_9
    const/4 v1, 0x0

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_Status:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Status - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 204
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "DAC - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 214
    :cond_b
    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_X:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_Y:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_DAC_Z:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "DAC - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 231
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 241
    :cond_d
    const/4 v1, 0x0

    .line 242
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_X:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 260
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Self - Fail (data[0] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v1, 0x0

    goto/16 :goto_8

    .line 270
    :cond_f
    const/4 v1, 0x0

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SX:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SY:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mText_SZ:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 298
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Released - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v1, 0x0

    goto/16 :goto_a

    .line 304
    :cond_11
    const/4 v1, 0x0

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Released - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 310
    :cond_12
    const-string v2, "FAIL"

    goto/16 :goto_c

    .line 311
    :cond_13
    const/high16 v2, -0x10000

    goto/16 :goto_d

    .line 312
    :cond_14
    const-string v2, "FAIL"

    goto/16 :goto_e
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->setContentView(I)V

    .line 67
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mFeature:Ljava/lang/String;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->init()V

    .line 70
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 96
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 73
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mFeature:Ljava/lang/String;

    const-string v1, "STMICRO_K303C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Initialized:I

    .line 77
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Status:I

    .line 78
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_ADC:I

    .line 79
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Self:I

    .line 82
    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Initialized:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Status:I

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_ADC:I

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSensorID_Self:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSenserID:[I

    .line 87
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 91
    return-void
.end method
