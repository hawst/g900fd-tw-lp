.class public Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TempHumidDisplay.java"

# interfaces
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;,
        Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;
    }
.end annotation


# instance fields
.field private HumidSensorMgrValue:Ljava/lang/String;

.field public final MSG_UPDATE_UI:I

.field private final TEXT_SCALING_COOR:F

.field private TempSensorMgrValue:Ljava/lang/String;

.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mHumidCheck:F

.field private mHumidComp:F

.field private mHumidRaw:F

.field private mListView:Landroid/widget/ListView;

.field private mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

.field private mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

.field private mTempCheck:F

.field private mTempComp:F

.field private mTempRaw:F

.field private mUIHandler:Landroid/os/Handler;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    const-string v0, "TempHumidDisplay"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 38
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->TEXT_SCALING_COOR:F

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mValueList:Ljava/util/List;

    .line 52
    const-string v0, "0,0,0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->TempSensorMgrValue:Ljava/lang/String;

    .line 53
    const-string v0, "0,0,0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->HumidSensorMgrValue:Ljava/lang/String;

    .line 54
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->MSG_UPDATE_UI:I

    .line 56
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mCount:I

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHandler:Landroid/os/Handler;

    .line 63
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$1;-><init>(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mUIHandler:Landroid/os/Handler;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mCount:I

    return v0
.end method

.method static synthetic access$004(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempRaw:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidRaw:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempComp:F

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidComp:F

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempCheck:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidCheck:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private displayValueFromSensor()V
    .locals 6

    .prologue
    .line 143
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 144
    .local v0, "mTimer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;-><init>(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 158
    return-void
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 161
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;

    const v1, 0x7f030078

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;

    .line 162
    const v0, 0x7f0b0092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mListView:Landroid/widget/ListView;

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 164
    return-void
.end method

.method private startTempHumidDisplay()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->displayValueFromSensor()V

    .line 96
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const v0, 0x7f030077

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->setContentView(I)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->initTextView()V

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->startTempHumidDisplay()V

    .line 81
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOff()V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;->SensorOff()V

    .line 108
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v5, 0x3e8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 84
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 85
    new-array v1, v4, [I

    const/16 v2, 0x9

    aput v2, v1, v3

    .line 86
    .local v1, "mSensorTempID":[I
    new-array v0, v4, [I

    const/16 v2, 0xa

    aput v2, v0, v3

    .line 87
    .local v0, "mSensorHumidID":[I
    new-instance v2, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-virtual {v2, p0, v1, v5}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V

    .line 89
    new-instance v2, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    invoke-virtual {v2, p0, v0, v5}, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;->SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V

    .line 91
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 4
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    sparse-switch p1, :sswitch_data_0

    .line 129
    :goto_0
    return-void

    .line 115
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mUIHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 119
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_TEMP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->TempSensorMgrValue:Ljava/lang/String;

    goto :goto_0

    .line 123
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_HUMID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->HumidSensorMgrValue:Ljava/lang/String;

    goto :goto_0

    .line 113
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public updateUI()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->TempSensorMgrValue:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "TempCompRaw":[Ljava/lang/String;
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempComp:F

    .line 134
    aget-object v1, v0, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempRaw:F

    .line 135
    aget-object v1, v0, v5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempCheck:F

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->HumidSensorMgrValue:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 137
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidComp:F

    .line 138
    aget-object v1, v0, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidRaw:F

    .line 139
    aget-object v1, v0, v5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidCheck:F

    .line 140
    return-void
.end method
