.class public Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"


# static fields
.field private static KEYCODE_USER:I

.field private static mBaseActivityUsingCount:I

.field public static mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

.field private static mSystemScreenBrightness:I

.field private static mSystemScreenBrightnessMode:I


# instance fields
.field private final BACK_KEY_EVENT_TIMELAG:J

.field protected CLASS_NAME:Ljava/lang/String;

.field private mIsLongPress:Z

.field private mPrevBackKeyEventTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0xe4

    sput v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->KEYCODE_USER:I

    .line 24
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    const-string v0, "BaseActivity"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    .line 20
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->BACK_KEY_EVENT_TIMELAG:J

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mIsLongPress:Z

    .line 22
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mPrevBackKeyEventTime:J

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 139
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 140
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->overridePendingTransition(II)V

    .line 141
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Create "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    const/16 v1, 0xbb

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    sget v1, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->KEYCODE_USER:I

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveStatusBar(Landroid/view/Window;)V

    .line 47
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    .line 48
    return-void

    .line 47
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDestroy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Destroy "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 100
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, 0x1

    .line 110
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mIsLongPress:Z

    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mIsLongPress:Z

    .line 114
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mIsLongPress:Z

    if-nez v0, :cond_1

    .line 115
    packed-switch p1, :pswitch_data_0

    .line 131
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mPrevBackKeyEventTime:J

    .line 134
    :cond_1
    :goto_0
    return v4

    .line 118
    :pswitch_0
    iget-wide v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mPrevBackKeyEventTime:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 119
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mPrevBackKeyEventTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->finish()V

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mPrevBackKeyEventTime:J

    goto :goto_0

    .line 125
    :cond_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mPrevBackKeyEventTime:J

    goto :goto_0

    .line 115
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mIsLongPress:Z

    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pause "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 78
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Resume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isRunningFactoryApp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/FactoryTest;->isRunningFactoryApp()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->setBrightnessMode()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->setBrightness()V

    .line 71
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 72
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onStart"

    const-string v2, "onStart"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    sget v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    if-nez v0, :cond_0

    .line 54
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getBrightness()I

    move-result v0

    sput v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mSystemScreenBrightness:I

    .line 55
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getScreenBrightnessMode()I

    move-result v0

    sput v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mSystemScreenBrightnessMode:I

    .line 56
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/FactoryTest;->setRunningFactoryApp(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onStart"

    const-string v2, "setRunningFactoryApp true"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :cond_0
    sget v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    .line 62
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 63
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onStop"

    const-string v2, "onStop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    .line 85
    sget v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mBaseActivityUsingCount:I

    if-nez v0, :cond_1

    .line 86
    sget v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mSystemScreenBrightnessMode:I

    if-nez v0, :cond_0

    .line 87
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    sget v1, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mSystemScreenBrightness:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    .line 88
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    sget v1, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mSystemScreenBrightnessMode:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setScreenBrightnessMode(I)V

    .line 89
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/os/FactoryTest;->setRunningFactoryApp(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onStop"

    const-string v2, "setRunningFactoryApp false"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 94
    return-void
.end method

.method public setBrightness()V
    .locals 2

    .prologue
    .line 144
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    .line 145
    return-void
.end method

.method public setBrightnessMode()V
    .locals 2

    .prologue
    .line 148
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setScreenBrightnessMode(I)V

    .line 149
    return-void
.end method

.method public unSetBrightnessMode()V
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setScreenBrightnessMode(I)V

    .line 153
    return-void
.end method
