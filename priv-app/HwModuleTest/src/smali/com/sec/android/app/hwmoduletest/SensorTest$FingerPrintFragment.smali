.class Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;
.super Ljava/lang/Object;
.source "SensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FingerPrintFragment"
.end annotation


# static fields
.field private static CLASS_NAME:Ljava/lang/String;

.field private static mFragment_fingerprint:Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;

.field private static use_Fingerprint:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1684
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->use_Fingerprint:Z

    .line 1686
    const-string v0, "FingerPrintFragment"

    sput-object v0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setBackGroundColor(I)V
    .locals 1
    .param p0, "color"    # I

    .prologue
    .line 1694
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->use_Fingerprint:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->mFragment_fingerprint:Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;

    if-eqz v0, :cond_0

    .line 1696
    :try_start_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->mFragment_fingerprint:Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setBackgroundColor(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1700
    :cond_0
    :goto_0
    return-void

    .line 1697
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static setEnable(Z)V
    .locals 4
    .param p0, "enable"    # Z

    .prologue
    .line 1689
    sget-object v0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setEnable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    sput-boolean p0, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->use_Fingerprint:Z

    .line 1691
    return-void
.end method

.method public static setFragment(Landroid/app/Activity;)V
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1703
    sget-boolean v2, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->use_Fingerprint:Z

    if-nez v2, :cond_0

    .line 1713
    :goto_0
    return-void

    .line 1706
    :cond_0
    sget-object v2, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setFragment"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1708
    .local v0, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1710
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v2, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->mFragment_fingerprint:Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;

    .line 1711
    const v2, 0x7f0b0220

    sget-object v3, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->mFragment_fingerprint:Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1712
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
