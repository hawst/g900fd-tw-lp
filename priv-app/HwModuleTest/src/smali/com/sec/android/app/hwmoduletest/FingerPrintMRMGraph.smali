.class public Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
.super Landroid/view/View;
.source "FingerPrintMRMGraph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private GRAPH_MAX_VALUE:F

.field private GRAPH_X_SCALING:F

.field private GRAPH_Y_SCALING:F

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private SCLAING:I

.field private mAvgData:I

.field private mAvgMinSepc:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mCalEnd:I

.field private mCalStart:I

.field private mCol:I

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mEachColWidth:[I

.field private mEachRowHeight:I

.field private mEachTitleRowHeight:I

.field private mEffects:Landroid/graphics/PathEffect;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMaxDataPrimary:I

.field private mMaxDataSecondary:I

.field private mMaxStdevPrimary:I

.field private mMaxStdevSecondary:I

.field private mMinDataPrimary:I

.field private mMinDataSecondary:I

.field private mMinStdevPrimary:I

.field private mMinStdevSecondary:I

.field private mNumberPaint:Landroid/graphics/Paint;

.field private mPathSpecMax:Landroid/graphics/Path;

.field private mPathSpecMin:Landroid/graphics/Path;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mPathZ:Landroid/graphics/Path;

.field private mPrimaryEndPixel:I

.field private mPrimarySpecMax:I

.field private mPrimarySpecMin:I

.field private mPrimaryStartPixel:I

.field private mRect:Landroid/graphics/Rect;

.field private mResultFailPaint:Landroid/graphics/Paint;

.field private mResultPassPaint:Landroid/graphics/Paint;

.field private mRow:I

.field private mScreenHeight:I

.field private mScreenHeight_Max:I

.field private mScreenTitleHeight_Max:I

.field private mScreenWidth:I

.field private mSecondaryEndPixel:I

.field private mSecondarySpecMax:I

.field private mSecondarySpecMin:I

.field private mSecondaryStartPixel:I

.field private mSpecPaint:Landroid/graphics/Paint;

.field private mStartPixel:I

.field private mSumData:I

.field private mTableTexts:[[Ljava/lang/String;

.field private mTextCenterPaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTitleRows:I

.field private mTitleTexts:[Ljava/lang/String;

.field private mValuePaint:Landroid/graphics/Paint;

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueZ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;

.field private final paddingHeight:I

.field private final paddingWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 90
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 24
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_X:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    .line 27
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    .line 28
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    .line 29
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->paddingWidth:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->paddingHeight:I

    .line 37
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    .line 38
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    .line 39
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    .line 40
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    .line 41
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight_Max:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    .line 43
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachTitleRowHeight:I

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathZ:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueZ:Ljava/util/ArrayList;

    .line 63
    const-string v0, "FingerPrintMRMGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->CLASS_NAME:Ljava/lang/String;

    .line 64
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    .line 65
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    .line 66
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    .line 67
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimaryStartPixel:I

    .line 68
    const/16 v0, 0x73

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimaryEndPixel:I

    .line 69
    const/16 v0, 0x78

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondaryStartPixel:I

    .line 70
    const/16 v0, 0xb9

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondaryEndPixel:I

    .line 71
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    .line 72
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMax:I

    .line 73
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x4b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondarySpecMin:I

    .line 74
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit16 v0, v0, 0xb4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondarySpecMax:I

    .line 75
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    .line 76
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    .line 77
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataSecondary:I

    .line 78
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataSecondary:I

    .line 79
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevPrimary:I

    .line 80
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxStdevPrimary:I

    .line 81
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevSecondary:I

    .line 82
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxStdevSecondary:I

    .line 83
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSumData:I

    .line 84
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgData:I

    .line 85
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgMinSepc:I

    .line 91
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->init(Landroid/content/Context;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_X:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    .line 27
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    .line 28
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    .line 29
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->paddingWidth:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->paddingHeight:I

    .line 37
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    .line 38
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    .line 39
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    .line 40
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    .line 41
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight_Max:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    .line 43
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachTitleRowHeight:I

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathZ:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueZ:Ljava/util/ArrayList;

    .line 63
    const-string v0, "FingerPrintMRMGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->CLASS_NAME:Ljava/lang/String;

    .line 64
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    .line 65
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    .line 66
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    .line 67
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimaryStartPixel:I

    .line 68
    const/16 v0, 0x73

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimaryEndPixel:I

    .line 69
    const/16 v0, 0x78

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondaryStartPixel:I

    .line 70
    const/16 v0, 0xb9

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondaryEndPixel:I

    .line 71
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    .line 72
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMax:I

    .line 73
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x4b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondarySpecMin:I

    .line 74
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit16 v0, v0, 0xb4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondarySpecMax:I

    .line 75
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    .line 76
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    .line 77
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataSecondary:I

    .line 78
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataSecondary:I

    .line 79
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevPrimary:I

    .line 80
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxStdevPrimary:I

    .line 81
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevSecondary:I

    .line 82
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxStdevSecondary:I

    .line 83
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSumData:I

    .line 84
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgData:I

    .line 85
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgMinSepc:I

    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->init(Landroid/content/Context;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_X:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    .line 27
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    .line 28
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    .line 29
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->paddingWidth:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->paddingHeight:I

    .line 37
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    .line 38
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    .line 39
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    .line 40
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    .line 41
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight_Max:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    .line 43
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachTitleRowHeight:I

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathZ:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueZ:Ljava/util/ArrayList;

    .line 63
    const-string v0, "FingerPrintMRMGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->CLASS_NAME:Ljava/lang/String;

    .line 64
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    .line 65
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    .line 66
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    .line 67
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimaryStartPixel:I

    .line 68
    const/16 v0, 0x73

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimaryEndPixel:I

    .line 69
    const/16 v0, 0x78

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondaryStartPixel:I

    .line 70
    const/16 v0, 0xb9

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondaryEndPixel:I

    .line 71
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    .line 72
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMax:I

    .line 73
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x4b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondarySpecMin:I

    .line 74
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit16 v0, v0, 0xb4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSecondarySpecMax:I

    .line 75
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    .line 76
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    .line 77
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataSecondary:I

    .line 78
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataSecondary:I

    .line 79
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevPrimary:I

    .line 80
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxStdevPrimary:I

    .line 81
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevSecondary:I

    .line 82
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxStdevSecondary:I

    .line 83
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSumData:I

    .line 84
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgData:I

    .line 85
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgMinSepc:I

    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->init(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method private calRawData()V
    .locals 7

    .prologue
    .line 330
    const/4 v1, 0x0

    .line 331
    .local v1, "tempX":I
    const/4 v2, 0x0

    .line 332
    .local v2, "tempY":I
    const/4 v3, 0x0

    .line 335
    .local v3, "tempZ":I
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 336
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueZ:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 348
    :goto_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    if-ge v1, v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    if-le v4, v5, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    add-int/lit8 v5, v5, 0x1

    if-gt v4, v5, :cond_0

    .line 349
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    .line 364
    :cond_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    if-le v1, v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    if-le v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    add-int/lit8 v5, v5, 0x1

    if-gt v4, v5, :cond_1

    .line 365
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    .line 379
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    if-le v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    add-int/lit8 v5, v5, 0x1

    if-gt v4, v5, :cond_2

    .line 380
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSumData:I

    add-int/2addr v4, v1

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSumData:I

    .line 381
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSumData:I

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    div-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgData:I

    .line 383
    :cond_2
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x42200000    # 40.0f

    const/high16 v7, -0x1000000

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 105
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mContext:Landroid/content/Context;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 107
    .local v1, "mWm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    .line 108
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight:I

    .line 109
    const-string v2, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    .line 112
    :cond_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight:I

    div-int/lit8 v2, v2, 0xc

    mul-int/lit8 v2, v2, 0x7

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    .line 113
    new-instance v2, Landroid/graphics/CornerPathEffect;

    const/high16 v3, 0x41200000    # 10.0f

    invoke-direct {v2, v3}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 114
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 119
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mYPaint:Landroid/graphics/Paint;

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mYPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mYPaint:Landroid/graphics/Paint;

    const v3, -0xff0100

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mYPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 124
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mZPaint:Landroid/graphics/Paint;

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mZPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mZPaint:Landroid/graphics/Paint;

    const v3, -0xff0001

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mZPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 129
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSpecPaint:Landroid/graphics/Paint;

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSpecPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 131
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSpecPaint:Landroid/graphics/Paint;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSpecPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 134
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 139
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mDataPaint:Landroid/graphics/Paint;

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 144
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 149
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 153
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 158
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 164
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 170
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 176
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 183
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    .line 184
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataSecondary:I

    .line 185
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevPrimary:I

    .line 186
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinStdevSecondary:I

    .line 188
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v2, v2, -0x14

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    div-int/lit8 v4, v4, 0x5

    mul-int/lit8 v4, v4, 0x2

    aput v4, v2, v3

    .line 190
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    if-ge v0, v2, :cond_1

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    div-int/lit8 v3, v3, 0x5

    mul-int/lit8 v3, v3, 0x3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    add-int/lit8 v4, v4, -0x1

    div-int/2addr v3, v4

    aput v3, v2, v0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_1
    const-string v2, "FINGERPRINT_MRM_PIXEL_START"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    .line 195
    const-string v2, "FINGERPRINT_MRM_PIXEL_END"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    .line 196
    const-string v2, "FINGERPRINT_MRM_SPEC_MIN"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    .line 197
    const-string v2, "FINGERPRINT_MRM_SPEC_MAX"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMax:I

    .line 198
    const-string v2, "FINGERPRINT_MRM_SPEC_AVG_MIN"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgMinSepc:I

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->invalidate()V

    .line 200
    return-void
.end method

.method public static isNumeric(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 312
    const-string v0, "-?\\d+(\\.\\d+)?"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addValue(III)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->calRawData()V

    .line 327
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v13, 0x0

    const/high16 v12, 0x41a00000    # 20.0f

    const/high16 v11, 0x42200000    # 40.0f

    const/high16 v10, 0x42480000    # 50.0f

    const/high16 v9, 0x41200000    # 10.0f

    .line 204
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 206
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenHeight:I

    int-to-float v0, v0

    div-float v6, v0, v12

    .line 209
    .local v6, "baseYpx":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenTitleHeight_Max:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v6, v0

    .line 210
    const-string v0, "Delta :"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 211
    const/high16 v1, 0x43020000    # 130.0f

    sub-float v2, v6, v9

    const/high16 v3, 0x43520000    # 210.0f

    sub-float v4, v6, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 217
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v10

    move v3, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 218
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 220
    const-string v0, "100"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 221
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v9

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v9

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    sub-float v4, v0, v1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 223
    const-string v0, "200"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v12

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 224
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v12

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v12

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    sub-float v4, v0, v1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 226
    const-string v0, "300"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v1, v1

    const/high16 v2, 0x41f00000    # 30.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 227
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v1, 0x41f00000    # 30.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v1, 0x41f00000    # 30.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    sub-float v4, v0, v1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 229
    const-string v0, "400"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v11

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 230
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v11

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v11

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    sub-float v4, v0, v1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 232
    const-string v0, "500"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v10

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 233
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v10

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    sub-float v4, v0, v1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 235
    const-string v0, "600"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    add-float/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 236
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float v2, v0, v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    sub-float v4, v0, v1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 239
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v12

    add-float v1, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v12

    add-float v3, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 241
    const-string v0, " 20"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v12

    add-float/2addr v1, v10

    sub-float/2addr v1, v12

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 242
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v11

    add-float v1, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v11

    add-float v3, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 244
    const-string v0, " 40"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v11

    add-float/2addr v1, v10

    sub-float/2addr v1, v12

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 245
    const/high16 v0, 0x42700000    # 60.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v1

    add-float v1, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42700000    # 60.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v3

    add-float v3, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 247
    const-string v0, " 60"

    const/high16 v1, 0x42700000    # 60.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v10

    sub-float/2addr v1, v12

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 248
    const/high16 v0, 0x42a00000    # 80.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v1

    add-float v1, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42a00000    # 80.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v0, v3

    add-float v3, v10, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 250
    const-string v0, " 80"

    const/high16 v1, 0x42a00000    # 80.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v10

    sub-float/2addr v1, v12

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 252
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, 0x82

    int-to-float v6, v0

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 263
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0x1e

    int-to-float v6, v0

    .line 264
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleRows:I

    if-ge v7, v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    const/16 v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    mul-int/2addr v1, v7

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v1, v7, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    aget-object v0, v0, v7

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0xa

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 264
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 276
    :cond_1
    const/4 v7, 0x0

    :goto_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    if-ge v7, v0, :cond_8

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    const/16 v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v1, v1, v13

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    mul-int/2addr v2, v7

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v2, v7, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v13

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v13

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0xa

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 286
    :cond_2
    const/4 v8, 0x1

    .local v8, "j":I
    :goto_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    if-ge v8, v0, :cond_7

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v1, v1, v13

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v2, v8, -0x1

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v3, v3, v8

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v1, v1, v13

    add-int/lit8 v1, v1, 0xa

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v2, v2, v8

    mul-int/2addr v2, v8

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    mul-int/2addr v2, v7

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v2, v7, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    if-eqz v0, :cond_3

    .line 294
    const-string v0, "PASS"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v7

    aget-object v1, v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v2, v2, v8

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 286
    :cond_3
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 296
    :cond_4
    const-string v0, "FAIL"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v7

    aget-object v1, v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v2, v2, v8

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 298
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v2, v2, v8

    div-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 301
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachColWidth:[I

    aget v2, v2, v8

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mEachRowHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 276
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 308
    .end local v8    # "j":I
    :cond_8
    return-void
.end method

.method public setGraphXScale(F)V
    .locals 0
    .param p1, "xScale"    # F

    .prologue
    .line 467
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    .line 468
    return-void
.end method

.method public setGraphYScale(F)V
    .locals 0
    .param p1, "yScale"    # F

    .prologue
    .line 471
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    .line 472
    return-void
.end method

.method public setStartPixel(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 475
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    .line 476
    return-void
.end method

.method public setTextData(IILjava/lang/String;)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 316
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mRow:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCol:I

    if-ge p2, v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, p1

    aput-object p3, v0, p2

    .line 319
    :cond_0
    return-void
.end method

.method public showGraph()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/high16 v7, 0x42480000    # 50.0f

    const/4 v6, 0x2

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 387
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 389
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    .line 400
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v7, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 401
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v7, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 403
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathX:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v7, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathY:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v7, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 403
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 420
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 421
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-virtual {v1, v7, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v7

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v4, v4, 0x0

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mStartPixel:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v7

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v7

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v7

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v4, v4, 0x0

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v7

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->INIT_COOR_Y:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->SCLAING:I

    mul-int/lit8 v4, v4, 0x0

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Section ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalStart:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mCalEnd:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mTitleTexts:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Spec ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMax:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Spec Avg ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgMinSepc:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~ )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 435
    const-string v1, ""

    invoke-virtual {p0, v9, v9, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 436
    const-string v1, "VALUE"

    invoke-virtual {p0, v9, v8, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 437
    const-string v1, "RESULT"

    invoke-virtual {p0, v9, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 438
    const-string v1, "Delta Min"

    invoke-virtual {p0, v8, v9, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 439
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v8, v8, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 440
    const-string v1, "Delta Max"

    invoke-virtual {p0, v6, v9, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 441
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6, v8, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 443
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMinDataPrimary:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMin:I

    if-le v1, v2, :cond_1

    .line 444
    const-string v1, "PASS"

    invoke-virtual {p0, v8, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 449
    :goto_1
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mMaxDataPrimary:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mPrimarySpecMax:I

    if-ge v1, v2, :cond_2

    .line 450
    const-string v1, "PASS"

    invoke-virtual {p0, v6, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 455
    :goto_2
    const-string v1, "Delta Avg"

    invoke-virtual {p0, v10, v9, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 456
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgData:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v10, v8, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 458
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgData:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->mAvgMinSepc:I

    if-le v1, v2, :cond_3

    .line 459
    const-string v1, "PASS"

    invoke-virtual {p0, v10, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    .line 463
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->invalidate()V

    .line 464
    return-void

    .line 446
    :cond_1
    const-string v1, "FAIL"

    invoke-virtual {p0, v8, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    goto :goto_1

    .line 452
    :cond_2
    const-string v1, "FAIL"

    invoke-virtual {p0, v6, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    goto :goto_2

    .line 461
    :cond_3
    const-string v1, "FAIL"

    invoke-virtual {p0, v10, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setTextData(IILjava/lang/String;)V

    goto :goto_3
.end method
