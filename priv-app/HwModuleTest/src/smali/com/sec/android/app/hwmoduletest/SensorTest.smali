.class public Lcom/sec/android/app/hwmoduletest/SensorTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "SensorTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;,
        Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;
    }
.end annotation


# static fields
.field private static final CP_ACCEL_POWER_OFF:I = 0x0

.field private static final CP_ACCEL_POWER_ON:I = 0x1

.field private static final TYPE_AIRMOTION:I = 0x2


# instance fields
.field private final ACTION_WAKE_UP:Ljava/lang/String;

.field private final CP_ACCELERMETER_OFF:Ljava/lang/String;

.field private final CP_ACCELERMETER_ON:Ljava/lang/String;

.field private final FEED_BACK_PATTERN:[J

.field private final MSG_SET_BAROMETER_SENSOR_PRESSURE:B

.field private final MSG_UPDATE_SENSOR_VALUE:B

.field private final MSG_VIBRATE_FEED_BACK_END:B

.field private final MSG_VIBRATE_FEED_BACK_START:B

.field private final TIMER_TASK_PERIOD:I

.field private final WORK_HANDLER_THREAD:B

.field private final WORK_SENSOR_THREAD:B

.field private bReadUltrasonicData:Z

.field private countValue:I

.field private isCloseMode:Z

.field private mAcceGraphButton:Landroid/widget/Button;

.field private mAcceImageTestButton:Landroid/widget/Button;

.field private mAcceSensorSleepValueText:Landroid/widget/TextView;

.field private mAcceSensorTitleText:Landroid/widget/TextView;

.field private mAcceSensorValueText:Landroid/widget/TextView;

.field private mAcceSeparator:Landroid/view/View;

.field private mAccelerometerSensor:Landroid/hardware/Sensor;

.field private mBIOSensor:Landroid/hardware/Sensor;

.field private mBackground:Landroid/widget/LinearLayout;

.field private mBaromPressureAvg:F

.field private mBaromPressureTotal:F

.field private mBaromPressureTotalArray:[F

.field private mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

.field private mBaromSensorAltitudeText:Landroid/widget/TextView;

.field private mBaromSensorPressureText:Landroid/widget/TextView;

.field private mBaromSensorSelftestButton:Landroid/widget/Button;

.field private mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

.field private mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

.field private mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

.field private mBaromSensorTitleText:Landroid/widget/TextView;

.field private mBaromSeparator:Landroid/view/View;

.field private mBarometerSensor:Landroid/hardware/Sensor;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCloseOutBoundCount:I

.field private mCpAcceSensorTitleText:Landroid/widget/TextView;

.field private mCpAcceSensorValueText:Landroid/widget/TextView;

.field private mCpAcceSeparator:Landroid/view/View;

.field private mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mGestureSensorCrosstalkButton:Landroid/widget/Button;

.field private mGestureSensorDirectionButton:Landroid/widget/Button;

.field private mGestureSensorTitleText:Landroid/widget/TextView;

.field private mGestureSeparator:Landroid/view/View;

.field private mGyroSensorDisplayButton:Landroid/widget/Button;

.field private mGyroSensorGraphButton:Landroid/widget/Button;

.field private mGyroSensorSelftestButton:Landroid/widget/Button;

.field private mGyroSensorSleepValueText:Landroid/widget/TextView;

.field private mGyroSensorTitleText:Landroid/widget/TextView;

.field private mGyroSensorValueText:Landroid/widget/TextView;

.field private mGyroSeparator:Landroid/view/View;

.field private mGyroscopeSensor:Landroid/hardware/Sensor;

.field private mHRMHRAvg:F

.field private mHRMHRCount:I

.field private mHRMHRMax:F

.field private mHRMHRMin:F

.field private mHRMHRTotal:F

.field private mHRMSensor:Landroid/hardware/Sensor;

.field private mHRMSensorDeivceIdText:Landroid/widget/TextView;

.field private mHRMSensorEOLTestButton:Landroid/widget/Button;

.field private mHRMSensorLibraryELFText:Landroid/widget/TextView;

.field private mHRMSensorLibraryEOLText:Landroid/widget/TextView;

.field private mHRMSensorLibraryText:Landroid/widget/TextView;

.field private mHRMSensorStartButton:Landroid/widget/Button;

.field private mHRMSensorTitleText:Landroid/widget/TextView;

.field private mHRMSeparator:Landroid/view/View;

.field private mHRMSignalQtyAvg:F

.field private mHRMSignalQtyCount:I

.field private mHRMSignalQtyMax:F

.field private mHRMSignalQtyMin:F

.field private mHRMSignalQtyTotal:F

.field private mHRMSpO2Avg:F

.field private mHRMSpO2Count:I

.field private mHRMSpO2Max:F

.field private mHRMSpO2Min:F

.field private mHRMSpO2Total:F

.field private mHandler:Landroid/os/Handler;

.field private mHrm_Vendor:Ljava/lang/String;

.field private mHumi_raw:Landroid/widget/TextView;

.field private mHumi_raw_sleep:Landroid/widget/TextView;

.field private mHumidSensor:Landroid/hardware/Sensor;

.field private mIsRGBSensor:Z

.field private mIsReadFromManager:Z

.field private mIsSensorHubTest:Z

.field private mIsVibrate:Z

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLightSensorAdcText:Landroid/widget/TextView;

.field private mLightSensorLuxText:Landroid/widget/TextView;

.field private mLightSensorSysfsPath:Ljava/lang/String;

.field private mLightSensorTestButton:Landroid/widget/Button;

.field private mLightSensorTitleText:Landroid/widget/TextView;

.field private mLightSeparator:Landroid/view/View;

.field private mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

.field private mMagnSensorRawValueText:Landroid/widget/TextView;

.field private mMagnSensorSelfTestButton:Landroid/widget/Button;

.field private mMagnSensorSleepValueText:Landroid/widget/TextView;

.field private mMagnSensorTitleText:Landroid/widget/TextView;

.field private mMagnSensorValueText:Landroid/widget/TextView;

.field private mMagnSeparator:Landroid/view/View;

.field private mMagneticSensor:Landroid/hardware/Sensor;

.field private mOISGyroSelftestButton:Landroid/widget/Button;

.field private mOISGyroSensorSub:Landroid/widget/TextView;

.field private mOrieSensorAzimuthText:Landroid/widget/TextView;

.field private mOrieSensorPitchText:Landroid/widget/TextView;

.field private mOrieSensorRollText:Landroid/widget/TextView;

.field private mOrientationSensor:Landroid/hardware/Sensor;

.field private mProduct:Ljava/lang/String;

.field private mProxSensorAdcAvgText:Landroid/widget/TextView;

.field private mProxSensorDefaultTrimText:Landroid/widget/TextView;

.field private mProxSensorHighThresholdText:Landroid/widget/TextView;

.field private mProxSensorLowThresholdText:Landroid/widget/TextView;

.field private mProxSensorOffsetText:Landroid/widget/TextView;

.field private mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

.field private mProxSensorStatusText:Landroid/widget/TextView;

.field private mProxSensorTitleText:Landroid/widget/TextView;

.field private mProxSensorTspColorIdText:Landroid/widget/TextView;

.field private mProxSensorValue:F

.field private mProxSeparator:Landroid/view/View;

.field private mProximityListener:Landroid/hardware/SensorEventListener;

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mProximityTimer:Ljava/util/Timer;

.field private mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

.field private mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private mSensorHubSeparator:Landroid/view/View;

.field private mSensorHubTestButton:Landroid/widget/Button;

.field private mSensorHubText:Landroid/widget/TextView;

.field private mSensorHubTitleText:Landroid/widget/TextView;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

.field private mTempHumidButton:Landroid/widget/Button;

.field private mTempHumidDisplayButton:Landroid/widget/Button;

.field private mTempHumidEngineText:Landroid/widget/TextView;

.field private mTempHumidGraphButton:Landroid/widget/Button;

.field private mTempHumidSeparator:Landroid/view/View;

.field private mTempHumidThermistorButton:Landroid/widget/Button;

.field private mTempHumidTitleText:Landroid/widget/TextView;

.field private mTempSensor:Landroid/hardware/Sensor;

.field private mTemp_raw:Landroid/widget/TextView;

.field private mTemp_raw_sleep:Landroid/widget/TextView;

.field mThreadGetData:Ljava/lang/Thread;

.field private mTimer:Ljava/util/Timer;

.field private mUVDisplayButton:Landroid/widget/Button;

.field private mUVICcheckButton:Landroid/widget/Button;

.field private mUVLibraryText:Landroid/widget/TextView;

.field private mUVSeparator:Landroid/view/View;

.field private mUVStartButton:Landroid/widget/Button;

.field private mUVTitleText:Landroid/widget/TextView;

.field private mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;

.field private mUltraSensorCompanyFWValueText:Landroid/widget/TextView;

.field private mUltraSensorTitleText:Landroid/widget/TextView;

.field private mUltraSeparator:Landroid/view/View;

.field private mUltrasonicSensor:Landroid/hardware/Sensor;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private magAdcData:[Ljava/lang/String;

.field private use_Accelerometer:Z

.field private use_Barometer:Z

.field private use_CpAccelerometer:Z

.field private use_Gesture:Z

.field private use_Gyroscope:Z

.field private use_HRM:Z

.field private use_Light_ADC:Z

.field private use_Light_LUX:Z

.field private use_Magnetic:Z

.field private use_Magnetic_PowerNoise:Z

.field private use_OIS_Gyro:Z

.field private use_Proximity:Z

.field private use_Proximity_ADC:Z

.field private use_TempHumid:Z

.field private use_UV:Z

.field private use_Ultrasonic:Z

.field private work_thread:B


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 436
    const-string v0, "SensorTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 67
    const-string v0, "com.sec.factory.WakeUp"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->ACTION_WAKE_UP:Ljava/lang/String;

    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->FEED_BACK_PATTERN:[J

    .line 71
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->TIMER_TASK_PERIOD:I

    .line 74
    const/16 v0, 0xa

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->MSG_UPDATE_SENSOR_VALUE:B

    .line 75
    const/16 v0, 0xb

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->MSG_VIBRATE_FEED_BACK_START:B

    .line 76
    const/16 v0, 0xc

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->MSG_VIBRATE_FEED_BACK_END:B

    .line 77
    const/16 v0, 0xd

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->MSG_SET_BAROMETER_SENSOR_PRESSURE:B

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Accelerometer:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Barometer:Z

    .line 85
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_LUX:Z

    .line 86
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_ADC:Z

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gyroscope:Z

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic:Z

    .line 89
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic_PowerNoise:Z

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gesture:Z

    .line 91
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_TempHumid:Z

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_UV:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_OIS_Gyro:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z

    .line 96
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_HRM:Z

    .line 102
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsRGBSensor:Z

    .line 103
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsReadFromManager:Z

    .line 128
    const-string v0, "com.android.samsungtest.CpAccelermeterOn"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CP_ACCELERMETER_ON:Ljava/lang/String;

    .line 129
    const-string v0, "com.android.samsungtest.CpAccelermeterOff"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CP_ACCELERMETER_OFF:Ljava/lang/String;

    .line 236
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProduct:Ljava/lang/String;

    .line 240
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTimer:Ljava/util/Timer;

    .line 242
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximityTimer:Ljava/util/Timer;

    .line 244
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 245
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 262
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I

    .line 263
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z

    .line 267
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z

    .line 273
    iput-byte v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->WORK_SENSOR_THREAD:B

    .line 274
    iput-byte v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->WORK_HANDLER_THREAD:B

    .line 275
    iput-byte v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B

    .line 280
    const/4 v0, 0x5

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotalArray:[F

    .line 282
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureAvg:F

    .line 283
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I

    .line 285
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMHRMin:F

    .line 286
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMHRAvg:F

    .line 287
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMHRMax:F

    .line 288
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMHRTotal:F

    .line 289
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMHRCount:I

    .line 290
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSpO2Min:F

    .line 291
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSpO2Avg:F

    .line 292
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSpO2Max:F

    .line 293
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSpO2Total:F

    .line 294
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSpO2Count:I

    .line 295
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSignalQtyMin:F

    .line 296
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSignalQtyAvg:F

    .line 297
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSignalQtyMax:F

    .line 298
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSignalQtyTotal:F

    .line 299
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSignalQtyCount:I

    .line 301
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    .line 302
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    .line 303
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;

    .line 306
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;

    .line 440
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SensorTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1771
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    .line 1983
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorValue:F

    .line 1984
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SensorTest$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$6;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximityListener:Landroid/hardware/SensorEventListener;

    .line 437
    return-void

    .line 68
    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/SensorTest;)B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-byte v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/SensorTest;B)B
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # B

    .prologue
    .line 65
    iput-byte p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorStatusText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorAdcAvgText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Barometer:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/SensorTest;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotalArray:[F

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/SensorTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/hwmoduletest/SensorTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I

    return p1
.end method

.method static synthetic access$1508(Lcom/sec/android/app/hwmoduletest/SensorTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/SensorTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # F

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F

    return p1
.end method

.method static synthetic access$1716(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # F

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/SensorTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureAvg:F

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # F

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureAvg:F

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorPressureText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorValueText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProduct:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/view/SensorArrow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    return v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/hwmoduletest/SensorTest;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->FEED_BACK_PATTERN:[J

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBackground:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/hwmoduletest/SensorTest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->catchCPsAccelerometerData(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/sec/android/app/hwmoduletest/SensorTest;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    return-object v0
.end method

.method static synthetic access$5902(Lcom/sec/android/app/hwmoduletest/SensorTest;Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/app/hwmoduletest/SensorTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorValue:F

    return v0
.end method

.method static synthetic access$6402(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # F

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorValue:F

    return p1
.end method

.method static synthetic access$6500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z

    return v0
.end method

.method static synthetic access$6800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z

    return v0
.end method

.method static synthetic access$7302(Lcom/sec/android/app/hwmoduletest/SensorTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z

    return p1
.end method

.method static synthetic access$7400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z

    return v0
.end method

.method static synthetic access$7500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsRGBSensor:Z

    return v0
.end method

.method static synthetic access$7800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z

    return v0
.end method

.method static synthetic access$7802(Lcom/sec/android/app/hwmoduletest/SensorTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z

    return p1
.end method

.method static synthetic access$7900(Lcom/sec/android/app/hwmoduletest/SensorTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I

    return v0
.end method

.method static synthetic access$7902(Lcom/sec/android/app/hwmoduletest/SensorTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p1, "x1"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I

    return p1
.end method

.method static synthetic access$7908(Lcom/sec/android/app/hwmoduletest/SensorTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorValueText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private catchCPsAccelerometerData(Ljava/lang/String;)V
    .locals 14
    .param p1, "cmdData"    # Ljava/lang/String;

    .prologue
    .line 1937
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "catchCPsAccelerometerData"

    const-string v11, "TEST-SENSORTEST_____ catchCPsAccelerometerData()_____TEST-SENSORTEST"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1939
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "catchCPsAccelermeteroData"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "TEST-SENSORTEST_____ cmdData=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]_____TEST-SENSORTEST"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1942
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v10, 0x8

    const/16 v11, 0xa

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x6

    const/16 v11, 0x8

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1943
    .local v4, "xData":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v10, 0xc

    const/16 v11, 0xe

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0xa

    const/16 v11, 0xc

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1944
    .local v6, "yData":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v10, 0x10

    const/16 v11, 0x12

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0xe

    const/16 v11, 0x10

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1945
    .local v8, "zData":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "catchCPsAccelerometerData"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "TEST-SENSORTEST_____ xData=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "],yData=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "],zData=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]_____TEST-SENSORTEST"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1947
    const/16 v9, 0x10

    invoke-static {v4, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    int-to-short v3, v9

    .line 1948
    .local v3, "x":I
    const/16 v9, 0x10

    invoke-static {v6, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    int-to-short v5, v9

    .line 1949
    .local v5, "y":I
    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    int-to-short v7, v9

    .line 1950
    .local v7, "z":I
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "catchCPsAccelerometerData"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "TEST-SENSORTEST_____ x=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "],y=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "],z=["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]_____TEST-SENSORTEST"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    const-string v9, "CP ACCELER: X:%d, Y:%d, Z:%d \n"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1954
    .local v2, "tempStr":Ljava/lang/String;
    const/4 v9, 0x3

    new-array v0, v9, [I

    .line 1955
    .local v0, "angel":[I
    mul-int v9, v3, v3

    mul-int v10, v5, v5

    add-int/2addr v9, v10

    mul-int v10, v7, v7

    add-int/2addr v9, v10

    int-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v1, v10

    .line 1956
    .local v1, "realg":F
    const/4 v9, 0x0

    int-to-float v10, v3

    div-float/2addr v10, v1

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->asin(D)D

    move-result-wide v10

    double-to-float v10, v10

    const v11, 0x42652ee1

    mul-float/2addr v10, v11

    float-to-int v10, v10

    aput v10, v0, v9

    .line 1957
    const/4 v9, 0x1

    int-to-float v10, v5

    div-float/2addr v10, v1

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->asin(D)D

    move-result-wide v10

    double-to-float v10, v10

    const v11, 0x42652ee1

    mul-float/2addr v10, v11

    float-to-int v10, v10

    aput v10, v0, v9

    .line 1958
    const/4 v9, 0x2

    int-to-float v10, v7

    div-float/2addr v10, v1

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->acos(D)D

    move-result-wide v10

    double-to-float v10, v10

    const v11, 0x42652ee1

    mul-float/2addr v10, v11

    float-to-int v10, v10

    add-int/lit8 v10, v10, -0x5a

    aput v10, v0, v9

    .line 1959
    const/4 v9, 0x2

    aget v10, v0, v9

    mul-int/lit8 v10, v10, -0x1

    aput v10, v0, v9

    .line 1960
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "CP X-angle: %d, Y-angle: %d, Z-angle: %d "

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    aget v13, v0, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    aget v13, v0, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    const/4 v13, 0x2

    aget v13, v0, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1962
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorValueText:Landroid/widget/TextView;

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1963
    return-void
.end method

.method private checkValidSysfPathLightSensor()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1774
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1791
    const-string v0, "LIGHT_SENSOR_RAW"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1792
    const-string v0, "LIGHT_SENSOR_RAW"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    .line 1801
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read target : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1802
    return-void

    .line 1793
    :cond_0
    const-string v0, "LIGHT_SENSOR_LUX"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1794
    const-string v0, "LIGHT_SENSOR_LUX"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    goto :goto_0

    .line 1795
    :cond_1
    const-string v0, "LIGHT_SENSOR_ADC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1796
    const-string v0, "LIGHT_SENSOR_ADC"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    goto :goto_0

    .line 1798
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    const-string v2, "no read target"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1799
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;

    goto :goto_0
.end method

.method private controlCPsAccelerometer(I)V
    .locals 5
    .param p1, "control"    # I

    .prologue
    const/4 v4, 0x4

    .line 1909
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    if-nez v1, :cond_0

    .line 1910
    new-instance v1, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .line 1911
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->bindSecPhoneService()V

    .line 1914
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1934
    :goto_0
    return-void

    .line 1916
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "controlCPsAccelerometer"

    const-string v3, "TEST_____ controlCPsAccelerometer : CP_ACCEL_POWER_ON _____TEST"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    new-array v0, v4, [B

    fill-array-data v0, :array_0

    .line 1921
    .local v0, "controlData":[B
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->sendToRilCpAccelermeter([B)V

    goto :goto_0

    .line 1925
    .end local v0    # "controlData":[B
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "controlCPsAccelerometer"

    const-string v3, "TEST_____ controlCPsAccelerometer : CP_ACCEL_POWER_OFF _____TEST"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1927
    new-array v0, v4, [B

    fill-array-data v0, :array_1

    .line 1930
    .restart local v0    # "controlData":[B
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->sendToRilCpAccelermeter([B)V

    goto :goto_0

    .line 1914
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 1918
    :array_0
    .array-data 1
        0x2t
        0x0t
        0xct
        0x1t
    .end array-data

    .line 1927
    :array_1
    .array-data 1
        0x2t
        0x0t
        0xct
        0x0t
    .end array-data
.end method

.method private getUltrasonicSensorVerValueString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1968
    const/4 v1, 0x0

    .line 1971
    .local v1, "ultraVerData":[Ljava/lang/String;
    :try_start_0
    const-string v2, "ULTRASONIC_VER_CHECK"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1972
    const-string v2, "0"

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "0"

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1973
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1980
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " F/W  Version : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1975
    :cond_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1977
    :catch_0
    move-exception v0

    .line 1978
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readToUltrasonicSensor"

    const-string v4, "Exception accessing ultrasonic file"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initialize()V
    .locals 15

    .prologue
    .line 936
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getSensorTest()[Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    invoke-direct {v5, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 939
    .local v5, "menuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v0, v11, :cond_13

    .line 940
    add-int/lit8 v11, v0, 0x3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lt v11, v12, :cond_11

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    :goto_1
    invoke-virtual {v5, v0, v11}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 943
    .local v4, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 944
    .local v3, "item":Ljava/lang/String;
    const-string v11, ","

    invoke-virtual {v3, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v2, v11, v12

    .line 945
    .local v2, "id":Ljava/lang/String;
    const-string v11, ","

    invoke-virtual {v3, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v8, v11, v12

    .line 947
    .local v8, "text":Ljava/lang/String;
    const-string v11, "Accelerometer"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 948
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Accelerometer:Z

    .line 951
    :cond_1
    const-string v11, "CpAccelerometer"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 952
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    .line 955
    :cond_2
    const-string v11, "Proximity_state"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 956
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    .line 959
    :cond_3
    const-string v11, "Proximity_ADC"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 960
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z

    .line 963
    :cond_4
    const-string v11, "Barometer"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 964
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Barometer:Z

    .line 967
    :cond_5
    const-string v11, "Light_LUX"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 968
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_LUX:Z

    .line 971
    :cond_6
    const-string v11, "Light_ADC"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 972
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_ADC:Z

    .line 975
    :cond_7
    const-string v11, "Gyroscope"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 976
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gyroscope:Z

    .line 979
    :cond_8
    const-string v11, "OIS_Gyro"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 980
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_OIS_Gyro:Z

    .line 983
    :cond_9
    const-string v11, "Magnetic"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 984
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic:Z

    .line 987
    :cond_a
    const-string v11, "Magnetic_PowerNoise"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 988
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic_PowerNoise:Z

    .line 991
    :cond_b
    const-string v11, "Gesture"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 992
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gesture:Z

    .line 995
    :cond_c
    const-string v11, "Temp_Humid"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 996
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_TempHumid:Z

    .line 999
    :cond_d
    const-string v11, "UV"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 1000
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_UV:Z

    .line 1003
    :cond_e
    const-string v11, "Ultrasonic"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 1004
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z

    .line 1007
    :cond_f
    const-string v11, "HRM"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 1008
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_HRM:Z

    .line 1011
    :cond_10
    const-string v11, "Fingerprint"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1012
    const/4 v11, 0x1

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->setEnable(Z)V

    goto/16 :goto_2

    .line 940
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "item":Ljava/lang/String;
    .end local v4    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "text":Ljava/lang/String;
    :cond_11
    add-int/lit8 v11, v0, 0x3

    goto/16 :goto_1

    .line 939
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_12
    add-int/lit8 v0, v0, 0x3

    goto/16 :goto_0

    .line 1018
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_13
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "initialize"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[Accelerometer=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Accelerometer:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[CpAccelerometer=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Proximity_state=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Proximity_ADC=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Gesture=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gesture:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Temp_Humid=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_TempHumid:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Barometer=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Barometer:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Light_LUX=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_LUX:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Light_ADC=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_ADC:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Gyroscope=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gyroscope:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Magnetic=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] , "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Magnetic_PowerNoise=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic_PowerNoise:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Gesture=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gesture:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Temp_Humid=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_TempHumid:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[UV=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_UV:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[Ultrasonic=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[HRM=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_HRM:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "],"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "[OIS=>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v14, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_OIS_Gyro:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->checkValidSysfPathLightSensor()V

    .line 1032
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->isRgbSensorSupported()Z

    move-result v11

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsRGBSensor:Z

    .line 1035
    const v11, 0x7f0b000c

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBackground:Landroid/widget/LinearLayout;

    .line 1037
    const v11, 0x7f0b01cc

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTitleText:Landroid/widget/TextView;

    .line 1038
    const v11, 0x7f0b01cd

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubSeparator:Landroid/view/View;

    .line 1039
    const v11, 0x7f0b01ce

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubText:Landroid/widget/TextView;

    .line 1040
    const v11, 0x7f0b01cf

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTestButton:Landroid/widget/Button;

    .line 1042
    const v11, 0x7f0b01d0

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorTitleText:Landroid/widget/TextView;

    .line 1043
    const v11, 0x7f0b01d1

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSeparator:Landroid/view/View;

    .line 1044
    const v11, 0x7f0b01d2

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorValueText:Landroid/widget/TextView;

    .line 1045
    const v11, 0x7f0b01d5

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    .line 1046
    const v11, 0x7f0b01d3

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceImageTestButton:Landroid/widget/Button;

    .line 1047
    const v11, 0x7f0b01d4

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceGraphButton:Landroid/widget/Button;

    .line 1050
    const v11, 0x7f0b01d6

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorTitleText:Landroid/widget/TextView;

    .line 1051
    const v11, 0x7f0b01d7

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSeparator:Landroid/view/View;

    .line 1052
    const v11, 0x7f0b01d8

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorValueText:Landroid/widget/TextView;

    .line 1056
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Accelerometer:Z

    if-nez v11, :cond_14

    .line 1057
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1058
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1059
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1060
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1061
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceImageTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1062
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceGraphButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1066
    :cond_14
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    if-nez v11, :cond_15

    .line 1067
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1068
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1069
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1074
    :cond_15
    const v11, 0x7f0b01d9

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTitleText:Landroid/widget/TextView;

    .line 1075
    const v11, 0x7f0b01da

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSeparator:Landroid/view/View;

    .line 1076
    const v11, 0x7f0b01db

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorStatusText:Landroid/widget/TextView;

    .line 1077
    const v11, 0x7f0b01dc

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorAdcAvgText:Landroid/widget/TextView;

    .line 1078
    const v11, 0x7f0b01dd

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    .line 1079
    const v11, 0x7f0b01de

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorDefaultTrimText:Landroid/widget/TextView;

    .line 1080
    const v11, 0x7f0b01df

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    .line 1081
    const v11, 0x7f0b01e0

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorOffsetText:Landroid/widget/TextView;

    .line 1082
    const v11, 0x7f0b01e1

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorHighThresholdText:Landroid/widget/TextView;

    .line 1083
    const v11, 0x7f0b01e2

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorLowThresholdText:Landroid/widget/TextView;

    .line 1087
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    if-nez v11, :cond_16

    .line 1088
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1089
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1090
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorStatusText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1091
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorDefaultTrimText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1094
    :cond_16
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z

    if-nez v11, :cond_17

    .line 1095
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorAdcAvgText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1096
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1097
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1098
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorOffsetText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1099
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorHighThresholdText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1100
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorLowThresholdText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1103
    :cond_17
    const-string v11, "WINDOW_TYPE"

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2d

    .line 1104
    const-string v10, ""

    .line 1106
    .local v10, "windowType":Ljava/lang/String;
    const-string v11, "WINDOW_TYPE"

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x2

    if-ge v11, v12, :cond_28

    .line 1107
    const-string v10, "NA"

    .line 1112
    :goto_3
    const-string v11, "a2"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_29

    .line 1113
    const-string v10, "NA"

    .line 1128
    :goto_4
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "TSP color ID : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1139
    .end local v10    # "windowType":Ljava/lang/String;
    :goto_5
    const v11, 0x7f0b0209

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorTitleText:Landroid/widget/TextView;

    .line 1140
    const v11, 0x7f0b020a

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSeparator:Landroid/view/View;

    .line 1141
    const v11, 0x7f0b020b

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    .line 1142
    const v11, 0x7f0b020c

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    .line 1153
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gesture:Z

    if-nez v11, :cond_18

    .line 1154
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1155
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1156
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1157
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1170
    :cond_18
    const v11, 0x7f0b020d

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidTitleText:Landroid/widget/TextView;

    .line 1171
    const v11, 0x7f0b020e

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidSeparator:Landroid/view/View;

    .line 1172
    const v11, 0x7f0b0213

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    .line 1173
    const v11, 0x7f0b0214

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidDisplayButton:Landroid/widget/Button;

    .line 1174
    const v11, 0x7f0b0215

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidGraphButton:Landroid/widget/Button;

    .line 1175
    const v11, 0x7f0b0216

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidThermistorButton:Landroid/widget/Button;

    .line 1176
    const v11, 0x7f0b0217

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidEngineText:Landroid/widget/TextView;

    .line 1177
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidEngineText:Landroid/widget/TextView;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Engine Version : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "TEMP_HUMID_ENGINE_VER"

    invoke-static {v13}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1178
    const v11, 0x7f0b020f

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw:Landroid/widget/TextView;

    .line 1179
    const v11, 0x7f0b0210

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw:Landroid/widget/TextView;

    .line 1180
    const v11, 0x7f0b0211

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw_sleep:Landroid/widget/TextView;

    .line 1181
    const v11, 0x7f0b0212

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw_sleep:Landroid/widget/TextView;

    .line 1183
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_TempHumid:Z

    if-nez v11, :cond_19

    .line 1184
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1185
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1186
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1187
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidDisplayButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1188
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidGraphButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1189
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidThermistorButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1190
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidEngineText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1191
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1192
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1193
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw_sleep:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1194
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw_sleep:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1198
    :cond_19
    const v11, 0x7f0b0218

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorTitleText:Landroid/widget/TextView;

    .line 1199
    const v11, 0x7f0b0219

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSeparator:Landroid/view/View;

    .line 1200
    const v11, 0x7f0b021a

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    .line 1201
    const v11, 0x7f0b021b

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    .line 1202
    const v11, 0x7f0b021c

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    .line 1203
    const v11, 0x7f0b021d

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    .line 1204
    const v11, 0x7f0b021e

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryEOLText:Landroid/widget/TextView;

    .line 1205
    const v11, 0x7f0b021f

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryELFText:Landroid/widget/TextView;

    .line 1207
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_HRM:Z

    if-nez v11, :cond_1a

    .line 1208
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1209
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1210
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1211
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1212
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1213
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1214
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryEOLText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1215
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryELFText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1218
    :cond_1a
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_HRM:Z

    const/4 v12, 0x1

    if-ne v11, v12, :cond_1c

    .line 1219
    const-string v11, "HRM"

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1220
    .local v7, "testcase":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "displayValue"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "testcase = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    const-string v11, "EOL_DISABLE"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1b

    .line 1222
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1225
    :cond_1b
    const-string v11, "HRM_VENDOR"

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    .line 1226
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    if-eqz v11, :cond_1c

    .line 1227
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    const-string v12, "MAXIM"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2e

    .line 1228
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1229
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1238
    .end local v7    # "testcase":Ljava/lang/String;
    :cond_1c
    :goto_6
    const v11, 0x7f0b0221

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    .line 1239
    const v11, 0x7f0b0222

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVSeparator:Landroid/view/View;

    .line 1240
    const v11, 0x7f0b0226

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVLibraryText:Landroid/widget/TextView;

    .line 1241
    const v11, 0x7f0b0223

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVStartButton:Landroid/widget/Button;

    .line 1242
    const v11, 0x7f0b0224

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVDisplayButton:Landroid/widget/Button;

    .line 1243
    const v11, 0x7f0b0225

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVICcheckButton:Landroid/widget/Button;

    .line 1244
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_UV:Z

    if-nez v11, :cond_1d

    .line 1245
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1246
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1247
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVLibraryText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1248
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVStartButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1249
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVDisplayButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1250
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVICcheckButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1253
    :cond_1d
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1254
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1255
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1256
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidDisplayButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1257
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidGraphButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1258
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidThermistorButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1259
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1260
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1261
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVStartButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1262
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVDisplayButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1263
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVICcheckButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1265
    const v11, 0x7f0b01e3

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorTitleText:Landroid/widget/TextView;

    .line 1266
    const v11, 0x7f0b01e4

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSeparator:Landroid/view/View;

    .line 1267
    const v11, 0x7f0b01e5

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    .line 1268
    const v11, 0x7f0b01e8

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorPressureText:Landroid/widget/TextView;

    .line 1269
    const v11, 0x7f0b01ea

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeText:Landroid/widget/TextView;

    .line 1270
    const v11, 0x7f0b01eb

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    .line 1271
    const v11, 0x7f0b01e9

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSelftestButton:Landroid/widget/Button;

    .line 1272
    const v11, 0x7f0b01e6

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    .line 1273
    const v11, 0x7f0b01e7

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

    .line 1274
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

    new-instance v12, Lcom/sec/android/app/hwmoduletest/SensorTest$5;

    invoke-direct {v12, p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$5;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1292
    const-string v11, "factory"

    const-string v12, "BINARY_TYPE"

    invoke-static {v12}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1e

    .line 1293
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "initialize"

    const-string v13, "IS_FACTORY_BINARY = user"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1295
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1296
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1300
    :cond_1e
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Barometer:Z

    if-nez v11, :cond_1f

    .line 1301
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1302
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1303
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1304
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorPressureText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1305
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1306
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1307
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSelftestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1308
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1309
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1313
    :cond_1f
    const v11, 0x7f0b01ec

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTitleText:Landroid/widget/TextView;

    .line 1314
    const v11, 0x7f0b01ed

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSeparator:Landroid/view/View;

    .line 1315
    const v11, 0x7f0b01ee

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    .line 1316
    const v11, 0x7f0b01ef

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    .line 1317
    const v11, 0x7f0b01f0

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTestButton:Landroid/widget/Button;

    .line 1320
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_LUX:Z

    if-nez v11, :cond_2f

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_ADC:Z

    if-nez v11, :cond_2f

    .line 1321
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1322
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1323
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1324
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1325
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1334
    :cond_20
    :goto_7
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->isReadFromManager()Z

    move-result v11

    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsReadFromManager:Z

    .line 1337
    const v11, 0x7f0b01f1

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorTitleText:Landroid/widget/TextView;

    .line 1338
    const v11, 0x7f0b01f2

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSeparator:Landroid/view/View;

    .line 1339
    const v11, 0x7f0b01f3

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorValueText:Landroid/widget/TextView;

    .line 1340
    const v11, 0x7f0b01f4

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    .line 1341
    const v11, 0x7f0b01f5

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSelftestButton:Landroid/widget/Button;

    .line 1342
    const v11, 0x7f0b01f6

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorDisplayButton:Landroid/widget/Button;

    .line 1343
    const v11, 0x7f0b01f7

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorGraphButton:Landroid/widget/Button;

    .line 1346
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Gyroscope:Z

    if-nez v11, :cond_21

    .line 1347
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1348
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1349
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1350
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1351
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSelftestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1352
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorDisplayButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1353
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorGraphButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1356
    :cond_21
    const v11, 0x7f0b01f8

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSensorSub:Landroid/widget/TextView;

    .line 1357
    const v11, 0x7f0b01f9

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSelftestButton:Landroid/widget/Button;

    .line 1359
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_OIS_Gyro:Z

    if-nez v11, :cond_22

    .line 1360
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSensorSub:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1361
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSelftestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1365
    :cond_22
    const v11, 0x7f0b01fe

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorTitleText:Landroid/widget/TextView;

    .line 1366
    const v11, 0x7f0b01ff

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSeparator:Landroid/view/View;

    .line 1367
    const v11, 0x7f0b0200

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;

    .line 1368
    const v11, 0x7f0b0201

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;

    .line 1369
    const v11, 0x7f0b0202

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    .line 1370
    const v11, 0x7f0b0207

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    .line 1371
    const v11, 0x7f0b0206

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSelfTestButton:Landroid/widget/Button;

    .line 1374
    const v11, 0x7f0b0203

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;

    .line 1375
    const v11, 0x7f0b0204

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;

    .line 1376
    const v11, 0x7f0b0205

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;

    .line 1378
    const v11, 0x7f0b0208

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    .line 1379
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    const/high16 v12, 0x42340000    # 45.0f

    invoke-virtual {v11, v12}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setDirection(F)V

    .line 1382
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic:Z

    if-nez v11, :cond_23

    .line 1383
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1384
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1385
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1386
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1387
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1388
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1389
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSelfTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1390
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1391
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1392
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1393
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setVisibility(I)V

    .line 1397
    :cond_23
    const v11, 0x7f0b01fa

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorTitleText:Landroid/widget/TextView;

    .line 1398
    const v11, 0x7f0b01fb

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSeparator:Landroid/view/View;

    .line 1399
    const v11, 0x7f0b01fc

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorCompanyFWValueText:Landroid/widget/TextView;

    .line 1400
    const v11, 0x7f0b01fd

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;

    .line 1402
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z

    if-nez v11, :cond_24

    .line 1403
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1404
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1405
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorCompanyFWValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1406
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1409
    :cond_24
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Magnetic_PowerNoise:Z

    if-nez v11, :cond_25

    .line 1410
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1414
    :cond_25
    new-instance v11, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;Lcom/sec/android/app/hwmoduletest/SensorTest$1;)V

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .line 1415
    const-string v11, "vibrator"

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/SystemVibrator;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mVibrator:Landroid/os/SystemVibrator;

    .line 1416
    const-string v11, "sensor"

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/SensorManager;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 1418
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    if-eqz v11, :cond_26

    .line 1419
    const-string v11, "sensorhub"

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sensorhub/SensorHubManager;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 1420
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 1423
    :cond_26
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 1424
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximitySensor:Landroid/hardware/Sensor;

    .line 1425
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v12, 0x6

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 1426
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensor:Landroid/hardware/Sensor;

    .line 1427
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 1428
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 1429
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrientationSensor:Landroid/hardware/Sensor;

    .line 1430
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v12, 0xd

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempSensor:Landroid/hardware/Sensor;

    .line 1431
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v12, 0xc

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumidSensor:Landroid/hardware/Sensor;

    .line 1432
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v12, 0x10019

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBIOSensor:Landroid/hardware/Sensor;

    .line 1433
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v12, 0x1001a

    invoke-virtual {v11, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensor:Landroid/hardware/Sensor;

    .line 1436
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceImageTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1437
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceGraphButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1439
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1440
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setClickable(Z)V

    .line 1441
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSelftestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1442
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1443
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSelftestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1444
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorDisplayButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1445
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorGraphButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1446
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSelftestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1447
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1448
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSelfTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1449
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1450
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1451
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1452
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1453
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1456
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    if-eqz v11, :cond_31

    .line 1457
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTestButton:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1458
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceImageTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1459
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceGraphButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1460
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1461
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1462
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1463
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1464
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1465
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1466
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1467
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1468
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorDisplayButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1469
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorGraphButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1470
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1471
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1472
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1473
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1474
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1475
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setVisibility(I)V

    .line 1476
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1477
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1478
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1479
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidDisplayButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1480
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidGraphButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1481
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidThermistorButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1482
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1483
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1484
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1485
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1486
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1487
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1488
    const/4 v11, 0x0

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->setEnable(Z)V

    .line 1489
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [I

    const/4 v13, 0x0

    sget v14, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    aput v14, v12, v13

    invoke-virtual {v11, v12}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 1492
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1493
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1494
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVLibraryText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1495
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVStartButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1496
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVDisplayButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1497
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVICcheckButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1525
    :goto_8
    const/16 v11, 0x9

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getUIRate(I)F

    move-result v6

    .line 1527
    .local v6, "rate":F
    const/4 v11, 0x0

    cmpl-float v11, v6, v11

    if-eqz v11, :cond_27

    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v11, v6, v11

    if-eqz v11, :cond_27

    .line 1529
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1531
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1533
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1536
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1538
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1540
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1542
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceImageTestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceImageTestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1544
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceGraphButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceGraphButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1547
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1549
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCpAcceSensorValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1552
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1554
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorStatusText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorStatusText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1556
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorAdcAvgText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorAdcAvgText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1558
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1560
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorDefaultTrimText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1562
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1564
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorOffsetText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1566
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorHighThresholdText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1568
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorLowThresholdText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1572
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1574
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorCrosstalkButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1576
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGestureSensorDirectionButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1579
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1581
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1583
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidDisplayButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidDisplayButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1585
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidGraphButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidGraphButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1587
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidThermistorButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempHumidThermistorButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1590
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1592
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVLibraryText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1594
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVStartButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1596
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVDisplayButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVDisplayButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1598
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVICcheckButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVICcheckButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1601
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1603
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorStartButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1605
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorEOLTestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1607
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1609
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1611
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryEOLText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryEOLText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1613
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryELFText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryELFText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1617
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1619
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1621
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorPressureText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorPressureText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1623
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1625
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1627
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1629
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSelftestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSelftestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1632
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1634
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1636
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1638
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorTestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1641
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1643
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1645
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1647
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSelftestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSelftestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1649
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorDisplayButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorDisplayButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1651
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorGraphButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorGraphButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1654
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorTitleText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1656
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1658
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1660
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1662
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorPowerNoiseTestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1664
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSelfTestButton:Landroid/widget/Button;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSelfTestButton:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1667
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1669
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1671
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1675
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorCompanyFWValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorCompanyFWValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1677
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getTextSize()F

    move-result v13

    mul-float/2addr v13, v6

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1680
    :cond_27
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->setFragment(Landroid/app/Activity;)V

    .line 1681
    return-void

    .line 1109
    .end local v6    # "rate":F
    .restart local v10    # "windowType":Ljava/lang/String;
    :cond_28
    const-string v11, "WINDOW_TYPE"

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_3

    .line 1115
    :cond_29
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    and-int/lit8 v9, v11, 0x3

    .line 1117
    .local v9, "type":I
    if-nez v9, :cond_2a

    .line 1118
    const-string v10, "00"

    goto/16 :goto_4

    .line 1119
    :cond_2a
    const/4 v11, 0x1

    if-ne v9, v11, :cond_2b

    .line 1120
    const-string v10, "01"

    goto/16 :goto_4

    .line 1121
    :cond_2b
    const/4 v11, 0x2

    if-ne v9, v11, :cond_2c

    .line 1122
    const-string v10, "10"

    goto/16 :goto_4

    .line 1124
    :cond_2c
    const-string v10, "NA"

    goto/16 :goto_4

    .line 1130
    .end local v9    # "type":I
    .end local v10    # "windowType":Ljava/lang/String;
    :cond_2d
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorTspColorIdText:Landroid/widget/TextView;

    const-string v12, "TSP color ID : NA"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1230
    .restart local v7    # "testcase":Ljava/lang/String;
    :cond_2e
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    const-string v12, "ADI"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1c

    .line 1231
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryEOLText:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1232
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryELFText:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 1326
    .end local v7    # "testcase":Ljava/lang/String;
    :cond_2f
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_LUX:Z

    if-nez v11, :cond_30

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_ADC:Z

    if-eqz v11, :cond_30

    .line 1327
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1328
    :cond_30
    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_LUX:Z

    if-eqz v11, :cond_20

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Light_ADC:Z

    if-nez v11, :cond_20

    .line 1329
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1499
    :cond_31
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTitleText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1500
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubSeparator:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1501
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1502
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubTestButton:Landroid/widget/Button;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1503
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1504
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1505
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1506
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1507
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1508
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1518
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1519
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1520
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw_sleep:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1521
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw_sleep:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8
.end method

.method private isReadFromManager()Z
    .locals 4

    .prologue
    .line 921
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "isReadFromManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IS_DISPLAY_LUX_ADC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "IS_DISPLAY_LUX_ADC"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    const-string v0, "IS_DISPLAY_LUX_ADC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    const/4 v0, 0x1

    .line 931
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRgbSensorSupported()Z
    .locals 6

    .prologue
    .line 1751
    const/4 v3, 0x1

    .line 1752
    .local v3, "isrgbsensor":Z
    const-string v4, "HW_REVISION"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1754
    .local v2, "hwrevision":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1755
    const-string v4, "NO_RGBSENSOR_SUPPORT_REV"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1758
    .local v1, "hwrevString":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 1759
    const-string v4, "-1"

    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1761
    :cond_0
    const/4 v3, 0x0

    .line 1767
    .end local v0    # "count":I
    .end local v1    # "hwrevString":[Ljava/lang/String;
    :cond_1
    return v3

    .line 1758
    .restart local v0    # "count":I
    .restart local v1    # "hwrevString":[Ljava/lang/String;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private readToOisGyroSensor()V
    .locals 6

    .prologue
    .line 2005
    :try_start_0
    const-string v3, "GYRO_OIS_RAWDATA"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2007
    .local v1, "RawdataResult":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2008
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2010
    .local v0, "RawData":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSensorSub:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OIS GYRO : X: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Y: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2018
    .end local v0    # "RawData":[Ljava/lang/String;
    .end local v1    # "RawdataResult":Ljava/lang/String;
    :goto_0
    return-void

    .line 2012
    .restart local v1    # "RawdataResult":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOISGyroSensorSub:Landroid/widget/TextView;

    const-string v4, "OIS GYRO : X: null Y: null"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2014
    .end local v1    # "RawdataResult":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 2015
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readToOisGyroSensor"

    const-string v5, "ArrayIndexOutOfBoundsException - OIS Gyro Sensor"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private registerCpAccelermeterReceiver()V
    .locals 2

    .prologue
    .line 1903
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1904
    .local v0, "CpAccelermeterData":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1905
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1906
    return-void
.end method

.method private startGyroscopeSelfTest()V
    .locals 9

    .prologue
    .line 1858
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v4

    iget-object v1, v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    .line 1859
    .local v1, "feature":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "startGyroscopeSelfTest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Start Gyroscope Sensor - feature : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1863
    const-string v4, "INVENSENSE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "INVENSENSE_MPU6050"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "INVENSENSE_MPU6051"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "INVENSENSE_MPU6500"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "INVENSENSE_MPU6051M"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "INVENSENSE_MPU6515M"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "INVENSENSE_MPU6515"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1870
    :cond_0
    const-class v3, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;

    .line 1871
    .local v3, "intentClass":Ljava/lang/Class;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1872
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    .line 1897
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "intentClass":Ljava/lang/Class;
    :cond_1
    :goto_0
    return-void

    .line 1873
    :cond_2
    const-string v4, "STMICRO_SMARTPHONE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1874
    const-class v3, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;

    .line 1875
    .restart local v3    # "intentClass":Ljava/lang/Class;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1876
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1877
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "intentClass":Ljava/lang/Class;
    :cond_3
    const-string v4, "STMICRO_TABLET"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1878
    const-class v3, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;

    .line 1879
    .restart local v3    # "intentClass":Ljava/lang/Class;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1880
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1881
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "intentClass":Ljava/lang/Class;
    :cond_4
    const-string v4, "BOSCH"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1882
    const-class v3, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    .line 1883
    .restart local v3    # "intentClass":Ljava/lang/Class;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1885
    .restart local v2    # "intent":Landroid/content/Intent;
    :try_start_0
    const-string v4, "subtype"

    const-string v5, "_"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1886
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "GyroscopeBOSCH"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sub type : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1890
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1887
    :catch_0
    move-exception v0

    .line 1888
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "GyroscopeBOSCH"

    const-string v6, "cannot parse sub type"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1890
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    throw v4

    .line 1892
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "intentClass":Ljava/lang/Class;
    :cond_5
    const-string v4, "MAXIM"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1893
    const-class v3, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;

    .line 1894
    .restart local v3    # "intentClass":Ljava/lang/Class;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1895
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private startMagneticSelfTest()V
    .locals 7

    .prologue
    .line 1808
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v3

    iget-object v0, v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    .line 1809
    .local v0, "feature":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "startMagnetic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start Magnetic Sensor - feature : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1812
    const-string v3, "AK8963"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "AK8963C"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "AK8973"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "AK8975"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1826
    :cond_0
    const-class v2, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;

    .line 1827
    .local v2, "intentClass":Ljava/lang/Class;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1828
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    .line 1852
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "intentClass":Ljava/lang/Class;
    :cond_1
    :goto_0
    return-void

    .line 1829
    :cond_2
    const-string v3, "YAS529"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "YAS530"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "YAS530C"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1832
    :cond_3
    const-class v2, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;

    .line 1833
    .restart local v2    # "intentClass":Ljava/lang/Class;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1834
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1835
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "intentClass":Ljava/lang/Class;
    :cond_4
    const-string v3, "HSCDTD004"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "HSCDTD004A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "HSCDTD006A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1838
    :cond_5
    const-class v2, Lcom/sec/android/app/hwmoduletest/MagneticAlps;

    .line 1839
    .restart local v2    # "intentClass":Ljava/lang/Class;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1840
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1841
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "intentClass":Ljava/lang/Class;
    :cond_6
    const-string v3, "STMICRO_K303C"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1842
    const-class v2, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;

    .line 1843
    .restart local v2    # "intentClass":Ljava/lang/Class;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1844
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1845
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "intentClass":Ljava/lang/Class;
    :cond_7
    const-string v3, "BOSCH_BMC150"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "BMC150_COMBINATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1848
    :cond_8
    const-class v2, Lcom/sec/android/app/hwmoduletest/MagneticBosch;

    .line 1849
    .restart local v2    # "intentClass":Ljava/lang/Class;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1850
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 28
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 648
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    sparse-switch v19, :sswitch_data_0

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 655
    :sswitch_0
    const/4 v6, 0x0

    .line 656
    .local v6, "baroaltitudeValue":F
    const-string v19, "BAROMETE_VENDOR"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 658
    .local v18, "vendor":Ljava/lang/String;
    const-string v19, "SENSOR_NAME_GYROSCOPE"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, "INVENSENSE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    const-string v19, "SENSOR_NAME_GYROSCOPE"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, "BOSCH"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 660
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "gyroConvert"

    const-string v21, "gyroConvert=INVENSENSE || BOSCH"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    const/high16 v9, 0x3c7a0000

    .line 665
    .local v9, "gyroConvert":F
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startSensrohubSleepTest()V

    .line 666
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/WakeUpService;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 667
    .local v10, "intent":Landroid/content/Intent;
    const-string v19, "com.sec.factory.WakeUp"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 668
    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v10, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v17

    .line 669
    .local v17, "sender":Landroid/app/PendingIntent;
    const-string v19, "alarm"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/AlarmManager;

    .line 670
    .local v5, "am":Landroid/app/AlarmManager;
    const/16 v19, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/16 v22, 0x1388

    add-long v20, v20, v22

    move/from16 v0, v19

    move-wide/from16 v1, v20

    move-object/from16 v3, v17

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 671
    const-string v19, "power"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/PowerManager;

    .line 672
    .local v13, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 675
    const-wide/16 v20, 0x1388

    :try_start_0
    invoke-static/range {v20 .. v21}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 680
    :goto_2
    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->readSensrohubSleepTest()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v16

    .line 686
    .local v16, "returnValue":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "sensorTest"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "barometer vendor="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 689
    .local v15, "result":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorSleepValueText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sleep Raw Data - x: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x0

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", y: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x1

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", z:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x2

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorSleepAdcAvgText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sleep ADC: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0xb

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 695
    const-string v19, "STM"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 696
    const/16 v19, 0x9

    aget-object v19, v15, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v19

    const/high16 v20, 0x45800000    # 4096.0f

    div-float v6, v19, v20

    .line 701
    :goto_4
    const-wide v20, 0x40e5a54000000000L    # 44330.0

    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    const v19, 0x447d5000    # 1013.25f

    div-float v19, v6, v19

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide v26, 0x3fc85b95c0000000L    # 0.19029495120048523

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    sub-double v22, v22, v24

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v4, v0

    .line 703
    .local v4, "altitude":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeSleepText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const-string v20, "Sleep Value: %.2f hPa, %.2f m"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 708
    const-string v19, "GYRO_SENSOR_NAME"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v12

    .line 709
    .local v12, "name_gyro":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "gyroConvert"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "gyroConvert="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const-string v19, "LSM330DLC"

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    const-string v19, "K330"

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 711
    :cond_2
    const v9, 0x3c8f5c29    # 0.0175f

    .line 716
    :cond_3
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorSleepValueText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const-string v20, "Sleep Value: Y: %.2f, P: %.2f, R: %.2f"

    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x3

    aget-object v23, v15, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v23

    mul-float v23, v23, v9

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x4

    aget-object v23, v15, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v23

    mul-float v23, v23, v9

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const/16 v23, 0x5

    aget-object v23, v15, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v23

    mul-float v23, v23, v9

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorSleepValueText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sleep Value: x: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x6

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", y: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x7

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", z: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x8

    aget-object v21, v15, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw_sleep:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw_sleep:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f070005

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(I)V

    .line 738
    const-string v19, "LIGHT_SENSOR_NAME"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    const-string v20, "MAX88921"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_4

    const-string v19, "LIGHT_SENSOR_NAME"

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    const-string v20, "TMG399X"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 740
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw_sleep:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "TEMP_RAW : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;

    move-object/from16 v21, v0

    const/16 v22, 0x16

    aget-object v22, v15, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v22

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "(Sleep value)"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw_sleep:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "HUMI_RAW : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;

    move-object/from16 v21, v0

    const/16 v22, 0x17

    aget-object v22, v15, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v22

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "(Sleep value)"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 663
    .end local v4    # "altitude":F
    .end local v5    # "am":Landroid/app/AlarmManager;
    .end local v9    # "gyroConvert":F
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v12    # "name_gyro":Ljava/lang/String;
    .end local v13    # "pm":Landroid/os/PowerManager;
    .end local v15    # "result":[Ljava/lang/String;
    .end local v16    # "returnValue":Ljava/lang/String;
    .end local v17    # "sender":Landroid/app/PendingIntent;
    :cond_5
    const v9, 0x39a00db1

    .restart local v9    # "gyroConvert":F
    goto/16 :goto_1

    .line 681
    .restart local v5    # "am":Landroid/app/AlarmManager;
    .restart local v10    # "intent":Landroid/content/Intent;
    .restart local v13    # "pm":Landroid/os/PowerManager;
    .restart local v17    # "sender":Landroid/app/PendingIntent;
    :catch_0
    move-exception v7

    .line 682
    .local v7, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "Firmware is broken - no sysfs"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const-string v16, "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"

    .restart local v16    # "returnValue":Ljava/lang/String;
    goto/16 :goto_3

    .line 698
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v15    # "result":[Ljava/lang/String;
    :cond_6
    const/16 v19, 0x9

    aget-object v19, v15, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v19

    const/high16 v20, 0x42c80000    # 100.0f

    div-float v6, v19, v20

    goto/16 :goto_4

    .line 712
    .restart local v4    # "altitude":F
    .restart local v12    # "name_gyro":Ljava/lang/String;
    :cond_7
    const-string v19, "MAX21103"

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 713
    const v9, 0x3c888889

    goto/16 :goto_5

    .line 743
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw_sleep:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "TEMP_RAW : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;

    move-object/from16 v21, v0

    const/16 v22, 0x14

    aget-object v22, v15, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v22

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "(Sleep value)"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw_sleep:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "HUMI_RAW : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;

    move-object/from16 v21, v0

    const/16 v22, 0x15

    aget-object v22, v15, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v22

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "(Sleep value)"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 750
    .end local v4    # "altitude":F
    .end local v5    # "am":Landroid/app/AlarmManager;
    .end local v6    # "baroaltitudeValue":F
    .end local v9    # "gyroConvert":F
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v12    # "name_gyro":Ljava/lang/String;
    .end local v13    # "pm":Landroid/os/PowerManager;
    .end local v15    # "result":[Ljava/lang/String;
    .end local v16    # "returnValue":Ljava/lang/String;
    .end local v17    # "sender":Landroid/app/PendingIntent;
    .end local v18    # "vendor":Ljava/lang/String;
    :sswitch_1
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/AccImageTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 751
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 755
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_2
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 756
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 765
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeEdit:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    .line 767
    .local v14, "pressureText":Ljava/lang/String;
    if-eqz v14, :cond_0

    .line 768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-object/from16 v19, v0

    invoke-static {v14}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v20

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->setBarometerSensorPressure(Ljava/lang/Double;)V
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$6100(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;Ljava/lang/Double;)V

    goto/16 :goto_0

    .line 774
    .end local v14    # "pressureText":Ljava/lang/String;
    :sswitch_4
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 775
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 779
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_5
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 780
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 784
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startGyroscopeSelfTest()V

    goto/16 :goto_0

    .line 789
    :sswitch_7
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 790
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 794
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_8
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 795
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 799
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_9
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 800
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 804
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_a
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 805
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 810
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_b
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 811
    .restart local v10    # "intent":Landroid/content/Intent;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    .line 813
    .local v8, "feature":Ljava/lang/String;
    const-string v19, "AK8963"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    const-string v19, "AK8963C"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    const-string v19, "AK8973"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    const-string v19, "AK8975"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    const-string v19, "AK8963C_MANAGER"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    const-string v19, "AK09911"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    const-string v19, "AK09911C"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 821
    :cond_9
    const-class v19, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 822
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 823
    :cond_a
    const-string v19, "YAS529"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_b

    const-string v19, "YAS530"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_b

    const-string v19, "YAS530C"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_b

    const-string v19, "YAS532B"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_b

    const-string v19, "YAS532"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 828
    :cond_b
    const-class v19, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 829
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 830
    :cond_c
    const-string v19, "HSCDTD004"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_d

    const-string v19, "HSCDTD004A"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_d

    const-string v19, "HSCDTD006A"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_d

    const-string v19, "HSCDTD008A"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 834
    :cond_d
    const-class v19, Lcom/sec/android/app/hwmoduletest/MagneticAlps;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 835
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 836
    :cond_e
    const-string v19, "BOSCH_BMC150"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_f

    const-string v19, "BOSCH_BMC150_POWER_NOISE"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_f

    const-string v19, "BMC150_COMBINATION"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_f

    const-string v19, "BMC150_NEWEST"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 840
    :cond_f
    const-class v11, Lcom/sec/android/app/hwmoduletest/MagneticBosch;

    .line 841
    .local v11, "intentClass":Ljava/lang/Class;
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-direct {v10, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 842
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 843
    .end local v11    # "intentClass":Ljava/lang/Class;
    :cond_10
    const-string v19, "STMICRO_K303C"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 844
    const-class v11, Lcom/sec/android/app/hwmoduletest/MagneticSTMicro;

    .line 845
    .restart local v11    # "intentClass":Ljava/lang/Class;
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-direct {v10, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 846
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 853
    .end local v8    # "feature":Ljava/lang/String;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v11    # "intentClass":Ljava/lang/Class;
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_gesture_crosstalk"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/GestureSensor;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 855
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 858
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_gesture_direction"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 860
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 863
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_temp_humid"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/TempHumidTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 865
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 868
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_temphumid_display"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 870
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 873
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_temphumid_graph"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 875
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 878
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_temphumid_thermistor"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 880
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 883
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_hrm_start"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/HrmTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 885
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 888
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_hrm_eoltest"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 890
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 893
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_uv_start"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/UVTest;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 895
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 898
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_uv_display"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/UVDisplay;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 900
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 903
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "onClick"

    const-string v21, "btn_uv_ic_check"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    new-instance v10, Landroid/content/Intent;

    const-class v19, Lcom/sec/android/app/hwmoduletest/UVICcheck;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 905
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/hwmoduletest/SensorTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 676
    .restart local v5    # "am":Landroid/app/AlarmManager;
    .restart local v6    # "baroaltitudeValue":F
    .restart local v9    # "gyroConvert":F
    .restart local v13    # "pm":Landroid/os/PowerManager;
    .restart local v17    # "sender":Landroid/app/PendingIntent;
    .restart local v18    # "vendor":Ljava/lang/String;
    :catch_1
    move-exception v19

    goto/16 :goto_2

    .line 648
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b01cf -> :sswitch_0
        0x7f0b01d3 -> :sswitch_1
        0x7f0b01d4 -> :sswitch_2
        0x7f0b01e6 -> :sswitch_3
        0x7f0b01e9 -> :sswitch_4
        0x7f0b01f0 -> :sswitch_5
        0x7f0b01f5 -> :sswitch_6
        0x7f0b01f6 -> :sswitch_7
        0x7f0b01f7 -> :sswitch_8
        0x7f0b01f9 -> :sswitch_9
        0x7f0b0206 -> :sswitch_b
        0x7f0b0207 -> :sswitch_a
        0x7f0b020b -> :sswitch_c
        0x7f0b020c -> :sswitch_d
        0x7f0b0213 -> :sswitch_e
        0x7f0b0214 -> :sswitch_f
        0x7f0b0215 -> :sswitch_10
        0x7f0b0216 -> :sswitch_11
        0x7f0b021a -> :sswitch_12
        0x7f0b021b -> :sswitch_13
        0x7f0b0223 -> :sswitch_14
        0x7f0b0224 -> :sswitch_15
        0x7f0b0225 -> :sswitch_16
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 459
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 460
    const v0, 0x7f03006f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->setContentView(I)V

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isSensorHubTest"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 464
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->initialize()V

    .line 466
    new-instance v6, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v6}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 467
    .local v6, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v0, 0x2e

    invoke-virtual {v6, v0}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 468
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    invoke-direct {v0, v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;

    .line 471
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 472
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->bindSecPhoneService()V

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 480
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 640
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximityTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 643
    return-void
.end method

.method protected onPause()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 585
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 587
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->pause()V
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$5800(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)V

    .line 589
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 590
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximityListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 591
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    if-eqz v1, :cond_1

    .line 592
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 593
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onPause"

    const-string v3, "mThreadGetData get interrupt"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 596
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    .line 597
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 601
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    if-ne v1, v4, :cond_2

    .line 603
    :try_start_0
    const-string v1, "PROXI_SENSOR_ADC_AVG"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 609
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_OIS_Gyro:Z

    if-ne v1, v4, :cond_3

    .line 610
    const-string v1, "GYRO_OIS_POWER"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 614
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    if-ne v1, v4, :cond_4

    .line 615
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->controlCPsAccelerometer(I)V

    .line 616
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 618
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/SensorTest$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$4;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 635
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v1}, Landroid/os/SystemVibrator;->cancel()V

    .line 636
    return-void

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onPause"

    const-string v3, "Exception accessing prox avg file"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const v4, 0x7f070001

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 484
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 485
    iput-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z

    .line 487
    const-string v2, "IS_PROXIMITY_TEST_MOTOR_FEEDBACK"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 488
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v2}, Landroid/os/SystemVibrator;->cancel()V

    .line 491
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z

    if-eqz v2, :cond_1

    .line 492
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/SensorTest$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$3;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    .line 506
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 509
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBackground:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->setBackGroundColor(I)V

    .line 514
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setCurrentCal(I)V

    .line 516
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mAccelerometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 517
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximityListener:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 518
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mBarometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 519
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v5, 0x3

    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroscopeSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 521
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagneticSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrientationSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mTempSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 524
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumidSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 526
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z

    if-ne v2, v6, :cond_2

    .line 528
    :try_start_0
    const-string v2, "PROXI_SENSOR_ADC_AVG"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_0
    const-string v2, "PROXI_SENSOR_DEFAULT_TRIM"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 534
    .local v0, "default_trim":Ljava/lang/String;
    if-nez v0, :cond_9

    .line 535
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorDefaultTrimText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 541
    .end local v0    # "default_trim":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_OIS_Gyro:Z

    if-ne v2, v6, :cond_3

    .line 542
    const-string v2, "GYRO_OIS_POWER"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 544
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->readToOisGyroSensor()V

    .line 547
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z

    if-eqz v2, :cond_4

    .line 548
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->showProxOffset()V

    .line 552
    :cond_4
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z

    if-ne v2, v6, :cond_5

    .line 553
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorCompanyFWValueText:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getUltrasonicSensorVerValueString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 554
    iput-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z

    .line 555
    iput v8, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I

    .line 559
    :cond_5
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_CpAccelerometer:Z

    if-ne v2, v6, :cond_6

    .line 560
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/SensorTest;->controlCPsAccelerometer(I)V

    .line 561
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->registerCpAccelermeterReceiver()V

    .line 564
    :cond_6
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_HRM:Z

    if-ne v2, v6, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 565
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    const-string v3, "MAXIM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 566
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorDeivceIdText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "HRM_DEVICE_ID"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "HRM_LIBRARY_VER"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 574
    :cond_7
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->use_UV:Z

    if-ne v2, v6, :cond_8

    .line 575
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mUVLibraryText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UV_LIBRARY_VER"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 580
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->resume()V
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$5700(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)V

    .line 581
    return-void

    .line 529
    :catch_0
    move-exception v1

    .line 530
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onResume"

    const-string v4, "Exception accessing prox avg file"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 537
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "default_trim":Ljava/lang/String;
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorDefaultTrimText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Default Trim : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 568
    .end local v0    # "default_trim":Ljava/lang/String;
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHrm_Vendor:Ljava/lang/String;

    const-string v3, "ADI"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 569
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryEOLText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "HRM_LIBRARY_EOL_VER"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mHRMSensorLibraryELFText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ELF : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "HRM_LIBRARY_ELF_VER"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public readOffset()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1738
    const/4 v0, 0x0

    .line 1739
    .local v0, "data":Ljava/lang/String;
    const-string v1, "PROXI_SENSOR_OFFSET"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1741
    if-eqz v0, :cond_0

    .line 1742
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1743
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readOffset"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Offset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 1747
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "NONE"

    goto :goto_0
.end method

.method public showProxOffset()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1727
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->readOffset()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1728
    .local v0, "OffsetValue":[Ljava/lang/String;
    array-length v1, v0

    if-le v1, v4, :cond_0

    .line 1729
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorOffsetText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1730
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorHighThresholdText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "High THD: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1731
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorLowThresholdText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Low THD: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1735
    :goto_0
    return-void

    .line 1733
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readOffset"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readOffset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->readOffset()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
