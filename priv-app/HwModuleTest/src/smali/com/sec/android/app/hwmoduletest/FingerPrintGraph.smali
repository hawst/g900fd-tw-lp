.class public Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
.super Landroid/view/View;
.source "FingerPrintGraph.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    }
.end annotation


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private GRAPH_MAX_VALUE:F

.field private GRAPH_X_SCALING:F

.field private GRAPH_Y_SCALING:F

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private SCLAING:I

.field private final TEXTSIZE_RATIO:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mCol:I

.field private mColWidth:I

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMaxDataPrimary:I

.field private mMaxDataSecondary:I

.field private mMaxSection1Stdev:F

.field private mMaxSection2Stdev:F

.field private mMaxStdevPrimary:F

.field private mMaxStdevSecondary:F

.field private mMinDataPrimary:I

.field private mMinDataSecondary:I

.field private mNumberPaint:Landroid/graphics/Paint;

.field private mPathSpecMax:Landroid/graphics/Path;

.field private mPathSpecMin:Landroid/graphics/Path;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mPathZ:Landroid/graphics/Path;

.field private mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimaryEndPixel:I

.field private mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimaryStartPixel:I

.field private mRect:Landroid/graphics/Rect;

.field private mResultFailPaint:Landroid/graphics/Paint;

.field private mResultPassPaint:Landroid/graphics/Paint;

.field private mRow:I

.field private mRowHeight:I

.field private mScreenHeight:I

.field private mScreenHeight_Max:I

.field private mScreenTitleHeight:I

.field private mScreenTitleHeight_Max:I

.field private mScreenWidth:I

.field private mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryEndPixel:I

.field private mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryStartPixel:I

.field public mSerialNumber:Ljava/lang/String;

.field private mSpecPaint:Landroid/graphics/Paint;

.field private mSpecRow:I

.field private mSpecRowHeight:I

.field private mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mSumPrimaryData:I

.field private mSumPrimaryStdev:F

.field private mSumSecondaryData:I

.field private mSumSecondaryStdev:F

.field private mSumSection1Stdev:F

.field private mSumSection2Stdev:F

.field private mTableTexts:[[Ljava/lang/String;

.field private mTextCenterPaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValuePaint:Landroid/graphics/Paint;

.field private mValueSection1Stdev:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueSection2Stdev:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueZ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;

.field private final paddingHeight:I

.field private final paddingWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0xa

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 28
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_X:I

    .line 29
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    .line 30
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    .line 31
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    .line 32
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    .line 33
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->TEXTSIZE_RATIO:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    .line 38
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->paddingWidth:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->paddingHeight:I

    .line 42
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    .line 43
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    .line 44
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    .line 45
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight:I

    .line 46
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight_Max:I

    .line 47
    const/16 v0, 0x226

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight_Max:I

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    .line 55
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    .line 56
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    .line 57
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathZ:Landroid/graphics/Path;

    .line 58
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 59
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSerialNumber:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueZ:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection1Stdev:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection2Stdev:Ljava/util/ArrayList;

    .line 73
    const-string v0, "FingerPrintGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->CLASS_NAME:Ljava/lang/String;

    .line 79
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    .line 80
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    .line 81
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    .line 84
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    .line 86
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    .line 88
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    .line 89
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    .line 90
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    .line 91
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    .line 92
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    .line 93
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    .line 94
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection1Stdev:F

    .line 95
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection2Stdev:F

    .line 124
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->init(Landroid/content/Context;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v1, 0xa

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_X:I

    .line 29
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    .line 30
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    .line 31
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    .line 32
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    .line 33
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->TEXTSIZE_RATIO:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    .line 38
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->paddingWidth:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->paddingHeight:I

    .line 42
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    .line 43
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    .line 44
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    .line 45
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight:I

    .line 46
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight_Max:I

    .line 47
    const/16 v0, 0x226

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight_Max:I

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    .line 55
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    .line 56
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    .line 57
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathZ:Landroid/graphics/Path;

    .line 58
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 59
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSerialNumber:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueZ:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection1Stdev:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection2Stdev:Ljava/util/ArrayList;

    .line 73
    const-string v0, "FingerPrintGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->CLASS_NAME:Ljava/lang/String;

    .line 79
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    .line 80
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    .line 81
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    .line 84
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    .line 86
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    .line 88
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    .line 89
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    .line 90
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    .line 91
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    .line 92
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    .line 93
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    .line 94
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection1Stdev:F

    .line 95
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection2Stdev:F

    .line 134
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->init(Landroid/content/Context;)V

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v1, 0xa

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 128
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_X:I

    .line 29
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    .line 30
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    .line 31
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    .line 32
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    .line 33
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->TEXTSIZE_RATIO:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    .line 38
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->paddingWidth:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->paddingHeight:I

    .line 42
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    .line 43
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    .line 44
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    .line 45
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight:I

    .line 46
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight_Max:I

    .line 47
    const/16 v0, 0x226

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight_Max:I

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    .line 55
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    .line 56
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    .line 57
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathZ:Landroid/graphics/Path;

    .line 58
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 59
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSerialNumber:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueZ:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection1Stdev:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection2Stdev:Ljava/util/ArrayList;

    .line 73
    const-string v0, "FingerPrintGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->CLASS_NAME:Ljava/lang/String;

    .line 79
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    .line 80
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    .line 81
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    .line 84
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    .line 86
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    .line 88
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    .line 89
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    .line 90
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    .line 91
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    .line 92
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    .line 93
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    .line 94
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection1Stdev:F

    .line 95
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection2Stdev:F

    .line 129
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->init(Landroid/content/Context;)V

    .line 130
    return-void
.end method

.method private calRawData()V
    .locals 9

    .prologue
    .line 435
    const/4 v3, 0x0

    .line 436
    .local v3, "tempX":I
    const/4 v4, 0x0

    .line 439
    .local v4, "tempY":F
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .line 440
    .local v1, "indexX":I
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v2, v5, -0x1

    .line 442
    .local v2, "indexY":I
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 443
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 452
    :goto_0
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    if-ge v3, v5, :cond_0

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    if-lt v1, v5, :cond_0

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    if-gt v1, v5, :cond_0

    .line 453
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    .line 456
    :cond_0
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    if-ge v3, v5, :cond_1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    if-lt v1, v5, :cond_1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    if-gt v1, v5, :cond_1

    .line 457
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    .line 461
    :cond_1
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    if-le v3, v5, :cond_2

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    if-lt v1, v5, :cond_2

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    if-gt v1, v5, :cond_2

    .line 462
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    .line 466
    :cond_2
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    if-le v3, v5, :cond_3

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    if-lt v1, v5, :cond_3

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    if-gt v1, v5, :cond_3

    .line 467
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    .line 471
    :cond_3
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    cmpl-float v5, v4, v5

    if-lez v5, :cond_4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    if-lt v2, v5, :cond_4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    if-gt v2, v5, :cond_4

    .line 472
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    .line 476
    :cond_4
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    cmpl-float v5, v4, v5

    if-lez v5, :cond_5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    if-lt v2, v5, :cond_5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    if-gt v2, v5, :cond_5

    .line 477
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    .line 479
    :cond_5
    return-void

    .line 445
    :catch_0
    move-exception v0

    .line 446
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "calRawData"

    const-string v7, "get value fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "calRawData"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Size of X, Y, Z : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 138
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mContext:Landroid/content/Context;

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mContext:Landroid/content/Context;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 140
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 141
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 142
    const-string v4, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 143
    iget v4, v1, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/16 v6, 0x11

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/graphics/Point;->x:I

    .line 145
    :cond_0
    iget v4, v1, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    .line 146
    iget v4, v1, Landroid/graphics/Point;->y:I

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight:I

    .line 148
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    div-int/lit8 v3, v4, 0x24

    .line 149
    .local v3, "textSize":I
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight:I

    div-int/lit8 v4, v4, 0x24

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRowHeight:I

    .line 150
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight:I

    div-int/lit8 v4, v4, 0x24

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRowHeight:I

    .line 151
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRowHeight:I

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    add-int/lit8 v5, v5, 0x2

    mul-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight:I

    .line 153
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v4, v4, -0x14

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    .line 154
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    div-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mColWidth:I

    .line 156
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight:I

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenHeight:I

    div-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    .line 157
    new-instance v4, Landroid/graphics/CornerPathEffect;

    const/high16 v5, 0x41200000    # 10.0f

    invoke-direct {v4, v5}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 158
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    const v5, -0xffff01

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 162
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 163
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    .line 164
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 165
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 166
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    const v5, -0xff0100

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 167
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 168
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mZPaint:Landroid/graphics/Paint;

    .line 169
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mZPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mZPaint:Landroid/graphics/Paint;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 171
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mZPaint:Landroid/graphics/Paint;

    const v5, -0xff0001

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 172
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mZPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 173
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    .line 174
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 175
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 176
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 177
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 178
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 180
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 183
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 185
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 187
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 188
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    .line 189
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 190
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 191
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 193
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mLinePaint:Landroid/graphics/Paint;

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mLinePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 195
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 196
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mLinePaint:Landroid/graphics/Paint;

    const v5, -0x777778

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 197
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 200
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 201
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextPaint:Landroid/graphics/Paint;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 202
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 204
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 207
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 208
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 213
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 214
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 216
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 217
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    const v5, -0xffff01

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    add-int/lit8 v5, v3, 0xa

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 219
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 220
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    .line 221
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 223
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 224
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    add-int/lit8 v5, v3, 0xa

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 225
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 227
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    float-to-int v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    mul-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    .line 228
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    float-to-int v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    mul-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    .line 232
    const-string v4, "FINGERPRINT_PRIMARY_PIXEL_START"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    .line 233
    const-string v4, "FINGERPRINT_PRIMARY_PIXEL_END"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    .line 234
    const-string v4, "FINGERPRINT_SECONDARY_PIXEL_START"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    .line 235
    const-string v4, "FINGERPRINT_SECONDARY_PIXEL_END"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    .line 236
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, "FINGERPRINT_PRIMARY_SPEC_MIN"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "FINGERPRINT_PRIMARY_SPEC_MAX"

    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 239
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, "FINGERPRINT_SECONDARY_SPEC_MIN"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "FINGERPRINT_SECONDARY_SPEC_MAX"

    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 243
    const-string v4, "FINGERPRINT_PRIMARY_SPEC_AVG"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 244
    .local v2, "temp":Ljava/lang/String;
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_1

    .line 245
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 248
    :goto_0
    const-string v4, "FINGERPRINT_SECONDARY_SPEC_AVG"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 249
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_2

    .line 250
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 253
    :goto_1
    const-string v4, "FINGERPRINT_STDEV_PRIMARY_SPEC_AVG"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 254
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_3

    .line 255
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 258
    :goto_2
    const-string v4, "FINGERPRINT_STDEV_PRIMARY_SPEC_MAX"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 259
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_4

    .line 260
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 263
    :goto_3
    const-string v4, "FINGERPRINT_STDEV_SECONDARY_SPEC_AVG"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 264
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_5

    .line 265
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 268
    :goto_4
    const-string v4, "FINGERPRINT_STDEV_SECONDARY_SPEC_MAX"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 269
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_6

    .line 270
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 273
    :goto_5
    const-string v4, "FINGERPRINT_STDEV_SECTION1_SPEC_AVG"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 274
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_7

    .line 275
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 278
    :goto_6
    const-string v4, "FINGERPRINT_STDEV_SECTION1_SPEC_MAX"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 279
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_8

    .line 280
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 283
    :goto_7
    const-string v4, "FINGERPRINT_STDEV_SECTION2_SPEC_AVG"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 284
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_9

    .line 285
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 288
    :goto_8
    const-string v4, "FINGERPRINT_STDEV_SECTION2_SPEC_MAX"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 289
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ne v4, v10, :cond_a

    .line 290
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    .line 293
    :goto_9
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->invalidate()V

    .line 294
    return-void

    .line 246
    :cond_1
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_0

    .line 251
    :cond_2
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_1

    .line 256
    :cond_3
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_2

    .line 261
    :cond_4
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_3

    .line 266
    :cond_5
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_4

    .line 271
    :cond_6
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_5

    .line 276
    :cond_7
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_6

    .line 281
    :cond_8
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_7

    .line 286
    :cond_9
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_8

    .line 291
    :cond_a
    new-instance v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    goto/16 :goto_9
.end method

.method public static isNumeric(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 397
    const-string v0, "-?\\d+(\\.\\d+)?"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addStdev1Value(F)V
    .locals 2
    .param p1, "x"    # F

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection1Stdev:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 418
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 419
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    .line 421
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection1Stdev:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection1Stdev:F

    .line 422
    return-void
.end method

.method public addStdev2Value(F)V
    .locals 2
    .param p1, "x"    # F

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection2Stdev:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 429
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    .line 431
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection2Stdev:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection2Stdev:F

    .line 432
    return-void
.end method

.method public addValue(IFI)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # F
    .param p3, "z"    # I

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->calRawData()V

    .line 412
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 298
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 300
    const/4 v6, 0x0

    .line 303
    .local v6, "baseYpx":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenTitleHeight:I

    add-int/lit8 v0, v0, -0x1e

    int-to-float v6, v0

    .line 304
    const-string v0, "Pixel :"

    const/high16 v1, 0x41200000    # 10.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 305
    const/high16 v1, 0x42f00000    # 120.0f

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v2, v6, v0

    const/high16 v3, 0x43480000    # 200.0f

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v4, v6, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 306
    const-string v0, "Stdev :"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 307
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x43020000    # 130.0f

    add-float/2addr v1, v0

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v2, v6, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    int-to-float v0, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    const/high16 v3, 0x43520000    # 210.0f

    add-float/2addr v3, v0

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v4, v6, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 310
    const/high16 v1, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v3, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 311
    const/high16 v1, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 313
    const-string v0, " 50"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 314
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x42480000    # 50.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x42480000    # 50.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 316
    const-string v0, "100"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 317
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x42c80000    # 100.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x42c80000    # 100.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 319
    const-string v0, "150"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x43160000    # 150.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 320
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x43160000    # 150.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x43160000    # 150.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 322
    const-string v0, "200"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x43480000    # 200.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 323
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x43480000    # 200.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x43480000    # 200.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 325
    const-string v0, "250"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x437a0000    # 250.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 326
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 329
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x42480000    # 50.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v3, 0x42480000    # 50.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 331
    const-string v0, " 50"

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 332
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x42c80000    # 100.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v3, 0x42c80000    # 100.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 334
    const-string v0, " 100"

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x42c80000    # 100.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 335
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x43160000    # 150.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v3, 0x43160000    # 150.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 337
    const-string v0, " 150"

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x43160000    # 150.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 338
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x43480000    # 200.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v3, 0x43480000    # 200.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 340
    const-string v0, " 200"

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x43480000    # 200.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 342
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0x64

    int-to-float v6, v0

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mYPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathZ:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 351
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0x1e

    int-to-float v6, v0

    .line 354
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    if-ge v8, v0, :cond_9

    .line 355
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    if-ge v9, v0, :cond_8

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    if-eqz v0, :cond_1

    .line 357
    add-int/lit8 v10, v9, 0x1

    .line 358
    .local v10, "k":I
    :goto_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    if-ge v10, v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v10

    if-eqz v0, :cond_2

    .line 362
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    if-ge v8, v0, :cond_3

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRowHeight:I

    mul-int/2addr v1, v8

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v1, v8, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRowHeight:I

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 370
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mColWidth:I

    mul-int/2addr v1, v9

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mColWidth:I

    mul-int/2addr v1, v10

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 375
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    if-ge v8, v0, :cond_4

    .line 376
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRowHeight:I

    .line 380
    .local v7, "height":I
    :goto_4
    const-string v0, "PASS"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v8

    aget-object v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 389
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .line 355
    .end local v7    # "height":I
    .end local v10    # "k":I
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 358
    .restart local v10    # "k":I
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 367
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    sub-int v2, v8, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v2, v8, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSpecRow:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 378
    :cond_4
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRowHeight:I

    .restart local v7    # "height":I
    goto :goto_4

    .line 382
    :cond_5
    const-string v0, "FAIL"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v8

    aget-object v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_5

    .line 384
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 387
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 354
    .end local v7    # "height":I
    .end local v10    # "k":I
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 394
    .end local v9    # "j":I
    :cond_9
    return-void
.end method

.method public setGraphXScale(F)V
    .locals 0
    .param p1, "xScale"    # F

    .prologue
    .line 674
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    .line 675
    return-void
.end method

.method public setGraphYScale(F)V
    .locals 0
    .param p1, "yScale"    # F

    .prologue
    .line 678
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    .line 679
    return-void
.end method

.method public setTextData(IILjava/lang/String;)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 401
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mRow:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mCol:I

    if-ge p2, v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, p1

    aput-object p3, v0, p2

    .line 404
    :cond_0
    return-void
.end method

.method public showGraph()V
    .locals 14

    .prologue
    .line 482
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 483
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    .line 484
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 485
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    .line 486
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    .line 487
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    .line 488
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    .line 489
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    .line 497
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 498
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 500
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    .local v0, "i":I
    :goto_0
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    if-gt v0, v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_0

    .line 502
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    int-to-float v9, v0

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 503
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    int-to-float v9, v0

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 506
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    .line 507
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    .line 500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 511
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 512
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 514
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    :goto_1
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    if-gt v0, v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_1

    .line 515
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathX:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    int-to-float v9, v0

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 516
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathY:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    int-to-float v9, v0

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 518
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    .line 519
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    .line 514
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 522
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 523
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    .line 524
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 525
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    .line 528
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 529
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 530
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 531
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 532
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 534
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 535
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 536
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 537
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 538
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 539
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMin:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0xa

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 543
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 544
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 545
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 548
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 549
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 550
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v7, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v10, v7

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v7, v11

    sub-float v7, v10, v7

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 551
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 552
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 553
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPathSpecMax:Landroid/graphics/Path;

    const/high16 v8, 0x42480000    # 50.0f

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0xa

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_X_SCALING:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->INIT_COOR_Y:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_MAX_VALUE:F

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->SCLAING:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->GRAPH_Y_SCALING:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 555
    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Pixel Section Primary : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ~ "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Secondary : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ~ "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 557
    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v9, "SPEC"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 558
    const/4 v7, 0x1

    const/4 v8, 0x2

    const-string v9, "Primary"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 559
    const/4 v7, 0x1

    const/4 v8, 0x3

    const-string v9, "Secondary"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 560
    const/4 v7, 0x1

    const/4 v8, 0x4

    const-string v9, ""

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 561
    const/4 v7, 0x1

    const/4 v8, 0x5

    const-string v9, "Primary"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 562
    const/4 v7, 0x1

    const/4 v8, 0x6

    const-string v9, "Secondary"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 564
    const/4 v7, 0x2

    const/4 v8, 0x0

    const-string v9, "Pixel"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 565
    const/4 v7, 0x2

    const/4 v8, 0x1

    const-string v9, "Min/Max"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 566
    const/4 v7, 0x2

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 567
    const/4 v7, 0x2

    const/4 v8, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 568
    const/4 v7, 0x2

    const/4 v8, 0x4

    const-string v9, "Avg"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 569
    const/4 v7, 0x2

    const/4 v8, 0x5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 570
    const/4 v7, 0x2

    const/4 v8, 0x6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 572
    const/4 v7, 0x3

    const/4 v8, 0x0

    const-string v9, "Stdev"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 573
    const/4 v7, 0x3

    const/4 v8, 0x1

    const-string v9, "V Max"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 574
    const/4 v7, 0x3

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 575
    const/4 v7, 0x3

    const/4 v8, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 576
    const/4 v7, 0x3

    const/4 v8, 0x4

    const-string v9, "V Avg"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 577
    const/4 v7, 0x3

    const/4 v8, 0x5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 578
    const/4 v7, 0x3

    const/4 v8, 0x6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 580
    const/4 v7, 0x4

    const/4 v8, 0x0

    const-string v9, "Stdev"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 581
    const/4 v7, 0x4

    const/4 v8, 0x1

    const-string v9, "S Max"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 582
    const/4 v7, 0x4

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 583
    const/4 v7, 0x4

    const/4 v8, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 584
    const/4 v7, 0x4

    const/4 v8, 0x4

    const-string v9, "S Avg"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 585
    const/4 v7, 0x4

    const/4 v8, 0x5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 586
    const/4 v7, 0x4

    const/4 v8, 0x6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v10, v10, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 588
    const/4 v7, 0x5

    const/4 v8, 0x0

    const-string v9, "S/N"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 589
    const/4 v7, 0x5

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSerialNumber:Ljava/lang/String;

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 591
    const/4 v7, 0x6

    const/4 v8, 0x0

    const-string v9, ""

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 592
    const/4 v7, 0x6

    const/4 v8, 0x4

    const-string v9, "Primary"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 593
    const/4 v7, 0x6

    const/4 v8, 0x5

    const-string v9, "Secondary"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 594
    const/4 v7, 0x6

    const/4 v8, 0x6

    const-string v9, "RESULT"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 595
    const/4 v7, 0x7

    const/4 v8, 0x0

    const-string v9, "Pixel Min"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 596
    const/4 v7, 0x7

    const/4 v8, 0x4

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 597
    const/4 v7, 0x7

    const/4 v8, 0x5

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 598
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataPrimary:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v8, v7, :cond_2

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMinDataSecondary:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v8, v7, :cond_2

    .line 599
    const/4 v7, 0x7

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 604
    :goto_2
    const/16 v7, 0x8

    const/4 v8, 0x0

    const-string v9, "Pixel Max"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 605
    const/16 v7, 0x8

    const/4 v8, 0x4

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 606
    const/16 v7, 0x8

    const/4 v8, 0x5

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 607
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataPrimary:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v8, v7, :cond_3

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxDataSecondary:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v8, v7, :cond_3

    .line 608
    const/16 v7, 0x8

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 613
    :goto_3
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryData:I

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    sub-int/2addr v8, v9

    div-int v1, v7, v8

    .line 614
    .local v1, "primary_avg":I
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryData:I

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    sub-int/2addr v8, v9

    div-int v2, v7, v8

    .line 615
    .local v2, "secondary_avg":I
    const/16 v7, 0x9

    const/4 v8, 0x0

    const-string v9, "Pixel Avg"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 616
    const/16 v7, 0x9

    const/4 v8, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 617
    const/16 v7, 0x9

    const/4 v8, 0x5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 618
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v1, v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v1, v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v2, v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 620
    const/16 v7, 0x9

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 625
    :goto_4
    const/16 v7, 0xa

    const/4 v8, 0x0

    const-string v9, "Stdev(V) Max"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 626
    const/16 v7, 0xa

    const/4 v8, 0x4

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 627
    const/16 v7, 0xa

    const/4 v8, 0x5

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 628
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v8, v7

    if-lez v7, :cond_5

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevPrimary:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v8, v7

    if-gez v7, :cond_5

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v8, v7

    if-lez v7, :cond_5

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxStdevSecondary:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryMaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v8, v7

    if-gez v7, :cond_5

    .line 630
    const/16 v7, 0xa

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 635
    :goto_5
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumPrimaryStdev:F

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryEndPixel:I

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mPrimaryStartPixel:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float v5, v7, v8

    .line 636
    .local v5, "stdev1_avg":F
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSecondaryStdev:F

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryEndPixel:I

    iget v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSecondaryStartPixel:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float v6, v7, v8

    .line 637
    .local v6, "stdev2_avg":F
    const/16 v7, 0xb

    const/4 v8, 0x0

    const-string v9, "Stdev(V) Avg"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 638
    const/16 v7, 0xb

    const/4 v8, 0x4

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 639
    const/16 v7, 0xb

    const/4 v8, 0x5

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 640
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v5, v7

    if-lez v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevPrimaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v5, v7

    if-gez v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v6, v7

    if-lez v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSecondaryAvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v6, v7

    if-gez v7, :cond_6

    .line 642
    const/16 v7, 0xb

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 647
    :goto_6
    const/16 v7, 0xc

    const/4 v8, 0x0

    const-string v9, "Stdev(S) Max"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 648
    const/16 v7, 0xc

    const/4 v8, 0x4

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 649
    const/16 v7, 0xc

    const/4 v8, 0x5

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 650
    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v8, v7

    if-lez v7, :cond_7

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection1Stdev:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v8, v7

    if-gez v7, :cond_7

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v8, v7

    if-lez v7, :cond_7

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mMaxSection2Stdev:F

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2MaxSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v8, v7

    if-gez v7, :cond_7

    .line 652
    const/16 v7, 0xc

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 658
    :goto_7
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection1Stdev:F

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection1Stdev:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-float v8, v8

    div-float v3, v7, v8

    .line 659
    .local v3, "section1_avg":F
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSumSection2Stdev:F

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mValueSection2Stdev:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-float v8, v8

    div-float v4, v7, v8

    .line 660
    .local v4, "section2_avg":F
    const/16 v7, 0xd

    const/4 v8, 0x0

    const-string v9, "Stdev(S) Avg"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 661
    const/16 v7, 0xd

    const/4 v8, 0x4

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 662
    const/16 v7, 0xd

    const/4 v8, 0x5

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "%.1f"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 663
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v3, v7

    if-lez v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection1AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v3, v7

    if-gez v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->lower:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpl-float v7, v4, v7

    if-lez v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mStdevSection2AvgSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph$Spec;->upper:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    cmpg-float v7, v4, v7

    if-gez v7, :cond_8

    .line 665
    const/16 v7, 0xd

    const/4 v8, 0x6

    const-string v9, "PASS"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    .line 670
    :goto_8
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->invalidate()V

    .line 671
    return-void

    .line 601
    .end local v1    # "primary_avg":I
    .end local v2    # "secondary_avg":I
    .end local v3    # "section1_avg":F
    .end local v4    # "section2_avg":F
    .end local v5    # "stdev1_avg":F
    .end local v6    # "stdev2_avg":F
    :cond_2
    const/4 v7, 0x7

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_2

    .line 610
    :cond_3
    const/16 v7, 0x8

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_3

    .line 622
    .restart local v1    # "primary_avg":I
    .restart local v2    # "secondary_avg":I
    :cond_4
    const/16 v7, 0x9

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_4

    .line 632
    :cond_5
    const/16 v7, 0xa

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_5

    .line 644
    .restart local v5    # "stdev1_avg":F
    .restart local v6    # "stdev2_avg":F
    :cond_6
    const/16 v7, 0xb

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_6

    .line 654
    :cond_7
    const/16 v7, 0xc

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 667
    .restart local v3    # "section1_avg":F
    .restart local v4    # "section2_avg":F
    :cond_8
    const/16 v7, 0xd

    const/4 v8, 0x6

    const-string v9, "FAIL"

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setTextData(IILjava/lang/String;)V

    goto :goto_8
.end method
