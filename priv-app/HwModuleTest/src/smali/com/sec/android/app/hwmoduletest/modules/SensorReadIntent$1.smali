.class Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent$1;
.super Landroid/content/BroadcastReceiver;
.source "SensorReadIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent$1;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    .line 191
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "action":Ljava/lang/String;
    const-string v3, "SensorReadIntent"

    const-string v4, "mBroadcastReceiver.onReceive()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v3, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 196
    const-string v3, "COMMAND"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "cmdData":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 198
    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "sensorData":Ljava/lang/String;
    const-string v3, "SensorReadIntent"

    const-string v4, "mBroadcastReceiver.onReceive()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cmdData : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , sensorData : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v3, "030005"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eq v3, v7, :cond_0

    const-string v3, "07000b"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v7, :cond_2

    .line 205
    :cond_0
    const-string v3, "SensorReadIntent"

    const-string v4, "onReceive"

    const-string v5, "Grip"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent$1;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    # invokes: Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->setValueGrip(Ljava/lang/String;)V
    invoke-static {v3, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;Ljava/lang/String;)V

    .line 226
    .end local v1    # "cmdData":Ljava/lang/String;
    .end local v2    # "sensorData":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 209
    .restart local v1    # "cmdData":Ljava/lang/String;
    .restart local v2    # "sensorData":Ljava/lang/String;
    :cond_2
    const-string v3, "070006"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v7, :cond_1

    .line 210
    const-string v3, "SensorReadIntent"

    const-string v4, "onReceive"

    const-string v5, "Accelerometer"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent$1;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    # invokes: Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->setValueCPsAccelerometerData(Ljava/lang/String;)V
    invoke-static {v3, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;Ljava/lang/String;)V

    goto :goto_0
.end method
