.class Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;
.super Landroid/os/Handler;
.source "BarometerWaterProofTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 316
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 427
    :goto_0
    return-void

    .line 318
    :pswitch_0
    iget v5, p1, Landroid/os/Message;->arg1:I

    packed-switch v5, :pswitch_data_1

    goto :goto_0

    .line 320
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    const/4 v6, 0x3

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$002(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;[F)[F

    .line 321
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v7

    aget v7, v7, v10

    aput v7, v5, v6

    goto :goto_0

    .line 324
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v7

    aget v7, v7, v10

    aput v7, v5, v6

    goto :goto_0

    .line 327
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v7

    aget v7, v7, v10

    aput v7, v5, v6

    .line 328
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    aget v5, v5, v10

    cmpl-float v5, v5, v9

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    aget v5, v5, v11

    cmpl-float v5, v5, v9

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    aget v5, v5, v12

    cmpl-float v5, v5, v9

    if-lez v5, :cond_4

    .line 329
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    aget v5, v5, v10

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v6

    aget v6, v6, v11

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v6

    aget v6, v6, v12

    add-float/2addr v5, v6

    const/high16 v6, 0x40400000    # 3.0f

    div-float v1, v5, v6

    .line 330
    .local v1, "initValue":F
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 331
    .local v0, "firstTimeStamp":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;

    const-string v5, "Ref"

    invoke-direct {v3, v10, v5, v1, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;-><init>(ILjava/lang/String;FLjava/lang/String;)V

    .line 332
    .local v3, "refItem":Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 335
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->close()Z

    .line 336
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5, v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$302(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    .line 339
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    new-instance v6, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    invoke-direct {v6, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$302(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    .line 340
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->open()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 341
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getAllData()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->write(Ljava/lang/String;)Z

    .line 344
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$400(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 345
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    invoke-static {v5, v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$402(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    .line 347
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    new-instance v6, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;)V

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    invoke-static {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$402(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    .line 348
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$400(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    move-result-object v5

    invoke-virtual {v5, v10, v1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->write(IF)Z

    .line 350
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 351
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5, v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$602(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    .line 354
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    new-instance v6, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    invoke-direct {v6, v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;)V

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$602(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    .line 355
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    invoke-virtual {v5, v1, v10}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->setValue(FI)V

    .line 357
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    sget-object v6, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$802(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 359
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$900(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;->notifyDataSetChanged()V

    .line 360
    const-string v5, "BarometerWaterProofTest"

    const-string v6, "GET_INIT_REF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CurrentStage = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/Button;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->getStageNameID()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 364
    .end local v0    # "firstTimeStamp":Ljava/lang/String;
    .end local v1    # "initValue":F
    .end local v3    # "refItem":Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
    :cond_4
    const-string v5, "BarometerWaterProofTest"

    const-string v6, "Initial Stage Errors"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 374
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    aget v4, v5, v10

    .line 375
    .local v4, "value":F
    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Read"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v5, v6, v4, v7}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;-><init>(ILjava/lang/String;FLjava/lang/String;)V

    .line 377
    .local v2, "item":Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 379
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getAllData()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->write(Ljava/lang/String;)Z

    .line 381
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$400(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 382
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$400(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->write(IF)Z

    .line 384
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$900(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;->notifyDataSetChanged()V

    .line 386
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 414
    :cond_7
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1108(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I

    goto/16 :goto_0

    .line 388
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    invoke-virtual {v5, v4, v11}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->setValue(FI)V

    .line 389
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->checkInitialLLeak()Z

    move-result v5

    if-nez v5, :cond_7

    .line 390
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/TextView;

    move-result-object v5

    const/high16 v6, -0x10000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 392
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v5

    if-nez v5, :cond_8

    .line 393
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I

    move-result v6

    rsub-int/lit8 v6, v6, 0x14

    mul-int/lit16 v6, v6, 0x3e8

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runFinishStage(I)V

    goto :goto_1

    .line 395
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runFinishStage(I)V

    goto :goto_1

    .line 400
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    invoke-virtual {v5, v4, v12}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->setValue(FI)V

    goto :goto_1

    .line 403
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->setValue(FI)V

    goto :goto_1

    .line 406
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->setValue(FI)V

    .line 407
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runFinishStage(I)V

    .line 408
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->setLeakageResult()V

    goto :goto_1

    .line 418
    .end local v2    # "item":Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
    .end local v4    # "value":F
    :pswitch_5
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/Button;

    move-result-object v5

    const v6, 0x7f08028b

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 422
    :pswitch_6
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentSensorValue:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v6

    aget v6, v6, v10

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_0
    .end packed-switch

    .line 318
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 320
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data

    .line 386
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xa -> :sswitch_1
        0xb -> :sswitch_2
        0x14 -> :sswitch_3
    .end sparse-switch
.end method
