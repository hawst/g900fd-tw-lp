.class public Lcom/sec/android/app/hwmoduletest/GestureSensor;
.super Landroid/app/Activity;
.source "GestureSensor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private static mSensorName:Ljava/lang/String;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final MSG_UPDATE_GESTURE_VALUE:B

.field private avgData:Ljava/lang/String;

.field private avgDataValueTextView:Landroid/widget/TextView;

.field private bgView:Landroid/view/View;

.field private checkBlue:Z

.field private checkGreen:Z

.field private checkRed:Z

.field private checkYellow:Z

.field private currentData:Ljava/lang/String;

.field private dataValueTextView:Landroid/widget/TextView;

.field private directionTextView:Landroid/widget/TextView;

.field private gestureData:Ljava/lang/String;

.field private gestureValueTextView:Landroid/widget/TextView;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mGetDataCnt:I

.field private mHandler:Landroid/os/Handler;

.field private final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mStartBtn:Landroid/widget/Button;

.field private mTotalData:[I

.field private rawDatas:[Ljava/lang/String;

.field private sensorReadTimer:Ljava/util/Timer;

.field private updateTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    const-string v0, "Gesture Sensor"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->CLASS_NAME:Ljava/lang/String;

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkBlue:Z

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkRed:Z

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkGreen:Z

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkYellow:Z

    .line 71
    const/16 v0, 0x65

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    .line 75
    const/16 v0, 0xa

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->MSG_UPDATE_GESTURE_VALUE:B

    .line 77
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, ".##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    .line 79
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureSensor$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mHandler:Landroid/os/Handler;

    .line 307
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;-><init>(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method private DirectionSetText(F)V
    .locals 4
    .param p1, "DirectionResult"    # F

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 178
    .local v0, "direction_str":Ljava/lang/String;
    const/high16 v1, 0x42880000    # 68.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    .line 179
    const-string v0, "LEFT"

    .line 190
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->directionTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Direction : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    return-void

    .line 180
    :cond_0
    const/high16 v1, 0x42aa0000    # 85.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    .line 181
    const-string v0, "RIGHT"

    goto :goto_0

    .line 182
    :cond_1
    const/high16 v1, 0x42a40000    # 82.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_2

    .line 183
    const-string v0, "DOWN"

    goto :goto_0

    .line 184
    :cond_2
    const/high16 v1, 0x42980000    # 76.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_3

    .line 185
    const-string v0, "UP"

    goto :goto_0

    .line 187
    :cond_3
    const-string v0, "No Detecting"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->getSelfetestData()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->gestureData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgDataValueTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkBlue:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkBlue:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkGreen:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkGreen:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkRed:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkRed:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkYellow:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkYellow:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/GestureSensor;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;
    .param p1, "x1"    # F

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->DirectionSetText(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->gestureValueTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->updateGUI()V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->getSensorData()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->currentData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->dataValueTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureSensor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgData:Ljava/lang/String;

    return-object v0
.end method

.method private getSelfetestData()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 294
    const/4 v0, 0x0

    .line 295
    .local v0, "selfTestData":Ljava/lang/String;
    const-string v2, "/sys/class/sensors/gesture_sensor/selftest"

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 296
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 297
    .local v1, "selfTestDatas":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget-object v3, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v4

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget-object v3, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v5

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget-object v3, v1, v6

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v6

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget-object v3, v1, v7

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v7

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Geture Raw\nN: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", S: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", W:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", E:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aget v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->gestureData:Ljava/lang/String;

    .line 305
    return-void
.end method

.method private getSensorData()V
    .locals 12

    .prologue
    .line 231
    const/4 v5, 0x4

    new-array v0, v5, [I

    .line 232
    .local v0, "dir_data":[I
    const/4 v1, 0x0

    .line 233
    .local v1, "rawData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 235
    .local v3, "selfTestData":Ljava/lang/String;
    const-string v5, "/sys/class/sensors/gesture_sensor/raw_data"

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 237
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 238
    const-string v5, " No sysfs files"

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->currentData:Ljava/lang/String;

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 241
    .local v2, "rawDatas":[Ljava/lang/String;
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 242
    const/4 v5, 0x2

    const/4 v6, 0x2

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 243
    const/4 v5, 0x3

    const/4 v6, 0x3

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 244
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 245
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 246
    const/4 v5, 0x2

    const/4 v6, 0x2

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 247
    const/4 v5, 0x3

    const/4 v6, 0x3

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 248
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "A : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", B : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", C : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", D : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->currentData:Ljava/lang/String;

    .line 251
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    const/16 v6, 0x64

    if-ge v5, v6, :cond_2

    .line 252
    const-string v5, "GESTURE_CROSSTALK_RAW"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 253
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    const/4 v8, 0x0

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 254
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    const/4 v8, 0x1

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 255
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x2

    aget v7, v7, v8

    const/4 v8, 0x2

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 256
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x3

    aget v7, v7, v8

    const/4 v8, 0x3

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 257
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    .line 270
    :cond_2
    :goto_1
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_0

    .line 271
    const-string v5, "GESTURE_CROSSTALK_RAW"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 272
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RESULT - A:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", B:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", C:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x2

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", D:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x3

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgData:Ljava/lang/String;

    .line 288
    :goto_2
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    goto/16 :goto_0

    .line 259
    :cond_3
    const-string v5, "Gesture Sensor"

    const-string v6, "START CROSSTALK"

    const-string v7, "GESTURE_CROSSTALK with selftest"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v5, "/sys/class/sensors/gesture_sensor/selftest"

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 261
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 262
    .local v4, "selfTestDatas":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 263
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x1

    const/4 v7, 0x1

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 264
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x2

    const/4 v7, 0x2

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 265
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v6, 0x3

    const/4 v7, 0x3

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 266
    const/16 v5, 0x64

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    goto/16 :goto_1

    .line 276
    .end local v4    # "selfTestDatas":[Ljava/lang/String;
    :cond_4
    sget-object v5, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorName:Ljava/lang/String;

    const-string v6, "TMG3992"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 277
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Geture Result\nN: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", S: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", W:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v7, 0x2

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", E:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v7, 0x3

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgData:Ljava/lang/String;

    goto/16 :goto_2

    .line 282
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RESULT - A:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", B:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", C:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x2

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", D:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v8, 0x3

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgData:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 194
    const-string v4, ""

    .line 195
    .local v4, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 198
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x1fa0

    invoke-direct {v1, v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 201
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 202
    if-eqz v4, :cond_0

    .line 203
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 213
    :cond_0
    if-eqz v1, :cond_4

    .line 215
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 223
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v4, :cond_2

    .line 224
    const-string v4, ""

    .line 226
    .end local v4    # "result":Ljava/lang/String;
    :cond_2
    return-object v4

    .line 216
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v4    # "result":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 217
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 219
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 206
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 207
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "FileNotFoundException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 213
    if-eqz v0, :cond_1

    .line 215
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 216
    :catch_2
    move-exception v2

    .line 217
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 209
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 210
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 213
    if-eqz v0, :cond_1

    .line 215
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 216
    :catch_4
    move-exception v2

    .line 217
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 213
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v0, :cond_3

    .line 215
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 219
    :cond_3
    :goto_4
    throw v5

    .line 216
    :catch_5
    move-exception v2

    .line 217
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "Gesture Sensor"

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 213
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 209
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 206
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_7
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private updateGUI()V
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureSensor$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor$4;-><init>(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 173
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b008f

    if-ne v0, v1, :cond_0

    .line 155
    const-string v0, "Gathering raw datas..."

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgData:Ljava/lang/String;

    .line 156
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mGetDataCnt:I

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    aput v2, v0, v2

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 162
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x4

    .line 92
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->setContentView(I)V

    .line 94
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mTotalData:[I

    .line 95
    const v0, 0x7f0b008d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->gestureValueTextView:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0b008c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->dataValueTextView:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0b008e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->directionTextView:Landroid/widget/TextView;

    .line 98
    const-string v0, "CROSSTALK_VALUE_DIRECTION"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->dataValueTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->directionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    :cond_0
    const v0, 0x7f0b008a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;

    .line 104
    const v0, 0x7f0b0090

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->avgDataValueTextView:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0b008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mStartBtn:Landroid/widget/Button;

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    const-string v0, "sensorhub"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 108
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gesture:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorName:Ljava/lang/String;

    .line 109
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 140
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->updateTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->sensorReadTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 146
    const-string v0, "CROSSTALK_VALUE_DIRECTION"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;)V

    .line 149
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->finish()V

    .line 150
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const-wide/16 v2, 0xc8

    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 114
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v6

    .line 116
    .local v6, "gesture":Lcom/samsung/android/sensorhub/SensorHub;
    const-string v0, "CROSSTALK_VALUE_DIRECTION"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v6, v4}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 121
    :cond_0
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Gesture_Update"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->updateTimer:Ljava/util/Timer;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->updateTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/GestureSensor$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor$2;-><init>(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 130
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Gesture_Read"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->sensorReadTimer:Ljava/util/Timer;

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor;->sensorReadTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/GestureSensor$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GestureSensor$3;-><init>(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->DirectionSetText(F)V

    .line 137
    return-void
.end method
