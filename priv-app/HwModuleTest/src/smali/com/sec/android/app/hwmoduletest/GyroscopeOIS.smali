.class public Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeOIS.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/GyroscopeOIS$EmptyListener;
    }
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIntialTestResults:[Ljava/lang/String;

.field private mPass:Z

.field private mSelfTestResults:[Ljava/lang/String;

.field private txt_diff_x:Landroid/widget/TextView;

.field private txt_diff_y:Landroid/widget/TextView;

.field private txt_dps_x:Landroid/widget/TextView;

.field private txt_dps_y:Landroid/widget/TextView;

.field private txt_intial_x:Landroid/widget/TextView;

.field private txt_intial_y:Landroid/widget/TextView;

.field private txt_self_result:Landroid/widget/TextView;

.field private txtresult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "GyroscopeOIS"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mPass:Z

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method private CheckSpecin([Ljava/lang/String;)Z
    .locals 10
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 134
    const-string v5, "OIS_INITIAL_SPEC_MAX"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 135
    .local v2, "mSpec_Max":Ljava/lang/Double;
    const-string v5, "OIS_INITIAL_SPEC_MIN"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 137
    .local v3, "mSpec_Min":Ljava/lang/Double;
    const/4 v1, 0x1

    .line 139
    .local v1, "isSpecin":Z
    if-nez p1, :cond_0

    .line 140
    const/4 v5, 0x0

    .line 157
    :goto_0
    return v5

    .line 143
    :cond_0
    array-length v5, p1

    new-array v4, v5, [Ljava/lang/Double;

    .line 145
    .local v4, "measure":[Ljava/lang/Double;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v5, p1

    if-ge v0, v5, :cond_1

    .line 146
    aget-object v5, p1, v0

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v0

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 149
    :cond_1
    const/4 v0, 0x0

    :goto_2
    array-length v5, v4

    if-ge v0, v5, :cond_4

    .line 150
    aget-object v5, v4, v0

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    cmpg-double v5, v6, v8

    if-ltz v5, :cond_2

    aget-object v5, v4, v0

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    cmpl-double v5, v6, v8

    if-lez v5, :cond_3

    .line 151
    :cond_2
    const/4 v1, 0x0

    .line 149
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 155
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "CheckSpecin"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v5, v1

    .line 157
    goto :goto_0
.end method

.method private GyroTestFail()V
    .locals 4

    .prologue
    .line 161
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 162
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const-string v3, "Gyro Test Fail"

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 163
    const-string v3, "Do not move & Re \u2013test"

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 164
    new-instance v2, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS$EmptyListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS$EmptyListener;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;)V

    .line 165
    .local v2, "pl":Landroid/content/DialogInterface$OnDismissListener;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 166
    .local v0, "ad":Landroid/app/AlertDialog;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 167
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 168
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mSelfTestResults:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mIntialTestResults:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # [Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->showTestResults([Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f0b0102

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f0b00fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_dps_x:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0b00fc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_dps_y:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0b00fd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_intial_x:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f0b00fe

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_intial_y:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0b00ff

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_diff_x:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0b0100

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_diff_y:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0b0101

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_self_result:Landroid/widget/TextView;

    .line 51
    return-void
.end method

.method private showTestResults([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10
    .param p1, "results"    # [Ljava/lang/String;
    .param p2, "intial_results"    # [Ljava/lang/String;

    .prologue
    .line 95
    const/4 v6, 0x1

    aget-object v6, p1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "dps_x":Ljava/lang/String;
    const/4 v6, 0x2

    aget-object v6, p1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "dps_y":Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v6, p2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 98
    .local v4, "intial_x":Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v6, p2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 100
    .local v5, "intial_y":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_dps_x:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_dps_y:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_intial_x:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_intial_y:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 106
    .local v0, "diff_x_value":Ljava/lang/Double;
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 108
    .local v1, "diff_y_value":Ljava/lang/Double;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_diff_x:Landroid/widget/TextView;

    const-string v7, "%.3f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_diff_y:Landroid/widget/TextView;

    const-string v7, "%.3f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const/4 v6, 0x0

    aget-object v6, p1, v6

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 112
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_self_result:Landroid/widget/TextView;

    const-string v7, "PASS"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_self_result:Landroid/widget/TextView;

    const v7, -0xffff01

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mPass:Z

    .line 120
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_self_result:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mPass:Z

    if-eqz v6, :cond_1

    invoke-direct {p0, p2}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->CheckSpecin([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 123
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const-string v7, "PASS"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const v7, -0xffff01

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    return-void

    .line 116
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_self_result:Landroid/widget/TextView;

    const-string v7, "FAIL"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txt_self_result:Landroid/widget/TextView;

    const/high16 v7, -0x10000

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 126
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const-string v7, "FAIL"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const/high16 v7, -0x10000

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->GyroTestFail()V

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->setContentView(I)V

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->init()V

    .line 40
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 87
    const-string v0, "GYRO_OIS_POWER"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 88
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    .line 54
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 57
    const-string v3, "GYRO_OIS_POWER"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 59
    const-string v3, "GYRO_OIS_RAWDATA"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "intialData":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->txtresult:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :try_start_0
    const-string v3, "GYRO_OIS_SELFTEST"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, "selfTestResult":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 68
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mSelfTestResults:[Ljava/lang/String;

    .line 69
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mIntialTestResults:[Ljava/lang/String;

    .line 71
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 82
    .end local v2    # "selfTestResult":Ljava/lang/String;
    :goto_0
    return-void

    .line 77
    .restart local v2    # "selfTestResult":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeOIS;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "onResume"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "selfTestResult : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", intialData : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 79
    .end local v2    # "selfTestResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method
