.class public Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TspPatternStyleHoverX.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TspPatternStyleHoverX"


# instance fields
.field private HEIGHT_BASIS:I

.field private HEIGHT_BASIS_10:I

.field private HEIGHT_BASIS_6:I

.field protected KEY_TIMEOUT:I

.field protected KEY_TIMER_EXPIRED:I

.field protected MILLIS_IN_SEC:I

.field private WIDTH_BASIS:I

.field private WIDTH_BASIS_10:I

.field private WIDTH_BASIS_6:I

.field private col_height:F

.field private col_height_10:F

.field private col_height_6:F

.field private col_start_x_10:F

.field private col_start_x_6:F

.field private col_start_y_10:F

.field private col_start_y_6:F

.field private col_width:F

.field private col_width_10:F

.field private col_width_6:F

.field private dialog_showing:Z

.field private draw10:[Z

.field private draw6:[Z

.field private isHovering:Z

.field private mBottommostOfMatrix:I

.field private mCenterOfHorizontalOfMatrix:I

.field private mCenterOfVerticalOfMatrix:I

.field private mLeftmostOfMatrix:I

.field private mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

.field private mRightmostOfMatrix:I

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTopmostOfMatrix:I

.field private needFailPopupDispaly:Z

.field private remoteCall:Z

.field private successTest:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 78
    const-string v0, "TspPatternStyleHoverX"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 34
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS:I

    .line 35
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS:I

    .line 36
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->remoteCall:Z

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->successTest:Z

    .line 38
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->needFailPopupDispaly:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->dialog_showing:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z

    .line 57
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height:F

    .line 58
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width:F

    .line 60
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    .line 61
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    .line 62
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F

    .line 63
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F

    .line 64
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    .line 65
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    .line 67
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F

    .line 68
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F

    .line 69
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F

    .line 70
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F

    .line 71
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    .line 72
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isPass10()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isPass6()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->successTest:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->successTest:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->remoteCall:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw6:[Z

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F

    return v0
.end method

.method private decideRemote()V
    .locals 3

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 210
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RemoteCall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->remoteCall:Z

    .line 211
    return-void
.end method

.method private isNeededCheck(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 214
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mTopmostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mBottommostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNeededCheck_Hovering(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mTopmostOfMatrix:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_2

    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mBottommostOfMatrix:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mLeftmostOfMatrix:I

    add-int/lit8 v0, v0, 0x1

    if-eq p2, v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mRightmostOfMatrix:I

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPass10()Z
    .locals 4

    .prologue
    .line 225
    const/4 v1, 0x1

    .line 227
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x2

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_1

    .line 228
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 227
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 231
    :cond_1
    return v1
.end method

.method private isPass6()Z
    .locals 4

    .prologue
    .line 235
    const/4 v1, 0x1

    .line 237
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_1

    .line 238
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw6:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 237
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 241
    :cond_1
    return v1
.end method

.method private setTSP()V
    .locals 9

    .prologue
    const/high16 v5, 0x40200000    # 2.5f

    const/high16 v8, 0x3fc00000    # 1.5f

    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->getApplication()Landroid/app/Application;

    move-result-object v3

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 84
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 86
    .local v2, "outpoint":Landroid/graphics/Point;
    const-string v3, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 87
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 91
    :goto_0
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    .line 92
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    .line 94
    const-string v3, "TSP_X_AXIS_CHANNEL"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS:I

    .line 95
    const-string v3, "TSP_Y_AXIS_CHANNEL"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS:I

    .line 97
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height:F

    .line 98
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width:F

    .line 100
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height:F

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height:F

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    .line 101
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width:F

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width:F

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    .line 102
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height:F

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    .line 103
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width:F

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    .line 104
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F

    .line 105
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F

    .line 107
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    add-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height:F

    mul-float/2addr v4, v8

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    .line 108
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    add-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width:F

    mul-float/2addr v4, v8

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    .line 109
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    add-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F

    .line 110
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    add-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F

    .line 111
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F

    .line 112
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F

    .line 115
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    new-array v3, v3, [Z

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z

    .line 116
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    add-int/2addr v3, v4

    new-array v3, v3, [Z

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw6:[Z

    .line 117
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    if-ge v0, v3, :cond_1

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z

    aput-boolean v7, v3, v0

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 89
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    goto/16 :goto_0

    .line 121
    .restart local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I

    add-int/2addr v3, v4

    if-ge v0, v3, :cond_2

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw6:[Z

    aput-boolean v7, v3, v0

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 126
    :cond_2
    iput v7, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mTopmostOfMatrix:I

    .line 127
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mBottommostOfMatrix:I

    .line 128
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS:I

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mCenterOfVerticalOfMatrix:I

    .line 129
    iput v7, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mLeftmostOfMatrix:I

    .line 130
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mRightmostOfMatrix:I

    .line 131
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS:I

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mCenterOfHorizontalOfMatrix:I

    .line 132
    const/4 v3, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->KEY_TIMER_EXPIRED:I

    .line 133
    const/16 v3, 0x3e8

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->MILLIS_IN_SEC:I

    .line 134
    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->KEY_TIMEOUT:I

    .line 135
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 138
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "isHovering"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z

    .line 144
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TEST_TSP_SELF = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "TEST_TSP_SELF"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "TEST_TSP_SELF"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->successTest:Z

    .line 149
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->needFailPopupDispaly:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->setTSP()V

    .line 156
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->decideRemote()V

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "TouchTest is created"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    new-instance v1, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->setContentView(Landroid/view/View;)V

    .line 159
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 189
    packed-switch p1, :pswitch_data_0

    .line 205
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 192
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isPass10()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isPass6()Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->setResult(I)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->finish()V

    goto :goto_0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 176
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, "DisableHovering"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->hoveringOnOFF(Ljava/lang/String;)V

    .line 182
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->unregisterBroadcastReceiver(Landroid/content/Context;)V

    .line 184
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 164
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "EnableHovering"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->hoveringOnOFF(Ljava/lang/String;)V

    .line 170
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->registerBroadcastReceiver(Landroid/content/Context;)V

    .line 172
    :cond_0
    return-void
.end method
