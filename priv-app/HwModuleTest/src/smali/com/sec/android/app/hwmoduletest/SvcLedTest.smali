.class public Lcom/sec/android/app/hwmoduletest/SvcLedTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "SvcLedTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final LED_BLUE:I

.field private final LED_MAGENTA:I

.field private final LED_POWER_LOW:Ljava/lang/String;

.field private final LED_RED:I

.field private _buttonBlue:Landroid/widget/Button;

.field private _buttonMagenta:Landroid/widget/Button;

.field private _buttonOff:Landroid/widget/Button;

.field private _buttonRed:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "SvcLedTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 15
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->LED_POWER_LOW:Ljava/lang/String;

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->LED_RED:I

    .line 18
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->LED_BLUE:I

    .line 19
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->LED_MAGENTA:I

    .line 28
    return-void
.end method

.method private turnOffLed()V
    .locals 2

    .prologue
    .line 78
    const-string v0, "LED_RED"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 79
    const-string v0, "LED_BLUE"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 80
    return-void
.end method

.method private turnOnLed(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 73
    const-string v1, "LED_RED"

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 74
    const-string v1, "LED_BLUE"

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_1
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 75
    return-void

    .line 73
    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 74
    :cond_1
    const-string v0, "0"

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 70
    :goto_0
    return-void

    .line 58
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->turnOffLed()V

    goto :goto_0

    .line 61
    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->turnOnLed(I)V

    goto :goto_0

    .line 64
    :pswitch_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->turnOnLed(I)V

    goto :goto_0

    .line 67
    :pswitch_3
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->turnOnLed(I)V

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0231
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f030074

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->setContentView(I)V

    .line 34
    const v0, 0x7f0b0231

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonOff:Landroid/widget/Button;

    .line 35
    const v0, 0x7f0b0232

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonRed:Landroid/widget/Button;

    .line 36
    const v0, 0x7f0b0233

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonBlue:Landroid/widget/Button;

    .line 37
    const v0, 0x7f0b0234

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonMagenta:Landroid/widget/Button;

    .line 39
    const-string v0, "APQ8084"

    const-string v1, "APCHIP_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "LED_LOWPOWER"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonOff:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonRed:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonBlue:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->_buttonMagenta:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SvcLedTest;->turnOffLed()V

    .line 52
    return-void
.end method
