.class public Lcom/sec/android/app/hwmoduletest/TempHumidTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TempHumidTest.java"

# interfaces
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# instance fields
.field private HumidSensorMgrValue:Ljava/lang/String;

.field public final MSG_UPDATE_UI:I

.field private TempSensorMgrValue:Ljava/lang/String;

.field public humiCompRaw:[Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHumid_Acc:Landroid/widget/TextView;

.field private mHumid_Comp:Landroid/widget/TextView;

.field private mHumid_Raw:Landroid/widget/TextView;

.field private mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

.field private mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

.field private mTemp_Acc:Landroid/widget/TextView;

.field private mTemp_Comp:Landroid/widget/TextView;

.field private mTemp_Raw:Landroid/widget/TextView;

.field public tempCompRaw:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    const-string v0, "TempHumidTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 35
    const-string v0, "0,0,0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->TempSensorMgrValue:Ljava/lang/String;

    .line 36
    const-string v0, "0,0,0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->HumidSensorMgrValue:Ljava/lang/String;

    .line 38
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->MSG_UPDATE_UI:I

    .line 39
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v2

    const-string v1, "0"

    aput-object v1, v0, v3

    const-string v1, "0"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    .line 43
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v2

    const-string v1, "0"

    aput-object v1, v0, v3

    const-string v1, "0"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/TempHumidTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHandler:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 144
    const v0, 0x7f0b0240

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Acc:Landroid/widget/TextView;

    .line 145
    const v0, 0x7f0b0241

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Raw:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f0b0242

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Comp:Landroid/widget/TextView;

    .line 147
    const v0, 0x7f0b0243

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Acc:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f0b0244

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Raw:Landroid/widget/TextView;

    .line 149
    const v0, 0x7f0b0245

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Comp:Landroid/widget/TextView;

    .line 150
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v0, 0x7f030076

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->setContentView(I)V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->init()V

    .line 67
    return-void
.end method

.method protected onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Temp ACC :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Temp Raw :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Temp Comp :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Humid ACC :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Humid Raw :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Humid Comp :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOff()V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;->SensorOff()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 139
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 140
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v5, 0x3e8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 71
    new-array v1, v4, [I

    const/16 v2, 0x9

    aput v2, v1, v3

    .line 72
    .local v1, "mSensorTempID":[I
    new-array v0, v4, [I

    const/16 v2, 0xa

    aput v2, v0, v3

    .line 73
    .local v0, "mSensorHumidID":[I
    new-instance v2, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-virtual {v2, p0, v1, v5}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V

    .line 75
    new-instance v2, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mSensorHumid:Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;

    invoke-virtual {v2, p0, v0, v5}, Lcom/sec/android/app/hwmoduletest/sensors/SensorHumid;->SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V

    .line 77
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 4
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sparse-switch p1, :sswitch_data_0

    .line 98
    :goto_0
    return-void

    .line 84
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 88
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_TEMP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->TempSensorMgrValue:Ljava/lang/String;

    goto :goto_0

    .line 92
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_HUMID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->HumidSensorMgrValue:Ljava/lang/String;

    goto :goto_0

    .line 82
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public updateUI()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "tempACC":I
    const/4 v0, 0x0

    .line 105
    .local v0, "humidACC":I
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->TempSensorMgrValue:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->TempSensorMgrValue:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->TempSensorMgrValue:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    .line 108
    :cond_0
    const-string v2, "3.0"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Comp:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    float-to-int v1, v2

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Acc:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Raw:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->tempCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->HumidSensorMgrValue:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->HumidSensorMgrValue:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->HumidSensorMgrValue:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    .line 122
    :cond_1
    const-string v2, "3.0"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Comp:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    float-to-int v0, v2

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Acc:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Raw:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->humiCompRaw:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-void

    .line 111
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mTemp_Comp:Landroid/widget/TextView;

    const-string v3, "- -"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 125
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidTest;->mHumid_Comp:Landroid/widget/TextView;

    const-string v3, "- -"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
