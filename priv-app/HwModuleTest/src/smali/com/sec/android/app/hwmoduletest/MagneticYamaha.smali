.class public Lcom/sec/android/app/hwmoduletest/MagneticYamaha;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "MagneticYamaha.java"


# instance fields
.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mSensorID_ADC:I

.field private mSensorID_DAC:I

.field private mSensorID_Initialized:I

.field private mSensorID_None:I

.field private mSensorID_Offset_H:I

.field private mSensorID_Released:I

.field private mSensorID_Self:I

.field private mSensorID_Status:I

.field private mTableRow_ADC:Landroid/widget/TableRow;

.field private mTableRow_DAC:Landroid/widget/TableRow;

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTableRow_Offset_H:Landroid/widget/TableRow;

.field private mTableRow_SX:Landroid/widget/TableRow;

.field private mTableRow_SY:Landroid/widget/TableRow;

.field private mTableRow_Temp:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_ADC_X:Landroid/widget/TextView;

.field private mText_ADC_Y:Landroid/widget/TextView;

.field private mText_ADC_Z:Landroid/widget/TextView;

.field private mText_DAC_X:Landroid/widget/TextView;

.field private mText_DAC_Y:Landroid/widget/TextView;

.field private mText_DAC_Z:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Offset_H_X:Landroid/widget/TextView;

.field private mText_Offset_H_Y:Landroid/widget/TextView;

.field private mText_Offset_H_Z:Landroid/widget/TextView;

.field private mText_SX:Landroid/widget/TextView;

.field private mText_SY:Landroid/widget/TextView;

.field private mText_Temp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "MagneticYamaha"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSenserID:[I

    .line 59
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    .line 60
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Initialized:I

    .line 61
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Status:I

    .line 62
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_DAC:I

    .line 63
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_ADC:I

    .line 64
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Self:I

    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Offset_H:I

    .line 66
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Released:I

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->WHAT_UPDATE:I

    .line 112
    new-instance v0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha$1;-><init>(Lcom/sec/android/app/hwmoduletest/MagneticYamaha;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mHandler:Landroid/os/Handler;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/MagneticYamaha;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticYamaha;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/MagneticYamaha;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticYamaha;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->update()V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 122
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 123
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Initialized:Landroid/widget/TextView;

    .line 129
    const v0, 0x7f0b016a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_SX:Landroid/widget/TableRow;

    .line 130
    const v0, 0x7f0b016b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_SX:Landroid/widget/TextView;

    .line 132
    const v0, 0x7f0b016c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_SY:Landroid/widget/TableRow;

    .line 133
    const v0, 0x7f0b016d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_SY:Landroid/widget/TextView;

    .line 135
    const v0, 0x7f0b016e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_DAC:Landroid/widget/TableRow;

    .line 136
    const v0, 0x7f0b016f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_X:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f0b0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_Y:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f0b0171

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_Z:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_ADC:Landroid/widget/TableRow;

    .line 141
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_X:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f0b0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_Y:Landroid/widget/TextView;

    .line 143
    const v0, 0x7f0b0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_Z:Landroid/widget/TextView;

    .line 145
    const v0, 0x7f0b0176

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_Offset_H:Landroid/widget/TableRow;

    .line 146
    const v0, 0x7f0b0177

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_X:Landroid/widget/TextView;

    .line 147
    const v0, 0x7f0b0178

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_Y:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f0b0179

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_Z:Landroid/widget/TextView;

    .line 150
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTextResult:Landroid/widget/TextView;

    .line 151
    return-void
.end method

.method private update()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const/4 v3, 0x0

    .line 156
    .local v3, "tempdata":Ljava/lang/String;
    const/4 v1, 0x0

    .line 157
    .local v1, "data":[Ljava/lang/String;
    const/4 v2, 0x1

    .line 158
    .local v2, "isPass":Z
    const/4 v0, 0x0

    .line 159
    .local v0, "PowerOnResult":Ljava/lang/String;
    const-string v4, "GEOMAGNETIC_SENSOR_SELFTEST"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 161
    if-eqz v3, :cond_0

    .line 162
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 165
    :cond_0
    if-eqz v1, :cond_8

    .line 167
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_Initialized:Landroid/widget/TableRow;

    invoke-virtual {v4, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 169
    aget-object v4, v1, v8

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Initialized - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v0, "1"

    .line 178
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Initialized:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Initialized Return : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    aget-object v4, v1, v9

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Status - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Retuen : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_DAC:Landroid/widget/TableRow;

    invoke-virtual {v4, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 193
    aget-object v4, v1, v10

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "DAC - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_X:Landroid/widget/TextView;

    aget-object v5, v1, v11

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_Y:Landroid/widget/TextView;

    aget-object v5, v1, v12

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_Z:Landroid/widget/TextView;

    const/4 v5, 0x6

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Retuen : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", [DAC]X:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Y:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Z:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x6

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_ADC:Landroid/widget/TableRow;

    invoke-virtual {v4, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 208
    const/4 v4, 0x7

    aget-object v4, v1, v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "ADC - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_X:Landroid/widget/TextView;

    const/16 v5, 0x8

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v5, "0"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v5, "0"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Retuen : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x7

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", [ADC]X:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x8

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Y:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Z:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_SX:Landroid/widget/TableRow;

    invoke-virtual {v4, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_SY:Landroid/widget/TableRow;

    invoke-virtual {v4, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 224
    const/16 v4, 0x9

    aget-object v4, v1, v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 225
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Self - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_SX:Landroid/widget/TextView;

    const/16 v5, 0xa

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_SY:Landroid/widget/TextView;

    const/16 v5, 0xb

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Retuen : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x9

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", SX:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xa

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", SY:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xb

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTableRow_Offset_H:Landroid/widget/TableRow;

    invoke-virtual {v4, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 238
    const/16 v4, 0xc

    aget-object v4, v1, v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 239
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Offset - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :goto_5
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_X:Landroid/widget/TextView;

    const/16 v5, 0xd

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_Y:Landroid/widget/TextView;

    const/16 v5, 0xe

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_Z:Landroid/widget/TextView;

    const/16 v5, 0xf

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Retuen : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xc

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", [Offset H]X:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xd

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Y:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xe

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Z:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xf

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const/16 v4, 0x10

    aget-object v4, v1, v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 253
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Released - Pass"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :goto_6
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Released Return : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x10

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_7
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTextResult:Landroid/widget/TextView;

    if-eqz v2, :cond_9

    const-string v4, "PASS"

    :goto_8
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mTextResult:Landroid/widget/TextView;

    if-eqz v2, :cond_a

    const v4, -0xffff01

    :goto_9
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 285
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Result:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v2, :cond_b

    const-string v4, "PASS"

    :goto_a
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    return-void

    .line 173
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Initialized - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v2, 0x0

    .line 175
    const-string v0, "0"

    goto/16 :goto_0

    .line 185
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Status - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 196
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "DAC - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 211
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "ADC - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 227
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Self - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 241
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Offset - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 255
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Released - Fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 261
    :cond_8
    const/4 v2, 0x0

    .line 262
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Initialized:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Initialized - Fail : null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_X:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_Y:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_DAC_Z:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "DAC - Fail : null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_X:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "ADC - Fail : null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_SX:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_SY:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Self - Fail : null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_X:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_Y:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mText_Offset_H_Z:Landroid/widget/TextView;

    const-string v5, "NONE"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Offset - Fail : null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "update"

    const-string v6, "Released - Fail : null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 283
    :cond_9
    const-string v4, "FAIL"

    goto/16 :goto_8

    .line 284
    :cond_a
    const/high16 v4, -0x10000

    goto/16 :goto_9

    .line 285
    :cond_b
    const-string v4, "FAIL"

    goto/16 :goto_a
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v0, 0x7f030057

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->setContentView(I)V

    .line 77
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mFeature:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->init()V

    .line 80
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 110
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mFeature:Ljava/lang/String;

    const-string v1, "YAS530C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mFeature:Ljava/lang/String;

    const-string v1, "YAS532B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mFeature:Ljava/lang/String;

    const-string v1, "YAS532"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    :cond_0
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Initialized:I

    .line 89
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Status:I

    .line 90
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_DAC:I

    .line 91
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_ADC:I

    .line 92
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Self:I

    .line 93
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Offset_H:I

    .line 94
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Released:I

    .line 97
    :cond_1
    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Initialized:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Status:I

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_DAC:I

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_ADC:I

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Self:I

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Offset_H:I

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSensorID_Released:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSenserID:[I

    .line 101
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticYamaha;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 105
    return-void
.end method
