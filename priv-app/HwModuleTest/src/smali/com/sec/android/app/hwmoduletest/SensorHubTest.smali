.class public Lcom/sec/android/app/hwmoduletest/SensorHubTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "SensorHubTest.java"


# static fields
.field private static final ACTION_WAKE_UP:Ljava/lang/String; = "com.sec.factory.WakeUp"

.field public static final FINISH_TEST:I = 0x3

.field private static final RTC_OFF:Ljava/lang/String; = "android.intent.action.START_FACTORY_TEST"

.field private static final RTC_ON:Ljava/lang/String; = "android.intent.action.STOP_FACTORY_TEST"

.field public static final SENSORHUB_START:I = 0x1

.field public static final SENSORHUB_WAKEUP:I = 0x2

.field public static final WAKEUP_DELAY:I = 0x1388

.field protected static mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;


# instance fields
.field private BinFwVersion:Landroid/widget/TextView;

.field private final FAIL:I

.field private IntCheckResult:Landroid/widget/TextView;

.field private McuChipName:Landroid/widget/TextView;

.field private McuFwVersion:Landroid/widget/TextView;

.field private final PASS:I

.field private PassFail:Landroid/widget/TextView;

.field isFactoryMode:Z

.field public mHandler:Landroid/os/Handler;

.field private mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

.field private mPM:Landroid/os/PowerManager;

.field private result:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "SensorHubTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->isFactoryMode:Z

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->PASS:I

    .line 38
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->FAIL:I

    .line 55
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mHandler:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getMcuFirmware()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->IntCheckResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->McuChipName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->checkPassFail()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->startSensorHubTest()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getBinFirmware()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/os/PowerManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mPM:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/hwmoduletest/SensorHubTest;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorHubTest;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;

    return-object p1
.end method

.method private checkPassFail()V
    .locals 4

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 178
    .local v0, "passfail":Z
    const-string v1, "OK"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "OK"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    const/4 v0, 0x1

    .line 182
    :cond_0
    if-eqz v0, :cond_1

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->PassFail:Landroid/widget/TextView;

    const-string v2, "PASS"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->PassFail:Landroid/widget/TextView;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 189
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->PassFail:Landroid/widget/TextView;

    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->PassFail:Landroid/widget/TextView;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private getBinFirmware()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getBinFirmware"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v2, "SENSORHUB_FIRMWARE_VERSION"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "tempResult":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 167
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "BinFirmware":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getBinFirmware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BinFirmware = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->BinFwVersion:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bin Firm Version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    .end local v0    # "BinFirmware":[Ljava/lang/String;
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getBinFirmware"

    const-string v4, "BinFirmware is NULL"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getMcuFirmware()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getMcuFirmware"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v2, "SENSORHUB_FIRMWARE_VERSION"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "temp":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 156
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "McuFirmware":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->McuFwVersion:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MCU Firm Version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getMcuFirmware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "McuFirmware = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .end local v0    # "McuFirmware":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "init()"

    const-string v2, "init_start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const v0, 0x7f0b0229

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->IntCheckResult:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f0b022a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->McuFwVersion:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f0b022b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->BinFwVersion:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f0b022c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->McuChipName:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f0b022e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->PassFail:Landroid/widget/TextView;

    .line 120
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mPM:Landroid/os/PowerManager;

    .line 122
    sget-object v0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    .line 123
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->isFactoryMode:Z

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "handleCommand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isFactoryMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->isFactoryMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void

    .line 122
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    goto :goto_0
.end method

.method private startSensorHubTest()V
    .locals 4

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startSensorHubTest"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 149
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate()"

    const-string v2, "startOnCreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 133
    const v0, 0x7f030070

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->setContentView(I)V

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->init()V

    .line 135
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/SensorHubTest$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 142
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 200
    packed-switch p1, :pswitch_data_0

    .line 205
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 202
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->finish()V

    .line 195
    const/4 v0, 0x1

    return v0
.end method
