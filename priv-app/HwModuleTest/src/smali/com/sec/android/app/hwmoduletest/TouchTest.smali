.class public Lcom/sec/android/app/hwmoduletest/TouchTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TouchTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;
    }
.end annotation


# instance fields
.field private HEIGHT_BASIS:I

.field protected KEY_TIMEOUT:I

.field protected KEY_TIMER_EXPIRED:I

.field protected MILLIS_IN_SEC:I

.field private WIDTH_BASIS:I

.field private click:[[Z

.field private draw:[[Z

.field private isDrawArea:[[Z

.field private isHovering:Z

.field private iscompleted:Z

.field private mBottommostOfMatrix:I

.field private mCenterOfHorizontalOfMatrix:I

.field private mCenterOfVerticalOfMatrix:I

.field private mLeftmostOfMatrix:I

.field private mRightmostOfMatrix:I

.field private mTopmostOfMatrix:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    const-string v0, "TouchTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 29
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    .line 30
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    .line 32
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->click:[[Z

    .line 33
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z

    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    .line 37
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mTopmostOfMatrix:I

    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mBottommostOfMatrix:I

    .line 39
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfVerticalOfMatrix:I

    .line 40
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    .line 41
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfHorizontalOfMatrix:I

    .line 46
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->iscompleted:Z

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/TouchTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->click:[[Z

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/TouchTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->isPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->iscompleted:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/hwmoduletest/TouchTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TouchTest;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->iscompleted:Z

    return p1
.end method

.method private fillUpMatrix()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 96
    const/4 v1, 0x0

    .local v1, "row":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    if-ge v1, v2, :cond_3

    .line 97
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    if-ge v0, v2, :cond_2

    .line 98
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->isNeededCheck_Hovering(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    aput-boolean v4, v2, v0

    .line 97
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 100
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z

    if-nez v2, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->isNeededCheck(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    aput-boolean v4, v2, v0

    goto :goto_2

    .line 103
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    const/4 v3, 0x0

    aput-boolean v3, v2, v0

    goto :goto_2

    .line 96
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "column":I
    :cond_3
    return-void
.end method

.method private isNeededCheck(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 110
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mTopmostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mBottommostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfVerticalOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfHorizontalOfMatrix:I

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNeededCheck_Hovering(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mTopmostOfMatrix:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_3

    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mBottommostOfMatrix:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_3

    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    add-int/lit8 v0, v0, 0x1

    if-eq p2, v0, :cond_3

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    add-int/lit8 v0, v0, -0x1

    if-eq p2, v0, :cond_3

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfVerticalOfMatrix:I

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_3

    :cond_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfHorizontalOfMatrix:I

    if-ne p2, v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPass()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 129
    const/4 v1, 0x1

    .line 131
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    if-ge v0, v4, :cond_3

    .line 132
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    if-ge v2, v4, :cond_2

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-ne v4, v3, :cond_0

    .line 134
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_1

    move v1, v3

    .line 132
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 134
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 131
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    .end local v2    # "j":I
    :cond_3
    return v1
.end method

.method private setTSP()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    const-string v0, "TSP_X_AXIS_CHANNEL"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    .line 55
    const-string v0, "TSP_Y_AXIS_CHANNEL"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    .line 56
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->click:[[Z

    .line 57
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z

    .line 58
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z

    .line 59
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mTopmostOfMatrix:I

    .line 60
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mBottommostOfMatrix:I

    .line 61
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfVerticalOfMatrix:I

    .line 62
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mLeftmostOfMatrix:I

    .line 63
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mRightmostOfMatrix:I

    .line 64
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->mCenterOfHorizontalOfMatrix:I

    .line 65
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->KEY_TIMER_EXPIRED:I

    .line 66
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->MILLIS_IN_SEC:I

    .line 67
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->KEY_TIMEOUT:I

    .line 68
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->finish()V

    .line 159
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isHovering"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->setTSP()V

    .line 76
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/TouchTest;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->setContentView(Landroid/view/View;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->fillUpMatrix()V

    .line 79
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 169
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 142
    sparse-switch p1, :sswitch_data_0

    .line 154
    :goto_0
    :sswitch_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 144
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->finish()V

    goto :goto_0

    .line 149
    :sswitch_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    .line 142
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 164
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 86
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "TEST_TSP_SELF"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/hwmoduletest/TspFailPop;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .local v1, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TouchTest;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onResume"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onStop"

    const-string v2, "Do nothing"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onStop()V

    .line 126
    return-void
.end method
