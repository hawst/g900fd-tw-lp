.class public Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "AccGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;
    }
.end annotation


# static fields
.field private static final mIsProxMotorTest:Z


# instance fields
.field private final FEED_BACK_PATTERN:[J

.field LPFButtonListener:Landroid/view/View$OnClickListener;

.field private final MSG_UPDATE_SENSOR_VALUE:B

.field private final MSG_VIBRATE_FEED_BACK_END:B

.field private final MSG_VIBRATE_FEED_BACK_START:B

.field private btnLPFOnOff:Landroid/widget/Button;

.field private mAccGraph:Lcom/sec/android/app/hwmoduletest/view/AccGraph;

.field private mAcceSensor:Landroid/hardware/Sensor;

.field private mAcceSensorValues:[F

.field private mAccelerometer_name:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIsDownKeyVib:Z

.field private mIsVibrate:Z

.field private mLogCount:I

.field private mProxSensorValue:F

.field private mProxStatus:I

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mRate:[I

.field private mRawData:[I

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTimer:Ljava/util/Timer;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private task:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "IS_PROXIMITY_TEST_MOTOR_FEEDBACK"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsProxMotorTest:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 34
    const-string v0, "AccGraphActivity"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 39
    const/16 v0, 0xa

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->MSG_UPDATE_SENSOR_VALUE:B

    .line 40
    const/16 v0, 0xb

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->MSG_VIBRATE_FEED_BACK_START:B

    .line 41
    const/16 v0, 0xc

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->MSG_VIBRATE_FEED_BACK_END:B

    .line 50
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensorValues:[F

    .line 51
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    .line 52
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxSensorValue:F

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxStatus:I

    .line 54
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRate:[I

    .line 56
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mLogCount:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->FEED_BACK_PATTERN:[J

    .line 68
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;-><init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    .line 113
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$2;-><init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->LPFButtonListener:Landroid/view/View$OnClickListener;

    .line 36
    return-void

    .line 62
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/AccGraph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAccGraph:Lcom/sec/android/app/hwmoduletest/view/AccGraph;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
    .param p1, "x1"    # F

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxSensorValue:F

    return p1
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsProxMotorTest:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->FEED_BACK_PATTERN:[J

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->readToAccelerometerSensor()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->updateUI()V

    return-void
.end method

.method static synthetic access$902(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
    .param p1, "x1"    # [F

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensorValues:[F

    return-object p1
.end method

.method private readToAccelerometerSensor()V
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4090000000000000L    # 1024.0

    const-wide v10, 0x40239eb851eb851fL    # 9.81

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 229
    :try_start_0
    const-string v2, "ACCEL_SENSOR_RAW"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "rawDatas":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v3

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v3

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    const/4 v3, 0x2

    const/4 v4, 0x2

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    .end local v1    # "rawDatas":[Ljava/lang/String;
    :goto_0
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensorValues:[F

    aget v3, v3, v6

    float-to-double v4, v3

    mul-double/2addr v4, v12

    div-double/2addr v4, v10

    double-to-int v3, v4

    aput v3, v2, v6

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensorValues:[F

    aget v3, v3, v7

    float-to-double v4, v3

    mul-double/2addr v4, v12

    div-double/2addr v4, v10

    double-to-int v3, v4

    aput v3, v2, v7

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensorValues:[F

    aget v3, v3, v8

    float-to-double v4, v3

    mul-double/2addr v4, v12

    div-double/2addr v4, v10

    double-to-int v3, v4

    aput v3, v2, v8

    goto :goto_0
.end method

.method private sensorRegister()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "sensorRegister"

    const-string v2, "ACC_SENSOR"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mVibrator:Landroid/os/SystemVibrator;

    .line 205
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensor:Landroid/hardware/Sensor;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProximitySensor:Landroid/hardware/Sensor;

    .line 208
    const-string v0, "SENSOR_NAME_ACCELEROMETER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAccelerometer_name:Ljava/lang/String;

    .line 209
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProximitySensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 212
    return-void
.end method

.method private sensorUnregister()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "sensorUnregister"

    const-string v2, "ACC_SENSOR"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 221
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 222
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;

    .line 223
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensor:Landroid/hardware/Sensor;

    .line 224
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProximitySensor:Landroid/hardware/Sensor;

    .line 225
    return-void
.end method

.method private declared-synchronized updateUI()V
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x1

    .line 243
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxSensorValue:F

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxStatus:I

    .line 245
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mLogCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mLogCount:I

    .line 246
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mLogCount:I

    if-ne v0, v1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, " updateProx "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mProxStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, " updateAcc "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " y: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " z: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mLogCount:I

    .line 252
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z

    if-nez v0, :cond_3

    .line 253
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxStatus:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    if-ne v0, v5, :cond_2

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z

    .line 266
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    monitor-exit p0

    return-void

    .line 257
    :cond_3
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z

    if-ne v0, v5, :cond_2

    .line 258
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxStatus:I

    if-eqz v0, :cond_2

    .line 259
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    if-nez v0, :cond_2

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 100
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->setContentView(I)V

    .line 101
    const v0, 0x7f0b000a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAccGraph:Lcom/sec/android/app/hwmoduletest/view/AccGraph;

    .line 102
    const v0, 0x7f0b000b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;

    .line 103
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;

    const-string v1, "LPF Status : ON"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->LPFButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 184
    const-string v0, "MOTOR_LPF"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 185
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 189
    const/16 v1, 0x19

    if-ne p1, v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyUp"

    const-string v3, "KEYCODE_VOLUME_DOWN"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z

    if-nez v1, :cond_0

    .line 192
    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    .line 196
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyUp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsDownKeyVib: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :goto_1
    return v0

    .line 194
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsDownKeyVib:Z

    goto :goto_0

    .line 199
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 178
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 179
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 174
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 175
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 7
    .param p1, "hasFocus"    # Z

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0xb

    const/16 v4, 0xa

    .line 130
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onWindowFocusChanged(Z)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onWindowFocusChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "focused : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    if-eqz p1, :cond_1

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->sensorRegister()V

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsVibrate:Z

    .line 138
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsProxMotorTest:Z

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 142
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mTimer:Ljava/util/Timer;

    .line 143
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$3;-><init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->task:Ljava/util/TimerTask;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->task:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 171
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->sensorUnregister()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 165
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    goto :goto_0
.end method
