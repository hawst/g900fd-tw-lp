.class public Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TspPatternStyleX.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TspPatternStyleX"


# instance fields
.field private HEIGHT_BASIS:I

.field private HEIGHT_BASIS_CROSS:I

.field protected KEY_TIMEOUT:I

.field protected KEY_TIMER_EXPIRED:I

.field protected MILLIS_IN_SEC:I

.field private WIDTH_BASIS:I

.field private WIDTH_BASIS_CROSS:I

.field private click:[[Z

.field private dialog_showing:Z

.field private draw:[[Z

.field private drawCross:[Z

.field private isDrawArea:[[Z

.field private isHovering:Z

.field private mBottommostOfMatrix:I

.field private mCenterOfHorizontalOfMatrix:I

.field private mCenterOfVerticalOfMatrix:I

.field private mLeftmostOfMatrix:I

.field private mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

.field private mRightmostOfMatrix:I

.field private mTopmostOfMatrix:I

.field private needFailPopupDispaly:Z

.field private remoteCall:Z

.field private successTest:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x13

    const/16 v2, 0xb

    const/4 v1, 0x0

    .line 67
    const-string v0, "TspPatternStyleX"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 38
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    .line 39
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    .line 40
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS_CROSS:I

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->remoteCall:Z

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->successTest:Z

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->needFailPopupDispaly:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->dialog_showing:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isPassCross()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->successTest:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->successTest:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->remoteCall:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->click:[[Z

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z

    return-object v0
.end method

.method private decideRemote()V
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 169
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RemoteCall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->remoteCall:Z

    .line 170
    return-void
.end method

.method private fillUpMatrix()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 173
    const/4 v1, 0x0

    .local v1, "row":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    if-ge v1, v2, :cond_3

    .line 174
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    if-ge v0, v2, :cond_2

    .line 175
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isNeededCheck_Hovering(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z

    aget-object v2, v2, v1

    aput-boolean v4, v2, v0

    .line 174
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    if-nez v2, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isNeededCheck(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z

    aget-object v2, v2, v1

    aput-boolean v4, v2, v0

    goto :goto_2

    .line 180
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z

    aget-object v2, v2, v1

    const/4 v3, 0x0

    aput-boolean v3, v2, v0

    goto :goto_2

    .line 173
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "column":I
    :cond_3
    return-void
.end method

.method private isNeededCheck(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mTopmostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mBottommostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNeededCheck_Hovering(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 192
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mTopmostOfMatrix:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_2

    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mBottommostOfMatrix:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mLeftmostOfMatrix:I

    add-int/lit8 v0, v0, 0x1

    if-eq p2, v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mRightmostOfMatrix:I

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPass()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 198
    const/4 v1, 0x1

    .line 200
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    if-ge v0, v4, :cond_3

    .line 201
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    if-ge v2, v4, :cond_2

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-ne v4, v3, :cond_0

    .line 203
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_1

    move v1, v3

    .line 201
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 203
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 200
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    .end local v2    # "j":I
    :cond_3
    return v1
.end method

.method private isPassCross()Z
    .locals 3

    .prologue
    .line 211
    const/4 v1, 0x1

    .line 213
    .local v1, "isPassCross":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_1

    .line 214
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 213
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 217
    :cond_1
    return v1
.end method

.method private setTSP()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    const-string v0, "TSP_X_AXIS_CHANNEL"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    .line 72
    const-string v0, "TSP_Y_AXIS_CHANNEL"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    .line 73
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x2

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I

    .line 74
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS_CROSS:I

    .line 75
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->click:[[Z

    .line 76
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z

    .line 77
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z

    .line 78
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z

    .line 79
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mTopmostOfMatrix:I

    .line 80
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mBottommostOfMatrix:I

    .line 81
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mCenterOfVerticalOfMatrix:I

    .line 82
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mLeftmostOfMatrix:I

    .line 83
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mRightmostOfMatrix:I

    .line 84
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mCenterOfHorizontalOfMatrix:I

    .line 85
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->KEY_TIMER_EXPIRED:I

    .line 86
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->MILLIS_IN_SEC:I

    .line 87
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->KEY_TIMEOUT:I

    .line 88
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const-string v1, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const v1, 0x7f030045

    invoke-static {p0, v1}, Lcom/sec/android/app/dummy/SlookCocktailSubWindow;->setSubContentView(Landroid/app/Activity;I)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate:setSubContentView"

    const-string v3, "ON"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "isHovering"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    .line 101
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TEST_TSP_SELF = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "TEST_TSP_SELF"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "TEST_TSP_SELF"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->successTest:Z

    .line 106
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->needFailPopupDispaly:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->setTSP()V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->decideRemote()V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "TouchTest is created"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    new-instance v1, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->setContentView(Landroid/view/View;)V

    .line 117
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->fillUpMatrix()V

    .line 118
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 148
    packed-switch p1, :pswitch_data_0

    .line 164
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 151
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isPass()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isPassCross()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->setResult(I)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->finish()V

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 135
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, "DisableHovering"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->hoveringOnOFF(Ljava/lang/String;)V

    .line 141
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->unregisterBroadcastReceiver(Landroid/content/Context;)V

    .line 143
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "EnableHovering"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->hoveringOnOFF(Ljava/lang/String;)V

    .line 129
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->registerBroadcastReceiver(Landroid/content/Context;)V

    .line 131
    :cond_0
    return-void
.end method
