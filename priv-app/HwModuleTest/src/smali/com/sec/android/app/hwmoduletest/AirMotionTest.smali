.class public Lcom/sec/android/app/hwmoduletest/AirMotionTest;
.super Landroid/app/Activity;
.source "AirMotionTest.java"


# static fields
.field private static final AIRMOTION_DEFAULT:I = 0x0

.field private static final AIRMOTION_HORIZONTAL:I = 0x1

.field private static final AIRMOTION_VERTICAL:I = 0x2

.field private static final MODE_SENSOR_HUB:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AirMotionTest"

.field private static final TYPE_AIRMOTION:I = 0x2

.field private static gesture_count_D:I

.field private static gesture_count_L:I

.field private static gesture_count_R:I

.field private static gesture_count_U:I

.field private static mCheckbox_logging:Landroid/widget/CheckBox;

.field private static mDirectionMode:I

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mGesture_count_direction:Landroid/widget/TextView;

.field private static mGesture_count_title:Landroid/widget/TextView;

.field private static mIsTesting:Z

.field private static mLoggingOn:Z

.field private static mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private static final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private static mSensorName:Ljava/lang/String;

.field private static mSpin:Landroid/widget/Spinner;

.field private static mTestMode:I

.field private static mTv_Item_1:Landroid/widget/TextView;

.field private static mTv_Item_2:Landroid/widget/TextView;

.field private static mTv_Item_3:Landroid/widget/TextView;

.field private static mTv_Item_4:Landroid/widget/TextView;

.field private static mTv_Item_5:Landroid/widget/TextView;

.field private static mTv_Item_6:Landroid/widget/TextView;

.field private static mTv_Item_7:Landroid/widget/TextView;

.field private static mTv_Item_8:Landroid/widget/TextView;

.field private static mTv_Item_9:Landroid/widget/TextView;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBtn_start:Landroid/widget/ToggleButton;

.field private mPowerManager:Landroid/os/PowerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    sput v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTestMode:I

    .line 31
    sput v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    .line 32
    sput v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    sput v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    sput v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    sput v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    .line 33
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 34
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 36
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 38
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    .line 39
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    .line 40
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    .line 41
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    .line 42
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    .line 43
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    .line 44
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    .line 45
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_7:Landroid/widget/TextView;

    .line 46
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_8:Landroid/widget/TextView;

    .line 47
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_9:Landroid/widget/TextView;

    .line 48
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_title:Landroid/widget/TextView;

    .line 49
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_direction:Landroid/widget/TextView;

    .line 51
    sput-boolean v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z

    .line 52
    sput-boolean v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z

    .line 53
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSpin:Landroid/widget/Spinner;

    .line 54
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    .line 55
    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 181
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest$4;

    invoke-direct {v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$4;-><init>()V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mPowerManager:Landroid/os/PowerManager;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mBtn_start:Landroid/widget/ToggleButton;

    return-void
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 23
    sput p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    return p0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->createArrayList()V

    return-void
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 23
    sput-boolean p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/AirMotionTest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AirMotionTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->removeFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 23
    sput p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTestMode:I

    return p0
.end method

.method static synthetic access$200()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mBtn_start:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->startAirmotionTest()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->stopAirmotionTest()V

    return-void
.end method

.method static synthetic access$600()Z
    .locals 1

    .prologue
    .line 23
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z

    return v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 23
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->setTextViewAndLogAsMAX88922(Lcom/samsung/android/sensorhub/SensorHubEvent;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 23
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->setTextViewAndLogAsTMG3992(Lcom/samsung/android/sensorhub/SensorHubEvent;)V

    return-void
.end method

.method private changeTestMode()V
    .locals 5

    .prologue
    .line 338
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v4, "SensorHub"

    aput-object v4, v2, v3

    .line 341
    .local v2, "items":[Ljava/lang/CharSequence;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 342
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const-string v3, "Select Test Mode"

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 343
    sget v3, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTestMode:I

    new-instance v4, Lcom/sec/android/app/hwmoduletest/AirMotionTest$7;

    invoke-direct {v4, p0, v2}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$7;-><init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 349
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 350
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 351
    return-void
.end method

.method private createArrayList()V
    .locals 7

    .prologue
    .line 303
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mArrayList:Ljava/util/ArrayList;

    .line 304
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mArrayList:Ljava/util/ArrayList;

    const-string v6, "Select All"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    const-string v4, "/storage/sdcard0/"

    .line 306
    .local v4, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 307
    .local v1, "dir":Ljava/io/File;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 308
    .local v2, "filename":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "children":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 311
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_1

    .line 312
    aget-object v2, v0, v3

    .line 314
    const-string v5, ".txt"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 315
    const-string v5, "AirMotion"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 316
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 322
    .end local v3    # "i":I
    :cond_1
    const-string v5, "AirMotionTest"

    const-string v6, "createArrayList()"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return-void
.end method

.method private deleteLogFiles()V
    .locals 5

    .prologue
    .line 225
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 226
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v2, "Gesture sensor data is stored in \'/storage/sdcard0/\'.\nDo you want to delete log files?"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Yes"

    new-instance v4, Lcom/sec/android/app/hwmoduletest/AirMotionTest$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$6;-><init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "No"

    new-instance v4, Lcom/sec/android/app/hwmoduletest/AirMotionTest$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$5;-><init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 297
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 298
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 299
    const-string v2, "AirMotionTest"

    const-string v3, "deleteLogFiles()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    return-void
.end method

.method private initUIAsMAX88922()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 354
    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    .line 355
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    const v0, 0x7f0b001b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    .line 357
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    const v1, 0x7f080206

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 358
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_title:Landroid/widget/TextView;

    .line 359
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_title:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 360
    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_direction:Landroid/widget/TextView;

    .line 361
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_direction:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 362
    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    .line 363
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    const v1, 0x7f080207

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 364
    const v0, 0x7f0b0020

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    .line 365
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    const v1, 0x7f080208

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 366
    const v0, 0x7f0b0021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    .line 367
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    const v1, 0x7f080209

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 368
    const v0, 0x7f0b0022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    .line 369
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    const v1, 0x7f08020a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 370
    const v0, 0x7f0b0023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_7:Landroid/widget/TextView;

    .line 371
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_7:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    const v0, 0x7f0b0024

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_8:Landroid/widget/TextView;

    .line 373
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_8:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 374
    const v0, 0x7f0b0025

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_9:Landroid/widget/TextView;

    .line 375
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_9:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    return-void
.end method

.method private initUIAsTMG3992()V
    .locals 2

    .prologue
    .line 379
    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    .line 380
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    const v0, 0x7f0b001b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    .line 382
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    const v1, 0x7f080206

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 383
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_title:Landroid/widget/TextView;

    .line 384
    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_direction:Landroid/widget/TextView;

    .line 385
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_direction:Landroid/widget/TextView;

    const-string v1, "R:    L:    U:    D:"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 386
    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    .line 387
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    const v1, 0x7f08020e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 388
    const v0, 0x7f0b0020

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    .line 389
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    const v1, 0x7f08020f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 390
    const v0, 0x7f0b0021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    .line 391
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    const v1, 0x7f080210

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 392
    const v0, 0x7f0b0022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    .line 393
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 394
    const v0, 0x7f0b0023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_7:Landroid/widget/TextView;

    .line 395
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_7:Landroid/widget/TextView;

    const v1, 0x7f080212

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 396
    const v0, 0x7f0b0024

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_8:Landroid/widget/TextView;

    .line 397
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_8:Landroid/widget/TextView;

    const v1, 0x7f080213

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 398
    const v0, 0x7f0b0025

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_9:Landroid/widget/TextView;

    .line 399
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_9:Landroid/widget/TextView;

    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 400
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    .line 401
    return-void
.end method

.method private removeFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 326
    const-string v1, "AirMotion"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/storage/sdcard0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 329
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 334
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    const-string v1, "AirMotionTest"

    const-string v2, "removeFile()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void
.end method

.method private static setTextViewAndLogAsMAX88922(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 12
    .param p0, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x2

    .line 404
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 405
    .local v4, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 406
    .local v2, "miliTime":J
    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 407
    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1}, Ljava/util/Formatter;-><init>()V

    .line 408
    .local v1, "form":Ljava/util/Formatter;
    const-string v5, "%02d:%02d:%02d"

    new-array v6, v11, [Ljava/lang/Object;

    iget v7, v4, Landroid/text/format/Time;->hour:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, v4, Landroid/text/format/Time;->minute:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    iget v7, v4, Landroid/text/format/Time;->second:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v1, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 409
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuffer;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 411
    const/4 v0, 0x0

    .line 412
    .local v0, "directionStr":Ljava/lang/String;
    sget v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    if-ne v5, v10, :cond_4

    .line 413
    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v9

    float-to-int v5, v5

    if-lez v5, :cond_3

    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v9

    float-to-int v5, v5

    const/16 v6, 0xb4

    if-ge v5, v6, :cond_3

    .line 414
    const-string v0, "Right"

    .line 433
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 434
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    :cond_1
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuffer;

    const-string v7, "Peak To Peak : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v7, v7, v10

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 437
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuffer;

    const-string v7, "Angle : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v7, v7, v9

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuffer;

    const-string v7, "Valid_cnt : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuffer;

    const-string v7, "Zmax_delta : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    sget-boolean v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z

    if-eqz v5, :cond_2

    .line 442
    sget-object v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    iget-object v6, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v6, v6, v9

    float-to-int v6, v6

    invoke-virtual {v5, v0, v6}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(Ljava/lang/String;I)V

    .line 444
    :cond_2
    const-string v5, "AirMotionTest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SensorHubListener : direction = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", angle = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v7, v7, v9

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    return-void

    .line 416
    :cond_3
    const-string v0, "Left"

    goto/16 :goto_0

    .line 418
    :cond_4
    sget v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    if-ne v5, v9, :cond_6

    .line 419
    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v9

    float-to-int v5, v5

    const/16 v6, 0x5a

    if-le v5, v6, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v9

    float-to-int v5, v5

    const/16 v6, 0x10e

    if-ge v5, v6, :cond_5

    .line 420
    const-string v0, "Up"

    goto/16 :goto_0

    .line 422
    :cond_5
    const-string v0, "Down"

    goto/16 :goto_0

    .line 424
    :cond_6
    sget v5, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    if-nez v5, :cond_0

    .line 425
    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v8

    float-to-int v5, v5

    int-to-char v5, v5

    const/16 v6, 0x4c

    if-ne v5, v6, :cond_7

    const-string v0, "UP"

    :goto_1
    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v8

    float-to-int v5, v5

    int-to-char v5, v5

    const/16 v6, 0x52

    if-ne v5, v6, :cond_8

    const-string v0, "Down"

    goto :goto_1

    :cond_8
    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v8

    float-to-int v5, v5

    int-to-char v5, v5

    const/16 v6, 0x44

    if-ne v5, v6, :cond_9

    const-string v0, "Left"

    goto :goto_1

    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v5, v5, v8

    float-to-int v5, v5

    int-to-char v5, v5

    const/16 v6, 0x55

    if-ne v5, v6, :cond_a

    const-string v0, "Right"

    goto :goto_1

    :cond_a
    const-string v0, "No Direction"

    goto :goto_1
.end method

.method private static setTextViewAndLogAsTMG3992(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 15
    .param p0, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 448
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11}, Landroid/text/format/Time;-><init>()V

    .line 449
    .local v11, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 450
    .local v12, "miliTime":J
    invoke-virtual {v11, v12, v13}, Landroid/text/format/Time;->set(J)V

    .line 451
    new-instance v10, Ljava/util/Formatter;

    invoke-direct {v10}, Ljava/util/Formatter;-><init>()V

    .line 452
    .local v10, "form":Ljava/util/Formatter;
    const-string v0, "%02d:%02d:%02d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v11, Landroid/text/format/Time;->hour:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, v11, Landroid/text/format/Time;->minute:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, v11, Landroid/text/format/Time;->second:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v10, v0, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 453
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 455
    const/4 v1, 0x0

    .line 456
    .local v1, "directionStr":Ljava/lang/String;
    const-string v9, "R L U D"

    .line 457
    .local v9, "gesture_count":Ljava/lang/String;
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 458
    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x2

    aget v0, v0, v2

    float-to-int v0, v0

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x2

    aget v0, v0, v2

    float-to-int v0, v0

    const/16 v2, 0xb4

    if-gt v0, v2, :cond_3

    .line 459
    const-string v1, "Right"

    .line 460
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    .line 498
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGesture_count_direction:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "R:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   L:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   U:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   D:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 500
    if-eqz v1, :cond_1

    .line 501
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_3:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Cross_Angle : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 504
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_4:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Count : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_5:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Max.Sum_NSWE : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_6:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Enter_Angle : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_7:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Exit_Angle : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 508
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_8:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Enter_Mag : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 509
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTv_Item_9:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Exit_Mag : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z

    if-eqz v0, :cond_2

    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "L"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "U"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "D"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 512
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    iget-object v2, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    float-to-int v5, v5

    iget-object v6, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    float-to-int v6, v6

    iget-object v7, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v8, 0x5

    aget v7, v7, v8

    float-to-int v7, v7

    iget-object v8, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v14, 0x6

    aget v8, v8, v14

    float-to-int v8, v8

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(Ljava/lang/String;IIIIIIILjava/lang/String;)V

    .line 515
    :cond_2
    const-string v0, "AirMotionTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SensorHubListener : direction = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", angle(Cross_Angle) = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    return-void

    .line 462
    :cond_3
    const-string v1, "Left"

    .line 463
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    goto/16 :goto_0

    .line 465
    :cond_4
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_6

    .line 466
    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x2

    aget v0, v0, v2

    float-to-int v0, v0

    const/16 v2, 0x5a

    if-lt v0, v2, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x2

    aget v0, v0, v2

    float-to-int v0, v0

    const/16 v2, 0x10e

    if-gt v0, v2, :cond_5

    .line 467
    const-string v1, "Up"

    .line 468
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    goto/16 :goto_0

    .line 470
    :cond_5
    const-string v1, "Down"

    .line 471
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    goto/16 :goto_0

    .line 473
    :cond_6
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    if-nez v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    float-to-int v0, v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_7

    .line 475
    const-string v1, "Up"

    .line 476
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_U:I

    goto/16 :goto_0

    .line 477
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    float-to-int v0, v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_8

    .line 478
    const-string v1, "Down"

    .line 479
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_D:I

    goto/16 :goto_0

    .line 480
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    float-to-int v0, v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_9

    .line 481
    const-string v1, "Left"

    .line 482
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_L:I

    goto/16 :goto_0

    .line 483
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    float-to-int v0, v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_a

    .line 484
    const-string v1, "Right"

    .line 485
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->gesture_count_R:I

    goto/16 :goto_0

    .line 487
    :cond_a
    const-string v1, "No Direction"

    goto/16 :goto_0
.end method

.method private startAirmotionTest()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 133
    const-string v0, "AirMotionTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTestMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTestMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mDirectionMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mDirectionMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    sput-boolean v4, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z

    .line 135
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 136
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSpin:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 138
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTestMode:I

    if-nez v0, :cond_0

    .line 139
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    sget-object v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 144
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z

    if-eqz v0, :cond_1

    .line 145
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-direct {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;-><init>()V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 148
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 149
    const-string v0, "AirMotionTest"

    const-string v1, "startAirmotionTest()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    return-void
.end method

.method private stopAirmotionTest()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 153
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z

    if-eqz v0, :cond_0

    .line 154
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z

    .line 155
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 156
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSpin:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 158
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    const-string v1, "MAX88922"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->initUIAsMAX88922()V

    .line 166
    :cond_0
    :goto_0
    sget v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mTestMode:I

    if-nez v0, :cond_1

    .line 167
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    sget-object v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 170
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v0, :cond_2

    .line 171
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 174
    :cond_2
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 175
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 178
    :cond_3
    const-string v0, "AirMotionTest"

    const-string v1, "stopAirmotionTest()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    return-void

    .line 161
    :cond_4
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    const-string v1, "TMG3992"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->initUIAsTMG3992()V

    goto :goto_0
.end method


# virtual methods
.method public initialize()V
    .locals 4

    .prologue
    .line 79
    const-string v0, "sensorhub"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 80
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 82
    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSpin:Landroid/widget/Spinner;

    .line 83
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSpin:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 95
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gesture:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    .line 96
    const-string v0, "AirMotionTest"

    const-string v1, "initialize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    const-string v1, "MAX88922"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->initUIAsMAX88922()V

    .line 105
    :cond_0
    :goto_0
    const v0, 0x7f0b0019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    .line 106
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 107
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mBtn_start:Landroid/widget/ToggleButton;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mBtn_start:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/AirMotionTest$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest$3;-><init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    return-void

    .line 100
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;

    const-string v1, "TMG3992"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->initUIAsTMG3992()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->setContentView(I)V

    .line 64
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mPowerManager:Landroid/os/PowerManager;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mPowerManager:Landroid/os/PowerManager;

    const/16 v1, 0x1a

    const-string v2, "Gesture Sensor"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x200000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->initialize()V

    .line 69
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 198
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->stopAirmotionTest()V

    .line 75
    const-string v0, "AirMotionTest"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 203
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 211
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 205
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->deleteLogFiles()V

    goto :goto_0

    .line 208
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->changeTestMode()V

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b02e9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 217
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z

    if-eqz v0, :cond_0

    .line 218
    const/4 v0, 0x0

    .line 220
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
