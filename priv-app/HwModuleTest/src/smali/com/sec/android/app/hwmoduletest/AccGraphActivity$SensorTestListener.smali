.class Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;
.super Ljava/lang/Object;
.source "AccGraphActivity.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 272
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 274
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 282
    :goto_0
    return-void

    .line 276
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAcceSensorValues:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$902(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;[F)[F

    goto :goto_0

    .line 279
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mProxSensorValue:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$1002(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;F)F

    goto :goto_0

    .line 274
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method
