.class public Lcom/sec/android/app/hwmoduletest/support/LtUtil;
.super Ljava/lang/Object;
.source "LtUtil.java"


# static fields
.field private static final LOG_DISABLED:Z = false

.field private static final LOG_ENABLED:Z = true

.field private static final LOG_TYPE_D:I = 0x0

.field private static final LOG_TYPE_E:I = 0x1

.field private static final LOG_TYPE_I:I = 0x2

.field private static final LOG_TYPE_V:I = 0x3

.field private static final LOG_TYPE_W:I = 0x4

.field private static final LOG_TYPE_WTF:I = 0x5

.field private static counter:I

.field private static mEnableLogs:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->mEnableLogs:Z

    .line 68
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->counter:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DisableLogs()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->mEnableLogs:Z

    .line 76
    return-void
.end method

.method public static EnableLogs()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->mEnableLogs:Z

    .line 72
    return-void
.end method

.method public static convertDpFromPixel(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pixel"    # I

    .prologue
    .line 39
    const/high16 v0, 0x40000000    # 2.0f

    .line 40
    .local v0, "DEFAULT_HDIP_DENSITY_SCALE":F
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    .line 41
    .local v1, "scale":F
    int-to-float v2, p1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    mul-float/2addr v2, v1

    float-to-int v2, v2

    return v2
.end method

.method public static convertPixelFromDp(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # I

    .prologue
    .line 45
    const/high16 v0, 0x40000000    # 2.0f

    .line 46
    .local v0, "DEFAULT_HDIP_DENSITY_SCALE":F
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    .line 47
    .local v1, "scale":F
    int-to-float v2, p1

    div-float/2addr v2, v1

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    return v2
.end method

.method public static getStateLogs()Z
    .locals 1

    .prologue
    .line 79
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->mEnableLogs:Z

    return v0
.end method

.method private static log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "type"    # I
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->getStateLogs()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    if-eq p0, v2, :cond_1

    .line 84
    sget v2, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->counter:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->counter:I

    .line 86
    sget v2, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->counter:I

    const/16 v3, 0xa

    if-le v2, v3, :cond_0

    .line 87
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->counter:I

    .line 88
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->EnableLogs()V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    const-string v0, "HwModuleTest"

    .line 97
    .local v0, "TAG":Ljava/lang/String;
    if-eqz p1, :cond_2

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 98
    :cond_2
    const-string p1, " "

    .line 102
    :cond_3
    if-eqz p2, :cond_4

    const-string v2, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 103
    :cond_4
    const-string p2, " "

    .line 107
    :cond_5
    if-eqz p3, :cond_6

    const-string v2, ""

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 108
    :cond_6
    const-string p3, " "

    .line 111
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "$"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "str":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 115
    :pswitch_0
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 118
    :pswitch_1
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :pswitch_3
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 127
    :pswitch_4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 130
    :pswitch_5
    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "className"    # Ljava/lang/String;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-static {v0, p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public static log_e(Ljava/lang/Exception;)V
    .locals 5
    .param p0, "e"    # Ljava/lang/Exception;

    .prologue
    .line 159
    const-string v0, "HwModuleTest"

    .line 160
    .local v0, "TAG":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 161
    .local v2, "stackTraceElements":[Ljava/lang/StackTraceElement;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WARNNING: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 164
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WARNNING:     "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    :cond_0
    return-void
.end method

.method public static log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "className"    # Ljava/lang/String;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-static {v0, p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public static log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "className"    # Ljava/lang/String;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 146
    const/4 v0, 0x2

    invoke-static {v0, p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public static log_v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "className"    # Ljava/lang/String;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 150
    const/4 v0, 0x3

    invoke-static {v0, p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public static log_wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "className"    # Ljava/lang/String;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 154
    const/4 v0, 0x5

    invoke-static {v0, p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public static setRemoveStatusBar(Landroid/view/Window;)V
    .locals 2
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 202
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 203
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 205
    invoke-virtual {p0, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 206
    return-void
.end method

.method public static setRemoveSystemUI(Landroid/view/Window;Z)V
    .locals 6
    .param p0, "window"    # Landroid/view/Window;
    .param p1, "remove"    # Z

    .prologue
    .line 172
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 173
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    const/4 v3, 0x0

    .line 176
    .local v3, "systemUiVisibility":I
    :try_start_0
    const-class v4, Landroid/view/View;

    const-string v5, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 178
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_1

    .line 179
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 187
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    if-eqz p1, :cond_2

    .line 189
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    or-int/2addr v4, v3

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 197
    :cond_0
    :goto_1
    invoke-virtual {p0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 198
    return-void

    .line 181
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :cond_1
    const/4 v3, 0x2

    goto :goto_0

    .line 183
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 192
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    and-int/2addr v4, v3

    if-eqz v4, :cond_0

    .line 193
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    xor-int/2addr v4, v3

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    goto :goto_1
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 3
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 20
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 25
    .local v1, "wm":Landroid/view/IWindowManager;
    if-eqz v1, :cond_0

    .line 26
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static sleep(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 35
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 36
    return-void
.end method
