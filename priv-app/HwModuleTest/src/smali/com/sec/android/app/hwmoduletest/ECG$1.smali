.class Lcom/sec/android/app/hwmoduletest/ECG$1;
.super Landroid/content/BroadcastReceiver;
.source "ECG.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/ECG;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/ECG;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/ECG;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 90
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 92
    .local v4, "action":Ljava/lang/String;
    const-string v27, "com.sec.factory.app.factorytest.ECG_DATA"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 93
    const-string v27, "ecg_delta"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 94
    .local v5, "ecg_delta":Ljava/lang/String;
    const-string v27, "ecg_delta_rawData"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 95
    .local v7, "ecg_delta_rawData":Ljava/lang/String;
    const-string v27, "ecg_delta_rawData_mV"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 96
    .local v8, "ecg_delta_rawData_mV":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/ECG;->access$000()Ljava/lang/String;

    move-result-object v27

    const-string v28, "onReceive"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "ecg_delta : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/ECG;->access$000()Ljava/lang/String;

    move-result-object v27

    const-string v28, "onReceive"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "ecg_delta_rawData : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/ECG;->access$000()Ljava/lang/String;

    move-result-object v27

    const-string v28, "onReceive"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "ecg_delta_rawData_mV : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$100(Lcom/sec/android/app/hwmoduletest/ECG;)Ljava/text/DecimalFormat;

    move-result-object v27

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-virtual/range {v27 .. v29}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v11

    .line 101
    .local v11, "ecg_delta_transformed":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 103
    .local v6, "ecg_delta_float":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mFormat2:Ljava/text/DecimalFormat;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$200(Lcom/sec/android/app/hwmoduletest/ECG;)Ljava/text/DecimalFormat;

    move-result-object v27

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-virtual/range {v27 .. v29}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    .line 104
    .local v10, "ecg_delta_rawData_mV_transformed":Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    .line 106
    .local v9, "ecg_delta_rawData_mV_float":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "D_range"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 107
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 109
    const-string v27, "ECG_DYNAMIC_RANGE_MIN"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v15

    .line 110
    .local v15, "mECG_DR_Spec_Min":F
    const-string v27, "ECG_DYNAMIC_RANGE_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v14

    .line 112
    .local v14, "mECG_DR_Spec_Max":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Measuring have been completed"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    cmpl-float v27, v6, v15

    if-ltz v27, :cond_2

    cmpg-float v27, v6, v14

    if-gtz v27, :cond_2

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    move/from16 v27, v0

    if-eqz v27, :cond_0

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    .line 127
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const-string v28, "READY"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    .line 300
    .end local v5    # "ecg_delta":Ljava/lang/String;
    .end local v6    # "ecg_delta_float":F
    .end local v7    # "ecg_delta_rawData":Ljava/lang/String;
    .end local v8    # "ecg_delta_rawData_mV":Ljava/lang/String;
    .end local v9    # "ecg_delta_rawData_mV_float":F
    .end local v10    # "ecg_delta_rawData_mV_transformed":Ljava/lang/String;
    .end local v11    # "ecg_delta_transformed":Ljava/lang/String;
    .end local v14    # "mECG_DR_Spec_Max":F
    .end local v15    # "mECG_DR_Spec_Min":F
    :cond_1
    :goto_1
    return-void

    .line 119
    .restart local v5    # "ecg_delta":Ljava/lang/String;
    .restart local v6    # "ecg_delta_float":F
    .restart local v7    # "ecg_delta_rawData":Ljava/lang/String;
    .restart local v8    # "ecg_delta_rawData_mV":Ljava/lang/String;
    .restart local v9    # "ecg_delta_rawData_mV_float":F
    .restart local v10    # "ecg_delta_rawData_mV_transformed":Ljava/lang/String;
    .restart local v11    # "ecg_delta_transformed":Ljava/lang/String;
    .restart local v14    # "mECG_DR_Spec_Max":F
    .restart local v15    # "mECG_DR_Spec_Min":F
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto :goto_0

    .line 129
    .end local v14    # "mECG_DR_Spec_Max":F
    .end local v15    # "mECG_DR_Spec_Min":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "Frequency"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 130
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 132
    const-string v27, "ECG_FREQUENCY_MIN"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v17

    .line 133
    .local v17, "mECG_FQ_Spec_Min":F
    const-string v27, "ECG_FREQUENCY_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v16

    .line 135
    .local v16, "mECG_FQ_Spec_Max":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Measuring have been completed"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    cmpl-float v27, v6, v17

    if-ltz v27, :cond_5

    cmpg-float v27, v6, v16

    if-gtz v27, :cond_5

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    move/from16 v27, v0

    if-eqz v27, :cond_4

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    .line 151
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const-string v28, "READY"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    goto/16 :goto_1

    .line 142
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto :goto_2

    .line 152
    .end local v16    # "mECG_FQ_Spec_Max":F
    .end local v17    # "mECG_FQ_Spec_Min":F
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "System noise"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 153
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 155
    const-string v27, "ECG_SYSTEM_NOISE_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v26

    .line 157
    .local v26, "mECG_SN_Spec_Max":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Measuring have been completed"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    cmpg-float v27, v9, v26

    if-gtz v27, :cond_8

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    move/from16 v27, v0

    if-eqz v27, :cond_7

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    .line 173
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const-string v28, "READY"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    goto/16 :goto_1

    .line 164
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto :goto_3

    .line 174
    .end local v26    # "mECG_SN_Spec_Max":F
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "Impedance_B"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 175
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$100(Lcom/sec/android/app/hwmoduletest/ECG;)Ljava/text/DecimalFormat;

    move-result-object v28

    const v29, 0x3d71a9fc    # 0.059f

    mul-float v29, v29, v6

    sub-float v29, v6, v29

    move/from16 v0, v29

    float-to-double v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v28

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Min:F
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$902(Lcom/sec/android/app/hwmoduletest/ECG;F)F

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$100(Lcom/sec/android/app/hwmoduletest/ECG;)Ljava/text/DecimalFormat;

    move-result-object v28

    const v29, 0x3d71a9fc    # 0.059f

    mul-float v29, v29, v6

    add-float v29, v29, v6

    move/from16 v0, v29

    float-to-double v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v28

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Max:F
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$1002(Lcom/sec/android/app/hwmoduletest/ECG;F)F

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Measuring have been completed"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x1000000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "SPEC : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Min:F
    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/hwmoduletest/ECG;->access$900(Lcom/sec/android/app/hwmoduletest/ECG;)F

    move-result v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "~"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Max:F
    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/hwmoduletest/ECG;->access$1000(Lcom/sec/android/app/hwmoduletest/ECG;)F

    move-result v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    move/from16 v27, v0

    if-eqz v27, :cond_a

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    .line 191
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const-string v28, "READY"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    goto/16 :goto_1

    .line 192
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "Impedance_A"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 193
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Measuring have been completed"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Min:F
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$900(Lcom/sec/android/app/hwmoduletest/ECG;)F

    move-result v27

    cmpl-float v27, v6, v27

    if-ltz v27, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Max:F
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$1000(Lcom/sec/android/app/hwmoduletest/ECG;)F

    move-result v27

    cmpg-float v27, v6, v27

    if-gtz v27, :cond_d

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    move/from16 v27, v0

    if-eqz v27, :cond_c

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    .line 211
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const-string v28, "READY"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    goto/16 :goto_1

    .line 202
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto :goto_4

    .line 215
    .end local v5    # "ecg_delta":Ljava/lang/String;
    .end local v6    # "ecg_delta_float":F
    .end local v7    # "ecg_delta_rawData":Ljava/lang/String;
    .end local v8    # "ecg_delta_rawData_mV":Ljava/lang/String;
    .end local v9    # "ecg_delta_rawData_mV_float":F
    .end local v10    # "ecg_delta_rawData_mV_transformed":Ljava/lang/String;
    .end local v11    # "ecg_delta_transformed":Ljava/lang/String;
    :cond_e
    const-string v27, "com.sec.factory.app.factorytest.HEART_RATE"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_18

    .line 216
    const-string v27, "heart_rate"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 217
    .local v12, "heart_rate":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/ECG;->access$000()Ljava/lang/String;

    move-result-object v27

    const-string v28, "onReceive"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "heart_rate : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 221
    .local v13, "heat_rate_decimal":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "HR"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "WAIT"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 222
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "HR(50bpm)"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 225
    const-string v27, "ECG_HR_50_MIN"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v23

    .line 226
    .local v23, "mECG_HR50_Spec_Min":I
    const-string v27, "ECG_HR_50_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 228
    .local v22, "mECG_HR50_Spec_Max":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    move/from16 v0, v23

    if-lt v13, v0, :cond_11

    move/from16 v0, v22

    if-gt v13, v0, :cond_11

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    .end local v22    # "mECG_HR50_Spec_Max":I
    .end local v23    # "mECG_HR50_Spec_Min":I
    :cond_f
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Measuring have been completed"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    move/from16 v27, v0

    if-eqz v27, :cond_10

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    .line 285
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const-string v28, "READY"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    goto/16 :goto_1

    .line 234
    .restart local v22    # "mECG_HR50_Spec_Max":I
    .restart local v23    # "mECG_HR50_Spec_Min":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto/16 :goto_5

    .line 238
    .end local v22    # "mECG_HR50_Spec_Max":I
    .end local v23    # "mECG_HR50_Spec_Min":I
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "HR(70bpm)"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_14

    .line 239
    const-string v27, "ECG_HR_70_MIN"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v25

    .line 240
    .local v25, "mECG_HR70_Spec_Min":I
    const-string v27, "ECG_HR_70_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 242
    .local v24, "mECG_HR70_Spec_Max":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    move/from16 v0, v25

    if-lt v13, v0, :cond_13

    move/from16 v0, v24

    if-gt v13, v0, :cond_13

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 248
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto/16 :goto_5

    .line 252
    .end local v24    # "mECG_HR70_Spec_Max":I
    .end local v25    # "mECG_HR70_Spec_Min":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "HR(100bpm)"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_16

    .line 253
    const-string v27, "ECG_HR_100_MIN"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 254
    .local v19, "mECG_HR100_Spec_Min":I
    const-string v27, "ECG_HR_100_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 256
    .local v18, "mECG_HR100_Spec_Max":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    move/from16 v0, v19

    if-lt v13, v0, :cond_15

    move/from16 v0, v18

    if-gt v13, v0, :cond_15

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 262
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto/16 :goto_5

    .line 266
    .end local v18    # "mECG_HR100_Spec_Max":I
    .end local v19    # "mECG_HR100_Spec_Min":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "HR(200bpm)"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 267
    const-string v27, "ECG_HR_200_MIN"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v21

    .line 268
    .local v21, "mECG_HR200_Spec_Min":I
    const-string v27, "ECG_HR_200_MAX"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v20

    .line 270
    .local v20, "mECG_HR200_Spec_Max":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    move/from16 v0, v21

    if-lt v13, v0, :cond_17

    move/from16 v0, v20

    if-gt v13, v0, :cond_17

    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const v28, -0xffff01

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "PASS"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 275
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const/high16 v28, -0x10000

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setTextColor(I)V

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    const-string v28, "FAIL"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z
    invoke-static/range {v27 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z

    goto/16 :goto_5

    .line 289
    .end local v12    # "heart_rate":Ljava/lang/String;
    .end local v13    # "heat_rate_decimal":I
    .end local v20    # "mECG_HR200_Spec_Max":I
    .end local v21    # "mECG_HR200_Spec_Min":I
    :cond_18
    const-string v27, "com.sec.factory.app.factorytest.ECG_INIT_PROGRESS"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_19

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # operator++ for: Lcom/sec/android/app/hwmoduletest/ECG;->ecg_init_progress:I
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$1108(Lcom/sec/android/app/hwmoduletest/ECG;)I

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$1200(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/ProgressBar;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->ecg_init_progress:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/app/hwmoduletest/ECG;->access$1100(Lcom/sec/android/app/hwmoduletest/ECG;)I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_1

    .line 296
    :cond_19
    const-string v27, "com.sec.factory.app.factorytest.DEVICE_DETACHED"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;

    move-result-object v27

    const-string v28, "Device have been detached!"

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/ECG$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECG;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;
    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/hwmoduletest/ECG;->access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;

    move-result-object v27

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_1
.end method
