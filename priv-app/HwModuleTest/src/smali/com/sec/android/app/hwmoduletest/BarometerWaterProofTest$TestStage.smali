.class final enum Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
.super Ljava/lang/Enum;
.source "BarometerWaterProofTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TestStage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

.field public static final enum INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

.field public static final enum MEASURING_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

.field public static final enum RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;


# instance fields
.field private mButtonStringID:I

.field private mStageName:Ljava/lang/String;

.field public mTouchedTHD:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 80
    new-instance v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    const-string v2, "INIT_STAGE"

    const-string v3, "INIT_STAGE"

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080285

    :goto_0
    invoke-direct {v1, v2, v4, v3, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 81
    new-instance v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    const-string v2, "RELASE_STAGE"

    const-string v3, "RELASE_STAGE"

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f080286

    :goto_1
    invoke-direct {v1, v2, v5, v3, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 82
    new-instance v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    const-string v2, "MEASURING_STAGE"

    const-string v3, "MEASURING_STAGE"

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f080287

    :goto_2
    invoke-direct {v1, v2, v6, v3, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->MEASURING_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 79
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    sget-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->MEASURING_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->$VALUES:[Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    return-void

    .line 80
    :cond_0
    const v0, 0x7f080288

    goto :goto_0

    .line 81
    :cond_1
    const v0, 0x7f080289

    goto :goto_1

    .line 82
    :cond_2
    const v0, 0x7f08028a

    goto :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 1
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "stringID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mTouchedTHD:Z

    .line 89
    iput-object p3, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mStageName:Ljava/lang/String;

    .line 90
    iput p4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mButtonStringID:I

    .line 91
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    const-class v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->$VALUES:[Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-virtual {v0}, [Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    return-object v0
.end method


# virtual methods
.method getStageNameID()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mButtonStringID:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mStageName:Ljava/lang/String;

    return-object v0
.end method
