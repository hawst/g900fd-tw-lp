.class public Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "BarometerSelfTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$1;,
        Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;
    }
.end annotation


# instance fields
.field private mAltitudeText:Landroid/widget/TextView;

.field private mAltitudeValue:F

.field private mHandler:Landroid/os/Handler;

.field private mPressureSensor:Landroid/hardware/Sensor;

.field private mPressureText:Landroid/widget/TextView;

.field private mPressureValue:F

.field private mResultText:Landroid/widget/TextView;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTempResultText:Landroid/widget/TextView;

.field private mTempText:Landroid/widget/TextView;

.field private mTempValue:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x447a0000    # 1000.0f

    .line 37
    const-string v0, "BarometerSelfTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 29
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    .line 30
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeValue:F

    .line 32
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mHandler:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;
    .param p1, "x1"    # F

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;
    .param p1, "x1"    # F

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;
    .param p1, "x1"    # F

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeValue:F

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->displayResult()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private changeDot(F)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # F

    .prologue
    .line 121
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "format":Ljava/text/DecimalFormat;
    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private displayResult()V
    .locals 10

    .prologue
    const/high16 v9, -0x10000

    const v8, -0xffff01

    const/high16 v7, 0x42aa0000    # 85.0f

    const/high16 v6, -0x3dcc0000    # -45.0f

    const/4 v1, 0x0

    .line 84
    const/4 v0, 0x0

    .line 85
    .local v0, "isPass":Z
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Pressure:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->changeDot(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hPa"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempText:Landroid/widget/TextView;

    const-string v3, "Temperature:  "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Temperature:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    cmpg-float v2, v2, v7

    if-gtz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempResultText:Landroid/widget/TextView;

    const-string v3, " Pass "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempResultText:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Altitude  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeValue:F

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->changeDot(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " m"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "displayResult"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pressure="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", temp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Altitude="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeValue:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F

    const/high16 v3, 0x43960000    # 300.0f

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F

    const v3, 0x44898000    # 1100.0f

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 102
    const-string v2, "STM"

    const-string v3, "BAROMETE_VENDOR"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 103
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    const/high16 v3, -0x3e100000    # -30.0f

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    const/high16 v3, 0x42d20000    # 105.0f

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2

    const/4 v0, 0x1

    .line 109
    :cond_0
    :goto_1
    if-eqz v0, :cond_5

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mResultText:Landroid/widget/TextView;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mResultText:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 112
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->setResult(I)V

    .line 118
    :goto_2
    return-void

    .line 93
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempResultText:Landroid/widget/TextView;

    const-string v3, " Fail "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempResultText:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 103
    goto :goto_1

    .line 105
    :cond_3
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    cmpg-float v2, v2, v6

    if-ltz v2, :cond_4

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F

    cmpl-float v2, v2, v7

    if-gtz v2, :cond_4

    const/4 v0, 0x1

    :goto_3
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    .line 114
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mResultText:Landroid/widget/TextView;

    const v3, 0x7f080004

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mResultText:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->setResult(I)V

    goto :goto_2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->setContentView(I)V

    .line 44
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureSensor:Landroid/hardware/Sensor;

    .line 46
    new-instance v0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;

    .line 47
    const v0, 0x7f0b002d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempText:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0b002e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempResultText:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0b002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureText:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0b0030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeText:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mResultText:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 59
    return-void
.end method
