.class public Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;
.super Ljava/lang/Object;
.source "SensorReadIntent.java"


# instance fields
.field private final ACCELERMETER_INTENT_ACTION_READ:Ljava/lang/String;

.field private final ACCELERMETER_INTENT_ACTION_STOP:Ljava/lang/String;

.field private final ACCEL_COUNT_MAX:I

.field private final CLASS_NAME:Ljava/lang/String;

.field private final CP_INTENT_ACTION_READ:Ljava/lang/String;

.field private final DEBUG:Z

.field private DUMMY:I

.field private final GRIP_COUNT_MAX:I

.field private final GRIP_INTENT_ACTION_READ:Ljava/lang/String;

.field private final GRIP_INTENT_ACTION_READ_OTHER:Ljava/lang/String;

.field private final GRIP_INTENT_ACTION_STOP:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_NAME:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2N3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2N3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0000:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0100:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__RELEASE_ALL:Ljava/lang/String;

.field private final SENSOR_ON_ARRAY_INDEX_GRIP:I

.field private final SENSOR_ON_ARRAY_LENGTH:I

.field private count_Grip:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mModuleSensorIDArray:[I

.field private mOriginalData_CPsAccelerometer:[I

.field private mOriginalData_Grip:[I

.field private mSensorOn:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;[I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "moduleSensorID"    # [I

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v2, "SensorReadIntent"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->CLASS_NAME:Ljava/lang/String;

    .line 14
    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->DEBUG:Z

    .line 19
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->DUMMY:I

    .line 20
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    .line 22
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->DUMMY:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    .line 23
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mSensorOn:[Z

    .line 29
    const-string v2, "com.sec.android.app.factorytest"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->CP_INTENT_ACTION_READ:Ljava/lang/String;

    .line 30
    const-string v2, "030005"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_INTENT_ACTION_READ:Ljava/lang/String;

    .line 31
    const-string v2, "07000b"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_INTENT_ACTION_READ_OTHER:Ljava/lang/String;

    .line 32
    const-string v2, "070006"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->ACCELERMETER_INTENT_ACTION_READ:Ljava/lang/String;

    .line 37
    const-string v2, "com.android.samsungtest.GripTestStop"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_INTENT_ACTION_STOP:Ljava/lang/String;

    .line 38
    const-string v2, "COMMAND"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_NAME:Ljava/lang/String;

    .line 39
    const-string v2, "010000000000"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1:Ljava/lang/String;

    .line 40
    const-string v2, "000001000000"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2:Ljava/lang/String;

    .line 41
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_3:Ljava/lang/String;

    .line 42
    const-string v2, "010001000000"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2:Ljava/lang/String;

    .line 44
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N3:Ljava/lang/String;

    .line 45
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2N3:Ljava/lang/String;

    .line 46
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2N3:Ljava/lang/String;

    .line 47
    const-string v2, "000000000000"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__RELEASE_ALL:Ljava/lang/String;

    .line 50
    const-string v2, "0100"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0100:Ljava/lang/String;

    .line 51
    const-string v2, "0000"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0000:Ljava/lang/String;

    .line 57
    const-string v2, "com.android.samsungtest.CpAccelermeterOff"

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->ACCELERMETER_INTENT_ACTION_STOP:Ljava/lang/String;

    .line 58
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->ACCEL_COUNT_MAX:I

    .line 61
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->GRIP_COUNT_MAX:I

    .line 65
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    .line 66
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    .line 188
    new-instance v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent$1;-><init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 71
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "Sensor On"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    .line 75
    new-array v2, v5, [I

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    .line 76
    new-array v2, v5, [I

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    .line 78
    const/4 v1, -0x1

    .line 80
    .local v1, "sensorOnArrayIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mSensorOn:[Z

    aput-boolean v6, v2, v0

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne v2, v3, :cond_3

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mSensorOn:[Z

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    aget-boolean v2, v2, v3

    if-nez v2, :cond_2

    .line 87
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "Grip Sensor"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v2, "FACTORY_TEST_APO"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 90
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "CPO"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->ConverterID(I)I

    move-result v1

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mSensorOn:[Z

    aput-boolean v7, v2, v1

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->registerReceiver_Grip()V

    .line 84
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 100
    :cond_2
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "Grip Sensor - On"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 104
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne v2, v3, :cond_1

    .line 105
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "CP Accelerometer Sensor - On"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->registerReceiver_CPAccelerometer()V

    goto :goto_2

    .line 112
    :cond_4
    return-void
.end method

.method private ConverterID(I)I
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 334
    const/4 v0, -0x1

    .line 336
    .local v0, "sensorOnArrayIndex":I
    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p1, v1, :cond_0

    .line 337
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    .line 342
    :cond_0
    return v0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->setValueGrip(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->setValueCPsAccelerometerData(Ljava/lang/String;)V

    return-void
.end method

.method private dataCheck([I)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [I

    .prologue
    .line 348
    const-string v1, ""

    .line 350
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 351
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 354
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 355
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 351
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 362
    :cond_2
    return-object v1
.end method

.method private registerReceiver_CPAccelerometer()V
    .locals 3

    .prologue
    .line 174
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 175
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 176
    const-string v1, "com.android.samsungtest.CpAccelermeterOff"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 178
    return-void
.end method

.method private registerReceiver_Grip()V
    .locals 3

    .prologue
    .line 161
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 162
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    const-string v1, "com.android.samsungtest.GripTestStop"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 165
    return-void
.end method

.method private setValueCPsAccelerometerData(Ljava/lang/String;)V
    .locals 11
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x4

    const/4 v10, 0x1

    const/16 v9, 0x10

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 308
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 309
    .local v0, "xData":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x6

    const/16 v5, 0x8

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, "yData":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0xa

    const/16 v5, 0xc

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x8

    const/16 v5, 0xa

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, "zData":Ljava/lang/String;
    const-string v3, "SensorReadIntent"

    const-string v4, "setValueCPsAccelerometerData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "xData=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],yData=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],zData=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    invoke-static {v0, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-short v4, v4

    aput v4, v3, v8

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    invoke-static {v1, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-short v4, v4

    aput v4, v3, v10

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    invoke-static {v2, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-short v4, v4

    aput v4, v3, v7

    .line 316
    const-string v3, "SensorReadIntent"

    const-string v4, "setValueCPsAccelerometerData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "x=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    aget v6, v6, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],y=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    aget v6, v6, v10

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],z=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method private setValueGrip(Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 232
    const-string v0, "SensorReadIntent"

    const-string v1, "setValueGrip"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v0, "15"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->count_Grip:I

    .line 236
    const-string v0, "010000000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v4

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v5

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    .line 284
    :goto_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->count_Grip:I

    if-ne v0, v5, :cond_2

    .line 285
    const-string v0, "010000000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "000001000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v4

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v5

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    .line 293
    :cond_2
    return-void

    .line 240
    :cond_3
    const-string v0, "000001000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v4

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v5

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    goto :goto_0

    .line 244
    :cond_4
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v4

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v5

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v6

    goto :goto_0

    .line 248
    :cond_5
    const-string v0, "010001000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v4

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v5

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    goto :goto_0

    .line 252
    :cond_6
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v4

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v5

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v6

    goto :goto_0

    .line 256
    :cond_7
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v4

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v5

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v6

    goto/16 :goto_0

    .line 260
    :cond_8
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v4

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v5

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v6

    goto/16 :goto_0

    .line 264
    :cond_9
    const-string v0, "000000000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v4

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v5

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    goto/16 :goto_0

    .line 270
    :cond_a
    const-string v0, "0100"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v4

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v5

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    goto/16 :goto_0

    .line 274
    :cond_b
    const-string v0, "0000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v5, v0, v4

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v5

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    aput v4, v0, v6

    goto/16 :goto_0

    .line 281
    :cond_c
    const-string v0, "SensorReadIntent"

    const-string v1, "setValueGrip"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private unregisterReceiver_CPAccelerometer()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 184
    :cond_0
    return-void
.end method

.method private unregisterReceiver_Grip()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 171
    :cond_0
    return-void
.end method


# virtual methods
.method public disableReceiver_CPsAccelerometer()V
    .locals 3

    .prologue
    .line 144
    const-string v0, "SensorReadIntent"

    const-string v1, "disableBroadcastReceiverCPsAccelerometer"

    const-string v2, "CP\'s Acceler... OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->unregisterReceiver_CPAccelerometer()V

    .line 147
    return-void
.end method

.method public isSensorOn(I)Z
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->ConverterID(I)I

    move-result v0

    .line 155
    .local v0, "sensorOnArrayIndex":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mSensorOn:[Z

    aget-boolean v1, v1, v0

    return v1
.end method

.method public returnCPsAccelerometerData()[I
    .locals 3

    .prologue
    .line 323
    const-string v0, "SensorReadIntent"

    const-string v1, "returnCPsAccelerometerData"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->dataCheck([I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_CPsAccelerometer:[I

    return-object v0
.end method

.method public returnGrip()[I
    .locals 3

    .prologue
    .line 299
    const-string v0, "SensorReadIntent"

    const-string v1, "returnGrip"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->dataCheck([I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    return-object v0
.end method

.method public sensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    const-string v1, "SensorReadIntent"

    const-string v2, "sensorOff"

    const-string v3, "Grip Sensor Off"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    aget v1, v1, v0

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne v1, v2, :cond_0

    .line 119
    const-string v1, "SensorReadIntent"

    const-string v2, "sensorOff"

    const-string v3, "Grip Sensor"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v1, "FACTORY_TEST_APO"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    const-string v1, "SensorReadIntent"

    const-string v2, "sensorOff"

    const-string v3, "CPO"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mOriginalData_Grip:[I

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->unregisterReceiver_Grip()V

    .line 117
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v1, :cond_2

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mSensorOn:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 139
    :cond_2
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    .line 140
    return-void
.end method
