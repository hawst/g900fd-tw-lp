.class public Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;
.super Landroid/app/Fragment;
.source "FragmentFingerPrint.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mFPSeparator:Landroid/view/View;

.field private mFPTestButton_Method3:Landroid/widget/Button;

.field private mFPTestButton_Method4:Landroid/widget/Button;

.field private mFPTestButton_NormalScan:Landroid/widget/Button;

.field private mFPTestButton_SensorInfo:Landroid/widget/Button;

.field private mFPTitleText:Landroid/widget/TextView;

.field private mFPVersion:Landroid/widget/TextView;

.field private mFingerprint:Lcom/validity/fingerprint/Fingerprint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 21
    const-string v0, "FragmentFingerPrint"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->CLASS_NAME:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 21
    const-string v0, "FragmentFingerPrint"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->CLASS_NAME:Ljava/lang/String;

    .line 36
    const-string v0, "FragmentFingerPrint"

    const-string v1, "FragmentFingerPrint"

    const-string v2, "constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private init(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x8

    .line 75
    const-string v0, "FragmentFingerPrint"

    const-string v1, "init"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const v0, 0x7f0b007b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0b007c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPSeparator:Landroid/view/View;

    .line 79
    const v0, 0x7f0b007d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_NormalScan:Landroid/widget/Button;

    .line 80
    const v0, 0x7f0b007e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method3:Landroid/widget/Button;

    .line 81
    const v0, 0x7f0b007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method4:Landroid/widget/Button;

    .line 82
    const v0, 0x7f0b0080

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_SensorInfo:Landroid/widget/Button;

    .line 83
    const v0, 0x7f0b0081

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPVersion:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_NormalScan:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method3:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method4:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_SensorInfo:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method3:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 90
    const-string v0, "factory"

    const-string v1, "BINARY_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method4:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 93
    :cond_0
    return-void
.end method

.method private setUIRate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 96
    const/16 v1, 0x9

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getUIRate(I)F

    move-result v0

    .line 97
    .local v0, "rate":F
    const-string v1, "FragmentFingerPrint"

    const-string v2, "setUIRate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_NormalScan:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method3:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_Method4:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTestButton_SensorInfo:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPVersion:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPTitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 107
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 141
    :goto_0
    return-void

    .line 112
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "FragmentFingerPrint"

    const-string v2, "onClick"

    const-string v3, "btn_fingerprint_normalscan"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v1, "Action"

    const-string v2, "NORMALDATA"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 124
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "Action"

    const-string v2, "METHOD3"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 129
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "Action"

    const-string v2, "METHOD4"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 134
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 135
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "Action"

    const-string v2, "SENSORINFO"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b007d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    const-string v0, "FragmentFingerPrint"

    const-string v1, "onCreate"

    const-string v2, "This is real Fragment file"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 48
    const-string v1, "FragmentFingerPrint"

    const-string v2, "onCreateView"

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const v1, 0x7f03001b

    invoke-virtual {p1, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 50
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 51
    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->init(Landroid/view/View;)V

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->setUIRate()V

    .line 54
    new-instance v1, Lcom/validity/fingerprint/Fingerprint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/validity/fingerprint/Fingerprint;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFPVersion:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v3}, Lcom/validity/fingerprint/Fingerprint;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :goto_0
    return-object v0

    .line 58
    :cond_0
    const-string v1, "FragmentFingerPrint"

    const-string v2, "onCreate"

    const-string v3, "mFP is null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    const-string v0, "FragmentFingerPrint"

    const-string v1, "onDestroyView"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v0}, Lcom/validity/fingerprint/Fingerprint;->cancel()I

    .line 70
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FragmentFingerPrint;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    .line 72
    :cond_0
    return-void
.end method
