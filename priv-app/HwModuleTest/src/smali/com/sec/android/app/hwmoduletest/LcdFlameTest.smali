.class public Lcom/sec/android/app/hwmoduletest/LcdFlameTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "LcdFlameTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;
    }
.end annotation


# static fields
.field private static final OFF:Ljava/lang/String; = "0"

.field private static final ON:Ljava/lang/String; = "1"

.field private static final TAG:Ljava/lang/String; = "Lcdflametest"

.field static mLayout:Landroid/widget/LinearLayout;


# instance fields
.field black_color:I

.field color:I

.field gray_color:I

.field private mPowerManager:Landroid/os/PowerManager;

.field white_color:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xff

    const/16 v2, 0x3f

    const/4 v1, 0x0

    .line 46
    const-string v0, "LcdFlameTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 36
    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->gray_color:I

    .line 37
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->white_color:I

    .line 38
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->black_color:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    .line 48
    return-void
.end method

.method private setBrightness(I)V
    .locals 2
    .param p1, "brightness"    # I

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mPowerManager:Landroid/os/PowerManager;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 190
    :cond_0
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 199
    const/4 v1, 0x0

    .line 202
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 208
    if-eqz v2, :cond_0

    .line 209
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 215
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    return-void

    .line 211
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "Lcdflametest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 214
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 204
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 205
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "Lcdflametest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 208
    if-eqz v1, :cond_1

    .line 209
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 211
    :catch_2
    move-exception v0

    .line 212
    const-string v3, "Lcdflametest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 208
    :goto_2
    if-eqz v1, :cond_2

    .line 209
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 213
    :cond_2
    :goto_3
    throw v3

    .line 211
    :catch_3
    move-exception v0

    .line 212
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "Lcdflametest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "IOException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 207
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 204
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1
.end method


# virtual methods
.method public getBrightness()I
    .locals 4

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 195
    .local v0, "brightness":I
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 53
    const-string v1, "Lcdflametest"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    .line 58
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->black_color:I

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    .line 59
    sget-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 60
    sget-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setKeepScreenOn(Z)V

    .line 62
    sget-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "button_key_light"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 66
    const-string v1, "IS_DBLC_FUNCTION"

    invoke-static {v1, v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 68
    .local v0, "textView":Landroid/widget/TextView;
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 69
    const-string v1, "."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    sget-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 72
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_0
    sget-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->setContentView(Landroid/view/View;)V

    .line 73
    sget-object v1, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;

    invoke-direct {v2, p0, p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/LcdFlameTest;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 74
    const/16 v1, 0xff

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->setBrightness(I)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 76
    const-string v1, "/sys/class/mdnie/mdnie/cabc"

    const-string v2, "0"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v1, "IS_BACKLIGHT_ON_FOR_GREEN_LCD"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    const-string v1, "PATH_BACKLIGHT_ON_FOR_GREEN_LCD"

    const-string v2, "1"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 84
    const-string v0, "Lcdflametest"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "button_key_light"

    const/16 v2, 0x5dc

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 88
    const-string v0, "IS_BACKLIGHT_ON_FOR_GREEN_LCD"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "PATH_BACKLIGHT_ON_FOR_GREEN_LCD"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 92
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 110
    sparse-switch p1, :sswitch_data_0

    .line 133
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 112
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->finish()V

    goto :goto_0

    .line 115
    :sswitch_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->black_color:I

    if-ne v0, v1, :cond_0

    .line 116
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->gray_color:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    .line 117
    sget-object v0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 118
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->gray_color:I

    if-ne v0, v1, :cond_1

    .line 119
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->white_color:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    .line 120
    sget-object v0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 122
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->black_color:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    .line 123
    sget-object v0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->mLayout:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->color:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 128
    :sswitch_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    .line 110
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 104
    const-string v0, "IS_BACKLIGHT_ON_FOR_GREEN_LCD"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "PATH_BACKLIGHT_ON_FOR_GREEN_LCD"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 96
    const-string v0, "IS_BACKLIGHT_ON_FOR_GREEN_LCD"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "PATH_BACKLIGHT_ON_FOR_GREEN_LCD"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    return-void
.end method
