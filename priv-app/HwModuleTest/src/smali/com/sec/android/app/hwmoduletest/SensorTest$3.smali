.class Lcom/sec/android/app/hwmoduletest/SensorTest$3;
.super Ljava/lang/Object;
.source "SensorTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/SensorTest;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V
    .locals 0

    .prologue
    .line 492
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 494
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/SensorTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 496
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    const-string v2, "GEOMAGNETIC_SENSOR_ADC"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5502(Lcom/sec/android/app/hwmoduletest/SensorTest;[Ljava/lang/String;)[Ljava/lang/String;

    .line 498
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onResume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "magAdcData : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 500
    :catch_0
    move-exception v0

    .line 501
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 504
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method
