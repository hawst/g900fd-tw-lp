.class public Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
.super Landroid/view/View;
.source "FingerPrintMethod3Graph.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;
    }
.end annotation


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private GRAPH_MAX_VALUE:F

.field private GRAPH_X_SCALING:F

.field private GRAPH_Y_SCALING:F

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private SCLAING:I

.field private final TEXTSIZE_RATIO:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mCol:I

.field private mColWidth:I

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mDataResult:Ljava/lang/String;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mNumberPaint:Landroid/graphics/Paint;

.field private mPathSpecMax:Landroid/graphics/Path;

.field private mPathSpecMin:Landroid/graphics/Path;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mPathZ:Landroid/graphics/Path;

.field private mPrimaryEndPixel:I

.field private mPrimaryStartPixel:I

.field private mRect:Landroid/graphics/Rect;

.field private mResultFailPaint:Landroid/graphics/Paint;

.field private mResultPassPaint:Landroid/graphics/Paint;

.field private mRow:I

.field private mRowHeight:I

.field private mSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenHeight:I

.field private mScreenHeight_Max:I

.field private mScreenTitleHeight:I

.field private mScreenTitleHeight_Max:I

.field private mScreenWidth:I

.field private mSecondaryEndPixel:I

.field private mSecondaryStartPixel:I

.field private mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mSpecPaint:Landroid/graphics/Paint;

.field private mSpecRow:I

.field private mSpecRowHeight:I

.field private mTableTexts:[[Ljava/lang/String;

.field private mTextCenterPaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValuePaint:Landroid/graphics/Paint;

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueZ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;

.field private final paddingHeight:I

.field private final paddingWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0x1f4

    const/16 v1, 0xa

    .line 87
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 23
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_X:I

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    .line 25
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_MAX_VALUE:F

    .line 26
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    .line 27
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    .line 28
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->TEXTSIZE_RATIO:I

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    .line 34
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->paddingWidth:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->paddingHeight:I

    .line 36
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    .line 37
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    .line 38
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    .line 39
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight:I

    .line 40
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight_Max:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight_Max:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    sub-int/2addr v1, v2

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    .line 49
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathZ:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueZ:Ljava/util/ArrayList;

    .line 62
    const-string v0, "FingerPrintMethod3Graph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->CLASS_NAME:Ljava/lang/String;

    .line 64
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    .line 65
    const/16 v0, 0x7a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryEndPixel:I

    .line 66
    const/16 v0, 0x7b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    .line 67
    const/16 v0, 0xbf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryEndPixel:I

    .line 74
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataResult:Ljava/lang/String;

    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->init(Landroid/content/Context;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v2, 0x1f4

    const/16 v1, 0xa

    .line 97
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_X:I

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    .line 25
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_MAX_VALUE:F

    .line 26
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    .line 27
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    .line 28
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->TEXTSIZE_RATIO:I

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    .line 34
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->paddingWidth:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->paddingHeight:I

    .line 36
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    .line 37
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    .line 38
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    .line 39
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight:I

    .line 40
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight_Max:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight_Max:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    sub-int/2addr v1, v2

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    .line 49
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathZ:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueZ:Ljava/util/ArrayList;

    .line 62
    const-string v0, "FingerPrintMethod3Graph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->CLASS_NAME:Ljava/lang/String;

    .line 64
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    .line 65
    const/16 v0, 0x7a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryEndPixel:I

    .line 66
    const/16 v0, 0x7b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    .line 67
    const/16 v0, 0xbf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryEndPixel:I

    .line 74
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataResult:Ljava/lang/String;

    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->init(Landroid/content/Context;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v2, 0x1f4

    const/16 v1, 0xa

    .line 92
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_X:I

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    .line 25
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_MAX_VALUE:F

    .line 26
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    .line 27
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    .line 28
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->TEXTSIZE_RATIO:I

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    .line 34
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->paddingWidth:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->paddingHeight:I

    .line 36
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    .line 37
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    .line 38
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    .line 39
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight:I

    .line 40
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight_Max:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight_Max:I

    .line 42
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    sub-int/2addr v1, v2

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    .line 49
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathZ:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueZ:Ljava/util/ArrayList;

    .line 62
    const-string v0, "FingerPrintMethod3Graph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->CLASS_NAME:Ljava/lang/String;

    .line 64
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    .line 65
    const/16 v0, 0x7a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryEndPixel:I

    .line 66
    const/16 v0, 0x7b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    .line 67
    const/16 v0, 0xbf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryEndPixel:I

    .line 74
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataResult:Ljava/lang/String;

    .line 93
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->init(Landroid/content/Context;)V

    .line 94
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const v9, 0x3dcccccd    # 0.1f

    const/high16 v8, -0x1000000

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mContext:Landroid/content/Context;

    .line 103
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mContext:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 104
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 105
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 106
    const-string v3, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Point;->x:I

    .line 109
    :cond_0
    iget v3, v1, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    .line 110
    iget v3, v1, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight:I

    .line 112
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    div-int/lit8 v2, v3, 0x24

    .line 113
    .local v2, "textSize":I
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight:I

    div-int/lit8 v3, v3, 0x24

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    .line 114
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight:I

    div-int/lit8 v3, v3, 0x24

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    .line 115
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    add-int/lit8 v4, v4, 0x2

    mul-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight:I

    .line 117
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v3, v3, -0x14

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    .line 118
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    div-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mColWidth:I

    .line 120
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight:I

    div-int/lit8 v4, v4, 0x3

    add-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    .line 121
    new-instance v3, Landroid/graphics/CornerPathEffect;

    const/high16 v4, 0x41200000    # 10.0f

    invoke-direct {v3, v4}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mEffects:Landroid/graphics/PathEffect;

    .line 122
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    const v4, -0xffff01

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 127
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mYPaint:Landroid/graphics/Paint;

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mYPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40800000    # 4.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mYPaint:Landroid/graphics/Paint;

    const v4, -0xff0100

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mYPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 132
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mZPaint:Landroid/graphics/Paint;

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mZPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mZPaint:Landroid/graphics/Paint;

    const v4, -0xff0001

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mZPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 137
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecPaint:Landroid/graphics/Paint;

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x10000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 142
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 143
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 147
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataPaint:Landroid/graphics/Paint;

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 152
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 157
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mLinePaint:Landroid/graphics/Paint;

    .line 158
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mLinePaint:Landroid/graphics/Paint;

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 161
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextPaint:Landroid/graphics/Paint;

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 166
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 172
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 178
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 180
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 181
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    const v4, -0xffff01

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    add-int/lit8 v4, v2, 0xa

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 184
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x10000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    add-int/lit8 v4, v2, 0xa

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 192
    const-string v3, "FINGERPRINT_PRIMARY_PIXEL_START"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    .line 193
    const-string v3, "FINGERPRINT_PRIMARY_PIXEL_END"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryEndPixel:I

    .line 194
    const-string v3, "FINGERPRINT_SECONDARY_PIXEL_START"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    .line 195
    const-string v3, "FINGERPRINT_SECONDARY_PIXEL_END"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryEndPixel:I

    .line 197
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    const/high16 v4, 0x42c80000    # 100.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x43800000    # 256.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    .line 198
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    .line 199
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    const/high16 v4, 0x42aa0000    # 85.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x434d0000    # 205.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    .line 200
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    const/high16 v4, 0x42aa0000    # 85.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x43660000    # 230.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    .line 201
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x40400000    # 3.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->invalidate()V

    .line 204
    return-void
.end method

.method public static isNumeric(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 380
    const-string v0, "-?\\d+(\\.\\d+)?"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private round(FI)F
    .locals 6
    .param p1, "t"    # F
    .param p2, "decimal"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 461
    float-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public addValue(III)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueZ:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 208
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 210
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenHeight:I

    int-to-float v0, v0

    const/high16 v1, 0x41a00000    # 20.0f

    div-float v6, v0, v1

    .line 212
    .local v6, "baseYpx":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenTitleHeight:I

    add-int/lit8 v0, v0, -0x1e

    int-to-float v6, v0

    .line 213
    const-string v0, "Stimulus Av :"

    const/high16 v1, 0x41200000    # 10.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 214
    const/high16 v1, 0x437a0000    # 250.0f

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v2, v6, v0

    const/high16 v3, 0x43a50000    # 330.0f

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v4, v6, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 220
    const/high16 v1, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v3, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 221
    const/high16 v1, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 223
    const-string v0, "50"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 224
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x42480000    # 50.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x42480000    # 50.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 226
    const-string v0, "100"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 227
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x42c80000    # 100.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x42c80000    # 100.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 229
    const-string v0, "150"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x43160000    # 150.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 230
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x43160000    # 150.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x43160000    # 150.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 232
    const-string v0, "200"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x43480000    # 200.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 233
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x43480000    # 200.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x43480000    # 200.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 235
    const-string v0, "250"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x437a0000    # 250.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 236
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x437a0000    # 250.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x437a0000    # 250.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 239
    const/16 v8, 0x14

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 240
    const/high16 v0, 0x42480000    # 50.0f

    int-to-float v1, v8

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42480000    # 50.0f

    int-to-float v3, v8

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 242
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    int-to-float v2, v8

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 239
    add-int/lit8 v8, v8, 0x14

    goto :goto_0

    .line 245
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0x1e

    int-to-float v6, v0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mYPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 253
    const/4 v8, 0x0

    :goto_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    if-ge v8, v0, :cond_a

    .line 254
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    if-ge v9, v0, :cond_9

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    if-eqz v0, :cond_2

    .line 256
    add-int/lit8 v10, v9, 0x1

    .line 257
    .local v10, "k":I
    :goto_3
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    if-ge v10, v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v10

    if-eqz v0, :cond_3

    .line 261
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    if-ge v8, v0, :cond_4

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    mul-int/2addr v1, v8

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v1, v8, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 269
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mColWidth:I

    mul-int/2addr v1, v9

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mColWidth:I

    mul-int/2addr v1, v10

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 274
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    if-ge v8, v0, :cond_5

    .line 275
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRowHeight:I

    .line 278
    .local v7, "height":I
    :goto_5
    const-string v0, "PASS"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v8

    aget-object v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 287
    :goto_6
    add-int/lit8 v9, v10, -0x1

    .line 254
    .end local v7    # "height":I
    .end local v10    # "k":I
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 257
    .restart local v10    # "k":I
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 266
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    sub-int v2, v8, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v2, v8, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSpecRow:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_4

    .line 276
    :cond_5
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRowHeight:I

    .restart local v7    # "height":I
    goto :goto_5

    .line 280
    :cond_6
    const-string v0, "FAIL"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v8

    aget-object v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_6

    .line 282
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 285
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 253
    .end local v7    # "height":I
    .end local v10    # "k":I
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 292
    .end local v9    # "j":I
    :cond_a
    return-void
.end method

.method public setDataResult([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 28
    .param p1, "primary"    # [Ljava/lang/String;
    .param p2, "secondary"    # [Ljava/lang/String;

    .prologue
    .line 295
    const/16 v22, 0x0

    aget-object v12, p1, v22

    .line 296
    .local v12, "passGain1":Ljava/lang/String;
    const/16 v22, 0x1

    aget-object v14, p1, v22

    .line 297
    .local v14, "signal1":Ljava/lang/String;
    const/16 v22, 0x2

    aget-object v8, p1, v22

    .line 298
    .local v8, "noise1":Ljava/lang/String;
    const/16 v22, 0x3

    aget-object v18, p1, v22

    .line 299
    .local v18, "snr1":Ljava/lang/String;
    const/16 v22, 0x4

    aget-object v4, p1, v22

    .line 300
    .local v4, "n_noise1":Ljava/lang/String;
    const/16 v22, 0x0

    aget-object v13, p2, v22

    .line 301
    .local v13, "passGain2":Ljava/lang/String;
    const/16 v22, 0x1

    aget-object v15, p2, v22

    .line 302
    .local v15, "signal2":Ljava/lang/String;
    const/16 v22, 0x2

    aget-object v9, p2, v22

    .line 303
    .local v9, "noise2":Ljava/lang/String;
    const/16 v22, 0x3

    aget-object v19, p2, v22

    .line 304
    .local v19, "snr2":Ljava/lang/String;
    const/16 v22, 0x4

    aget-object v5, p2, v22

    .line 306
    .local v5, "n_noise2":Ljava/lang/String;
    const/16 v22, 0x0

    const/16 v23, 0x0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Pixel Section Primary : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " ~ "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryEndPixel:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", Secondary : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " ~ "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryEndPixel:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 308
    const/16 v22, 0x1

    const/16 v23, 0x0

    const-string v24, "SPEC"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 309
    const/16 v22, 0x1

    const/16 v23, 0x1

    const-string v24, "Primary"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 310
    const/16 v22, 0x1

    const/16 v23, 0x2

    const-string v24, "Secondary"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 311
    const/16 v22, 0x2

    const/16 v23, 0x0

    const-string v24, "Signal"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 312
    const/16 v22, 0x2

    const/16 v23, 0x1

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 313
    const/16 v22, 0x2

    const/16 v23, 0x2

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 315
    const/16 v22, 0x3

    const/16 v23, 0x0

    const-string v24, "Noise"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 316
    const/16 v22, 0x3

    const/16 v23, 0x1

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 317
    const/16 v22, 0x3

    const/16 v23, 0x2

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 319
    const/16 v22, 0x4

    const/16 v23, 0x0

    const-string v24, "SNR"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 320
    const/16 v22, 0x4

    const/16 v23, 0x1

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 321
    const/16 v22, 0x4

    const/16 v23, 0x2

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 323
    const/16 v22, 0x5

    const/16 v23, 0x0

    const-string v24, "N.Noise"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 324
    const/16 v22, 0x5

    const/16 v23, 0x1

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 325
    const/16 v22, 0x5

    const/16 v23, 0x2

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "~"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 327
    const/16 v22, 0x6

    const/16 v23, 0x0

    const-string v24, ""

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 328
    const/16 v22, 0x6

    const/16 v23, 0x1

    const-string v24, "Primary"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 329
    const/16 v22, 0x6

    const/16 v23, 0x2

    const-string v24, "Secondary"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 331
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x41200000    # 10.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v16

    .line 332
    .local v16, "signal_value1":F
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x41200000    # 10.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v17

    .line 333
    .local v17, "signal_value2":F
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 334
    .local v10, "noise_value1":F
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 335
    .local v11, "noise_value2":F
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x41200000    # 10.0f

    div-float v20, v22, v23

    .line 336
    .local v20, "snr_value1":F
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x41200000    # 10.0f

    div-float v21, v22, v23

    .line 337
    .local v21, "snr_value2":F
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 338
    .local v6, "n_noise_value1":F
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 340
    .local v7, "n_noise_value2":F
    const/16 v22, 0x7

    const/16 v23, 0x0

    const-string v24, "RESULT"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v20, v22

    if-ltz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v20, v22

    if-gtz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v6, v22

    if-ltz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v6, v22

    if-gtz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v10, v22

    if-ltz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v10, v22

    if-gtz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v16, v22

    if-ltz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v16, v22

    if-gtz v22, :cond_0

    .line 345
    const/16 v22, 0x7

    const/16 v23, 0x1

    const-string v24, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 349
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v21, v22

    if-ltz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v21, v22

    if-gtz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v7, v22

    if-ltz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v7, v22

    if-gtz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v11, v22

    if-ltz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mNoiseSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v11, v22

    if-gtz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpl-float v22, v17, v22

    if-ltz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSignalSpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v22

    cmpg-float v22, v17, v22

    if-gtz v22, :cond_1

    .line 353
    const/16 v22, 0x7

    const/16 v23, 0x2

    const-string v24, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 358
    :goto_1
    const/16 v22, 0x8

    const/16 v23, 0x0

    const-string v24, "Gain"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 359
    const/16 v22, 0x8

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v12}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 360
    const/16 v22, 0x8

    const/16 v23, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v13}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 362
    const/16 v22, 0x9

    const/16 v23, 0x0

    const-string v24, "Signal"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 363
    const/16 v22, 0x9

    const/16 v23, 0x1

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 364
    const/16 v22, 0x9

    const/16 v23, 0x2

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 366
    const/16 v22, 0xa

    const/16 v23, 0x0

    const-string v24, "Noise"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 367
    const/16 v22, 0xa

    const/16 v23, 0x1

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v10, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 368
    const/16 v22, 0xa

    const/16 v23, 0x2

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v11, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 370
    const/16 v22, 0xb

    const/16 v23, 0x0

    const-string v24, "SNR"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 371
    const/16 v22, 0xb

    const/16 v23, 0x1

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 372
    const/16 v22, 0xb

    const/16 v23, 0x2

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 374
    const/16 v22, 0xc

    const/16 v23, 0x0

    const-string v24, "N.Noise"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 375
    const/16 v22, 0xc

    const/16 v23, 0x1

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v6, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 376
    const/16 v22, 0xc

    const/16 v23, 0x2

    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v7, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->round(FI)F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    .line 377
    return-void

    .line 347
    :cond_0
    const/16 v22, 0x7

    const/16 v23, 0x1

    const-string v24, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 355
    :cond_1
    const/16 v22, 0x7

    const/16 v23, 0x2

    const-string v24, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_1
.end method

.method public setGraphXScale(F)V
    .locals 0
    .param p1, "xScale"    # F

    .prologue
    .line 453
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    .line 454
    return-void
.end method

.method public setGraphYScale(F)V
    .locals 0
    .param p1, "yScale"    # F

    .prologue
    .line 457
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    .line 458
    return-void
.end method

.method public setPixelResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "primary"    # Ljava/lang/String;
    .param p2, "secondary"    # Ljava/lang/String;

    .prologue
    .line 396
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "priPixel":[Ljava/lang/String;
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 398
    .local v2, "secPixel":[Ljava/lang/String;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 399
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    aget-object v4, v1, v0

    const-string v5, "\\s"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 401
    :cond_0
    const/4 v0, 0x1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 402
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    aget-object v4, v2, v0

    const-string v5, "\\s"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 404
    :cond_1
    return-void
.end method

.method public setTextData(IILjava/lang/String;)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 384
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mRow:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mCol:I

    if-ge p2, v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, p1

    aput-object p3, v0, p2

    .line 387
    :cond_0
    return-void
.end method

.method public showGraph()V
    .locals 7

    .prologue
    const/high16 v6, 0x42480000    # 50.0f

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 408
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 410
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    .line 421
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 424
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryStartPixel:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPrimaryEndPixel:I

    if-gt v0, v1, :cond_0

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 431
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueX:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 432
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 433
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryStartPixel:I

    :goto_1
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mSecondaryEndPixel:I

    if-gt v0, v1, :cond_1

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mPathY:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 433
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 449
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->invalidate()V

    .line 450
    return-void
.end method
