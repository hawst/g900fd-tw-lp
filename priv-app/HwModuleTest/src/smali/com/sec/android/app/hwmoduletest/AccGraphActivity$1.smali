.class Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;
.super Landroid/os/Handler;
.source "AccGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 70
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mAccGraph:Lcom/sec/android/app/hwmoduletest/view/AccGraph;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$100(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/AccGraph;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)[I

    move-result-object v1

    aget v1, v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)[I

    move-result-object v2

    const/4 v3, 0x1

    aget v2, v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mRawData:[I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)[I

    move-result-object v3

    const/4 v4, 0x2

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->addValue(FFF)V

    goto :goto_0

    .line 77
    :pswitch_1
    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsProxMotorTest:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$400(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Landroid/os/SystemVibrator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->FEED_BACK_PATTERN:[J
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$300(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)[J

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrate([JI)V

    goto :goto_0

    .line 87
    :pswitch_2
    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mIsProxMotorTest:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$400(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Landroid/os/SystemVibrator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
