.class Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;
.super Ljava/lang/Object;
.source "FingerPrintTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    const v2, 0x7f0b0076

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->iv_image:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$402(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->iv_image:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/FingerprintBitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 156
    const-string v0, "FingerPrintTest"

    const-string v1, "mFingerprintBitmap == null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->iv_image:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/FingerprintBitmap;

    move-result-object v1

    iget-object v1, v1, Lcom/validity/fingerprint/FingerprintBitmap;->fingerprint:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Width : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/FingerprintBitmap;

    move-result-object v2

    iget-object v2, v2, Lcom/validity/fingerprint/FingerprintBitmap;->fingerprint:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/FingerprintBitmap;

    move-result-object v2

    iget-object v2, v2, Lcom/validity/fingerprint/FingerprintBitmap;->fingerprint:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
