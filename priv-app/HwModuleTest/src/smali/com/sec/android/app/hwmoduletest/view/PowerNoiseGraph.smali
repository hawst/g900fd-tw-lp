.class public Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;
.super Landroid/view/View;
.source "PowerNoiseGraph.java"


# static fields
.field private static CALCULATING:I = 0x0

.field private static FAIL:I = 0x0

.field private static PASS:I = 0x0

.field private static final mFlag:F = 1000.0f


# instance fields
.field private GRAPH_SCALE_LIMITE:F

.field private final GRAPH_SCALING_COOR:I

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private final INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private SENSOR_SPEC_MAX:F

.field private SENSOR_SPEC_MIN:F

.field private TAG:Ljava/lang/String;

.field private final TEXT_SCALING_COOR:F

.field private VALUE_MAX:F

.field private VALUE_SCALING_X:F

.field private VALUE_SCALING_Y:F

.field private VALUE_SCALING_Z:F

.field private graphSizeSetting:F

.field private isfirstvalue:Z

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mPNTextPaint:Landroid/graphics/Paint;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mPathZ:Landroid/graphics/Path;

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValuePowerNoise:[Ljava/lang/String;

.field private mValueRawData:[F

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueZ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;

.field private result:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->FAIL:I

    .line 54
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->PASS:I

    .line 55
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->CALCULATING:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 73
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 23
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INIT_COOR_X:I

    .line 24
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INIT_COOR_Y:I

    .line 25
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INCREASING_COOR:I

    .line 26
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALING_COOR:I

    .line 27
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TEXT_SCALING_COOR:F

    .line 33
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    .line 35
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    .line 43
    const-string v0, "PowerNoiseGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    .line 46
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->isfirstvalue:Z

    .line 47
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    .line 48
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0.000"

    aput-object v2, v0, v1

    const-string v1, "0.000"

    aput-object v1, v0, v4

    const-string v1, "0.000"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    .line 52
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    .line 58
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_X:F

    .line 59
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Y:F

    .line 60
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Z:F

    .line 61
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    .line 62
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    .line 63
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    .line 64
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F

    .line 70
    const v0, 0x3a03126f    # 5.0E-4f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->graphSizeSetting:F

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->init(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 83
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INIT_COOR_X:I

    .line 24
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INIT_COOR_Y:I

    .line 25
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INCREASING_COOR:I

    .line 26
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALING_COOR:I

    .line 27
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TEXT_SCALING_COOR:F

    .line 33
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    .line 35
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    .line 43
    const-string v0, "PowerNoiseGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    .line 46
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->isfirstvalue:Z

    .line 47
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    .line 48
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0.000"

    aput-object v2, v0, v1

    const-string v1, "0.000"

    aput-object v1, v0, v4

    const-string v1, "0.000"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    .line 52
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    .line 58
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_X:F

    .line 59
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Y:F

    .line 60
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Z:F

    .line 61
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    .line 62
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    .line 63
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    .line 64
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F

    .line 70
    const v0, 0x3a03126f    # 5.0E-4f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->graphSizeSetting:F

    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->init(Landroid/content/Context;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INIT_COOR_X:I

    .line 24
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INIT_COOR_Y:I

    .line 25
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->INCREASING_COOR:I

    .line 26
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALING_COOR:I

    .line 27
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TEXT_SCALING_COOR:F

    .line 33
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    .line 35
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    .line 43
    const-string v0, "PowerNoiseGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    .line 46
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->isfirstvalue:Z

    .line 47
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    .line 48
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0.000"

    aput-object v2, v0, v1

    const-string v1, "0.000"

    aput-object v1, v0, v4

    const-string v1, "0.000"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    .line 52
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    .line 58
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_X:F

    .line 59
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Y:F

    .line 60
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Z:F

    .line 61
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    .line 62
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    .line 63
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    .line 64
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F

    .line 70
    const v0, 0x3a03126f    # 5.0E-4f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->graphSizeSetting:F

    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->init(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method private checkMaximun(F)F
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 271
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 272
    iget p1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F

    .line 277
    :cond_0
    :goto_0
    return p1

    .line 273
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 274
    iget p1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v8, 0x41f00000    # 30.0f

    const/high16 v7, -0x1000000

    const/high16 v6, 0x43480000    # 200.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    .line 88
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mContext:Landroid/content/Context;

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 90
    .local v1, "mWm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mScreenWidth:I

    .line 93
    :try_start_0
    const-string v2, "MAGNETIC_SENSOR_SELFTEST_X_MIN"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    .line 94
    const-string v2, "MAGNETIC_SENSOR_SELFTEST_X_MAX"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    .line 100
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    div-float v2, v6, v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    .line 103
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mScreenWidth:I

    add-int/lit8 v2, v2, -0xa

    div-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->LIST_SIZE:I

    .line 104
    new-instance v2, Landroid/graphics/CornerPathEffect;

    const/high16 v3, 0x41200000    # 10.0f

    invoke-direct {v2, v3}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 105
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 110
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    const v3, -0xff0100

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 115
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 120
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 125
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 131
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->setSpecGraphScale()V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->invalidate()V

    .line 139
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/Exception;
    const/high16 v2, -0x31000000

    :try_start_1
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    .line 97
    const/high16 v2, 0x4f000000

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MAX:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    .line 100
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    div-float v2, v6, v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    goto/16 :goto_0

    .line 99
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->SENSOR_SPEC_MIN:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    .line 100
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_MAX:F

    div-float v3, v6, v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    throw v2
.end method

.method private setPath()V
    .locals 6

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v5, 0x43960000    # 300.0f

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 248
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 250
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 252
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 257
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->checkMaximun(F)F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    mul-float/2addr v1, v4

    sub-float v1, v5, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->checkMaximun(F)F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    mul-float/2addr v1, v4

    sub-float v1, v5, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->checkMaximun(F)F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    mul-float/2addr v1, v4

    sub-float v1, v5, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->invalidate()V

    .line 268
    return-void
.end method

.method private setSpecGraphScale()V
    .locals 5

    .prologue
    .line 281
    const-string v1, "MAGNETIC_POWERNOISE_GRAPH_Y_RANGE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "GraphYRange":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    const-string v2, "setSpecGraphScale"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GRAPH_SCALE_LIMITE :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    return-void

    .line 286
    :cond_1
    const/high16 v1, 0x43480000    # 200.0f

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->GRAPH_SCALE_LIMITE:F

    goto :goto_0
.end method


# virtual methods
.method public addValueWithPowerNoise(FFF[Ljava/lang/String;[FI)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "power_noise"    # [Ljava/lang/String;
    .param p5, "raw_data"    # [F
    .param p6, "result"    # I

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 215
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->isfirstvalue:Z

    if-eqz v0, :cond_2

    .line 216
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_X:F

    .line 217
    iput p2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Y:F

    .line 218
    iput p3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Z:F

    .line 225
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->isfirstvalue:Z

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 236
    :cond_0
    iput-object p5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    .line 238
    sget v0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->CALCULATING:I

    if-eq p6, v0, :cond_1

    .line 239
    iput-object p4, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    .line 242
    :cond_1
    iput p6, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->setPath()V

    .line 244
    return-void

    .line 220
    :cond_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_X:F

    add-float/2addr v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_X:F

    .line 221
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Y:F

    add-float/2addr v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Y:F

    .line 222
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Z:F

    add-float/2addr v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->VALUE_SCALING_Z:F

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 143
    const/4 v7, 0x0

    .line 144
    .local v7, "textx":F
    const/4 v8, 0x0

    .line 145
    .local v8, "texty":F
    const/4 v9, 0x0

    .line 148
    .local v9, "textz":F
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    const/4 v1, 0x0

    aget v7, v0, v1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    const/4 v1, 0x1

    aget v8, v0, v1

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    const/4 v1, 0x2

    aget v9, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :goto_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 159
    const-string v0, "x :"

    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 160
    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x43020000    # 130.0f

    const/high16 v4, 0x42200000    # 40.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 161
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x428c0000    # 70.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 162
    const-string v0, "y :"

    const/high16 v1, 0x43200000    # 160.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 163
    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x438c0000    # 280.0f

    const/high16 v4, 0x42200000    # 40.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 164
    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v2, 0x428c0000    # 70.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 165
    const-string v0, "z :"

    const/high16 v1, 0x439b0000    # 310.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 166
    const/high16 v1, 0x43af0000    # 350.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x43d70000    # 430.0f

    const/high16 v4, 0x42200000    # 40.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 167
    invoke-static {v9}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43af0000    # 350.0f

    const/high16 v2, 0x428c0000    # 70.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 168
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x42c80000    # 100.0f

    const/high16 v3, 0x41200000    # 10.0f

    const/high16 v4, 0x43fa0000    # 500.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 170
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x43960000    # 300.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    const/high16 v4, 0x43960000    # 300.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathY:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mYPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPathZ:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 177
    const-string v0, "Raw Data (ADC)"

    const/high16 v1, 0x41200000    # 10.0f

    const v2, 0x44034000    # 525.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 178
    const-string v0, "X :"

    const/high16 v1, 0x41200000    # 10.0f

    const v2, 0x44098000    # 550.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    const v2, 0x44098000    # 550.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 181
    const-string v0, "Y :"

    const/high16 v1, 0x43200000    # 160.0f

    const v2, 0x44098000    # 550.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43480000    # 200.0f

    const v2, 0x44098000    # 550.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 184
    const-string v0, "Z :"

    const/high16 v1, 0x439b0000    # 310.0f

    const v2, 0x44098000    # 550.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueRawData:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43af0000    # 350.0f

    const v2, 0x44098000    # 550.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 187
    const-string v0, "Power Noise"

    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x44160000    # 600.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 188
    const-string v0, "X :"

    const/high16 v1, 0x41200000    # 10.0f

    const v2, 0x441c4000    # 625.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/high16 v1, 0x42480000    # 50.0f

    const v2, 0x441c4000    # 625.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 191
    const-string v0, "Y :"

    const/high16 v1, 0x43200000    # 160.0f

    const v2, 0x441c4000    # 625.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const/high16 v1, 0x43480000    # 200.0f

    const v2, 0x441c4000    # 625.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 194
    const-string v0, "Z :"

    const/high16 v1, 0x439b0000    # 310.0f

    const v2, 0x441c4000    # 625.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValuePowerNoise:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/high16 v1, 0x43af0000    # 350.0f

    const v2, 0x441c4000    # 625.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 198
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    sget v1, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->PASS:I

    if-ne v0, v1, :cond_1

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 201
    const-string v0, "PASS"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mScreenWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x32

    int-to-float v1, v1

    const/high16 v2, 0x442f0000    # 700.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 211
    :cond_0
    :goto_1
    return-void

    .line 151
    :catch_0
    move-exception v6

    .line 152
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    const-string v1, "onDraw"

    const-string v2, "get value fail"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Size of x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", z="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->TAG:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LIST_SIZE="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->LIST_SIZE:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 202
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    sget v1, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->FAIL:I

    if-ne v0, v1, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 205
    const-string v0, "FAIL"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mScreenWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x32

    int-to-float v1, v1

    const/high16 v2, 0x442f0000    # 700.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 206
    :cond_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->result:I

    sget v1, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->CALCULATING:I

    if-ne v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 209
    const-string v0, "Wait..."

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mScreenWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x32

    int-to-float v1, v1

    const/high16 v2, 0x442f0000    # 700.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->mPNTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method
