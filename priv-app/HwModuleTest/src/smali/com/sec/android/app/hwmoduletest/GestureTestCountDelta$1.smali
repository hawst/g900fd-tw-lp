.class Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$1;
.super Ljava/lang/Object;
.source "GestureTestCountDelta.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 72
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    # setter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mLoggingOn:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$002(Z)Z

    .line 74
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$100()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    # setter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mLoggingOn:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$002(Z)Z

    .line 77
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$100()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
