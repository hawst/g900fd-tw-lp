.class public Lcom/sec/android/app/hwmoduletest/VibrationTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "VibrationTest.java"


# static fields
.field private static final LEFT:I = 0x2

.field private static final MAGNITUDE_MAX:I = 0x2710

.field private static final RIGHT:I = 0x1

.field public static final TIMELESS_VIBRATOR:J = 0x7fffffffL


# instance fields
.field private DIRECTION:I

.field private final mLeftPattern:[B

.field private final mRightPattern:[B

.field private mVibrator:Landroid/os/SystemVibrator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x12

    .line 45
    const-string v0, "VibrationTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 30
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mLeftPattern:[B

    .line 37
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mRightPattern:[B

    .line 46
    return-void

    .line 30
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        -0x1t
        0xft
        0x0t
        0x7ft
        0x0t
        0x0t
        -0xft
    .end array-data

    .line 37
    nop

    :array_1
    .array-data 1
        0x2t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        -0x1t
        0x1ft
        0x0t
        0x7ft
        0x0t
        0x0t
        -0xft
    .end array-data
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 80
    .local v1, "view":Landroid/view/View;
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 81
    invoke-virtual {v1, v4}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 82
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->setContentView(Landroid/view/View;)V

    .line 83
    const-string v2, "vibrator"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/SystemVibrator;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 87
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "Direction"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->DIRECTION:I

    .line 88
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    .line 116
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 117
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 107
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->resetMagnitude()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, "Vibrate stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 92
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 93
    const-string v0, "SUPPORT_DUAL_MOTOR"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->DIRECTION:I

    if-ne v0, v1, :cond_0

    .line 95
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->startVibrate_dual_motor(I)V

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "Vibrate start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->startVibrate_dual_motor(I)V

    goto :goto_0

    .line 100
    :cond_1
    const-wide/32 v0, 0x7fffffff

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->startVibrate(J)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/VibrationTest;->finish()V

    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public startVibrate(J)V
    .locals 11
    .param p1, "milliseconds"    # J

    .prologue
    const/4 v6, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 50
    const-string v3, "IS_VIBETONZ_UNSUPPORTED"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, p1, v4

    if-nez v3, :cond_0

    .line 52
    const-string v3, "IS_VIBETONZ_UNSUPPORTED"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "patternValues":[Ljava/lang/String;
    aget-object v3, v0, v8

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 54
    .local v1, "turnOnTime":I
    aget-object v3, v0, v9

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 55
    .local v2, "vibeDurationTime":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    new-array v4, v6, [J

    int-to-long v6, v1

    aput-wide v6, v4, v8

    int-to-long v6, v2

    aput-wide v6, v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v8, v5}, Landroid/os/SystemVibrator;->vibrate([JII)V

    .line 63
    .end local v0    # "patternValues":[Ljava/lang/String;
    .end local v1    # "turnOnTime":I
    .end local v2    # "vibeDurationTime":I
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    new-array v4, v6, [J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v8

    aput-wide p1, v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrate([JI)V

    goto :goto_0
.end method

.method public startVibrate_dual_motor(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/16 v2, 0x2710

    .line 66
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mLeftPattern:[B

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startVibrate_dual_motor"

    const-string v2, "Start Vibrate for left"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->mRightPattern:[B

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/VibrationTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startVibrate_dual_motor"

    const-string v2, "Start Vibrate for Right"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
