.class public Lcom/validity/fingerprint/ConfigReader$DataHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ConfigReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/validity/fingerprint/ConfigReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataHandler"
.end annotation


# instance fields
.field private _data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

.field private _disableButtons:Z

.field private _fingerActionGenericLabel:Z

.field private _fingerLiftLabel:Z

.field private _fingerPlaceOrSwipeLabel:Z

.field private _fingerSwipeAnimation:Z

.field private _fpDisplay:Z

.field private _hapticFeedback:Z

.field private _practiceMode:Z

.field private _screenOrientation:Z

.field private _sensorBar:Z

.field private _sensorType:Z

.field private _showVideo:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 6
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    const/16 v5, 0x31

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 412
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    .line 413
    .local v0, "chars":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 414
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_screenOrientation:Z

    if-eqz v1, :cond_1

    .line 415
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->screenOrientation:I

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_sensorType:Z

    if-eqz v1, :cond_2

    .line 417
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorType:I

    goto :goto_0

    .line 418
    :cond_2
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_showVideo:Z

    if-eqz v1, :cond_3

    .line 419
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->showVideo:Z

    goto :goto_0

    .line 420
    :cond_3
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_practiceMode:Z

    if-eqz v1, :cond_4

    .line 421
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->practiceMode:Z

    goto :goto_0

    .line 422
    :cond_4
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_hapticFeedback:Z

    if-eqz v1, :cond_5

    .line 423
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->hapticFeedback:Z

    goto :goto_0

    .line 424
    :cond_5
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerPlaceOrSwipeLabel:Z

    if-eqz v1, :cond_6

    .line 425
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iput-object v0, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerPlaceOrSwipeLabel:Ljava/lang/String;

    goto :goto_0

    .line 426
    :cond_6
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerLiftLabel:Z

    if-eqz v1, :cond_7

    .line 427
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iput-object v0, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerLiftLabel:Ljava/lang/String;

    goto :goto_0

    .line 428
    :cond_7
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerActionGenericLabel:Z

    if-eqz v1, :cond_8

    .line 429
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iput-object v0, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerActionGenericLabel:Ljava/lang/String;

    goto :goto_0

    .line 430
    :cond_8
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_disableButtons:Z

    if-eqz v1, :cond_d

    .line 431
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iput-object v0, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButton:Ljava/lang/String;

    .line 432
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v4, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    add-int/lit8 v1, p3, -0x1

    aget-char v1, p1, v1

    if-ne v1, v5, :cond_9

    move v1, v2

    :goto_1
    iput-boolean v1, v4, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->search:Z

    .line 433
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v4, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    add-int/lit8 v1, p3, -0x2

    aget-char v1, p1, v1

    if-ne v1, v5, :cond_a

    move v1, v2

    :goto_2
    iput-boolean v1, v4, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->home:Z

    .line 434
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v4, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    add-int/lit8 v1, p3, -0x3

    aget-char v1, p1, v1

    if-ne v1, v5, :cond_b

    move v1, v2

    :goto_3
    iput-boolean v1, v4, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->back:Z

    .line 435
    iget-object v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v1, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    add-int/lit8 v4, p3, -0x4

    aget-char v4, p1, v4

    if-ne v4, v5, :cond_c

    :goto_4
    iput-boolean v2, v1, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->menu:Z

    goto/16 :goto_0

    :cond_9
    move v1, v3

    .line 432
    goto :goto_1

    :cond_a
    move v1, v3

    .line 433
    goto :goto_2

    :cond_b
    move v1, v3

    .line 434
    goto :goto_3

    :cond_c
    move v2, v3

    .line 435
    goto :goto_4

    .line 436
    :cond_d
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_sensorBar:Z

    if-nez v1, :cond_0

    .line 438
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerSwipeAnimation:Z

    if-nez v1, :cond_0

    .line 440
    iget-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fpDisplay:Z

    if-eqz v1, :cond_0

    goto/16 :goto_0
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 214
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 374
    const-string v0, "screenOrientation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_screenOrientation:Z

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    const-string v0, "sensorType"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_sensorType:Z

    goto :goto_0

    .line 378
    :cond_2
    const-string v0, "showVideo"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 379
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_showVideo:Z

    goto :goto_0

    .line 380
    :cond_3
    const-string v0, "practiceMode"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 381
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_practiceMode:Z

    goto :goto_0

    .line 382
    :cond_4
    const-string v0, "hapticFeedback"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 383
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_hapticFeedback:Z

    goto :goto_0

    .line 384
    :cond_5
    const-string v0, "fingerPlaceOrSwipeLabel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 385
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerPlaceOrSwipeLabel:Z

    goto :goto_0

    .line 386
    :cond_6
    const-string v0, "fingerLiftLabel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 387
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerLiftLabel:Z

    goto :goto_0

    .line 388
    :cond_7
    const-string v0, "fingerActionGenericLabel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 389
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerActionGenericLabel:Z

    goto :goto_0

    .line 390
    :cond_8
    const-string v0, "disableButtons"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 391
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_disableButtons:Z

    goto :goto_0

    .line 392
    :cond_9
    const-string v0, "sensorBar"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 393
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_sensorBar:Z

    goto :goto_0

    .line 394
    :cond_a
    const-string v0, "fingerSwipeAnimation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 395
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerSwipeAnimation:Z

    goto :goto_0

    .line 396
    :cond_b
    const-string v0, "fpDisplay"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fpDisplay:Z

    goto :goto_0
.end method

.method public getData()Lcom/validity/fingerprint/ConfigReader$ConfigData;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    return-object v0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-direct {v0}, Lcom/validity/fingerprint/ConfigReader$ConfigData;-><init>()V

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    .line 206
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v0, 0x1

    .line 229
    const-string v2, "screenOrientation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_screenOrientation:Z

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    const-string v2, "sensorType"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 235
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_sensorType:Z

    goto :goto_0

    .line 237
    :cond_2
    const-string v2, "showVideo"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 239
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_showVideo:Z

    goto :goto_0

    .line 241
    :cond_3
    const-string v2, "practiceMode"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 243
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_practiceMode:Z

    goto :goto_0

    .line 245
    :cond_4
    const-string v2, "hapticFeedback"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 247
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_hapticFeedback:Z

    goto :goto_0

    .line 249
    :cond_5
    const-string v2, "fingerPlaceOrSwipeLabel"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 251
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerPlaceOrSwipeLabel:Z

    goto :goto_0

    .line 253
    :cond_6
    const-string v2, "fingerLiftLabel"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 255
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerLiftLabel:Z

    goto :goto_0

    .line 257
    :cond_7
    const-string v2, "fingerActionGenericLabel"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 259
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerActionGenericLabel:Z

    goto :goto_0

    .line 261
    :cond_8
    const-string v2, "disableButtons"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 263
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_disableButtons:Z

    goto :goto_0

    .line 265
    :cond_9
    const-string v2, "sensorBar"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 267
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_sensorBar:Z

    .line 269
    const-string v2, "visible"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_a

    .line 270
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    const-string v3, "visible"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v0, :cond_e

    :goto_1
    iput-boolean v0, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->visible:Z

    .line 274
    :cond_a
    const-string v0, "xPos"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v4, :cond_b

    .line 275
    iget-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v0, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    const-string v1, "xPos"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->xPos:I

    .line 278
    :cond_b
    const-string v0, "yPos"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v4, :cond_c

    .line 279
    iget-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v0, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    const-string v1, "yPos"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->yPos:I

    .line 282
    :cond_c
    const-string v0, "width"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v4, :cond_d

    .line 283
    iget-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v0, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    const-string v1, "width"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->width:I

    .line 286
    :cond_d
    const-string v0, "height"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 287
    iget-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v0, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    const-string v1, "height"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->height:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 270
    goto :goto_1

    .line 290
    :cond_f
    const-string v2, "fingerSwipeAnimation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 292
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fingerSwipeAnimation:Z

    .line 294
    const-string v2, "visible"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_10

    .line 295
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v2, "visible"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_18

    move v2, v0

    :goto_2
    iput-boolean v2, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->visible:Z

    .line 299
    :cond_10
    const-string v2, "orientation"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_11

    .line 300
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "orientation"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->orientation:I

    .line 304
    :cond_11
    const-string v2, "xPos"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_12

    .line 305
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "xPos"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xPos:I

    .line 308
    :cond_12
    const-string v2, "yPos"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_13

    .line 309
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "yPos"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yPos:I

    .line 312
    :cond_13
    const-string v2, "xScale"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_14

    .line 313
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "xScale"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xScale:F

    .line 316
    :cond_14
    const-string v2, "yScale"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_15

    .line 317
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "yScale"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yScale:F

    .line 320
    :cond_15
    const-string v2, "offsetLength"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_16

    .line 321
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "offsetLength"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->offsetLength:I

    .line 325
    :cond_16
    const-string v2, "outlineVisible"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_17

    .line 326
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v3, "outlineVisible"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v0, :cond_19

    :goto_3
    iput-boolean v0, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->outlineVisible:Z

    .line 330
    :cond_17
    const-string v0, "animationSpeed"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 331
    iget-object v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v0, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    const-string v1, "animationSpeed"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->animationSpeed:I

    goto/16 :goto_0

    :cond_18
    move v2, v1

    .line 295
    goto/16 :goto_2

    :cond_19
    move v0, v1

    .line 326
    goto :goto_3

    .line 335
    :cond_1a
    const-string v2, "fpDisplay"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 337
    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_fpDisplay:Z

    .line 339
    const-string v2, "xPos"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_1b

    .line 340
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    const-string v3, "xPos"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->xPos:I

    .line 343
    :cond_1b
    const-string v2, "yPos"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_1c

    .line 344
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    const-string v3, "yPos"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->xPos:I

    .line 347
    :cond_1c
    const-string v2, "width"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_1d

    .line 348
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    const-string v3, "width"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->width:I

    .line 351
    :cond_1d
    const-string v2, "height"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_1e

    .line 352
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    const-string v3, "height"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->height:I

    .line 355
    :cond_1e
    const-string v2, "showStartupVideo"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 356
    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$DataHandler;->_data:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    iget-object v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    const-string v3, "showStartupVideo"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v0, :cond_1f

    :goto_4
    iput-boolean v0, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->showStartupVideo:Z

    goto/16 :goto_0

    :cond_1f
    move v0, v1

    goto :goto_4
.end method
