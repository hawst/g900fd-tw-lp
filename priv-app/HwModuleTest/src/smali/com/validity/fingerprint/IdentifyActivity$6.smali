.class Lcom/validity/fingerprint/IdentifyActivity$6;
.super Ljava/lang/Object;
.source "IdentifyActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/validity/fingerprint/IdentifyActivity;->cancel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/validity/fingerprint/IdentifyActivity;


# direct methods
.method constructor <init>(Lcom/validity/fingerprint/IdentifyActivity;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # invokes: Lcom/validity/fingerprint/IdentifyActivity;->stopCountdownTimer()V
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$800(Lcom/validity/fingerprint/IdentifyActivity;)V

    .line 323
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$300(Lcom/validity/fingerprint/IdentifyActivity;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$300(Lcom/validity/fingerprint/IdentifyActivity;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/validity/fingerprint/Fingerprint;->cancel()I

    .line 325
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$300(Lcom/validity/fingerprint/IdentifyActivity;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/validity/fingerprint/Fingerprint;->cleanUp()I

    .line 326
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    const/4 v1, 0x0

    # setter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v0, v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$302(Lcom/validity/fingerprint/IdentifyActivity;Lcom/validity/fingerprint/Fingerprint;)Lcom/validity/fingerprint/Fingerprint;

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$6;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$600(Lcom/validity/fingerprint/IdentifyActivity;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 329
    return-void
.end method
