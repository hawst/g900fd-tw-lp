.class Lcom/validity/fingerprint/IdentifyActivity$8$1;
.super Landroid/os/CountDownTimer;
.source "IdentifyActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/validity/fingerprint/IdentifyActivity$8;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/validity/fingerprint/IdentifyActivity$8;


# direct methods
.method constructor <init>(Lcom/validity/fingerprint/IdentifyActivity$8;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 505
    iput-object p1, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity$8;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mPowerMgr:Landroid/os/PowerManager;
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$1000(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    const/4 v0, 0x0

    # setter for: Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$1202(I)I

    .line 518
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity$8;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$1100(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity$8;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # invokes: Lcom/validity/fingerprint/IdentifyActivity;->startIdentify()V
    invoke-static {v0}, Lcom/validity/fingerprint/IdentifyActivity;->access$700(Lcom/validity/fingerprint/IdentifyActivity;)V

    .line 521
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 508
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    iget-object v1, v1, Lcom/validity/fingerprint/IdentifyActivity$8;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mPowerMgr:Landroid/os/PowerManager;
    invoke-static {v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$1000(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/os/PowerManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 509
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    iget-object v1, v1, Lcom/validity/fingerprint/IdentifyActivity$8;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$200(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "Please wait"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v0, v2

    .line 511
    .local v0, "secondsRemaining":I
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$8$1;->this$1:Lcom/validity/fingerprint/IdentifyActivity$8;

    iget-object v1, v1, Lcom/validity/fingerprint/IdentifyActivity$8;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$1100(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Try again in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seconds."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    .end local v0    # "secondsRemaining":I
    :cond_0
    return-void
.end method
