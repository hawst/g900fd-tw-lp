.class public Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;
.super Ljava/lang/Object;
.source "MaximUVSensorEol.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "MaximUVEol"

.field private static final MAX86902_ENHANCED_UV_EOL_HR_MODE:I = -0x7

.field private static final MAX86902_ENHANCED_UV_EOL_SUM_MODE:I = -0x6

.field private static final MAX86902_ENHANCED_UV_EOL_VB_MODE:I = -0x5

.field private static final VERSION:Ljava/lang/String; = "1.0"


# instance fields
.field private dataFull:I

.field private eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

.field private eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

.field private eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

.field private sensor_type:I

.field private sm:Landroid/hardware/SensorManager;

.field private uvEolSensor:Landroid/hardware/Sensor;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensor_type"    # I

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sensor_type:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    .line 30
    iput v1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    .line 34
    const-string v0, "MaximUVEol"

    const-string v1, "Version:1.0"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iput p2, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sensor_type:I

    .line 38
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 37
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sm:Landroid/hardware/SensorManager;

    .line 39
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->uvEolSensor:Landroid/hardware/Sensor;

    .line 40
    const-string v0, "MaximUVEol"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MaximUVSensor() - uvEolSensor : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->uvEolSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-void
.end method


# virtual methods
.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->uvEolSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 128
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const v5, 0x3ffff

    .line 64
    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    iget v4, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sensor_type:I

    if-ne v3, v4, :cond_2

    .line 67
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v2, v3

    .line 69
    .local v2, "sensor_data":I
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 70
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    if-eqz v3, :cond_2

    .line 71
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    const/high16 v4, -0x3f600000    # -5.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    .line 72
    shr-int/lit8 v3, v2, 0x16

    and-int/lit8 v1, v3, 0x3

    .line 74
    .local v1, "order":I
    packed-switch v1, :pswitch_data_0

    .line 99
    .end local v1    # "order":I
    :cond_0
    :goto_0
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    const/16 v4, 0x3f

    if-ne v3, v4, :cond_2

    .line 101
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    if-eqz v3, :cond_1

    .line 102
    const-string v3, "MaximUVEol"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EOL0:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 103
    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb0:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb3:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 104
    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->sum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->hr:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 102
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    invoke-interface {v3, v4}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;->onMaximUVSensorEolIRChanged(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;)V

    .line 107
    :cond_1
    const/4 v3, 0x0

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    .end local v2    # "sensor_data":I
    :cond_2
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    return-void

    .line 76
    .restart local v1    # "order":I
    .restart local v2    # "sensor_data":I
    :pswitch_0
    :try_start_2
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    and-int v4, v2, v5

    iput v4, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb0:I

    .line 77
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 120
    .end local v1    # "order":I
    .end local v2    # "sensor_data":I
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v3, "MaximUVEol"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 64
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 80
    .restart local v1    # "order":I
    .restart local v2    # "sensor_data":I
    :pswitch_1
    :try_start_4
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    and-int v4, v2, v5

    iput v4, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb1:I

    .line 81
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    goto/16 :goto_0

    .line 84
    :pswitch_2
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    and-int v4, v2, v5

    iput v4, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb2:I

    .line 85
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    goto/16 :goto_0

    .line 88
    :pswitch_3
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    and-int v4, v2, v5

    iput v4, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb3:I

    .line 89
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    goto/16 :goto_0

    .line 92
    .end local v1    # "order":I
    :cond_3
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    const/high16 v4, -0x3f400000    # -6.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_4

    .line 93
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iput v2, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->sum:I

    .line 94
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    goto/16 :goto_0

    .line 95
    :cond_4
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    const/high16 v4, -0x3f200000    # -7.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 96
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    iput v2, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->hr:I

    .line 97
    iget v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->dataFull:I

    goto/16 :goto_0

    .line 111
    :cond_5
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    if-eqz v3, :cond_2

    .line 112
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    mul-int/lit8 v4, v2, 0x2

    iput v4, v3, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;->adccount:I

    .line 113
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    if-eqz v3, :cond_2

    .line 114
    const-string v3, "MaximUVEol"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EOL1:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    iget v5, v5, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;->adccount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    invoke-interface {v3, v4}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;->onMaximUVSensorEolUVChanged(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public registerListener(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;)V
    .locals 3
    .param p1, "eolListener"    # Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    .line 50
    new-instance v0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    invoke-direct {v0}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;-><init>()V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    .line 51
    new-instance v0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    invoke-direct {v0}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;-><init>()V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    .line 52
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->uvEolSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 53
    return-void
.end method

.method public unregisterListener(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;)V
    .locals 2
    .param p1, "eolListener"    # Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 57
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolListener:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;

    .line 58
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolIREvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    .line 59
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->eolUVEvent:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    .line 60
    return-void
.end method
