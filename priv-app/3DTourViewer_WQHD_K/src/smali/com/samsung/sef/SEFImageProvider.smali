.class public Lcom/samsung/sef/SEFImageProvider;
.super Lcom/voovio/sweep/ImageProvider;
.source "SEFImageProvider.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/voovio/sweep/ImageProvider;-><init>()V

    .line 14
    const-string v0, "SEFImageProvider"

    iput-object v0, p0, Lcom/samsung/sef/SEFImageProvider;->TAG:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getImageDataByIndex(Ljava/lang/String;[BI)V
    .locals 10
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "baImage"    # [B
    .param p3, "imageIndex"    # I

    .prologue
    const/4 v9, 0x0

    .line 25
    add-int/lit8 v4, p3, 0x1

    .line 27
    .local v4, "tempIndex":I
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "VirtualTour_%03d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "aKeyName":Ljava/lang/String;
    :try_start_0
    invoke-static {p1, v0}, Lcom/sec/android/secvision/sef/SEF;->getSEFData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 32
    .local v3, "temp":[B
    array-length v1, v3

    .line 33
    .local v1, "copy_length":I
    array-length v5, v3

    array-length v6, p2

    if-eq v5, v6, :cond_0

    .line 36
    array-length v5, v3

    array-length v6, p2

    if-ge v5, v6, :cond_1

    array-length v1, v3

    .line 38
    :cond_0
    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v5, p2, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42
    .end local v1    # "copy_length":I
    .end local v3    # "temp":[B
    :goto_1
    return-void

    .line 36
    .restart local v1    # "copy_length":I
    .restart local v3    # "temp":[B
    :cond_1
    array-length v1, p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 39
    .end local v1    # "copy_length":I
    .end local v3    # "temp":[B
    :catch_0
    move-exception v2

    .line 40
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
