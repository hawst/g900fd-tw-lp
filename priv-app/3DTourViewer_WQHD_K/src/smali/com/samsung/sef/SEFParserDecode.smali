.class public Lcom/samsung/sef/SEFParserDecode;
.super Ljava/lang/Object;
.source "SEFParserDecode.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SEFParserDecode"

.field private static final TRANSITION_TYPE_FORWARD_WALK:I = 0x9

.field private static final TRANSITION_TYPE_FORWARD_WALK_PLUS:I = 0xa

.field private static final TRANSITION_TYPE_LATERAL_WALK_LEFT:I = 0xd

.field private static final TRANSITION_TYPE_LATERAL_WALK_PLUS_LEFT:I = 0xb

.field private static final TRANSITION_TYPE_LATERAL_WALK_PLUS_RIGHT:I = 0xc

.field private static final TRANSITION_TYPE_LATERAL_WALK_RIGHT:I = 0xe

.field private static final TRANSITION_TYPE_NONE:I = 0x0

.field private static final TRANSITION_TYPE_STAIRS_DOWN:I = 0xf

.field private static final TRANSITION_TYPE_STAIRS_DOWN_PLUS:I = 0x10

.field private static final TRANSITION_TYPE_STAIRS_UP:I = 0x11

.field private static final TRANSITION_TYPE_STAIRS_UP_PLUS:I = 0x12

.field private static final TRANSITION_TYPE_STEP_DOWN:I = 0x5

.field private static final TRANSITION_TYPE_STEP_DOWN_PLUS:I = 0x6

.field private static final TRANSITION_TYPE_STEP_UP:I = 0x7

.field private static final TRANSITION_TYPE_STEP_UP_PLUS:I = 0x8

.field private static final TRANSITION_TYPE_TURN_LEFT:I = 0x1

.field private static final TRANSITION_TYPE_TURN_RIGHT:I = 0x2

.field private static final TRANSITION_TYPE_WALK_AND_TURN_90_LEFT:I = 0x3

.field private static final TRANSITION_TYPE_WALK_AND_TURN_90_RIGHT:I = 0x4


# instance fields
.field public mAngles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private m_aAngles:[I

.field private m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v3, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    .line 54
    iput-object v3, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    .line 56
    iput-object v3, p0, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 75
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    const-string v3, "TransitionTemplates.ttf"

    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 76
    .local v2, "is":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/voovio/sweep/TemplateManager;->createTemplateManager(Ljava/io/InputStream;)Lcom/voovio/sweep/TemplateManager;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    .line 78
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    iput-object v3, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    .line 85
    .end local v2    # "is":Ljava/io/InputStream;
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 84
    nop

    :array_0
    .array-data 4
        0xa
        0x1e
        0x2d
        0x5a
    .end array-data
.end method

.method private getAngle(Lcom/voovio/sweep/Template;)F
    .locals 4
    .param p1, "oTemplate"    # Lcom/voovio/sweep/Template;

    .prologue
    .line 271
    invoke-virtual {p1}, Lcom/voovio/sweep/Template;->getId()I

    move-result v1

    .line 273
    .local v1, "nId":I
    const/4 v0, 0x0

    .line 274
    .local v0, "fAngle":F
    packed-switch v1, :pswitch_data_0

    .line 305
    :goto_0
    :pswitch_0
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2

    .line 276
    :pswitch_1
    const/high16 v0, -0x3ee00000    # -10.0f

    .line 277
    goto :goto_0

    .line 279
    :pswitch_2
    const/high16 v0, 0x41200000    # 10.0f

    .line 280
    goto :goto_0

    .line 282
    :pswitch_3
    const/high16 v0, -0x3e100000    # -30.0f

    .line 283
    goto :goto_0

    .line 285
    :pswitch_4
    const/high16 v0, 0x41f00000    # 30.0f

    .line 286
    goto :goto_0

    .line 288
    :pswitch_5
    const/high16 v0, -0x3dcc0000    # -45.0f

    .line 289
    goto :goto_0

    .line 291
    :pswitch_6
    const/high16 v0, 0x42340000    # 45.0f

    .line 292
    goto :goto_0

    .line 295
    :pswitch_7
    const/high16 v0, -0x3d4c0000    # -90.0f

    .line 296
    goto :goto_0

    .line 299
    :pswitch_8
    const/high16 v0, 0x42b40000    # 90.0f

    .line 300
    goto :goto_0

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private getTemplate(IF)Lcom/voovio/sweep/Template;
    .locals 7
    .param p1, "nTransitionType"    # I
    .param p2, "fAngle"    # F

    .prologue
    .line 90
    const/4 v3, 0x0

    .line 92
    .local v3, "oTemplate":Lcom/voovio/sweep/Template;
    float-to-double v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result p2

    .line 94
    const/4 v2, 0x0

    .line 95
    .local v2, "nAngle":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-lt v1, v4, :cond_0

    .line 104
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 194
    :goto_2
    :pswitch_0
    return-object v3

    .line 96
    :cond_0
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    aget v4, v4, v1

    iget-object v5, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    add-int/lit8 v6, v1, 0x1

    aget v5, v5, v6

    add-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v0, v4, v5

    .line 97
    .local v0, "fMiddle":F
    cmpg-float v4, p2, v0

    if-gtz v4, :cond_1

    .line 98
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    aget v2, v4, v1

    .line 99
    goto :goto_1

    .line 101
    :cond_1
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_aAngles:[I

    add-int/lit8 v5, v1, 0x1

    aget v2, v4, v5

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    .end local v0    # "fMiddle":F
    :pswitch_1
    sparse-switch v2, :sswitch_data_0

    goto :goto_2

    .line 112
    :sswitch_0
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xc

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 113
    goto :goto_2

    .line 115
    :sswitch_1
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 116
    goto :goto_2

    .line 118
    :sswitch_2
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x12

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 119
    goto :goto_2

    .line 121
    :sswitch_3
    iget-object v5, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    invoke-virtual {v4}, Lcom/voovio/sweep/TemplateManager;->getImageAspect()Ljava/lang/String;

    move-result-object v4

    const-string v6, "horizontal"

    if-ne v4, v6, :cond_2

    const/16 v4, 0x14

    :goto_3
    invoke-virtual {v5, v4}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 122
    goto :goto_2

    .line 121
    :cond_2
    const/16 v4, 0x16

    goto :goto_3

    .line 129
    :pswitch_2
    sparse-switch v2, :sswitch_data_1

    goto :goto_2

    .line 131
    :sswitch_4
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 132
    goto :goto_2

    .line 134
    :sswitch_5
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 135
    goto :goto_2

    .line 137
    :sswitch_6
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x13

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 138
    goto :goto_2

    .line 140
    :sswitch_7
    iget-object v5, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    invoke-virtual {v4}, Lcom/voovio/sweep/TemplateManager;->getImageAspect()Ljava/lang/String;

    move-result-object v4

    const-string v6, "horizontal"

    if-ne v4, v6, :cond_3

    const/16 v4, 0x15

    :goto_4
    invoke-virtual {v5, v4}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 141
    goto/16 :goto_2

    .line 140
    :cond_3
    const/16 v4, 0x17

    goto :goto_4

    .line 148
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 149
    goto/16 :goto_2

    .line 151
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 152
    goto/16 :goto_2

    .line 154
    :pswitch_5
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 155
    goto/16 :goto_2

    .line 157
    :pswitch_6
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 158
    goto/16 :goto_2

    .line 160
    :pswitch_7
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 161
    goto/16 :goto_2

    .line 163
    :pswitch_8
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 164
    goto/16 :goto_2

    .line 166
    :pswitch_9
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 167
    goto/16 :goto_2

    .line 169
    :pswitch_a
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 170
    goto/16 :goto_2

    .line 172
    :pswitch_b
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 173
    goto/16 :goto_2

    .line 175
    :pswitch_c
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 176
    goto/16 :goto_2

    .line 178
    :pswitch_d
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 179
    goto/16 :goto_2

    .line 181
    :pswitch_e
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 182
    goto/16 :goto_2

    .line 184
    :pswitch_f
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 185
    goto/16 :goto_2

    .line 187
    :pswitch_10
    iget-object v4, p0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 188
    goto/16 :goto_2

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 110
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x1e -> :sswitch_1
        0x2d -> :sswitch_2
        0x5a -> :sswitch_3
    .end sparse-switch

    .line 129
    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_4
        0x1e -> :sswitch_5
        0x2d -> :sswitch_6
        0x5a -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method public decodeSEF(Ljava/lang/String;)Lcom/voovio/sweep/Sweep;
    .locals 30
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 313
    const/16 v25, 0x850

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/SEF;->hasDataType(Ljava/lang/String;I)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 315
    const-string v25, "SEFParserDecode"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "File"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\' is a Virtual Tour SEF File"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    const/4 v14, 0x0

    .line 331
    .local v14, "exifReader":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v15, Landroid/media/ExifInterface;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v14    # "exifReader":Landroid/media/ExifInterface;
    .local v15, "exifReader":Landroid/media/ExifInterface;
    move-object v14, v15

    .line 339
    .end local v15    # "exifReader":Landroid/media/ExifInterface;
    .restart local v14    # "exifReader":Landroid/media/ExifInterface;
    :goto_0
    const-string v25, "Orientation"

    const/16 v26, -0x1

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v14, v0, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v6

    .line 344
    .local v6, "orientation":I
    const/16 v23, 0x0

    .line 346
    .local v23, "oSweep":Lcom/voovio/sweep/Sweep;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    .line 350
    invoke-static/range {p1 .. p1}, Lcom/sec/android/secvision/sef/SEF;->getSEFDataCount(Ljava/lang/String;)I

    move-result v22

    .line 352
    .local v22, "numSEFData":I
    const-string v25, "SEFParserDecode"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "Number of SEF data :"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v12, 0x0

    .line 358
    .local v12, "dataBuffer":[[B
    if-lez v22, :cond_1

    .line 360
    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v12, v0, [[B

    .line 363
    const/16 v25, 0x0

    :try_start_1
    const-string v26, "VirtualTour_Info"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/SEF;->getSEFData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v26

    aput-object v26, v12, v25
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 378
    :goto_1
    const/16 v25, 0x0

    aget-object v25, v12, v25

    if-nez v25, :cond_2

    .line 380
    const-string v25, "SEFParserDecode"

    const-string v26, "Failed in SEF decode"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/16 v23, 0x0

    .line 528
    .end local v6    # "orientation":I
    .end local v12    # "dataBuffer":[[B
    .end local v14    # "exifReader":Landroid/media/ExifInterface;
    .end local v22    # "numSEFData":I
    .end local v23    # "oSweep":Lcom/voovio/sweep/Sweep;
    :goto_2
    return-object v23

    .line 319
    :cond_0
    const-string v25, "SEFParserDecode"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "File"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\' is a not Virtual Tour SEF File"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/16 v23, 0x0

    goto :goto_2

    .line 333
    .restart local v14    # "exifReader":Landroid/media/ExifInterface;
    :catch_0
    move-exception v13

    .line 335
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 365
    .end local v13    # "e":Ljava/io/IOException;
    .restart local v6    # "orientation":I
    .restart local v12    # "dataBuffer":[[B
    .restart local v22    # "numSEFData":I
    .restart local v23    # "oSweep":Lcom/voovio/sweep/Sweep;
    :catch_1
    move-exception v13

    .line 367
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 373
    .end local v13    # "e":Ljava/io/IOException;
    :cond_1
    const/16 v23, 0x0

    goto :goto_2

    .line 388
    :cond_2
    const/16 v25, 0x0

    aget-object v25, v12, v25

    invoke-static/range {v25 .. v25}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 390
    .local v11, "bbHeader":Ljava/nio/ByteBuffer;
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v18

    .line 392
    .local v18, "nImages":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 394
    .local v4, "nImageWidth":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 396
    .local v5, "nImageHeight":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 398
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 402
    const/16 v25, 0x6

    move/from16 v0, v25

    if-eq v6, v0, :cond_3

    const/16 v25, 0x8

    move/from16 v0, v25

    if-ne v6, v0, :cond_4

    .line 404
    :cond_3
    move/from16 v20, v4

    .line 406
    .local v20, "nPivot":I
    move v4, v5

    .line 408
    move/from16 v5, v20

    .line 412
    .end local v20    # "nPivot":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sef/SEFParserDecode;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    move-object/from16 v26, v0

    if-le v4, v5, :cond_5

    const-string v25, "horizontal"

    :goto_3
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/TemplateManager;->setImageAspect(Ljava/lang/String;)V

    .line 414
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v25

    new-array v7, v0, [Lcom/voovio/sweep/Template;

    .line 416
    .local v7, "aTemplates":[Lcom/voovio/sweep/Template;
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v25

    new-array v8, v0, [Lcom/voovio/voo3d/data/Vector3;

    .line 418
    .local v8, "aTranslations":[Lcom/voovio/voo3d/data/Vector3;
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v25

    new-array v9, v0, [F

    .line 422
    .local v9, "aTransitionsData":[F
    const/16 v17, 0x0

    .local v17, "index":I
    :goto_4
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v25

    if-lt v0, v1, :cond_6

    .line 472
    const/16 v17, 0x0

    :goto_5
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v25

    if-lt v0, v1, :cond_e

    .line 495
    add-int/lit8 v25, v22, -0x1

    move/from16 v0, v25

    new-array v3, v0, [I

    .line 497
    .local v3, "aSizes":[I
    const/16 v16, 0x0

    .local v16, "imageIndex":I
    :goto_6
    add-int/lit8 v25, v22, -0x1

    move/from16 v0, v16

    move/from16 v1, v25

    if-lt v0, v1, :cond_10

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/sef/SEFParserDecode;->setmAngles(Ljava/util/ArrayList;)V

    .line 518
    :try_start_2
    invoke-static/range {v3 .. v9}, Lcom/voovio/sweep/Sweep;->createSweep([IIII[Lcom/voovio/sweep/Template;[Lcom/voovio/voo3d/data/Vector3;[F)Lcom/voovio/sweep/Sweep;

    move-result-object v23

    .line 519
    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/Sweep;->setSEFFile(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/voovio/sweep/SweepException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    .line 520
    :catch_2
    move-exception v13

    .line 522
    .local v13, "e":Lcom/voovio/sweep/SweepException;
    invoke-virtual {v13}, Lcom/voovio/sweep/SweepException;->printStackTrace()V

    goto/16 :goto_2

    .line 412
    .end local v3    # "aSizes":[I
    .end local v7    # "aTemplates":[Lcom/voovio/sweep/Template;
    .end local v8    # "aTranslations":[Lcom/voovio/voo3d/data/Vector3;
    .end local v9    # "aTransitionsData":[F
    .end local v13    # "e":Lcom/voovio/sweep/SweepException;
    .end local v16    # "imageIndex":I
    .end local v17    # "index":I
    :cond_5
    const-string v25, "vertical"

    goto :goto_3

    .line 424
    .restart local v7    # "aTemplates":[Lcom/voovio/sweep/Template;
    .restart local v8    # "aTranslations":[Lcom/voovio/voo3d/data/Vector3;
    .restart local v9    # "aTransitionsData":[F
    .restart local v17    # "index":I
    :cond_6
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v21

    .line 426
    .local v21, "nTransitionType":I
    const/16 v25, 0x0

    aput-object v25, v8, v17

    .line 428
    const/high16 v25, 0x7fc00000    # NaNf

    aput v25, v9, v17

    .line 430
    if-nez v21, :cond_7

    .line 432
    const-string v25, "SEFParfer"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "Unsupported Transition Type:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 436
    :cond_7
    const/16 v25, 0x1

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    const/16 v25, 0x2

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_9

    .line 438
    :cond_8
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v25

    aput v25, v9, v17

    .line 440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    aget v26, v9, v17

    invoke-static/range {v26 .. v26}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    :goto_7
    aget v25, v9, v17

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/sef/SEFParserDecode;->getTemplate(IF)Lcom/voovio/sweep/Template;

    move-result-object v25

    aput-object v25, v7, v17

    .line 422
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_4

    .line 442
    :cond_9
    const/16 v25, 0x9

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_a

    const/16 v25, 0xa

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    .line 444
    :cond_a
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v25

    aput v25, v9, v17

    .line 446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    aget v26, v9, v17

    invoke-static/range {v26 .. v26}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 448
    :cond_b
    const/16 v25, 0xd

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 450
    const/16 v25, 0xb

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 452
    const/16 v25, 0xe

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 454
    const/16 v25, 0xc

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 456
    :cond_c
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 458
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    goto :goto_7

    .line 462
    :cond_d
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    goto :goto_7

    .line 474
    .end local v21    # "nTransitionType":I
    :cond_e
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    .line 476
    .local v19, "nIsValid":I
    const/16 v25, 0x1

    move/from16 v0, v19

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    .line 478
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/samsung/sef/SEFParserDecode;->newVector3(Ljava/nio/ByteBuffer;)Lcom/voovio/voo3d/data/Vector3;

    move-result-object v25

    aput-object v25, v8, v17

    .line 472
    :goto_8
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_5

    .line 482
    :cond_f
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 484
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 486
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    goto :goto_8

    .line 498
    .end local v19    # "nIsValid":I
    .restart local v3    # "aSizes":[I
    .restart local v16    # "imageIndex":I
    :cond_10
    sget-object v25, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v26, "VirtualTour_%03d"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    add-int/lit8 v29, v16, 0x1

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v25 .. v27}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 499
    .local v10, "aKeyName":Ljava/lang/String;
    if-eqz v10, :cond_11

    .line 501
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/sec/android/secvision/sef/SEF;->getSEFDataPosition(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;

    move-result-object v24

    .line 502
    .local v24, "tmp":Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;
    if-eqz v24, :cond_11

    .line 504
    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;->length:I

    move/from16 v25, v0

    aput v25, v3, v16

    .line 497
    .end local v24    # "tmp":Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;
    :cond_11
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_6
.end method

.method public getType(Lcom/voovio/sweep/Template;)I
    .locals 2
    .param p1, "oTemplate"    # Lcom/voovio/sweep/Template;

    .prologue
    .line 200
    const/4 v1, 0x0

    .line 202
    .local v1, "nType":I
    invoke-virtual {p1}, Lcom/voovio/sweep/Template;->getId()I

    move-result v0

    .line 203
    .local v0, "nId":I
    packed-switch v0, :pswitch_data_0

    .line 265
    :goto_0
    return v1

    .line 205
    :pswitch_0
    const/16 v1, 0xb

    .line 206
    goto :goto_0

    .line 208
    :pswitch_1
    const/16 v1, 0xc

    .line 209
    goto :goto_0

    .line 211
    :pswitch_2
    const/16 v1, 0xd

    .line 212
    goto :goto_0

    .line 214
    :pswitch_3
    const/16 v1, 0xe

    .line 215
    goto :goto_0

    .line 217
    :pswitch_4
    const/16 v1, 0xf

    .line 218
    goto :goto_0

    .line 220
    :pswitch_5
    const/16 v1, 0x10

    .line 221
    goto :goto_0

    .line 223
    :pswitch_6
    const/16 v1, 0x11

    .line 224
    goto :goto_0

    .line 226
    :pswitch_7
    const/16 v1, 0x12

    .line 227
    goto :goto_0

    .line 229
    :pswitch_8
    const/4 v1, 0x5

    .line 230
    goto :goto_0

    .line 232
    :pswitch_9
    const/4 v1, 0x6

    .line 233
    goto :goto_0

    .line 235
    :pswitch_a
    const/4 v1, 0x7

    .line 236
    goto :goto_0

    .line 238
    :pswitch_b
    const/16 v1, 0x8

    .line 239
    goto :goto_0

    .line 241
    :pswitch_c
    const/16 v1, 0x9

    .line 242
    goto :goto_0

    .line 244
    :pswitch_d
    const/16 v1, 0xa

    .line 245
    goto :goto_0

    .line 251
    :pswitch_e
    const/4 v1, 0x1

    .line 252
    goto :goto_0

    .line 258
    :pswitch_f
    const/4 v1, 0x2

    .line 259
    goto :goto_0

    .line 203
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_e
        :pswitch_f
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_e
        :pswitch_f
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public getmAngles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    iget-object v0, p0, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public newVector3(Ljava/nio/ByteBuffer;)Lcom/voovio/voo3d/data/Vector3;
    .locals 4
    .param p1, "bbHeader"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 533
    new-instance v0, Lcom/voovio/voo3d/data/Vector3;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/voovio/voo3d/data/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public setmAngles(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "mAngles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/samsung/sef/SEFParserDecode;->mAngles:Ljava/util/ArrayList;

    .line 542
    return-void
.end method
