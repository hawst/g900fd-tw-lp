.class public final Lcom/sec/android/app/tourviewer/player/FullMapView;
.super Landroid/widget/RelativeLayout;
.source "FullMapView.java"


# instance fields
.field private angles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private backLine:Landroid/graphics/Paint;

.field private mCurrentNode:I

.field private mCurrentNodeBitmap:Landroid/graphics/Bitmap;

.field private mCurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

.field private mCurrentNodeTopBitmap:Landroid/graphics/Bitmap;

.field private mCurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

.field private mCurrentPosition:I

.field private mEndNodeBitmap:Landroid/graphics/Bitmap;

.field private mEndNodeBitmapSize2:Lcom/voovio/geometry/Point;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mNodePoints:[Lcom/voovio/geometry/Point;

.field private mNodePositions:[I

.field private mNodeProxyPoints:[Lcom/voovio/geometry/Point;

.field protected mNodesInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/voovio/sweep/Sweep$NodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPaintCircle:Landroid/graphics/Paint;

.field private mPaintLines:Landroid/graphics/Paint;

.field private mPathLength:F

.field private mPoint:Lcom/voovio/geometry/Point;

.field private mPositionsForEachNode:[I

.field private mRotationAngles:[F

.field private mStartNodeBitmap:Landroid/graphics/Bitmap;

.field private mStartNodeBitmapSize2:Lcom/voovio/geometry/Point;

.field private mTravelledPaintCircle:Landroid/graphics/Paint;

.field private mTravelledPaintLines:Landroid/graphics/Paint;

.field private mWalkCount:I

.field private miniMapSize:I

.field private sweepSize:I

.field private totalWalkCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v12, 0x7f05000e

    const/high16 v11, 0x40400000    # 3.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 96
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 50
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodesInfo:Ljava/util/ArrayList;

    .line 84
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->sweepSize:I

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 99
    const v6, 0x7f050002

    .line 98
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    .line 100
    invoke-virtual {p0, v9}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setBackgroundColor(I)V

    .line 101
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 102
    iget v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    iget v6, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    .line 101
    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 103
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 104
    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 105
    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 107
    const/high16 v6, 0x7f050000

    .line 106
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 108
    .local v2, "pixels":I
    invoke-virtual {v0, v9, v2, v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 109
    invoke-virtual {p0, v0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintLines:Landroid/graphics/Paint;

    .line 113
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintLines:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 114
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintLines:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050012

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 115
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintLines:Landroid/graphics/Paint;

    const-string v6, "#66CC00"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintLines:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 118
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    .line 119
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 120
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050012

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 121
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 124
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintCircle:Landroid/graphics/Paint;

    .line 125
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintCircle:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 126
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 127
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintCircle:Landroid/graphics/Paint;

    const-string v6, "#66CC00"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 130
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    .line 131
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 132
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 133
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 136
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    .line 137
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    const-string v6, "#33ffffff"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 139
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 140
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 142
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    .line 144
    new-instance v5, Lcom/voovio/geometry/Point;

    invoke-direct {v5}, Lcom/voovio/geometry/Point;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    .line 145
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mMatrix:Landroid/graphics/Matrix;

    .line 146
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 148
    const v6, 0x7f05000b

    .line 147
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    iput v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPathLength:F

    .line 149
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mWalkCount:I

    .line 150
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentPosition:I

    .line 152
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 153
    .local v4, "resources":Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "packageName":Ljava/lang/String;
    const-string v5, "moving_dot"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 156
    .local v3, "resid":I
    if-eqz v3, :cond_0

    .line 157
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmap:Landroid/graphics/Bitmap;

    .line 158
    new-instance v5, Lcom/voovio/geometry/Point;

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    .line 161
    :cond_0
    const-string v5, "moving_node_top"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 162
    if-eqz v3, :cond_1

    .line 163
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    .line 164
    new-instance v5, Lcom/voovio/geometry/Point;

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    .line 167
    :cond_1
    const-string v5, "start_dot"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 168
    if-eqz v3, :cond_2

    .line 169
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmap:Landroid/graphics/Bitmap;

    .line 170
    new-instance v5, Lcom/voovio/geometry/Point;

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmapSize2:Lcom/voovio/geometry/Point;

    .line 173
    :cond_2
    const-string v5, "end_dot"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 174
    if-eqz v3, :cond_3

    .line 175
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmap:Landroid/graphics/Bitmap;

    .line 176
    new-instance v5, Lcom/voovio/geometry/Point;

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmapSize2:Lcom/voovio/geometry/Point;

    .line 178
    :cond_3
    return-void
.end method

.method private computeMatrix(FFFFF)V
    .locals 3
    .param p1, "angle"    # F
    .param p2, "rotCenterX"    # F
    .param p3, "rotCenterY"    # F
    .param p4, "posX"    # F
    .param p5, "posY"    # F

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mMatrix:Landroid/graphics/Matrix;

    neg-float v1, p2

    neg-float v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p4, p5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 499
    return-void
.end method


# virtual methods
.method public arrowLength([Lcom/voovio/geometry/Point;)F
    .locals 13
    .param p1, "p"    # [Lcom/voovio/geometry/Point;

    .prologue
    .line 510
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 512
    .local v8, "tempPoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    iget v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float v5, v9, v10

    .local v5, "maxY":F
    move v4, v5

    .local v4, "maxX":F
    move v7, v5

    .local v7, "minY":F
    move v6, v5

    .line 513
    .local v6, "minX":F
    const/4 v3, 0x0

    .line 514
    .local v3, "max":F
    const/high16 v2, 0x3f800000    # 1.0f

    .line 515
    .local v2, "length":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v9, p1

    if-lt v0, v9, :cond_1

    .line 551
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_12

    .line 552
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_e

    .line 553
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    sub-float v3, v9, v10

    .line 556
    :goto_1
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v1, v9, :cond_f

    .line 566
    iget v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v9, v3

    .line 567
    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 568
    const/high16 v12, 0x7f050000

    .line 567
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v10, v11

    .line 566
    div-float v2, v9, v10

    .line 585
    .end local v1    # "j":I
    :cond_0
    :goto_3
    iget v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPathLength:F

    div-float/2addr v9, v2

    return v9

    .line 516
    :cond_1
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    const/4 v10, 0x0

    cmpg-float v9, v9, v10

    if-ltz v9, :cond_2

    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_2

    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    const/4 v10, 0x0

    cmpg-float v9, v9, v10

    if-ltz v9, :cond_2

    .line 517
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 518
    :cond_2
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_4

    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 519
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 515
    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 520
    :cond_4
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_9

    .line 521
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_5

    .line 522
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 523
    :cond_5
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_7

    .line 524
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    aget-object v10, p1, v0

    iget v10, v10, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_6

    .line 525
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 527
    :cond_6
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 529
    :cond_7
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    aget-object v10, p1, v0

    iget v10, v10, Lcom/voovio/geometry/Point;->y:F

    iget v11, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_8

    .line 530
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 532
    :cond_8
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 535
    :cond_9
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_a

    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_a

    .line 536
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 537
    :cond_a
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_c

    .line 538
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    aget-object v10, p1, v0

    iget v10, v10, Lcom/voovio/geometry/Point;->x:F

    iget v11, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_b

    .line 539
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 541
    :cond_b
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 543
    :cond_c
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    aget-object v10, p1, v0

    iget v10, v10, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_d

    .line 544
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 546
    :cond_d
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 555
    :cond_e
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v3

    goto/16 :goto_1

    .line 557
    .restart local v1    # "j":I
    :cond_f
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_11

    .line 558
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    cmpg-float v9, v3, v9

    if-gez v9, :cond_10

    .line 559
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget v10, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v10, v10

    sub-float v3, v9, v10

    .line 556
    :cond_10
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 562
    :cond_11
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpg-float v9, v3, v9

    if-gez v9, :cond_10

    .line 563
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v3

    goto :goto_5

    .line 571
    .end local v1    # "j":I
    :cond_12
    const/4 v0, 0x0

    :goto_6
    array-length v9, p1

    if-lt v0, v9, :cond_14

    .line 581
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v6, v9

    if-ltz v9, :cond_13

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050005

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    cmpl-float v9, v4, v9

    if-gtz v9, :cond_13

    .line 582
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v7, v9

    if-ltz v9, :cond_13

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050005

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    cmpl-float v9, v5, v9

    if-lez v9, :cond_0

    .line 583
    :cond_13
    const/high16 v2, 0x3fa00000    # 1.25f

    goto/16 :goto_3

    .line 572
    :cond_14
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    cmpl-float v9, v6, v9

    if-lez v9, :cond_15

    .line 573
    aget-object v9, p1, v0

    iget v6, v9, Lcom/voovio/geometry/Point;->x:F

    .line 574
    :cond_15
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->x:F

    cmpg-float v9, v4, v9

    if-gez v9, :cond_16

    .line 575
    aget-object v9, p1, v0

    iget v4, v9, Lcom/voovio/geometry/Point;->x:F

    .line 576
    :cond_16
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v9, v7, v9

    if-lez v9, :cond_17

    .line 577
    aget-object v9, p1, v0

    iget v7, v9, Lcom/voovio/geometry/Point;->y:F

    .line 578
    :cond_17
    aget-object v9, p1, v0

    iget v9, v9, Lcom/voovio/geometry/Point;->y:F

    cmpg-float v9, v5, v9

    if-gez v9, :cond_18

    .line 579
    aget-object v9, p1, v0

    iget v5, v9, Lcom/voovio/geometry/Point;->y:F

    .line 571
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_6
.end method

.method public findNextPoint(Lcom/voovio/geometry/Point;FFLcom/voovio/geometry/Point;)V
    .locals 3
    .param p1, "startPoint"    # Lcom/voovio/geometry/Point;
    .param p2, "distance"    # F
    .param p3, "angle"    # F
    .param p4, "nextPoint"    # Lcom/voovio/geometry/Point;

    .prologue
    .line 483
    iget v0, p1, Lcom/voovio/geometry/Point;->x:F

    float-to-double v1, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p4, Lcom/voovio/geometry/Point;->x:F

    .line 484
    iget v0, p1, Lcom/voovio/geometry/Point;->y:F

    float-to-double v1, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p2

    sub-float/2addr v0, v1

    iput v0, p4, Lcom/voovio/geometry/Point;->y:F

    .line 485
    return-void
.end method

.method public getAngles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTotalWalkCount()I
    .locals 1

    .prologue
    .line 701
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->totalWalkCount:I

    return v0
.end method

.method public newPoint()Lcom/voovio/geometry/Point;
    .locals 1

    .prologue
    .line 714
    new-instance v0, Lcom/voovio/geometry/Point;

    invoke-direct {v0}, Lcom/voovio/geometry/Point;-><init>()V

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v11, 0x7f050013

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const v8, 0x7f050002

    const v7, 0x7f050008

    .line 322
    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 326
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 328
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 322
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050014

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 333
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 335
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 329
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050015

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 340
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 342
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 336
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050016

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 347
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 349
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 343
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050017

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 354
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 356
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 350
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050018

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 361
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 363
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 357
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050019

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 368
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 370
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 364
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f05001a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 375
    sub-int/2addr v0, v4

    int-to-float v4, v0

    .line 377
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 371
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 381
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 383
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 386
    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 380
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f050014

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 390
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 393
    const v4, 0x7f050014

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 387
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f050015

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 397
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 400
    const v4, 0x7f050015

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 394
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 402
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f050016

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 404
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 407
    const v4, 0x7f050016

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 401
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 409
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f050017

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 411
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 414
    const v4, 0x7f050017

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 408
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f050018

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 418
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 420
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 421
    const v4, 0x7f050018

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 415
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 423
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f050019

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 425
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 425
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 428
    const v4, 0x7f050019

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 422
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f05001a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 433
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 432
    sub-int/2addr v0, v3

    int-to-float v3, v0

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 435
    const v4, 0x7f05001a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 429
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 436
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->sweepSize:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v1, v1

    div-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v2, v2

    div-float/2addr v2, v10

    .line 438
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 437
    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v1, v1

    div-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v2, v2

    div-float/2addr v2, v10

    .line 440
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 439
    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-lt v6, v0, :cond_4

    .line 452
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    if-ltz v0, :cond_2

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->y:F

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v0, v0, v2

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v3, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v4, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 458
    :cond_2
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mWalkCount:I

    if-eqz v0, :cond_3

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    .line 460
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mStartNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 459
    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    .line 462
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mEndNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 461
    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 466
    :cond_3
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    if-ltz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0, v1, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    .line 469
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 468
    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 445
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v2, v6, 0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v2, v6, 0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    .line 446
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v6

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v6

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050010

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x40a00000    # 5.0f

    div-float v3, v0, v3

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    if-gt v6, v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintCircle:Landroid/graphics/Paint;

    :goto_2
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v6

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v6

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v3, v6, 0x1

    aget-object v0, v0, v3

    iget v3, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v4, v6, 0x1

    aget-object v0, v0, v4

    iget v4, v0, Lcom/voovio/geometry/Point;->y:F

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    if-ge v6, v0, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mTravelledPaintLines:Landroid/graphics/Paint;

    :goto_3
    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 444
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 446
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintCircle:Landroid/graphics/Paint;

    goto :goto_2

    .line 447
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPaintLines:Landroid/graphics/Paint;

    goto :goto_3
.end method

.method public postShiftMiniMap([Lcom/voovio/geometry/Point;)Lcom/voovio/geometry/Point;
    .locals 11
    .param p1, "p"    # [Lcom/voovio/geometry/Point;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 596
    new-instance v7, Lcom/voovio/geometry/Point;

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    div-float/2addr v8, v10

    iget v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v9, v9

    div-float/2addr v9, v10

    invoke-direct {v7, v8, v9}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 599
    .local v7, "point":Lcom/voovio/geometry/Point;
    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    div-float v4, v8, v10

    .local v4, "maxY":F
    move v3, v4

    .local v3, "maxX":F
    move v6, v4

    .local v6, "minY":F
    move v5, v4

    .line 600
    .local v5, "minX":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, p1

    if-lt v2, v8, :cond_2

    .line 610
    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    sub-float v3, v8, v3

    .line 611
    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    sub-float v4, v8, v4

    .line 612
    add-float v8, v5, v3

    div-float v0, v8, v10

    .line 613
    .local v0, "distanceX":F
    add-float v8, v6, v4

    div-float v1, v8, v10

    .line 614
    .local v1, "distanceY":F
    cmpl-float v8, v5, v3

    if-lez v8, :cond_7

    .line 615
    iget v8, v7, Lcom/voovio/geometry/Point;->x:F

    sub-float v9, v5, v0

    sub-float/2addr v8, v9

    iput v8, v7, Lcom/voovio/geometry/Point;->x:F

    .line 618
    :cond_0
    :goto_1
    cmpl-float v8, v6, v4

    if-lez v8, :cond_8

    .line 619
    iget v8, v7, Lcom/voovio/geometry/Point;->y:F

    sub-float v9, v6, v1

    sub-float/2addr v8, v9

    iput v8, v7, Lcom/voovio/geometry/Point;->y:F

    .line 623
    :cond_1
    :goto_2
    iget v8, v7, Lcom/voovio/geometry/Point;->x:F

    iget v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v9, v9

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    iput v8, v7, Lcom/voovio/geometry/Point;->x:F

    .line 624
    iget v8, v7, Lcom/voovio/geometry/Point;->y:F

    iget v9, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v9, v9

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    iput v8, v7, Lcom/voovio/geometry/Point;->y:F

    .line 625
    return-object v7

    .line 601
    .end local v0    # "distanceX":F
    .end local v1    # "distanceY":F
    :cond_2
    aget-object v8, p1, v2

    iget v8, v8, Lcom/voovio/geometry/Point;->x:F

    cmpl-float v8, v5, v8

    if-lez v8, :cond_3

    .line 602
    aget-object v8, p1, v2

    iget v5, v8, Lcom/voovio/geometry/Point;->x:F

    .line 603
    :cond_3
    aget-object v8, p1, v2

    iget v8, v8, Lcom/voovio/geometry/Point;->x:F

    cmpg-float v8, v3, v8

    if-gez v8, :cond_4

    .line 604
    aget-object v8, p1, v2

    iget v3, v8, Lcom/voovio/geometry/Point;->x:F

    .line 605
    :cond_4
    aget-object v8, p1, v2

    iget v8, v8, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v8, v6, v8

    if-lez v8, :cond_5

    .line 606
    aget-object v8, p1, v2

    iget v6, v8, Lcom/voovio/geometry/Point;->y:F

    .line 607
    :cond_5
    aget-object v8, p1, v2

    iget v8, v8, Lcom/voovio/geometry/Point;->y:F

    cmpg-float v8, v4, v8

    if-gez v8, :cond_6

    .line 608
    aget-object v8, p1, v2

    iget v4, v8, Lcom/voovio/geometry/Point;->y:F

    .line 600
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 616
    .restart local v0    # "distanceX":F
    .restart local v1    # "distanceY":F
    :cond_7
    cmpl-float v8, v3, v5

    if-lez v8, :cond_0

    .line 617
    iget v8, v7, Lcom/voovio/geometry/Point;->x:F

    sub-float v9, v3, v0

    add-float/2addr v8, v9

    iput v8, v7, Lcom/voovio/geometry/Point;->x:F

    goto :goto_1

    .line 620
    :cond_8
    cmpl-float v8, v4, v6

    if-lez v8, :cond_1

    .line 621
    iget v8, v7, Lcom/voovio/geometry/Point;->y:F

    sub-float v9, v4, v1

    add-float/2addr v8, v9

    iput v8, v7, Lcom/voovio/geometry/Point;->y:F

    goto :goto_2
.end method

.method public preShiftMiniMap([Lcom/voovio/geometry/Point;)Lcom/voovio/geometry/Point;
    .locals 11
    .param p1, "p"    # [Lcom/voovio/geometry/Point;

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 635
    new-instance v6, Lcom/voovio/geometry/Point;

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    div-float/2addr v7, v9

    .line 636
    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    div-float/2addr v8, v9

    .line 635
    invoke-direct {v6, v7, v8}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 639
    .local v6, "point":Lcom/voovio/geometry/Point;
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    div-float v3, v7, v9

    .local v3, "maxY":F
    move v2, v3

    .local v2, "maxX":F
    move v5, v3

    .local v5, "minY":F
    move v4, v3

    .line 640
    .local v4, "minX":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, p1

    if-lt v1, v7, :cond_0

    .line 651
    cmpg-float v7, v4, v10

    if-gtz v7, :cond_6

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    cmpl-float v7, v2, v7

    if-ltz v7, :cond_6

    .line 652
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float v7, v2, v7

    sub-float/2addr v7, v4

    div-float v0, v7, v9

    .line 653
    .local v0, "diff":F
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v7, v7, v0

    if-lez v7, :cond_5

    .line 654
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    sub-float/2addr v8, v0

    add-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    .line 674
    :goto_1
    cmpg-float v7, v5, v10

    if-gtz v7, :cond_b

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    cmpl-float v7, v3, v7

    if-ltz v7, :cond_b

    .line 675
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float v7, v3, v7

    sub-float/2addr v7, v5

    div-float v0, v7, v9

    .line 676
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v7, v7, v0

    if-lez v7, :cond_a

    .line 677
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v8

    sub-float/2addr v8, v0

    add-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    .line 696
    :goto_2
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    .line 697
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    .line 698
    return-object v6

    .line 641
    .end local v0    # "diff":F
    :cond_0
    aget-object v7, p1, v1

    iget v7, v7, Lcom/voovio/geometry/Point;->x:F

    cmpl-float v7, v4, v7

    if-lez v7, :cond_1

    .line 642
    aget-object v7, p1, v1

    iget v4, v7, Lcom/voovio/geometry/Point;->x:F

    .line 643
    :cond_1
    aget-object v7, p1, v1

    iget v7, v7, Lcom/voovio/geometry/Point;->x:F

    cmpg-float v7, v2, v7

    if-gez v7, :cond_2

    .line 644
    aget-object v7, p1, v1

    iget v2, v7, Lcom/voovio/geometry/Point;->x:F

    .line 645
    :cond_2
    aget-object v7, p1, v1

    iget v7, v7, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v7, v5, v7

    if-lez v7, :cond_3

    .line 646
    aget-object v7, p1, v1

    iget v5, v7, Lcom/voovio/geometry/Point;->y:F

    .line 647
    :cond_3
    aget-object v7, p1, v1

    iget v7, v7, Lcom/voovio/geometry/Point;->y:F

    cmpg-float v7, v3, v7

    if-gez v7, :cond_4

    .line 648
    aget-object v7, p1, v1

    iget v3, v7, Lcom/voovio/geometry/Point;->y:F

    .line 640
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 656
    .restart local v0    # "diff":F
    :cond_5
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    sub-float v8, v2, v8

    sub-float/2addr v8, v0

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    goto :goto_1

    .line 658
    .end local v0    # "diff":F
    :cond_6
    cmpg-float v7, v4, v10

    if-gtz v7, :cond_7

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_7

    .line 659
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    sub-float/2addr v7, v4

    div-float v0, v7, v9

    .line 660
    .restart local v0    # "diff":F
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    add-float/2addr v7, v0

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    goto/16 :goto_1

    .line 662
    .end local v0    # "diff":F
    :cond_7
    cmpl-float v7, v4, v10

    if-ltz v7, :cond_9

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_9

    .line 663
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    add-float/2addr v7, v4

    div-float v0, v7, v9

    .line 664
    .restart local v0    # "diff":F
    cmpl-float v7, v4, v0

    if-lez v7, :cond_8

    .line 665
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    sub-float v8, v4, v0

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    goto/16 :goto_1

    .line 667
    :cond_8
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    sub-float/2addr v8, v2

    sub-float/2addr v8, v0

    add-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    goto/16 :goto_1

    .line 670
    .end local v0    # "diff":F
    :cond_9
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float v7, v2, v7

    add-float/2addr v7, v4

    div-float v0, v7, v9

    .line 671
    .restart local v0    # "diff":F
    iget v7, v6, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v7, v0

    iput v7, v6, Lcom/voovio/geometry/Point;->x:F

    goto/16 :goto_1

    .line 679
    :cond_a
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    sub-float v8, v3, v8

    sub-float/2addr v8, v0

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_2

    .line 681
    :cond_b
    cmpg-float v7, v5, v10

    if-gtz v7, :cond_c

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gtz v7, :cond_c

    .line 682
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float/2addr v7, v3

    sub-float/2addr v7, v5

    div-float v0, v7, v9

    .line 683
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    add-float/2addr v7, v0

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_2

    .line 685
    :cond_c
    cmpl-float v7, v5, v10

    if-ltz v7, :cond_e

    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gtz v7, :cond_e

    .line 686
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float/2addr v7, v3

    add-float/2addr v7, v5

    div-float v0, v7, v9

    .line 687
    cmpl-float v7, v5, v0

    if-lez v7, :cond_d

    .line 688
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    sub-float v8, v5, v0

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_2

    .line 690
    :cond_d
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    iget v8, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v8, v8

    sub-float/2addr v8, v3

    sub-float/2addr v8, v0

    add-float/2addr v7, v8

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_2

    .line 693
    :cond_e
    iget v7, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v7, v7

    sub-float v7, v3, v7

    add-float/2addr v7, v5

    div-float v0, v7, v9

    .line 694
    iget v7, v6, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v7, v0

    iput v7, v6, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_2
.end method

.method public setAngles(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p1, "angles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    .line 186
    return-void
.end method

.method public setCurrentPosition(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodesInfo:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iput p1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentPosition:I

    .line 286
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    .line 287
    :goto_1
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget v0, v0, v2

    if-gt p1, v0, :cond_5

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget v0, v0, v2

    if-eq p1, v0, :cond_3

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    .line 292
    :cond_3
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    if-ltz v0, :cond_0

    .line 294
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentPosition:I

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget v2, v2, v3

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPositionsForEachNode:[I

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget v2, v2, v3

    int-to-float v2, v2

    div-float v6, v0, v2

    .line 296
    .local v6, "fPercentage":F
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget v1, v0, v2

    .line 297
    .local v1, "fAngle":F
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    iput v2, v0, Lcom/voovio/geometry/Point;->x:F

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    iput v2, v0, Lcom/voovio/geometry/Point;->y:F

    .line 302
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v2, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v4, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/voovio/geometry/Point;->x:F

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v4, v4, v5

    iget v4, v4, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v0, Lcom/voovio/geometry/Point;->x:F

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v4, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    iget v5, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget-object v4, v4, v5

    iget v4, v4, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v0, Lcom/voovio/geometry/Point;->y:F

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    add-int/lit8 v2, v2, 0x1

    aget v0, v0, v2

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    aget v2, v2, v3

    sub-float/2addr v0, v2

    mul-float/2addr v0, v6

    add-float/2addr v1, v0

    .line 308
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v4, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v5, v0, Lcom/voovio/geometry/Point;->y:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/tourviewer/player/FullMapView;->computeMatrix(FFFFF)V

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->invalidate()V

    goto/16 :goto_0

    .line 288
    .end local v1    # "fAngle":F
    .end local v6    # "fPercentage":F
    :cond_5
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mCurrentNode:I

    goto/16 :goto_1
.end method

.method public setSweep(Lcom/voovio/sweep/Sweep;[I)V
    .locals 18
    .param p1, "oSweep"    # Lcom/voovio/sweep/Sweep;
    .param p2, "aNodePositions"    # [I

    .prologue
    .line 196
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/voovio/sweep/Sweep;->getNodesInfo()Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodesInfo:Ljava/util/ArrayList;

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getAngles()Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    .line 203
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    .line 204
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodesInfo:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 207
    .local v10, "size":I
    new-array v12, v10, [Lcom/voovio/geometry/Point;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    .line 208
    new-array v12, v10, [Lcom/voovio/geometry/Point;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodeProxyPoints:[Lcom/voovio/geometry/Point;

    .line 209
    new-array v12, v10, [F

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    .line 211
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    const/4 v13, 0x0

    new-instance v14, Lcom/voovio/geometry/Point;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    int-to-float v15, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->miniMapSize:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    invoke-direct/range {v14 .. v16}, Lcom/voovio/geometry/Point;-><init>(FF)V

    aput-object v14, v12, v13

    .line 212
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput v14, v12, v13

    .line 214
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_1
    if-lt v4, v10, :cond_2

    .line 228
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/tourviewer/player/FullMapView;->preShiftMiniMap([Lcom/voovio/geometry/Point;)Lcom/voovio/geometry/Point;

    move-result-object v8

    .line 229
    .local v8, "preShift":Lcom/voovio/geometry/Point;
    const/4 v6, 0x0

    .local v6, "k":I
    :goto_2
    if-lt v6, v10, :cond_4

    .line 235
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodeProxyPoints:[Lcom/voovio/geometry/Point;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/tourviewer/player/FullMapView;->arrowLength([Lcom/voovio/geometry/Point;)F

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPathLength:F

    .line 238
    new-instance v11, Lcom/voovio/geometry/Point;

    invoke-direct {v11}, Lcom/voovio/geometry/Point;-><init>()V

    .line 239
    .local v11, "vDir":Lcom/voovio/geometry/Point;
    const/4 v6, 0x1

    :goto_3
    if-lt v6, v10, :cond_5

    .line 259
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/tourviewer/player/FullMapView;->postShiftMiniMap([Lcom/voovio/geometry/Point;)Lcom/voovio/geometry/Point;

    move-result-object v9

    .line 260
    .local v9, "ptShift":Lcom/voovio/geometry/Point;
    const/4 v6, 0x0

    :goto_4
    if-lt v6, v10, :cond_8

    .line 265
    new-array v12, v10, [I

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPositionsForEachNode:[I

    .line 266
    const/4 v4, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    array-length v12, v12

    add-int/lit8 v12, v12, -0x1

    if-lt v4, v12, :cond_9

    .line 268
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPositionsForEachNode:[I

    add-int/lit8 v13, v10, -0x1

    const/4 v14, 0x0

    aput v14, v12, v13

    .line 269
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mWalkCount:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setTotalWalkCount(I)V

    goto/16 :goto_0

    .line 215
    .end local v6    # "k":I
    .end local v8    # "preShift":Lcom/voovio/geometry/Point;
    .end local v9    # "ptShift":Lcom/voovio/geometry/Point;
    .end local v11    # "vDir":Lcom/voovio/geometry/Point;
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->newPoint()Lcom/voovio/geometry/Point;

    move-result-object v13

    aput-object v13, v12, v4

    .line 217
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodesInfo:Ljava/util/ArrayList;

    add-int/lit8 v13, v4, -0x1

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/voovio/sweep/Sweep$NodeInfo;

    .line 218
    .local v7, "oNode":Lcom/voovio/sweep/Sweep$NodeInfo;
    iget v12, v7, Lcom/voovio/sweep/Sweep$NodeInfo;->m_nType:I

    if-nez v12, :cond_3

    .line 219
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v13, v4, -0x1

    aget-object v13, v12, v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPathLength:F

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    add-int/lit8 v15, v4, -0x1

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    mul-float/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    add-int/lit8 v15, v4, -0x1

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v15, v15, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12, v14, v15}, Lcom/sec/android/app/tourviewer/player/FullMapView;->findNextPoint(Lcom/voovio/geometry/Point;FFLcom/voovio/geometry/Point;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    add-int/lit8 v14, v4, -0x1

    aget v13, v13, v14

    aput v13, v12, v4

    .line 222
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mWalkCount:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mWalkCount:I

    .line 214
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 224
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v14, v4, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v12, v13}, Lcom/voovio/geometry/Point;->copy(Lcom/voovio/geometry/Point;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mRotationAngles:[F

    add-int/lit8 v14, v4, -0x1

    aget v13, v13, v14

    iget v14, v7, Lcom/voovio/sweep/Sweep$NodeInfo;->m_fAngle:F

    float-to-double v14, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v14

    double-to-float v14, v14

    add-float/2addr v13, v14

    aput v13, v12, v4

    goto :goto_6

    .line 230
    .end local v7    # "oNode":Lcom/voovio/sweep/Sweep$NodeInfo;
    .restart local v6    # "k":I
    .restart local v8    # "preShift":Lcom/voovio/geometry/Point;
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodeProxyPoints:[Lcom/voovio/geometry/Point;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->newPoint()Lcom/voovio/geometry/Point;

    move-result-object v13

    aput-object v13, v12, v6

    .line 231
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodeProxyPoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v13, v13, v6

    iget v13, v13, Lcom/voovio/geometry/Point;->x:F

    iget v14, v8, Lcom/voovio/geometry/Point;->x:F

    add-float/2addr v13, v14

    iput v13, v12, Lcom/voovio/geometry/Point;->x:F

    .line 232
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodeProxyPoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v13, v13, v6

    iget v13, v13, Lcom/voovio/geometry/Point;->y:F

    iget v14, v8, Lcom/voovio/geometry/Point;->y:F

    add-float/2addr v13, v14

    iput v13, v12, Lcom/voovio/geometry/Point;->y:F

    .line 229
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    .line 240
    .restart local v11    # "vDir":Lcom/voovio/geometry/Point;
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v13, v6, -0x1

    aget-object v12, v12, v13

    iget v12, v12, Lcom/voovio/geometry/Point;->x:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v13, v13, v6

    iget v13, v13, Lcom/voovio/geometry/Point;->x:F

    cmpl-float v12, v12, v13

    if-nez v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v13, v6, -0x1

    aget-object v12, v12, v13

    iget v12, v12, Lcom/voovio/geometry/Point;->y:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v13, v13, v6

    iget v13, v13, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v12, v12, v13

    if-nez v12, :cond_7

    .line 239
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    .line 243
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v13, v6, -0x1

    aget-object v12, v12, v13

    iget v12, v12, Lcom/voovio/geometry/Point;->x:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v13, v13, v6

    iget v13, v13, Lcom/voovio/geometry/Point;->x:F

    sub-float v2, v12, v13

    .line 244
    .local v2, "fx":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    add-int/lit8 v13, v6, -0x1

    aget-object v12, v12, v13

    iget v12, v12, Lcom/voovio/geometry/Point;->y:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v13, v13, v6

    iget v13, v13, Lcom/voovio/geometry/Point;->y:F

    sub-float v3, v12, v13

    .line 246
    .local v3, "fy":F
    neg-float v12, v2

    iput v12, v11, Lcom/voovio/geometry/Point;->x:F

    .line 247
    neg-float v12, v3

    iput v12, v11, Lcom/voovio/geometry/Point;->y:F

    .line 248
    invoke-virtual {v11}, Lcom/voovio/geometry/Point;->normalize()F

    .line 250
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPathLength:F

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    add-int/lit8 v14, v6, -0x1

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    mul-float/2addr v12, v13

    iget v13, v11, Lcom/voovio/geometry/Point;->x:F

    mul-float/2addr v12, v13

    add-float/2addr v2, v12

    .line 251
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPathLength:F

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->angles:Ljava/util/ArrayList;

    add-int/lit8 v14, v6, -0x1

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    mul-float/2addr v12, v13

    iget v13, v11, Lcom/voovio/geometry/Point;->y:F

    mul-float/2addr v12, v13

    add-float/2addr v3, v12

    .line 253
    move v5, v6

    .local v5, "j":I
    :goto_7
    if-ge v5, v10, :cond_6

    .line 254
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v5

    iget v13, v12, Lcom/voovio/geometry/Point;->x:F

    add-float/2addr v13, v2

    iput v13, v12, Lcom/voovio/geometry/Point;->x:F

    .line 255
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v5

    iget v13, v12, Lcom/voovio/geometry/Point;->y:F

    add-float/2addr v13, v3

    iput v13, v12, Lcom/voovio/geometry/Point;->y:F

    .line 253
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 261
    .end local v2    # "fx":F
    .end local v3    # "fy":F
    .end local v5    # "j":I
    .restart local v9    # "ptShift":Lcom/voovio/geometry/Point;
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v6

    iget v13, v12, Lcom/voovio/geometry/Point;->x:F

    iget v14, v9, Lcom/voovio/geometry/Point;->x:F

    add-float/2addr v13, v14

    iput v13, v12, Lcom/voovio/geometry/Point;->x:F

    .line 262
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePoints:[Lcom/voovio/geometry/Point;

    aget-object v12, v12, v6

    iget v13, v12, Lcom/voovio/geometry/Point;->y:F

    iget v14, v9, Lcom/voovio/geometry/Point;->y:F

    add-float/2addr v13, v14

    iput v13, v12, Lcom/voovio/geometry/Point;->y:F

    .line 260
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_4

    .line 267
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mPositionsForEachNode:[I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    add-int/lit8 v14, v4, 0x1

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/tourviewer/player/FullMapView;->mNodePositions:[I

    aget v14, v14, v4

    sub-int/2addr v13, v14

    aput v13, v12, v4

    .line 266
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5
.end method

.method public setTotalWalkCount(I)V
    .locals 0
    .param p1, "totalWalkCount"    # I

    .prologue
    .line 705
    iput p1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->totalWalkCount:I

    .line 706
    return-void
.end method

.method public updateDraw(I)V
    .locals 1
    .param p1, "sweepSize"    # I

    .prologue
    .line 709
    iput p1, p0, Lcom/sec/android/app/tourviewer/player/FullMapView;->sweepSize:I

    .line 710
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->invalidate()V

    .line 712
    :cond_0
    return-void
.end method
