.class public Lcom/sec/android/app/tourviewer/utils/ProgressFragment;
.super Landroid/app/DialogFragment;
.source "ProgressFragment.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private dialogBoxWidthLandscape:I

.field private dialogboxHeight:I

.field private orientation:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "orientation"    # I
    .param p2, "dialogboxHeight"    # I
    .param p3, "dialogBoxWidthLandscape"    # I

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 18
    iput v0, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogboxHeight:I

    .line 19
    iput v0, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogBoxWidthLandscape:I

    .line 25
    iput p1, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->orientation:I

    .line 26
    iput p2, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogboxHeight:I

    .line 27
    iput p3, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogBoxWidthLandscape:I

    .line 28
    return-void
.end method

.method public static isDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/app/tourviewer/utils/Constant;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isLdevice()Z
    .locals 2

    .prologue
    .line 93
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 94
    .local v0, "currentapiVersion":I
    const/16 v1, 0x13

    if-le v0, v1, :cond_0

    .line 95
    const/4 v1, 0x1

    .line 97
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLightThemeRequired()Z
    .locals 1

    .prologue
    .line 102
    const-string v0, "tr"

    invoke-static {v0}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    const-string v0, "tb"

    invoke-static {v0}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "a5"

    invoke-static {v0}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "a7"

    invoke-static {v0}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isLdevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x1

    .line 105
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 31
    invoke-super {p0, p3}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const/4 v1, 0x0

    .line 34
    .local v1, "view":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isLightThemeRequired()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 35
    const v3, 0x7f030004

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 41
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 42
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 43
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 45
    invoke-static {}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->isLightThemeRequired()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 46
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x7f02002b

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 51
    :goto_1
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 52
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 54
    .local v2, "window":Landroid/view/Window;
    iget v3, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->orientation:I

    if-ne v3, v5, :cond_3

    .line 55
    const/4 v3, -0x1

    iget v4, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogboxHeight:I

    invoke-virtual {v2, v3, v4}, Landroid/view/Window;->setLayout(II)V

    .line 60
    :cond_0
    :goto_2
    return-object v1

    .line 37
    .end local v0    # "dialog":Landroid/app/Dialog;
    .end local v2    # "window":Landroid/view/Window;
    :cond_1
    const v3, 0x7f030003

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 48
    .restart local v0    # "dialog":Landroid/app/Dialog;
    :cond_2
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x7f02002a

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto :goto_1

    .line 56
    .restart local v2    # "window":Landroid/view/Window;
    :cond_3
    iget v3, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 57
    iget v3, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogBoxWidthLandscape:I

    iget v4, p0, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dialogboxHeight:I

    invoke-virtual {v2, v3, v4}, Landroid/view/Window;->setLayout(II)V

    goto :goto_2
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 73
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 79
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 85
    return-void
.end method
