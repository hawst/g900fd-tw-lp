.class Lcom/sec/android/app/tourviewer/TourPlayerActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "TourPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/tourviewer/TourPlayerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$1;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    .line 242
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 246
    const-string v1, "state"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "state":Ljava/lang/String;
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$1;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v1, v1, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$1;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v1, v1, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const v2, 0x7fffffff

    invoke-virtual {v1, v2}, Lcom/voovio/sweep/SweepView;->autodriveTo(I)Z

    goto :goto_0
.end method
