.class Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;
.super Landroid/os/AsyncTask;
.source "TourPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/tourviewer/TourPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSweepTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field m_oContext:Landroid/content/Context;

.field m_oSweep:Lcom/voovio/sweep/Sweep;

.field final synthetic this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/tourviewer/TourPlayerActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 947
    iput-object p1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 942
    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oContext:Landroid/content/Context;

    .line 943
    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oSweep:Lcom/voovio/sweep/Sweep;

    .line 948
    iput-object p2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oContext:Landroid/content/Context;

    .line 949
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .param p1, "strFilePaths"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 955
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    new-instance v1, Lcom/samsung/sef/SEFParserDecode;

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/sef/SEFParserDecode;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->oSEFParser:Lcom/samsung/sef/SEFParserDecode;

    .line 956
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v0, v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->oSEFParser:Lcom/samsung/sef/SEFParserDecode;

    aget-object v1, p1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/sef/SEFParserDecode;->decodeSEF(Ljava/lang/String;)Lcom/voovio/sweep/Sweep;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oSweep:Lcom/voovio/sweep/Sweep;

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    # getter for: Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;
    invoke-static {v0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->access$0(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)Lcom/sec/android/app/tourviewer/player/FullMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v1, v1, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->oSEFParser:Lcom/samsung/sef/SEFParserDecode;

    invoke-virtual {v1}, Lcom/samsung/sef/SEFParserDecode;->getmAngles()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setAngles(Ljava/util/ArrayList;)V

    .line 958
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    # getter for: Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;
    invoke-static {v0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->access$1(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)Lcom/sec/android/app/tourviewer/player/DetailMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v1, v1, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->oSEFParser:Lcom/samsung/sef/SEFParserDecode;

    invoke-virtual {v1}, Lcom/samsung/sef/SEFParserDecode;->getmAngles()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setAngles(Ljava/util/ArrayList;)V

    .line 959
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    invoke-static {v0, v3}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->access$2(Lcom/sec/android/app/tourviewer/TourPlayerActivity;Z)V

    .line 960
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->appStateVar:Z

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v0, v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/SweepView;->autodriveTo(I)Z

    .line 970
    :goto_0
    const-string v0, "onResume"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->appStateVar:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v0, v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->onResume()V

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oSweep:Lcom/voovio/sweep/Sweep;

    if-eqz v0, :cond_1

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oSweep:Lcom/voovio/sweep/Sweep;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->setSweep(Lcom/voovio/sweep/Sweep;)V

    .line 978
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    invoke-static {v0, v3}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->access$3(Lcom/sec/android/app/tourviewer/TourPlayerActivity;Z)V

    .line 979
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 968
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    iget-object v0, v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    goto :goto_0

    .line 976
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->this$0:Lcom/sec/android/app/tourviewer/TourPlayerActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->finish()V

    goto :goto_1
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v0, 0x0

    .line 984
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 985
    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oSweep:Lcom/voovio/sweep/Sweep;

    .line 986
    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->m_oContext:Landroid/content/Context;

    .line 987
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
