.class public Lcom/sec/android/app/tourviewer/utils/Constant;
.super Ljava/lang/Object;
.source "Constant.java"


# static fields
.field public static final DETAIL_MAP_PATH_LENTH:I = 0x3c

.field public static final DEVICE_NAME:Ljava/lang/String;

.field public static final EG_VT_WALK_INDICATOR:F = 1000.0f

.field public static final FULL_MAP_PATH_LENTH:I = 0x32

.field public static final NODE_TYPE_NORMAL:I = 0x0

.field public static final NODE_TYPE_TURN:I = 0x1

.field public static final SELECTED_SWEEP_KEY:Ljava/lang/String; = "sef_file_name"

.field public static final VIRTUAL_TOUR_HEADER:Ljava/lang/String; = "VirtualTour_Info"

.field public static final VIRTUAL_TOUR_INFO:I = 0x850


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/tourviewer/utils/Constant;->DEVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
