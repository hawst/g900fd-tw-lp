.class public final Lcom/sec/android/app/tourviewer/player/DetailMapView;
.super Landroid/widget/RelativeLayout;
.source "DetailMapView.java"


# instance fields
.field private CurrentNodeBitmap:Landroid/graphics/Bitmap;

.field private CurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

.field private CurrentNodeTopBitmap:Landroid/graphics/Bitmap;

.field private CurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

.field private EndNodeBitmapSize2:Lcom/voovio/geometry/Point;

.field private StartNodeBitmapSize2:Lcom/voovio/geometry/Point;

.field private Xshift:F

.field private Yshift:F

.field private angleOfRoations:[F

.field private angles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private backLine:Landroid/graphics/Paint;

.field private currentIndex:I

.field private endNodeBitMap:Landroid/graphics/Bitmap;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mNodePositions:[I

.field protected mNodesInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/voovio/sweep/Sweep$NodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPoint:Lcom/voovio/geometry/Point;

.field private mWalkCount:I

.field private m_oPaintCircle:Landroid/graphics/Paint;

.field private m_oPaintLines:Landroid/graphics/Paint;

.field private m_oTravelledPaintCircle:Landroid/graphics/Paint;

.field private m_oTravelledPaintLines:Landroid/graphics/Paint;

.field private miniMapSize:I

.field private mwalkCounter:[I

.field private pathLength:I

.field private points:[Lcom/voovio/geometry/Point;

.field private pointsForEachImage:[I

.field private proxyPoints:[Lcom/voovio/geometry/Point;

.field private startNodeBitMap:Landroid/graphics/Bitmap;

.field private sweepSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, -0x1

    const v11, 0x7f05000e

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 107
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 81
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    .line 97
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->sweepSize:I

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 109
    const v6, 0x7f050007

    .line 108
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    .line 111
    invoke-virtual {p0, v9}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setBackgroundColor(I)V

    .line 112
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 113
    iget v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    iget v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    .line 112
    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 114
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 115
    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 116
    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 118
    const v6, 0x7f050001

    .line 117
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 119
    .local v2, "pixels":I
    invoke-virtual {v0, v9, v2, v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 120
    invoke-virtual {p0, v0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angles:Ljava/util/ArrayList;

    .line 124
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    .line 125
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 126
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 127
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    const-string v6, "#66CC00"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 130
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    .line 131
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 132
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 133
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 136
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintCircle:Landroid/graphics/Paint;

    .line 137
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintCircle:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 138
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 139
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintCircle:Landroid/graphics/Paint;

    const-string v6, "#66CC00"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 140
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 142
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    .line 143
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 144
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 145
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 146
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 148
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    .line 149
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    const-string v6, "#33ffffff"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 150
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 151
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 152
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 154
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    .line 155
    iput v9, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pathLength:I

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 158
    .local v4, "resources":Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "packageName":Ljava/lang/String;
    const-string v5, "detailedmap_current_dot"

    .line 161
    const-string v6, "drawable"

    .line 160
    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 162
    .local v3, "resid":I
    if-eqz v3, :cond_0

    .line 163
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmap:Landroid/graphics/Bitmap;

    .line 164
    new-instance v5, Lcom/voovio/geometry/Point;

    .line 165
    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    .line 166
    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 164
    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    .line 169
    :cond_0
    const-string v5, "detailedmap_current_light"

    .line 170
    const-string v6, "drawable"

    .line 169
    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 171
    if-eqz v3, :cond_1

    .line 172
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    .line 174
    new-instance v5, Lcom/voovio/geometry/Point;

    .line 175
    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    .line 176
    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 174
    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    .line 179
    :cond_1
    const-string v5, "detail_start_dot"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 181
    if-eqz v3, :cond_2

    .line 182
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->startNodeBitMap:Landroid/graphics/Bitmap;

    .line 183
    new-instance v5, Lcom/voovio/geometry/Point;

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->startNodeBitMap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    .line 184
    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->startNodeBitMap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 183
    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->StartNodeBitmapSize2:Lcom/voovio/geometry/Point;

    .line 187
    :cond_2
    const-string v5, "detail_end_dot"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 189
    if-eqz v3, :cond_3

    .line 190
    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->endNodeBitMap:Landroid/graphics/Bitmap;

    .line 191
    new-instance v5, Lcom/voovio/geometry/Point;

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->endNodeBitMap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    .line 192
    iget-object v7, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->endNodeBitMap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-direct {v5, v6, v7}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 191
    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->EndNodeBitmapSize2:Lcom/voovio/geometry/Point;

    .line 196
    :cond_3
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mMatrix:Landroid/graphics/Matrix;

    .line 197
    new-instance v5, Lcom/voovio/geometry/Point;

    invoke-direct {v5}, Lcom/voovio/geometry/Point;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mPoint:Lcom/voovio/geometry/Point;

    .line 198
    return-void
.end method

.method private computeMatrix(FFFFF)V
    .locals 3
    .param p1, "angle"    # F
    .param p2, "rotCenterX"    # F
    .param p3, "rotCenterY"    # F
    .param p4, "posX"    # F
    .param p5, "posY"    # F

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mMatrix:Landroid/graphics/Matrix;

    neg-float v1, p2

    neg-float v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p4, p5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 452
    return-void
.end method

.method private findNextPoint(Lcom/voovio/geometry/Point;FFLcom/voovio/geometry/Point;)V
    .locals 3
    .param p1, "startPoint"    # Lcom/voovio/geometry/Point;
    .param p2, "distance"    # F
    .param p3, "angle"    # F
    .param p4, "nextPoint"    # Lcom/voovio/geometry/Point;

    .prologue
    .line 275
    iget v0, p1, Lcom/voovio/geometry/Point;->x:F

    .line 276
    float-to-double v1, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    .line 275
    iput v0, p4, Lcom/voovio/geometry/Point;->x:F

    .line 277
    iget v0, p1, Lcom/voovio/geometry/Point;->y:F

    .line 278
    float-to-double v1, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p2

    sub-float/2addr v0, v1

    .line 277
    iput v0, p4, Lcom/voovio/geometry/Point;->y:F

    .line 279
    return-void
.end method


# virtual methods
.method public countWalk()I
    .locals 5

    .prologue
    const/high16 v4, 0x447a0000    # 1000.0f

    .line 425
    const/4 v0, 0x0

    .line 426
    .local v0, "count":I
    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-nez v2, :cond_1

    .line 427
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    cmpl-float v2, v2, v4

    if-nez v2, :cond_0

    .line 428
    const/4 v0, 0x1

    .line 436
    :cond_0
    return v0

    .line 430
    :cond_1
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-gt v1, v2, :cond_0

    .line 431
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    aget v2, v2, v1

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    .line 432
    add-int/lit8 v0, v0, 0x1

    .line 430
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getAngles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public newPoint()Lcom/voovio/geometry/Point;
    .locals 1

    .prologue
    .line 460
    new-instance v0, Lcom/voovio/geometry/Point;

    invoke-direct {v0}, Lcom/voovio/geometry/Point;-><init>()V

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    const v9, 0x7f05001e

    const/4 v2, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 348
    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f05001b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 352
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 348
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f05001c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 357
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 353
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f05001d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 362
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    move-object v0, p1

    .line 358
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v5, v0

    .line 367
    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    move-object v1, p1

    .line 363
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 370
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v5, v0

    .line 372
    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    move-object v1, p1

    .line 368
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v5, v0

    .line 377
    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->backLine:Landroid/graphics/Paint;

    move-object v1, p1

    .line 373
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 379
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->sweepSize:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v1, v1

    div-float/2addr v1, v8

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v2, v2

    div-float/2addr v2, v8

    .line 381
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 380
    invoke-virtual {p1, v0, v1, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v1, v1

    div-float/2addr v1, v8

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v2, v2

    div-float/2addr v2, v8

    .line 383
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 382
    invoke-virtual {p1, v0, v1, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v11

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v11

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    iget v3, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v11

    iget v4, v0, Lcom/voovio/geometry/Point;->y:F

    .line 387
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    move-object v0, p1

    .line 386
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 388
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt v7, v0, :cond_4

    .line 400
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-ltz v0, :cond_2

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget-object v0, v0, v2

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    .line 402
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v0, v0

    div-float v3, v0, v8

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v0, v0

    div-float v4, v0, v8

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    move-object v0, p1

    .line 401
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->y:F

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v8

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 406
    :cond_2
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    if-eqz v0, :cond_3

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->endNodeBitMap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->EndNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    .line 409
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->EndNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 407
    invoke-virtual {p1, v0, v1, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->startNodeBitMap:Landroid/graphics/Bitmap;

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v1, v1, v11

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->StartNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v11

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    .line 412
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->StartNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 410
    invoke-virtual {p1, v0, v1, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 415
    :cond_3
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-ltz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0, v1, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v1, v1

    div-float/2addr v1, v8

    .line 418
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v2, v2

    div-float/2addr v2, v8

    .line 419
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v3, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v2, v3

    .line 417
    invoke-virtual {p1, v0, v1, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 389
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v7

    iget v0, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v2, v7, 0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v7

    iget v0, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v2, v7, 0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    .line 391
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v7

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v7

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v3, v7, 0x1

    aget-object v0, v0, v3

    iget v3, v0, Lcom/voovio/geometry/Point;->x:F

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v4, v7, 0x1

    aget-object v0, v0, v4

    iget v4, v0, Lcom/voovio/geometry/Point;->y:F

    .line 393
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-ge v7, v0, :cond_7

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintLines:Landroid/graphics/Paint;

    :goto_2
    move-object v0, p1

    .line 391
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v1, v7, 0x1

    aget-object v0, v0, v1

    iget v1, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v2, v7, 0x1

    aget-object v0, v0, v2

    iget v2, v0, Lcom/voovio/geometry/Point;->y:F

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050010

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    div-float v3, v0, v8

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-ge v7, v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oTravelledPaintCircle:Landroid/graphics/Paint;

    :goto_3
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 388
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 394
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintLines:Landroid/graphics/Paint;

    goto :goto_2

    .line 396
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->m_oPaintCircle:Landroid/graphics/Paint;

    goto :goto_3
.end method

.method public setAngles(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 205
    .local p1, "angles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angles:Ljava/util/ArrayList;

    .line 206
    return-void
.end method

.method public setCurrentPosition(I)V
    .locals 9
    .param p1, "position"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    .line 292
    :goto_1
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget v0, v0, v2

    .line 292
    if-gt p1, v0, :cond_6

    .line 296
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget v0, v0, v2

    if-eq p1, v0, :cond_3

    .line 297
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    .line 299
    :cond_3
    new-instance v8, Lcom/voovio/geometry/Point;

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-direct {v8, v0, v2}, Lcom/voovio/geometry/Point;-><init>(FF)V

    .line 301
    .local v8, "p":Lcom/voovio/geometry/Point;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v0, v0

    if-lt v7, v0, :cond_7

    .line 306
    const/4 v7, 0x0

    :goto_3
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    add-int/lit8 v0, v0, 0x1

    if-lt v7, v0, :cond_8

    .line 312
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 313
    iget v0, v8, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Xshift:F

    .line 314
    iget v0, v8, Lcom/voovio/geometry/Point;->y:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Yshift:F

    .line 317
    :cond_4
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    if-ltz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget v0, v0, v2

    sub-int v0, p1, v0

    int-to-float v0, v0

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pointsForEachImage:[I

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget v2, v2, v3

    int-to-float v2, v2

    .line 319
    div-float v6, v0, v2

    .line 322
    .local v6, "fPercentage":F
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget v1, v0, v2

    .line 324
    .local v1, "fAngle":F
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Xshift:F

    mul-float/2addr v2, v6

    iput v2, v0, Lcom/voovio/geometry/Point;->x:F

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Yshift:F

    mul-float/2addr v2, v6

    iput v2, v0, Lcom/voovio/geometry/Point;->y:F

    .line 327
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v0, v0, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mPoint:Lcom/voovio/geometry/Point;

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->shiftPoints(FF)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    add-int/lit8 v2, v2, 0x1

    aget v0, v0, v2

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    aget v2, v2, v3

    sub-float/2addr v0, v2

    mul-float/2addr v0, v6

    add-float/2addr v1, v0

    .line 333
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v2, v0, Lcom/voovio/geometry/Point;->x:F

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->CurrentNodeTopBitmapSize2:Lcom/voovio/geometry/Point;

    iget v3, v0, Lcom/voovio/geometry/Point;->y:F

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v0, v0

    div-float v4, v0, v5

    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v0, v0

    div-float v5, v0, v5

    move-object v0, p0

    .line 333
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->computeMatrix(FFFFF)V

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->invalidate()V

    goto/16 :goto_0

    .line 294
    .end local v1    # "fAngle":F
    .end local v6    # "fPercentage":F
    .end local v7    # "i":I
    .end local v8    # "p":Lcom/voovio/geometry/Point;
    :cond_6
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->currentIndex:I

    goto/16 :goto_1

    .line 302
    .restart local v7    # "i":I
    .restart local v8    # "p":Lcom/voovio/geometry/Point;
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v7

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->proxyPoints:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v7

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    iput v2, v0, Lcom/voovio/geometry/Point;->x:F

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v0, v0, v7

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->proxyPoints:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v7

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    iput v2, v0, Lcom/voovio/geometry/Point;->y:F

    .line 301
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 307
    :cond_8
    iget v0, v8, Lcom/voovio/geometry/Point;->x:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v7

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Xshift:F

    .line 308
    iget v0, v8, Lcom/voovio/geometry/Point;->y:F

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v7

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Yshift:F

    .line 309
    iget v0, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Xshift:F

    iget v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->Yshift:F

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->shiftPoints(FF)V

    .line 306
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3
.end method

.method public setSweep(Lcom/voovio/sweep/Sweep;[I)V
    .locals 9
    .param p1, "oSweep"    # Lcom/voovio/sweep/Sweep;
    .param p2, "aNodePositions"    # [I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 216
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getAngles()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angles:Ljava/util/ArrayList;

    .line 219
    invoke-virtual {p1}, Lcom/voovio/sweep/Sweep;->getNodesInfo()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    .line 220
    iput-object p2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    .line 222
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 223
    .local v2, "size":I
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 224
    const v4, 0x7f05000c

    .line 223
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pathLength:I

    .line 227
    new-array v3, v2, [Lcom/voovio/geometry/Point;

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    .line 228
    new-array v3, v2, [F

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    .line 229
    new-array v3, v2, [I

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mwalkCounter:[I

    .line 230
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    new-instance v4, Lcom/voovio/geometry/Point;

    iget v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v5, v5

    div-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->miniMapSize:I

    int-to-float v6, v6

    div-float/2addr v6, v8

    invoke-direct {v4, v5, v6}, Lcom/voovio/geometry/Point;-><init>(FF)V

    aput-object v4, v3, v7

    .line 231
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    const/4 v4, 0x0

    aput v4, v3, v7

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mwalkCounter:[I

    iget v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    aput v4, v3, v7

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v3, v3

    new-array v3, v3, [Lcom/voovio/geometry/Point;

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->proxyPoints:[Lcom/voovio/geometry/Point;

    .line 234
    new-array v3, v2, [I

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mwalkCounter:[I

    .line 236
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-lt v0, v2, :cond_2

    .line 254
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v3, v3

    if-lt v0, v3, :cond_4

    .line 260
    new-array v3, v2, [I

    iput-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pointsForEachImage:[I

    .line 261
    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_5

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pointsForEachImage:[I

    add-int/lit8 v4, v2, -0x1

    aput v7, v3, v4

    goto :goto_0

    .line 237
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->newPoint()Lcom/voovio/geometry/Point;

    move-result-object v4

    aput-object v4, v3, v0

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodesInfo:Ljava/util/ArrayList;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/voovio/sweep/Sweep$NodeInfo;

    .line 240
    .local v1, "oNode":Lcom/voovio/sweep/Sweep$NodeInfo;
    iget v3, v1, Lcom/voovio/sweep/Sweep$NodeInfo;->m_nType:I

    if-nez v3, :cond_3

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v4, v0, -0x1

    aget-object v4, v3, v4

    iget v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pathLength:I

    int-to-float v5, v3

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angles:Ljava/util/ArrayList;

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    mul-float/2addr v3, v5

    .line 242
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    add-int/lit8 v6, v0, -0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v6, v6, v0

    .line 241
    invoke-direct {p0, v4, v3, v5, v6}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->findNextPoint(Lcom/voovio/geometry/Point;FFLcom/voovio/geometry/Point;)V

    .line 243
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    add-int/lit8 v5, v0, -0x1

    aget v4, v4, v5

    aput v4, v3, v0

    .line 244
    iget v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    .line 245
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mwalkCounter:[I

    iget v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    aput v4, v3, v0

    .line 236
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 247
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    add-int/lit8 v5, v0, -0x1

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Lcom/voovio/geometry/Point;->copy(Lcom/voovio/geometry/Point;)V

    .line 248
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->angleOfRoations:[F

    add-int/lit8 v5, v0, -0x1

    aget v4, v4, v5

    .line 249
    iget v5, v1, Lcom/voovio/sweep/Sweep$NodeInfo;->m_fAngle:F

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v5

    double-to-float v5, v5

    add-float/2addr v4, v5

    .line 248
    aput v4, v3, v0

    .line 250
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mwalkCounter:[I

    iget v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mWalkCount:I

    aput v4, v3, v0

    goto :goto_4

    .line 255
    .end local v1    # "oNode":Lcom/voovio/sweep/Sweep$NodeInfo;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->proxyPoints:[Lcom/voovio/geometry/Point;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->newPoint()Lcom/voovio/geometry/Point;

    move-result-object v4

    aput-object v4, v3, v0

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->proxyPoints:[Lcom/voovio/geometry/Point;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/voovio/geometry/Point;->x:F

    iput v4, v3, Lcom/voovio/geometry/Point;->x:F

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->proxyPoints:[Lcom/voovio/geometry/Point;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/voovio/geometry/Point;->y:F

    iput v4, v3, Lcom/voovio/geometry/Point;->y:F

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 262
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->pointsForEachImage:[I

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    add-int/lit8 v5, v0, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->mNodePositions:[I

    aget v5, v5, v0

    sub-int/2addr v4, v5

    aput v4, v3, v0

    .line 261
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3
.end method

.method public shiftPoints(FF)V
    .locals 3
    .param p1, "xshift"    # F
    .param p2, "yshift"    # F

    .prologue
    .line 440
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 445
    return-void

    .line 441
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/voovio/geometry/Point;->x:F

    add-float/2addr v2, p1

    iput v2, v1, Lcom/voovio/geometry/Point;->x:F

    .line 442
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->points:[Lcom/voovio/geometry/Point;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/voovio/geometry/Point;->y:F

    add-float/2addr v2, p2

    iput v2, v1, Lcom/voovio/geometry/Point;->y:F

    .line 440
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateDraw(I)V
    .locals 1
    .param p1, "sweepSize"    # I

    .prologue
    .line 455
    iput p1, p0, Lcom/sec/android/app/tourviewer/player/DetailMapView;->sweepSize:I

    .line 456
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->invalidate()V

    .line 458
    :cond_0
    return-void
.end method
