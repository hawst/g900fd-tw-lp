.class public final Lcom/sec/android/app/tourviewer/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/tourviewer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final arrow_length_max:I = 0x7f050005

.field public static final arrow_length_min:I = 0x7f050004

.field public static final backArrow_Height:I = 0x7f050039

.field public static final backArrow_Width:I = 0x7f050038

.field public static final backArrow_leftMargin_land:I = 0x7f05003a

.field public static final backArrow_leftMargin_port:I = 0x7f05003c

.field public static final backArrow_topMargin_land:I = 0x7f05003b

.field public static final backArrow_topMargin_port:I = 0x7f05003d

.field public static final detail_map_alignment:I = 0x7f050001

.field public static final detail_mini_map_size:I = 0x7f050007

.field public static final detail_path_length:I = 0x7f05000c

.field public static final dialogBoxBackground:I = 0x7f050030

.field public static final dialogBoxWidthLandscape:I = 0x7f050031

.field public static final dialog_box_width:I = 0x7f05000d

.field public static final direction_hint_parent_height:I = 0x7f05002a

.field public static final direction_hint_parent_margin_bottom:I = 0x7f05002b

.field public static final direction_hint_parent_margin_right:I = 0x7f05002c

.field public static final direction_hint_parent_width:I = 0x7f050029

.field public static final forwardArrow_Height:I = 0x7f050033

.field public static final forwardArrow_Width:I = 0x7f050032

.field public static final forwardArrow_leftMargin_land:I = 0x7f050034

.field public static final forwardArrow_leftMargin_port:I = 0x7f050036

.field public static final forwardArrow_topMargin_land:I = 0x7f050035

.field public static final forwardArrow_topMargin_port:I = 0x7f050037

.field public static final full_path_length:I = 0x7f05000b

.field public static final leftArrow_Height:I = 0x7f050045

.field public static final leftArrow_Width:I = 0x7f050044

.field public static final leftArrow_leftMargin_land:I = 0x7f050046

.field public static final leftArrow_leftMargin_port:I = 0x7f050048

.field public static final leftArrow_topMargin_land:I = 0x7f050047

.field public static final leftArrow_topMargin_port:I = 0x7f050049

.field public static final line_dimen03:I = 0x7f050008

.field public static final line_dimen11:I = 0x7f050013

.field public static final line_dimen22:I = 0x7f050014

.field public static final line_dimen23:I = 0x7f05001b

.field public static final line_dimen33:I = 0x7f050015

.field public static final line_dimen44:I = 0x7f050016

.field public static final line_dimen46:I = 0x7f05001c

.field public static final line_dimen55:I = 0x7f050017

.field public static final line_dimen66:I = 0x7f050018

.field public static final line_dimen70:I = 0x7f05001d

.field public static final line_dimen77:I = 0x7f050019

.field public static final line_dimen88:I = 0x7f05001a

.field public static final line_dimen94:I = 0x7f05001e

.field public static final loadingText:I = 0x7f05002e

.field public static final loadingText_margin:I = 0x7f05002f

.field public static final mini_map_alignment:I = 0x7f050000

.field public static final mini_map_layoutsize:I = 0x7f050003

.field public static final mini_map_margin:I = 0x7f050006

.field public static final mini_map_size:I = 0x7f050002

.field public static final minimap_animation:I = 0x7f05000a

.field public static final rightArrow_Height:I = 0x7f05003f

.field public static final rightArrow_Width:I = 0x7f05003e

.field public static final rightArrow_leftMargin_land:I = 0x7f050040

.field public static final rightArrow_leftMargin_port:I = 0x7f050042

.field public static final rightArrow_topMargin_land:I = 0x7f050041

.field public static final rightArrow_topMargin_port:I = 0x7f050043

.field public static final rotatingWheel:I = 0x7f05002d

.field public static final seekbar_animation:I = 0x7f050009

.field public static final seekbar_bottom_margin:I = 0x7f050024

.field public static final seekbar_left_margin:I = 0x7f050023

.field public static final seekbar_max_height:I = 0x7f050022

.field public static final seekbar_min_height:I = 0x7f050021

.field public static final seekbar_padding_l:I = 0x7f05001f

.field public static final seekbar_padding_r:I = 0x7f050020

.field public static final stroke_width01:I = 0x7f05000e

.field public static final stroke_width02:I = 0x7f05000f

.field public static final stroke_width03:I = 0x7f050010

.field public static final stroke_width04:I = 0x7f050011

.field public static final stroke_width05:I = 0x7f050012

.field public static final tour_play_pause_parent_button_height:I = 0x7f050026

.field public static final tour_play_pause_parent_button_margin_bottom:I = 0x7f050027

.field public static final tour_play_pause_parent_button_margin_left:I = 0x7f050028

.field public static final tour_play_pause_parent_button_width:I = 0x7f050025


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
