.class public Lcom/sec/android/app/tourviewer/TourPlayerActivity;
.super Landroid/app/Activity;
.source "TourPlayerActivity.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcom/sec/android/app/tourviewer/player/OnInteractionListener;
.implements Lcom/voovio/sweep/SweepView$OnSweepViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;
    }
.end annotation


# static fields
.field private static final DIRECTION_ARROW_BACKWARD_INDEX:I = 0x1

.field private static final DIRECTION_ARROW_FORWARD_INDEX:I = 0x0

.field private static final DIRECTION_ARROW_LEFT_INDEX:I = 0x2

.field private static final DIRECTION_ARROW_RIGHT_INDEX:I = 0x3

.field public static GESTURE_NONE:I

.field public static GESTURE_PINCH:I


# instance fields
.field private CurrentImage:I

.field private animator:Landroid/animation/ObjectAnimator;

.field appStateVar:Z

.field arrows:Landroid/widget/RelativeLayout;

.field private bArrowsState:[Z

.field private backgroundImage:Landroid/widget/ImageView;

.field bottomMenulayout:Landroid/widget/RelativeLayout;

.field dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

.field private dialogBoxWidthLandscape:I

.field private dialogboxHeight:I

.field private directionArrows:[Landroid/widget/ImageButton;

.field private fAngularFactor:F

.field filter:Landroid/content/IntentFilter;

.field private firstTime:Z

.field fm:Landroid/app/FragmentManager;

.field private isActionUp:Z

.field private isBackPressed:Z

.field private isFullMap:Z

.field private isNavigating:Z

.field private isTrackingActive:Z

.field private istourStarted:Z

.field private m_bButtonTracking:Z

.field private m_bDragging:Ljava/lang/Boolean;

.field private m_bSeekBarTracking:Ljava/lang/Boolean;

.field private m_fLastPinchLength:F

.field private m_lTouchDownTimeMillis:J

.field private m_nMovementGesture:I

.field private m_oBtnAutodrive:Landroid/widget/ImageButton;

.field private m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

.field private m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

.field private m_oGestureDetector:Landroid/view/GestureDetector;

.field m_oLoadSweepTask:Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;

.field m_oProgressDialog:Landroid/app/ProgressDialog;

.field public m_oSweepView:Lcom/voovio/sweep/SweepView;

.field private m_ptAnchor:Lcom/voovio/geometry/Point;

.field private m_ptPosition:Lcom/voovio/geometry/Point;

.field private miniMap:Landroid/widget/ImageView;

.field private miniMapLayout:Landroid/widget/RelativeLayout;

.field oSEFParser:Lcom/samsung/sef/SEFParserDecode;

.field private playerview:Landroid/widget/RelativeLayout;

.field private seekbar:Landroid/widget/SeekBar;

.field private showContent:Z

.field private sweepSize:I

.field private telephonyBroadCastreceiver:Landroid/content/BroadcastReceiver;

.field private textProgress:Landroid/widget/TextView;

.field private touchedArrowIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->GESTURE_NONE:I

    .line 107
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->GESTURE_PINCH:I

    .line 150
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    .line 89
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oBtnAutodrive:Landroid/widget/ImageButton;

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    .line 95
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    .line 96
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oGestureDetector:Landroid/view/GestureDetector;

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMap:Landroid/widget/ImageView;

    .line 98
    iput v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    .line 100
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->oSEFParser:Lcom/samsung/sef/SEFParserDecode;

    .line 101
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->appStateVar:Z

    .line 109
    sget v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->GESTURE_PINCH:I

    iput v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_nMovementGesture:I

    .line 113
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    .line 114
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_fLastPinchLength:F

    .line 122
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->backgroundImage:Landroid/widget/ImageView;

    .line 125
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bSeekBarTracking:Ljava/lang/Boolean;

    .line 129
    const/high16 v0, 0x3e000000    # 0.125f

    iput v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->fAngularFactor:F

    .line 130
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->fm:Landroid/app/FragmentManager;

    .line 133
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    .line 134
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oLoadSweepTask:Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;

    .line 136
    iput-boolean v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isFullMap:Z

    .line 138
    iput-boolean v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    .line 140
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    .line 142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_lTouchDownTimeMillis:J

    .line 143
    iput v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    .line 145
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    .line 146
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->istourStarted:Z

    .line 151
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    .line 152
    iput-boolean v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isActionUp:Z

    .line 156
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isNavigating:Z

    .line 157
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->telephonyBroadCastreceiver:Landroid/content/BroadcastReceiver;

    .line 158
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->filter:Landroid/content/IntentFilter;

    .line 160
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isBackPressed:Z

    .line 162
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    .line 164
    iput-boolean v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->firstTime:Z

    .line 165
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    .line 167
    iput v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dialogboxHeight:I

    .line 168
    iput v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dialogBoxWidthLandscape:I

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)Lcom/sec/android/app/tourviewer/player/FullMapView;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)Lcom/sec/android/app/tourviewer/player/DetailMapView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/tourviewer/TourPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/app/tourviewer/TourPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->firstTime:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public static isLdevice()Z
    .locals 2

    .prologue
    .line 1293
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1294
    .local v0, "currentapiVersion":I
    const/16 v1, 0x13

    if-le v0, v1, :cond_0

    .line 1295
    const/4 v1, 0x1

    .line 1297
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public displayArrows()Z
    .locals 3

    .prologue
    .line 1096
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V

    .line 1097
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 1102
    const/4 v1, 0x1

    return v1

    .line 1098
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 1099
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1097
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->dispose()V

    .line 425
    return-void
.end method

.method public getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x0

    .line 406
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 407
    .local v2, "proj":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 408
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 410
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 411
    .local v6, "column_index":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 412
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 419
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v6    # "column_index":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v0

    .restart local v2    # "proj":[Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_0
    move-object v0, v9

    .line 414
    goto :goto_0

    .line 417
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 418
    .local v8, "exception":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 419
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getScreenOrientation()I
    .locals 4

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 391
    .local v0, "getOrient":Landroid/view/Display;
    const/4 v1, 0x0

    .line 392
    .local v1, "orientation":I
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 393
    const/4 v1, 0x3

    .line 401
    :goto_0
    return v1

    .line 395
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 396
    const/4 v1, 0x1

    .line 397
    goto :goto_0

    .line 398
    :cond_1
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public getSweep()Lcom/voovio/sweep/Sweep;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->getSweep()Lcom/voovio/sweep/Sweep;

    move-result-object v0

    return-object v0
.end method

.method public hideArrows()V
    .locals 3

    .prologue
    .line 1111
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 1114
    return-void

    .line 1112
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public loadSweepFromFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->fm:Landroid/app/FragmentManager;

    const-string v2, "Dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 492
    new-instance v0, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;-><init>(Lcom/sec/android/app/tourviewer/TourPlayerActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oLoadSweepTask:Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oLoadSweepTask:Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/TourPlayerActivity$LoadSweepTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 494
    return-void
.end method

.method public onAutodriveStateChanged(Z)V
    .locals 10
    .param p1, "active"    # Z

    .prologue
    const/4 v9, 0x0

    const-wide/16 v7, 0x1f4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 839
    if-eqz p1, :cond_0

    .line 841
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    .line 842
    const-string v3, "translationY"

    new-array v4, v1, [F

    .line 843
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 844
    const v6, 0x7f050009

    .line 843
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    int-to-float v5, v5

    aput v5, v4, v0

    .line 840
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 845
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 846
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 847
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    const-string v3, "translationY"

    new-array v4, v1, [F

    aput v9, v4, v0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 848
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 849
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 852
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    const-string v3, "translationY"

    new-array v4, v1, [F

    aput v9, v4, v0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 851
    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 853
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 854
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 855
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    const-string v3, "translationY"

    new-array v4, v1, [F

    .line 856
    aput v9, v4, v0

    .line 855
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 857
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 858
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 860
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isBackPressed:Z

    if-eqz v2, :cond_3

    .line 861
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    if-nez v0, :cond_2

    .line 862
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V

    .line 869
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oBtnAutodrive:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 871
    return-void

    .line 864
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    if-eqz v2, :cond_2

    .line 865
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 866
    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    if-eqz v2, :cond_4

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public onAvailableDirectionsChanged(B)V
    .locals 7
    .param p1, "availableDirections"    # B

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1169
    shr-int/lit8 v2, p1, 0x4

    int-to-byte v0, v2

    .line 1170
    .local v0, "bNextDirection":B
    and-int/lit8 v2, p1, 0xf

    int-to-byte v1, v2

    .line 1177
    .local v1, "bPreviousDirection":B
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_2

    move v2, v3

    :goto_0
    aput-boolean v2, v5, v4

    .line 1178
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    and-int/lit8 v2, v1, 0x2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    aput-boolean v2, v5, v3

    .line 1179
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    const/4 v6, 0x2

    and-int/lit8 v2, v0, 0x4

    if-nez v2, :cond_4

    .line 1180
    and-int/lit8 v2, v1, 0x4

    if-nez v2, :cond_4

    move v2, v4

    .line 1179
    :goto_2
    aput-boolean v2, v5, v6

    .line 1181
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    const/4 v5, 0x3

    and-int/lit8 v6, v0, 0x8

    if-nez v6, :cond_5

    .line 1182
    and-int/lit8 v6, v1, 0x8

    if-nez v6, :cond_5

    .line 1181
    :goto_3
    aput-boolean v4, v2, v5

    .line 1184
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v2}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->istourStarted:Z

    if-eqz v2, :cond_0

    .line 1185
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1187
    :cond_0
    iget v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->CurrentImage:I

    iget v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isNavigating:Z

    if-nez v2, :cond_1

    .line 1188
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V

    .line 1190
    :cond_1
    const-string v2, "onAvailableDirectionsChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " available directions "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    return-void

    :cond_2
    move v2, v4

    .line 1177
    goto :goto_0

    :cond_3
    move v2, v4

    .line 1178
    goto :goto_1

    :cond_4
    move v2, v3

    .line 1179
    goto :goto_2

    :cond_5
    move v4, v3

    .line 1181
    goto :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v5, 0x1f4

    const/16 v1, 0x8

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 646
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oBtnAutodrive:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oBtnAutodrive:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 650
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 658
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    .line 661
    const-string v1, "translationY"

    new-array v2, v2, [F

    aput v4, v2, v3

    .line 660
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 664
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    .line 684
    :cond_0
    :goto_1
    return-void

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/SweepView;->autodriveTo(I)Z

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V

    goto :goto_0

    .line 667
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 670
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isFullMap:Z

    goto :goto_1

    .line 671
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    .line 674
    iput-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isFullMap:Z

    goto :goto_1

    .line 675
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->backgroundImage:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    const-string v1, "translationY"

    new-array v2, v2, [F

    .line 678
    aput v4, v2, v3

    .line 677
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 681
    iput-boolean v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1120
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1121
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1122
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v2, :cond_4

    .line 1123
    const v2, 0x7f080005

    invoke-virtual {p0, v2}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    .line 1124
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/high16 v3, 0x7f030000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1125
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1131
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    aput-object v2, v3, v5

    .line 1132
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const v2, 0x7f080001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    aput-object v2, v3, v6

    .line 1133
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const v2, 0x7f080003

    invoke-virtual {p0, v2}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    aput-object v2, v3, v7

    .line 1134
    iget-object v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const v2, 0x7f080002

    invoke-virtual {p0, v2}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    aput-object v2, v3, v8

    .line 1136
    invoke-static {}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isLdevice()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1137
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v5

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02002e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1138
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v6

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1139
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v7

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1140
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v8

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02001a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1142
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    .line 1143
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v9, :cond_5

    .line 1150
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v2}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->istourStarted:Z

    if-nez v2, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    if-eqz v2, :cond_6

    .line 1151
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1156
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isActionUp:Z

    if-nez v2, :cond_7

    .line 1158
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v2}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 1159
    iput-boolean v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isActionUp:Z

    .line 1165
    :goto_3
    return-void

    .line 1126
    .end local v0    # "i":I
    :cond_4
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v7, v2, :cond_0

    .line 1127
    const v2, 0x7f080005

    invoke-virtual {p0, v2}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    .line 1128
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030001

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1129
    .restart local v1    # "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1144
    .end local v1    # "v":Landroid/view/View;
    .restart local v0    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v0

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1145
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v0

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1146
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1143
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1153
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V

    goto :goto_2

    .line 1161
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1162
    iput-boolean v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isActionUp:Z

    goto :goto_3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 179
    const-string v8, "version"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Sweep : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Lcom/voovio/sweep/Sweep;->VERSION:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->requestWindowFeature(I)Z

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    .line 182
    const/high16 v9, 0x1000000

    .line 183
    const/high16 v10, 0x1000000

    .line 181
    invoke-virtual {v8, v9, v10}, Landroid/view/Window;->setFlags(II)V

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/16 v9, 0x400

    .line 185
    const/16 v10, 0x400

    .line 184
    invoke-virtual {v8, v9, v10}, Landroid/view/Window;->setFlags(II)V

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/16 v9, 0x80

    .line 187
    const/16 v10, 0x80

    .line 186
    invoke-virtual {v8, v9, v10}, Landroid/view/Window;->setFlags(II)V

    .line 188
    const v8, 0x7f030002

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->setContentView(I)V

    .line 190
    const v8, 0x7f080006

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    .line 191
    const v8, 0x7f08000a

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->textProgress:Landroid/widget/TextView;

    .line 192
    const v8, 0x7f080007

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->backgroundImage:Landroid/widget/ImageView;

    .line 195
    const v8, 0x7f080004

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    .line 197
    const v8, 0x7f080008

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oBtnAutodrive:Landroid/widget/ImageButton;

    .line 199
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oBtnAutodrive:Landroid/widget/ImageButton;

    invoke-virtual {v8, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->backgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v8, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->fm:Landroid/app/FragmentManager;

    .line 204
    new-instance v8, Lcom/voovio/sweep/SweepView;

    new-instance v9, Lcom/samsung/sef/SEFImageProvider;

    invoke-direct {v9}, Lcom/samsung/sef/SEFImageProvider;-><init>()V

    invoke-direct {v8, p0, v9}, Lcom/voovio/sweep/SweepView;-><init>(Landroid/content/Context;Lcom/voovio/sweep/ImageProvider;)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 205
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v8, p0}, Lcom/voovio/sweep/SweepView;->setOnSweepViewListener(Lcom/voovio/sweep/SweepView$OnSweepViewListener;)V

    .line 206
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/16 v9, 0x3e8

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->setAutodriveOnStart(I)V

    .line 207
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->setBitmapPreferredConfig(Landroid/graphics/Bitmap$Config;)V

    .line 208
    const v8, 0x7f080005

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getScreenOrientation()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 210
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const/high16 v9, 0x7f030000

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 211
    .local v7, "v":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 217
    .end local v7    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 218
    .local v1, "displaymetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v8

    .line 221
    .local v0, "density":F
    const/high16 v8, 0x43f00000    # 480.0f

    cmpl-float v8, v0, v8

    if-ltz v8, :cond_4

    .line 223
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->enabledMipMaps(Z)V

    .line 224
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->preloadSweepImages(Z)V

    .line 236
    :goto_1
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->filter:Landroid/content/IntentFilter;

    .line 237
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->filter:Landroid/content/IntentFilter;

    const-string v9, "android.intent.action.PHONE_STATE"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050030

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dialogboxHeight:I

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050031

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dialogBoxWidthLandscape:I

    .line 240
    new-instance v8, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getScreenOrientation()I

    move-result v9

    iget v10, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dialogboxHeight:I

    iget v11, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dialogBoxWidthLandscape:I

    invoke-direct {v8, v9, v10, v11}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;-><init>(III)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    .line 242
    new-instance v8, Lcom/sec/android/app/tourviewer/TourPlayerActivity$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity$1;-><init>(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->telephonyBroadCastreceiver:Landroid/content/BroadcastReceiver;

    .line 269
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/high16 v9, 0x40a00000    # 5.0f

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->setZoomInFov(F)V

    .line 273
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/high16 v9, 0x42c80000    # 100.0f

    const/high16 v10, 0x41700000    # 15.0f

    invoke-virtual {v8, v9, v10}, Lcom/voovio/sweep/SweepView;->setAutodriveVelocity(FF)V

    .line 275
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 276
    new-instance v8, Landroid/widget/RelativeLayout;

    invoke-direct {v8, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    .line 277
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 278
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 280
    const v9, 0x7f050003

    .line 279
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 281
    const v10, 0x7f050003

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 278
    invoke-direct {v5, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 283
    .local v5, "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x1

    iput-boolean v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 284
    const/4 v8, 0x6

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 285
    const/4 v8, 0x7

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 286
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    new-instance v8, Landroid/widget/ImageView;

    invoke-direct {v8, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMap:Landroid/widget/ImageView;

    .line 289
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMap:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 291
    const v10, 0x7f02000a

    .line 289
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 292
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050002

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 292
    invoke-direct {v4, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 296
    .local v4, "lp1":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x1

    iput-boolean v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 297
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 298
    const/4 v8, 0x5

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 299
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMap:Landroid/widget/ImageView;

    invoke-virtual {v8, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 301
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMap:Landroid/widget/ImageView;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 302
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 304
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 308
    new-instance v8, Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-direct {v8, p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    .line 309
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    .line 310
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 314
    new-instance v8, Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-direct {v8, p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    .line 315
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 316
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->playerview:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 318
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v8, p0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v8, p0}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    const v8, 0x7f080009

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/SeekBar;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    .line 323
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 327
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    new-instance v9, Lcom/sec/android/app/tourviewer/TourPlayerActivity$2;

    invoke-direct {v9, p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity$2;-><init>(Lcom/sec/android/app/tourviewer/TourPlayerActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 345
    new-instance v8, Lcom/voovio/geometry/Point;

    invoke-direct {v8}, Lcom/voovio/geometry/Point;-><init>()V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    .line 346
    new-instance v8, Lcom/voovio/geometry/Point;

    invoke-direct {v8}, Lcom/voovio/geometry/Point;-><init>()V

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    .line 350
    new-instance v8, Landroid/view/GestureDetector;

    .line 351
    new-instance v9, Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v9}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    invoke-direct {v8, p0, v9}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 350
    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oGestureDetector:Landroid/view/GestureDetector;

    .line 352
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v8, p0}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 356
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "sef_file_name"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 357
    .local v6, "strSweepUrl":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 358
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 359
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 366
    :cond_1
    :goto_2
    invoke-virtual {p0, v6}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->loadSweepFromFile(Ljava/lang/String;)V

    .line 368
    const/4 v8, 0x4

    new-array v8, v8, [Landroid/widget/ImageButton;

    iput-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    .line 370
    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v10, 0x0

    const/high16 v8, 0x7f080000

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    aput-object v8, v9, v10

    .line 371
    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v10, 0x1

    const v8, 0x7f080001

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    aput-object v8, v9, v10

    .line 372
    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v10, 0x2

    const v8, 0x7f080003

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    aput-object v8, v9, v10

    .line 373
    iget-object v9, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v10, 0x3

    const v8, 0x7f080002

    invoke-virtual {p0, v8}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    aput-object v8, v9, v10

    .line 375
    invoke-static {}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isLdevice()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 376
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02002e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 377
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020006

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 378
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v9, 0x2

    aget-object v8, v8, v9

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02000d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 379
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v9, 0x3

    aget-object v8, v8, v9

    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02001a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 382
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    const/4 v8, 0x4

    if-lt v2, v8, :cond_6

    .line 387
    return-void

    .line 212
    .end local v0    # "density":F
    .end local v1    # "displaymetrics":Landroid/util/DisplayMetrics;
    .end local v2    # "i":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "lp1":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "strSweepUrl":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getScreenOrientation()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    .line 213
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030001

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 214
    .restart local v7    # "v":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->arrows:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 230
    .end local v7    # "v":Landroid/view/View;
    .restart local v0    # "density":F
    .restart local v1    # "displaymetrics":Landroid/util/DisplayMetrics;
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->enabledMipMaps(Z)V

    .line 232
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->setImageSampleSize(I)V

    .line 233
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/voovio/sweep/SweepView;->preloadSweepImages(Z)V

    goto/16 :goto_1

    .line 361
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "lp1":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v5    # "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v6    # "strSweepUrl":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->finish()V

    goto/16 :goto_2

    .line 383
    .restart local v2    # "i":I
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v8, v8, v2

    invoke-virtual {v8, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 382
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 713
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/voovio/sweep/SweepView;->toogleZoomLevel(FF)V

    .line 714
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 717
    const/4 v1, 0x1

    return v1

    .line 715
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 714
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 724
    const/4 v0, 0x0

    return v0
.end method

.method public onImageChanged(II)V
    .locals 3
    .param p1, "currentImage"    # I
    .param p2, "totalLength"    # I

    .prologue
    .line 815
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->istourStarted:Z

    .line 816
    iput p1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->CurrentImage:I

    .line 817
    iget v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_0

    .line 818
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isNavigating:Z

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->textProgress:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 824
    iget v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_nMovementGesture:I

    sget v1, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->GESTURE_PINCH:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget v0, v0, Lcom/voovio/geometry/Point;->y:F

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    iget v1, v1, Lcom/voovio/geometry/Point;->y:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    iget v1, v1, Lcom/voovio/geometry/Point;->y:F

    iput v1, v0, Lcom/voovio/geometry/Point;->y:F

    .line 829
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 830
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->finish()V

    .line 832
    :cond_2
    return-void
.end method

.method public onInteractionEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 703
    return-void
.end method

.method public declared-synchronized onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1196
    monitor-enter p0

    :try_start_0
    const-string v2, "onKeyDown"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v4}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Key Code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1197
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1196
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    .line 1203
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1205
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 1241
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1208
    :sswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1209
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1210
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    const/4 v2, 0x3

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    .line 1211
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1214
    :sswitch_1
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1215
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1216
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    const/4 v2, 0x2

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    .line 1217
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    goto :goto_0

    .line 1220
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1221
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1222
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    const/4 v2, 0x0

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    .line 1223
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    goto :goto_0

    .line 1226
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1227
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1228
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bArrowsState:[Z

    const/4 v2, 0x1

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    .line 1229
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    goto :goto_0

    .line 1233
    :sswitch_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isBackPressed:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1205
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
    .end sparse-switch
.end method

.method public declared-synchronized onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x1

    .line 1246
    monitor-enter p0

    :try_start_0
    const-string v1, "onKeyUp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v3}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Key Code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1247
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1246
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    .line 1249
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    .line 1250
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    .line 1253
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_4

    .line 1256
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1257
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 1258
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1264
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    .line 1265
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1266
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    .line 1267
    const-string v2, "translationY"

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 1266
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 1268
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1269
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1270
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1287
    :cond_1
    :goto_2
    monitor-exit p0

    return v6

    .line 1251
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1260
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const v2, 0x7fffffff

    invoke-virtual {v1, v2}, Lcom/voovio/sweep/SweepView;->autodriveTo(I)Z

    .line 1261
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1246
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1277
    .restart local v0    # "i":I
    :cond_4
    packed-switch p1, :pswitch_data_0

    .line 1286
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    goto :goto_2

    .line 1280
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1277
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 434
    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    .line 435
    iput-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->telephonyBroadCastreceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->getAutodrive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->appStateVar:Z

    .line 439
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->onPause()V

    .line 445
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 446
    return-void
.end method

.method public onPositionChanged(II)V
    .locals 2
    .param p1, "currentPosition"    # I
    .param p2, "totalLength"    # I

    .prologue
    .line 802
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bSeekBarTracking:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 805
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setCurrentPosition(I)V

    .line 806
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setCurrentPosition(I)V

    .line 808
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    add-int/lit8 v1, p2, -0x1

    if-lt v0, v1, :cond_1

    .line 809
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->finish()V

    .line 811
    :cond_1
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 906
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bSeekBarTracking:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/SweepView;->moveToPosition(I)V

    .line 912
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 463
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->telephonyBroadCastreceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->filter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 465
    iget-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->firstTime:Z

    if-nez v0, :cond_0

    .line 466
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    .line 467
    iget-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->appStateVar:Z

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/SweepView;->autodriveTo(I)Z

    .line 476
    :goto_0
    const-string v0, "onResume"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->appStateVar:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->onResume()V

    .line 485
    :cond_0
    return-void

    .line 473
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const v10, 0x7f05000a

    const/4 v4, 0x0

    const-wide/16 v8, 0x1f4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 731
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 734
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 735
    iget-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    if-eqz v1, :cond_2

    .line 736
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    const-string v2, "translationY"

    new-array v3, v7, [F

    .line 737
    aput v4, v3, v6

    .line 736
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 738
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 739
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    const-string v2, "translationY"

    new-array v3, v7, [F

    aput v4, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 741
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 742
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 745
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    const-string v2, "translationY"

    new-array v3, v7, [F

    aput v4, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 744
    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 746
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 747
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 748
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    const-string v2, "translationY"

    new-array v3, v7, [F

    .line 749
    aput v4, v3, v6

    .line 748
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 750
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 751
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 752
    iget-boolean v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isFullMap:Z

    if-eqz v1, :cond_1

    .line 753
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    .line 754
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 759
    :goto_1
    iput-boolean v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    .line 794
    :goto_2
    return v7

    .line 732
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 731
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 756
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 757
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    goto :goto_1

    .line 762
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    .line 763
    const-string v2, "translationY"

    new-array v3, v7, [F

    .line 764
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 765
    const v5, 0x7f050009

    .line 764
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    aput v4, v3, v6

    .line 761
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 767
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    .line 771
    const-string v2, "translationY"

    new-array v3, v7, [F

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v3, v6

    .line 769
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 774
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 778
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    .line 779
    const-string v2, "translationY"

    new-array v3, v7, [F

    .line 780
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v3, v6

    .line 777
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 782
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 783
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 786
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    .line 787
    const-string v2, "translationY"

    new-array v3, v7, [F

    .line 788
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v3, v6

    .line 785
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 791
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 792
    iput-boolean v7, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    goto/16 :goto_2
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 920
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bSeekBarTracking:Ljava/lang/Boolean;

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->bottomMenulayout:Landroid/widget/RelativeLayout;

    const-string v1, "translationY"

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 926
    iput-boolean v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->showContent:Z

    .line 929
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 454
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 455
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 933
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bSeekBarTracking:Ljava/lang/Boolean;

    .line 934
    return-void
.end method

.method public onSweepReady()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/tourviewer/utils/ProgressFragment;->dismiss()V

    .line 889
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->dFragment:Lcom/sec/android/app/tourviewer/utils/ProgressFragment;

    .line 891
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->miniMapLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 892
    iget-boolean v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isFullMap:Z

    if-eqz v0, :cond_1

    .line 893
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 896
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setVisibility(I)V

    .line 897
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 993
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 994
    .local v0, "action":I
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isTrackingActive:Z

    .line 995
    if-nez v0, :cond_4

    .line 996
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    .line 997
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_lTouchDownTimeMillis:J

    .line 998
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    :goto_0
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    const/4 v6, 0x4

    if-lt v5, v6, :cond_1

    .line 1003
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v5, 0x4

    if-lt v1, v5, :cond_2

    .line 1008
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    packed-switch v5, :pswitch_data_0

    .line 1029
    const-string v5, "InonTouch"

    const-string v6, "No Direction arrow selected"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    :goto_2
    const/4 v5, 0x1

    .line 1088
    .end local v1    # "i":I
    :goto_3
    return v5

    .line 999
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    if-eq p1, v5, :cond_0

    .line 998
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    goto :goto_0

    .line 1004
    .restart local v1    # "i":I
    :cond_2
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    if-eq v1, v5, :cond_3

    .line 1005
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1003
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1010
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1011
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_2

    .line 1014
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1015
    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1016
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_2

    .line 1019
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1020
    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1021
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_2

    .line 1024
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1025
    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1026
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_2

    .line 1032
    .end local v1    # "i":I
    :cond_4
    const/4 v5, 0x2

    if-ne v0, v5, :cond_6

    .line 1033
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 1034
    iget-wide v7, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_lTouchDownTimeMillis:J

    .line 1033
    sub-long v3, v5, v7

    .line 1035
    .local v3, "lTouchTimeMillis":J
    const-wide/16 v5, 0xc8

    cmp-long v5, v3, v5

    if-lez v5, :cond_5

    .line 1036
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->displayArrows()Z

    .line 1037
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isActionUp:Z

    .line 1088
    .end local v3    # "lTouchTimeMillis":J
    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    .line 1040
    :cond_6
    const/4 v5, 0x1

    if-ne v0, v5, :cond_5

    .line 1041
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bButtonTracking:Z

    .line 1042
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 1043
    iget-wide v7, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_lTouchDownTimeMillis:J

    .line 1042
    sub-long v3, v5, v7

    .line 1046
    .restart local v3    # "lTouchTimeMillis":J
    const-wide/16 v5, 0xc8

    cmp-long v5, v3, v5

    if-gtz v5, :cond_9

    .line 1047
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isNavigating:Z

    .line 1048
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    :goto_4
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    const/4 v6, 0x4

    if-lt v5, v6, :cond_8

    .line 1052
    :cond_7
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    packed-switch v5, :pswitch_data_1

    .line 1074
    const-string v5, "InonTouch"

    const-string v6, "No Direction arrow selected"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    :goto_5
    invoke-virtual {p0}, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->hideArrows()V

    .line 1085
    :goto_6
    const/4 v5, 0x1

    goto/16 :goto_3

    .line 1049
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    if-eq p1, v5, :cond_7

    .line 1048
    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    goto :goto_4

    .line 1054
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1055
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1056
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_5

    .line 1059
    :pswitch_5
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1060
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1061
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_5

    .line 1064
    :pswitch_6
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1065
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1066
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_5

    .line 1069
    :pswitch_7
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    .line 1070
    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/voovio/sweep/SweepView;->autodriveDirectional(I)Z

    .line 1071
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->touchedArrowIndex:I

    aget-object v5, v5, v6

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_5

    .line 1080
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v5}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 1081
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_7
    const/4 v5, 0x4

    if-lt v2, v5, :cond_a

    .line 1083
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isActionUp:Z

    goto :goto_6

    .line 1082
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->directionArrows:[Landroid/widget/ImageButton;

    aget-object v5, v5, v2

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1081
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1008
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1052
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 551
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v4, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 630
    :cond_0
    :goto_0
    :pswitch_0
    return v9

    .line 554
    :cond_1
    iget v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_nMovementGesture:I

    sget v5, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->GESTURE_PINCH:I

    if-ne v4, v5, :cond_0

    .line 555
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 558
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v4}, Lcom/voovio/sweep/SweepView;->stopAutodrive()V

    .line 559
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    .line 560
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iput v6, v5, Lcom/voovio/geometry/Point;->x:F

    iput v6, v4, Lcom/voovio/geometry/Point;->x:F

    .line 561
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iput v6, v5, Lcom/voovio/geometry/Point;->y:F

    iput v6, v4, Lcom/voovio/geometry/Point;->y:F

    .line 565
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v4, v8, v8}, Lcom/voovio/sweep/SweepView;->setCameraDisplacements(FF)V

    goto :goto_0

    .line 571
    :pswitch_3
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    goto :goto_0

    .line 579
    :pswitch_4
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    .line 581
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    sub-float v2, v4, v5

    .line 582
    .local v2, "x":F
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float v3, v4, v5

    .line 583
    .local v3, "y":F
    mul-float v4, v2, v2

    mul-float v5, v3, v3

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_fLastPinchLength:F

    .line 585
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    add-float/2addr v5, v6

    div-float/2addr v5, v10

    iput v5, v4, Lcom/voovio/geometry/Point;->x:F

    .line 586
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    add-float/2addr v5, v6

    div-float/2addr v5, v10

    iput v5, v4, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_0

    .line 595
    .end local v2    # "x":F
    .end local v3    # "y":F
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 597
    .local v1, "numPointers":I
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_bDragging:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    if-ne v1, v9, :cond_2

    .line 598
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iput v5, v4, Lcom/voovio/geometry/Point;->x:F

    .line 599
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iput v5, v4, Lcom/voovio/geometry/Point;->y:F

    .line 602
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->fAngularFactor:F

    .line 603
    iget-object v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    iget v6, v6, Lcom/voovio/geometry/Point;->y:F

    iget-object v7, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget v7, v7, Lcom/voovio/geometry/Point;->y:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->fAngularFactor:F

    .line 604
    iget-object v7, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget v7, v7, Lcom/voovio/geometry/Point;->x:F

    iget-object v8, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    iget v8, v8, Lcom/voovio/geometry/Point;->x:F

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    .line 602
    invoke-virtual {v4, v5, v6}, Lcom/voovio/sweep/SweepView;->setCameraDisplacements(FF)V

    .line 606
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    iget v5, v5, Lcom/voovio/geometry/Point;->x:F

    iput v5, v4, Lcom/voovio/geometry/Point;->x:F

    .line 607
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget-object v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptPosition:Lcom/voovio/geometry/Point;

    iget v5, v5, Lcom/voovio/geometry/Point;->y:F

    iput v5, v4, Lcom/voovio/geometry/Point;->y:F

    goto/16 :goto_0

    .line 609
    :cond_2
    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 610
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    sub-float v2, v4, v5

    .line 611
    .restart local v2    # "x":F
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float v3, v4, v5

    .line 612
    .restart local v3    # "y":F
    mul-float v4, v2, v2

    mul-float v5, v3, v3

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 616
    .local v0, "fPinchLength":F
    iget-object v4, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    iget v5, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_fLastPinchLength:F

    .line 617
    sub-float/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget v6, v6, Lcom/voovio/geometry/Point;->x:F

    iget-object v7, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_ptAnchor:Lcom/voovio/geometry/Point;

    iget v7, v7, Lcom/voovio/geometry/Point;->y:F

    .line 616
    invoke-virtual {v4, v5, v6, v7}, Lcom/voovio/sweep/SweepView;->setCameraZoomIncrement(FFF)V

    .line 619
    iput v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_fLastPinchLength:F

    goto/16 :goto_0

    .line 555
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateForce(FFF)V
    .locals 0
    .param p1, "fForward"    # F
    .param p2, "fUpward"    # F
    .param p3, "fRightward"    # F

    .prologue
    .line 695
    return-void
.end method

.method public onZoomStateChanged(Z)V
    .locals 0
    .param p1, "zoomin"    # Z

    .prologue
    .line 880
    return-void
.end method

.method public setMovementGesture(I)V
    .locals 0
    .param p1, "nMovementGesture"    # I

    .prologue
    .line 533
    iput p1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_nMovementGesture:I

    .line 534
    return-void
.end method

.method public setSweep(Lcom/voovio/sweep/Sweep;)V
    .locals 3
    .param p1, "oSweep"    # Lcom/voovio/sweep/Sweep;

    .prologue
    const/4 v2, 0x0

    .line 503
    if-eqz p1, :cond_2

    .line 506
    invoke-virtual {p1}, Lcom/voovio/sweep/Sweep;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v0, p1}, Lcom/voovio/sweep/SweepView;->setSweep(Lcom/voovio/sweep/Sweep;)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    iget v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->updateDraw(I)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    iget v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/tourviewer/player/FullMapView;->updateDraw(I)V

    .line 511
    iget v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->sweepSize:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->getImagePositions()[I

    move-result-object v1

    .line 512
    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/tourviewer/player/FullMapView;->setSweep(Lcom/voovio/sweep/Sweep;[I)V

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oDetailMapView:Lcom/sec/android/app/tourviewer/player/DetailMapView;

    .line 515
    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->getImagePositions()[I

    move-result-object v1

    .line 514
    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/tourviewer/player/DetailMapView;->setSweep(Lcom/voovio/sweep/Sweep;[I)V

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oFullMapView:Lcom/sec/android/app/tourviewer/player/FullMapView;

    invoke-virtual {v0}, Lcom/sec/android/app/tourviewer/player/FullMapView;->getTotalWalkCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 520
    iput-boolean v2, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->isFullMap:Z

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->m_oSweepView:Lcom/voovio/sweep/SweepView;

    invoke-virtual {v1}, Lcom/voovio/sweep/SweepView;->getMaxPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/tourviewer/TourPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 525
    :cond_2
    return-void
.end method
