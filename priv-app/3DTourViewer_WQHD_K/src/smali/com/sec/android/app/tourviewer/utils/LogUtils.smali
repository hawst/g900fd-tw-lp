.class public Lcom/sec/android/app/tourviewer/utils/LogUtils;
.super Ljava/lang/Object;
.source "LogUtils.java"


# static fields
.field private static DEBUG:Z = false

.field public static final TAG:Ljava/lang/String; = "Virtual Tour Viewer"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/tourviewer/utils/LogUtils;->DEBUG:Z

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static criticalLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    return-void
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 10
    sget-boolean v0, Lcom/sec/android/app/tourviewer/utils/LogUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 11
    const-string v0, "Virtual Tour"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 13
    :cond_0
    return-void
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 16
    sget-boolean v0, Lcom/sec/android/app/tourviewer/utils/LogUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 17
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    :cond_0
    return-void
.end method
