.class public Lcom/android/providers/media/MediaScannerService;
.super Landroid/app/Service;
.source "MediaScannerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/media/MediaScannerService$ServiceHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaScannerService"

.field public static final VOLUME_EXTERNAL:I = 0x2

.field public static final VOLUME_EXTERNAL_PRIVATE:I = 0x4

.field public static final VOLUME_EXTERNAL_SD:I = 0x3

.field public static final VOLUME_INTERNAL:I = 0x1

.field public static final VOLUME_NONE:I

.field private static isBootComplete:Z

.field private static mExternalStorageSdPath:Ljava/lang/String;

.field private static stateForExtSdCardTermination:Z

.field private static stateForPrivateTermination:Z

.field private static storageManager:Landroid/os/storage/StorageManager;

.field private static syncTerminateStatus:Ljava/lang/Object;


# instance fields
.field private final mBinder:Landroid/media/IMediaScannerService$Stub;

.field private mExternalStoragePaths:[Ljava/lang/String;

.field private volatile mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 60
    sput-object v0, Lcom/android/providers/media/MediaScannerService;->storageManager:Landroid/os/storage/StorageManager;

    .line 61
    sput-object v0, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    .line 62
    sput-boolean v1, Lcom/android/providers/media/MediaScannerService;->stateForExtSdCardTermination:Z

    .line 63
    sput-boolean v1, Lcom/android/providers/media/MediaScannerService;->stateForPrivateTermination:Z

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/providers/media/MediaScannerService;->syncTerminateStatus:Ljava/lang/Object;

    .line 66
    sput-boolean v1, Lcom/android/providers/media/MediaScannerService;->isBootComplete:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 379
    new-instance v0, Lcom/android/providers/media/MediaScannerService$1;

    invoke-direct {v0, p0}, Lcom/android/providers/media/MediaScannerService$1;-><init>(Lcom/android/providers/media/MediaScannerService;)V

    iput-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mBinder:Landroid/media/IMediaScannerService$Stub;

    .line 401
    return-void
.end method

.method static synthetic access$100(Lcom/android/providers/media/MediaScannerService;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/media/MediaScannerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/providers/media/MediaScannerService;->scanFile(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/providers/media/MediaScannerService;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/media/MediaScannerService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mExternalStoragePaths:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/providers/media/MediaScannerService;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/media/MediaScannerService;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/providers/media/MediaScannerService;->scan([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/android/providers/media/MediaScannerService;->isBootComplete:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 52
    sput-boolean p0, Lcom/android/providers/media/MediaScannerService;->isBootComplete:Z

    return p0
.end method

.method private createMediaScanner()Landroid/media/MediaScanner;
    .locals 7

    .prologue
    .line 85
    new-instance v4, Landroid/media/MediaScanner;

    invoke-direct {v4, p0}, Landroid/media/MediaScanner;-><init>(Landroid/content/Context;)V

    .line 86
    .local v4, "scanner":Landroid/media/MediaScanner;
    invoke-virtual {p0}, Lcom/android/providers/media/MediaScannerService;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v2, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 87
    .local v2, "locale":Ljava/util/Locale;
    if-eqz v2, :cond_0

    .line 88
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "language":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "country":Ljava/lang/String;
    const/4 v3, 0x0

    .line 91
    .local v3, "localeString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 92
    if-eqz v0, :cond_1

    .line 93
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaScanner;->setLocale(Ljava/lang/String;)V

    .line 100
    .end local v0    # "country":Ljava/lang/String;
    .end local v1    # "language":Ljava/lang/String;
    .end local v3    # "localeString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 95
    .restart local v0    # "country":Ljava/lang/String;
    .restart local v1    # "language":Ljava/lang/String;
    .restart local v3    # "localeString":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4, v1}, Landroid/media/MediaScanner;->setLocale(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private openDatabase(Ljava/lang/String;)V
    .locals 4
    .param p1, "volumeName"    # Ljava/lang/String;

    .prologue
    .line 76
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 77
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "name"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://media/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .end local v1    # "values":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "MediaScannerService"

    const-string v3, "failed to open media database"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private scan([Ljava/lang/String;Ljava/lang/String;)V
    .locals 21
    .param p1, "directories"    # [Ljava/lang/String;
    .param p2, "volumeName"    # Ljava/lang/String;

    .prologue
    .line 104
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "file://"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x0

    aget-object v19, p1, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 106
    .local v15, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/media/MediaScannerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 107
    const/4 v3, 0x0

    .line 110
    .local v3, "cleanBuffer":Z
    :try_start_0
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 111
    .local v17, "values":Landroid/content/ContentValues;
    const/4 v11, 0x0

    .line 112
    .local v11, "scanVolumeName":Ljava/lang/String;
    const-string v18, "external"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 113
    move-object/from16 v11, p2

    .line 114
    const-string p2, "external"

    .line 116
    :cond_0
    const-string v18, "volume"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v10

    .line 118
    .local v10, "scanUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 119
    .local v6, "isSdcardmounted":Z
    new-instance v18, Landroid/content/Intent;

    const-string v19, "android.intent.action.MEDIA_SCANNER_STARTED"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/providers/media/MediaScannerService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 122
    :try_start_1
    const-string v18, "external"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 123
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/providers/media/MediaScannerService;->openDatabase(Ljava/lang/String;)V

    .line 125
    :cond_1
    sget-object v19, Lcom/android/providers/media/MediaScannerService;->syncTerminateStatus:Ljava/lang/Object;

    monitor-enter v19
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126
    const/16 v18, 0x0

    :try_start_2
    sput-boolean v18, Lcom/android/providers/media/MediaScannerService;->stateForExtSdCardTermination:Z

    .line 127
    const/16 v18, 0x0

    sput-boolean v18, Lcom/android/providers/media/MediaScannerService;->stateForPrivateTermination:Z

    .line 128
    monitor-exit v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->createMediaScanner()Landroid/media/MediaScanner;

    move-result-object v12

    .line 130
    .local v12, "scanner":Landroid/media/MediaScanner;
    sget-object v18, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    if-eqz v18, :cond_2

    .line 131
    sget-object v18, Lcom/android/providers/media/MediaScannerService;->storageManager:Landroid/os/storage/StorageManager;

    sget-object v19, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v19, "mounted"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 133
    :cond_2
    const-string v18, "external"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 134
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 135
    .local v16, "value":Landroid/content/ContentValues;
    const-string v18, "external_sd"

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 136
    invoke-static/range {p2 .. p2}, Landroid/provider/MediaStore;->getRestoreSdcardUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 137
    .local v9, "restoreSdcardUri":Landroid/net/Uri;
    const-string v18, "_data"

    sget-object v19, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 144
    .end local v9    # "restoreSdcardUri":Landroid/net/Uri;
    :cond_3
    :goto_0
    invoke-static/range {p2 .. p2}, Landroid/provider/MediaStore;->getPlaylistsSaveUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 145
    .local v7, "playlistSaveUri":Landroid/net/Uri;
    if-nez v6, :cond_4

    .line 146
    const-string v18, "_data"

    sget-object v19, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 149
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getBaseContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/providers/media/MediaProvider;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 150
    const-string v18, "_data"

    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/providers/media/MediaProvider;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 155
    .end local v7    # "playlistSaveUri":Landroid/net/Uri;
    .end local v16    # "value":Landroid/content/ContentValues;
    :cond_5
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v12, v0, v1}, Landroid/media/MediaScanner;->scanDirectories([Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v18, "external"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    if-eqz v6, :cond_6

    .line 158
    const/4 v13, 0x0

    .line 159
    .local v13, "status_stateForExtSdCardTermination":Z
    sget-object v19, Lcom/android/providers/media/MediaScannerService;->syncTerminateStatus:Ljava/lang/Object;

    monitor-enter v19
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 160
    :try_start_4
    sget-boolean v13, Lcom/android/providers/media/MediaScannerService;->stateForExtSdCardTermination:Z

    .line 161
    monitor-exit v19
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 162
    if-nez v13, :cond_9

    .line 163
    :try_start_5
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 164
    .restart local v16    # "value":Landroid/content/ContentValues;
    invoke-static/range {p2 .. p2}, Landroid/provider/MediaStore;->getPlaylistsSyncUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 165
    .local v8, "playlistSyncUri":Landroid/net/Uri;
    const-string v18, "_data"

    sget-object v19, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 167
    const/4 v3, 0x1

    .line 171
    .end local v8    # "playlistSyncUri":Landroid/net/Uri;
    .end local v13    # "status_stateForExtSdCardTermination":Z
    .end local v16    # "value":Landroid/content/ContentValues;
    :cond_6
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getBaseContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/providers/media/MediaProvider;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 172
    const/4 v14, 0x0

    .line 173
    .local v14, "status_stateForPrivateTermination":Z
    sget-object v19, Lcom/android/providers/media/MediaScannerService;->syncTerminateStatus:Ljava/lang/Object;

    monitor-enter v19
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 174
    :try_start_6
    sget-boolean v14, Lcom/android/providers/media/MediaScannerService;->stateForPrivateTermination:Z

    .line 175
    monitor-exit v19
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 176
    if-nez v14, :cond_a

    .line 177
    :try_start_7
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 178
    .restart local v16    # "value":Landroid/content/ContentValues;
    invoke-static/range {p2 .. p2}, Landroid/provider/MediaStore;->getPlaylistsSyncUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 179
    .restart local v8    # "playlistSyncUri":Landroid/net/Uri;
    const-string v18, "_data"

    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/providers/media/MediaProvider;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 181
    const/4 v3, 0x1

    .line 189
    .end local v8    # "playlistSyncUri":Landroid/net/Uri;
    .end local v12    # "scanner":Landroid/media/MediaScanner;
    .end local v14    # "status_stateForPrivateTermination":Z
    .end local v16    # "value":Landroid/content/ContentValues;
    :cond_7
    :goto_2
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 192
    new-instance v5, Landroid/content/Intent;

    const-string v18, "android.intent.action.MEDIA_SCANNER_FINISHED"

    move-object/from16 v0, v18

    invoke-direct {v5, v0, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 193
    .local v5, "intent":Landroid/content/Intent;
    const-string v18, "CleanBuffer"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 194
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/providers/media/MediaScannerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/media/MediaScannerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 197
    return-void

    .line 128
    .end local v5    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v18

    :try_start_9
    monitor-exit v19
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    throw v18
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 185
    :catch_0
    move-exception v4

    .line 186
    .local v4, "e":Ljava/lang/Exception;
    :try_start_b
    const-string v18, "MediaScannerService"

    const-string v19, "exception in MediaScanner.scan()"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_2

    .line 192
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "isSdcardmounted":Z
    .end local v10    # "scanUri":Landroid/net/Uri;
    .end local v11    # "scanVolumeName":Ljava/lang/String;
    .end local v17    # "values":Landroid/content/ContentValues;
    :catchall_1
    move-exception v18

    new-instance v5, Landroid/content/Intent;

    const-string v19, "android.intent.action.MEDIA_SCANNER_FINISHED"

    move-object/from16 v0, v19

    invoke-direct {v5, v0, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 193
    .restart local v5    # "intent":Landroid/content/Intent;
    const-string v19, "CleanBuffer"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 194
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/providers/media/MediaScannerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/media/MediaScannerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 196
    throw v18

    .line 139
    .end local v5    # "intent":Landroid/content/Intent;
    .restart local v6    # "isSdcardmounted":Z
    .restart local v10    # "scanUri":Landroid/net/Uri;
    .restart local v11    # "scanVolumeName":Ljava/lang/String;
    .restart local v12    # "scanner":Landroid/media/MediaScanner;
    .restart local v16    # "value":Landroid/content/ContentValues;
    .restart local v17    # "values":Landroid/content/ContentValues;
    :cond_8
    :try_start_c
    const-string v18, "external_private"

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 140
    invoke-static/range {p2 .. p2}, Landroid/provider/MediaStore;->getRestoreSdcardUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 141
    .restart local v9    # "restoreSdcardUri":Landroid/net/Uri;
    const-string v18, "_data"

    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/providers/media/MediaProvider;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/media/MediaScannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 161
    .end local v9    # "restoreSdcardUri":Landroid/net/Uri;
    .end local v16    # "value":Landroid/content/ContentValues;
    .restart local v13    # "status_stateForExtSdCardTermination":Z
    :catchall_2
    move-exception v18

    :try_start_d
    monitor-exit v19
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :try_start_e
    throw v18

    .line 169
    :cond_9
    const-string v18, "MediaScannerService"

    const-string v19, "restorePlaylistConfiguration(SD card) is skipped"

    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_1

    .line 175
    .end local v13    # "status_stateForExtSdCardTermination":Z
    .restart local v14    # "status_stateForPrivateTermination":Z
    :catchall_3
    move-exception v18

    :try_start_f
    monitor-exit v19
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :try_start_10
    throw v18

    .line 183
    :cond_a
    const-string v18, "MediaScannerService"

    const-string v19, "restorePlaylistConfiguration(Private) is skipped"

    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_2
.end method

.method private scanFile(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 11
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 345
    const-string v6, "external"

    .line 346
    .local v6, "volumeName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 347
    .local v0, "canonicalPath":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "externalStoragePath":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    .line 349
    .local v4, "lagacyStoragePath":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/android/providers/media/MediaScannerService;->openDatabase(Ljava/lang/String;)V

    .line 350
    invoke-direct {p0}, Lcom/android/providers/media/MediaScannerService;->createMediaScanner()Landroid/media/MediaScanner;

    move-result-object v5

    .line 353
    .local v5, "scanner":Landroid/media/MediaScanner;
    :try_start_0
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    .line 354
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 355
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 356
    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 357
    const-string v8, "MediaScannerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Current path starts with "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " It will be changed to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 359
    invoke-virtual {v5, v0, v6, p2}, Landroid/media/MediaScanner;->scanSingleFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 369
    .end local v3    # "file":Ljava/io/File;
    :goto_0
    return-object v7

    .line 361
    .restart local v3    # "file":Ljava/io/File;
    :cond_0
    invoke-virtual {v5, v0, v6, p2}, Landroid/media/MediaScanner;->scanSingleFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    goto :goto_0

    .line 364
    :cond_1
    const-string v8, "MediaScannerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is not exist file path. This scanFile request will skip."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 367
    .end local v3    # "file":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 368
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "MediaScannerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "bad path "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in scanFile()"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static setExternalStorageSdPath(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    sget-object v3, Lcom/android/providers/media/MediaScannerService;->storageManager:Landroid/os/storage/StorageManager;

    if-nez v3, :cond_0

    .line 201
    const-string v3, "storage"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    sput-object v3, Lcom/android/providers/media/MediaScannerService;->storageManager:Landroid/os/storage/StorageManager;

    .line 202
    sget-object v3, Lcom/android/providers/media/MediaScannerService;->storageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v3}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v1

    .line 203
    .local v1, "storageVolumes":[Landroid/os/storage/StorageVolume;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 204
    aget-object v2, v1, v0

    .line 205
    .local v2, "volume":Landroid/os/storage/StorageVolume;
    const-string v3, "sd"

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 206
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    .line 211
    .end local v0    # "i":I
    .end local v1    # "storageVolumes":[Landroid/os/storage/StorageVolume;
    .end local v2    # "volume":Landroid/os/storage/StorageVolume;
    :cond_0
    return-void

    .line 203
    .restart local v0    # "i":I
    .restart local v1    # "storageVolumes":[Landroid/os/storage/StorageVolume;
    .restart local v2    # "volume":Landroid/os/storage/StorageVolume;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static setTerminatedStatusForExtSdCard(Ljava/lang/String;)V
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 214
    const-string v0, "MediaScannerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTerminatedStatus is called for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    sget-object v0, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    sget-object v0, Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    sget-object v1, Lcom/android/providers/media/MediaScannerService;->syncTerminateStatus:Ljava/lang/Object;

    monitor-enter v1

    .line 217
    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lcom/android/providers/media/MediaScannerService;->stateForExtSdCardTermination:Z

    .line 218
    monitor-exit v1

    .line 220
    :cond_0
    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setTerminatedStatusForPrivate(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 223
    if-nez p0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-static {p0}, Lcom/android/providers/media/MediaProvider;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 226
    .local v0, "privatePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    sget-object v2, Lcom/android/providers/media/MediaScannerService;->syncTerminateStatus:Ljava/lang/Object;

    monitor-enter v2

    .line 228
    const/4 v1, 0x1

    :try_start_0
    sput-boolean v1, Lcom/android/providers/media/MediaScannerService;->stateForPrivateTermination:Z

    .line 229
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mBinder:Landroid/media/IMediaScannerService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 236
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/android/providers/media/MediaScannerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 237
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v2, 0x1

    const-string v3, "MediaScannerService"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/media/MediaScannerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 238
    invoke-static {p0}, Lcom/android/providers/media/MediaScannerService;->setExternalStorageSdPath(Landroid/content/Context;)V

    .line 239
    sget-object v2, Lcom/android/providers/media/MediaScannerService;->storageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/media/MediaScannerService;->mExternalStoragePaths:[Ljava/lang/String;

    .line 244
    new-instance v1, Ljava/lang/Thread;

    const/4 v2, 0x0

    const-string v3, "MediaScannerService"

    invoke-direct {v1, v2, p0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 245
    .local v1, "thr":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 246
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 319
    :goto_0
    iget-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mServiceLooper:Landroid/os/Looper;

    if-nez v0, :cond_0

    .line 320
    monitor-enter p0

    .line 322
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 328
    return-void

    .line 323
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x3

    .line 251
    :goto_0
    iget-object v6, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    if-nez v6, :cond_0

    .line 252
    monitor-enter p0

    .line 254
    const-wide/16 v6, 0x64

    :try_start_0
    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 260
    :cond_0
    if-nez p1, :cond_1

    .line 261
    const-string v5, "MediaScannerService"

    const-string v6, "Intent is null in onStartCommand: "

    new-instance v7, Ljava/lang/NullPointerException;

    invoke-direct {v7}, Ljava/lang/NullPointerException;-><init>()V

    invoke-static {v5, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 312
    :goto_2
    return v4

    .line 267
    :cond_1
    iget-object v6, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    invoke-virtual {v6}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 268
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 269
    .local v0, "arguments":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 270
    .local v2, "volume":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 271
    const-string v6, "volume"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 272
    :cond_2
    const/4 v3, 0x0

    .line 273
    .local v3, "volumeInMsg":I
    if-eqz v2, :cond_4

    .line 275
    const-string v6, "external"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 276
    iget-object v6, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    invoke-virtual {v6, v4}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 277
    const-string v4, "MediaScannerService"

    const-string v6, "We receive a same request for scanning the external storage. It will be ignored."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 278
    goto :goto_2

    .line 280
    :cond_3
    const/4 v3, 0x2

    .line 305
    :cond_4
    :goto_3
    iput v3, v1, Landroid/os/Message;->what:I

    .line 306
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 307
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 308
    iget-object v4, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    invoke-virtual {v4, v1}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    move v4, v5

    .line 312
    goto :goto_2

    .line 282
    :cond_5
    const-string v4, "internal"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 283
    iget-object v4, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 284
    const-string v4, "MediaScannerService"

    const-string v6, "We receive a same request for scanning the internal storage. It will be ignored."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 285
    goto :goto_2

    .line 287
    :cond_6
    const/4 v3, 0x1

    goto :goto_3

    .line 289
    :cond_7
    const-string v4, "external_sd"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 290
    iget-object v4, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    invoke-virtual {v4, v5}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 291
    const-string v4, "MediaScannerService"

    const-string v6, "We receive a same request for scanning the external sd storage. It will be ignored."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 292
    goto :goto_2

    .line 294
    :cond_8
    const/4 v3, 0x3

    goto :goto_3

    .line 296
    :cond_9
    const-string v4, "external_private"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 297
    iget-object v4, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 298
    const-string v4, "MediaScannerService"

    const-string v6, "We receive a same request for scanning the private storage. It will be ignored."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 299
    goto/16 :goto_2

    .line 301
    :cond_a
    const/4 v3, 0x4

    goto :goto_3

    .line 255
    .end local v0    # "arguments":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "volume":Ljava/lang/String;
    .end local v3    # "volumeInMsg":I
    :catch_0
    move-exception v6

    goto/16 :goto_1
.end method

.method public run()V
    .locals 2

    .prologue
    .line 334
    const/16 v0, 0xb

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 336
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 338
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mServiceLooper:Landroid/os/Looper;

    .line 339
    new-instance v0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;-><init>(Lcom/android/providers/media/MediaScannerService;Lcom/android/providers/media/MediaScannerService$1;)V

    iput-object v0, p0, Lcom/android/providers/media/MediaScannerService;->mServiceHandler:Lcom/android/providers/media/MediaScannerService$ServiceHandler;

    .line 341
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 342
    return-void
.end method
