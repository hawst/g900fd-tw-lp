.class final Lcom/android/providers/media/MediaScannerService$ServiceHandler;
.super Landroid/os/Handler;
.source "MediaScannerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/media/MediaScannerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/media/MediaScannerService;


# direct methods
.method private constructor <init>(Lcom/android/providers/media/MediaScannerService;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->this$0:Lcom/android/providers/media/MediaScannerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/providers/media/MediaScannerService;Lcom/android/providers/media/MediaScannerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/providers/media/MediaScannerService;
    .param p2, "x1"    # Lcom/android/providers/media/MediaScannerService$1;

    .prologue
    .line 401
    invoke-direct {p0, p1}, Lcom/android/providers/media/MediaScannerService$ServiceHandler;-><init>(Lcom/android/providers/media/MediaScannerService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x0

    .line 406
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 407
    .local v0, "arguments":Landroid/os/Bundle;
    if-nez v0, :cond_1

    move-object v6, v11

    .line 410
    .local v6, "filePath":Ljava/lang/String;
    :goto_0
    if-eqz v6, :cond_3

    .line 411
    :try_start_0
    const-string v12, "listener"

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getIBinder(Ljava/lang/String;)Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 412
    .local v2, "binder":Landroid/os/IBinder;
    if-nez v2, :cond_2

    move-object v7, v11

    .line 414
    .local v7, "listener":Landroid/media/IMediaScannerListener;
    :goto_1
    const/4 v9, 0x0

    .line 416
    .local v9, "uri":Landroid/net/Uri;
    :try_start_1
    iget-object v11, p0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->this$0:Lcom/android/providers/media/MediaScannerService;

    const-string v12, "mimetype"

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    # invokes: Lcom/android/providers/media/MediaScannerService;->scanFile(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v11, v6, v12}, Lcom/android/providers/media/MediaScannerService;->access$100(Lcom/android/providers/media/MediaScannerService;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v9

    .line 420
    :goto_2
    if-eqz v7, :cond_0

    .line 421
    :try_start_2
    invoke-interface {v7, v6, v9}, Landroid/media/IMediaScannerListener;->scanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 472
    .end local v2    # "binder":Landroid/os/IBinder;
    .end local v7    # "listener":Landroid/media/IMediaScannerListener;
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_3
    iget-object v11, p0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->this$0:Lcom/android/providers/media/MediaScannerService;

    iget v12, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v11, v12}, Lcom/android/providers/media/MediaScannerService;->stopSelf(I)V

    .line 473
    return-void

    .line 407
    .end local v6    # "filePath":Ljava/lang/String;
    :cond_1
    const-string v12, "filepath"

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 412
    .restart local v2    # "binder":Landroid/os/IBinder;
    .restart local v6    # "filePath":Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-static {v2}, Landroid/media/IMediaScannerListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IMediaScannerListener;

    move-result-object v7

    goto :goto_1

    .line 417
    .restart local v7    # "listener":Landroid/media/IMediaScannerListener;
    .restart local v9    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v5

    .line 418
    .local v5, "e":Ljava/lang/Exception;
    const-string v11, "MediaScannerService"

    const-string v12, "Exception scanning file"

    invoke-static {v11, v12, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 468
    .end local v2    # "binder":Landroid/os/IBinder;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v7    # "listener":Landroid/media/IMediaScannerListener;
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v5

    .line 469
    .restart local v5    # "e":Ljava/lang/Exception;
    const-string v11, "MediaScannerService"

    const-string v12, "Exception in handleMessage"

    invoke-static {v11, v12, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 424
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_4
    const-string v11, "volume"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 425
    .local v10, "volume":Ljava/lang/String;
    const/4 v4, 0x0

    .line 427
    .local v4, "directories":[Ljava/lang/String;
    const-string v11, "internal"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 429
    const/4 v11, 0x2

    new-array v4, v11, [Ljava/lang/String;

    .end local v4    # "directories":[Ljava/lang/String;
    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/media"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getOemDirectory()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/media"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v11

    .line 448
    .restart local v4    # "directories":[Ljava/lang/String;
    :cond_4
    :goto_4
    if-eqz v4, :cond_0

    .line 449
    const-string v11, "MediaScannerService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "!@start scanning volume "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    iget-object v11, p0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->this$0:Lcom/android/providers/media/MediaScannerService;

    # invokes: Lcom/android/providers/media/MediaScannerService;->scan([Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v11, v4, v10}, Lcom/android/providers/media/MediaScannerService;->access$400(Lcom/android/providers/media/MediaScannerService;[Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-string v11, "MediaScannerService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "!@done scanning volume "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    # getter for: Lcom/android/providers/media/MediaScannerService;->isBootComplete:Z
    invoke-static {}, Lcom/android/providers/media/MediaScannerService;->access$500()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 456
    const/4 v3, 0x0

    .line 457
    .local v3, "cfmsService":Landroid/os/ICustomFrequencyManager;
    const-string v11, "CustomFrequencyManagerService"

    invoke-static {v11}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 458
    .local v1, "b":Landroid/os/IBinder;
    invoke-static {v1}, Landroid/os/ICustomFrequencyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/ICustomFrequencyManager;

    move-result-object v3

    .line 459
    const-string v11, "REQ_DROP_CACHE"

    const-string v12, ""

    invoke-interface {v3, v11, v12}, Landroid/os/ICustomFrequencyManager;->sendCommandToSSRM(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const/4 v11, 0x0

    # setter for: Lcom/android/providers/media/MediaScannerService;->isBootComplete:Z
    invoke-static {v11}, Lcom/android/providers/media/MediaScannerService;->access$502(Z)Z

    .line 461
    const-string v11, "MediaScannerService"

    const-string v12, "send command to ssrm : REQ_DROP_CACHE"

    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    .end local v1    # "b":Landroid/os/IBinder;
    .end local v3    # "cfmsService":Landroid/os/ICustomFrequencyManager;
    :cond_5
    const-string v11, "internal"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 464
    const/4 v11, 0x1

    # setter for: Lcom/android/providers/media/MediaScannerService;->isBootComplete:Z
    invoke-static {v11}, Lcom/android/providers/media/MediaScannerService;->access$502(Z)Z

    goto/16 :goto_3

    .line 434
    :cond_6
    const-string v11, "external"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 436
    iget-object v11, p0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->this$0:Lcom/android/providers/media/MediaScannerService;

    # getter for: Lcom/android/providers/media/MediaScannerService;->mExternalStoragePaths:[Ljava/lang/String;
    invoke-static {v11}, Lcom/android/providers/media/MediaScannerService;->access$200(Lcom/android/providers/media/MediaScannerService;)[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 438
    :cond_7
    const-string v11, "external_sd"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 440
    const/4 v11, 0x1

    new-array v4, v11, [Ljava/lang/String;

    .end local v4    # "directories":[Ljava/lang/String;
    const/4 v11, 0x0

    # getter for: Lcom/android/providers/media/MediaScannerService;->mExternalStorageSdPath:Ljava/lang/String;
    invoke-static {}, Lcom/android/providers/media/MediaScannerService;->access$300()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v11

    .restart local v4    # "directories":[Ljava/lang/String;
    goto/16 :goto_4

    .line 442
    :cond_8
    const-string v11, "external_private"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 444
    iget-object v11, p0, Lcom/android/providers/media/MediaScannerService$ServiceHandler;->this$0:Lcom/android/providers/media/MediaScannerService;

    invoke-virtual {v11}, Lcom/android/providers/media/MediaScannerService;->getBaseContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/android/providers/media/MediaProvider;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 445
    .local v8, "privatePath":Ljava/lang/String;
    const/4 v11, 0x1

    new-array v4, v11, [Ljava/lang/String;

    .end local v4    # "directories":[Ljava/lang/String;
    const/4 v11, 0x0

    aput-object v8, v4, v11
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .restart local v4    # "directories":[Ljava/lang/String;
    goto/16 :goto_4
.end method
