.class public Lcom/sec/android/app/modemui/activities/USBSettings;
.super Landroid/app/Activity;
.source "USBSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final SET_DM_PORT_STATUS_LIST:[Ljava/lang/String;

.field private currentMode:I

.field private dm_modem_adb_mode:Landroid/widget/RadioButton;

.field private mUsbManager:Landroid/hardware/usb/UsbManager;

.field private mode_ID:I

.field private mtp_adb_mode:Landroid/widget/RadioButton;

.field private mtp_mode:Landroid/widget/RadioButton;

.field private ptp_adb_mode:Landroid/widget/RadioButton;

.field private ptp_mode:Landroid/widget/RadioButton;

.field radio_listener:Landroid/view/View$OnClickListener;

.field private rmnet_dm_modem_mode:Landroid/widget/RadioButton;

.field private rndis_dm_modem_mode:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    iput v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->currentMode:I

    .line 94
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "setNone"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "mtp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mtp,adb"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ptp"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ptp,adb"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "rndis"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rndis,adb"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "rndis,acm,diag"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rmnet,acm,diag"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "diag,acm,adb"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->SET_DM_PORT_STATUS_LIST:[Ljava/lang/String;

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;

    .line 101
    new-instance v0, Lcom/sec/android/app/modemui/activities/USBSettings$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/modemui/activities/USBSettings$1;-><init>(Lcom/sec/android/app/modemui/activities/USBSettings;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBSettings;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBSettings;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;

    return-object v0
.end method

.method private isMDMBaseband()Z
    .locals 4

    .prologue
    .line 311
    const-string v2, "ro.chipname"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 313
    .local v1, "solution":Ljava/lang/String;
    const-string v2, "NONE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 314
    const-string v2, "ro.product.board"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    :cond_0
    const-string v2, "ro.baseband"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "Baseband":Ljava/lang/String;
    const-string v2, "smdk4x12"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "mdm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 320
    const/4 v2, 0x1

    .line 323
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setDefaultSelection(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x1

    .line 276
    const-string v0, "USBSettings"

    const-string v1, "setDefaultSelection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CurrentDefaultSelection : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    packed-switch p1, :pswitch_data_0

    .line 308
    :goto_0
    :pswitch_0
    return-void

    .line 280
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mtp_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 283
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mtp_adb_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 286
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->ptp_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 289
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->ptp_adb_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 297
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rndis_dm_modem_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 300
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rmnet_dm_modem_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 303
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->dm_modem_adb_mode:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 214
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 228
    :goto_0
    return-void

    .line 216
    :pswitch_0
    const-string v0, "USBSettings"

    const-string v1, "onClick"

    const-string v2, "OK USBSetting"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Saved! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 218
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 221
    :pswitch_1
    const-string v0, "USBSettings"

    const-string v1, "onClick"

    const-string v2, "cancel USBSetting"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "Cancel!"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 223
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x7f090019
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 176
    const-string v2, "USBSettings"

    const-string v3, "onCreate"

    const-string v4, "Class create!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const v2, 0x7f030020

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->setContentView(I)V

    .line 178
    const-string v2, "usb"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbManager;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;

    .line 179
    const v2, 0x7f090076

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mtp_mode:Landroid/widget/RadioButton;

    .line 180
    const v2, 0x7f090077

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mtp_adb_mode:Landroid/widget/RadioButton;

    .line 181
    const v2, 0x7f090078

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->ptp_mode:Landroid/widget/RadioButton;

    .line 182
    const v2, 0x7f090079

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->ptp_adb_mode:Landroid/widget/RadioButton;

    .line 187
    const v2, 0x7f09007a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rndis_dm_modem_mode:Landroid/widget/RadioButton;

    .line 188
    const v2, 0x7f09007b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rmnet_dm_modem_mode:Landroid/widget/RadioButton;

    .line 189
    const v2, 0x7f09007c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->dm_modem_adb_mode:Landroid/widget/RadioButton;

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mtp_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->mtp_adb_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->ptp_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->ptp_adb_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rndis_dm_modem_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rmnet_dm_modem_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->dm_modem_adb_mode:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->radio_listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    const v2, 0x7f090019

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 202
    .local v1, "ok_button":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    const v2, 0x7f09001a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 204
    .local v0, "cancel_button":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/modemui/activities/USBSettings;->readCurrentSettings()V

    .line 207
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/USBSettings;->isMDMBaseband()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/USBSettings;->rmnet_dm_modem_mode:Landroid/widget/RadioButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 210
    :cond_0
    return-void
.end method

.method public readCurrentSettings()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 231
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "readCurrentSettings"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v0, 0x0

    .line 233
    .local v0, "selected_mode_id":I
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 234
    .local v1, "usbSetStatus":Ljava/lang/String;
    const-string v2, "sys.usb.config"

    const-string v3, "None"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CurrentUSB Setting : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v2, "mtp,adb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_1

    .line 238
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is mtp,adb"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x2

    .line 272
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/modemui/activities/USBSettings;->setDefaultSelection(I)V

    .line 273
    return-void

    .line 241
    :cond_1
    const-string v2, "mtp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_2

    .line 242
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is mtp"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/4 v0, 0x1

    goto :goto_0

    .line 244
    :cond_2
    const-string v2, "ptp,adb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_3

    .line 245
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is ptp,adb"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v0, 0x4

    goto :goto_0

    .line 248
    :cond_3
    const-string v2, "ptp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_4

    .line 249
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is ptp"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/4 v0, 0x3

    goto :goto_0

    .line 251
    :cond_4
    const-string v2, "rndis,acm,diag"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_5

    .line 252
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is rndis,acm,diag"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const/4 v0, 0x7

    goto :goto_0

    .line 255
    :cond_5
    const-string v2, "rndis,adb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_6

    .line 256
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is rndis,adb"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/4 v0, 0x6

    goto :goto_0

    .line 259
    :cond_6
    const-string v2, "rndis"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_7

    .line 260
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is rndis"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x5

    goto :goto_0

    .line 262
    :cond_7
    const-string v2, "rmnet,acm,diag"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_8

    .line 263
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is rmnet,acm,diag"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 266
    :cond_8
    const-string v2, "diag,acm,adb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_0

    .line 267
    const-string v2, "USBSettings"

    const-string v3, "readCurrentSettings"

    const-string v4, "Check Radio Button is diag,acm,adb"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const/16 v0, 0x9

    goto/16 :goto_0
.end method
