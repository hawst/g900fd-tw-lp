.class Lcom/sec/android/app/servicemodeapp/SysDump$7$11;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V
    .locals 0

    .prologue
    .line 2537
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2539
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "logcat -v threadtime -b radio -d -f /data/log/radio_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v2, v2, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5100(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->DoShellCmd(Ljava/lang/String;)I

    .line 2541
    const-string v0, "SysDump"

    const-string v1, "SendData(mOem.OEM_MODEM_LOG)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2542
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5500()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2543
    const-string v0, "sys.trace.control"

    const-string v1, "path=dump"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iscpdumpdone:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$102(Lcom/sec/android/app/servicemodeapp/SysDump;Z)Z

    .line 2545
    const-string v0, "SysDump"

    const-string v1, "MODEMLOG_DONE Success, isEOS2=true"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2563
    :cond_0
    :goto_0
    return-void

    .line 2558
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x12

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2559
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2300(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2560
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3fb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
