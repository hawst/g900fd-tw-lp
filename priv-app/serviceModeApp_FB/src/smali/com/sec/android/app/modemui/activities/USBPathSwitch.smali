.class public Lcom/sec/android/app/modemui/activities/USBPathSwitch;
.super Landroid/app/Activity;
.source "USBPathSwitch.java"


# instance fields
.field private mSaveResetButton:Landroid/widget/Button;

.field private mUartToAp:Z

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbSettings:Landroid/widget/Button;

.field private mUsbToAp:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbToAp:Z

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->isUartAP()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUartToAp:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/USBPathSwitch;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBPathSwitch;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->changeUsb(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbToAp:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/modemui/activities/USBPathSwitch;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBPathSwitch;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbToAp:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUartToAp:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/modemui/activities/USBPathSwitch;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/USBPathSwitch;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->doRebootNSave(I)V

    return-void
.end method

.method private changeUsb(Ljava/lang/String;)V
    .locals 3
    .param p1, "usb"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    const-string v0, "USBPathSwitch"

    const-string v1, "changeUsb"

    const-string v2, "USB to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "PDA"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "USBPathSwitch"

    const-string v1, "changeUsb"

    const-string v2, "USB to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "MODEM"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 119
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "USBPathSwitch"

    const-string v3, "doRebootNSave"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchsel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 122
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method private initialSetting(Landroid/widget/RadioGroup;)V
    .locals 5
    .param p1, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    .line 127
    const-string v1, "/sys/class/sec/switch/usb_sel"

    invoke-direct {p0, v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "currentUsb":Ljava/lang/String;
    const-string v1, "USBPathSwitch"

    const-string v2, "initialSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "USB_Init : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v1, "MODEM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    const-string v1, "USBPathSwitch"

    const-string v2, "initialSetting"

    const-string v3, "Initial USB_STATUS1 : MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const v1, 0x7f090028

    invoke-virtual {p1, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    const-string v1, "PDA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    const-string v1, "USBPathSwitch"

    const-string v2, "initialSetting"

    const-string v3, "Initial USB_STATUS1 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const v1, 0x7f090029

    invoke-virtual {p1, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method

.method private isUartAP()Z
    .locals 2

    .prologue
    .line 194
    const-string v1, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "currentUart":Ljava/lang/String;
    const-string v1, "AP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    const/4 v1, 0x1

    .line 197
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 154
    const-string v6, ""

    .line 155
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 156
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 159
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 162
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 163
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 173
    :cond_0
    if-eqz v5, :cond_1

    .line 174
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 177
    :cond_1
    if-eqz v1, :cond_2

    .line 178
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 186
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 187
    const-string v7, ""

    .line 189
    :goto_1
    return-object v7

    .line 180
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 181
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "USBPathSwitch"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 184
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 165
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 166
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "USBPathSwitch"

    const-string v8, "readOneLine"

    const-string v9, "FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 173
    if-eqz v4, :cond_4

    .line 174
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 177
    :cond_4
    if-eqz v0, :cond_3

    .line 178
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 180
    :catch_2
    move-exception v2

    .line 181
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "USBPathSwitch"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 168
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 169
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "USBPathSwitch"

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 173
    if-eqz v4, :cond_5

    .line 174
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 177
    :cond_5
    if-eqz v0, :cond_3

    .line 178
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 180
    :catch_4
    move-exception v2

    .line 181
    const-string v7, "USBPathSwitch"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 172
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 173
    :goto_4
    if-eqz v4, :cond_6

    .line 174
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 177
    :cond_6
    if-eqz v0, :cond_7

    .line 178
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 183
    :cond_7
    :goto_5
    throw v7

    .line 180
    :catch_5
    move-exception v2

    .line 181
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "USBPathSwitch"

    const-string v9, "readOneLine"

    const-string v10, "IOException close()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 189
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 172
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 168
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 165
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 201
    const/4 v1, 0x0

    .line 204
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 209
    const-string v3, "USBPathSwitch"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    if-eqz v2, :cond_2

    .line 213
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 219
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 214
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "USBPathSwitch"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 216
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 206
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 207
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "USBPathSwitch"

    const-string v4, "writeFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOExceptionfilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 209
    const-string v3, "USBPathSwitch"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    if-eqz v1, :cond_0

    .line 213
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 214
    :catch_2
    move-exception v0

    .line 215
    const-string v3, "USBPathSwitch"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "USBPathSwitch"

    const-string v5, "writeFile"

    const-string v6, "Write Success!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    if-eqz v1, :cond_1

    .line 213
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 216
    :cond_1
    :goto_3
    throw v3

    .line 214
    :catch_3
    move-exception v0

    .line 215
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "USBPathSwitch"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 209
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 206
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->setContentView(I)V

    .line 68
    const-string v0, "USBPathSwitch"

    const-string v1, "onCreate"

    const-string v2, "Hello oncreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;-><init>(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 98
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mSaveResetButton:Landroid/widget/Button;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mSaveResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/USBPathSwitch$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch$2;-><init>(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    const v0, 0x7f090074

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/USBPathSwitch$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch$3;-><init>(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->initialSetting(Landroid/widget/RadioGroup;)V

    .line 116
    return-void
.end method
