.class public Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;
.super Landroid/app/Activity;
.source "DiagMdlogOption.java"


# instance fields
.field private alert:Landroid/app/AlertDialog;

.field private diag_mb:I

.field foundFiles:[Ljava/lang/String;

.field paths_to_search:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I

    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/etc"

    aput-object v2, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->paths_to_search:[Ljava/lang/String;

    .line 21
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->foundFiles:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;
    .param p1, "x1"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private hideDialog()Z
    .locals 2

    .prologue
    .line 47
    const-string v0, "DiagMdlogOption"

    const-string v1, "hideDialog()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 50
    const/4 v0, 0x0

    .line 56
    :goto_0
    return v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 55
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    .line 56
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public chooseDialog()V
    .locals 6

    .prologue
    .line 105
    const/4 v2, -0x1

    .line 106
    .local v2, "pos":I
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->findItems()[Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "items":[Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "diag_mdlog size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 111
    const/4 v4, -0x1

    new-instance v5, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;-><init>(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 126
    new-instance v3, Landroid/widget/SeekBar;

    invoke-direct {v3, p0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 127
    .local v3, "sb":Landroid/widget/SeekBar;
    const/16 v4, 0x13

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 128
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 130
    new-instance v4, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;-><init>(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)V

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 149
    new-instance v4, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$3;-><init>(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 157
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 160
    return-void
.end method

.method public concatStrArray([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "a"    # [Ljava/lang/String;
    .param p2, "b"    # [Ljava/lang/String;
    .param p3, "prefix"    # Ljava/lang/String;

    .prologue
    .line 30
    if-nez p2, :cond_0

    .line 38
    .end local p1    # "a":[Ljava/lang/String;
    :goto_0
    return-object p1

    .line 32
    .restart local p1    # "a":[Ljava/lang/String;
    :cond_0
    array-length v4, p1

    array-length v5, p2

    add-int/2addr v4, v5

    new-array v3, v4, [Ljava/lang/String;

    .line 33
    .local v3, "result":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 34
    aget-object v4, p1, v0

    aput-object v4, v3, v0

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 35
    :cond_1
    const/4 v1, 0x0

    .line 36
    .local v1, "k":I
    array-length v0, p1

    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_2

    .line 37
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "k":I
    .local v2, "k":I
    aget-object v5, p2, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 36
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "k":I
    .restart local v1    # "k":I
    goto :goto_2

    :cond_2
    move-object p1, v3

    .line 38
    goto :goto_0
.end method

.method public files(Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 67
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "config_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    .line 69
    const-string v8, "DiagMdlogOption"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Config dir not found : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    return-object v6

    .line 72
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v7

    .line 73
    .local v7, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 74
    .local v1, "count":I
    const-string v8, "\\w+\\.cfg"

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 75
    .local v5, "pattern":Ljava/util/regex/Pattern;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, v7

    if-ge v2, v8, :cond_3

    .line 76
    aget-object v8, v7, v2

    invoke-virtual {v5, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 77
    .local v4, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 78
    add-int/lit8 v1, v1, 0x1

    .line 79
    const-string v8, "DiagMdlogOption"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v7, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    :cond_2
    aput-object v6, v7, v2

    goto :goto_1

    .line 83
    .end local v4    # "matcher":Ljava/util/regex/Matcher;
    :cond_3
    new-array v6, v1, [Ljava/lang/String;

    .line 84
    .local v6, "result":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 86
    .local v3, "j":I
    const/4 v2, 0x0

    :goto_2
    array-length v8, v7

    if-ge v2, v8, :cond_0

    .line 87
    aget-object v8, v7, v2

    if-eqz v8, :cond_4

    .line 88
    aget-object v8, v7, v2

    aput-object v8, v6, v3

    .line 89
    add-int/lit8 v3, v3, 0x1

    .line 86
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public findItems()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 95
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const v4, 0x7f0700af

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 97
    .local v1, "result":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->paths_to_search:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->paths_to_search:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->files(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "temp":[Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->concatStrArray([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 100
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->foundFiles:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->paths_to_search:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v2, v4}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->concatStrArray([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->foundFiles:[Ljava/lang/String;

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    .end local v2    # "temp":[Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->foundFiles:[Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f0700af

    invoke-virtual {p0, v2}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 27
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->hideDialog()Z

    .line 43
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 44
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->chooseDialog()V

    .line 63
    return-void
.end method
