.class public Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;
.super Landroid/app/Activity;
.source "PhoneUtil_TD.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;
    }
.end annotation


# instance fields
.field private final ACTION_REQUEST_START_FTCLIENT:Ljava/lang/String;

.field public mHandler:Landroid/os/Handler;

.field private mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;

.field private mResultText:Landroid/widget/TextView;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mdualmodeON:Landroid/widget/Button;

.field private phone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    const-string v0, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->ACTION_REQUEST_START_FTCLIENT:Ljava/lang/String;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->phone:Lcom/android/internal/telephony/Phone;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mServiceMessenger:Landroid/os/Messenger;

    .line 98
    new-instance v0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mHandler:Landroid/os/Handler;

    .line 297
    new-instance v0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$5;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 335
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSvcModeMessenger:Landroid/os/Messenger;

    return-void
.end method

.method private SendData(III)V
    .locals 5
    .param p1, "maincmd"    # I
    .param p2, "subcmd"    # I
    .param p3, "value"    # I

    .prologue
    .line 309
    const/4 v0, 0x0

    .line 311
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;->sendDualModeValue(III)[B

    move-result-object v0

    .line 313
    if-nez v0, :cond_0

    .line 314
    const-string v1, "PhoneUtil_TD"

    const-string v2, "SendData"

    const-string v3, " err - data is NULL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 318
    :cond_0
    const-string v1, "PhoneUtil_TD"

    const-string v2, "SendData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "data : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->changeUartPath(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;III)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->SendData(III)V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method private changeUartPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 256
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 257
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.factory.aporiented.athandler.AtUartswit.SetUartPath"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const-string v2, "PATH"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->sendBroadcast(Landroid/content/Intent;)V

    .line 260
    const/16 v2, 0x1f4

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->getCurrentUARTPath(I)Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "path":Ljava/lang/String;
    return-void
.end method

.method private connectToRilService()V
    .locals 4

    .prologue
    .line 291
    const-string v1, "PhoneUtil_TD"

    const-string v2, "connectToRilService"

    const-string v3, "connect To Ril service"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 293
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 295
    return-void
.end method

.method private getCurrentUARTPath(I)Ljava/lang/String;
    .locals 11
    .param p1, "millis"    # I

    .prologue
    const v10, 0x7f090025

    .line 201
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCurrentUARTStatus : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    int-to-long v6, p1

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    const/4 v2, 0x0

    .line 210
    .local v2, "path":Ljava/lang/String;
    const-string v0, ""

    .line 211
    .local v0, "correct_path":Ljava/lang/String;
    const/4 v3, 0x0

    .line 212
    .local v3, "reader":Ljava/io/BufferedReader;
    const-string v0, "data/misc/radio/uart.txt"

    .line 213
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FILE PATH : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 218
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    if-eqz v4, :cond_0

    .line 219
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "readerTemp":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 221
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 228
    .end local v5    # "readerTemp":Ljava/lang/String;
    :cond_0
    if-eqz v4, :cond_6

    .line 230
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v3, v4

    .line 238
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_1
    if-eqz v2, :cond_5

    .line 239
    const-string v6, "CP"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 240
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "getCurrentUARTStatus : CP"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mUartRadioGroup:Landroid/widget/RadioGroup;

    const v7, 0x7f090024

    invoke-virtual {v6, v7}, Landroid/widget/RadioGroup;->check(I)V

    .line 252
    :cond_2
    :goto_2
    return-object v2

    .line 205
    .end local v0    # "correct_path":Ljava/lang/String;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "correct_path":Ljava/lang/String;
    .restart local v2    # "path":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v1

    .line 232
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Closed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 234
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 224
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 225
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_4
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Read"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 228
    if-eqz v3, :cond_1

    .line 230
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 231
    :catch_3
    move-exception v1

    .line 232
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Closed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 228
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v3, :cond_3

    .line 230
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 234
    :cond_3
    :goto_5
    throw v6

    .line 231
    :catch_4
    move-exception v1

    .line 232
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_TD"

    const-string v8, "getCurrentUARTPath"

    const-string v9, "Exception! While Buffer Closed"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 242
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    const-string v6, "AP"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 243
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "getCurrentUARTStatus : AP"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mUartRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v6, v10}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_2

    .line 247
    :cond_5
    const-string v6, "PhoneUtil_TD"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "path == null"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v2, "AP"

    .line 249
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mUartRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v6, v10}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_2

    .line 228
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 224
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_5
    move-exception v1

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_6
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 323
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 324
    .local v1, "req":Landroid/os/Bundle;
    const-string v2, "request"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 325
    invoke-virtual {p2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v2, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 329
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :goto_0
    return-void

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private runFtClient()V
    .locals 3

    .prologue
    .line 196
    const-string v0, "PhoneUtil_TD"

    const-string v1, "runFtClient"

    const-string v2, "send ACTION_REQUEST_START_FTCLIENT"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->sendBroadcast(Landroid/content/Intent;)V

    .line 198
    return-void
.end method


# virtual methods
.method public display(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x0

    .line 114
    if-nez p1, :cond_1

    .line 115
    const-string v0, "Nothing updated!"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 117
    const-string v0, "DUAL MODE CHANGED"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 123
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->setContentView(I)V

    .line 124
    const-string v0, "PhoneUtil_TD"

    const-string v1, "onCreate"

    const-string v2, "onCreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 148
    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mResultText:Landroid/widget/TextView;

    .line 150
    const-string v0, "true"

    const-string v1, "persist.radio.master.testmode"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mResultText:Landroid/widget/TextView;

    const-string v1, "Auto mode change off"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :goto_0
    const v0, 0x7f090035

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mdualmodeON:Landroid/widget/Button;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mdualmodeON:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$3;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const v0, 0x7f090036

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mdualmodeON:Landroid/widget/Button;

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mdualmodeON:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$4;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->getCurrentUARTPath(I)Ljava/lang/String;

    .line 178
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->runFtClient()V

    .line 179
    new-instance v0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$1;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->connectToRilService()V

    .line 183
    return-void

    .line 152
    :cond_0
    const-string v0, "false"

    const-string v1, "persist.radio.master.testmode"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mResultText:Landroid/widget/TextView;

    const-string v1, "Auto mode change on"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mResultText:Landroid/widget/TextView;

    const-string v1, "test is not ok"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->unbindService(Landroid/content/ServiceConnection;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 192
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 193
    return-void
.end method
