.class Lcom/sec/android/app/modemui/activities/PhoneUtil$3;
.super Ljava/lang/Object;
.source "PhoneUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 178
    const/4 v0, -0x1

    .line 180
    .local v0, "parameter":I
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->isFactoryAppAPO()Z
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$400(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    const-string v1, "PhoneUtil"

    const-string v4, "onClick"

    const-string v5, "isFactoryAppAPO true"

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const-string v4, "+APUART"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUartPath(Ljava/lang/String;)V
    invoke-static {v1, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$500(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V

    .line 189
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    shl-int/lit8 v1, v1, 0x1

    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbToAp:Z
    invoke-static {v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    or-int v0, v1, v2

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->doRebootNSave(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$600(Lcom/sec/android/app/modemui/activities/PhoneUtil;I)V

    .line 191
    return-void

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const-string v4, "+CPUART"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUartPath(Ljava/lang/String;)V
    invoke-static {v1, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$500(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 189
    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method
