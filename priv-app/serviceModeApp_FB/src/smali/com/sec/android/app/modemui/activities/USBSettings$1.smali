.class Lcom/sec/android/app/modemui/activities/USBSettings$1;
.super Ljava/lang/Object;
.source "USBSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/USBSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/USBSettings;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/USBSettings;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 104
    move-object v0, p1

    check-cast v0, Landroid/widget/RadioButton;

    .line 105
    .local v0, "rb":Landroid/widget/RadioButton;
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 106
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "OnClickListener"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 166
    const-string v1, "CLASS_NAME"

    const-string v2, "onClick"

    const-string v3, "default"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_0
    return-void

    .line 110
    :pswitch_0
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "MTP_MODE "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v4}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "mtp"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_0

    .line 116
    :pswitch_1
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "MTP_ADB_MODE "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    const/4 v2, 0x2

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "mtp,adb"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_0

    .line 122
    :pswitch_2
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "PTP_MODE"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    const/4 v2, 0x3

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "ptp"

    invoke-virtual {v1, v2, v5}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_0

    .line 128
    :pswitch_3
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "PTP_ADB_MODE"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    const/4 v2, 0x4

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "ptp,adb"

    invoke-virtual {v1, v2, v5}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 148
    :pswitch_4
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "RNDIS_DM_MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    const/4 v2, 0x7

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "rndis,acm,diag"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 154
    :pswitch_5
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "RMNET_DM_MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    const/16 v2, 0x8

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "rmnet,acm,diag"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 160
    :pswitch_6
    const-string v1, "USBSettings"

    const-string v2, "onClick"

    const-string v3, "DM_MODEM_ADB"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    const/16 v2, 0x9

    # setter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mode_ID:I
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$002(Lcom/sec/android/app/modemui/activities/USBSettings;I)I

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    invoke-virtual {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "adb_enabled"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBSettings$1;->this$0:Lcom/sec/android/app/modemui/activities/USBSettings;

    # getter for: Lcom/sec/android/app/modemui/activities/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBSettings;->access$100(Lcom/sec/android/app/modemui/activities/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v1

    const-string v2, "diag,acm,adb"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x7f090076
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
