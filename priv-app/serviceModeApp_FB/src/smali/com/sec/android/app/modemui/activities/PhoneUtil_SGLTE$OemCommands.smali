.class Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
.super Ljava/lang/Object;
.source "PhoneUtil_SGLTE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_MAIN_CMD_CFG:I

.field final OEM_SUB_CMD_DUAL_MODE:I

.field final OEM_SUB_CMD_SGLTE_BAND:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;->OEM_MAIN_CMD_CFG:I

    .line 339
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;->OEM_SUB_CMD_DUAL_MODE:I

    .line 342
    const/16 v0, 0x52

    iput v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;->OEM_SUB_CMD_SGLTE_BAND:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$1;

    .prologue
    .line 334
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;-><init>()V

    return-void
.end method


# virtual methods
.method sendDualModeValue(III)[B
    .locals 8
    .param p1, "maincmd"    # I
    .param p2, "subcmd"    # I
    .param p3, "value"    # I

    .prologue
    .line 345
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 346
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 347
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 348
    .local v3, "fileSize":C
    const-string v4, "PhoneUtil_SGLTE"

    const-string v5, "sendDualModeValue"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "subcmd = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 351
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 352
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 353
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 354
    :catch_0
    move-exception v2

    .line 355
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "PhoneUtil_SGLTE"

    const-string v5, "getServMEnterData"

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const/4 v4, 0x0

    goto :goto_0
.end method
