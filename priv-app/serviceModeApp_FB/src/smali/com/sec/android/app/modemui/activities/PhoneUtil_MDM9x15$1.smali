.class Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;
.super Ljava/lang/Object;
.source "PhoneUtil_MDM9x15.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 132
    const v1, 0x7f090024

    if-ne p2, v1, :cond_1

    .line 134
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;Z)Z

    .line 140
    const-string v1, "PhoneUtil_MDM9x15"

    const-string v2, "onCheckedChanged"

    const-string v3, "UART_STATUS1 : MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_MDM9x15"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! UART to Modem"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090025

    if-ne p2, v1, :cond_0

    .line 147
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    const-string v2, "PDA"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;Z)Z

    .line 155
    const-string v1, "PhoneUtil_MDM9x15"

    const-string v2, "onCheckedChanged"

    const-string v3, "UART_STATUS2 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 156
    :catch_1
    move-exception v0

    .line 157
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_MDM9x15"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! UART to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
