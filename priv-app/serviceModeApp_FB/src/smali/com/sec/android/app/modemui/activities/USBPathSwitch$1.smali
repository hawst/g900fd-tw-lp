.class Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;
.super Ljava/lang/Object;
.source "USBPathSwitch.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/USBPathSwitch;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 73
    const v1, 0x7f090028

    if-ne p2, v1, :cond_1

    .line 75
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$000(Lcom/sec/android/app/modemui/activities/USBPathSwitch;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$102(Lcom/sec/android/app/modemui/activities/USBPathSwitch;Z)Z

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    # getter for: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$200(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    # getter for: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$200(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Landroid/widget/Button;

    move-result-object v1

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 79
    const-string v1, "USBPathSwitch"

    const-string v2, "onCheckedChanged"

    const-string v3, "USB_STATUS1 : MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "USBPathSwitch"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! USB to MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090029

    if-ne p2, v1, :cond_0

    .line 86
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    const-string v2, "PDA"

    # invokes: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$000(Lcom/sec/android/app/modemui/activities/USBPathSwitch;Ljava/lang/String;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$102(Lcom/sec/android/app/modemui/activities/USBPathSwitch;Z)Z

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    # getter for: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$200(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/USBPathSwitch$1;->this$0:Lcom/sec/android/app/modemui/activities/USBPathSwitch;

    # getter for: Lcom/sec/android/app/modemui/activities/USBPathSwitch;->mUsbSettings:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/USBPathSwitch;->access$200(Lcom/sec/android/app/modemui/activities/USBPathSwitch;)Landroid/widget/Button;

    move-result-object v1

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 90
    const-string v1, "USBPathSwitch"

    const-string v2, "onCheckedChanged"

    const-string v3, "USB_STATUS1 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    .line 92
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "USBPathSwitch"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! USB to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
