.class public Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;
.super Landroid/app/Activity;
.source "PhoneUtil_MDM9x15.java"


# instance fields
.field private final AP_PDA_SELECTION:[B

.field private final CP_MODEM_SELECTION:[B

.field private MODEL:Ljava/lang/String;

.field private mGoQualcommUSBSettingMenu:Landroid/widget/Button;

.field private mSaveResetButton:Landroid/widget/Button;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUartToAp:Z

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbToAp:Z

.field private final modem:[B

.field private final pda:[B

.field private textUSB:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 59
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->MODEL:Ljava/lang/String;

    .line 95
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->AP_PDA_SELECTION:[B

    .line 99
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->pda:[B

    .line 103
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->modem:[B

    .line 107
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->CP_MODEM_SELECTION:[B

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartToAp:Z

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUsbToAp:Z

    return-void

    .line 95
    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 99
    nop

    :array_1
    .array-data 1
        0x50t
        0x44t
        0x41t
        0x0t
    .end array-data

    .line 103
    :array_2
    .array-data 1
        0x4dt
        0x4ft
        0x44t
        0x45t
        0x4dt
        0x0t
    .end array-data

    .line 107
    nop

    :array_3
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->changeUart(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartToAp:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartToAp:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUsbToAp:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;
    .param p1, "x1"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->doRebootNSave(I)V

    return-void
.end method

.method private changeUart(Ljava/lang/String;)V
    .locals 3
    .param p1, "uart"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    const-string v0, "PhoneUtil_MDM9x15"

    const-string v1, "changeUart"

    const-string v2, "UART to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "AP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const-string v0, "PhoneUtil_MDM9x15"

    const-string v1, "changeUart"

    const-string v2, "UART to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "CP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 255
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 256
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "doRebootNSave"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchsel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 258
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 259
    return-void
.end method

.method private getPixels(I)I
    .locals 5
    .param p1, "dipValue"    # I

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 242
    .local v1, "r":Landroid/content/res/Resources;
    const/4 v2, 0x1

    int-to-float v3, p1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v0, v2

    .line 244
    .local v0, "px":I
    return v0
.end method

.method private initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V
    .locals 6
    .param p1, "uart"    # Landroid/widget/RadioGroup;
    .param p2, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    .line 269
    const-string v2, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "currentUart":Ljava/lang/String;
    const-string v2, "/sys/class/sec/switch/usb_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "currentUsb":Ljava/lang/String;
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UART_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "USB_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v2, "CP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 275
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : MODEM"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const v2, 0x7f090024

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 282
    :cond_0
    :goto_0
    const-string v2, "MODEM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 283
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : MODEM"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const v2, 0x7f090028

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 289
    :cond_1
    :goto_1
    return-void

    .line 277
    :cond_2
    const-string v2, "AP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const v2, 0x7f090025

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 285
    :cond_3
    const-string v2, "PDA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const v2, 0x7f090029

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 352
    const-string v6, ""

    .line 353
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 354
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 357
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 360
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 361
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 371
    :cond_0
    if-eqz v5, :cond_1

    .line 372
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 375
    :cond_1
    if-eqz v1, :cond_2

    .line 376
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 384
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 385
    const-string v7, ""

    .line 387
    :goto_1
    return-object v7

    .line 378
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 379
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_MDM9x15"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 382
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 363
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 364
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "PhoneUtil_MDM9x15"

    const-string v8, "readOneLine"

    const-string v9, "FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 371
    if-eqz v4, :cond_4

    .line 372
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 375
    :cond_4
    if-eqz v0, :cond_3

    .line 376
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 378
    :catch_2
    move-exception v2

    .line 379
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_MDM9x15"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 366
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 367
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "PhoneUtil_MDM9x15"

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 371
    if-eqz v4, :cond_5

    .line 372
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 375
    :cond_5
    if-eqz v0, :cond_3

    .line 376
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 378
    :catch_4
    move-exception v2

    .line 379
    const-string v7, "PhoneUtil_MDM9x15"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 370
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 371
    :goto_4
    if-eqz v4, :cond_6

    .line 372
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 375
    :cond_6
    if-eqz v0, :cond_7

    .line 376
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 381
    :cond_7
    :goto_5
    throw v7

    .line 378
    :catch_5
    move-exception v2

    .line 379
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "PhoneUtil_MDM9x15"

    const-string v9, "readOneLine"

    const-string v10, "IOException close()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 387
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 370
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 366
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 363
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private runFtClient()V
    .locals 4

    .prologue
    .line 248
    const-string v1, "PhoneUtil_MDM9x15"

    const-string v2, "runFtClient"

    const-string v3, "runFtClient()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 250
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.aporiented.FtClient"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 252
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 392
    const/4 v1, 0x0

    .line 395
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 401
    const-string v3, "PhoneUtil_MDM9x15"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    if-eqz v2, :cond_2

    .line 405
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 411
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 406
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "PhoneUtil_MDM9x15"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 408
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 397
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 398
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "PhoneUtil_MDM9x15"

    const-string v4, "writeFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOExceptionfilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 401
    const-string v3, "PhoneUtil_MDM9x15"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    if-eqz v1, :cond_0

    .line 405
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 406
    :catch_2
    move-exception v0

    .line 407
    const-string v3, "PhoneUtil_MDM9x15"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 401
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "PhoneUtil_MDM9x15"

    const-string v5, "writeFile"

    const-string v6, "Write Success!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    if-eqz v1, :cond_1

    .line 405
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 408
    :cond_1
    :goto_3
    throw v3

    .line 406
    :catch_3
    move-exception v0

    .line 407
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "PhoneUtil_MDM9x15"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 401
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 397
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x8

    const/4 v5, -0x1

    .line 123
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    const v2, 0x7f030011

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->setContentView(I)V

    .line 126
    const-string v2, "PhoneUtil_MDM9x15"

    const-string v3, "onCreate"

    const-string v4, "Hello oncreate()"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const v2, 0x7f090023

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 163
    const v2, 0x7f090027

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v6}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 165
    const v2, 0x7f09002c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->textUSB:Landroid/widget/TextView;

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->textUSB:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    const v2, 0x7f09002b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mSaveResetButton:Landroid/widget/Button;

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mSaveResetButton:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUartRadioGroup:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V

    .line 216
    new-instance v2, Landroid/widget/Button;

    invoke-direct {v2, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    const-string v3, "Qualcomm USB Settings"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15$3;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 226
    .local v1, "lContainerLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/16 v3, 0x3c

    invoke-direct {p0, v3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->getPixels(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 230
    .local v0, "lButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 231
    const/16 v2, 0x64

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 235
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MDM9x15;->runFtClient()V

    .line 238
    return-void
.end method
