.class public Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;
.super Landroid/app/Activity;
.source "ResetTotalCallTime.java"


# instance fields
.field public mHandler:Landroid/os/Handler;

.field mText:Landroid/widget/TextView;

.field private mTotalCallTime:J

.field private phone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->phone:Lcom/android/internal/telephony/Phone;

    .line 150
    new-instance v0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime$1;-><init>(Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private GetTotalCallTime()V
    .locals 7

    .prologue
    .line 126
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 127
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 130
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v4, 0xf

    :try_start_0
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 131
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 132
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 138
    .local v1, "data":[B
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->phone:Lcom/android/internal/telephony/Phone;

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x3f4

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 139
    return-void

    .line 133
    .end local v1    # "data":[B
    :catch_0
    move-exception v3

    .line 134
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "TotalCallTime"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private SetTotalCallTime()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x2

    const-wide/16 v8, 0xff

    const/4 v6, 0x0

    .line 101
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 102
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 103
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/4 v4, 0x4

    new-array v0, v4, [B

    .line 104
    .local v0, "ba":[B
    iget-wide v4, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mTotalCallTime:J

    and-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v6

    .line 105
    const/4 v4, 0x1

    iget-wide v6, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mTotalCallTime:J

    shr-long/2addr v6, v11

    and-long/2addr v6, v8

    long-to-int v5, v6

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 106
    iget-wide v4, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mTotalCallTime:J

    const/16 v6, 0x10

    shr-long/2addr v4, v6

    and-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v10

    .line 107
    const/4 v4, 0x3

    iget-wide v6, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mTotalCallTime:J

    const/16 v5, 0x18

    shr-long/2addr v6, v5

    and-long/2addr v6, v8

    long-to-int v5, v6

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 110
    const/16 v4, 0xf

    :try_start_0
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 111
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 112
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 113
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 114
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 115
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 116
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->phone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3f3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 123
    return-void

    .line 117
    :catch_0
    move-exception v3

    .line 118
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "TotalCallTime"

    const-string v5, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->GetTotalCallTime()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mTotalCallTime:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;
    .param p1, "x1"    # J

    .prologue
    .line 75
    iput-wide p1, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mTotalCallTime:J

    return-wide p1
.end method

.method static synthetic access$200([BI)J
    .locals 2
    .param p0, "x0"    # [B
    .param p1, "x1"    # I

    .prologue
    .line 75
    invoke-static {p0, p1}, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->byteArrayToInt([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method private static byteArrayToInt([BI)J
    .locals 4
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 142
    const-wide/16 v0, 0x0

    .line 143
    .local v0, "result":J
    add-int/lit8 v2, p1, 0x0

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 144
    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x8

    const v3, 0xff00

    and-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 145
    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x10

    const/high16 v3, 0xff0000

    and-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 146
    add-int/lit8 v2, p1, 0x3

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x18

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 147
    return-wide v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->setContentView(I)V

    .line 95
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->phone:Lcom/android/internal/telephony/Phone;

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->SetTotalCallTime()V

    .line 97
    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/ResetTotalCallTime;->mText:Landroid/widget/TextView;

    .line 98
    return-void
.end method
