.class public Lcom/sec/android/app/servicemodeapp/TotalCallTime;
.super Landroid/app/Activity;
.source "TotalCallTime.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v14, 0x7f03001e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/servicemodeapp/TotalCallTime;->setContentView(I)V

    .line 24
    const/4 v4, 0x0

    .line 25
    .local v4, "file":Ljava/io/File;
    const/4 v7, 0x0

    .line 26
    .local v7, "in":Ljava/io/InputStream;
    const/4 v14, 0x4

    new-array v2, v14, [B

    .line 28
    .local v2, "buffer":[B
    const-wide/16 v10, 0x0

    .line 32
    .local v10, "mTotalCallTime":J
    :try_start_0
    new-instance v5, Ljava/io/File;

    const-string v14, "/efs/total_call_time"

    invoke-direct {v5, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 33
    .end local v4    # "file":Ljava/io/File;
    .local v5, "file":Ljava/io/File;
    if-nez v5, :cond_0

    .line 34
    :try_start_1
    const-string v14, "TotalCallTime"

    const-string v15, "NullPointer"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/TotalCallTime;->finish()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_0
    move-object v4, v5

    .line 58
    .end local v5    # "file":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    :goto_1
    const v14, 0x7f090073

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/servicemodeapp/TotalCallTime;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 60
    .local v9, "mText":Landroid/widget/TextView;
    const-wide/16 v14, 0xe10

    div-long v14, v10, v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 61
    .local v6, "hour":Ljava/lang/String;
    const-wide/16 v14, 0xe10

    rem-long v14, v10, v14

    const-wide/16 v16, 0x3c

    div-long v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    .line 62
    .local v12, "min":Ljava/lang/String;
    const-wide/16 v14, 0x3c

    rem-long v14, v10, v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    .line 64
    .local v13, "sec":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " h"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " m"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " s"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-void

    .line 36
    .end local v4    # "file":Ljava/io/File;
    .end local v6    # "hour":Ljava/lang/String;
    .end local v9    # "mText":Landroid/widget/TextView;
    .end local v12    # "min":Ljava/lang/String;
    .end local v13    # "sec":Ljava/lang/String;
    .restart local v5    # "file":Ljava/io/File;
    :cond_0
    :try_start_2
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 37
    const-string v14, "TotalCallTime"

    const-string v15, "File not exists"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 50
    :catch_0
    move-exception v3

    move-object v4, v5

    .line 51
    .end local v5    # "file":Ljava/io/File;
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "file":Ljava/io/File;
    :goto_2
    const-string v14, "TotalCallTime"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[Read] "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/TotalCallTime;->finish()V

    goto/16 :goto_1

    .line 39
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "file":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    :cond_1
    :try_start_3
    new-instance v8, Ljava/io/BufferedInputStream;

    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct {v14, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v8, v14}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 40
    .end local v7    # "in":Ljava/io/InputStream;
    .local v8, "in":Ljava/io/InputStream;
    const/4 v14, 0x0

    const/4 v15, 0x4

    :try_start_4
    invoke-virtual {v8, v2, v14, v15}, Ljava/io/InputStream;->read([BII)I

    .line 42
    const/4 v14, 0x0

    aget-byte v14, v2, v14

    and-int/lit16 v14, v14, 0xff

    int-to-long v14, v14

    add-long/2addr v10, v14

    .line 43
    const/4 v14, 0x1

    aget-byte v14, v2, v14

    shl-int/lit8 v14, v14, 0x8

    const v15, 0xff00

    and-int/2addr v14, v15

    int-to-long v14, v14

    add-long/2addr v10, v14

    .line 44
    const/4 v14, 0x2

    aget-byte v14, v2, v14

    shl-int/lit8 v14, v14, 0x10

    const/high16 v15, 0xff0000

    and-int/2addr v14, v15

    int-to-long v14, v14

    add-long/2addr v10, v14

    .line 45
    const/4 v14, 0x3

    aget-byte v14, v2, v14

    shl-int/lit8 v14, v14, 0x18

    const/high16 v15, -0x1000000

    and-int/2addr v14, v15

    int-to-long v14, v14

    add-long/2addr v10, v14

    .line 47
    if-eqz v8, :cond_2

    .line 48
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    move-object v7, v8

    .end local v8    # "in":Ljava/io/InputStream;
    .restart local v7    # "in":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 53
    .end local v5    # "file":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    :catch_1
    move-exception v3

    .line 54
    .local v3, "e":Ljava/io/IOException;
    :goto_3
    const-string v14, "TotalCallTime"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[Read] "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/TotalCallTime;->finish()V

    goto/16 :goto_1

    .line 53
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "file":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    :catch_2
    move-exception v3

    move-object v4, v5

    .end local v5    # "file":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    goto :goto_3

    .end local v4    # "file":Ljava/io/File;
    .end local v7    # "in":Ljava/io/InputStream;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v8    # "in":Ljava/io/InputStream;
    :catch_3
    move-exception v3

    move-object v7, v8

    .end local v8    # "in":Ljava/io/InputStream;
    .restart local v7    # "in":Ljava/io/InputStream;
    move-object v4, v5

    .end local v5    # "file":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    goto :goto_3

    .line 50
    :catch_4
    move-exception v3

    goto :goto_2

    .end local v4    # "file":Ljava/io/File;
    .end local v7    # "in":Ljava/io/InputStream;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v8    # "in":Ljava/io/InputStream;
    :catch_5
    move-exception v3

    move-object v7, v8

    .end local v8    # "in":Ljava/io/InputStream;
    .restart local v7    # "in":Ljava/io/InputStream;
    move-object v4, v5

    .end local v5    # "file":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    goto/16 :goto_2
.end method
