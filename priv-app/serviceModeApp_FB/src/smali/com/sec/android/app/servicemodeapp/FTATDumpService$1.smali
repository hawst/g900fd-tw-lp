.class Lcom/sec/android/app/servicemodeapp/FTATDumpService$1;
.super Ljava/lang/Object;
.source "FTATDumpService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/FTATDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$1;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 57
    const-string v0, "FTATDumpService"

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$1;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$002(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 59
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 62
    const-string v0, "FTATDumpService"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$1;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$002(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 64
    return-void
.end method
