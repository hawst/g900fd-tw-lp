.class Lcom/sec/android/app/modemui/activities/PhoneUtil$1;
.super Ljava/lang/Object;
.source "PhoneUtil.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 105
    const v1, 0x7f090024

    if-ne p2, v1, :cond_1

    .line 107
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil;Z)Z

    .line 113
    const-string v1, "PhoneUtil"

    const-string v2, "onCreate"

    const-string v3, "UART_STATUS1 : MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil"

    const-string v2, "onCreate"

    const-string v3, "Exception! UART to Modem"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090025

    if-ne p2, v1, :cond_0

    .line 120
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const-string v2, "PDA"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil;Z)Z

    .line 128
    const-string v1, "PhoneUtil"

    const-string v2, "onCreate"

    const-string v3, "UART_STATUS2 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 129
    :catch_1
    move-exception v0

    .line 130
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil"

    const-string v2, "onCreate"

    const-string v3, "Exception! UART to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
