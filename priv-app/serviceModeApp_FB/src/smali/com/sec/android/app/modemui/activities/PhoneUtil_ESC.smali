.class public Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;
.super Landroid/app/Activity;
.source "PhoneUtil_ESC.java"


# instance fields
.field private final AP_PDA_SELECTION:[B

.field private final CP_MODEM_SELECTION:[B

.field private MODEL:Ljava/lang/String;

.field private mSaveResetButton:Landroid/widget/Button;

.field private mUART:B

.field private mUSB:B

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUartToAp:Z

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbToAp:Z

.field private final modem:[B

.field private final pda:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->MODEL:Ljava/lang/String;

    .line 73
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->AP_PDA_SELECTION:[B

    .line 76
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->pda:[B

    .line 79
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->modem:[B

    .line 82
    new-array v0, v3, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->CP_MODEM_SELECTION:[B

    .line 86
    iput-boolean v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUartToAp:Z

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUsbToAp:Z

    .line 89
    iput-byte v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUART:B

    .line 90
    iput-byte v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUSB:B

    return-void

    .line 73
    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 76
    nop

    :array_1
    .array-data 1
        0x50t
        0x44t
        0x41t
        0x0t
    .end array-data

    .line 79
    :array_2
    .array-data 1
        0x4dt
        0x4ft
        0x44t
        0x45t
        0x4dt
        0x0t
    .end array-data

    .line 82
    nop

    :array_3
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->changeUart(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;

    .prologue
    .line 53
    iget-byte v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUART:B

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;B)B
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;
    .param p1, "x1"    # B

    .prologue
    .line 53
    iput-byte p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUART:B

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->changeUsb(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;

    .prologue
    .line 53
    iget-byte v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUSB:B

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;B)B
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;
    .param p1, "x1"    # B

    .prologue
    .line 53
    iput-byte p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUSB:B

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->doRebootNSave(I)V

    return-void
.end method

.method private changeUart(Ljava/lang/String;)V
    .locals 3
    .param p1, "uart"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "changeUart"

    const-string v2, "UART to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "AP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "changeUart"

    const-string v2, "UART to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "CP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :cond_2
    const-string v0, "ESC"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "changeUart"

    const-string v2, "UART to ESC"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "CP2"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private changeUsb(Ljava/lang/String;)V
    .locals 3
    .param p1, "usb"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "changeUsb"

    const-string v2, "USB to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "PDA"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "changeUsb"

    const-string v2, "USB to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "MODEM"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :cond_2
    const-string v0, "ESC"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "changeUsb"

    const-string v2, "USB to ESC"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "MODEM2"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "doRebootNSave"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchsel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 219
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 220
    return-void
.end method

.method private initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V
    .locals 6
    .param p1, "uart"    # Landroid/widget/RadioGroup;
    .param p2, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    .line 230
    const-string v2, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, "currentUart":Ljava/lang/String;
    const-string v2, "/sys/class/sec/switch/usb_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "currentUsb":Ljava/lang/String;
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UART_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "USB_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v2, "CP2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 236
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : ESC"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const v2, 0x7f090026

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 246
    :cond_0
    :goto_0
    const-string v2, "MODEM2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 247
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : ESC"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const v2, 0x7f09002a

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 256
    :cond_1
    :goto_1
    return-void

    .line 238
    :cond_2
    const-string v2, "AP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 239
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const v2, 0x7f090025

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 241
    :cond_3
    const-string v2, "CP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : MODEM"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const v2, 0x7f090024

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 249
    :cond_4
    const-string v2, "PDA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 250
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const v2, 0x7f090029

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1

    .line 252
    :cond_5
    const-string v2, "MODEM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 253
    const-string v2, "PhoneUtil_ESC"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : MODEM"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const v2, 0x7f090028

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 325
    const-string v6, ""

    .line 326
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 327
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 330
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 333
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 334
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 344
    :cond_0
    if-eqz v5, :cond_1

    .line 345
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 348
    :cond_1
    if-eqz v1, :cond_2

    .line 349
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 357
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 358
    const-string v7, ""

    .line 360
    :goto_1
    return-object v7

    .line 351
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 352
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_ESC"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 355
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 336
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 337
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "PhoneUtil_ESC"

    const-string v8, "readOneLine"

    const-string v9, "FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 344
    if-eqz v4, :cond_4

    .line 345
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 348
    :cond_4
    if-eqz v0, :cond_3

    .line 349
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 351
    :catch_2
    move-exception v2

    .line 352
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_ESC"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 339
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 340
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "PhoneUtil_ESC"

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 344
    if-eqz v4, :cond_5

    .line 345
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 348
    :cond_5
    if-eqz v0, :cond_3

    .line 349
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 351
    :catch_4
    move-exception v2

    .line 352
    const-string v7, "PhoneUtil_ESC"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 343
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 344
    :goto_4
    if-eqz v4, :cond_6

    .line 345
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 348
    :cond_6
    if-eqz v0, :cond_7

    .line 349
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 354
    :cond_7
    :goto_5
    throw v7

    .line 351
    :catch_5
    move-exception v2

    .line 352
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "PhoneUtil_ESC"

    const-string v9, "readOneLine"

    const-string v10, "IOException close()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 360
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 343
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 339
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 336
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 365
    const/4 v1, 0x0

    .line 368
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 373
    const-string v3, "PhoneUtil_ESC"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    if-eqz v2, :cond_2

    .line 382
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 388
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 383
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "PhoneUtil_ESC"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 385
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 370
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 371
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "PhoneUtil_ESC"

    const-string v4, "writeFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOExceptionfilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 373
    const-string v3, "PhoneUtil_ESC"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    if-eqz v1, :cond_0

    .line 382
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 383
    :catch_2
    move-exception v0

    .line 384
    const-string v3, "PhoneUtil_ESC"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 373
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "PhoneUtil_ESC"

    const-string v5, "writeFile"

    const-string v6, "Write Success!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    if-eqz v1, :cond_1

    .line 382
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 385
    :cond_1
    :goto_3
    throw v3

    .line 383
    :catch_3
    move-exception v0

    .line 384
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "PhoneUtil_ESC"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 373
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 370
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->setContentView(I)V

    .line 99
    const-string v0, "PhoneUtil_ESC"

    const-string v1, "onCreate"

    const-string v2, "Hello oncreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 142
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 192
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mSaveResetButton:Landroid/widget/Button;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mSaveResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUartRadioGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V

    .line 206
    return-void
.end method
