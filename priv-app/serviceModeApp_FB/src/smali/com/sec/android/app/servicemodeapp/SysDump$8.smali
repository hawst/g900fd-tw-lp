.class Lcom/sec/android/app/servicemodeapp/SysDump$8;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;->startStopWiFiTcpdump(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

.field final synthetic val$infName:Ljava/lang/String;

.field final synthetic val$start:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 3020
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iput-boolean p2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->val$start:Z

    iput-object p3, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->val$infName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 3022
    const/4 v7, 0x0

    .line 3024
    .local v7, "socket":Landroid/net/LocalSocket;
    const/4 v5, 0x0

    .line 3025
    .local v5, "readSize":I
    :try_start_0
    new-instance v8, Landroid/net/LocalSocket;

    invoke-direct {v8}, Landroid/net/LocalSocket;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3026
    .end local v7    # "socket":Landroid/net/LocalSocket;
    .local v8, "socket":Landroid/net/LocalSocket;
    :try_start_1
    new-instance v9, Landroid/net/LocalSocketAddress;

    const-string v10, "/data/.tcpdump_socket"

    sget-object v11, Landroid/net/LocalSocketAddress$Namespace;->FILESYSTEM:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v9, v10, v11}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual {v8, v9}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 3028
    new-instance v1, Ljava/io/DataInputStream;

    invoke-virtual {v8}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 3029
    .local v1, "dis":Ljava/io/DataInputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-virtual {v8}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 3031
    .local v2, "dos":Ljava/io/DataOutputStream;
    iget-boolean v9, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->val$start:Z

    if-eqz v9, :cond_3

    .line 3032
    iget-object v9, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->val$infName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/io/DataOutputStream;->write([B)V

    .line 3036
    :goto_0
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 3039
    :cond_0
    const/4 v9, 0x2

    new-array v0, v9, [B

    .line 3040
    .local v0, "byteMsg":[B
    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->read([B)I

    move-result v5

    .line 3042
    if-eqz v5, :cond_0

    .line 3045
    const/4 v9, -0x1

    if-ne v5, v9, :cond_4

    .line 3046
    const-string v9, "SysDump"

    const-string v10, "din.read() error"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3073
    :goto_1
    if-eqz v8, :cond_1

    .line 3074
    :try_start_2
    const-string v9, "SysDump"

    const-string v10, "Socket Closed."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3075
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    move-object v7, v8

    .line 3081
    .end local v0    # "byteMsg":[B
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v8    # "socket":Landroid/net/LocalSocket;
    .restart local v7    # "socket":Landroid/net/LocalSocket;
    :cond_2
    :goto_2
    return-void

    .line 3034
    .end local v7    # "socket":Landroid/net/LocalSocket;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    .restart local v8    # "socket":Landroid/net/LocalSocket;
    :cond_3
    :try_start_3
    const-string v9, "stop"

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3069
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v3

    move-object v7, v8

    .line 3070
    .end local v8    # "socket":Landroid/net/LocalSocket;
    .local v3, "e":Ljava/io/IOException;
    .restart local v7    # "socket":Landroid/net/LocalSocket;
    :goto_3
    :try_start_4
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception occurred: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3073
    if-eqz v7, :cond_2

    .line 3074
    :try_start_5
    const-string v9, "SysDump"

    const-string v10, "Socket Closed."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3075
    invoke-virtual {v7}, Landroid/net/LocalSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 3077
    :catch_1
    move-exception v3

    .line 3078
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception occurred: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 3049
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "socket":Landroid/net/LocalSocket;
    .restart local v0    # "byteMsg":[B
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    .restart local v8    # "socket":Landroid/net/LocalSocket;
    :cond_4
    :try_start_6
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    .line 3050
    .local v6, "receiveMsg":Ljava/lang/String;
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Received Msg : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3052
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 3053
    .local v4, "msg":Landroid/os/Message;
    const/16 v9, 0x405

    iput v9, v4, Landroid/os/Message;->what:I

    .line 3054
    const-string v9, "OK"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 3055
    iget-boolean v9, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->val$start:Z

    if-eqz v9, :cond_6

    .line 3056
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 3057
    iget-object v9, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v9, v9, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    invoke-virtual {v9, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 3072
    .end local v0    # "byteMsg":[B
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v4    # "msg":Landroid/os/Message;
    .end local v6    # "receiveMsg":Ljava/lang/String;
    :catchall_0
    move-exception v9

    move-object v7, v8

    .line 3073
    .end local v8    # "socket":Landroid/net/LocalSocket;
    .restart local v7    # "socket":Landroid/net/LocalSocket;
    :goto_4
    if-eqz v7, :cond_5

    .line 3074
    :try_start_7
    const-string v10, "SysDump"

    const-string v11, "Socket Closed."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3075
    invoke-virtual {v7}, Landroid/net/LocalSocket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 3079
    :cond_5
    :goto_5
    throw v9

    .line 3059
    .end local v7    # "socket":Landroid/net/LocalSocket;
    .restart local v0    # "byteMsg":[B
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    .restart local v4    # "msg":Landroid/os/Message;
    .restart local v6    # "receiveMsg":Ljava/lang/String;
    .restart local v8    # "socket":Landroid/net/LocalSocket;
    :cond_6
    const/4 v9, 0x1

    :try_start_8
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 3060
    iget-object v9, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v9, v9, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    invoke-virtual {v9, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 3064
    :cond_7
    const/4 v9, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 3065
    iget-object v9, p0, Lcom/sec/android/app/servicemodeapp/SysDump$8;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v9, v9, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    invoke-virtual {v9, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 3077
    .end local v4    # "msg":Landroid/os/Message;
    .end local v6    # "receiveMsg":Ljava/lang/String;
    :catch_2
    move-exception v3

    .line 3078
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception occurred: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    .line 3080
    .end local v8    # "socket":Landroid/net/LocalSocket;
    .restart local v7    # "socket":Landroid/net/LocalSocket;
    goto/16 :goto_2

    .line 3077
    .end local v0    # "byteMsg":[B
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v3    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 3078
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "SysDump"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception occurred: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 3072
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v9

    goto :goto_4

    .line 3069
    :catch_4
    move-exception v3

    goto/16 :goto_3
.end method
