.class Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;
.super Ljava/lang/Object;
.source "PhoneUtil_SGLTE.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 183
    const v1, 0x7f090031

    if-ne p2, v1, :cond_1

    .line 188
    :try_start_0
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "auto_mode pre"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
    invoke-static {v3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x52

    const/4 v4, 0x2

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->SendData(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;III)V

    .line 190
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "auto_mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "Exception! auto_mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 195
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090032

    if-ne p2, v1, :cond_2

    .line 200
    :try_start_1
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "sglte_mode pre"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
    invoke-static {v3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x52

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->SendData(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;III)V

    .line 202
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "sglte_mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 203
    :catch_1
    move-exception v0

    .line 204
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "Exception! sglte_mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const v1, 0x7f090033

    if-ne p2, v1, :cond_0

    .line 212
    :try_start_2
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "csfb_mode pre"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mOem:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;
    invoke-static {v3}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$OemCommands;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x52

    const/4 v4, 0x1

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->SendData(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;III)V

    .line 214
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "csfb_mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 215
    :catch_2
    move-exception v0

    .line 216
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_SGLTE"

    const-string v2, "onCreate"

    const-string v3, "Exception! csfb_mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method
