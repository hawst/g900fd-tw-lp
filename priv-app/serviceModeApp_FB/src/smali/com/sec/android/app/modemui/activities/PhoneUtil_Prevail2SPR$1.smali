.class Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;
.super Landroid/os/Handler;
.source "PhoneUtil_Prevail2SPR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 100
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 102
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    invoke-virtual {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->displayError()V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 104
    :pswitch_0
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    const-string v4, "Got EVENT_GET_SIO_MODE_DONE"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_1

    .line 107
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    const-string v4, "Exception Occur!!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v2, :cond_2

    .line 110
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    const-string v4, "ar.result == NULL! - it does not need to refresh"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_2
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, [B

    move-object v1, v2

    check-cast v1, [B

    .line 115
    .local v1, "buf":[B
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "### Setting UART "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    aget-byte v2, v1, v6

    if-nez v2, :cond_3

    .line 118
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    const-string v4, "### Setting UART "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_uart:Landroid/widget/RadioButton;
    invoke-static {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RadioButton;->toggle()V

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    iput-boolean v7, v2, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->updated:Z

    goto :goto_0

    .line 121
    :cond_3
    aget-byte v2, v1, v6

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 122
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    const-string v4, "### Setting USB "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_usb:Landroid/widget/RadioButton;
    invoke-static {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RadioButton;->toggle()V

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    iput-boolean v7, v2, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->updated:Z

    goto :goto_0

    .line 130
    .end local v1    # "buf":[B
    :pswitch_1
    const-string v2, "PhoneUtil"

    const-string v3, "handleMessage"

    const-string v4, "Got EVENT_SET_SIO_MODE_DONE"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x3f2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
