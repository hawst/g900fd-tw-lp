.class Lcom/sec/android/app/servicemodeapp/SysDump$7$14;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V
    .locals 0

    .prologue
    .line 2631
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$14;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private DoCPDump()V
    .locals 8

    .prologue
    .line 2640
    const/4 v4, 0x0

    .line 2641
    .local v4, "socket":Ljava/net/Socket;
    const/4 v1, 0x0

    .line 2642
    .local v1, "bos":Ljava/io/BufferedOutputStream;
    const/4 v0, 0x0

    .line 2643
    .local v0, "Message":Ljava/lang/String;
    new-instance v0, Ljava/lang/String;

    .end local v0    # "Message":Ljava/lang/String;
    const-string v6, "trace --trigger_report"

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 2644
    .restart local v0    # "Message":Ljava/lang/String;
    const-string v6, "SysDump"

    const-string v7, "[ModemLog] Send Modem log Command."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2647
    :try_start_0
    new-instance v5, Ljava/net/Socket;

    const-string v6, "127.0.0.1"

    const/16 v7, 0x7d1

    invoke-direct {v5, v6, v7}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2648
    .end local v4    # "socket":Ljava/net/Socket;
    .local v5, "socket":Ljava/net/Socket;
    :try_start_1
    const-string v6, "SysDump"

    const-string v7, "[ModemLog] Socket Created."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2649
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2650
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .local v2, "bos":Ljava/io/BufferedOutputStream;
    :try_start_2
    const-string v6, "SysDump"

    const-string v7, "[ModemLog] Connect In-out Stream."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2651
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 2652
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    .line 2653
    const-string v6, "SysDump"

    const-string v7, "[ModemLog] Write Command complete."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2660
    if-eqz v5, :cond_0

    .line 2662
    :try_start_3
    invoke-virtual {v5}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 2668
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 2670
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_1
    move-object v1, v2

    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    move-object v4, v5

    .line 2676
    .end local v5    # "socket":Ljava/net/Socket;
    .restart local v4    # "socket":Ljava/net/Socket;
    :cond_2
    :goto_2
    return-void

    .line 2656
    :catch_0
    move-exception v3

    .line 2660
    .local v3, "ex":Ljava/io/IOException;
    :goto_3
    if-eqz v4, :cond_3

    .line 2662
    :try_start_5
    invoke-virtual {v4}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 2668
    :cond_3
    :goto_4
    if-eqz v1, :cond_2

    .line 2670
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 2671
    :catch_1
    move-exception v6

    goto :goto_2

    .line 2660
    .end local v3    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_5
    if-eqz v4, :cond_4

    .line 2662
    :try_start_7
    invoke-virtual {v4}, Ljava/net/Socket;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 2668
    :cond_4
    :goto_6
    if-eqz v1, :cond_5

    .line 2670
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 2673
    :cond_5
    :goto_7
    throw v6

    .line 2663
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "socket":Ljava/net/Socket;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "socket":Ljava/net/Socket;
    :catch_2
    move-exception v6

    goto :goto_0

    .line 2671
    :catch_3
    move-exception v6

    goto :goto_1

    .line 2663
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v5    # "socket":Ljava/net/Socket;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "ex":Ljava/io/IOException;
    .restart local v4    # "socket":Ljava/net/Socket;
    :catch_4
    move-exception v6

    goto :goto_4

    .end local v3    # "ex":Ljava/io/IOException;
    :catch_5
    move-exception v7

    goto :goto_6

    .line 2671
    :catch_6
    move-exception v7

    goto :goto_7

    .line 2660
    .end local v4    # "socket":Ljava/net/Socket;
    .restart local v5    # "socket":Ljava/net/Socket;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "socket":Ljava/net/Socket;
    .restart local v4    # "socket":Ljava/net/Socket;
    goto :goto_5

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "socket":Ljava/net/Socket;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "socket":Ljava/net/Socket;
    :catchall_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    move-object v4, v5

    .end local v5    # "socket":Ljava/net/Socket;
    .restart local v4    # "socket":Ljava/net/Socket;
    goto :goto_5

    .line 2656
    .end local v4    # "socket":Ljava/net/Socket;
    .restart local v5    # "socket":Ljava/net/Socket;
    :catch_7
    move-exception v3

    move-object v4, v5

    .end local v5    # "socket":Ljava/net/Socket;
    .restart local v4    # "socket":Ljava/net/Socket;
    goto :goto_3

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "socket":Ljava/net/Socket;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "socket":Ljava/net/Socket;
    :catch_8
    move-exception v3

    move-object v1, v2

    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    move-object v4, v5

    .end local v5    # "socket":Ljava/net/Socket;
    .restart local v4    # "socket":Ljava/net/Socket;
    goto :goto_3
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2634
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump$7$14;->DoCPDump()V

    .line 2635
    const-string v0, "SysDump"

    const-string v1, "DoCPDump()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2636
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$14;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 2638
    return-void
.end method
