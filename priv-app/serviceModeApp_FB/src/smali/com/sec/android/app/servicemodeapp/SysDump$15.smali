.class Lcom/sec/android/app/servicemodeapp/SysDump$15;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;->checkCopyToSdcard()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0

    .prologue
    .line 3576
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 3578
    new-instance v9, Ljava/io/File;

    const-string v15, "/data/log"

    invoke-direct {v9, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3579
    .local v9, "dataLogDirectory":Ljava/io/File;
    new-instance v14, Ljava/io/File;

    const-string v15, "/data/slog"

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3580
    .local v14, "silentLogDirectory":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    const-string v15, "/data/log/err"

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3581
    .local v6, "dataCPLogDirectory":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const-string v15, "/efs/root/ERR"

    invoke-direct {v7, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3582
    .local v7, "dataCPLogDirectoryEfs":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    const-string v15, "/tombstones/mdm"

    invoke-direct {v5, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3583
    .local v5, "dataCPCrashLogDirectory":Ljava/io/File;
    new-instance v8, Ljava/io/File;

    const-string v15, "/data/cp_log"

    invoke-direct {v8, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3584
    .local v8, "dataCPLogNewDirectory":Ljava/io/File;
    new-instance v12, Ljava/io/File;

    const-string v15, "/mnt/sdcard/log"

    invoke-direct {v12, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3585
    .local v12, "sdcardLogDirectory":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    const-string v15, "/mnt/sdcard/log/cp"

    invoke-direct {v11, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3586
    .local v11, "sdcardCPCrashLogDirectory":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v15, "/data/app/bt.log"

    invoke-direct {v3, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3588
    .local v3, "btlog":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v15, "/NVM"

    invoke-direct {v1, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3589
    .local v1, "NVMDirectory":Ljava/io/File;
    new-instance v13, Ljava/io/File;

    const-string v15, "/mnt/sdcard/log/NVM"

    invoke-direct {v13, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3592
    .local v13, "sdcardNVMDirectory":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_0

    .line 3593
    invoke-virtual {v12}, Ljava/io/File;->mkdir()Z

    .line 3597
    :cond_0
    new-instance v2, Ljava/io/File;

    const-string v15, "/data/system/users/service/data"

    invoke-direct {v2, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3598
    .local v2, "ResetreasonDirectory":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v2, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3610
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v9, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3611
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v14, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3612
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v5, v11}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3613
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v8, v11}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3615
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isMarvell:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5600()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 3616
    new-instance v4, Ljava/io/File;

    const-string v15, "data/com_DDR_RW.bin"

    invoke-direct {v4, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3617
    .local v4, "cpCrashDumpFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 3618
    const-string v15, "SysDump"

    const-string v16, "com_DDR_RW.bin file is exist"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3619
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v4, v13}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3623
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v1, v13}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3626
    .end local v4    # "cpCrashDumpFile":Ljava/io/File;
    :cond_1
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 3627
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v6, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3630
    :cond_2
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 3631
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v7, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 3634
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 3635
    const-string v15, "SysDump"

    const-string v16, "btlog.exists == true"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3636
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v16, "/data/app/bt.log"

    const-string v17, "/mnt/sdcard/log/bt.log"

    const-string v18, "bt.log"

    invoke-virtual/range {v15 .. v18}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 3639
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->handler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/os/Handler;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3640
    const-string v15, "SysDump"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "broadcast media mounted = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3643
    new-instance v10, Landroid/content/Intent;

    const-string v15, "android.intent.action.MEDIA_MOUNTED"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "file://"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 3645
    .local v10, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->sendBroadcast(Landroid/content/Intent;)V

    .line 3646
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$15;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v16, 0x3fa

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3649
    return-void

    .line 3621
    .end local v10    # "intent":Landroid/content/Intent;
    .restart local v4    # "cpCrashDumpFile":Ljava/io/File;
    :cond_5
    const-string v15, "SysDump"

    const-string v16, "com_DDR_RW.bin file is not exist"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
