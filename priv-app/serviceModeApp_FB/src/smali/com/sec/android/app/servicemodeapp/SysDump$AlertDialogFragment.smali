.class public Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "SysDump.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlertDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3386
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;
    .locals 3
    .param p0, "title"    # I

    .prologue
    .line 3388
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;-><init>()V

    .line 3389
    .local v0, "alertDialogFragment":Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 3390
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3391
    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 3392
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 3397
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 3398
    .local v1, "title":I
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3399
    .local v0, "alertdialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Choose DEBUG LEVEL"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3401
    const-string v2, "LOW"

    new-instance v3, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment$1;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3409
    const-string v2, "MID"

    new-instance v3, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment$2;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3417
    const-string v2, "KOREA"

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6000()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "user"

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3418
    :cond_0
    const-string v2, "HIGH"

    new-instance v3, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment$3;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3427
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method
