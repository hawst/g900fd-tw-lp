.class public Lcom/sec/android/app/servicemodeapp/CPDebugLevel;
.super Landroid/app/Activity;
.source "CPDebugLevel.java"


# instance fields
.field private mCPRamdump:Landroid/widget/Button;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mCountryCode:Ljava/lang/String;

.field private productship:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const-string v0, "ro.csc.country_code"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCountryCode:Ljava/lang/String;

    .line 26
    const-string v0, "ro.product_ship"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->productship:Ljava/lang/String;

    .line 44
    new-instance v0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;-><init>(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mClicked:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCPRamdump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->productship:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/CPDebugLevel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->doRebootNSaveforCP(Ljava/lang/String;)V

    return-void
.end method

.method private doRebootNSaveforCP(Ljava/lang/String;)V
    .locals 5
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpdebug"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "androiddebug":Ljava/lang/String;
    const-string v2, "CPDebugLevel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set cpdebug: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 70
    .local v1, "pm":Landroid/os/PowerManager;
    invoke-virtual {v1, v0}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 71
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    const-string v1, "CPDebugLevel"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v1, 0x7f030004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->setContentView(I)V

    .line 33
    const v1, 0x7f090010

    invoke-virtual {p0, v1}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCPRamdump:Landroid/widget/Button;

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCPRamdump:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    const-string v1, "ro.cp_debug_level"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "cpdumping":Ljava/lang/String;
    const-string v1, "0x55FF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCPRamdump:Landroid/widget/Button;

    const-string v2, "CP ramdump Off"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCPRamdump:Landroid/widget/Button;

    const-string v2, "CP ramdump On"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
