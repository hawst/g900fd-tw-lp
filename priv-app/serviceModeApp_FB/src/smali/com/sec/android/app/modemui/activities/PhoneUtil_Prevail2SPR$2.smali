.class Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;
.super Ljava/lang/Object;
.source "PhoneUtil_Prevail2SPR.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 8
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    const/4 v7, 0x1

    .line 185
    const-string v4, "PhoneUtil"

    const-string v5, "onCheckedChanged"

    const-string v6, "---- In OnCheckedChanged() "

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    iget-boolean v4, v4, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->updated:Z

    if-ne v4, v7, :cond_2

    .line 188
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 189
    .local v0, "bos1":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 190
    .local v1, "dos1":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 193
    .local v3, "fileSize1":I
    const/16 v4, 0xc

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 194
    const/16 v4, 0xc

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 195
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_uart:Landroid/widget/RadioButton;
    invoke-static {v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-ne v4, v7, :cond_3

    .line 199
    const-string v4, "PhoneUtil"

    const-string v5, "onCheckedChanged"

    const-string v6, " #### new setting : UART ####"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 201
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_uart:Landroid/widget/RadioButton;
    invoke-static {v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RadioButton;->toggle()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 214
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 221
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->phone:Lcom/android/internal/telephony/Phone;
    invoke-static {v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    iget-object v6, v6, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3f3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 223
    .end local v0    # "bos1":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "dos1":Ljava/io/DataOutputStream;
    .end local v3    # "fileSize1":I
    :cond_2
    :goto_2
    return-void

    .line 202
    .restart local v0    # "bos1":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "dos1":Ljava/io/DataOutputStream;
    .restart local v3    # "fileSize1":I
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_usb:Landroid/widget/RadioButton;
    invoke-static {v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    if-ne v4, v7, :cond_0

    .line 203
    const-string v4, "PhoneUtil"

    const-string v5, "onCheckedChanged"

    const-string v6, " #### new setting : USB ####"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_usb:Landroid/widget/RadioButton;
    invoke-static {v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RadioButton;->toggle()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v2

    .line 209
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    const-string v4, "PhoneUtil"

    const-string v5, "onCheckedChanged"

    const-string v6, " failed to write"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 212
    if-eqz v1, :cond_2

    .line 214
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 215
    :catch_1
    move-exception v2

    .line 216
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 215
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 216
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 212
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_4

    .line 214
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 217
    :cond_4
    :goto_3
    throw v4

    .line 215
    :catch_3
    move-exception v2

    .line 216
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method
