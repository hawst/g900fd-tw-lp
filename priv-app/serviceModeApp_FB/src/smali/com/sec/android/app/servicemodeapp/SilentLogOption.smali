.class public Lcom/sec/android/app/servicemodeapp/SilentLogOption;
.super Landroid/app/Activity;
.source "SilentLogOption.java"


# instance fields
.field private alert:Landroid/app/AlertDialog;

.field public mHandler:Landroid/os/Handler;

.field private progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SilentLogOption$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption$1;-><init>(Lcom/sec/android/app/servicemodeapp/SilentLogOption;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/servicemodeapp/SilentLogOption;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SilentLogOption;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->hideProgressDialog()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/SilentLogOption;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SilentLogOption;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->hideSilentLogOption()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/servicemodeapp/SilentLogOption;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SilentLogOption;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->showProgressDialog(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private chooseSilentLogOption()V
    .locals 7

    .prologue
    .line 107
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    .line 108
    const-string v4, "SilentLogOption"

    const-string v5, "chooseSilentLogOption() : Choose Dialog is already created"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :goto_0
    return-void

    .line 115
    :cond_0
    const/4 v4, 0x4

    new-array v1, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const-string v5, "Default"

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string v5, "Audio"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "GPS"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "VoLTE"

    aput-object v5, v1, v4

    .line 118
    .local v1, "items":[Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 119
    .local v3, "silentlogging":Ljava/lang/String;
    const-string v4, "dev.silentlog.on"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 120
    const-string v4, "SilentLogOption"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Silent Log : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 122
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v4, "Choose Silent Log"

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 123
    const/4 v2, -0x1

    .line 129
    .local v2, "pos":I
    new-instance v4, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;-><init>(Lcom/sec/android/app/servicemodeapp/SilentLogOption;)V

    invoke-virtual {v0, v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 172
    new-instance v4, Lcom/sec/android/app/servicemodeapp/SilentLogOption$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption$5;-><init>(Lcom/sec/android/app/servicemodeapp/SilentLogOption;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 178
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private hideProgressDialog()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 207
    const-string v1, "SilentLogOption"

    const-string v2, "hideProgressDialog()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    .line 210
    const/4 v1, 0x0

    .line 223
    :goto_0
    return v1

    .line 214
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    .line 223
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    throw v1
.end method

.method private hideSilentLogOption()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 227
    const-string v1, "SilentLogOption"

    const-string v2, "hideSilentLogOption()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 230
    const/4 v1, 0x0

    .line 242
    :goto_0
    return v1

    .line 234
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    .line 242
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->alert:Landroid/app/AlertDialog;

    throw v1
.end method

.method private showProgressDialog(Ljava/lang/String;)Z
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 183
    const-string v3, "SilentLogOption"

    const-string v4, "showProgressDialog()"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    const-string v2, "SilentLogOption"

    const-string v3, "isFinishing()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :goto_0
    return v1

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_1

    .line 191
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    .line 195
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v1, v2

    .line 203
    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    const-string v1, "SilentLogOption"

    const-string v3, "BadTokenException"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "SilentLogOption"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->hideProgressDialog()Z

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->hideSilentLogOption()Z

    .line 42
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 43
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->chooseSilentLogOption()V

    .line 36
    return-void
.end method
