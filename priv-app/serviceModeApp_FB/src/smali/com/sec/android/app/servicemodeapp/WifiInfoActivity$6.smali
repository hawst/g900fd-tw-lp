.class Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;
.super Landroid/os/Handler;
.source "WifiInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 269
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 271
    :pswitch_0
    const-string v0, "WiFiDebugScreen"

    const-string v1, "EVENT_UPDATE_SCREEN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # invokes: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updateWifiInfo()V
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$000(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # getter for: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPause:Z
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$700(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 279
    :pswitch_1
    const-string v0, "WiFiDebugScreen"

    const-string v1, "EVENT_WIFI_ON"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # getter for: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$800(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
