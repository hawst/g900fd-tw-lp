.class public Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
.super Landroid/app/Activity;
.source "SetPortUartUSBCTCModel.java"


# instance fields
.field private final ACTION_REQUEST_START_FTCLIENT:Ljava/lang/String;

.field private final AP_PDA_SELECTION:[B

.field private final CP_MODEM_SELECTION:[B

.field private MODEL:Ljava/lang/String;

.field private mSaveResetButton:Landroid/widget/Button;

.field private mUartRadioButton_sprd:Landroid/widget/RadioButton;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUartToAp:Z

.field private mUsbRadioButton_modem:Landroid/widget/RadioButton;

.field private mUsbRadioButton_pda:Landroid/widget/RadioButton;

.field private mUsbRadioButton_sprd:Landroid/widget/RadioButton;

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbToAp:Z

.field private final modem:[B

.field private final pda:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 77
    const-string v0, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->ACTION_REQUEST_START_FTCLIENT:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->MODEL:Ljava/lang/String;

    .line 83
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->AP_PDA_SELECTION:[B

    .line 86
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->pda:[B

    .line 89
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->modem:[B

    .line 92
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->CP_MODEM_SELECTION:[B

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartToAp:Z

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbToAp:Z

    return-void

    .line 83
    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 86
    nop

    :array_1
    .array-data 1
        0x50t
        0x44t
        0x41t
        0x0t
    .end array-data

    .line 89
    :array_2
    .array-data 1
        0x4dt
        0x4ft
        0x44t
        0x45t
        0x4dt
        0x0t
    .end array-data

    .line 92
    nop

    :array_3
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUart(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartToAp:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartToAp:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUartPath(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUsb(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbToAp:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbToAp:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->doRebootNSave(I)V

    return-void
.end method

.method private changeUart(Ljava/lang/String;)V
    .locals 3
    .param p1, "uart"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "changeUart"

    const-string v2, "UART to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "AP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "changeUart"

    const-string v2, "UART to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "AP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    :cond_2
    const-string v0, "SPRD"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "changeUart"

    const-string v2, "UART to SPRD"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "CP2"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private changeUartPath(Ljava/lang/String;)V
    .locals 6
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 303
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 304
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.factory.aporiented.athandler.AtUartswit.SetUartPath"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string v2, "PATH"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->sendBroadcast(Landroid/content/Intent;)V

    .line 307
    const-string v2, "SetPortUartUSBCTCModel"

    const-string v3, "changeUartPath"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Send Broadcast ,extra: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const/16 v2, 0x1f4

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->getCurrentUARTPath(I)Ljava/lang/String;

    move-result-object v1

    .line 309
    .local v1, "path":Ljava/lang/String;
    return-void
.end method

.method private changeUsb(Ljava/lang/String;)V
    .locals 3
    .param p1, "usb"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    const-string v0, "PDA"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "changeUsb"

    const-string v2, "USB to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "PDA"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 294
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "changeUsb"

    const-string v2, "USB to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "MODEM"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 296
    :cond_2
    const-string v0, "SPRD"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "changeUsb"

    const-string v2, "USB to SPRD"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "MODEM2"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 239
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "SetPortUartUSBCTCModel"

    const-string v3, "doRebootNSave"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchsel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 241
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method private getCurrentUARTPath(I)Ljava/lang/String;
    .locals 10
    .param p1, "millis"    # I

    .prologue
    .line 197
    const-string v6, "SetPortUartUSBCTCModel"

    const-string v7, "getCurrentUARTPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCurrentUARTStatus : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    int-to-long v6, p1

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_0
    const/4 v2, 0x0

    .line 206
    .local v2, "path":Ljava/lang/String;
    const-string v0, ""

    .line 207
    .local v0, "correct_path":Ljava/lang/String;
    const/4 v3, 0x0

    .line 208
    .local v3, "reader":Ljava/io/BufferedReader;
    const-string v0, "data/misc/radio/uart.txt"

    .line 209
    const-string v6, "SetPortUartUSBCTCModel"

    const-string v7, "getCurrentUARTPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FILE PATH : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    if-eqz v4, :cond_0

    .line 215
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 216
    .local v5, "readerTemp":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 217
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 224
    .end local v5    # "readerTemp":Ljava/lang/String;
    :cond_0
    if-eqz v4, :cond_3

    .line 226
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v3, v4

    .line 234
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_1
    return-object v2

    .line 201
    .end local v0    # "correct_path":Ljava/lang/String;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 227
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "correct_path":Ljava/lang/String;
    .restart local v2    # "path":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "SetPortUartUSBCTCModel"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Closed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 230
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 220
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 221
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    const-string v6, "SetPortUartUSBCTCModel"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Read"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 224
    if-eqz v3, :cond_1

    .line 226
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 227
    :catch_3
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "SetPortUartUSBCTCModel"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Closed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 224
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v3, :cond_2

    .line 226
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 230
    :cond_2
    :goto_4
    throw v6

    .line 227
    :catch_4
    move-exception v1

    .line 228
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "SetPortUartUSBCTCModel"

    const-string v8, "getCurrentUARTPath"

    const-string v9, "Exception! While Buffer Closed"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 224
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 220
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_5
    move-exception v1

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method private initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V
    .locals 7
    .param p1, "uart"    # Landroid/widget/RadioGroup;
    .param p2, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    .line 246
    const-string v3, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v3}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "currentUart":Ljava/lang/String;
    const-string v3, "/sys/class/sec/switch/usb_sel"

    invoke-direct {p0, v3}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "currentUsb":Ljava/lang/String;
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UART_Init : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "USB_Init : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/16 v3, 0x1f4

    invoke-direct {p0, v3}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->getCurrentUARTPath(I)Ljava/lang/String;

    move-result-object v2

    .line 251
    .local v2, "path":Ljava/lang/String;
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v3, "CP2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 254
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    const-string v5, "Initial UART_STATUS1 : SPRD"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const v3, 0x7f090026

    invoke-virtual {p1, v3}, Landroid/widget/RadioGroup;->check(I)V

    .line 264
    :cond_0
    :goto_0
    const-string v3, "MODEM2"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 265
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    const-string v5, "Initial USB_STATUS1 : SPRD"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const v3, 0x7f09002a

    invoke-virtual {p2, v3}, Landroid/widget/RadioGroup;->check(I)V

    .line 274
    :cond_1
    :goto_1
    return-void

    .line 256
    :cond_2
    const-string v3, "AP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "AP"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 257
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    const-string v5, "Initial UART_STATUS1 : PDA"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const v3, 0x7f090025

    invoke-virtual {p1, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 259
    :cond_3
    const-string v3, "AP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "CP"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 260
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    const-string v5, "Initial UART_STATUS1 : MODEM"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const v3, 0x7f090024

    invoke-virtual {p1, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 267
    :cond_4
    const-string v3, "PDA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 268
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    const-string v5, "Initial USB_STATUS1 : PDA"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const v3, 0x7f090029

    invoke-virtual {p2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1

    .line 270
    :cond_5
    const-string v3, "MODEM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 271
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "initialSetting"

    const-string v5, "Initial USB_STATUS1 : MODEM"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const v3, 0x7f090028

    invoke-virtual {p2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 374
    const-string v6, ""

    .line 375
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 376
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 379
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 382
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 383
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 393
    :cond_0
    if-eqz v5, :cond_1

    .line 394
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 397
    :cond_1
    if-eqz v1, :cond_2

    .line 398
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 406
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 407
    const-string v7, ""

    .line 409
    :goto_1
    return-object v7

    .line 400
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 401
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "SetPortUartUSBCTCModel"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 404
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 385
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 386
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "SetPortUartUSBCTCModel"

    const-string v8, "readOneLine"

    const-string v9, "FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 393
    if-eqz v4, :cond_4

    .line 394
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 397
    :cond_4
    if-eqz v0, :cond_3

    .line 398
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 400
    :catch_2
    move-exception v2

    .line 401
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "SetPortUartUSBCTCModel"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 388
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 389
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "SetPortUartUSBCTCModel"

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 393
    if-eqz v4, :cond_5

    .line 394
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 397
    :cond_5
    if-eqz v0, :cond_3

    .line 398
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 400
    :catch_4
    move-exception v2

    .line 401
    const-string v7, "SetPortUartUSBCTCModel"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 392
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 393
    :goto_4
    if-eqz v4, :cond_6

    .line 394
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 397
    :cond_6
    if-eqz v0, :cond_7

    .line 398
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 403
    :cond_7
    :goto_5
    throw v7

    .line 400
    :catch_5
    move-exception v2

    .line 401
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "SetPortUartUSBCTCModel"

    const-string v9, "readOneLine"

    const-string v10, "IOException close()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 409
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 392
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 388
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 385
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private runFtClient()V
    .locals 3

    .prologue
    .line 192
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "runFtClient"

    const-string v2, "send ACTION_REQUEST_START_FTCLIENT"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->sendBroadcast(Landroid/content/Intent;)V

    .line 194
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 414
    const/4 v1, 0x0

    .line 417
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 422
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    if-eqz v2, :cond_2

    .line 426
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 432
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 427
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 428
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 429
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 419
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 420
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "writeFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOExceptionfilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 422
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    if-eqz v1, :cond_0

    .line 426
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 427
    :catch_2
    move-exception v0

    .line 428
    const-string v3, "SetPortUartUSBCTCModel"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 422
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "SetPortUartUSBCTCModel"

    const-string v5, "writeFile"

    const-string v6, "Write Success!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    if-eqz v1, :cond_1

    .line 426
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 429
    :cond_1
    :goto_3
    throw v3

    .line 427
    :catch_3
    move-exception v0

    .line 428
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "SetPortUartUSBCTCModel"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 422
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 419
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->setContentView(I)V

    .line 105
    const-string v0, "SetPortUartUSBCTCModel"

    const-string v1, "onCreate"

    const-string v2, "Hello oncreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 107
    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartRadioButton_sprd:Landroid/widget/RadioButton;

    .line 108
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioButton_sprd:Landroid/widget/RadioButton;

    .line 109
    const v0, 0x7f090028

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioButton_modem:Landroid/widget/RadioButton;

    .line 110
    const v0, 0x7f090029

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioButton_pda:Landroid/widget/RadioButton;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioButton_sprd:Landroid/widget/RadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartRadioButton_sprd:Landroid/widget/RadioButton;

    const-string v1, "SPRD"

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioButton_modem:Landroid/widget/RadioButton;

    const-string v1, "SPRD"

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioButton_pda:Landroid/widget/RadioButton;

    const-string v1, "MSM"

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;-><init>(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 150
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$2;-><init>(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 175
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mSaveResetButton:Landroid/widget/Button;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mSaveResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$3;-><init>(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->getCurrentUARTPath(I)Ljava/lang/String;

    .line 187
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->runFtClient()V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartRadioGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V

    .line 189
    return-void
.end method
