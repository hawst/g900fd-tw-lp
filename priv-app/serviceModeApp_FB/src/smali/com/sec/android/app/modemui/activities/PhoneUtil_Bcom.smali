.class public Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;
.super Landroid/app/Activity;
.source "PhoneUtil_Bcom.java"


# instance fields
.field private builder:Landroid/app/AlertDialog$Builder;

.field private context:Landroid/content/Context;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mExit:Landroid/widget/Button;

.field private final mOkListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSerialOff:Landroid/widget/Button;

.field private mSerialOn:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 105
    new-instance v0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mClicked:Landroid/view/View$OnClickListener;

    .line 121
    new-instance v0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mOkListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method private DisplayMessageDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "USBSerialPortSetting"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 103
    return-void
.end method

.method private SetSerialPortOff()V
    .locals 5

    .prologue
    .line 146
    const-string v0, "0"

    .local v0, "sport_value":Ljava/lang/String;
    const-string v1, "0"

    .line 148
    .local v1, "usbconf_value":Ljava/lang/String;
    const-string v2, "persist.sys.usb.sport"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    const-string v2, "USBSerialPortSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set SerialPortOff : sport_value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    const-string v2, "Serial port is already OFF"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 161
    :goto_0
    return-void

    .line 154
    :cond_0
    const-string v2, "persist.sys.usb.sport"

    const-string v3, "1"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v2, "sys.usb.config"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 158
    const-string v2, "USBSerialPortSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set SerialPortOff : usbconf_value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const-string v2, "sys.usb.config"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v2, "Serial OFF"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private SetSerialPortOn()V
    .locals 5

    .prologue
    .line 128
    const-string v0, "0"

    .local v0, "sport_value":Ljava/lang/String;
    const-string v1, "0"

    .line 130
    .local v1, "usbconf_value":Ljava/lang/String;
    const-string v2, "persist.sys.usb.sport"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    const-string v2, "USBSerialPortSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set SerialPortOn : sport_value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const-string v2, "Serial port is already ON"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->DisplayMessageDialog(Ljava/lang/String;)V

    .line 143
    :goto_0
    return-void

    .line 136
    :cond_0
    const-string v2, "persist.sys.usb.sport"

    const-string v3, "2"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v2, "sys.usb.config"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    const-string v2, "USBSerialPortSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set SerialPortOn : usbconf_value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const-string v2, "sys.usb.config"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v2, "Serial ON"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->DisplayMessageDialog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mSerialOn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->SetSerialPortOn()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mSerialOff:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->SetSerialPortOff()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mExit:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->setContentView(I)V

    .line 77
    const v0, 0x7f090020

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mSerialOn:Landroid/widget/Button;

    .line 78
    const v0, 0x7f090021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mSerialOff:Landroid/widget/Button;

    .line 80
    const v0, 0x7f090022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mExit:Landroid/widget/Button;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mSerialOn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mSerialOff:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mExit:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->builder:Landroid/app/AlertDialog$Builder;

    .line 89
    iput-object p0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Bcom;->context:Landroid/content/Context;

    .line 90
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 94
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 165
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 166
    return-void
.end method
