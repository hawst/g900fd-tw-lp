.class Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;
.super Ljava/lang/Object;
.source "SilentLogOption.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SilentLogOption;->chooseSilentLogOption()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SilentLogOption;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SilentLogOption;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;->this$0:Lcom/sec/android/app/servicemodeapp/SilentLogOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "item"    # I

    .prologue
    .line 131
    const-string v2, "SilentLogOption"

    const-string v3, "Silence saving : start now! "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 133
    .local v0, "SvcIntent":Landroid/content/Intent;
    const-string v2, "com.sec.modem.settings"

    const-string v3, "com.sec.modem.settings.cplogging.SilentLogService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    packed-switch p2, :pswitch_data_0

    .line 153
    :goto_0
    const-string v2, "action"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;->this$0:Lcom/sec/android/app/servicemodeapp/SilentLogOption;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 155
    const-string v2, "SilentLogOption"

    const-string v3, "progress dialog show"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;->this$0:Lcom/sec/android/app/servicemodeapp/SilentLogOption;

    const-string v3, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SilentLogOption;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/sec/android/app/servicemodeapp/SilentLogOption;->access$200(Lcom/sec/android/app/servicemodeapp/SilentLogOption;Ljava/lang/String;)Z

    .line 158
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/servicemodeapp/SilentLogOption$4$1;-><init>(Lcom/sec/android/app/servicemodeapp/SilentLogOption$4;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 169
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 170
    return-void

    .line 138
    .end local v1    # "thread":Ljava/lang/Thread;
    :pswitch_0
    const-string v2, "modem_profile"

    const-string v3, "default"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 141
    :pswitch_1
    const-string v2, "modem_profile"

    const-string v3, "audio"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 144
    :pswitch_2
    const-string v2, "modem_profile"

    const-string v3, "gps"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 147
    :pswitch_3
    const-string v2, "modem_profile"

    const-string v3, "volte"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
