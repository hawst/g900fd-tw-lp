.class public Lcom/sec/android/app/servicemodeapp/SysDump;
.super Landroid/app/Activity;
.source "SysDump.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;,
        Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    }
.end annotation


# static fields
.field private static bDbgEnable:Z

.field private static bFdEnable:Z

.field private static bindservice_flag:I

.field private static count:I

.field private static iCPPopupUIState:I

.field private static iRamdumpMode:I

.field private static final isBasebandProp:Ljava/lang/String;

.field private static final isBroadcom:Z

.field private static final isCane:Z

.field private static final isEOS2:Z

.field private static final isMarvell:Z

.field private static final isQCom:Z

.field public static mContext:Landroid/content/Context;

.field private static mCountryCode:Ljava/lang/String;

.field private static final wifiOnly:Z


# instance fields
.field private TCPDUMP_INTERFACE:Ljava/lang/String;

.field bCancel:Z

.field bCheck10:Z

.field private bLeaveHint:Z

.field private buf:[B

.field private builder:Landroid/app/AlertDialog$Builder;

.field private cp_timout:I

.field day:Ljava/lang/String;

.field private dumpstate_file_len:J

.field private handler:Landroid/os/Handler;

.field hour:Ljava/lang/String;

.field private inFile:Ljava/lang/String;

.field private inFile_dumpState:Ljava/lang/String;

.field private inFile_panic:Ljava/lang/String;

.field private isDumpstateRunning:Z

.field private isTMOUserMenu:Z

.field private iscpdumpdone:Z

.field private logcation_info:Ljava/lang/String;

.field private mAPCPLog:Landroid/widget/Button;

.field private mAutodata:Landroid/widget/Button;

.field private mCPForceCrash:Landroid/widget/Button;

.field mCPTimoutTimer:Ljava/util/Timer;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mCopyDump:Landroid/widget/Button;

.field private mCopyKernelLog:Landroid/widget/Button;

.field private mCopyLuckyRilLogToSdcard:Landroid/widget/Button;

.field private mCopyToExternal:Landroid/widget/Button;

.field private mCopyToSdcard:Landroid/widget/Button;

.field private mCopyToSdcardListener:Landroid/view/View$OnTouchListener;

.field private mCpBoot:Landroid/widget/Button;

.field private mCreateSilentLogfile:Landroid/widget/Button;

.field private mDeleteDump:Landroid/widget/Button;

.field private mExit:Landroid/widget/Button;

.field private mGPIODump:Landroid/widget/Button;

.field private mGoTrace:Landroid/widget/Button;

.field public mHandler:Landroid/os/Handler;

.field private mKeyString:Ljava/lang/String;

.field private mLBDump:Landroid/widget/Button;

.field private mLoggingSetting:Landroid/widget/Button;

.field private mMdlog:Landroid/widget/Button;

.field private mModemLog:Landroid/widget/Button;

.field private mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

.field private final mOkListener:Landroid/content/DialogInterface$OnClickListener;

.field private mPMICIntDebug:Landroid/widget/Button;

.field private mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private mPhoneType:I

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSilentLog:Landroid/widget/Button;

.field private mStartRilLog:Landroid/widget/Button;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mTcpDump:Landroid/widget/Button;

.field private mToggleCPPopupUIState:Landroid/widget/Button;

.field private mToggleDbgState:Landroid/widget/Button;

.field private mToggleRampdumpState:Landroid/widget/Button;

.field private mToggleSecLog:Landroid/widget/Button;

.field private mToggleTranslationAssistant:Landroid/widget/Button;

.field private mWake:Landroid/widget/Button;

.field min:Ljava/lang/String;

.field month:Ljava/lang/String;

.field private outFile:Ljava/lang/String;

.field private outFile_panic:Ljava/lang/String;

.field private progressDialog:Landroid/app/ProgressDialog;

.field sec:Ljava/lang/String;

.field private settings:Landroid/content/SharedPreferences;

.field private sysdump_time:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 119
    sput v1, Lcom/sec/android/app/servicemodeapp/SysDump;->bindservice_flag:I

    .line 235
    sput v1, Lcom/sec/android/app/servicemodeapp/SysDump;->count:I

    .line 236
    sput-boolean v1, Lcom/sec/android/app/servicemodeapp/SysDump;->bDbgEnable:Z

    .line 237
    sput-boolean v2, Lcom/sec/android/app/servicemodeapp/SysDump;->bFdEnable:Z

    .line 238
    sput v1, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    .line 240
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I

    .line 268
    const-string v0, "mrvl"

    const-string v3, "ro.board.platform"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isMarvell:Z

    .line 269
    const-string v0, "ro.csc.country_code"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;

    .line 270
    const-string v0, ""

    const-string v3, "Blueberry"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isBroadcom:Z

    .line 271
    const-string v0, ""

    const-string v3, "Combination"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isQCom:Z

    .line 272
    const-string v0, "ro.build.product"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v3, "cane"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.device"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v3, "cane"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isCane:Z

    .line 273
    const-string v0, "ro.baseband"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isBasebandProp:Ljava/lang/String;

    .line 274
    const-string v0, "ro.board.platform"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v3, "EOS2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ro.board.platform"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v3, "u2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    sput-boolean v1, Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z

    .line 275
    const-string v0, "wifi"

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->wifiOnly:Z

    .line 278
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mContext:Landroid/content/Context;

    return-void

    :cond_3
    move v0, v1

    .line 272
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 116
    iput v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPhoneType:I

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;

    .line 131
    const-string v0, "Saved Location : /data\n\n"

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->logcation_info:Ljava/lang/String;

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mKeyString:Ljava/lang/String;

    .line 134
    const v0, 0x11170

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->cp_timout:I

    .line 137
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z

    .line 139
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->buf:[B

    .line 140
    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    .line 255
    const-string v0, "any"

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;

    .line 263
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bLeaveHint:Z

    .line 264
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z

    .line 265
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->iscpdumpdone:Z

    .line 266
    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPTimoutTimer:Ljava/util/Timer;

    .line 276
    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 280
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$1;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    .line 668
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 951
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$2;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 1856
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$5;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->handler:Landroid/os/Handler;

    .line 1893
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bCheck10:Z

    .line 1894
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bCancel:Z

    .line 1895
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$6;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToSdcardListener:Landroid/view/View$OnTouchListener;

    .line 1932
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$7;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    .line 3443
    new-instance v0, Lcom/sec/android/app/servicemodeapp/SysDump$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$14;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOkListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public static ExternalSDcardMounted()Z
    .locals 3

    .prologue
    .line 3432
    const/4 v0, 0x0

    .line 3434
    .local v0, "mounted":Z
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3435
    const/4 v0, 0x1

    .line 3440
    :goto_0
    return v0

    .line 3437
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private RamdumpMode(I)V
    .locals 5
    .param p1, "mode"    # I

    .prologue
    .line 1820
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1821
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1824
    .local v1, "dos":Ljava/io/DataOutputStream;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1825
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1826
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1827
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1831
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1837
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3ef

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 1853
    return-void

    .line 1828
    :catch_0
    move-exception v2

    .line 1831
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 1832
    :catch_1
    move-exception v2

    goto :goto_0

    .line 1830
    :catchall_0
    move-exception v2

    .line 1831
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1834
    :goto_1
    throw v2

    .line 1832
    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_1
.end method

.method private SendData(I)V
    .locals 6
    .param p1, "cmd"    # I

    .prologue
    const/16 v5, 0x3f1

    const/16 v4, 0x14

    const/16 v3, 0x13

    .line 903
    const/4 v0, 0x0

    .line 906
    .local v0, "data":[B
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne p1, v3, :cond_1

    .line 907
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x12

    .line 914
    .local v1, "tempCMD":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->StartSysDumpData(I)[B

    move-result-object v0

    .line 916
    if-nez v0, :cond_3

    .line 917
    const-string v2, "SysDump"

    const-string v3, " err - data is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    :cond_0
    :goto_1
    return-void

    .line 908
    .end local v1    # "tempCMD":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne p1, v4, :cond_2

    .line 909
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    .restart local v1    # "tempCMD":I
    goto :goto_0

    .line 911
    .end local v1    # "tempCMD":I
    :cond_2
    move v1, p1

    .restart local v1    # "tempCMD":I
    goto :goto_0

    .line 921
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    if-eq p1, v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    if-eq p1, v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xc

    if-eq p1, v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xd

    if-eq p1, v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x16

    if-ne p1, v2, :cond_5

    .line 924
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 925
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x3

    if-ne p1, v2, :cond_6

    .line 926
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3ed

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 927
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne p1, v4, :cond_7

    .line 928
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 930
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne p1, v3, :cond_8

    .line 931
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 932
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x12

    if-ne p1, v2, :cond_9

    .line 933
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 934
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x17

    if-ne p1, v2, :cond_a

    .line 935
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 936
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x9

    if-ne p1, v2, :cond_b

    .line 937
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 938
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x6

    if-ne p1, v2, :cond_c

    .line 939
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 940
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xb

    if-ne p1, v2, :cond_d

    .line 941
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 942
    :cond_d
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x15

    if-ne p1, v2, :cond_e

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 944
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x1a

    if-ne p1, v2, :cond_f

    .line 945
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3fd

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 946
    :cond_f
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x1b

    if-ne p1, v2, :cond_0

    .line 947
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3fe

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1
.end method

.method private SendData_Silentlog(Z)V
    .locals 3
    .param p1, "mode"    # Z

    .prologue
    .line 886
    const/4 v0, 0x0

    .line 888
    .local v0, "data":[B
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNDUOS()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->StartSilentDataCP2(Z)[B

    move-result-object v0

    .line 894
    :goto_0
    if-nez v0, :cond_1

    .line 895
    const-string v1, "SysDump"

    const-string v2, " err - data is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    :goto_1
    return-void

    .line 891
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->StartSilentData(Z)[B

    move-result-object v0

    goto :goto_0

    .line 899
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1
.end method

.method private SendData_Silentlog_CP2(Z)V
    .locals 9
    .param p1, "mode"    # Z

    .prologue
    const/4 v6, 0x1

    .line 1015
    const/4 v0, 0x4

    .line 1016
    .local v0, "MODEM_GSM":C
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1017
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1018
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v4, 0x8

    .line 1020
    .local v4, "fileSize":C
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1021
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1022
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1023
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1024
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1025
    if-ne p1, v6, :cond_0

    .line 1026
    const/4 v5, 0x6

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1030
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1036
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x3f1

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 1037
    :goto_1
    return-void

    .line 1028
    :cond_0
    const/16 v5, 0x14

    :try_start_1
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1031
    :catch_0
    move-exception v3

    .line 1032
    .local v3, "e":Ljava/io/IOException;
    const-string v5, "SysDump"

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->iscpdumpdone:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/servicemodeapp/SysDump;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->iscpdumpdone:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->infoModemLog()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->infoModemLogTMO()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/servicemodeapp/SysDump;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->buf:[B

    return-object v0
.end method

.method static synthetic access$1502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 112
    sput-boolean p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bDbgEnable:Z

    return p0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleDbgState:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1700()I
    .locals 1

    .prologue
    .line 112
    sget v0, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    return v0
.end method

.method static synthetic access$1702(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 112
    sput p0, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    return p0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000()I
    .locals 1

    .prologue
    .line 112
    sget v0, Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I

    return v0
.end method

.method static synthetic access$2002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 112
    sput p0, Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I

    return p0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/os/Messenger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/servicemodeapp/SysDump;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$2402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 112
    sput p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bindservice_flag:I

    return p0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkCopyToSdcard()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/servicemodeapp/SysDump;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->cp_timout:I

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/servicemodeapp/SysDump;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->cp_timout:I

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog(Z)V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNDUOS()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->chooseSilentLog()V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/android/app/servicemodeapp/SysDump;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCreateSilentLogfile:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->createSilentLogFile()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkForNoAuthorityAndNotEngBuild()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->showOTPAlertDialogForAuth()V

    return-void
.end method

.method static synthetic access$3900()Z
    .locals 1

    .prologue
    .line 112
    sget-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->wifiOnly:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->infoLog()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isKOR()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/servicemodeapp/SysDump;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/servicemodeapp/SysDump;->startStopWiFiTcpdump(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyLuckyRilLogToSdcard:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTimeToString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mStartRilLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mDeleteDump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyDump:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSEAndroidLogs()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTSPLogs()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->chooseDiagMdlogOption()V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mAPCPLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5500()Z
    .locals 1

    .prologue
    .line 112
    sget-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z

    return v0
.end method

.method static synthetic access$5600()Z
    .locals 1

    .prologue
    .line 112
    sget-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isMarvell:Z

    return v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isU8420()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyKernelLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_panic:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6202(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_panic:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6302(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->outFile_panic:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToSdcard:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToExternal:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mExit:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkAndFinishSysDump()V

    return-void
.end method

.method static synthetic access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNTRModel()Z

    move-result v0

    return v0
.end method

.method static synthetic access$7000()Z
    .locals 1

    .prologue
    .line 112
    sget-boolean v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isBroadcom:Z

    return v0
.end method

.method static synthetic access$7100(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->displayDialog()V

    return-void
.end method

.method static synthetic access$7200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSwitchableLogProperty()Z

    move-result v0

    return v0
.end method

.method static synthetic access$7400(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->toggleSwitchableLogProperty()V

    return-void
.end method

.method static synthetic access$7500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$7600(Lcom/sec/android/app/servicemodeapp/SysDump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTranslationAssistantProperty()Z

    move-result v0

    return v0
.end method

.method static synthetic access$7700(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->toggleTranslationAssistantProperty()V

    return-void
.end method

.method static synthetic access$7800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->settings:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog_CP2(Z)V

    return-void
.end method

.method static synthetic access$8000(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump;->doRebootNSave(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8100(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialogForDebugLevel(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 112
    sput p0, Lcom/sec/android/app/servicemodeapp/SysDump;->count:I

    return p0
.end method

.method static synthetic access$900(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->infoLogAll()V

    return-void
.end method

.method private checkAndFinishSysDump()V
    .locals 0

    .prologue
    .line 3659
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->finish()V

    .line 3660
    return-void
.end method

.method private checkCopyToSdcard()V
    .locals 2

    .prologue
    .line 3561
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->ExternalSDcardMounted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3562
    const-string v1, "External SD Card UnMounted!!\nplease check if USB cable is connected or SD card is not inserted"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 3652
    :goto_0
    return-void

    .line 3571
    :cond_0
    const-string v1, "Wait..."

    invoke-direct {p0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z

    .line 3576
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/servicemodeapp/SysDump$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$15;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3651
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private checkForNoAuthorityAndNotEngBuild()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 3088
    const-string v3, "SYSDUMPOTP"

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->settings:Landroid/content/SharedPreferences;

    .line 3089
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->settings:Landroid/content/SharedPreferences;

    const-string v4, "ril.OTPAuth"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 3090
    .local v0, "auth":Z
    const-string v3, "ro.build.type"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3091
    .local v1, "buildtype":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v3, "eng"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 3092
    const-string v2, "SysDump"

    const-string v3, "It\'s user binary"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3093
    const/4 v2, 0x1

    .line 3096
    :goto_0
    return v2

    .line 3095
    :cond_0
    const-string v3, "SysDump"

    const-string v4, "It\'s eng binary"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private chooseDiagMdlogOption()V
    .locals 2

    .prologue
    .line 3100
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3101
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3102
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3103
    return-void
.end method

.method private chooseSilentLog()V
    .locals 2

    .prologue
    .line 3343
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/servicemodeapp/SilentLogOption;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3344
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3346
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3347
    return-void
.end method

.method private connectToRilService()V
    .locals 4

    .prologue
    .line 964
    const-string v2, "SysDump"

    const-string v3, "connect To Ril service"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 967
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "init.svc.ril-daemon2"

    const-string v3, "stopped"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 969
    .local v1, "strRilDaemon2Status":Ljava/lang/String;
    const-string v2, "running"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 970
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNDUOS()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 971
    const-string v2, "com.sec.phone"

    const-string v3, "com.sec.phone.SecPhoneService2"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 972
    const-string v2, "SysDump"

    const-string v3, "com.sec.phone.SecPhoneService2"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/servicemodeapp/SysDump;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 982
    return-void

    .line 974
    :cond_0
    const-string v2, "com.sec.phone"

    const-string v3, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 975
    const-string v2, "SysDump"

    const-string v3, "com.sec.phone.SecPhoneService"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 978
    :cond_1
    const-string v2, "com.sec.phone"

    const-string v3, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private createSilentLogFile()V
    .locals 7

    .prologue
    .line 1879
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v4, "storage/sdcard0/SilentLogQComm"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1880
    .local v1, "file":Ljava/io/File;
    const v4, 0x7f070056

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1884
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    .line 1885
    .local v2, "pw":Ljava/io/PrintWriter;
    const-string v3, "1"

    .line 1886
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1887
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1891
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "pw":Ljava/io/PrintWriter;
    .end local v3    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 1888
    :catch_0
    move-exception v0

    .line 1889
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "SysDump"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exceptions : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private displayDialog()V
    .locals 3

    .prologue
    .line 3322
    const v1, 0x7f070058

    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;->newInstance(I)Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;

    move-result-object v0

    .line 3323
    .local v0, "dialog":Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump$AlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 3324
    return-void
.end method

.method private doRebootNSave(Ljava/lang/String;)V
    .locals 5
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 3366
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "debug"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3367
    .local v0, "androiddebug":Ljava/lang/String;
    const-string v2, "SysDump"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set debug: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3368
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 3369
    .local v1, "pm":Landroid/os/PowerManager;
    const-string v2, "0x4f4c"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3370
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->RamdumpMode(I)V

    .line 3376
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 3377
    return-void

    .line 3371
    :cond_1
    const-string v2, "0x494d"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3372
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->RamdumpMode(I)V

    goto :goto_0

    .line 3373
    :cond_2
    const-string v2, "0x4948"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3374
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->RamdumpMode(I)V

    goto :goto_0
.end method

.method private getDebugLevel(Ljava/lang/String;)V
    .locals 7
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 1731
    const/4 v0, -0x1

    .line 1732
    .local v0, "debugLevel":I
    const-string v2, "SysDump"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDebugLevel  : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    const-string v2, "Unknown"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1735
    sput v5, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    .line 1736
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    const-string v3, "Debug Level Disabled/LOW"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1760
    :cond_0
    :goto_0
    return-void

    .line 1741
    :cond_1
    const/4 v2, 0x2

    :try_start_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1748
    const/16 v2, 0x4f4c

    if-ne v0, v2, :cond_2

    .line 1749
    sput v5, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    .line 1750
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    const-string v3, "Debug Level Disabled/LOW"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1742
    :catch_0
    move-exception v1

    .line 1743
    .local v1, "ne":Ljava/lang/NumberFormatException;
    sput v5, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    .line 1744
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    const-string v3, "Debug Level Unknown"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1751
    .end local v1    # "ne":Ljava/lang/NumberFormatException;
    :cond_2
    const/16 v2, 0x494d

    if-ne v0, v2, :cond_3

    .line 1752
    const/4 v2, 0x1

    sput v2, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    .line 1753
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    const-string v3, "Debug Level Enabled/MID"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1754
    :cond_3
    const/16 v2, 0x4948

    if-ne v0, v2, :cond_0

    .line 1755
    sput v6, Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I

    .line 1756
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    const-string v3, "Debug Level Enabled/HIGH"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private getSEAndroidLogs()Z
    .locals 7

    .prologue
    .line 3220
    const-string v3, "/proc/avc_msg"

    .line 3221
    .local v3, "srcName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/log/avc_msg_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3222
    .local v0, "dstName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3223
    .local v1, "result":Z
    new-instance v2, Ljava/io/File;

    const-string v4, "/proc/avc_msg"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3225
    .local v2, "srcFile":Ljava/io/File;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3226
    const-string v4, "SysDump"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSEAndroidLogs : get SEAndroid logs to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3227
    const-string v4, "/proc/avc_msg"

    const-string v5, "SEAndroid"

    invoke-virtual {p0, v4, v0, v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 3232
    :goto_0
    return v1

    .line 3229
    :cond_0
    const-string v4, "SysDump"

    const-string v5, "getSEAndroidLogs : kernel node is not exist"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSwitchableLogProperty()Z
    .locals 3

    .prologue
    .line 3252
    const-string v0, "1"

    const-string v1, "persist.log.seclevel"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private getTSPLogs()Z
    .locals 7

    .prologue
    .line 3236
    const-string v3, "/proc/tsp_msg"

    .line 3237
    .local v3, "srcName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/log/tsp_msg_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3238
    .local v0, "dstName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3239
    .local v1, "result":Z
    new-instance v2, Ljava/io/File;

    const-string v4, "/proc/tsp_msg"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3241
    .local v2, "srcFile":Ljava/io/File;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3242
    const-string v4, "SysDump"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTSPLogs : get TSP logs to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3243
    const-string v4, "/proc/tsp_msg"

    const-string v5, "TSP"

    invoke-virtual {p0, v4, v0, v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 3248
    :goto_0
    return v1

    .line 3245
    :cond_0
    const-string v4, "SysDump"

    const-string v5, "getTSPLogs : kernel node is not exist"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getTimeToString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 3664
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 3665
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "00"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/DecimalFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DecimalFormatSymbols;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 3666
    .local v1, "df":Ljava/text/DecimalFormat;
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->month:Ljava/lang/String;

    .line 3667
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->day:Ljava/lang/String;

    .line 3668
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->hour:Ljava/lang/String;

    .line 3669
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->min:Ljava/lang/String;

    .line 3670
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sec:Ljava/lang/String;

    .line 3671
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->month:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->day:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->hour:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->min:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    .line 3672
    const-string v2, "SysDump"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTimeToString : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3673
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    return-object v2
.end method

.method private getTranslationAssistantProperty()Z
    .locals 3

    .prologue
    .line 3256
    const-string v0, "1"

    const-string v1, "persist.translation.assistant"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private hideProgressDialog()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3488
    const-string v1, "SysDump"

    const-string v2, "hideProgressDialog()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3490
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    .line 3491
    const/4 v1, 0x0

    .line 3504
    :goto_0
    return v1

    .line 3495
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3496
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3501
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    .line 3504
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 3498
    :catch_0
    move-exception v0

    .line 3499
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3501
    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    throw v1
.end method

.method private infoLog()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    .line 1526
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/log/dumpState_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_dumpState:Ljava/lang/String;

    .line 1527
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_dumpState:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1529
    .local v1, "oFile_dumpState":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1530
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->dumpstate_file_len:J

    .line 1531
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saved Location :\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->dumpstate_file_len:J

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Kb)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1533
    .local v0, "dialog_message":Ljava/lang/String;
    iget-wide v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->dumpstate_file_len:J

    cmp-long v2, v2, v6

    if-gez v2, :cond_0

    .line 1534
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dumpstate is still running.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1535
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Please retry to get dumpstate about 2 minutes later.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1537
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 1541
    .end local v0    # "dialog_message":Ljava/lang/String;
    :goto_0
    return-void

    .line 1539
    :cond_1
    const-string v2, "dumpState fail!"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private infoLogAll()V
    .locals 8

    .prologue
    .line 1544
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/log/dumpState_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_dumpState:Ljava/lang/String;

    .line 1545
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_dumpState:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1547
    .local v1, "oFile_dumpState":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1548
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->dumpstate_file_len:J

    .line 1549
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saved Location :\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->dumpstate_file_len:J

    const-wide/16 v6, 0x400

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Kb)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1551
    .local v0, "dialog_message":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Modem log files : \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1552
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/data/log/err/AENEAS_TRACE_###.bin\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1553
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/data/log/err/MA_TRACE_###.bin\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1554
    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 1558
    .end local v0    # "dialog_message":Ljava/lang/String;
    :goto_0
    return-void

    .line 1556
    :cond_0
    const-string v2, "dumpState fail! "

    invoke-virtual {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private infoModemLog()V
    .locals 1

    .prologue
    .line 1516
    const-string v0, "GET MODEM LOG SUCCESS! \n Please copy to SDcard with other Menu Button."

    .line 1517
    .local v0, "dialog_message":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 1518
    return-void
.end method

.method private infoModemLogTMO()V
    .locals 1

    .prologue
    .line 1521
    const-string v0, "Please copy to SDcard with \'Copy Log to sdcard\' Button"

    .line 1522
    .local v0, "dialog_message":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 1523
    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 985
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 986
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "request"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 987
    invoke-virtual {p2, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 988
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v3, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 989
    const/4 v0, 0x0

    .line 992
    .local v0, "cnt":I
    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v0, v3, :cond_0

    .line 993
    :try_start_0
    sget v3, Lcom/sec/android/app/servicemodeapp/SysDump;->bindservice_flag:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 994
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1012
    :cond_0
    :goto_1
    return-void

    .line 998
    :cond_1
    :try_start_1
    const-string v3, "SysDump"

    const-string v4, "mServiceMessenger is NULL"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    const-wide/16 v4, 0xc8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 992
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1002
    :catch_0
    move-exception v1

    .line 1003
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 1007
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v3

    goto :goto_1

    .line 1009
    :catch_2
    move-exception v1

    .line 1010
    .restart local v1    # "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 1000
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v3

    goto :goto_2
.end method

.method private isCHNDUOS()Z
    .locals 4

    .prologue
    .line 3693
    const-string v2, "ro.product.model"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 3694
    .local v1, "model":Ljava/lang/String;
    const-string v2, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3695
    .local v0, "currentUart":Ljava/lang/String;
    const-string v2, "sm-n9002"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "sm-w2014"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "CP2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3696
    const/4 v2, 0x1

    .line 3699
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isCHNTRModel()Z
    .locals 3

    .prologue
    .line 3703
    const-string v1, "ro.product.model"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 3704
    .local v0, "model":Ljava/lang/String;
    const-string v1, "sm-n9106w"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "sm-n9109w"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHN"

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "sm-n9100"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3706
    :cond_0
    const/4 v1, 0x1

    .line 3709
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isKOR()Z
    .locals 2

    .prologue
    .line 3713
    const-string v0, "KOR"

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3714
    const/4 v0, 0x1

    .line 3716
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMSM7x27()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 3527
    const-string v0, "7x27"

    .line 3528
    .local v0, "APOSolution":Ljava/lang/String;
    const-string v3, "ro.chipname"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3530
    .local v1, "solution":Ljava/lang/String;
    const-string v3, "NONE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3531
    const-string v3, "ro.product.board"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3534
    :cond_0
    const-string v3, "7x27"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v2, :cond_1

    .line 3538
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isQCTBFamily()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3542
    const/4 v5, 0x4

    new-array v0, v5, [Ljava/lang/String;

    const-string v5, "MSM8974"

    aput-object v5, v0, v4

    const-string v5, "MSM8974PRO"

    aput-object v5, v0, v3

    const/4 v5, 0x2

    const-string v6, "APQ8084"

    aput-object v6, v0, v5

    const/4 v5, 0x3

    const-string v6, "MSM8916"

    aput-object v6, v0, v5

    .line 3545
    .local v0, "APOSolutionArray":[Ljava/lang/String;
    const-string v5, "ro.chipname"

    const-string v6, "NONE"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3547
    .local v2, "solution":Ljava/lang/String;
    const-string v5, "NONE"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3548
    const-string v5, "ro.product.board"

    const-string v6, "NONE"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3551
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_2

    .line 3552
    aget-object v5, v0, v1

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v3, :cond_1

    .line 3557
    :goto_1
    return v3

    .line 3551
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v3, v4

    .line 3557
    goto :goto_1
.end method

.method private isU8420()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 3678
    const-string v0, "montblanc"

    .line 3679
    .local v0, "MONTBLANC":Ljava/lang/String;
    const-string v3, "ro.chipname"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3681
    .local v1, "solution":Ljava/lang/String;
    const-string v3, "NONE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3682
    const-string v3, "ro.product.board"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3685
    :cond_0
    const-string v3, "montblanc"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v2, :cond_1

    .line 3689
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1782
    const-string v5, ""

    .line 1783
    .local v5, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1786
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1fa0

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 1789
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 1791
    .local v2, "buf_readline":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1792
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 1802
    .end local v2    # "buf_readline":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_4

    .line 1804
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 1812
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v5, :cond_2

    .line 1813
    const-string v5, ""

    .line 1815
    .end local v5    # "result":Ljava/lang/String;
    :cond_2
    return-object v5

    .line 1805
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "result":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1806
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "SysDump"

    const-string v7, "IOException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 1808
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1795
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 1796
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v6, "SysDump"

    const-string v7, "FileNotFoundException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1797
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1802
    if-eqz v0, :cond_1

    .line 1804
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1805
    :catch_2
    move-exception v3

    .line 1806
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v6, "SysDump"

    const-string v7, "IOException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1798
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 1799
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    const-string v6, "SysDump"

    const-string v7, "IOException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1800
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1802
    if-eqz v0, :cond_1

    .line 1804
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 1805
    :catch_4
    move-exception v3

    .line 1806
    const-string v6, "SysDump"

    const-string v7, "IOException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1802
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v0, :cond_3

    .line 1804
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1808
    :cond_3
    :goto_4
    throw v6

    .line 1805
    :catch_5
    move-exception v3

    .line 1806
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v7, "SysDump"

    const-string v8, "IOException"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1802
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 1798
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_6
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1795
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_7
    move-exception v4

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private showOTPAlertDialogForAuth()V
    .locals 6

    .prologue
    .line 3181
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3182
    .local v0, "alertOTP":Landroid/app/AlertDialog$Builder;
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 3183
    .local v2, "inputOTP":Landroid/widget/EditText;
    const-string v3, "OTP Authentication"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3184
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    const/16 v3, 0x24

    invoke-static {v4, v5, v3}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 3186
    .local v1, "d":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3187
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 3188
    const-string v3, "OK "

    new-instance v4, Lcom/sec/android/app/servicemodeapp/SysDump$12;

    invoke-direct {v4, p0, v2, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$12;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;Landroid/widget/EditText;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3209
    const-string v3, "Cancel"

    new-instance v4, Lcom/sec/android/app/servicemodeapp/SysDump$13;

    invoke-direct {v4, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$13;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3215
    const-string v3, "SysDump"

    const-string v4, "Showing alertDialogOTP"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3216
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3217
    return-void
.end method

.method private showProgressDialog(Ljava/lang/String;)Z
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3464
    const-string v3, "SysDump"

    const-string v4, "showProgressDialog()"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3466
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3467
    const-string v2, "SysDump"

    const-string v3, "isFinishing()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3484
    :goto_0
    return v1

    .line 3471
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_1

    .line 3472
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    .line 3476
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3477
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 3478
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 3479
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v1, v2

    .line 3484
    goto :goto_0

    .line 3480
    :catch_0
    move-exception v0

    .line 3481
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    const-string v1, "SysDump"

    const-string v3, "BadTokenException"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private showProgressDialogForDebugLevel(Ljava/lang/String;)V
    .locals 2
    .param p1, "level"    # Ljava/lang/String;

    .prologue
    .line 3355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Setting Debug level to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "...Reboot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z

    .line 3356
    return-void
.end method

.method private startStopWiFiTcpdump(ZLjava/lang/String;)V
    .locals 2
    .param p1, "start"    # Z
    .param p2, "infName"    # Ljava/lang/String;

    .prologue
    .line 3020
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/servicemodeapp/SysDump$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/servicemodeapp/SysDump$8;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;ZLjava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3083
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 3084
    return-void
.end method

.method private toggleSwitchableLogProperty()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 3260
    const/4 v1, 0x3

    .line 3261
    .local v1, "MAX_TOGGLE_RETRY_COUNT":I
    const/16 v0, 0x64

    .line 3262
    .local v0, "DELAY_DURATION":I
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSwitchableLogProperty()Z

    move-result v2

    .line 3263
    .local v2, "curLogEnabled":Z
    if-nez v2, :cond_0

    const/4 v4, 0x1

    .line 3264
    .local v4, "newLogEnabeld":Z
    :goto_0
    const-string v7, "persist.log.seclevel"

    if-eqz v4, :cond_1

    const-string v6, "1"

    :goto_1
    invoke-static {v7, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 3265
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSwitchableLogProperty()Z

    move-result v2

    .line 3267
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-eq v2, v4, :cond_2

    const/4 v6, 0x3

    if-ge v3, v6, :cond_2

    .line 3270
    const-wide/16 v6, 0x64

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3275
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSwitchableLogProperty()Z

    move-result v2

    .line 3267
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v3    # "i":I
    .end local v4    # "newLogEnabeld":Z
    :cond_0
    move v4, v5

    .line 3263
    goto :goto_0

    .line 3264
    .restart local v4    # "newLogEnabeld":Z
    :cond_1
    const-string v6, "0"

    goto :goto_1

    .line 3278
    .restart local v3    # "i":I
    :cond_2
    if-ne v2, v4, :cond_3

    .line 3279
    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    if-eqz v2, :cond_4

    const v6, 0x7f07009a

    :goto_4
    invoke-virtual {v7, v6}, Landroid/widget/Button;->setText(I)V

    .line 3283
    :cond_3
    if-ne v2, v4, :cond_5

    const v6, 0x7f07009e

    :goto_5
    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 3287
    return-void

    .line 3279
    :cond_4
    const v6, 0x7f07009b

    goto :goto_4

    .line 3283
    :cond_5
    const v6, 0x7f07009f

    goto :goto_5

    .line 3271
    :catch_0
    move-exception v6

    goto :goto_3
.end method

.method private toggleTranslationAssistantProperty()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 3290
    const/4 v1, 0x3

    .line 3291
    .local v1, "MAX_TOGGLE_RETRY_COUNT":I
    const/16 v0, 0x64

    .line 3293
    .local v0, "DELAY_DURATION":I
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTranslationAssistantProperty()Z

    move-result v2

    .line 3294
    .local v2, "curTranslationAssistantEnabled":Z
    if-nez v2, :cond_0

    const/4 v4, 0x1

    .line 3296
    .local v4, "newTranslationAssistantEnabeld":Z
    :goto_0
    const-string v7, "persist.translation.assistant"

    if-eqz v4, :cond_1

    const-string v6, "1"

    :goto_1
    invoke-static {v7, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 3298
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTranslationAssistantProperty()Z

    move-result v2

    .line 3300
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-eq v2, v4, :cond_2

    const/4 v6, 0x3

    if-ge v3, v6, :cond_2

    .line 3303
    const-wide/16 v6, 0x64

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3307
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTranslationAssistantProperty()Z

    move-result v2

    .line 3300
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v3    # "i":I
    .end local v4    # "newTranslationAssistantEnabeld":Z
    :cond_0
    move v4, v5

    .line 3294
    goto :goto_0

    .line 3296
    .restart local v4    # "newTranslationAssistantEnabeld":Z
    :cond_1
    const-string v6, "0"

    goto :goto_1

    .line 3310
    .restart local v3    # "i":I
    :cond_2
    if-ne v2, v4, :cond_3

    .line 3311
    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;

    if-eqz v2, :cond_4

    const v6, 0x7f07008e

    :goto_4
    invoke-virtual {v7, v6}, Landroid/widget/Button;->setText(I)V

    .line 3315
    :cond_3
    if-ne v2, v4, :cond_5

    const v6, 0x7f070092

    :goto_5
    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 3319
    return-void

    .line 3311
    :cond_4
    const v6, 0x7f07008f

    goto :goto_4

    .line 3315
    :cond_5
    const v6, 0x7f070093

    goto :goto_5

    .line 3304
    :catch_0
    move-exception v6

    goto :goto_3
.end method


# virtual methods
.method DoShellCmd(Ljava/lang/String;)I
    .locals 9
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 858
    iput-boolean v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z

    .line 859
    const-string v6, "SysDump"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DoShellCmd : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    const/4 v2, 0x0

    .line 861
    .local v2, "p":Ljava/lang/Process;
    const/4 v6, 0x3

    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "/system/bin/sh"

    aput-object v7, v3, v6

    const-string v6, "-c"

    aput-object v6, v3, v4

    const/4 v6, 0x2

    aput-object p1, v3, v6

    .line 866
    .local v3, "shell_command":[Ljava/lang/String;
    :try_start_0
    const-string v6, "SysDump"

    const-string v7, "exec command"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 868
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    .line 869
    const-string v6, "SysDump"

    const-string v7, "exec done"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 881
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DoShellCmd done: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    :goto_0
    return v4

    .line 870
    :catch_0
    move-exception v1

    .line 871
    .local v1, "exception":Ljava/io/IOException;
    const-string v4, "SysDump"

    const-string v6, "DoShellCmd - IOException"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 872
    goto :goto_0

    .line 873
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 874
    .local v1, "exception":Ljava/lang/SecurityException;
    const-string v4, "SysDump"

    const-string v6, "DoShellCmd - SecurityException"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 875
    goto :goto_0

    .line 876
    .end local v1    # "exception":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 877
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v4, v5

    .line 878
    goto :goto_0
.end method

.method public ResultMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1505
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_0

    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 1507
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "Dump Result"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1508
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1509
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1510
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1511
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1513
    :cond_0
    return-void
.end method

.method public WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "out"    # Ljava/lang/String;
    .param p3, "DumpType"    # Ljava/lang/String;

    .prologue
    .line 1654
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "write to sdcard DumpType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1655
    const/4 v7, 0x1

    .line 1658
    .local v7, "result":Z
    const/4 v1, 0x0

    .line 1659
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 1661
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1662
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1664
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_2
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->buf:[B

    invoke-virtual {v2, v8}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    .local v6, "n":I
    const/4 v8, -0x1

    if-le v6, v8, :cond_2

    .line 1665
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->buf:[B

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 1668
    .end local v6    # "n":I
    :catch_0
    move-exception v3

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .line 1669
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    .local v3, "fnfe":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fnfe : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    const/4 v7, 0x0

    .line 1671
    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v9, "// Exception from"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1672
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "FileNotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1677
    if-eqz v4, :cond_0

    .line 1679
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1684
    :goto_2
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 1690
    :cond_0
    :goto_3
    if-eqz v1, :cond_1

    .line 1692
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 1699
    .end local v3    # "fnfe":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_4
    return v7

    .line 1667
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "n":I
    :cond_2
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_f
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1677
    if-eqz v5, :cond_3

    .line 1679
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 1684
    :goto_5
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    .line 1690
    :cond_3
    :goto_6
    if-eqz v2, :cond_7

    .line 1692
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .line 1695
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 1680
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    .line 1681
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 1685
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 1686
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 1693
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 1694
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .line 1695
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 1680
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v6    # "n":I
    .restart local v3    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v0

    .line 1681
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1685
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v0

    .line 1686
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1693
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v0

    .line 1694
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1673
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_7
    move-exception v0

    .line 1674
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_7
    const/4 v7, 0x0

    .line 1675
    :try_start_b
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1677
    if-eqz v4, :cond_4

    .line 1679
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    .line 1684
    :goto_8
    :try_start_d
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    .line 1690
    :cond_4
    :goto_9
    if-eqz v1, :cond_1

    .line 1692
    :try_start_e
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    goto/16 :goto_4

    .line 1693
    :catch_8
    move-exception v0

    .line 1694
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1680
    :catch_9
    move-exception v0

    .line 1681
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 1685
    :catch_a
    move-exception v0

    .line 1686
    const-string v8, "SysDump"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 1677
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    :goto_a
    if-eqz v4, :cond_5

    .line 1679
    :try_start_f
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/FileDescriptor;->sync()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    .line 1684
    :goto_b
    :try_start_10
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_c

    .line 1690
    :cond_5
    :goto_c
    if-eqz v1, :cond_6

    .line 1692
    :try_start_11
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_d

    .line 1695
    :cond_6
    :goto_d
    throw v8

    .line 1680
    :catch_b
    move-exception v0

    .line 1681
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 1685
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v0

    .line 1686
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 1693
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v0

    .line 1694
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v9, "SysDump"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 1677
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v8

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 1673
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_e
    move-exception v0

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_f
    move-exception v0

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .line 1668
    :catch_10
    move-exception v3

    goto/16 :goto_1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_11
    move-exception v3

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "n":I
    :cond_7
    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public checkMdlogProperty()Z
    .locals 2

    .prologue
    .line 3111
    const-string v1, "sys.mdlogproperty"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3113
    .local v0, "mdlogging":Ljava/lang/String;
    const-string v1, "On"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3114
    const/4 v1, 0x1

    .line 3116
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public copyDirectory(Ljava/io/File;Ljava/io/File;)V
    .locals 15
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dest"    # Ljava/io/File;

    .prologue
    .line 1596
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "copyDirectory : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1599
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 1600
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->mkdir()Z

    .line 1603
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 1605
    .local v3, "fileList":[Ljava/lang/String;
    if-eqz v3, :cond_1

    array-length v11, v3

    if-gtz v11, :cond_2

    .line 1651
    .end local v3    # "fileList":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 1609
    .restart local v3    # "fileList":[Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v11, v3

    if-ge v9, v11, :cond_1

    .line 1610
    new-instance v11, Ljava/io/File;

    aget-object v12, v3, v9

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    aget-object v13, v3, v9

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v11, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 1609
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1614
    .end local v3    # "fileList":[Ljava/lang/String;
    .end local v9    # "i":I
    :cond_3
    const/4 v4, 0x0

    .line 1615
    .local v4, "fin":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 1618
    .local v7, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1619
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .local v5, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1621
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .local v8, "fout":Ljava/io/FileOutputStream;
    const/16 v11, 0x400

    :try_start_2
    new-array v1, v11, [B

    .line 1624
    .local v1, "buffer":[B
    :goto_2
    invoke-virtual {v5, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    .local v10, "len":I
    if-lez v10, :cond_5

    .line 1625
    const/4 v11, 0x0

    invoke-virtual {v8, v1, v11, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 1628
    .end local v1    # "buffer":[B
    .end local v10    # "len":I
    :catch_0
    move-exception v6

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 1629
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .local v6, "fnfe":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_3
    sget-object v11, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v12, "// Exception from"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1630
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "FileNotFoundException : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1634
    if-eqz v4, :cond_4

    .line 1636
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1642
    :cond_4
    :goto_4
    if-eqz v7, :cond_1

    .line 1644
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 1645
    :catch_1
    move-exception v2

    .line 1646
    .local v2, "e":Ljava/lang/Exception;
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1627
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "len":I
    :cond_5
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1634
    if-eqz v5, :cond_6

    .line 1636
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .line 1642
    :cond_6
    :goto_5
    if-eqz v8, :cond_1

    .line 1644
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_0

    .line 1645
    :catch_2
    move-exception v2

    .line 1646
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1637
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v2

    .line 1638
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 1637
    .end local v1    # "buffer":[B
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .end local v10    # "len":I
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v6    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v2

    .line 1638
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1631
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v2

    .line 1632
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_6
    :try_start_9
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1634
    if-eqz v4, :cond_7

    .line 1636
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    .line 1642
    :cond_7
    :goto_7
    if-eqz v7, :cond_1

    .line 1644
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    goto/16 :goto_0

    .line 1645
    :catch_6
    move-exception v2

    .line 1646
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1637
    :catch_7
    move-exception v2

    .line 1638
    const-string v11, "SysDump"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 1634
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    :goto_8
    if-eqz v4, :cond_8

    .line 1636
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    .line 1642
    :cond_8
    :goto_9
    if-eqz v7, :cond_9

    .line 1644
    :try_start_d
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    .line 1647
    :cond_9
    :goto_a
    throw v11

    .line 1637
    :catch_8
    move-exception v2

    .line 1638
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v12, "SysDump"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 1645
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v2

    .line 1646
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v12, "SysDump"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 1634
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_8

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v11

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_8

    .line 1631
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_a
    move-exception v2

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v2

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 1628
    :catch_c
    move-exception v6

    goto/16 :goto_3

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_d
    move-exception v6

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method public deleteDirectory(Ljava/lang/String;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1561
    const-string v6, "SysDump"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteDirectory : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1562
    const/4 v2, 0x0

    .line 1563
    .local v2, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 1566
    .local v5, "list":[Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1567
    .end local v2    # "file":Ljava/io/File;
    .local v3, "file":Ljava/io/File;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    .line 1569
    array-length v6, v5

    if-eqz v6, :cond_2

    .line 1570
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v6, v5

    if-ge v4, v6, :cond_2

    .line 1571
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v5, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1573
    .local v0, "delFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1574
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1575
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v5, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->deleteDirectory(Ljava/lang/String;)V

    .line 1570
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1578
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 1586
    .end local v0    # "delFile":Ljava/io/File;
    .end local v4    # "i":I
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 1587
    .end local v3    # "file":Ljava/io/File;
    .local v1, "e":Ljava/lang/NullPointerException;
    .restart local v2    # "file":Ljava/io/File;
    :goto_2
    :try_start_2
    const-string v6, "SysDump"

    const-string v7, "NullPointerException"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1591
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :goto_3
    return-void

    .line 1583
    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :cond_2
    :try_start_3
    const-string v6, "/data/log"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1584
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_3
    move-object v2, v3

    .line 1591
    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_3

    .line 1588
    :catch_1
    move-exception v1

    .line 1589
    .local v1, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_4
    const-string v6, "SysDump"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 1591
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    goto :goto_3

    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_3

    .line 1588
    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_4

    .line 1586
    :catch_3
    move-exception v1

    goto :goto_2
.end method

.method public diagMdlog(Landroid/content/Intent;)V
    .locals 9
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 3119
    const-string v6, "SysDump"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "diagMdlog() diag_mb: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "diag_mb"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "mask_file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "mask_file"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3123
    const-string v1, ""

    .line 3124
    .local v1, "maskFlag":Ljava/lang/String;
    const v6, 0x7f0700af

    invoke-virtual {p0, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mask_file"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3125
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " -f "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "mask_file"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3127
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/system/bin/diag_mdlog -s "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "diag_mb"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3129
    .local v4, "shell_cmd_mdlog":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/system/bin/log_logcat -s "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "diag_mb"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3137
    .local v3, "shell_cmd_logcat":Ljava/lang/String;
    const-string v6, "sys.mdlogproperty"

    const-string v7, "On"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 3138
    new-instance v2, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/servicemodeapp/SysDump$9;

    invoke-direct {v6, p0, v4}, Lcom/sec/android/app/servicemodeapp/SysDump$9;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)V

    invoke-direct {v2, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3144
    .local v2, "mdlog_thread":Ljava/lang/Thread;
    new-instance v0, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/servicemodeapp/SysDump$10;

    invoke-direct {v6, p0, v3}, Lcom/sec/android/app/servicemodeapp/SysDump$10;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)V

    invoke-direct {v0, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3150
    .local v0, "logcat_thread":Ljava/lang/Thread;
    new-instance v6, Ljava/io/File;

    const-string v7, "/sdcard/log/cp/silent_logs/"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 3154
    const-string v6, "Wait..."

    invoke-direct {p0, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z

    .line 3155
    const-string v6, "SysDump"

    const-string v7, "starting diag_mdlog"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3156
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 3158
    const-string v6, "SysDump"

    const-string v7, "starting log_logcat"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3159
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 3161
    const-string v6, "SysDump"

    const-string v7, "starting klog"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3162
    const-string v6, "ctl.start"

    const-string v7, "klog"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 3164
    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    const-string v7, "mdlog klog: On"

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3165
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/servicemodeapp/SysDump$11;

    invoke-direct {v6, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$11;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3175
    .local v5, "thread":Ljava/lang/Thread;
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 3178
    return-void
.end method

.method public getSysfsFile(Ljava/lang/String;[B)I
    .locals 7
    .param p1, "sysFs"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 802
    const/4 v1, 0x0

    .line 803
    .local v1, "in":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 806
    .local v3, "readSize":I
    :try_start_0
    const-string v4, "SysDump"

    const-string v5, "getSysfsFile() called!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809
    .end local v1    # "in":Ljava/io/FileInputStream;
    .local v2, "in":Ljava/io/FileInputStream;
    :try_start_1
    const-string v4, "SysDump"

    const-string v5, "FileInputStream success!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v2, p2, v4, v5}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    .line 811
    const-string v4, "SysDump"

    const-string v5, "read success!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 817
    if-eqz v2, :cond_0

    .line 818
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 825
    :cond_0
    :goto_0
    const-string v4, "SysDump"

    const-string v5, "close success!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 827
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    :goto_1
    return v3

    .line 820
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 822
    const-string v4, "SysDump"

    const-string v5, "close error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 812
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 813
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 814
    const-string v4, "SysDump"

    const-string v5, "file reading error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 817
    if-eqz v1, :cond_1

    .line 818
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 825
    :cond_1
    :goto_3
    const-string v4, "SysDump"

    const-string v5, "close success!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 820
    :catch_2
    move-exception v0

    .line 821
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 822
    const-string v4, "SysDump"

    const-string v5, "close error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 816
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 817
    :goto_4
    if-eqz v1, :cond_2

    .line 818
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 825
    :cond_2
    :goto_5
    const-string v5, "SysDump"

    const-string v6, "close success!"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    throw v4

    .line 820
    :catch_3
    move-exception v0

    .line 821
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 822
    const-string v5, "SysDump"

    const-string v6, "close error"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 816
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .line 812
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 3328
    const-string v0, "SysDump"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult: requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3329
    const/16 v0, 0xa

    if-ne p1, v0, :cond_1

    .line 3330
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 3332
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkMdlogProperty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3333
    invoke-virtual {p0, p3}, Lcom/sec/android/app/servicemodeapp/SysDump;->diagMdlog(Landroid/content/Intent;)V

    .line 3340
    :cond_0
    :goto_0
    return-void

    .line 3338
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3ec

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 25
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1042
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 1044
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    sput-object v22, Lcom/sec/android/app/servicemodeapp/SysDump;->mContext:Landroid/content/Context;

    .line 1046
    new-instance v15, Ljava/util/Locale;

    const-string v22, "en"

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 1047
    .local v15, "locale":Ljava/util/Locale;
    invoke-static {v15}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 1048
    new-instance v9, Landroid/content/res/Configuration;

    invoke-direct {v9}, Landroid/content/res/Configuration;-><init>()V

    .line 1049
    .local v9, "config":Landroid/content/res/Configuration;
    iput-object v15, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1050
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getBaseContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getBaseContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v9, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 1053
    const-string v22, "phone"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/telephony/TelephonyManager;

    .line 1054
    .local v21, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v21, :cond_0

    .line 1055
    invoke-virtual/range {v21 .. v21}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mPhoneType:I

    .line 1058
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getIntent()Landroid/content/Intent;

    move-result-object v22

    const-string v23, "mode"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z

    .line 1059
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getIntent()Landroid/content/Intent;

    move-result-object v22

    const-string v23, "keyString"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mKeyString:Ljava/lang/String;

    .line 1060
    const-string v22, "SysDump"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "dumpMode is "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    const-string v22, "SysDump"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "sysdump onCreate mKeyString is "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mKeyString:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    const v22, 0x7f03001a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->setContentView(I)V

    .line 1063
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getWindow()Landroid/view/Window;

    move-result-object v22

    const/16 v23, 0x80

    invoke-virtual/range {v22 .. v23}, Landroid/view/Window;->addFlags(I)V

    .line 1064
    const/16 v22, 0x0

    sput v22, Lcom/sec/android/app/servicemodeapp/SysDump;->bindservice_flag:I

    .line 1065
    new-instance v22, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;Lcom/sec/android/app/servicemodeapp/SysDump$1;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    .line 1066
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->connectToRilService()V

    .line 1067
    const v22, 0x7f090058

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mDeleteDump:Landroid/widget/Button;

    .line 1068
    const v22, 0x7f090059

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyDump:Landroid/widget/Button;

    .line 1069
    const v22, 0x7f090066

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mGoTrace:Landroid/widget/Button;

    .line 1070
    const v22, 0x7f09005a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyKernelLog:Landroid/widget/Button;

    .line 1071
    const v22, 0x7f09003f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToSdcard:Landroid/widget/Button;

    .line 1072
    const v22, 0x7f09005d

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToExternal:Landroid/widget/Button;

    .line 1073
    const v22, 0x7f09006a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    .line 1074
    const v22, 0x7f090022

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mExit:Landroid/widget/Button;

    .line 1075
    const v22, 0x7f09005e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    .line 1076
    const v22, 0x7f09006e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mStartRilLog:Landroid/widget/Button;

    .line 1077
    const v22, 0x7f09006f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyLuckyRilLogToSdcard:Landroid/widget/Button;

    .line 1078
    const v22, 0x7f09005b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    .line 1079
    const v22, 0x7f090057

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mAPCPLog:Landroid/widget/Button;

    .line 1080
    const v22, 0x7f090065

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;

    .line 1081
    const v22, 0x7f090060

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    .line 1082
    const v22, 0x7f090064

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    .line 1083
    const v22, 0x7f09006b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;

    .line 1084
    const v22, 0x7f09006c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;

    .line 1085
    const v22, 0x7f09006d

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;

    .line 1086
    const v22, 0x7f09005c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    .line 1087
    const v22, 0x7f090069

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mAutodata:Landroid/widget/Button;

    .line 1088
    const v22, 0x7f09005f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;

    .line 1089
    const v22, 0x7f090068

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    .line 1090
    const v22, 0x7f090062

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    .line 1092
    const v22, 0x7f090061

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCreateSilentLogfile:Landroid/widget/Button;

    .line 1094
    const-string v22, "ro.product.model"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1095
    .local v16, "modelName":Ljava/lang/String;
    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->wifiOnly:Z

    if-nez v22, :cond_1

    const-string v22, "GT-I9505X"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1096
    :cond_1
    const-string v22, "SysDump"

    const-string v23, "No phone"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mAPCPLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1098
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mStartRilLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1104
    :cond_2
    const-string v22, "I959"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 1105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1108
    :cond_3
    sget-object v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isBasebandProp:Ljava/lang/String;

    if-eqz v22, :cond_4

    sget-object v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isBasebandProp:Ljava/lang/String;

    const-string v23, "apq"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1112
    :cond_4
    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isQCom:Z

    if-eqz v22, :cond_5

    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isCane:Z

    if-nez v22, :cond_6

    .line 1114
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1116
    :cond_6
    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isBroadcom:Z

    if-eqz v22, :cond_18

    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z

    if-nez v22, :cond_18

    .line 1117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1120
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_17

    .line 1121
    const-string v22, "persist.brcm.log"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 1122
    .local v12, "currentLogging":Ljava/lang/String;
    const-string v22, "sdcard"

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_16

    .line 1123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "MTT Logging Setting : ON"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1153
    .end local v12    # "currentLogging":Ljava/lang/String;
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTimeToString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;

    .line 1157
    new-instance v22, Ljava/lang/Thread;

    new-instance v23, Lcom/sec/android/app/servicemodeapp/SysDump$3;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$3;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    invoke-direct/range {v22 .. v23}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Thread;->start()V

    .line 1170
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 1172
    new-instance v10, Ljava/lang/Thread;

    new-instance v22, Lcom/sec/android/app/servicemodeapp/SysDump$4;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$4;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1182
    .local v10, "cpPopupState":Ljava/lang/Thread;
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1187
    .end local v10    # "cpPopupState":Ljava/lang/Thread;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mDeleteDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mAPCPLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyKernelLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToExternal:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToSdcard:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198
    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isBroadcom:Z

    if-eqz v22, :cond_8

    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_8

    .line 1199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1201
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mExit:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1205
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_9

    .line 1206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGoTrace:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGoTrace:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1213
    :cond_9
    const-string v22, "ro.debug_level"

    const-string v23, "Unknown"

    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1215
    .local v18, "ramdumpstate":Ljava/lang/String;
    const-string v22, "Unknown"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_1e

    .line 1216
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->getDebugLevel(Ljava/lang/String;)V

    .line 1222
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mStartRilLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyLuckyRilLogToSdcard:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mStartRilLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyLuckyRilLogToSdcard:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCreateSilentLogfile:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mAutodata:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1253
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isMSM7x27()Z

    move-result v22

    if-nez v22, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isU8420()Z

    move-result v22

    if-eqz v22, :cond_b

    .line 1254
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1257
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1258
    const-string v22, "dev.silentlog.on"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 1260
    .local v19, "silentlogging":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z

    move/from16 v22, v0

    if-eqz v22, :cond_20

    .line 1261
    const-string v22, "On"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_1f

    .line 1262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Modem Log : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1274
    :goto_2
    const-string v22, "persist.sys.cpboot"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 1275
    .local v11, "cpboot":Ljava/lang/String;
    const-string v22, "disable"

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_22

    .line 1277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "CP Booting : Disable"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1281
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1283
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "LOW_BATTERY_DUMP"

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v14

    .line 1284
    .local v14, "lb_dump":I
    const-string v22, "ro.debug_level"

    const-string v23, "Unknown"

    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1285
    const/4 v13, -0x1

    .line 1287
    .local v13, "debugLevel":I
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v7, v0, [B

    .line 1288
    .local v7, "WAKE":[B
    const-string v22, "/sys/module/qpnp_power_on/parameters/wake_enabled"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSysfsFile(Ljava/lang/String;[B)I

    .line 1289
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    .line 1291
    .local v8, "WAKE_STR":Ljava/lang/String;
    const-string v22, "SysDump"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "read wake: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1292
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    const-string v23, "N"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_23

    .line 1293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Reset without Wakeup : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1294
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "WAKE"

    const/16 v24, 0x1

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1300
    :goto_4
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v3, v0, [B

    .line 1301
    .local v3, "GPIODUMP":[B
    const-string v22, "/sys/module/lpm_levels/parameters/secdebug"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSysfsFile(Ljava/lang/String;[B)I

    .line 1302
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    .line 1304
    .local v4, "GPIODUMP_STR":Ljava/lang/String;
    const-string v22, "SysDump"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "read gpiodump: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1305
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    const-string v23, "1"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_24

    .line 1306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "GPIO Dump : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1307
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "GPIO_DUMP"

    const/16 v24, 0x1

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1313
    :goto_5
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v5, v0, [B

    .line 1314
    .local v5, "PMICINTDEBUG":[B
    const-string v22, "/sys/module/qpnp_int/parameters/debug_mask"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSysfsFile(Ljava/lang/String;[B)I

    .line 1315
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V

    .line 1317
    .local v6, "PMICINTDEBUG_STR":Ljava/lang/String;
    const-string v22, "SysDump"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "read pmicintdebug: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1318
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    const-string v23, "1"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_25

    .line 1319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "PMIC interrupt source print : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1320
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "PMIC_INT_DEBUG"

    const/16 v24, 0x1

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1327
    :goto_6
    const/16 v22, 0x2

    :try_start_0
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x10

    invoke-static/range {v22 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 1332
    :goto_7
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v14, v0, :cond_26

    .line 1333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Low battery dump : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1344
    :goto_8
    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->wifiOnly:Z

    if-nez v22, :cond_c

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isKOR()Z

    move-result v22

    if-eqz v22, :cond_28

    .line 1345
    :cond_c
    const-string v22, "net.tcpdumping"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 1346
    .local v20, "tcpdumping":Ljava/lang/String;
    const-string v22, "On"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_d

    .line 1347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "TCP DUMP STOP"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1358
    :cond_d
    :goto_9
    const v22, 0x7f090067

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    .line 1360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    if-eqz v22, :cond_e

    .line 1361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    move-object/from16 v23, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSwitchableLogProperty()Z

    move-result v22

    if-eqz v22, :cond_29

    const v22, 0x7f07009a

    :goto_a
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1366
    :cond_e
    const v22, 0x7f090063

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;

    .line 1368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;

    move-object/from16 v22, v0

    if-eqz v22, :cond_f

    .line 1369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;

    move-object/from16 v23, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getTranslationAssistantProperty()Z

    move-result v22

    if-eqz v22, :cond_2a

    const v22, 0x7f07008e

    :goto_b
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1376
    :cond_f
    const-string v22, "KOREA"

    sget-object v23, Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_11

    .line 1378
    const-string v22, "eng"

    sget-object v23, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 1379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1382
    :cond_10
    const-string v22, "user"

    sget-object v23, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_11

    .line 1383
    sget-object v22, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    if-eqz v22, :cond_2b

    sget-object v22, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v23, "c1"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_2b

    sget-object v22, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v23, "harrison"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_2b

    sget-object v22, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v23, "gokey"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_2b

    const-string v22, "638732"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mKeyString:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2b

    .line 1387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyKernelLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mAutodata:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1407
    :cond_11
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z

    move/from16 v22, v0

    if-eqz v22, :cond_13

    .line 1408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mDeleteDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyKernelLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    if-eqz v22, :cond_12

    .line 1417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1420
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mAPCPLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Get Log"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToSdcard:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Copy Log to sdcard"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToExternal:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Copy Log to external sdcard"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1425
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isQCTBFamily()Z

    move-result v22

    if-nez v22, :cond_14

    .line 1426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1428
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1431
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNTRModel()Z

    move-result v22

    if-eqz v22, :cond_15

    .line 1432
    new-instance v22, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    const/16 v23, 0x2

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 1434
    :cond_15
    return-void

    .line 1125
    .end local v3    # "GPIODUMP":[B
    .end local v4    # "GPIODUMP_STR":Ljava/lang/String;
    .end local v5    # "PMICINTDEBUG":[B
    .end local v6    # "PMICINTDEBUG_STR":Ljava/lang/String;
    .end local v7    # "WAKE":[B
    .end local v8    # "WAKE_STR":Ljava/lang/String;
    .end local v11    # "cpboot":Ljava/lang/String;
    .end local v13    # "debugLevel":I
    .end local v14    # "lb_dump":I
    .end local v18    # "ramdumpstate":Ljava/lang/String;
    .end local v19    # "silentlogging":Ljava/lang/String;
    .end local v20    # "tcpdumping":Ljava/lang/String;
    .restart local v12    # "currentLogging":Ljava/lang/String;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "MTT Logging Setting : OFF"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1128
    .end local v12    # "currentLogging":Ljava/lang/String;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 1130
    :cond_18
    sget-boolean v22, Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z

    if-eqz v22, :cond_1d

    .line 1131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 1134
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1c

    .line 1135
    const-string v22, "persist.sys.trace.control"

    const-string v23, "OFF"

    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 1136
    .restart local v12    # "currentLogging":Ljava/lang/String;
    const-string v22, "path=nvm"

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_19

    .line 1137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "MTA Logging Setting : SDCARD"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1138
    :cond_19
    const-string v22, "path=usb"

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_1a

    .line 1139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "MTA Logging Setting : USB"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1140
    :cond_1a
    const-string v22, "path=off"

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_1b

    .line 1141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "MTA Logging Setting : OFF"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1143
    :cond_1b
    const-string v22, "sys.trace.control"

    const-string v23, "path=off"

    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "MTA Logging Setting : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1147
    .end local v12    # "currentLogging":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 1150
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 1218
    .restart local v18    # "ramdumpstate":Ljava/lang/String;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Debug Level Disabled/LOW"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1264
    .restart local v19    # "silentlogging":Ljava/lang/String;
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Modem Log : Off"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1267
    :cond_20
    const-string v22, "On"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_21

    .line 1268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Silent Log : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1270
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Silent Log : Off"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1279
    .restart local v11    # "cpboot":Ljava/lang/String;
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "CP Booting : Enable"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1296
    .restart local v7    # "WAKE":[B
    .restart local v8    # "WAKE_STR":Ljava/lang/String;
    .restart local v13    # "debugLevel":I
    .restart local v14    # "lb_dump":I
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Reset without Wakeup : Off"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1297
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "WAKE"

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_4

    .line 1309
    .restart local v3    # "GPIODUMP":[B
    .restart local v4    # "GPIODUMP_STR":Ljava/lang/String;
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "GPIO Dump : Off"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1310
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "GPIO_DUMP"

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_5

    .line 1322
    .restart local v5    # "PMICINTDEBUG":[B
    .restart local v6    # "PMICINTDEBUG_STR":Ljava/lang/String;
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "PMIC interrupt source print : Off"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1323
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "PMIC_INT_DEBUG"

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_6

    .line 1328
    :catch_0
    move-exception v17

    .line 1329
    .local v17, "ne":Ljava/lang/NumberFormatException;
    const/16 v13, 0x4f4c

    goto/16 :goto_7

    .line 1334
    .end local v17    # "ne":Ljava/lang/NumberFormatException;
    :cond_26
    const/16 v22, 0x4f4c

    move/from16 v0, v22

    if-eq v13, v0, :cond_27

    const-string v22, "KOREA"

    sget-object v23, Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_27

    .line 1335
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "LOW_BATTERY_DUMP"

    const/16 v24, 0x1

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1337
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "LOW_BATTERY_DUMP_COUNT"

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Low battery dump : On"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 1341
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Low battery dump : Off"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 1350
    :cond_28
    const-string v22, "ril.tcpdumping"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 1351
    .restart local v20    # "tcpdumping":Ljava/lang/String;
    const-string v22, "On"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_d

    .line 1352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "TCP DUMP STOP"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 1362
    :cond_29
    const v22, 0x7f07009b

    goto/16 :goto_a

    .line 1371
    :cond_2a
    const v22, 0x7f07008f

    goto/16 :goto_b

    .line 1394
    :cond_2b
    const-string v22, "638732"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mKeyString:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_11

    .line 1395
    const v22, 0x7f03001b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->setContentView(I)V

    .line 1397
    const v22, 0x7f090022

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mExit:Landroid/widget/Button;

    .line 1398
    const v22, 0x7f090060

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    .line 1399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mExit:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1401
    const-string v22, "SysDump"

    const-string v23, "sysdump kor silent log on!!!!!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 3451
    const-string v0, "SysDump"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3452
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->unbindService(Landroid/content/ServiceConnection;)V

    .line 3453
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 3454
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/servicemodeapp/SysDump;->bindservice_flag:I

    .line 3455
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z

    .line 3456
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 3457
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 3458
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNTRModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3459
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 3461
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1466
    packed-switch p1, :pswitch_data_0

    .line 1486
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 1474
    :pswitch_0
    const-string v0, "KOREA"

    sget-object v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1475
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1476
    const/4 v0, 0x1

    goto :goto_1

    .line 1480
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkAndFinishSysDump()V

    goto :goto_0

    .line 1466
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1456
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    .line 1457
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1459
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bLeaveHint:Z

    if-eqz v0, :cond_0

    .line 1460
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->finish()V

    .line 1462
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1438
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1439
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->updateMdlogButton()V

    .line 1440
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->builder:Landroid/app/AlertDialog$Builder;

    .line 1442
    const-string v0, "ro.hardware"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "qcom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1444
    const v0, 0x7f090061

    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1446
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 1450
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 1451
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->bLeaveHint:Z

    .line 1452
    return-void
.end method

.method public setSysfsFile(Ljava/lang/String;[B)V
    .locals 6
    .param p1, "sysFs"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 831
    const/4 v1, 0x0

    .line 834
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v3, "SysDump"

    const-string v4, "setSysfsFile() called!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 837
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    const-string v3, "SysDump"

    const-string v4, "FileOutputStream success!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write([B)V

    .line 839
    const-string v3, "SysDump"

    const-string v4, "write success!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 845
    if-eqz v2, :cond_0

    .line 846
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 853
    :cond_0
    :goto_0
    const-string v3, "SysDump"

    const-string v4, "close success!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 855
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :goto_1
    return-void

    .line 848
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 849
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 850
    const-string v3, "SysDump"

    const-string v4, "close error"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 840
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    .line 841
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 842
    const-string v3, "SysDump"

    const-string v4, "file writing error"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 845
    if-eqz v1, :cond_1

    .line 846
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 853
    :cond_1
    :goto_3
    const-string v3, "SysDump"

    const-string v4, "close success!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 848
    :catch_2
    move-exception v0

    .line 849
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 850
    const-string v3, "SysDump"

    const-string v4, "close error"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 844
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 845
    :goto_4
    if-eqz v1, :cond_2

    .line 846
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 853
    :cond_2
    :goto_5
    const-string v4, "SysDump"

    const-string v5, "close success!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    throw v3

    .line 848
    :catch_3
    move-exception v0

    .line 849
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 850
    const-string v4, "SysDump"

    const-string v5, "close error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 844
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 840
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public updateMdlogButton()V
    .locals 2

    .prologue
    .line 3105
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkMdlogProperty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3106
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    const-string v1, "mdlog klog: On"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3109
    :goto_0
    return-void

    .line 3108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;

    const-string v1, "mdlog klog: Off"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
