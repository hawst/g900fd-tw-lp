.class public Lcom/sec/android/app/servicemodeapp/RTN_View;
.super Landroid/preference/PreferenceActivity;
.source "RTN_View.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private LIFETIMECALLPref:Landroid/preference/Preference;

.field private LIFETIMETIMERPref:Landroid/preference/Preference;

.field private convertosix_digit_format:Z

.field private mHandler:Landroid/os/Handler;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private root:Landroid/preference/PreferenceScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/servicemodeapp/RTN_View;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mServiceMessenger:Landroid/os/Messenger;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->convertosix_digit_format:Z

    .line 44
    new-instance v0, Lcom/sec/android/app/servicemodeapp/RTN_View$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/RTN_View$1;-><init>(Lcom/sec/android/app/servicemodeapp/RTN_View;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mHandler:Landroid/os/Handler;

    .line 99
    new-instance v0, Lcom/sec/android/app/servicemodeapp/RTN_View$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/RTN_View$2;-><init>(Lcom/sec/android/app/servicemodeapp/RTN_View;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 113
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mSvcModeMessenger:Landroid/os/Messenger;

    return-void
.end method

.method private CoverttoSixDig(I)Ljava/lang/String;
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 205
    const v3, 0xf4240

    add-int/2addr v3, p1

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 206
    .local v0, "LongValue":Ljava/lang/Long;
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 207
    .local v1, "string":Ljava/lang/String;
    const/4 v2, 0x0

    .line 208
    .local v2, "subString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 209
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 210
    :cond_0
    return-object v2
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/RTN_View;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/RTN_View;
    .param p1, "x1"    # [B

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/RTN_View;->setLifeTimerCallStatus([B)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/servicemodeapp/RTN_View;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/RTN_View;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/servicemodeapp/RTN_View;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/RTN_View;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/servicemodeapp/RTN_View;->getOemData(II)V

    return-void
.end method

.method private static byteArrayToInt([BI)I
    .locals 3
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "result":I
    add-int/lit8 v1, p1, 0x0

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    .line 217
    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 218
    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x10

    const/high16 v2, 0xff0000

    and-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 219
    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x18

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 220
    return v0
.end method

.method private connectToRilService()V
    .locals 3

    .prologue
    .line 93
    sget-object v1, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    const-string v2, "connect To Secphone service"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 95
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/RTN_View;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 97
    return-void
.end method

.method private getOemData(II)V
    .locals 7
    .param p1, "mainOemid"    # I
    .param p2, "subOemid"    # I

    .prologue
    .line 228
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 229
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 231
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x3

    .line 232
    .local v3, "fileSize":I
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 233
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 234
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 237
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 238
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    sget-object v4, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " getOemData with "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/servicemodeapp/RTN_View;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 245
    :goto_0
    return-void

    .line 239
    :catch_0
    move-exception v2

    .line 240
    .local v2, "e":Ljava/io/IOException;
    sget-object v4, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getOemData(int, int).. exception occured during operation"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 116
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 117
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 119
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 123
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    sget-object v1, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    const-string v2, "mServiceMessenger is null. Do nothing."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private setLifeTimerCallStatus([B)V
    .locals 13
    .param p1, "data"    # [B

    .prologue
    .line 178
    const/4 v8, 0x4

    new-array v1, v8, [B

    .line 179
    .local v1, "newIntByte":[B
    const/4 v8, 0x0

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v2

    .line 180
    .local v2, "totalCallCnt":I
    const/4 v8, 0x4

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v6

    .line 181
    .local v6, "totalOutgoingCallCnt":I
    const/16 v8, 0x8

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v4

    .line 182
    .local v4, "totalIncomingCallCnt":I
    const/16 v8, 0xc

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v3

    .line 183
    .local v3, "totalCallTime":I
    const/16 v8, 0x10

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v7

    .line 184
    .local v7, "totalOutgoingCallTime":I
    const/16 v8, 0x14

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v5

    .line 185
    .local v5, "totalIncomingCallTime":I
    const/16 v8, 0x18

    invoke-static {p1, v8}, Lcom/sec/android/app/servicemodeapp/RTN_View;->byteArrayToInt([BI)I

    move-result v0

    .line 187
    .local v0, "lastCall":I
    iget-boolean v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->convertosix_digit_format:Z

    if-nez v8, :cond_0

    .line 188
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    const-string v9, "Total Call Time :%dH :%dM "

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    div-int/lit16 v12, v3, 0xe10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    rem-int/lit16 v12, v3, 0xe10

    div-int/lit8 v12, v12, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    const-string v9, "Out. time :%dH :%dM In. time :%dH :%dM"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    div-int/lit16 v12, v7, 0xe10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    rem-int/lit16 v12, v7, 0xe10

    div-int/lit8 v12, v12, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    div-int/lit16 v12, v5, 0xe10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    rem-int/lit16 v12, v5, 0xe10

    div-int/lit8 v12, v12, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    const-string v9, "Total Call Count : %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    const-string v9, "Outgoing counts : %d Incoming counts : %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 202
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Total Call Time : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    div-int/lit8 v10, v3, 0x3c

    invoke-direct {p0, v10}, Lcom/sec/android/app/servicemodeapp/RTN_View;->CoverttoSixDig(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "M"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Out. time : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    div-int/lit8 v10, v7, 0x3c

    invoke-direct {p0, v10}, Lcom/sec/android/app/servicemodeapp/RTN_View;->CoverttoSixDig(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "M"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " In. time : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    div-int/lit8 v10, v5, 0x3c

    invoke-direct {p0, v10}, Lcom/sec/android/app/servicemodeapp/RTN_View;->CoverttoSixDig(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "M"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Total Call Count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-direct {p0, v2}, Lcom/sec/android/app/servicemodeapp/RTN_View;->CoverttoSixDig(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Outgoing counts : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-direct {p0, v6}, Lcom/sec/android/app/servicemodeapp/RTN_View;->CoverttoSixDig(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Incoming counts : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-direct {p0, v4}, Lcom/sec/android/app/servicemodeapp/RTN_View;->CoverttoSixDig(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x1c

    const/4 v5, 0x0

    .line 134
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 135
    sget-object v3, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/RTN_View;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "keyString"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "RTN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/RTN_View;->finish()V

    .line 175
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/RTN_View;->connectToRilService()V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/servicemodeapp/RTN_View;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->root:Landroid/preference/PreferenceScreen;

    .line 148
    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-direct {v2, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 149
    .local v2, "lifeTimer":Landroid/preference/PreferenceCategory;
    const-string v3, "Life timer / calls "

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->root:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 153
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    const-string v4, "LIFETIMETIMERPref"

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    const-string v4, "Total Call Time :"

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->root:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMETIMERPref:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 160
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    const-string v4, "LIFETIMETIMERPref"

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    const-string v4, "Total Call Count :"

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->root:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LIFETIMECALLPref:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/RTN_View;->root:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/servicemodeapp/RTN_View;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 168
    new-array v0, v6, [B

    .line 169
    .local v0, "default_buff":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v6, :cond_1

    .line 170
    aput-byte v5, v0, v1

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 172
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/servicemodeapp/RTN_View;->setLifeTimerCallStatus([B)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 87
    sget-object v0, Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const/16 v0, 0xb

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/servicemodeapp/RTN_View;->getOemData(II)V

    .line 89
    return-void
.end method
