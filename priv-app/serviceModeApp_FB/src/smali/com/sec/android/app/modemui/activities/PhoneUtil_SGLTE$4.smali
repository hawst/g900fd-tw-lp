.class Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$4;
.super Ljava/lang/Object;
.source "PhoneUtil_SGLTE.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$4;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 372
    const-string v0, "PhoneUtil_SGLTE"

    const-string v1, "onServiceConnected"

    const-string v2, "onServiceConnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$4;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$402(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 374
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 376
    const-string v0, "PhoneUtil_SGLTE"

    const-string v1, "onServiceDisconnected"

    const-string v2, "onServiceDisconnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE$4;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;->access$402(Lcom/sec/android/app/modemui/activities/PhoneUtil_SGLTE;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 378
    return-void
.end method
