.class Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;
.super Landroid/os/Handler;
.source "FTATDumpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/FTATDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v6, 0x3e8

    const/16 v5, 0x3ee

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 69
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 132
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$300(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$400(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mConnectionSuccess:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$500(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 133
    invoke-virtual {p0, v5, v6, v7}, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->sendEmptyMessageDelayed(IJ)Z

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$302(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    .line 141
    :cond_0
    :goto_1
    return-void

    .line 71
    :pswitch_1
    const-string v1, "FTATDumpService"

    const-string v2, "faildumphandler : ACQUIRE_WAKE_LOCK "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->doWakeLock(Z)V

    goto :goto_1

    .line 75
    :pswitch_2
    const-string v1, "FTATDumpService"

    const-string v2, "faildumphandler : RELEASE_WAKE_LOCK "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->doWakeLock(Z)V

    goto :goto_1

    .line 79
    :pswitch_3
    const-string v1, "FTATDumpService"

    const-string v2, "MODEMLOG_DONE Success, isEOS2=true"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get modem log done - success"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$102(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    goto :goto_0

    .line 85
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 87
    .local v0, "error":I
    if-nez v0, :cond_1

    .line 88
    const-string v1, "FTATDumpService"

    const-string v2, "MODEMLOG_DONE Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get modem log done - success"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 97
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$102(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get modem log done - fail"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 94
    const-string v1, "FTATDumpService"

    const-string v2, "MODEMLOG_DONE fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 100
    .end local v0    # "error":I
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPSTELogDoneFlag:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$200(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-ne v1, v4, :cond_2

    .line 101
    const-string v1, "FTATDumpService"

    const-string v2, "STE_MODEMLOG_DONE Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get modem log done - success"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 109
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$102(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    goto/16 :goto_0

    .line 105
    :cond_2
    const-string v1, "FTATDumpService"

    const-string v2, "STE_MODEMLOG_DONE fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get modem log done - fail"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 112
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get AP log done - success"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 114
    const-string v1, "FTATDumpService"

    const-string v2, "AP LOG Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$302(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    goto/16 :goto_0

    .line 118
    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get AP log done - fail"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 120
    const-string v1, "FTATDumpService"

    const-string v2, "AP LOG fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 123
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v2, "Get Copy log done - success"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 125
    const-string v1, "FTATDumpService"

    const-string v2, "QUERY_COPY_LOG_DONE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$402(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    goto/16 :goto_0

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$300(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$100(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$400(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {p0, v5, v6, v7}, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->sendEmptyMessageDelayed(IJ)Z

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$302(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$102(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$402(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    goto/16 :goto_1

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x3ed
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch
.end method
