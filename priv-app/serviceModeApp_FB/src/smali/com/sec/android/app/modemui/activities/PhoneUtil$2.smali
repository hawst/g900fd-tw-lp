.class Lcom/sec/android/app/modemui/activities/PhoneUtil$2;
.super Ljava/lang/Object;
.source "PhoneUtil.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 141
    const v1, 0x7f090028

    if-ne p2, v1, :cond_1

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil;Z)Z

    .line 151
    const-string v1, "PhoneUtil"

    const-string v2, "onCheckedChanged"

    const-string v3, "USB_STATUS1 : MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! USB to MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 156
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090029

    if-ne p2, v1, :cond_0

    .line 158
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const-string v2, "PDA"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil;Z)Z

    .line 166
    const-string v1, "PhoneUtil"

    const-string v2, "onCheckedChanged"

    const-string v3, "USB_STATUS1 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 167
    :catch_1
    move-exception v0

    .line 168
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! USB to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
