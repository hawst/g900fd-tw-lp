.class Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;
.super Ljava/lang/Object;
.source "DiagMdlogOption.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->chooseDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    add-int/lit8 v1, p2, 0x1

    mul-int/lit8 v1, v1, 0xa

    # setter for: Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->access$002(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;I)I

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    # getter for: Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->alert:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->access$100(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "diag_mdlog size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$2;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    # getter for: Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I
    invoke-static {v2}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->access$000(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MB"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 137
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 141
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 145
    return-void
.end method
