.class Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$2;
.super Ljava/lang/Object;
.source "PhoneUtil_TD.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 128
    const v1, 0x7f090024

    if-ne p2, v1, :cond_1

    .line 130
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    const-string v2, "+CPUART"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->changeUartPath(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;Ljava/lang/String;)V

    .line 131
    const-string v1, "PhoneUtil_TD"

    const-string v2, "onCreate"

    const-string v3, "SWITCH_TO_CP_UART"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_TD"

    const-string v2, "onCreate"

    const-string v3, "Exception! UART to Modem"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 136
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090025

    if-ne p2, v1, :cond_0

    .line 138
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    const-string v2, "+APUART"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->changeUartPath(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;Ljava/lang/String;)V

    .line 139
    const-string v1, "PhoneUtil_TD"

    const-string v2, "onCreate"

    const-string v3, "SWITCH_TO_AP_UART"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 140
    :catch_1
    move-exception v0

    .line 141
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_TD"

    const-string v2, "onCreate"

    const-string v3, "Exception! UART to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
