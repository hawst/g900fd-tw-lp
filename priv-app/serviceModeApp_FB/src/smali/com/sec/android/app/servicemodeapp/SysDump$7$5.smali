.class Lcom/sec/android/app/servicemodeapp/SysDump$7$5;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V
    .locals 0

    .prologue
    .line 2265
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$5;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2267
    const-string v2, "SysDump"

    const-string v3, "Start TcpDumpLoggingService"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2269
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 2270
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.sec.tcpdumpservice"

    const-string v4, "com.sec.tcpdumpservice.TcpDumpService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2271
    .local v1, "tcpDumpSvcIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$5;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v2, v2, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2275
    .end local v1    # "tcpDumpSvcIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 2272
    :catch_0
    move-exception v0

    .line 2273
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "SysDump"

    const-string v3, "tcpDumpThread - exception occurred"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
