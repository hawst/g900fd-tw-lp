.class Lcom/sec/android/app/servicemodeapp/SysDump$2;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0

    .prologue
    .line 951
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$2;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 953
    const-string v0, "SysDump"

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$2;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2302(Lcom/sec/android/app/servicemodeapp/SysDump;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 955
    const/4 v0, 0x1

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->bindservice_flag:I
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2402(I)I

    .line 956
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 958
    const-string v0, "SysDump"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$2;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2302(Lcom/sec/android/app/servicemodeapp/SysDump;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 960
    return-void
.end method
