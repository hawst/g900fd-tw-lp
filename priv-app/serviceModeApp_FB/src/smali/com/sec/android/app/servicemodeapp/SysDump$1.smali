.class Lcom/sec/android/app/servicemodeapp/SysDump$1;
.super Landroid/os/Handler;
.source "SysDump.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 282
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 665
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 284
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 285
    const-string v5, "SysDump"

    const-string v6, "Sys dump Success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 289
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iscpdumpdone:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$102(Lcom/sec/android/app/servicemodeapp/SysDump;Z)Z

    .line 290
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 291
    .local v3, "title":Ljava/lang/String;
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get QUERY_CP_DUMP_START Title = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    if-nez v3, :cond_1

    .line 294
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    goto :goto_0

    .line 296
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static {v5, v3}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    goto :goto_0

    .line 302
    .end local v3    # "title":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 303
    .local v1, "error":I
    if-nez v1, :cond_2

    .line 304
    const-string v5, "SysDump"

    const-string v6, "KLOG_DONE success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 306
    :cond_2
    const-string v5, "SysDump"

    const-string v6, "KLOG_DONE failed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 313
    .end local v1    # "error":I
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 314
    .restart local v1    # "error":I
    if-nez v1, :cond_3

    .line 315
    const-string v5, "SysDump"

    const-string v6, "KLOG_KILL_DONE success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 317
    :cond_3
    const-string v5, "SysDump"

    const-string v6, "KLOG_KILL_DONE failed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 324
    .end local v1    # "error":I
    :pswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 325
    .restart local v1    # "error":I
    if-nez v1, :cond_4

    .line 326
    const-string v5, "SysDump"

    const-string v6, "MDLOG_DONE success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 328
    :cond_4
    const-string v5, "SysDump"

    const-string v6, "MDLOG_DONE failed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 335
    .end local v1    # "error":I
    :pswitch_6
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 336
    .restart local v1    # "error":I
    if-nez v1, :cond_5

    .line 337
    const-string v5, "SysDump"

    const-string v6, "MDLOG_KILL_DONE success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 339
    :cond_5
    const-string v5, "SysDump"

    const-string v6, "MDLOG_KILL_DONE failed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 346
    .end local v1    # "error":I
    :pswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 347
    .restart local v1    # "error":I
    if-nez v1, :cond_6

    .line 348
    const-string v5, "SysDump"

    const-string v6, "LOGCAT_DONE success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 350
    :cond_6
    const-string v5, "SysDump"

    const-string v6, "LOGCAT_DONE failed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 357
    .end local v1    # "error":I
    :pswitch_8
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 358
    .restart local v1    # "error":I
    if-nez v1, :cond_7

    .line 359
    const-string v5, "SysDump"

    const-string v6, "LOGCAT_KILL_DONE success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 361
    :cond_7
    const-string v5, "SysDump"

    const-string v6, "LOGCAT_KILL_DONE failed"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 367
    .end local v1    # "error":I
    :pswitch_9
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$300(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 368
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 370
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "modem log response time expired"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 373
    :pswitch_a
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 375
    .restart local v1    # "error":I
    if-nez v1, :cond_9

    .line 376
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 377
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->infoLog()V
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$400(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 378
    const-string v5, "SysDump"

    const-string v6, "DUMPSTATE_DONE Success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$302(Lcom/sec/android/app/servicemodeapp/SysDump;Z)Z

    .line 386
    const-string v5, "dev.silentlog.on"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 387
    .local v2, "silentlogging":Ljava/lang/String;
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Silent Log : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$500(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 390
    const-string v5, "On"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 391
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Modem Log : On"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 380
    .end local v2    # "silentlogging":Ljava/lang/String;
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 381
    const-string v5, "SysDump"

    const-string v6, "DUMPSTATE_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 393
    .restart local v2    # "silentlogging":Ljava/lang/String;
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Modem Log : Off"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 396
    :cond_b
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Silent Log : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    const-string v5, "On"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 399
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Silent Log : On"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 401
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Silent Log : Off"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 408
    .end local v1    # "error":I
    .end local v2    # "silentlogging":Ljava/lang/String;
    :pswitch_b
    const-string v5, "dev.silentlog.on"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 410
    .restart local v2    # "silentlogging":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$500(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 411
    const-string v5, "On"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 412
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Modem Log : On"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 428
    :cond_d
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    goto/16 :goto_0

    .line 414
    :cond_e
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Modem Log : Off"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 417
    :cond_f
    const-string v5, "On"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 418
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Silent Log : On"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 420
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Silent Log : Off"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 421
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNTRModel()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$700(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 422
    const-string v5, "SysDump"

    const-string v6, "Send Silent Log CP2"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog_CP2(Z)V
    invoke-static {v5, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V

    goto :goto_2

    .line 432
    .end local v2    # "silentlogging":Ljava/lang/String;
    :pswitch_c
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 434
    .restart local v1    # "error":I
    if-nez v1, :cond_11

    .line 435
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 436
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->infoLogAll()V
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$900(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 437
    const-string v5, "SysDump"

    const-string v6, "DUMPSTATE_DONE Success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 439
    :cond_11
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 440
    const-string v5, "SysDump"

    const-string v6, "DUMPSTATE_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 446
    .end local v1    # "error":I
    :pswitch_d
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 448
    .restart local v1    # "error":I
    if-nez v1, :cond_12

    .line 449
    const-string v5, "SysDump"

    const-string v6, "start modem dump"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0x13

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    goto/16 :goto_0

    .line 452
    :cond_12
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 453
    const-string v5, "SysDump"

    const-string v6, "DUMPSTATE_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 459
    .end local v1    # "error":I
    :pswitch_e
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 460
    .restart local v1    # "error":I
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iscpdumpdone:Z
    invoke-static {v5, v9}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$102(Lcom/sec/android/app/servicemodeapp/SysDump;Z)Z

    .line 461
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPTimoutTimer:Ljava/util/Timer;

    if-eqz v5, :cond_13

    .line 462
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPTimoutTimer:Ljava/util/Timer;

    invoke-virtual {v5}, Ljava/util/Timer;->cancel()V

    .line 465
    :cond_13
    if-nez v1, :cond_16

    .line 466
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$300(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 467
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 469
    :cond_14
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isTMOUserMenu:Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$500(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 470
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->infoModemLog()V
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1200(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 475
    :goto_3
    const-string v5, "SysDump"

    const-string v6, "MODEMLOG_DONE Success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 472
    :cond_15
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->infoModemLogTMO()V
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1300(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    goto :goto_3

    .line 477
    :cond_16
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isDumpstateRunning:Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$300(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 478
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 480
    :cond_17
    const-string v5, "SysDump"

    const-string v6, "MODEMLOG_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 486
    .end local v1    # "error":I
    :pswitch_f
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "QUERY_DBG_STATE_DONE : buf[3] : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->buf:[B
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1400(Lcom/sec/android/app/servicemodeapp/SysDump;)[B

    move-result-object v7

    aget-byte v7, v7, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 489
    .restart local v1    # "error":I
    if-nez v1, :cond_1b

    .line 490
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "response"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    check-cast v0, [B

    .line 492
    .local v0, "buf":[B
    if-eqz v0, :cond_1a

    array-length v5, v0

    if-le v5, v11, :cond_1a

    .line 493
    aget-byte v5, v0, v8

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x7

    if-ne v5, v6, :cond_19

    aget-byte v5, v0, v9

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x6

    if-ne v5, v6, :cond_19

    aget-byte v5, v0, v12

    const/4 v6, 0x5

    if-ne v5, v6, :cond_19

    .line 496
    aget-byte v5, v0, v11

    if-nez v5, :cond_18

    .line 497
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->bDbgEnable:Z
    invoke-static {v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1502(Z)Z

    .line 498
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleDbgState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Enable debug log"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 500
    :cond_18
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->bDbgEnable:Z
    invoke-static {v9}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1502(Z)Z

    .line 501
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleDbgState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Disable debug log"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 504
    :cond_19
    const-string v5, "SysDump"

    const-string v6, "QUERY_DBG_STATE_DONE error#1"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 507
    :cond_1a
    const-string v5, "SysDump"

    const-string v6, "null buf"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 510
    .end local v0    # "buf":[B
    :cond_1b
    const-string v5, "SysDump"

    const-string v6, "QUERY_DBG_STATE_DONE error#2"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 516
    .end local v1    # "error":I
    :pswitch_10
    const-string v5, "SysDump"

    const-string v6, "[f-d] QUERY_FD_STATE_DONE"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 520
    :pswitch_11
    const-string v5, "SysDump"

    const-string v6, "ENABLE_DBG_DONE"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x6

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    goto/16 :goto_0

    .line 525
    :pswitch_12
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 527
    .restart local v1    # "error":I
    if-nez v1, :cond_21

    .line 528
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "response"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    check-cast v0, [B

    .line 529
    .restart local v0    # "buf":[B
    if-eqz v0, :cond_0

    array-length v5, v0

    if-le v5, v11, :cond_0

    .line 530
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "QUERY_RAMDUMP_STATE_DONE : buf[0]:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / buf[1]:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / buf[3]:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    aget-byte v5, v0, v8

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v6, 0x7

    if-ne v5, v6, :cond_20

    aget-byte v5, v0, v9

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0xb

    if-ne v5, v6, :cond_20

    aget-byte v5, v0, v12

    const/4 v6, 0x5

    if-ne v5, v6, :cond_20

    .line 536
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "QUERY_RAMDUMP_STATE_DONE : state buf[4] : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    aget-byte v5, v0, v11

    if-nez v5, :cond_1c

    .line 539
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1702(I)I

    .line 540
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Debug Level Disabled/LOW"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 541
    :cond_1c
    aget-byte v5, v0, v11

    if-ne v5, v9, :cond_1d

    .line 542
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {v9}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1702(I)I

    .line 543
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Debug Level Enabled/MID"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 544
    :cond_1d
    aget-byte v5, v0, v11

    if-ne v5, v10, :cond_1e

    .line 545
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1702(I)I

    .line 546
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Debug Level Enabled/HIGH"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 547
    :cond_1e
    aget-byte v5, v0, v11

    if-ne v5, v12, :cond_1f

    .line 548
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1702(I)I

    .line 549
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Debug Level Enabled/AUTO"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 551
    :cond_1f
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1702(I)I

    .line 552
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Fail to read Ramdump Mode"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 555
    :cond_20
    const-string v5, "SysDump"

    const-string v6, "QUERY_RAMDUMP_STATE_DONE error#1"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 559
    .end local v0    # "buf":[B
    :cond_21
    const-string v5, "SysDump"

    const-string v6, "QUERY_RAMDUMP_STATE_DONE error#2"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 565
    .end local v1    # "error":I
    :pswitch_13
    const-string v5, "SysDump"

    const-string v6, "RAMDUMP_MODE_DONE"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v6, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static {v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0xb

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    goto/16 :goto_0

    .line 570
    :pswitch_14
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 572
    .restart local v1    # "error":I
    if-nez v1, :cond_22

    .line 573
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 574
    const-string v5, "SysDump"

    const-string v6, "QUERY_TCP_DUMP_DONE Success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "TCP DUMP STOP"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 578
    :cond_22
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 580
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "TCP DUMP START"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 581
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "TCP DUMP error\n(bind: Network is down)"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 582
    const-string v5, "SysDump"

    const-string v6, "QUERY_TCP_DUMP_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 588
    .end local v1    # "error":I
    :pswitch_15
    const-string v5, "SysDump"

    const-string v6, "QUERY_COPY_CARD_DONE"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "Copy Success!"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 593
    :pswitch_16
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 594
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "install Success!"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 599
    :pswitch_17
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 601
    .restart local v1    # "error":I
    if-nez v1, :cond_26

    .line 602
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "response"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    check-cast v0, [B

    .line 603
    .restart local v0    # "buf":[B
    if-eqz v0, :cond_25

    array-length v5, v0

    if-lez v5, :cond_25

    .line 604
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "QUERY_CP_POPUP_UI_STATE_DONE : result :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    aget-byte v5, v0, v8

    if-ne v5, v9, :cond_23

    .line 606
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {v9}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2002(I)I

    .line 607
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "CP Debugging Popup UI : Enabled"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 608
    :cond_23
    aget-byte v5, v0, v8

    if-ne v5, v10, :cond_24

    .line 609
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2002(I)I

    .line 610
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "CP Debugging Popup UI : Disabled"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 612
    :cond_24
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2002(I)I

    .line 613
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Fail to read CP Debugging Popup UI State"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 616
    :cond_25
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2002(I)I

    .line 617
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "Fail to read CP Debugging Popup UI State"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 620
    .end local v0    # "buf":[B
    :cond_26
    const-string v5, "SysDump"

    const-string v6, "QUERY_CP_POPUP_UI_STATE_DONE error"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 627
    .end local v1    # "error":I
    :pswitch_18
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 629
    .restart local v1    # "error":I
    if-nez v1, :cond_28

    .line 630
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 631
    const-string v5, "SysDump"

    const-string v6, "TOGGLE_CP_POPUP_UI_STATE_DONE Success"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2000()I

    move-result v5

    if-ne v5, v10, :cond_27

    .line 634
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {v9}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2002(I)I

    .line 635
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "CP Debugging Popup UI : Enabled"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 637
    :cond_27
    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2002(I)I

    .line 638
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "CP Debugging Popup UI : Disabled"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 641
    :cond_28
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 642
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "CP POPUP UI ERROR\n(Not Supported)"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 643
    const-string v5, "SysDump"

    const-string v6, "TOGGLE_CP_POPUP_UI_STATE_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 651
    .end local v1    # "error":I
    :pswitch_19
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    .line 652
    .local v4, "value":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_29

    .line 653
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "TCP DUMP STOP"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 654
    :cond_29
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v9, :cond_2a

    .line 655
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v5

    const-string v6, "TCP DUMP START"

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 657
    :cond_2a
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$1;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "TCP DUMP error\n(bind: Network is down)"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 658
    const-string v5, "SysDump"

    const-string v6, "WIFI_TCPDUMP_DONE fail"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 282
    nop

    :pswitch_data_0
    .packed-switch 0x3ec
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_13
        :pswitch_11
        :pswitch_1
        :pswitch_f
        :pswitch_12
        :pswitch_0
        :pswitch_10
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_14
        :pswitch_15
        :pswitch_2
        :pswitch_9
        :pswitch_17
        :pswitch_18
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_19
        :pswitch_16
    .end packed-switch
.end method
