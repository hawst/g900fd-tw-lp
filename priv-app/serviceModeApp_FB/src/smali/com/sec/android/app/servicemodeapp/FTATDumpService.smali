.class public Lcom/sec/android/app/servicemodeapp/FTATDumpService;
.super Landroid/app/Service;
.source "FTATDumpService.java"


# instance fields
.field private isEOS2:Z

.field private mAPLogsDoneFlag:Z

.field private mCPLogsDoneFlag:Z

.field private mCPSTELogDoneFlag:Z

.field private mConnectionSuccess:Z

.field private mCopyLogDoneFlag:Z

.field public mHandler:Landroid/os/Handler;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private sysdump:Lcom/sec/android/app/servicemodeapp/SysDump;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mServiceMessenger:Landroid/os/Messenger;

    .line 46
    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z

    .line 47
    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z

    .line 48
    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPSTELogDoneFlag:Z

    .line 49
    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 51
    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mConnectionSuccess:Z

    .line 52
    new-instance v1, Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-direct {v1}, Lcom/sec/android/app/servicemodeapp/SysDump;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->sysdump:Lcom/sec/android/app/servicemodeapp/SysDump;

    .line 53
    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EOS2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "u2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->isEOS2:Z

    .line 55
    new-instance v0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService$1;-><init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 67
    new-instance v0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService$2;-><init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    .line 144
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mSvcModeMessenger:Landroid/os/Messenger;

    return-void
.end method

.method private DoShellCmd(Ljava/lang/String;)Z
    .locals 9
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 329
    const-string v6, "FTATDumpService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DoShellCmd : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v2, 0x0

    .line 331
    .local v2, "p":Ljava/lang/Process;
    const/4 v6, 0x3

    new-array v3, v6, [Ljava/lang/String;

    const-string v6, "/system/bin/sh"

    aput-object v6, v3, v5

    const-string v6, "-c"

    aput-object v6, v3, v4

    const/4 v6, 0x2

    aput-object p1, v3, v6

    .line 336
    .local v3, "shell_command":[Ljava/lang/String;
    :try_start_0
    const-string v6, "FTATDumpService"

    const-string v7, "exec command"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 338
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    .line 339
    const-string v6, "FTATDumpService"

    const-string v7, "exec done"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 351
    const-string v5, "FTATDumpService"

    const-string v6, "DoShellCmd done"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :goto_0
    return v4

    .line 340
    :catch_0
    move-exception v1

    .line 341
    .local v1, "exception":Ljava/io/IOException;
    const-string v4, "FTATDumpService"

    const-string v6, "DoShellCmd - IOException"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 342
    goto :goto_0

    .line 343
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 344
    .local v1, "exception":Ljava/lang/SecurityException;
    const-string v4, "FTATDumpService"

    const-string v6, "DoShellCmd - SecurityException"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 345
    goto :goto_0

    .line 346
    .end local v1    # "exception":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 347
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v4, v5

    .line 348
    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->getCopyLog()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPSTELogDoneFlag:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mConnectionSuccess:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->isEOS2:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->getModemLogIPC()[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/servicemodeapp/FTATDumpService;[BLandroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;
    .param p1, "x1"    # [B
    .param p2, "x2"    # Landroid/os/Message;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->sendMessage([BLandroid/os/Message;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/FTATDumpService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->DoShellCmd(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private connectToRilService()V
    .locals 3

    .prologue
    .line 292
    const-string v1, "FTATDumpService"

    const-string v2, "connectToRilService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 294
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mConnectionSuccess:Z

    .line 299
    return-void
.end method

.method private getCopyLog()V
    .locals 3

    .prologue
    .line 242
    const-string v1, "FTATDumpService"

    const-string v2, "getCopyLog"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    const-string v1, "External SD Card UnMounted!!\nplease check if USB cable is connected or SD card is not inserted"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 283
    :goto_0
    return-void

    .line 250
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;-><init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 282
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private getModemLogIPC()[B
    .locals 8

    .prologue
    .line 302
    const/4 v1, 0x7

    .line 303
    .local v1, "OEM_SYSDUMP_FUNCTAG":I
    const/16 v0, 0x12

    .line 304
    .local v0, "OEM_MODEM_LOG":I
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 305
    .local v2, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 308
    .local v3, "dos":Ljava/io/DataOutputStream;
    const/4 v5, 0x7

    :try_start_0
    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 309
    const/16 v5, 0x12

    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 310
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 311
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    if-eqz v3, :cond_0

    .line 318
    :try_start_1
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 325
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    :cond_1
    :goto_1
    return-object v5

    .line 319
    :catch_0
    move-exception v4

    .line 320
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 312
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 313
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    const-string v5, "FTATDumpService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException in getModemLogIPC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 314
    const/4 v5, 0x0

    .line 316
    if-eqz v3, :cond_1

    .line 318
    :try_start_3
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 319
    :catch_2
    move-exception v4

    .line 320
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 316
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_2

    .line 318
    :try_start_4
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 321
    :cond_2
    :goto_2
    throw v5

    .line 319
    :catch_3
    move-exception v4

    .line 320
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private sendMessage([BLandroid/os/Message;)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 147
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 148
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "request"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 149
    invoke-virtual {p2, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v3, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 151
    const/4 v0, 0x0

    .line 154
    .local v0, "cnt":I
    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v0, v3, :cond_0

    .line 155
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_1

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    :cond_0
    :goto_1
    return-void

    .line 160
    :cond_1
    :try_start_1
    const-string v3, "FTATDumpService"

    const-string v4, "mServiceMessenger is NULL"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    const-wide/16 v4, 0xc8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 154
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :catch_0
    move-exception v1

    .line 163
    .local v1, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 167
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "out"    # Ljava/lang/String;
    .param p3, "DumpType"    # Ljava/lang/String;

    .prologue
    .line 416
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "write to sdcard DumpType : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v9, 0x1

    .line 420
    .local v9, "result":Z
    const/4 v3, 0x0

    .line 421
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 423
    .local v6, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 425
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    :try_start_2
    new-array v1, v10, [B

    .line 427
    .local v1, "buffer":[B
    :goto_0
    invoke-virtual {v4, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    .local v8, "n":I
    const/4 v10, -0x1

    if-le v8, v10, :cond_2

    .line 428
    const/4 v10, 0x0

    invoke-virtual {v7, v1, v10, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 431
    .end local v1    # "buffer":[B
    .end local v8    # "n":I
    :catch_0
    move-exception v5

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 432
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .local v5, "fnfe":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "fnfe : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    const/4 v9, 0x0

    .line 434
    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v11, "// Exception from"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 435
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FileNotFoundException : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 440
    if-eqz v6, :cond_0

    .line 442
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 447
    :goto_2
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 453
    :cond_0
    :goto_3
    if-eqz v3, :cond_1

    .line 455
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 462
    .end local v5    # "fnfe":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_4
    return v9

    .line 430
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "n":I
    :cond_2
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_f
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 440
    if-eqz v7, :cond_3

    .line 442
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 447
    :goto_5
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    .line 453
    :cond_3
    :goto_6
    if-eqz v4, :cond_7

    .line 455
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 458
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 443
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 444
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 448
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 456
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v2

    .line 457
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 458
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 443
    .end local v1    # "buffer":[B
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v8    # "n":I
    .restart local v5    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v2

    .line 444
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 448
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 456
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v2

    .line 457
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 436
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_7
    move-exception v2

    .line 437
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_7
    const/4 v9, 0x0

    .line 438
    :try_start_b
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 440
    if-eqz v6, :cond_4

    .line 442
    :try_start_c
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    .line 447
    :goto_8
    :try_start_d
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    .line 453
    :cond_4
    :goto_9
    if-eqz v3, :cond_1

    .line 455
    :try_start_e
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    goto/16 :goto_4

    .line 456
    :catch_8
    move-exception v2

    .line 457
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 443
    :catch_9
    move-exception v2

    .line 444
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 448
    :catch_a
    move-exception v2

    .line 449
    const-string v10, "FTATDumpService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 440
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    :goto_a
    if-eqz v6, :cond_5

    .line 442
    :try_start_f
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/FileDescriptor;->sync()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    .line 447
    :goto_b
    :try_start_10
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_c

    .line 453
    :cond_5
    :goto_c
    if-eqz v3, :cond_6

    .line 455
    :try_start_11
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_d

    .line 458
    :cond_6
    :goto_d
    throw v10

    .line 443
    :catch_b
    move-exception v2

    .line 444
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 448
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 456
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v2

    .line 457
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 440
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v10

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v10

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 436
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_e
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_f
    move-exception v2

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .line 431
    :catch_10
    move-exception v5

    goto/16 :goto_1

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_11
    move-exception v5

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "n":I
    :cond_7
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public copyDirectory(Ljava/io/File;Ljava/io/File;)V
    .locals 15
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dest"    # Ljava/io/File;

    .prologue
    .line 356
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "copyDirectory : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 359
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 360
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->mkdir()Z

    .line 363
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 365
    .local v3, "fileList":[Ljava/lang/String;
    if-eqz v3, :cond_1

    array-length v11, v3

    if-gtz v11, :cond_2

    .line 413
    .end local v3    # "fileList":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 369
    .restart local v3    # "fileList":[Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v11, v3

    if-ge v9, v11, :cond_1

    .line 370
    new-instance v11, Ljava/io/File;

    aget-object v12, v3, v9

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    aget-object v13, v3, v9

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v11, v12}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 369
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 374
    .end local v3    # "fileList":[Ljava/lang/String;
    .end local v9    # "i":I
    :cond_3
    const/4 v4, 0x0

    .line 375
    .local v4, "fin":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 378
    .local v7, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .local v5, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 381
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .local v8, "fout":Ljava/io/FileOutputStream;
    const/16 v11, 0x400

    :try_start_2
    new-array v1, v11, [B

    .line 384
    .local v1, "buffer":[B
    :goto_2
    invoke-virtual {v5, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    .local v10, "len":I
    if-lez v10, :cond_5

    .line 385
    const/4 v11, 0x0

    invoke-virtual {v8, v1, v11, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 388
    .end local v1    # "buffer":[B
    .end local v10    # "len":I
    :catch_0
    move-exception v6

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 389
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .local v6, "fnfe":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_3
    sget-object v11, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v12, "// Exception from"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 390
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "FileNotFoundException : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 394
    if-eqz v4, :cond_4

    .line 396
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 397
    const/4 v4, 0x0

    .line 403
    :cond_4
    :goto_4
    if-eqz v7, :cond_1

    .line 405
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 406
    const/4 v7, 0x0

    goto :goto_0

    .line 387
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "len":I
    :cond_5
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 394
    if-eqz v5, :cond_6

    .line 396
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 397
    const/4 v4, 0x0

    .line 403
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :goto_5
    if-eqz v8, :cond_1

    .line 405
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 406
    const/4 v7, 0x0

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 398
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 399
    .local v2, "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_6
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_5

    .line 407
    :catch_2
    move-exception v2

    .line 408
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 398
    .end local v1    # "buffer":[B
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .end local v10    # "len":I
    .restart local v6    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    .line 399
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 407
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 408
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 391
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v2

    .line 392
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_6
    :try_start_9
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 394
    if-eqz v4, :cond_7

    .line 396
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 397
    const/4 v4, 0x0

    .line 403
    :cond_7
    :goto_7
    if-eqz v7, :cond_1

    .line 405
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    .line 406
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 398
    :catch_6
    move-exception v2

    .line 399
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 407
    :catch_7
    move-exception v2

    .line 408
    const-string v11, "FTATDumpService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 394
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    :goto_8
    if-eqz v4, :cond_8

    .line 396
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    .line 397
    const/4 v4, 0x0

    .line 403
    :cond_8
    :goto_9
    if-eqz v7, :cond_9

    .line 405
    :try_start_d
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    .line 406
    const/4 v7, 0x0

    .line 409
    :cond_9
    :goto_a
    throw v11

    .line 398
    :catch_8
    move-exception v2

    .line 399
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v12, "FTATDumpService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 407
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v2

    .line 408
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v12, "FTATDumpService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 394
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_8

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v11

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_8

    .line 391
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_a
    move-exception v2

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v2

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 388
    :catch_c
    move-exception v6

    goto/16 :goto_3

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_d
    move-exception v6

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method public doWakeLock(Z)V
    .locals 4
    .param p1, "wake"    # Z

    .prologue
    .line 466
    const-string v1, "FTATDumpService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doWakeLock : wake="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 469
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 470
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 471
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string v2, "FTATDumpService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 475
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 477
    const-string v1, "FTATDumpService"

    const-string v2, "doWakeLock : FULL WAKELOCK ON"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_1
    :goto_0
    return-void

    .line 480
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 483
    const-string v1, "FTATDumpService"

    const-string v2, "doWakeLock : FULL WAKELOCK OFF"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 177
    const-string v0, "FTATDumpService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->connectToRilService()V

    .line 179
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 286
    const-string v0, "FTATDumpService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 289
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 183
    const-string v5, "FTATDumpService"

    const-string v6, "onStartCommand()"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const-string v5, "FTATDumpService"

    const-string v6, "faildumphandler : ACQUIRE_WAKE_LOCK "

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {p0, v2}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->doWakeLock(Z)V

    .line 188
    if-nez p1, :cond_0

    const-string v1, "Default"

    .line 189
    .local v1, "filename":Ljava/lang/String;
    :goto_0
    if-nez p1, :cond_1

    .line 191
    .local v2, "hasCP":Z
    :goto_1
    const-string v3, "/data/log/Bugreport.log"

    .line 192
    .local v3, "rawDumpFileName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/data/log/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".log"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "dumpFileName":Ljava/lang/String;
    iput-boolean v7, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mAPLogsDoneFlag:Z

    .line 195
    iput-boolean v7, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z

    .line 196
    iput-boolean v7, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCopyLogDoneFlag:Z

    .line 197
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;

    invoke-direct {v5, p0, v2, v0}, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;-><init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;ZLjava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 237
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 238
    const/4 v5, 0x2

    return v5

    .line 188
    .end local v0    # "dumpFileName":Ljava/lang/String;
    .end local v1    # "filename":Ljava/lang/String;
    .end local v2    # "hasCP":Z
    .end local v3    # "rawDumpFileName":Ljava/lang/String;
    .end local v4    # "thread":Ljava/lang/Thread;
    :cond_0
    const-string v5, "FILENAME"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 189
    .restart local v1    # "filename":Ljava/lang/String;
    :cond_1
    const-string v5, "has_modem"

    invoke-virtual {p1, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_1
.end method
