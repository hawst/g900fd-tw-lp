.class Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$5;
.super Ljava/lang/Object;
.source "PhoneUtil_TD.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$5;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 299
    const-string v0, "PhoneUtil_TD"

    const-string v1, "onServiceConnected"

    const-string v2, "onServiceConnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$5;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->access$402(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 301
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 303
    const-string v0, "PhoneUtil_TD"

    const-string v1, "onServiceDisconnected"

    const-string v2, "onServiceDisconnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$5;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;->access$402(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 305
    return-void
.end method
