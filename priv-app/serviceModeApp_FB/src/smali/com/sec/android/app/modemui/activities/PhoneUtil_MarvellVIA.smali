.class public Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;
.super Landroid/app/Activity;
.source "PhoneUtil_MarvellVIA.java"


# instance fields
.field private MODEL:Ljava/lang/String;

.field private mSaveResetButton:Landroid/widget/Button;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUartToAp:Z

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbToAp:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->MODEL:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUartToAp:Z

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbToAp:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->changeUart(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUartToAp:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUartToAp:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->changeUsb(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbToAp:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbToAp:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->doRebootNSave(I)V

    return-void
.end method

.method private changeUart(Ljava/lang/String;)V
    .locals 3
    .param p1, "uart"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    const-string v0, "PhoneUtil_Marvell"

    const-string v1, "changeUart"

    const-string v2, "UART to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "/sys/devices/virtual/sec/switch/uart_sel"

    const-string v1, "AP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "PhoneUtil_Marvell"

    const-string v1, "changeUart"

    const-string v2, "UART to VIA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "/sys/devices/virtual/sec/switch/uart_sel"

    const-string v1, "CP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private changeUsb(Ljava/lang/String;)V
    .locals 3
    .param p1, "usb"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    const-string v0, "PhoneUtil_Marvell"

    const-string v1, "changeUsb"

    const-string v2, "USB to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v0, "/sys/devices/virtual/sec/switch/usb_sel"

    const-string v1, "PDA"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const-string v0, "PhoneUtil_Marvell"

    const-string v1, "changeUsb"

    const-string v2, "USB to VIA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v0, "/sys/devices/virtual/sec/switch/usb_sel"

    const-string v1, "MODEM"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "doRebootNSave"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchsel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 152
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method private initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V
    .locals 6
    .param p1, "uart"    # Landroid/widget/RadioGroup;
    .param p2, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    .line 156
    const-string v2, "/sys/devices/virtual/sec/switch/uart_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "currentUart":Ljava/lang/String;
    const-string v2, "/sys/devices/virtual/sec/switch/usb_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "currentUsb":Ljava/lang/String;
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UART_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "USB_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v2, "CP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 163
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : VIA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const v2, 0x7f090024

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 170
    :cond_0
    :goto_0
    const-string v2, "MODEM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : VIA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const v2, 0x7f090028

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 177
    :cond_1
    :goto_1
    return-void

    .line 165
    :cond_2
    const-string v2, "AP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const v2, 0x7f090025

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 173
    :cond_3
    const-string v2, "PDA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    const-string v2, "PhoneUtil_Marvell"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const v2, 0x7f090029

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string v6, ""

    .line 205
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 206
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 208
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 210
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 211
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 221
    :cond_0
    if-eqz v5, :cond_1

    .line 222
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 223
    :cond_1
    if-eqz v1, :cond_2

    .line 224
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 230
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 231
    const-string v7, ""

    .line 233
    :goto_1
    return-object v7

    .line 225
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 226
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_Marvell"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 229
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 213
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 214
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "PhoneUtil_Marvell"

    const-string v8, "readOneLine"

    const-string v9, "FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 221
    if-eqz v4, :cond_4

    .line 222
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 223
    :cond_4
    if-eqz v0, :cond_3

    .line 224
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 225
    :catch_2
    move-exception v2

    .line 226
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil_Marvell"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 216
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 217
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "PhoneUtil_Marvell"

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 221
    if-eqz v4, :cond_5

    .line 222
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 223
    :cond_5
    if-eqz v0, :cond_3

    .line 224
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 225
    :catch_4
    move-exception v2

    .line 226
    const-string v7, "PhoneUtil_Marvell"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 220
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 221
    :goto_4
    if-eqz v4, :cond_6

    .line 222
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 223
    :cond_6
    if-eqz v0, :cond_7

    .line 224
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 228
    :cond_7
    :goto_5
    throw v7

    .line 225
    :catch_5
    move-exception v2

    .line 226
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "PhoneUtil_Marvell"

    const-string v9, "readOneLine"

    const-string v10, "IOException close()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 233
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 220
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 216
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 213
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 237
    const/4 v1, 0x0

    .line 239
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 245
    const-string v3, "PhoneUtil_Marvell"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    if-eqz v2, :cond_2

    .line 255
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 261
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 256
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "PhoneUtil_Marvell"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 258
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 241
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 242
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "PhoneUtil_Marvell"

    const-string v4, "writeFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOExceptionfilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 245
    const-string v3, "PhoneUtil_Marvell"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    if-eqz v1, :cond_0

    .line 255
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 256
    :catch_2
    move-exception v0

    .line 257
    const-string v3, "PhoneUtil_Marvell"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 245
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "PhoneUtil_Marvell"

    const-string v5, "writeFile"

    const-string v6, "Write Success!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    if-eqz v1, :cond_1

    .line 255
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 258
    :cond_1
    :goto_3
    throw v3

    .line 256
    :catch_3
    move-exception v0

    .line 257
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "PhoneUtil_Marvell"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 245
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 241
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f030012

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->setContentView(I)V

    .line 78
    const-string v0, "PhoneUtil_Marvell"

    const-string v1, "onCreate"

    const-string v2, "Hello oncreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 106
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 134
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mSaveResetButton:Landroid/widget/Button;

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mSaveResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$3;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUartRadioGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V

    .line 146
    return-void
.end method
