.class Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;
.super Ljava/lang/Object;
.source "DiagMdlogOption.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->chooseDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 117
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "DiagMdlogOption"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "choosen:("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    iget-object v3, v3, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->foundFiles:[Ljava/lang/String;

    aget-object v3, v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const-string v1, "mask_file"

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    iget-object v2, v2, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->foundFiles:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string v1, "diag_mb"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    # getter for: Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->diag_mb:I
    invoke-static {v3}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->access$000(Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->setResult(ILandroid/content/Intent;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption$1;->this$0:Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;

    invoke-virtual {v1}, Lcom/sec/android/app/servicemodeapp/DiagMdlogOption;->finish()V

    .line 123
    return-void
.end method
