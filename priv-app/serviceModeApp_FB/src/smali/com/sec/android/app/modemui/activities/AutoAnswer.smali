.class public Lcom/sec/android/app/modemui/activities/AutoAnswer;
.super Landroid/app/Activity;
.source "AutoAnswer.java"


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private mAutoAnswerMode:Landroid/widget/RadioGroup;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const-string v0, "AutoAnswer"

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method private initialSetting(Landroid/widget/RadioGroup;)V
    .locals 5
    .param p1, "AutoAnswer"    # Landroid/widget/RadioGroup;

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "autoanswering_without_device"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 80
    .local v0, "currentStatus":I
    const-string v1, "AutoAnswer"

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AutoAnswer Init Status : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    if-nez v0, :cond_1

    .line 83
    const v1, 0x7f090003

    invoke-virtual {p1, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 85
    const v1, 0x7f090002

    invoke-virtual {p1, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    iput-object p0, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer;->mContext:Landroid/content/Context;

    .line 46
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->setContentView(I)V

    .line 47
    const v0, 0x7f090001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer;->mAutoAnswerMode:Landroid/widget/RadioGroup;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer;->mAutoAnswerMode:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;-><init>(Lcom/sec/android/app/modemui/activities/AutoAnswer;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer;->mAutoAnswerMode:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->initialSetting(Landroid/widget/RadioGroup;)V

    .line 75
    return-void
.end method
