.class Lcom/sec/android/app/servicemodeapp/SysDump$7$12;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V
    .locals 0

    .prologue
    .line 2569
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$12;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2571
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.sec.tcpdumpservice"

    const-string v5, "com.sec.tcpdumpservice.TcpDumpService"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2572
    .local v2, "tcpDumpSvcIntent":Landroid/content/Intent;
    const-string v3, "ro.product_ship"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2574
    .local v1, "productship":Ljava/lang/String;
    const-string v3, "false"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1700()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1700()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2577
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$12;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v3, v3, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->stopService(Landroid/content/Intent;)Z

    .line 2578
    const-wide/16 v4, 0x3e8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 2579
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$12;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v3, v3, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2584
    :cond_1
    :goto_0
    return-void

    .line 2580
    :catch_0
    move-exception v0

    .line 2581
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "SysDump"

    const-string v4, "tcpDumpThread - exception occurred"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
