.class Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
.super Ljava/lang/Object;
.source "SysDump.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_DBG_STATE_GET:I

.field final OEM_DEL_RIL_LOG:I

.field final OEM_DPRAM_DUMP:I

.field final OEM_DUMPSTATE:I

.field final OEM_DUMPSTATE_ALL:I

.field final OEM_ENABLE_LOG:I

.field final OEM_FUNCTION_ID_MISC:I

.field final OEM_GCF_MODE_GET:I

.field final OEM_GCF_MODE_SET:I

.field final OEM_GET_PHONE_DEBUG_MSG:I

.field final OEM_IPC_DUMP_BIN:I

.field final OEM_IPC_DUMP_LOG:I

.field final OEM_KERNEL_LOG:I

.field final OEM_LOGCAT_CLEAR:I

.field final OEM_LOGCAT_MAIN:I

.field final OEM_LOGCAT_RADIO:I

.field final OEM_MISC_SILENT_LOGGING_CONTROL:I

.field final OEM_MODEM_FORCE_CRASH_EXIT:I

.field final OEM_MODEM_LOG:I

.field final OEM_NV_DATA_BACKUP:I

.field final OEM_OEM_DUMPSTATE_MODEM_LOG_AUTO_START:I

.field final OEM_RAMDUMP_MODE:I

.field final OEM_RAMDUMP_STATE_GET:I

.field final OEM_SET_PHONE_DEBUG_MSG:I

.field final OEM_START_RIL_LOG:I

.field final OEM_SYSDUMP_FUNCTAG:I

.field final OEM_TCPDUMP_START:I

.field final OEM_TCPDUMP_STOP:I

.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 3

    .prologue
    const/16 v2, 0x11

    const/4 v1, 0x7

    .line 670
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671
    iput v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_SYSDUMP_FUNCTAG:I

    .line 672
    iput v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_FUNCTION_ID_MISC:I

    .line 673
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_LOGCAT_MAIN:I

    .line 674
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_LOGCAT_RADIO:I

    .line 675
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_DUMPSTATE:I

    .line 676
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_KERNEL_LOG:I

    .line 677
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_LOGCAT_CLEAR:I

    .line 678
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_DBG_STATE_GET:I

    .line 679
    iput v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_ENABLE_LOG:I

    .line 680
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_IPC_DUMP_LOG:I

    .line 681
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_IPC_DUMP_BIN:I

    .line 682
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_RAMDUMP_MODE:I

    .line 683
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_RAMDUMP_STATE_GET:I

    .line 684
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_START_RIL_LOG:I

    .line 685
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_DEL_RIL_LOG:I

    .line 686
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_DPRAM_DUMP:I

    .line 687
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_GCF_MODE_GET:I

    .line 688
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_GCF_MODE_SET:I

    .line 689
    iput v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_NV_DATA_BACKUP:I

    .line 690
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_MODEM_LOG:I

    .line 691
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_OEM_DUMPSTATE_MODEM_LOG_AUTO_START:I

    .line 692
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_DUMPSTATE_ALL:I

    .line 693
    const/16 v0, 0x15

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_TCPDUMP_START:I

    .line 694
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_TCPDUMP_STOP:I

    .line 695
    const/16 v0, 0x1a

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_GET_PHONE_DEBUG_MSG:I

    .line 696
    const/16 v0, 0x1b

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_SET_PHONE_DEBUG_MSG:I

    .line 697
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_MODEM_FORCE_CRASH_EXIT:I

    .line 698
    const/16 v0, 0x40

    iput v0, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->OEM_MISC_SILENT_LOGGING_CONTROL:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;Lcom/sec/android/app/servicemodeapp/SysDump$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/servicemodeapp/SysDump;
    .param p2, "x1"    # Lcom/sec/android/app/servicemodeapp/SysDump$1;

    .prologue
    .line 670
    invoke-direct {p0, p1}, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    return-void
.end method


# virtual methods
.method StartSilentData(Z)[B
    .locals 6
    .param p1, "mode"    # Z

    .prologue
    const/4 v5, 0x1

    .line 752
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 753
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 756
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x11

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 757
    const/16 v3, 0x40

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 758
    const-string v3, "SysDump"

    const-string v4, "cmd : 64"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    const-string v3, "SysDump"

    const-string v4, "dos.writeByte(5)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 762
    if-ne p1, v5, :cond_0

    .line 763
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 772
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    :goto_1
    return-object v3

    .line 765
    :cond_0
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 767
    :catch_0
    move-exception v2

    .line 768
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "SysDump"

    const-string v4, "IOException in getServMQueryData!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    const/4 v3, 0x0

    goto :goto_1
.end method

.method StartSilentDataCP2(Z)[B
    .locals 7
    .param p1, "mode"    # Z

    .prologue
    const/4 v6, 0x1

    .line 777
    const/4 v0, 0x4

    .line 778
    .local v0, "MODEM_GSM":C
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 779
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 780
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v4, 0x8

    .line 782
    .local v4, "fileSize":C
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 783
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 784
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 785
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 786
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 787
    if-ne p1, v6, :cond_0

    .line 788
    const/4 v5, 0x6

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 792
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    :goto_1
    return-object v5

    .line 790
    :cond_0
    const/16 v5, 0x14

    :try_start_1
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 793
    :catch_0
    move-exception v3

    .line 794
    .local v3, "e":Ljava/io/IOException;
    const-string v5, "SysDump"

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    const/4 v5, 0x0

    goto :goto_1
.end method

.method StartSysDumpData(I)[B
    .locals 10
    .param p1, "cmd"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 701
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 702
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 705
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v5, 0x7

    :try_start_0
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 706
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 707
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cmd : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    if-eq p1, v9, :cond_0

    if-eq p1, v8, :cond_0

    const/16 v5, 0x9

    if-eq p1, v5, :cond_0

    const/4 v5, 0x3

    if-ne p1, v5, :cond_2

    .line 711
    :cond_0
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 712
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->month:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 713
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->day:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 714
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->hour:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 715
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->min:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 716
    const-string v5, "SysDump"

    const-string v6, "dos.writeByte(8)"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 749
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    :goto_1
    return-object v5

    .line 717
    :cond_2
    const/16 v5, 0x12

    if-ne p1, v5, :cond_3

    .line 718
    const/4 v5, 0x5

    :try_start_1
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 719
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 744
    :catch_0
    move-exception v2

    .line 745
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "SysDump"

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    const/4 v5, 0x0

    goto :goto_1

    .line 720
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    const/16 v5, 0x15

    if-ne p1, v5, :cond_4

    .line 721
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 722
    .local v4, "tcpdump_interface_byte":[B
    const-string v5, "SysDump"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dos.writeByte length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v4

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    array-length v5, v4

    add-int/lit8 v5, v5, 0x4

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 726
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v5, v4

    if-ge v3, v5, :cond_1

    .line 727
    aget-byte v5, v4, v3

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 726
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 729
    .end local v3    # "i":I
    .end local v4    # "tcpdump_interface_byte":[B
    :cond_4
    const/16 v5, 0x17

    if-ne p1, v5, :cond_5

    .line 730
    const-string v5, "SysDump"

    const-string v6, "OEM_MODEM_FORCE_CRASH_EXIT by user"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 732
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 733
    :cond_5
    const/16 v5, 0x1b

    if-ne p1, v5, :cond_7

    .line 734
    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 735
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iCPPopupUIState:I
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2000()I

    move-result v5

    if-ne v5, v8, :cond_6

    .line 736
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 738
    :cond_6
    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto/16 :goto_0

    .line 741
    :cond_7
    const-string v5, "SysDump"

    const-string v6, "dos.writeByte(4)"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method
