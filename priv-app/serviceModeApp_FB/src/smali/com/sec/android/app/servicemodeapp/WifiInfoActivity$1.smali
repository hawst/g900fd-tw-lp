.class Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "WifiInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 67
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    const-string v1, "WiFiDebugScreen"

    const-string v2, "1:WifiManager.NETWORK_STATE_CHANGED_ACTION"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    iput-object v1, v2, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mNetworkInfo:Landroid/net/NetworkInfo;

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # invokes: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updateWifiInfo()V
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$000(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    const-string v1, "WiFiDebugScreen"

    const-string v2, "3:WifiManager.WIFI_STATE_CHANGED_ACTION"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # invokes: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updateWifiInfo()V
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$000(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    .line 75
    const-string v1, "wifi_state"

    const/4 v2, 0x4

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 76
    const-string v1, "WiFiDebugScreen"

    const-string v2, "4:WifiManager.WIFI_STATE_DISABLED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # getter for: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$100(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 78
    const-string v1, "WiFiDebugScreen"

    const-string v2, "5:mIsPowerSaveChanged==true"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$102(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;Z)Z

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTimerHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 83
    :cond_2
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;->this$0:Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    # invokes: Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updateWifiInfo()V
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->access$000(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    goto :goto_0
.end method
