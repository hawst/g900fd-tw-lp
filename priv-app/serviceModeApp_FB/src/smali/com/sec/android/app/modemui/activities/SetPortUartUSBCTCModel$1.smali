.class Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;
.super Ljava/lang/Object;
.source "SetPortUartUSBCTCModel.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 118
    const v1, 0x7f090024

    if-ne p2, v1, :cond_1

    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$000(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$102(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Z)Z

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const-string v2, "+CPUART"

    # invokes: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUartPath(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$200(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V

    .line 123
    const-string v1, "SetPortUartUSBCTCModel"

    const-string v2, "onCreate"

    const-string v3, "UART_STATUS1 : MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SetPortUartUSBCTCModel"

    const-string v2, "onCreate"

    const-string v3, "Exception! UART to Modem"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090025

    if-ne p2, v1, :cond_2

    .line 130
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const-string v2, "PDA"

    # invokes: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$000(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$102(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Z)Z

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const-string v2, "+APUART"

    # invokes: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUartPath(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$200(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V

    .line 133
    const-string v1, "SetPortUartUSBCTCModel"

    const-string v2, "onCreate"

    const-string v3, "UART_STATUS2 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 134
    :catch_1
    move-exception v0

    .line 135
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "SetPortUartUSBCTCModel"

    const-string v2, "onCreate"

    const-string v3, "Exception! UART to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 138
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const v1, 0x7f090026

    if-ne p2, v1, :cond_0

    .line 140
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const-string v2, "SPRD"

    # invokes: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->changeUart(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$000(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Ljava/lang/String;)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel$1;->this$0:Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->mUartToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;->access$102(Lcom/sec/android/app/modemui/activities/SetPortUartUSBCTCModel;Z)Z

    .line 142
    const-string v1, "SetPortUartUSBCTCModel"

    const-string v2, "onCheckedChanged"

    const-string v3, "UART_STATUS1 : SPRD"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 143
    :catch_2
    move-exception v0

    .line 144
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "SetPortUartUSBCTCModel"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! UART to SPRD"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
