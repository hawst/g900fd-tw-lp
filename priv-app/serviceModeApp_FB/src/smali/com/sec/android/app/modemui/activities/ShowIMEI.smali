.class public Lcom/sec/android/app/modemui/activities/ShowIMEI;
.super Landroid/app/Activity;
.source "ShowIMEI.java"


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private CTC_CGG:Z

.field private DEVICE:Ljava/lang/String;

.field private IS_SDCG:Z

.field private MODEL:Ljava/lang/String;

.field private NAME:Ljava/lang/String;

.field private mOkPopup:Landroid/app/AlertDialog;

.field private mPhone:[Lcom/android/internal/telephony/Phone;

.field private productModel:Ljava/lang/String;

.field private tm1:Landroid/telephony/TelephonyManager;

.field private tm2:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const-string v0, "ShowIMEI"

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->CLASS_NAME:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mOkPopup:Landroid/app/AlertDialog;

    .line 29
    const-string v0, "ro.product.model"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    .line 30
    const-string v0, "ro.product.device"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->DEVICE:Ljava/lang/String;

    .line 31
    const-string v0, "ro.product.name"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->NAME:Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g5609w"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g9009d"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g9009w"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-n9009"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g3589w"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-n9109w"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-a7009"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g8509v"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-w2015"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-a5009"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g7509"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-e7009"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->CTC_CGG:Z

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-g3559"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v3, "sm-t331c"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->IS_SDCG:Z

    .line 34
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->productModel:Ljava/lang/String;

    return-void

    :cond_3
    move v0, v1

    .line 32
    goto :goto_0
.end method

.method private showIMEI()Z
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 51
    const-string v6, "phone"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    .line 53
    .local v4, "phoneType":I
    const-string v6, "ShowIMEI"

    const-string v9, "showIMEI"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "phoneType = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v6, "phone"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v1

    .line 56
    .local v1, "currentPhoneType":I
    const-string v6, "ShowIMEI"

    const-string v9, "showIMEI"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "currentPhoneType = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->productModel:Ljava/lang/String;

    const-string v9, "CTC"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->NAME:Ljava/lang/String;

    const-string v9, "ctc"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 60
    :cond_0
    const-string v6, "phone2"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 64
    .local v3, "mtelephonyManager2":Landroid/telephony/TelephonyManager;
    iget-boolean v6, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->CTC_CGG:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->IS_SDCG:Z

    if-eqz v6, :cond_2

    .line 65
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->showIMEIPanel()V

    move v6, v7

    .line 122
    .end local v3    # "mtelephonyManager2":Landroid/telephony/TelephonyManager;
    :goto_0
    return v6

    .line 69
    .restart local v3    # "mtelephonyManager2":Landroid/telephony/TelephonyManager;
    :cond_2
    if-nez v3, :cond_3

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->finish()V

    :goto_1
    move v6, v7

    .line 98
    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "imeiStr":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v5

    .line 86
    .local v5, "svStr":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " / "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 88
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v9, "IMEI"

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v9, 0x104000a

    new-instance v10, Lcom/sec/android/app/modemui/activities/ShowIMEI$1;

    invoke-direct {v10, p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI$1;-><init>(Lcom/sec/android/app/modemui/activities/ShowIMEI;)V

    invoke-virtual {v6, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 99
    .end local v2    # "imeiStr":Ljava/lang/String;
    .end local v3    # "mtelephonyManager2":Landroid/telephony/TelephonyManager;
    .end local v5    # "svStr":Ljava/lang/String;
    :cond_4
    iget-boolean v6, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->IS_SDCG:Z

    if-eqz v6, :cond_5

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->showIMEIPanel()V

    move v6, v7

    .line 101
    goto :goto_0

    .line 104
    :cond_5
    if-eq v4, v7, :cond_6

    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->productModel:Ljava/lang/String;

    const-string v9, "VZW"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 105
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->showIMEIPanel()V

    move v6, v7

    .line 106
    goto :goto_0

    .line 107
    :cond_7
    const/4 v6, 0x2

    if-ne v4, v6, :cond_8

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->showMEIDPanel()V

    move v6, v7

    .line 109
    goto :goto_0

    .line 110
    :cond_8
    if-nez v4, :cond_a

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v9, "connectivity"

    invoke-virtual {v6, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 114
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->showIMEIPanel()V

    move v6, v7

    .line 116
    goto/16 :goto_0

    .line 118
    :cond_9
    const-string v6, "ShowIMEI"

    const-string v7, "showIMEI"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ConnectivityManager.isNetworkSupported: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v7, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    :cond_a
    move v6, v8

    .line 122
    goto/16 :goto_0
.end method

.method private showIMEIPanel()V
    .locals 12

    .prologue
    const v11, 0x104000a

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 126
    const-string v0, ""

    .line 127
    .local v0, "imeiStr":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->productModel:Ljava/lang/String;

    const-string v8, "KDI"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 128
    invoke-static {}, Lcom/sec/android/app/dummy/TelephonyManager;->getImeiInCDMAGSMPhone()Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_0
    const-string v7, "phone"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, "svStr":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v8, "gt-i9502"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v8, "sch-i869via"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    const-string v8, "sm-n9002"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 143
    :cond_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getFirst()Landroid/telephony/TelephonyManager;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm1:Landroid/telephony/TelephonyManager;

    .line 144
    invoke-static {}, Landroid/telephony/TelephonyManager;->getSecondary()Landroid/telephony/TelephonyManager;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm2:Landroid/telephony/TelephonyManager;

    .line 145
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm1:Landroid/telephony/TelephonyManager;

    if-eqz v7, :cond_1

    .line 146
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm1:Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 147
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm1:Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v5

    .line 148
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm2:Landroid/telephony/TelephonyManager;

    if-eqz v7, :cond_2

    .line 151
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm2:Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "imeiStr2":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->tm2:Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v6

    .line 153
    .local v6, "svStr2":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    .end local v1    # "imeiStr2":Ljava/lang/String;
    .end local v6    # "svStr2":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->productModel:Ljava/lang/String;

    const-string v8, "VZW"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 178
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v8, "MEID"

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/modemui/activities/ShowIMEI$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI$2;-><init>(Lcom/sec/android/app/modemui/activities/ShowIMEI;)V

    invoke-virtual {v7, v11, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 220
    :goto_2
    return-void

    .line 131
    .end local v5    # "svStr":Ljava/lang/String;
    :cond_3
    const-string v7, "phone"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 156
    .restart local v5    # "svStr":Ljava/lang/String;
    :cond_4
    iget-boolean v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->CTC_CGG:Z

    if-nez v7, :cond_5

    iget-boolean v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->IS_SDCG:Z

    if-eqz v7, :cond_2

    .line 158
    :cond_5
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v7

    new-array v7, v7, [Lcom/android/internal/telephony/Phone;

    iput-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    .line 159
    const/4 v4, 0x0

    .local v4, "simSlotNum":I
    :goto_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v7

    if-ge v4, v7, :cond_6

    .line 160
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    invoke-static {v4}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v8

    aput-object v8, v7, v4

    .line 159
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 164
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    aget-object v7, v7, v9

    if-eqz v7, :cond_7

    .line 165
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    aget-object v7, v7, v9

    invoke-interface {v7}, Lcom/android/internal/telephony/Phone;->getImei()Ljava/lang/String;

    move-result-object v0

    .line 166
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    aget-object v7, v7, v9

    invoke-interface {v7}, Lcom/android/internal/telephony/Phone;->getDeviceSvn()Ljava/lang/String;

    move-result-object v5

    .line 167
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    aget-object v7, v7, v10

    if-eqz v7, :cond_2

    .line 171
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    aget-object v7, v7, v10

    invoke-interface {v7}, Lcom/android/internal/telephony/Phone;->getImei()Ljava/lang/String;

    move-result-object v1

    .line 172
    .restart local v1    # "imeiStr2":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mPhone:[Lcom/android/internal/telephony/Phone;

    aget-object v7, v7, v10

    invoke-interface {v7}, Lcom/android/internal/telephony/Phone;->getDeviceSvn()Ljava/lang/String;

    move-result-object v6

    .line 173
    .restart local v6    # "svStr2":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 184
    .end local v1    # "imeiStr2":Ljava/lang/String;
    .end local v4    # "simSlotNum":I
    .end local v6    # "svStr2":Ljava/lang/String;
    :cond_8
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_a

    .line 185
    iget-boolean v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->CTC_CGG:Z

    if-eqz v7, :cond_9

    .line 186
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v8, "IMEI"

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/modemui/activities/ShowIMEI$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI$3;-><init>(Lcom/sec/android/app/modemui/activities/ShowIMEI;)V

    invoke-virtual {v7, v11, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_2

    .line 195
    :cond_9
    const-string v7, "phone"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 196
    .local v3, "mtelephonyManager2":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3, v10}, Landroid/telephony/TelephonyManager;->getDeviceId(I)Ljava/lang/String;

    move-result-object v1

    .line 197
    .restart local v1    # "imeiStr2":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v6

    .line 198
    .restart local v6    # "svStr2":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, "imeiSum":Ljava/lang/String;
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v8, "IMEI"

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/modemui/activities/ShowIMEI$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI$4;-><init>(Lcom/sec/android/app/modemui/activities/ShowIMEI;)V

    invoke-virtual {v7, v11, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_2

    .line 210
    .end local v1    # "imeiStr2":Ljava/lang/String;
    .end local v2    # "imeiSum":Ljava/lang/String;
    .end local v3    # "mtelephonyManager2":Landroid/telephony/TelephonyManager;
    .end local v6    # "svStr2":Ljava/lang/String;
    :cond_a
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v8, "IMEI"

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/modemui/activities/ShowIMEI$5;

    invoke-direct {v8, p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI$5;-><init>(Lcom/sec/android/app/modemui/activities/ShowIMEI;)V

    invoke-virtual {v7, v11, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mOkPopup:Landroid/app/AlertDialog;

    .line 216
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mOkPopup:Landroid/app/AlertDialog;

    invoke-virtual {v7, v9}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 217
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mOkPopup:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 218
    iget-object v7, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->mOkPopup:Landroid/app/AlertDialog;

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_2
.end method

.method private showMEIDPanel()V
    .locals 4

    .prologue
    .line 222
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "meidStr":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "MEID"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/app/modemui/activities/ShowIMEI$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI$6;-><init>(Lcom/sec/android/app/modemui/activities/ShowIMEI;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 230
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const-string v0, "ShowIMEI"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->setContentView(I)V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->showIMEI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    const-string v0, "ShowIMEI"

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not supported on showIMEI."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ShowIMEI;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not supported on showIMEI."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/modemui/activities/ShowIMEI;->finish()V

    .line 48
    :cond_0
    return-void
.end method
