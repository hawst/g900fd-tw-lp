.class public Lcom/sec/android/app/modemui/activities/PhoneUtil;
.super Landroid/app/Activity;
.source "PhoneUtil.java"


# instance fields
.field private final AP_PDA_SELECTION:[B

.field private final CP_MODEM_SELECTION:[B

.field private MODEL:Ljava/lang/String;

.field private mSaveResetButton:Landroid/widget/Button;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUartToAp:Z

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbToAp:Z

.field private final modem:[B

.field private final pda:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->MODEL:Ljava/lang/String;

    .line 77
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->AP_PDA_SELECTION:[B

    .line 80
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->pda:[B

    .line 83
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->modem:[B

    .line 86
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->CP_MODEM_SELECTION:[B

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbToAp:Z

    return-void

    .line 77
    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 80
    nop

    :array_1
    .array-data 1
        0x50t
        0x44t
        0x41t
        0x0t
    .end array-data

    .line 83
    :array_2
    .array-data 1
        0x4dt
        0x4ft
        0x44t
        0x45t
        0x4dt
        0x0t
    .end array-data

    .line 86
    nop

    :array_3
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUart(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/modemui/activities/PhoneUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartToAp:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUsb(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbToAp:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbToAp:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/modemui/activities/PhoneUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->isFactoryAppAPO()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/modemui/activities/PhoneUtil;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->changeUartPath(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/modemui/activities/PhoneUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->doRebootNSave(I)V

    return-void
.end method

.method private changeUart(Ljava/lang/String;)V
    .locals 3
    .param p1, "uart"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    const-string v0, "PDA"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    const-string v0, "PhoneUtil"

    const-string v1, "changeUart"

    const-string v2, "UART to PDA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "AP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string v0, "PhoneUtil"

    const-string v1, "changeUart"

    const-string v2, "UART to MODEM"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v0, "/sys/class/sec/switch/uart_sel"

    const-string v1, "CP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private changeUartPath(Ljava/lang/String;)V
    .locals 5
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 261
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 262
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory.aporiented.athandler.AtUartswit.SetUartPath"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    const-string v1, "PATH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->sendBroadcast(Landroid/content/Intent;)V

    .line 265
    const-string v1, "PhoneUtil"

    const-string v2, "changeUartPath"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send Broadcast ,extra: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method private changeUsb(Ljava/lang/String;)V
    .locals 4
    .param p1, "usb"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    const/4 v0, 0x1

    .line 250
    .local v0, "apUsb":Z
    const-string v1, "PDA"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    const-string v1, "PhoneUtil"

    const-string v2, "changeUsb"

    const-string v3, "USB to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v1, "/sys/class/sec/switch/usb_sel"

    const-string v2, "PDA"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    const-string v1, "MODEM"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    const-string v1, "PhoneUtil"

    const-string v2, "changeUsb"

    const-string v3, "USB to MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v1, "/sys/class/sec/switch/usb_sel"

    const-string v2, "MODEM"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 198
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "PhoneUtil"

    const-string v3, "doRebootNSave"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchsel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 201
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method private initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V
    .locals 6
    .param p1, "uart"    # Landroid/widget/RadioGroup;
    .param p2, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    .line 212
    const-string v2, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "currentUart":Ljava/lang/String;
    const-string v2, "/sys/class/sec/switch/usb_sel"

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214
    .local v1, "currentUsb":Ljava/lang/String;
    const-string v2, "PhoneUtil"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UART_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v2, "PhoneUtil"

    const-string v3, "initialSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "USB_Init : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v2, "CP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 218
    const-string v2, "PhoneUtil"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : MODEM"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const v2, 0x7f090024

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 225
    :cond_0
    :goto_0
    const-string v2, "MODEM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 226
    const-string v2, "PhoneUtil"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : MODEM"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const v2, 0x7f090028

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 232
    :cond_1
    :goto_1
    return-void

    .line 220
    :cond_2
    const-string v2, "AP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    const-string v2, "PhoneUtil"

    const-string v3, "initialSetting"

    const-string v4, "Initial UART_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const v2, 0x7f090025

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 228
    :cond_3
    const-string v2, "PDA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    const-string v2, "PhoneUtil"

    const-string v3, "initialSetting"

    const-string v4, "Initial USB_STATUS1 : PDA"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const v2, 0x7f090029

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method private isFactoryAppAPO()Z
    .locals 9

    .prologue
    .line 303
    const/4 v4, 0x0

    .line 304
    .local v4, "isAPO":Z
    const/4 v2, 0x0

    .line 307
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v5, "system/bin/at_distributor"

    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v4, 0x1

    .line 309
    :try_start_1
    const-string v5, "PhoneUtil"

    const-string v6, "isFactoryAppAPO"

    const-string v7, "Found the at_distributor. Decided APO mode."

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 318
    if-eqz v3, :cond_2

    .line 320
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 327
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :cond_0
    :goto_0
    return v4

    .line 321
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "PhoneUtil"

    const-string v6, "isFactoryAppAPO"

    const-string v7, "File Close error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 323
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 311
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 312
    .local v1, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v4, 0x0

    .line 313
    :try_start_3
    const-string v5, "PhoneUtil"

    const-string v6, "isFactoryAppAPO"

    const-string v7, "Can not find at_distributor. Decided CPO mode"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 318
    if-eqz v2, :cond_0

    .line 320
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 321
    :catch_2
    move-exception v0

    .line 322
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "PhoneUtil"

    const-string v6, "isFactoryAppAPO"

    const-string v7, "File Close error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 315
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 316
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v5, "PhoneUtil"

    const-string v6, "isFactoryAppAPO"

    const-string v7, "Can not decide because of error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 318
    if-eqz v2, :cond_0

    .line 320
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 321
    :catch_4
    move-exception v0

    .line 322
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "PhoneUtil"

    const-string v6, "isFactoryAppAPO"

    const-string v7, "File Close error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 318
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v2, :cond_1

    .line 320
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 323
    :cond_1
    :goto_4
    throw v5

    .line 321
    :catch_5
    move-exception v0

    .line 322
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v6, "PhoneUtil"

    const-string v7, "isFactoryAppAPO"

    const-string v8, "File Close error"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 318
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 315
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 311
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_2
    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_0
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 331
    const-string v6, ""

    .line 332
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 333
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 336
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 339
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 340
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 350
    :cond_0
    if-eqz v5, :cond_1

    .line 351
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 354
    :cond_1
    if-eqz v1, :cond_2

    .line 355
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 363
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 364
    const-string v7, ""

    .line 366
    :goto_1
    return-object v7

    .line 357
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 358
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 361
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 342
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 343
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "PhoneUtil"

    const-string v8, "readOneLine"

    const-string v9, "FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 350
    if-eqz v4, :cond_4

    .line 351
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 354
    :cond_4
    if-eqz v0, :cond_3

    .line 355
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 357
    :catch_2
    move-exception v2

    .line 358
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "PhoneUtil"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 345
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 346
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "PhoneUtil"

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 350
    if-eqz v4, :cond_5

    .line 351
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 354
    :cond_5
    if-eqz v0, :cond_3

    .line 355
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 357
    :catch_4
    move-exception v2

    .line 358
    const-string v7, "PhoneUtil"

    const-string v8, "readOneLine"

    const-string v9, "IOException close()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 349
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 350
    :goto_4
    if-eqz v4, :cond_6

    .line 351
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 354
    :cond_6
    if-eqz v0, :cond_7

    .line 355
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 360
    :cond_7
    :goto_5
    throw v7

    .line 357
    :catch_5
    move-exception v2

    .line 358
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "PhoneUtil"

    const-string v9, "readOneLine"

    const-string v10, "IOException close()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 366
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 349
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 345
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 342
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 371
    const/4 v1, 0x0

    .line 374
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 379
    const-string v3, "PhoneUtil"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    if-eqz v2, :cond_2

    .line 383
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 389
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 384
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "PhoneUtil"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 386
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 376
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 377
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "PhoneUtil"

    const-string v4, "writeFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOExceptionfilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 379
    const-string v3, "PhoneUtil"

    const-string v4, "writeFile"

    const-string v5, "Write Success!"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    if-eqz v1, :cond_0

    .line 383
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 384
    :catch_2
    move-exception v0

    .line 385
    const-string v3, "PhoneUtil"

    const-string v4, "writeFile"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 379
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "PhoneUtil"

    const-string v5, "writeFile"

    const-string v6, "Write Success!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    if-eqz v1, :cond_1

    .line 383
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 386
    :cond_1
    :goto_3
    throw v3

    .line 384
    :catch_3
    move-exception v0

    .line 385
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "PhoneUtil"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 379
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 376
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    const v0, 0x7f030011

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->setContentView(I)V

    .line 99
    const-string v0, "PhoneUtil"

    const-string v1, "onCreate"

    const-string v2, "Hello oncreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 136
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 174
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mSaveResetButton:Landroid/widget/Button;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mSaveResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil$3;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUartRadioGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil;->initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V

    .line 195
    return-void
.end method
