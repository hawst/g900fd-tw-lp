.class Lcom/sec/android/app/servicemodeapp/SysDump$7;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;)V
    .locals 0

    .prologue
    .line 1932
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 58
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1934
    const-string v53, "ro.product.model"

    const-string v54, "Unknown"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 1935
    .local v43, "product":Ljava/lang/String;
    const-string v53, "SGH-I537"

    move-object/from16 v0, v43

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v53

    if-eqz v53, :cond_0

    .line 1936
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const v54, 0x1d4c0

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->cp_timout:I
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2702(Lcom/sec/android/app/servicemodeapp/SysDump;I)I

    .line 1938
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_1

    .line 1939
    const-string v53, "ril.modem.board"

    const-string v54, "Unknown"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 1977
    .local v19, "cpChip":Ljava/lang/String;
    const-string v53, "XMM"

    move-object/from16 v0, v19

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v53

    if-eqz v53, :cond_a

    .line 1978
    const-string v53, "SysDump"

    const-string v54, "Silence saving : start now! "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1979
    const/16 v53, 0x2

    move/from16 v0, v53

    new-array v10, v0, [B

    fill-array-data v10, :array_0

    .line 1982
    .local v10, "SILENTLOG_ON":[B
    const/16 v53, 0x2

    move/from16 v0, v53

    new-array v9, v0, [B

    fill-array-data v9, :array_1

    .line 1985
    .local v9, "SILENTLOG_OFF":[B
    const-string v48, "sys/devices/virtual/misc/umts_dm0/dm_state"

    .line 1986
    .local v48, "sysFs":Ljava/lang/String;
    const-string v53, "dev.silentlog.on"

    invoke-static/range {v53 .. v53}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v46

    .line 1988
    .local v46, "silentlogging":Ljava/lang/String;
    const-string v53, "On"

    move-object/from16 v0, v46

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-eq v0, v1, :cond_9

    .line 1989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Silent Log : On"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "rm /data/slog/*"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->DoShellCmd(Ljava/lang/String;)I

    .line 1991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const/16 v54, 0x1

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog(Z)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V

    .line 1992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    move-object/from16 v1, v48

    invoke-virtual {v0, v1, v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 1993
    const-string v53, "SysDump"

    const-string v54, "Send Silent LOG true."

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1994
    const-string v53, "dev.silentlog.on"

    const-string v54, "On"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063
    .end local v9    # "SILENTLOG_OFF":[B
    .end local v10    # "SILENTLOG_ON":[B
    .end local v19    # "cpChip":Ljava/lang/String;
    .end local v46    # "silentlogging":Ljava/lang/String;
    .end local v48    # "sysFs":Ljava/lang/String;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCreateSilentLogfile:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_2

    .line 2064
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->createSilentLogFile()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3200(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 2067
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3300(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_3

    .line 2069
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "LOW_BATTERY_DUMP"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v34

    .line 2072
    .local v34, "lb_dump":I
    if-nez v34, :cond_f

    .line 2073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "LOW_BATTERY_DUMP"

    const/16 v55, 0x1

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2075
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "LOW_BATTERY_DUMP_COUNT"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2077
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3300(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Low battery dump : On"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2088
    .end local v34    # "lb_dump":I
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_4

    .line 2090
    const/16 v53, 0x2

    :try_start_1
    move/from16 v0, v53

    new-array v14, v0, [B

    fill-array-data v14, :array_2

    .line 2093
    .local v14, "WAKE_ON":[B
    const/16 v53, 0x2

    move/from16 v0, v53

    new-array v13, v0, [B

    fill-array-data v13, :array_3

    .line 2096
    .local v13, "WAKE_OFF":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "WAKE"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v52

    .line 2099
    .local v52, "wake":I
    if-nez v52, :cond_10

    .line 2100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "WAKE"

    const/16 v55, 0x1

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/sys/module/qpnp_power_on/parameters/wake_enabled"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v14}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 2103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Reset without Wakeup : On"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2115
    .end local v13    # "WAKE_OFF":[B
    .end local v14    # "WAKE_ON":[B
    .end local v52    # "wake":I
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_5

    .line 2117
    const/16 v53, 0x2

    :try_start_2
    move/from16 v0, v53

    new-array v5, v0, [B

    fill-array-data v5, :array_4

    .line 2120
    .local v5, "GPIODUMP_ON":[B
    const/16 v53, 0x2

    move/from16 v0, v53

    new-array v4, v0, [B

    fill-array-data v4, :array_5

    .line 2123
    .local v4, "GPIODUMP_OFF":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "GPIO_DUMP"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v28

    .line 2126
    .local v28, "gpio_dump":I
    if-nez v28, :cond_11

    .line 2127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "GPIO_DUMP"

    const/16 v55, 0x1

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/sys/module/lpm_levels/parameters/secdebug"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 2130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "GPIO Dump : On"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2142
    .end local v4    # "GPIODUMP_OFF":[B
    .end local v5    # "GPIODUMP_ON":[B
    .end local v28    # "gpio_dump":I
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_6

    .line 2144
    const/16 v53, 0x2

    :try_start_3
    move/from16 v0, v53

    new-array v8, v0, [B

    fill-array-data v8, :array_6

    .line 2147
    .local v8, "PMICINTDEBUG_ON":[B
    const/16 v53, 0x2

    move/from16 v0, v53

    new-array v7, v0, [B

    fill-array-data v7, :array_7

    .line 2150
    .local v7, "PMICINTDEBUG_OFF":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "PMIC_INT_DEBUG"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v42

    .line 2153
    .local v42, "pmic_int_debug":I
    if-nez v42, :cond_12

    .line 2154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "PMIC_INT_DEBUG"

    const/16 v55, 0x1

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/sys/module/qpnp_int/parameters/debug_mask"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 2157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "PMIC interrupt source print : On"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 2169
    .end local v7    # "PMICINTDEBUG_OFF":[B
    .end local v8    # "PMICINTDEBUG_ON":[B
    .end local v42    # "pmic_int_debug":I
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_7

    .line 2186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->checkForNoAuthorityAndNotEngBuild()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3700(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-eqz v53, :cond_13

    .line 2187
    const-string v53, "SysDump"

    const-string v54, "TCPDUMP in user "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showOTPAlertDialogForAuth()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3800(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 2189
    const-string v53, "SysDump"

    const-string v54, "alertOTP show "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2285
    :cond_7
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyLuckyRilLogToSdcard:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_1c

    .line 2286
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->ExternalSDcardMounted()Z

    move-result v53

    if-nez v53, :cond_1a

    .line 2287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "External SD Card UnMounted!!"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 3016
    :cond_8
    :goto_6
    return-void

    .line 1996
    .restart local v9    # "SILENTLOG_OFF":[B
    .restart local v10    # "SILENTLOG_ON":[B
    .restart local v19    # "cpChip":Ljava/lang/String;
    .restart local v46    # "silentlogging":Ljava/lang/String;
    .restart local v48    # "sysFs":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Silent Log : Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1997
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const/16 v54, 0x0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog(Z)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V

    .line 1998
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    move-object/from16 v1, v48

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 1999
    const-string v53, "SysDump"

    const-string v54, "Send Silent LOG false."

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2000
    const-string v53, "dev.silentlog.on"

    const-string v54, "Off"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2001
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "chmod 664 /data/slog/*"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->DoShellCmd(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2003
    .end local v9    # "SILENTLOG_OFF":[B
    .end local v10    # "SILENTLOG_ON":[B
    .end local v46    # "silentlogging":Ljava/lang/String;
    .end local v48    # "sysFs":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNDUOS()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2900(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-eqz v53, :cond_c

    .line 2004
    const-string v53, "dev.silentlog.on"

    invoke-static/range {v53 .. v53}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v46

    .line 2006
    .restart local v46    # "silentlogging":Ljava/lang/String;
    const-string v53, "On"

    move-object/from16 v0, v46

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-eq v0, v1, :cond_b

    .line 2007
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Silent Log : On"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const/16 v54, 0x1

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog(Z)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V

    .line 2009
    const-string v53, "SysDump"

    const-string v54, "Send Silent LOG true."

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2010
    const-string v53, "dev.silentlog.on"

    const-string v54, "On"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2012
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mSilentLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Silent Log : Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const/16 v54, 0x0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog(Z)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V

    .line 2014
    const-string v53, "SysDump"

    const-string v54, "Send Silent LOG false."

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2015
    const-string v53, "dev.silentlog.on"

    const-string v54, "Off"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2019
    .end local v46    # "silentlogging":Ljava/lang/String;
    :cond_c
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v53

    if-eqz v53, :cond_d

    .line 2020
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, ""

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v36

    .line 2021
    .local v36, "mToast":Landroid/widget/Toast;
    const-string v53, "It is Sub user mode.\nPlease Turn on/off SilentLog in Owner mode"

    move-object/from16 v0, v36

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 2022
    const/16 v53, 0x11

    const/16 v54, 0x0

    const/16 v55, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v53

    move/from16 v2, v54

    move/from16 v3, v55

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 2023
    invoke-virtual/range {v36 .. v36}, Landroid/widget/Toast;->show()V

    goto/16 :goto_6

    .line 2028
    .end local v36    # "mToast":Landroid/widget/Toast;
    :cond_d
    const-string v53, "dev.silentlog.on"

    invoke-static/range {v53 .. v53}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v46

    .line 2030
    .restart local v46    # "silentlogging":Ljava/lang/String;
    const-string v53, "On"

    move-object/from16 v0, v53

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v53

    if-eqz v53, :cond_e

    .line 2031
    const-string v53, "SysDump"

    const-string v54, "Silence saving : start now! "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2032
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 2033
    .local v12, "SvcIntent":Landroid/content/Intent;
    const-string v53, "com.sec.modem.settings"

    const-string v54, "com.sec.modem.settings.cplogging.SilentLogService"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2035
    const-string v53, "action"

    const/16 v54, 0x2

    move-object/from16 v0, v53

    move/from16 v1, v54

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2036
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-virtual {v0, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2037
    const-string v53, "SysDump"

    const-string v54, "progress dialog show"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2040
    new-instance v51, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$1;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$1;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v51

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2051
    .local v51, "thread":Ljava/lang/Thread;
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 2053
    .end local v12    # "SvcIntent":Landroid/content/Intent;
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->chooseSilentLog()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3000(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 2054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->isCHNTRModel()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$700(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-eqz v53, :cond_1

    .line 2055
    const-string v53, "SysDump"

    const-string v54, "Send Silent Log CP2"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const/16 v54, 0x1

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData_Silentlog_CP2(Z)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$800(Lcom/sec/android/app/servicemodeapp/SysDump;Z)V

    goto/16 :goto_0

    .line 2079
    .end local v19    # "cpChip":Ljava/lang/String;
    .end local v46    # "silentlogging":Ljava/lang/String;
    .restart local v34    # "lb_dump":I
    :cond_f
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "LOW_BATTERY_DUMP"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2081
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLBDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3300(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Low battery dump : Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 2083
    .end local v34    # "lb_dump":I
    :catch_0
    move-exception v25

    .line 2084
    .local v25, "e":Ljava/lang/Exception;
    const-string v53, "SysDump"

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "exceptoin : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2105
    .end local v25    # "e":Ljava/lang/Exception;
    .restart local v13    # "WAKE_OFF":[B
    .restart local v14    # "WAKE_ON":[B
    .restart local v52    # "wake":I
    :cond_10
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "WAKE"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/sys/module/qpnp_power_on/parameters/wake_enabled"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v13}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 2108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mWake:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "Reset without Wakeup : Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_2

    .line 2110
    .end local v13    # "WAKE_OFF":[B
    .end local v14    # "WAKE_ON":[B
    .end local v52    # "wake":I
    :catch_1
    move-exception v25

    .line 2111
    .restart local v25    # "e":Ljava/lang/Exception;
    const-string v53, "SysDump"

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "exceptoin : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2132
    .end local v25    # "e":Ljava/lang/Exception;
    .restart local v4    # "GPIODUMP_OFF":[B
    .restart local v5    # "GPIODUMP_ON":[B
    .restart local v28    # "gpio_dump":I
    :cond_11
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "GPIO_DUMP"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/sys/module/lpm_levels/parameters/secdebug"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 2135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mGPIODump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "GPIO Dump : Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_3

    .line 2137
    .end local v4    # "GPIODUMP_OFF":[B
    .end local v5    # "GPIODUMP_ON":[B
    .end local v28    # "gpio_dump":I
    :catch_2
    move-exception v25

    .line 2138
    .restart local v25    # "e":Ljava/lang/Exception;
    const-string v53, "SysDump"

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "exceptoin : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2159
    .end local v25    # "e":Ljava/lang/Exception;
    .restart local v7    # "PMICINTDEBUG_OFF":[B
    .restart local v8    # "PMICINTDEBUG_ON":[B
    .restart local v42    # "pmic_int_debug":I
    :cond_12
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getApplicationContext()Landroid/content/Context;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v53

    const-string v54, "PMIC_INT_DEBUG"

    const/16 v55, 0x0

    invoke-static/range {v53 .. v55}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/sys/module/qpnp_int/parameters/debug_mask"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->setSysfsFile(Ljava/lang/String;[B)V

    .line 2162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mPMICIntDebug:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "PMIC interrupt source print : Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_4

    .line 2164
    .end local v7    # "PMICINTDEBUG_OFF":[B
    .end local v8    # "PMICINTDEBUG_ON":[B
    .end local v42    # "pmic_int_debug":I
    :catch_3
    move-exception v25

    .line 2165
    .restart local v25    # "e":Ljava/lang/Exception;
    const-string v53, "SysDump"

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "exceptoin : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 2191
    .end local v25    # "e":Ljava/lang/Exception;
    :cond_13
    const-string v53, "SysDump"

    const-string v54, "TCPDUMP in eng "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v53

    invoke-interface/range {v53 .. v53}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, "START"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v53

    if-eqz v53, :cond_17

    .line 2194
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2197
    .local v6, "InterfaceNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_8
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v26

    .local v26, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_14
    :goto_7
    if-eqz v26, :cond_15

    invoke-interface/range {v26 .. v26}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v53

    if-eqz v53, :cond_15

    .line 2198
    if-eqz v26, :cond_14

    .line 2199
    invoke-interface/range {v26 .. v26}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/net/NetworkInterface;

    .line 2200
    .local v31, "intf":Ljava/net/NetworkInterface;
    invoke-virtual/range {v31 .. v31}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v53

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_7

    .line 2203
    .end local v26    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v31    # "intf":Ljava/net/NetworkInterface;
    :catch_4
    move-exception v27

    .line 2204
    .local v27, "ex":Ljava/lang/Exception;
    const-string v53, "SysDump"

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2207
    .end local v27    # "ex":Ljava/lang/Exception;
    :cond_15
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v53

    add-int/lit8 v53, v53, 0x1

    move/from16 v0, v53

    new-array v11, v0, [Ljava/lang/String;

    .line 2208
    .local v11, "StrInterfaceNames":[Ljava/lang/String;
    const/16 v53, 0x0

    const-string v54, "any"

    aput-object v54, v11, v53

    .line 2210
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v53

    move/from16 v0, v29

    move/from16 v1, v53

    if-ge v0, v1, :cond_16

    .line 2211
    add-int/lit8 v54, v29, 0x1

    move/from16 v0, v29

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v53

    check-cast v53, Ljava/lang/String;

    aput-object v53, v11, v54

    .line 2210
    add-int/lit8 v29, v29, 0x1

    goto :goto_8

    .line 2214
    :cond_16
    new-instance v16, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2215
    .local v16, "alert":Landroid/app/AlertDialog$Builder;
    new-instance v30, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2216
    .local v30, "input":Landroid/widget/EditText;
    move-object/from16 v0, v16

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2217
    const/16 v53, -0x1

    new-instance v54, Lcom/sec/android/app/servicemodeapp/SysDump$7$2;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2, v11}, Lcom/sec/android/app/servicemodeapp/SysDump$7$2;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;Landroid/widget/EditText;[Ljava/lang/String;)V

    move-object/from16 v0, v16

    move/from16 v1, v53

    move-object/from16 v2, v54

    invoke-virtual {v0, v11, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2225
    const-string v53, "OK "

    new-instance v54, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;Landroid/widget/EditText;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2254
    const-string v53, "Cancel"

    new-instance v54, Lcom/sec/android/app/servicemodeapp/SysDump$7$4;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$4;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2259
    invoke-virtual/range {v16 .. v16}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_5

    .line 2260
    .end local v6    # "InterfaceNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v11    # "StrInterfaceNames":[Ljava/lang/String;
    .end local v16    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v29    # "i":I
    .end local v30    # "input":Landroid/widget/EditText;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v53

    invoke-interface/range {v53 .. v53}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, "STOP"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v53

    if-eqz v53, :cond_7

    .line 2261
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->wifiOnly:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3900()Z

    move-result v53

    if-nez v53, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->isKOR()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-eqz v53, :cond_19

    .line 2262
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const/16 v54, 0x0

    const/16 v55, 0x0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->startStopWiFiTcpdump(ZLjava/lang/String;)V
    invoke-static/range {v53 .. v55}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4100(Lcom/sec/android/app/servicemodeapp/SysDump;ZLjava/lang/String;)V

    .line 2279
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "TCP DUMP OK."

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 2280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mTcpDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "TCP DUMP START"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 2264
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0x16

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2265
    new-instance v49, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$5;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$5;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v49

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2277
    .local v49, "tcpDumpThread":Ljava/lang/Thread;
    invoke-virtual/range {v49 .. v49}, Ljava/lang/Thread;->start()V

    goto :goto_9

    .line 2291
    .end local v49    # "tcpDumpThread":Ljava/lang/Thread;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/data/log/lucky_ril.log"

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4302(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "/mnt/sdcard/lucky_ril_"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v55, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getTimeToString()Ljava/lang/String;
    invoke-static/range {v55 .. v55}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4500(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    const-string v55, ".log"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4300(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v55, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static/range {v55 .. v55}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v55

    const-string v56, "main"

    invoke-virtual/range {v53 .. v56}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v53

    if-eqz v53, :cond_1b

    .line 2296
    :try_start_9
    new-instance v53, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4300(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v54

    invoke-direct/range {v53 .. v54}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v53 .. v53}, Ljava/io/File;->delete()Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    .line 2301
    :cond_1b
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Copy RIL log Success!"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 2303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0xd

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2306
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mStartRilLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_1d

    .line 2307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0xc

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Start RIL log !"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 2312
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mDeleteDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4700(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_1e

    .line 2313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/data/log"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->deleteDirectory(Ljava/lang/String;)V

    .line 2314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/tombstones/mdm"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->deleteDirectory(Ljava/lang/String;)V

    .line 2315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Delete Dump !"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 2319
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyDump:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_1f

    .line 2320
    const-string v53, "SysDump"

    const-string v54, "--select - run dumpstate/logcat"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2321
    const-string v53, "SysDump"

    const-string v54, "progress dialog show"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0x9

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2336
    new-instance v51, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v51

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2388
    .restart local v51    # "thread":Ljava/lang/Thread;
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->start()V

    .line 2390
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_1f
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v53

    const v54, 0x7f090066

    move/from16 v0, v53

    move/from16 v1, v54

    if-ne v0, v1, :cond_20

    .line 2391
    const-string v53, "SysDump"

    const-string v54, "Launch GoTrace..."

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2392
    new-instance v33, Landroid/content/Intent;

    const-string v53, "com.sec.android.gotrace.MAIN"

    move-object/from16 v0, v33

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2393
    .local v33, "launchIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v40

    .line 2395
    .local v40, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v53, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v33

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v15

    .line 2396
    .local v15, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v53

    if-lez v53, :cond_27

    const/16 v32, 0x1

    .line 2399
    .local v32, "isIntentSafe":Z
    :goto_b
    if-eqz v32, :cond_20

    .line 2400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->startActivity(Landroid/content/Intent;)V

    .line 2403
    .end local v15    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v32    # "isIntentSafe":Z
    .end local v33    # "launchIntent":Landroid/content/Intent;
    .end local v40    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_21

    .line 2404
    const-string v53, "SysDump"

    const-string v54, "v == mMdlog"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    invoke-virtual/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->checkMdlogProperty()Z

    move-result v53

    if-eqz v53, :cond_28

    .line 2407
    const-string v53, "sys.mdlogproperty"

    const-string v54, "Off"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2408
    new-instance v37, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$7;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$7;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v37

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2414
    .local v37, "mdlog_thread":Ljava/lang/Thread;
    new-instance v35, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$8;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$8;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2420
    .local v35, "logcat_thread":Ljava/lang/Thread;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2421
    const-string v53, "SysDump"

    const-string v54, "killing diag_mdlog"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2422
    invoke-virtual/range {v37 .. v37}, Ljava/lang/Thread;->start()V

    .line 2424
    const-string v53, "SysDump"

    const-string v54, "killing log_logcat"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2425
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Thread;->start()V

    .line 2427
    const-string v53, "SysDump"

    const-string v54, "killing klog"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2428
    const-string v53, "ctl.start"

    const-string v54, "killklog"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mMdlog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "mdlog klog: Off"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2431
    new-instance v51, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$9;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$9;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v51

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2441
    .restart local v51    # "thread":Ljava/lang/Thread;
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->start()V

    .line 2449
    .end local v35    # "logcat_thread":Ljava/lang/Thread;
    .end local v37    # "mdlog_thread":Ljava/lang/Thread;
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_21
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mAPCPLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_24

    .line 2460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2466
    const-string v53, "SysDump"

    const-string v54, "Silence saving : save now! "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2467
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 2468
    .restart local v12    # "SvcIntent":Landroid/content/Intent;
    const-string v53, "com.sec.modem.settings"

    const-string v54, "com.sec.modem.settings.cplogging.SilentLogService"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2470
    const-string v53, "action"

    const/16 v54, 0x3

    move-object/from16 v0, v53

    move/from16 v1, v54

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-virtual {v0, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0x9

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2473
    new-instance v51, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$10;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$10;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v51

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2535
    .restart local v51    # "thread":Ljava/lang/Thread;
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->start()V

    .line 2537
    new-instance v22, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$11;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2565
    .local v22, "cpLogThread":Ljava/lang/Thread;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Thread;->start()V

    .line 2567
    const-string v53, "ril.tcpdumping"

    invoke-static/range {v53 .. v53}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v50

    .line 2568
    .local v50, "tcpdumping":Ljava/lang/String;
    const-string v53, "On"

    move-object/from16 v0, v50

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-eq v0, v1, :cond_22

    .line 2569
    new-instance v49, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$12;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$12;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v49

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2586
    .restart local v49    # "tcpDumpThread":Ljava/lang/Thread;
    invoke-virtual/range {v49 .. v49}, Ljava/lang/Thread;->start()V

    .line 2589
    .end local v49    # "tcpDumpThread":Ljava/lang/Thread;
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    new-instance v54, Ljava/util/Timer;

    invoke-direct/range {v54 .. v54}, Ljava/util/Timer;-><init>()V

    move-object/from16 v0, v54

    move-object/from16 v1, v53

    iput-object v0, v1, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPTimoutTimer:Ljava/util/Timer;

    .line 2590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump;->mCPTimoutTimer:Ljava/util/Timer;

    move-object/from16 v53, v0

    new-instance v54, Lcom/sec/android/app/servicemodeapp/SysDump$7$13;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$13;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v55, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->cp_timout:I
    invoke-static/range {v55 .. v55}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2700(Lcom/sec/android/app/servicemodeapp/SysDump;)I

    move-result v55

    move/from16 v0, v55

    int-to-long v0, v0

    move-wide/from16 v56, v0

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    move-wide/from16 v2, v56

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2599
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isMarvell:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5600()Z

    move-result v53

    if-eqz v53, :cond_24

    .line 2600
    new-instance v20, Ljava/io/File;

    const-string v53, "data/com_DDR_RW.bin"

    move-object/from16 v0, v20

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2601
    .local v20, "cpCrashDumpFile":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v53

    if-eqz v53, :cond_29

    .line 2602
    const-string v53, "SysDump"

    const-string v54, "com_DDR_RW.bin file is exist"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2604
    new-instance v45, Ljava/io/File;

    const-string v53, "data/log/NVM/"

    move-object/from16 v0, v45

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2605
    .local v45, "sdcardLogNVM":Ljava/io/File;
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->exists()Z

    move-result v53

    if-nez v53, :cond_23

    .line 2606
    const-string v53, "SysDump"

    const-string v54, "SDCARD/log/NVM is not exist"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2607
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->mkdir()Z

    .line 2610
    :cond_23
    new-instance v21, Ljava/io/File;

    const-string v53, "data/log/NVM/com_DDR_RW.bin"

    move-object/from16 v0, v21

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2611
    .local v21, "cpDumpFileSdcard":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2621
    .end local v12    # "SvcIntent":Landroid/content/Intent;
    .end local v20    # "cpCrashDumpFile":Ljava/io/File;
    .end local v21    # "cpDumpFileSdcard":Ljava/io/File;
    .end local v22    # "cpLogThread":Ljava/lang/Thread;
    .end local v45    # "sdcardLogNVM":Ljava/io/File;
    .end local v50    # "tcpdumping":Ljava/lang/String;
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_24
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mModemLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5700(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_25

    .line 2622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2624
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isMarvell:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5600()Z

    move-result v53

    if-eqz v53, :cond_2a

    .line 2625
    new-instance v29, Landroid/content/Intent;

    invoke-direct/range {v29 .. v29}, Landroid/content/Intent;-><init>()V

    .line 2626
    .local v29, "i":Landroid/content/Intent;
    const-string v53, "com.marvell.logtools"

    const-string v54, "com.marvell.logtools.logSettings.LogToolsMain"

    move-object/from16 v0, v29

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2627
    const/high16 v53, 0x10000000

    move-object/from16 v0, v29

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->startActivity(Landroid/content/Intent;)V

    .line 2684
    .end local v29    # "i":Landroid/content/Intent;
    :cond_25
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCPForceCrash:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_26

    .line 2686
    new-instance v17, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2687
    .local v17, "alert_builder":Landroid/app/AlertDialog$Builder;
    const v53, 0x7f0700ac

    move-object/from16 v0, v17

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const/16 v54, 0x0

    invoke-virtual/range {v53 .. v54}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const v54, 0x1040013

    new-instance v55, Lcom/sec/android/app/servicemodeapp/SysDump$7$16;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$16;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    invoke-virtual/range {v53 .. v55}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const v54, 0x1040009

    new-instance v55, Lcom/sec/android/app/servicemodeapp/SysDump$7$15;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$15;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    invoke-virtual/range {v53 .. v55}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2722
    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v18

    .line 2723
    .local v18, "alert_dialog":Landroid/app/AlertDialog;
    const v53, 0x7f0700ab

    move-object/from16 v0, v18

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 2724
    invoke-virtual/range {v18 .. v18}, Landroid/app/AlertDialog;->show()V

    .line 2728
    .end local v17    # "alert_builder":Landroid/app/AlertDialog$Builder;
    .end local v18    # "alert_dialog":Landroid/app/AlertDialog;
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyKernelLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_2e

    .line 2729
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->ExternalSDcardMounted()Z

    move-result v53

    if-nez v53, :cond_2c

    .line 2730
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "External SD Card UnMounted!!"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2396
    .restart local v15    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v33    # "launchIntent":Landroid/content/Intent;
    .restart local v40    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_27
    const/16 v32, 0x0

    goto/16 :goto_b

    .line 2444
    .end local v15    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v33    # "launchIntent":Landroid/content/Intent;
    .end local v40    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_28
    const-string v53, "SysDump"

    const-string v54, "Starting diag mdlog option activity "

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->chooseDiagMdlogOption()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5300(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    goto/16 :goto_c

    .line 2613
    .restart local v12    # "SvcIntent":Landroid/content/Intent;
    .restart local v20    # "cpCrashDumpFile":Ljava/io/File;
    .restart local v22    # "cpLogThread":Ljava/lang/Thread;
    .restart local v50    # "tcpdumping":Ljava/lang/String;
    .restart local v51    # "thread":Ljava/lang/Thread;
    :cond_29
    const-string v53, "SysDump"

    const-string v54, "com_DDR_RW.bin file is not exist"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_d

    .line 2630
    .end local v12    # "SvcIntent":Landroid/content/Intent;
    .end local v20    # "cpCrashDumpFile":Ljava/io/File;
    .end local v22    # "cpLogThread":Ljava/lang/Thread;
    .end local v50    # "tcpdumping":Ljava/lang/String;
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->isU8420()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5800(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-eqz v53, :cond_2b

    .line 2631
    new-instance v51, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$14;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$14;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    move-object/from16 v0, v51

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2678
    .restart local v51    # "thread":Ljava/lang/Thread;
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->start()V

    goto/16 :goto_e

    .line 2680
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0x12

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    goto/16 :goto_e

    .line 2734
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/data/panic"

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_panic:Ljava/lang/String;
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6202(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2735
    new-instance v38, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_panic:Ljava/lang/String;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v38

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2736
    .local v38, "oFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0x4

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2738
    invoke-virtual/range {v38 .. v38}, Ljava/io/File;->exists()Z

    move-result v53

    if-eqz v53, :cond_2d

    .line 2739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/mnt/sdcard/panic"

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile_panic:Ljava/lang/String;
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6302(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2740
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile_panic:Ljava/lang/String;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v55, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static/range {v55 .. v55}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v55

    const-string v56, "panic"

    invoke-virtual/range {v53 .. v56}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2743
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/data/anr/traces.txt"

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4302(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "/mnt/sdcard/traces.txt"

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2745
    new-instance v39, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4300(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v39

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2747
    .local v39, "oFile_kernel":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v53

    if-eqz v53, :cond_30

    .line 2748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->inFile:Ljava/lang/String;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4300(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v55, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static/range {v55 .. v55}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v55

    const-string v56, "traces.txt"

    invoke-virtual/range {v53 .. v56}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Copy success!!"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 2755
    .end local v38    # "oFile":Ljava/io/File;
    .end local v39    # "oFile_kernel":Ljava/io/File;
    :cond_2e
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToSdcard:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6400(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_2f

    .line 2756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->checkCopyToSdcard()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2600(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 2759
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCopyToExternal:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_32

    .line 2765
    new-instance v44, Ljava/io/File;

    const-string v53, "/storage/extSdCard/"

    move-object/from16 v0, v44

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2766
    .local v44, "sdcard":Ljava/io/File;
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->canWrite()Z

    move-result v53

    if-nez v53, :cond_31

    .line 2767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "External SD Card UnMounted!!\nplease check SD card is not inserted"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2751
    .end local v44    # "sdcard":Ljava/io/File;
    .restart local v38    # "oFile":Ljava/io/File;
    .restart local v39    # "oFile_kernel":Ljava/io/File;
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Kernel Log Not Exist!!"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto :goto_f

    .line 2773
    .end local v38    # "oFile":Ljava/io/File;
    .end local v39    # "oFile_kernel":Ljava/io/File;
    .restart local v44    # "sdcard":Ljava/io/File;
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2775
    new-instance v51, Ljava/lang/Thread;

    new-instance v53, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;

    move-object/from16 v0, v53

    move-object/from16 v1, p0

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;Ljava/io/File;)V

    move-object/from16 v0, v51

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2848
    .restart local v51    # "thread":Ljava/lang/Thread;
    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->start()V

    .line 2851
    .end local v44    # "sdcard":Ljava/io/File;
    .end local v51    # "thread":Ljava/lang/Thread;
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mExit:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6700(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_33

    .line 2852
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->checkAndFinishSysDump()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6800(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 2855
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_35

    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-eq v0, v1, :cond_35

    .line 2856
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isBroadcom:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7000()Z

    move-result v53

    if-eqz v53, :cond_34

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5500()Z

    move-result v53

    if-nez v53, :cond_34

    .line 2858
    const-string v53, "persist.brcm.log"

    invoke-static/range {v53 .. v53}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    .line 2859
    .local v24, "currentLogging":Ljava/lang/String;
    const-string v53, "SysDump"

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "Current MTT Logging setting : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2860
    const-string v53, "sdcard"

    move-object/from16 v0, v24

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-ne v0, v1, :cond_3a

    .line 2861
    const-string v53, "SysDump"

    const-string v54, "Change Logging setting to none!!"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2862
    const-string v53, "persist.brcm.log"

    const-string v54, "none"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "MTT Logging Setting : OFF"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2871
    .end local v24    # "currentLogging":Ljava/lang/String;
    :cond_34
    :goto_10
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isEOS2:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5500()Z

    move-result v53

    if-eqz v53, :cond_35

    .line 2873
    const-string v53, "sys.trace.control"

    const-string v54, "OFF"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    .line 2874
    .restart local v24    # "currentLogging":Ljava/lang/String;
    const-string v53, "SysDump"

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "Current MTA Logging setting : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2875
    const-string v53, "path=off"

    move-object/from16 v0, v53

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-ne v0, v1, :cond_3b

    .line 2876
    const-string v53, "SysDump"

    const-string v54, "Change Logging setting to sdcard!!"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2877
    const-string v53, "sys.trace.control"

    const-string v54, "path=nvm"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "MTA Logging Setting : SDCARD"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2894
    .end local v24    # "currentLogging":Ljava/lang/String;
    :cond_35
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleRampdumpState:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_36

    .line 2898
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->displayDialog()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7100(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    .line 2913
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleSecLog:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7200(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_37

    .line 2914
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getSwitchableLogProperty()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7300(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-nez v53, :cond_3e

    .line 2917
    new-instance v17, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2918
    .restart local v17    # "alert_builder":Landroid/app/AlertDialog$Builder;
    const v53, 0x7f07009c

    move-object/from16 v0, v17

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const/16 v54, 0x0

    invoke-virtual/range {v53 .. v54}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const v54, 0x1040013

    new-instance v55, Lcom/sec/android/app/servicemodeapp/SysDump$7$19;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$19;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    invoke-virtual/range {v53 .. v55}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const v54, 0x1040009

    new-instance v55, Lcom/sec/android/app/servicemodeapp/SysDump$7$18;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$18;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    invoke-virtual/range {v53 .. v55}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2934
    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v18

    .line 2935
    .restart local v18    # "alert_dialog":Landroid/app/AlertDialog;
    const v53, 0x7f07009d

    move-object/from16 v0, v18

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 2936
    invoke-virtual/range {v18 .. v18}, Landroid/app/AlertDialog;->show()V

    .line 2944
    .end local v17    # "alert_builder":Landroid/app/AlertDialog$Builder;
    .end local v18    # "alert_dialog":Landroid/app/AlertDialog;
    :cond_37
    :goto_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleCPPopupUIState:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2100(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_38

    .line 2945
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-eq v0, v1, :cond_38

    .line 2946
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v54, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mOem:Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;
    invoke-static/range {v54 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1000(Lcom/sec/android/app/servicemodeapp/SysDump;)Lcom/sec/android/app/servicemodeapp/SysDump$OemCommands;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v54, 0x1b

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->SendData(I)V
    invoke-static/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1100(Lcom/sec/android/app/servicemodeapp/SysDump;I)V

    .line 2968
    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mToggleTranslationAssistant:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7500(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_39

    .line 2969
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getTranslationAssistantProperty()Z
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7600(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v53

    if-nez v53, :cond_3f

    .line 2973
    new-instance v17, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v53

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2975
    .restart local v17    # "alert_builder":Landroid/app/AlertDialog$Builder;
    const v53, 0x7f070090

    move-object/from16 v0, v17

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const/16 v54, 0x0

    invoke-virtual/range {v53 .. v54}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const v54, 0x104000a

    new-instance v55, Lcom/sec/android/app/servicemodeapp/SysDump$7$22;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$22;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    invoke-virtual/range {v53 .. v55}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v53

    const v54, 0x1040009

    new-instance v55, Lcom/sec/android/app/servicemodeapp/SysDump$7$21;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/servicemodeapp/SysDump$7$21;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V

    invoke-virtual/range {v53 .. v55}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2993
    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v18

    .line 2994
    .restart local v18    # "alert_dialog":Landroid/app/AlertDialog;
    const v53, 0x7f070091

    move-object/from16 v0, v18

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 2995
    invoke-virtual/range {v18 .. v18}, Landroid/app/AlertDialog;->show()V

    .line 3002
    .end local v17    # "alert_builder":Landroid/app/AlertDialog$Builder;
    .end local v18    # "alert_dialog":Landroid/app/AlertDialog;
    :cond_39
    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    if-ne v0, v1, :cond_8

    .line 3004
    const-string v53, "persist.sys.cpboot"

    invoke-static/range {v53 .. v53}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v53 .. v53}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    .line 3005
    .local v23, "cpboot":Ljava/lang/String;
    const-string v47, "CP Booting"

    .line 3006
    .local v47, "state":Ljava/lang/String;
    const-string v53, "disable"

    move-object/from16 v0, v53

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v53

    if-eqz v53, :cond_40

    .line 3007
    const-string v53, "persist.sys.cpboot"

    const-string v54, "enable"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 3008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v54

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    const-string v55, ": Enable"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3013
    :goto_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    const-string v54, "power"

    invoke-virtual/range {v53 .. v54}, Lcom/sec/android/app/servicemodeapp/SysDump;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Landroid/os/PowerManager;

    .line 3014
    .local v41, "pm":Landroid/os/PowerManager;
    move-object/from16 v0, v41

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2865
    .end local v23    # "cpboot":Ljava/lang/String;
    .end local v41    # "pm":Landroid/os/PowerManager;
    .end local v47    # "state":Ljava/lang/String;
    .restart local v24    # "currentLogging":Ljava/lang/String;
    :cond_3a
    const-string v53, "SysDump"

    const-string v54, "Change Logging setting to sdcard!!"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2866
    const-string v53, "persist.brcm.log"

    const-string v54, "sdcard"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2867
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "MTT Logging Setting : ON"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    .line 2879
    :cond_3b
    const-string v53, "path=nvm"

    move-object/from16 v0, v53

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-ne v0, v1, :cond_3c

    .line 2880
    const-string v53, "SysDump"

    const-string v54, "Change Logging setting to usb!!"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2881
    const-string v53, "sys.trace.control"

    const-string v54, "path=usb"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "MTA Logging Setting : USB"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11

    .line 2883
    :cond_3c
    const-string v53, "path=usb"

    move-object/from16 v0, v53

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    const/16 v54, 0x1

    move/from16 v0, v53

    move/from16 v1, v54

    if-ne v0, v1, :cond_3d

    .line 2884
    const-string v53, "SysDump"

    const-string v54, "Change Logging setting to off!!"

    invoke-static/range {v53 .. v54}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2885
    const-string v53, "sys.trace.control"

    const-string v54, "path=off"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2886
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    const-string v54, "MTA Logging Setting : OFF"

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11

    .line 2888
    :cond_3d
    const-string v53, "sys.trace.control"

    const-string v54, "path=off"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2889
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mLoggingSetting:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "MTA Logging Setting : "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11

    .line 2939
    .end local v24    # "currentLogging":Ljava/lang/String;
    :cond_3e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->toggleSwitchableLogProperty()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7400(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    goto/16 :goto_12

    .line 2998
    :cond_3f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->toggleTranslationAssistantProperty()V
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7700(Lcom/sec/android/app/servicemodeapp/SysDump;)V

    goto/16 :goto_13

    .line 3010
    .restart local v23    # "cpboot":Ljava/lang/String;
    .restart local v47    # "state":Ljava/lang/String;
    :cond_40
    const-string v53, "persist.sys.cpboot"

    const-string v54, "disable"

    invoke-static/range {v53 .. v54}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 3011
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    move-object/from16 v53, v0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCpBoot:Landroid/widget/Button;
    invoke-static/range {v53 .. v53}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7800(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/widget/Button;

    move-result-object v53

    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v54

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    const-string v55, ": Disable"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-virtual/range {v53 .. v54}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_14

    .line 2297
    .end local v23    # "cpboot":Ljava/lang/String;
    .end local v47    # "state":Ljava/lang/String;
    :catch_5
    move-exception v53

    goto/16 :goto_a

    .line 1979
    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 1982
    nop

    :array_1
    .array-data 1
        0x30t
        0x0t
    .end array-data

    .line 2090
    nop

    :array_2
    .array-data 1
        0x4et
        0x0t
    .end array-data

    .line 2093
    nop

    :array_3
    .array-data 1
        0x59t
        0x0t
    .end array-data

    .line 2117
    nop

    :array_4
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 2120
    nop

    :array_5
    .array-data 1
        0x30t
        0x0t
    .end array-data

    .line 2144
    nop

    :array_6
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 2147
    nop

    :array_7
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method
