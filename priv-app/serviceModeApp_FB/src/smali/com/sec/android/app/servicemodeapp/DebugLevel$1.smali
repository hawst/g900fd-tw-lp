.class Lcom/sec/android/app/servicemodeapp/DebugLevel$1;
.super Ljava/lang/Object;
.source "DebugLevel.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/DebugLevel;->chooseDebugLevel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/DebugLevel;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/DebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "item"    # I

    .prologue
    .line 58
    packed-switch p2, :pswitch_data_0

    .line 70
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;

    # invokes: Lcom/sec/android/app/servicemodeapp/DebugLevel;->rebootDevice()V
    invoke-static {v0}, Lcom/sec/android/app/servicemodeapp/DebugLevel;->access$100(Lcom/sec/android/app/servicemodeapp/DebugLevel;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;

    invoke-virtual {v0}, Lcom/sec/android/app/servicemodeapp/DebugLevel;->finish()V

    .line 73
    return-void

    .line 60
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;

    const-string v1, "/proc/dump_enable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/servicemodeapp/DebugLevel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/DebugLevel;->access$000(Lcom/sec/android/app/servicemodeapp/DebugLevel;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;

    const-string v1, "/proc/dump_enable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/servicemodeapp/DebugLevel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/DebugLevel;->access$000(Lcom/sec/android/app/servicemodeapp/DebugLevel;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/DebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/DebugLevel;

    const-string v1, "/proc/dump_enable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/servicemodeapp/DebugLevel;->writeFile(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/DebugLevel;->access$000(Lcom/sec/android/app/servicemodeapp/DebugLevel;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
