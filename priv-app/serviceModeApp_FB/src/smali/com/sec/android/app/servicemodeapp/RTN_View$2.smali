.class Lcom/sec/android/app/servicemodeapp/RTN_View$2;
.super Ljava/lang/Object;
.source "RTN_View.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/RTN_View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/RTN_View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/RTN_View;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/RTN_View$2;->this$0:Lcom/sec/android/app/servicemodeapp/RTN_View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 101
    # getter for: Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/RTN_View;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View$2;->this$0:Lcom/sec/android/app/servicemodeapp/RTN_View;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/servicemodeapp/RTN_View;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/RTN_View;->access$202(Lcom/sec/android/app/servicemodeapp/RTN_View;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View$2;->this$0:Lcom/sec/android/app/servicemodeapp/RTN_View;

    const/16 v1, 0xb

    const/16 v2, 0xa

    # invokes: Lcom/sec/android/app/servicemodeapp/RTN_View;->getOemData(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/servicemodeapp/RTN_View;->access$300(Lcom/sec/android/app/servicemodeapp/RTN_View;II)V

    .line 105
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 108
    # getter for: Lcom/sec/android/app/servicemodeapp/RTN_View;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/RTN_View;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/RTN_View$2;->this$0:Lcom/sec/android/app/servicemodeapp/RTN_View;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/servicemodeapp/RTN_View;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/servicemodeapp/RTN_View;->access$202(Lcom/sec/android/app/servicemodeapp/RTN_View;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 110
    return-void
.end method
