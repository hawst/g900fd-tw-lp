.class Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;
.super Ljava/lang/Object;
.source "PhoneUtil_ESC.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 196
    const/4 v0, -0x1

    .line 199
    .local v0, "parameter":I
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUART:B
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)B

    move-result v1

    shl-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;

    # getter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->mUSB:B
    invoke-static {v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->access$300(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;)B

    move-result v2

    or-int v0, v1, v2

    .line 200
    const-string v1, "PhoneUtil_ESC"

    const-string v2, "onClick"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parameter: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC$3;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->doRebootNSave(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;->access$400(Lcom/sec/android/app/modemui/activities/PhoneUtil_ESC;I)V

    .line 202
    return-void
.end method
