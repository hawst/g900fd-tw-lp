.class public Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;
.super Landroid/app/Activity;
.source "PhoneUtil_Prevail2SPR.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDMRadioGroup:Landroid/widget/RadioGroup;

.field public mHandler:Landroid/os/Handler;

.field private phone:Lcom/android/internal/telephony/Phone;

.field private rb_uart:Landroid/widget/RadioButton;

.field private rb_usb:Landroid/widget/RadioButton;

.field updated:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->phone:Lcom/android/internal/telephony/Phone;

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->updated:Z

    .line 97
    new-instance v0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$1;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)V

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_uart:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_usb:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->phone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method


# virtual methods
.method public displayError()V
    .locals 2

    .prologue
    .line 140
    const-string v0, "Nothing updated!"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 141
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 145
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 146
    const-string v4, "PhoneUtil"

    const-string v5, "onCreate"

    const-string v6, "onCreate"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iput-object p0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->mContext:Landroid/content/Context;

    .line 149
    const v4, 0x7f030013

    invoke-virtual {p0, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->setContentView(I)V

    .line 150
    const v4, 0x7f09002e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioGroup;

    iput-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->mDMRadioGroup:Landroid/widget/RadioGroup;

    .line 151
    const v4, 0x7f090024

    invoke-virtual {p0, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_uart:Landroid/widget/RadioButton;

    .line 152
    const v4, 0x7f09002f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->rb_usb:Landroid/widget/RadioButton;

    .line 153
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->updated:Z

    .line 155
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->phone:Lcom/android/internal/telephony/Phone;

    .line 156
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 157
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 158
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x4

    .line 161
    .local v3, "fileSize":I
    :try_start_0
    const-string v4, "PhoneUtil"

    const-string v5, "onCreate"

    const-string v6, "writeByte"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const/16 v4, 0xc

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 163
    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 164
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    if-eqz v1, :cond_0

    .line 172
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 179
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->phone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x3f2

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;->mDMRadioGroup:Landroid/widget/RadioGroup;

    new-instance v5, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR$2;-><init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_Prevail2SPR;)V

    invoke-virtual {v4, v5}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 226
    :cond_1
    :goto_1
    return-void

    .line 173
    :catch_0
    move-exception v2

    .line 174
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 166
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 167
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "PhoneUtil"

    const-string v5, "onCreate"

    const-string v6, " failed to write"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170
    if-eqz v1, :cond_1

    .line 172
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 173
    :catch_2
    move-exception v2

    .line 174
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 170
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_2

    .line 172
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 175
    :cond_2
    :goto_2
    throw v4

    .line 173
    :catch_3
    move-exception v2

    .line 174
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 230
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 231
    return-void
.end method
