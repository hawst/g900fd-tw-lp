.class Lcom/sec/android/app/modemui/activities/ModemReset$1;
.super Landroid/os/Handler;
.source "ModemReset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/ModemReset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/ModemReset;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/ModemReset;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x2

    .line 82
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 84
    :pswitch_0
    const-string v1, "ModemReset"

    const-string v2, "handleMessage, modem CP1 reset done"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v1, "ModemReset"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage, productModel= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    iget-object v3, v3, Lcom/sec/android/app/modemui/activities/ModemReset;->productModel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const-string v1, "SHV-E210L"

    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    iget-object v2, v2, Lcom/sec/android/app/modemui/activities/ModemReset;->productModel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    const-string v1, "ModemReset"

    const-string v2, "handleMessage, modem VIA reset done"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v1, 0x70

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 90
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    # getter for: Lcom/sec/android/app/modemui/activities/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/ModemReset;->access$000(Lcom/sec/android/app/modemui/activities/ModemReset;)Lcom/samsung/android/sec_platform_library/FactoryPhone;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    iget-object v3, v3, Lcom/sec/android/app/modemui/activities/ModemReset;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3f1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 98
    .end local v0    # "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    # invokes: Lcom/sec/android/app/modemui/activities/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/ModemReset;->access$100(Lcom/sec/android/app/modemui/activities/ModemReset;)V

    goto :goto_0

    .line 103
    :pswitch_1
    const-string v1, "ModemReset"

    const-string v2, "handleMessage, modem CP2 reset done"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    # operator++ for: Lcom/sec/android/app/modemui/activities/ModemReset;->modemRstDone:I
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/ModemReset;->access$208(Lcom/sec/android/app/modemui/activities/ModemReset;)I

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    # getter for: Lcom/sec/android/app/modemui/activities/ModemReset;->modemRstDone:I
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/ModemReset;->access$200(Lcom/sec/android/app/modemui/activities/ModemReset;)I

    move-result v1

    if-ne v4, v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    # invokes: Lcom/sec/android/app/modemui/activities/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/ModemReset;->access$100(Lcom/sec/android/app/modemui/activities/ModemReset;)V

    goto :goto_0

    .line 109
    :pswitch_2
    const-string v1, "ModemReset"

    const-string v2, "handleMessage, modem CMC reset done"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/ModemReset$1;->this$0:Lcom/sec/android/app/modemui/activities/ModemReset;

    # invokes: Lcom/sec/android/app/modemui/activities/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/android/app/modemui/activities/ModemReset;->access$100(Lcom/sec/android/app/modemui/activities/ModemReset;)V

    goto/16 :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x3f0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
