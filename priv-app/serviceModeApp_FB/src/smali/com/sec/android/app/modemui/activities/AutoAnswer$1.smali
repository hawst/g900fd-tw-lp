.class Lcom/sec/android/app/modemui/activities/AutoAnswer$1;
.super Ljava/lang/Object;
.source "AutoAnswer.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/AutoAnswer;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/AutoAnswer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/AutoAnswer;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;->this$0:Lcom/sec/android/app/modemui/activities/AutoAnswer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 52
    const v2, 0x7f090002

    if-ne p2, v2, :cond_1

    .line 54
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;->this$0:Lcom/sec/android/app/modemui/activities/AutoAnswer;

    invoke-virtual {v2}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "autoanswering_without_device"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;->this$0:Lcom/sec/android/app/modemui/activities/AutoAnswer;

    invoke-virtual {v2}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "autoanswering_without_device"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 57
    .local v0, "currentStatus":I
    const-string v2, "AutoAnswer"

    const-string v3, "onCreate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AutoAnswer Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v0    # "currentStatus":I
    :cond_0
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const v2, 0x7f090003

    if-ne p2, v2, :cond_0

    .line 63
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;->this$0:Lcom/sec/android/app/modemui/activities/AutoAnswer;

    invoke-virtual {v2}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "autoanswering_without_device"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/modemui/activities/AutoAnswer$1;->this$0:Lcom/sec/android/app/modemui/activities/AutoAnswer;

    invoke-virtual {v2}, Lcom/sec/android/app/modemui/activities/AutoAnswer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "autoanswering_without_device"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 66
    .restart local v0    # "currentStatus":I
    const-string v2, "AutoAnswer"

    const-string v3, "onCreate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AutoAnswer Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 67
    .end local v0    # "currentStatus":I
    :catch_1
    move-exception v1

    .line 68
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
