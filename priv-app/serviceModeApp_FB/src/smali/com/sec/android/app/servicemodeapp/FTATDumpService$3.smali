.class Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;
.super Ljava/lang/Object;
.source "FTATDumpService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/FTATDumpService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

.field final synthetic val$dumpFileName:Ljava/lang/String;

.field final synthetic val$hasCP:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iput-boolean p2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->val$hasCP:Z

    iput-object p3, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->val$dumpFileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 199
    iget-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->val$hasCP:Z

    if-eqz v2, :cond_3

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # getter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->isEOS2:Z
    invoke-static {v2}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$600(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 201
    const-string v2, "sys.trace.control"

    const-string v3, "path=dump"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iget-object v2, v2, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 218
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v3, "bugreport > /data/log/Bugreport.log"

    # invokes: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->DoShellCmd(Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$900(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 219
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/log/Bugreport.log"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    .local v1, "rawFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->val$dumpFileName:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    .local v0, "dstFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    const-string v2, "FTATDumpService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand(), delete : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->val$dumpFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 226
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    const-string v2, "FTATDumpService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand(), rename : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->val$dumpFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iget-object v2, v2, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 234
    .end local v0    # "dstFile":Ljava/io/File;
    .end local v1    # "rawFile":Ljava/io/File;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # invokes: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->getCopyLog()V
    invoke-static {v2}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$1000(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V

    .line 235
    return-void

    .line 211
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    # invokes: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->getModemLogIPC()[B
    invoke-static {v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$700(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iget-object v4, v4, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x3f6

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    # invokes: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->sendMessage([BLandroid/os/Message;)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$800(Lcom/sec/android/app/servicemodeapp/FTATDumpService;[BLandroid/os/Message;)V

    goto/16 :goto_0

    .line 214
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->access$102(Lcom/sec/android/app/servicemodeapp/FTATDumpService;Z)Z

    .line 215
    const-string v2, "FTATDumpService"

    const-string v3, "onStartCommand(), No CP"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 231
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$3;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iget-object v2, v2, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
