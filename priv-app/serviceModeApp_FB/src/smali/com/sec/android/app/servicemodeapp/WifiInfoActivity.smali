.class public Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;
.super Landroid/app/Activity;
.source "WifiInfoActivity.java"


# static fields
.field private static mCodingrate:Ljava/lang/String;

.field private static mModulation:Ljava/lang/String;

.field private static mPowerSaveMode:Ljava/lang/String;

.field private static mTxPower:Ljava/lang/String;

.field private static mbgn:Ljava/lang/String;

.field private static mwSec:Ljava/lang/String;


# instance fields
.field private mIsPause:Z

.field private mIsPowerSaveChanged:Z

.field mNetworkInfo:Landroid/net/NetworkInfo;

.field mPingButtonHandler:Landroid/view/View$OnClickListener;

.field private mPingDestIP:Landroid/widget/EditText;

.field private mPingIpAddrResult:Landroid/widget/TextView;

.field private mPowerSaveModeBtn:Landroid/widget/ToggleButton;

.field private mPowerSavemodeOnClick:Landroid/view/View$OnClickListener;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field protected mTimerHandler:Landroid/os/Handler;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mstrPingIpAddrResult:Ljava/lang/String;

.field private pingTestButton:Landroid/widget/Button;

.field private final test_off:[B

.field private final test_on:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    .line 54
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    .line 55
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    .line 56
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    .line 57
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mwSec:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    new-array v0, v3, [B

    const/16 v1, 0x30

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->test_on:[B

    .line 40
    new-array v0, v3, [B

    const/16 v1, 0x31

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->test_off:[B

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPause:Z

    .line 64
    new-instance v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$1;-><init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 89
    new-instance v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$2;-><init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSavemodeOnClick:Landroid/view/View$OnClickListener;

    .line 130
    new-instance v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$3;-><init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingButtonHandler:Landroid/view/View$OnClickListener;

    .line 267
    new-instance v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$6;-><init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTimerHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updateWifiInfo()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->changePowerSaveMode()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updatePingState()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingIpAddrResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->pingIpAddr()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPause:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method private changePowerSaveMode()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    new-instance v1, Ljava/io/FileOutputStream;

    const-string v2, "/data/.psm.info"

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 102
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveModeBtn:Landroid/widget/ToggleButton;

    invoke-virtual {v2}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    const-string v2, "WiFiDebugScreen"

    const-string v3, "Button Checked Powersave On"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->test_off:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 108
    const-string v2, "WiFi reset to apply psm on"

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 110
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 128
    :goto_1
    return-void

    .line 114
    :cond_2
    :try_start_1
    const-string v2, "WiFiDebugScreen"

    const-string v3, "Button Not Checked Powersave off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->test_on:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 117
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPowerSaveChanged:Z

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 119
    const-string v2, "WiFi reset to apply psm off"

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 121
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v2
.end method

.method private getChannel(ILjava/lang/String;)I
    .locals 8
    .param p1, "frequency"    # I
    .param p2, "capabilities"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/16 v6, 0x2b

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 618
    new-array v0, v6, [[I

    new-array v3, v5, [I

    fill-array-data v3, :array_0

    aput-object v3, v0, v2

    new-array v3, v5, [I

    fill-array-data v3, :array_1

    aput-object v3, v0, v7

    new-array v3, v5, [I

    fill-array-data v3, :array_2

    aput-object v3, v0, v5

    const/4 v3, 0x3

    new-array v4, v5, [I

    fill-array-data v4, :array_3

    aput-object v4, v0, v3

    const/4 v3, 0x4

    new-array v4, v5, [I

    fill-array-data v4, :array_4

    aput-object v4, v0, v3

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_5

    aput-object v4, v0, v3

    const/4 v3, 0x6

    new-array v4, v5, [I

    fill-array-data v4, :array_6

    aput-object v4, v0, v3

    const/4 v3, 0x7

    new-array v4, v5, [I

    fill-array-data v4, :array_7

    aput-object v4, v0, v3

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_8

    aput-object v4, v0, v3

    const/16 v3, 0x9

    new-array v4, v5, [I

    fill-array-data v4, :array_9

    aput-object v4, v0, v3

    const/16 v3, 0xa

    new-array v4, v5, [I

    fill-array-data v4, :array_a

    aput-object v4, v0, v3

    const/16 v3, 0xb

    new-array v4, v5, [I

    fill-array-data v4, :array_b

    aput-object v4, v0, v3

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_c

    aput-object v4, v0, v3

    const/16 v3, 0xd

    new-array v4, v5, [I

    fill-array-data v4, :array_d

    aput-object v4, v0, v3

    const/16 v3, 0xe

    new-array v4, v5, [I

    fill-array-data v4, :array_e

    aput-object v4, v0, v3

    const/16 v3, 0xf

    new-array v4, v5, [I

    fill-array-data v4, :array_f

    aput-object v4, v0, v3

    const/16 v3, 0x10

    new-array v4, v5, [I

    fill-array-data v4, :array_10

    aput-object v4, v0, v3

    const/16 v3, 0x11

    new-array v4, v5, [I

    fill-array-data v4, :array_11

    aput-object v4, v0, v3

    const/16 v3, 0x12

    new-array v4, v5, [I

    fill-array-data v4, :array_12

    aput-object v4, v0, v3

    const/16 v3, 0x13

    new-array v4, v5, [I

    fill-array-data v4, :array_13

    aput-object v4, v0, v3

    const/16 v3, 0x14

    new-array v4, v5, [I

    fill-array-data v4, :array_14

    aput-object v4, v0, v3

    const/16 v3, 0x15

    new-array v4, v5, [I

    fill-array-data v4, :array_15

    aput-object v4, v0, v3

    const/16 v3, 0x16

    new-array v4, v5, [I

    fill-array-data v4, :array_16

    aput-object v4, v0, v3

    const/16 v3, 0x17

    new-array v4, v5, [I

    fill-array-data v4, :array_17

    aput-object v4, v0, v3

    const/16 v3, 0x18

    new-array v4, v5, [I

    fill-array-data v4, :array_18

    aput-object v4, v0, v3

    const/16 v3, 0x19

    new-array v4, v5, [I

    fill-array-data v4, :array_19

    aput-object v4, v0, v3

    const/16 v3, 0x1a

    new-array v4, v5, [I

    fill-array-data v4, :array_1a

    aput-object v4, v0, v3

    const/16 v3, 0x1b

    new-array v4, v5, [I

    fill-array-data v4, :array_1b

    aput-object v4, v0, v3

    const/16 v3, 0x1c

    new-array v4, v5, [I

    fill-array-data v4, :array_1c

    aput-object v4, v0, v3

    const/16 v3, 0x1d

    new-array v4, v5, [I

    fill-array-data v4, :array_1d

    aput-object v4, v0, v3

    const/16 v3, 0x1e

    new-array v4, v5, [I

    fill-array-data v4, :array_1e

    aput-object v4, v0, v3

    const/16 v3, 0x1f

    new-array v4, v5, [I

    fill-array-data v4, :array_1f

    aput-object v4, v0, v3

    const/16 v3, 0x20

    new-array v4, v5, [I

    fill-array-data v4, :array_20

    aput-object v4, v0, v3

    const/16 v3, 0x21

    new-array v4, v5, [I

    fill-array-data v4, :array_21

    aput-object v4, v0, v3

    const/16 v3, 0x22

    new-array v4, v5, [I

    fill-array-data v4, :array_22

    aput-object v4, v0, v3

    const/16 v3, 0x23

    new-array v4, v5, [I

    fill-array-data v4, :array_23

    aput-object v4, v0, v3

    const/16 v3, 0x24

    new-array v4, v5, [I

    fill-array-data v4, :array_24

    aput-object v4, v0, v3

    const/16 v3, 0x25

    new-array v4, v5, [I

    fill-array-data v4, :array_25

    aput-object v4, v0, v3

    const/16 v3, 0x26

    new-array v4, v5, [I

    fill-array-data v4, :array_26

    aput-object v4, v0, v3

    const/16 v3, 0x27

    new-array v4, v5, [I

    fill-array-data v4, :array_27

    aput-object v4, v0, v3

    const/16 v3, 0x28

    new-array v4, v5, [I

    fill-array-data v4, :array_28

    aput-object v4, v0, v3

    const/16 v3, 0x29

    new-array v4, v5, [I

    fill-array-data v4, :array_29

    aput-object v4, v0, v3

    const/16 v3, 0x2a

    new-array v4, v5, [I

    fill-array-data v4, :array_2a

    aput-object v4, v0, v3

    .line 633
    .local v0, "channel":[[I
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 634
    aget-object v3, v0, v1

    aget v3, v3, v7

    if-ne p1, v3, :cond_2

    .line 635
    sput-object p2, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mwSec:Ljava/lang/String;

    .line 639
    :cond_0
    if-ge v1, v6, :cond_1

    .line 640
    aget-object v3, v0, v1

    aget v2, v3, v2

    .line 642
    :cond_1
    return v2

    .line 633
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 618
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x96c
    .end array-data

    :array_2
    .array-data 4
        0x2
        0x971
    .end array-data

    :array_3
    .array-data 4
        0x3
        0x976
    .end array-data

    :array_4
    .array-data 4
        0x4
        0x97b
    .end array-data

    :array_5
    .array-data 4
        0x5
        0x980
    .end array-data

    :array_6
    .array-data 4
        0x6
        0x985
    .end array-data

    :array_7
    .array-data 4
        0x7
        0x98a
    .end array-data

    :array_8
    .array-data 4
        0x8
        0x98f
    .end array-data

    :array_9
    .array-data 4
        0x9
        0x994
    .end array-data

    :array_a
    .array-data 4
        0xa
        0x999
    .end array-data

    :array_b
    .array-data 4
        0xb
        0x99e
    .end array-data

    :array_c
    .array-data 4
        0xc
        0x9a3
    .end array-data

    :array_d
    .array-data 4
        0xd
        0x9a8
    .end array-data

    :array_e
    .array-data 4
        0xe
        0x9b4
    .end array-data

    :array_f
    .array-data 4
        0x22
        0x1432
    .end array-data

    :array_10
    .array-data 4
        0x24
        0x143c
    .end array-data

    :array_11
    .array-data 4
        0x26
        0x1446
    .end array-data

    :array_12
    .array-data 4
        0x28
        0x1450
    .end array-data

    :array_13
    .array-data 4
        0x2a
        0x145a
    .end array-data

    :array_14
    .array-data 4
        0x2c
        0x1464
    .end array-data

    :array_15
    .array-data 4
        0x2e
        0x146e
    .end array-data

    :array_16
    .array-data 4
        0x30
        0x1478
    .end array-data

    :array_17
    .array-data 4
        0x34
        0x148c
    .end array-data

    :array_18
    .array-data 4
        0x38
        0x14a0
    .end array-data

    :array_19
    .array-data 4
        0x3c
        0x14b4
    .end array-data

    :array_1a
    .array-data 4
        0x40
        0x14c8
    .end array-data

    :array_1b
    .array-data 4
        0x64
        0x157c
    .end array-data

    :array_1c
    .array-data 4
        0x68
        0x1590
    .end array-data

    :array_1d
    .array-data 4
        0x6c
        0x15a4
    .end array-data

    :array_1e
    .array-data 4
        0x70
        0x15b8
    .end array-data

    :array_1f
    .array-data 4
        0x74
        0x15cc
    .end array-data

    :array_20
    .array-data 4
        0x78
        0x15e0
    .end array-data

    :array_21
    .array-data 4
        0x7c
        0x15f4
    .end array-data

    :array_22
    .array-data 4
        0x80
        0x1608
    .end array-data

    :array_23
    .array-data 4
        0x84
        0x161c
    .end array-data

    :array_24
    .array-data 4
        0x88
        0x1630
    .end array-data

    :array_25
    .array-data 4
        0x8c
        0x1644
    .end array-data

    :array_26
    .array-data 4
        0x95
        0x1671
    .end array-data

    :array_27
    .array-data 4
        0x99
        0x1685
    .end array-data

    :array_28
    .array-data 4
        0x9d
        0x1699
    .end array-data

    :array_29
    .array-data 4
        0xa1
        0x16ad
    .end array-data

    :array_2a
    .array-data 4
        0xa5
        0x16c1
    .end array-data
.end method

.method private getNetworkInfofromLinkSpeed(ZI)V
    .locals 1
    .param p1, "Connected"    # Z
    .param p2, "LinkSpeed"    # I

    .prologue
    .line 287
    if-eqz p1, :cond_0

    .line 288
    sparse-switch p2, :sswitch_data_0

    .line 607
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    .line 615
    :goto_0
    return-void

    .line 291
    :sswitch_0
    const-string v0, "DBPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11b"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "17dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 293
    :sswitch_1
    const-string v0, "DQPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11b"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "17dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 295
    :sswitch_2
    const-string v0, "CCK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11b"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "17dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 297
    :sswitch_3
    const-string v0, "CCK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11b"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "17dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 301
    :sswitch_4
    const-string v0, "BPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 303
    :sswitch_5
    const-string v0, "BPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 305
    :sswitch_6
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto :goto_0

    .line 307
    :sswitch_7
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 309
    :sswitch_8
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 311
    :sswitch_9
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 313
    :sswitch_a
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 315
    :sswitch_b
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 321
    :sswitch_c
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 323
    :sswitch_d
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 325
    :sswitch_e
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 327
    :sswitch_f
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 329
    :sswitch_10
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 331
    :sswitch_11
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 333
    :sswitch_12
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 335
    :sswitch_13
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "13dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 339
    :sswitch_14
    const-string v0, "BPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 341
    :sswitch_15
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 343
    :sswitch_16
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 345
    :sswitch_17
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 347
    :sswitch_18
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 349
    :sswitch_19
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 353
    :sswitch_1a
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 355
    :sswitch_1b
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "13dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 361
    :sswitch_1c
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 363
    :sswitch_1d
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 367
    :sswitch_1e
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 369
    :sswitch_1f
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 371
    :sswitch_20
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 375
    :sswitch_21
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 382
    :sswitch_22
    const-string v0, "BPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 384
    :sswitch_23
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 386
    :sswitch_24
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 388
    :sswitch_25
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 390
    :sswitch_26
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 392
    :sswitch_27
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 394
    :sswitch_28
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 396
    :sswitch_29
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 398
    :sswitch_2a
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 400
    :sswitch_2b
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 404
    :sswitch_2c
    const-string v0, "BPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 408
    :sswitch_2d
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 410
    :sswitch_2e
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 412
    :sswitch_2f
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 414
    :sswitch_30
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 416
    :sswitch_31
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 420
    :sswitch_32
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 427
    :sswitch_33
    const-string v0, "BPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 431
    :sswitch_34
    const-string v0, "QPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 433
    :sswitch_35
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "1/2"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 435
    :sswitch_36
    const-string v0, "16-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 437
    :sswitch_37
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 439
    :sswitch_38
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 441
    :sswitch_39
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 443
    :sswitch_3a
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 445
    :sswitch_3b
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 452
    :sswitch_3c
    const-string v0, "DQPSK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11b"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "17dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 456
    :sswitch_3d
    const-string v0, "CCK"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11b"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "17dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 472
    :sswitch_3e
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11g"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "14dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 488
    :sswitch_3f
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 494
    :sswitch_40
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "13dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 508
    :sswitch_41
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 512
    :sswitch_42
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 514
    :sswitch_43
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "13dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 528
    :sswitch_44
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 530
    :sswitch_45
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 532
    :sswitch_46
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 534
    :sswitch_47
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 536
    :sswitch_48
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 551
    :sswitch_49
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 555
    :sswitch_4a
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11n"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 559
    :sswitch_4b
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "12dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 573
    :sswitch_4c
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 575
    :sswitch_4d
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 577
    :sswitch_4e
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 579
    :sswitch_4f
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "3/4"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 581
    :sswitch_50
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 596
    :sswitch_51
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "2/3"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 600
    :sswitch_52
    const-string v0, "64-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 604
    :sswitch_53
    const-string v0, "256-QAM"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    const-string v0, "5/6"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    const-string v0, "802.11ac"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v0, "11dBm"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 610
    :cond_0
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    .line 611
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    .line 612
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    .line 613
    const-string v0, "N/A"

    sput-object v0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    goto/16 :goto_0

    .line 288
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_3c
        0x5 -> :sswitch_2
        0x6 -> :sswitch_4
        0x7 -> :sswitch_14
        0x9 -> :sswitch_5
        0xb -> :sswitch_3
        0xc -> :sswitch_6
        0xd -> :sswitch_c
        0xe -> :sswitch_15
        0xf -> :sswitch_22
        0x12 -> :sswitch_7
        0x13 -> :sswitch_d
        0x15 -> :sswitch_16
        0x16 -> :sswitch_3d
        0x18 -> :sswitch_8
        0x1a -> :sswitch_e
        0x1b -> :sswitch_1c
        0x1c -> :sswitch_17
        0x1d -> :sswitch_2c
        0x1e -> :sswitch_23
        0x20 -> :sswitch_33
        0x24 -> :sswitch_9
        0x27 -> :sswitch_f
        0x28 -> :sswitch_1d
        0x2b -> :sswitch_18
        0x2d -> :sswitch_24
        0x30 -> :sswitch_a
        0x34 -> :sswitch_10
        0x36 -> :sswitch_b
        0x39 -> :sswitch_19
        0x3a -> :sswitch_11
        0x3c -> :sswitch_25
        0x41 -> :sswitch_12
        0x48 -> :sswitch_1a
        0x4e -> :sswitch_13
        0x51 -> :sswitch_1e
        0x56 -> :sswitch_1b
        0x57 -> :sswitch_2d
        0x5a -> :sswitch_26
        0x60 -> :sswitch_3e
        0x61 -> :sswitch_34
        0x68 -> :sswitch_3f
        0x6c -> :sswitch_1f
        0x73 -> :sswitch_41
        0x75 -> :sswitch_2e
        0x78 -> :sswitch_27
        0x79 -> :sswitch_20
        0x82 -> :sswitch_35
        0x87 -> :sswitch_28
        0x90 -> :sswitch_42
        0x96 -> :sswitch_29
        0x9c -> :sswitch_40
        0xa2 -> :sswitch_21
        0xad -> :sswitch_43
        0xaf -> :sswitch_2f
        0xb4 -> :sswitch_2a
        0xc3 -> :sswitch_36
        0xc8 -> :sswitch_2b
        0xd8 -> :sswitch_44
        0xea -> :sswitch_30
        0xf0 -> :sswitch_49
        0xf3 -> :sswitch_45
        0x104 -> :sswitch_37
        0x107 -> :sswitch_31
        0x10e -> :sswitch_46
        0x124 -> :sswitch_38
        0x12c -> :sswitch_4a
        0x144 -> :sswitch_47
        0x145 -> :sswitch_39
        0x15f -> :sswitch_32
        0x168 -> :sswitch_48
        0x186 -> :sswitch_3a
        0x190 -> :sswitch_4b
        0x1b1 -> :sswitch_3b
        0x1d4 -> :sswitch_4c
        0x208 -> :sswitch_51
        0x20e -> :sswitch_4d
        0x249 -> :sswitch_4e
        0x28a -> :sswitch_52
        0x2be -> :sswitch_4f
        0x30c -> :sswitch_50
        0x362 -> :sswitch_53
    .end sparse-switch
.end method

.method private final pingIpAddr()V
    .locals 7

    .prologue
    .line 143
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingDestIP:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "ipAddress":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ping -c 1 -w 100 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 145
    .local v2, "p":Ljava/lang/Process;
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    move-result v3

    .line 146
    .local v3, "status":I
    if-nez v3, :cond_0

    .line 147
    const-string v4, "Pass"

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;

    .line 156
    .end local v1    # "ipAddress":Ljava/lang/String;
    .end local v2    # "p":Ljava/lang/Process;
    .end local v3    # "status":I
    :goto_0
    return-void

    .line 149
    .restart local v1    # "ipAddress":Ljava/lang/String;
    .restart local v2    # "p":Ljava/lang/Process;
    .restart local v3    # "status":I
    :cond_0
    const-string v4, "Fail: IP addr not reachable"

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 151
    .end local v1    # "ipAddress":Ljava/lang/String;
    .end local v2    # "p":Ljava/lang/Process;
    .end local v3    # "status":I
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "Fail: IOException"

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;

    goto :goto_0

    .line 153
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v4, "Fail: InterruptedException"

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;

    goto :goto_0
.end method

.method private final updatePingState()V
    .locals 5

    .prologue
    .line 159
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 160
    .local v0, "handler":Landroid/os/Handler;
    const-string v3, "Waiting response..."

    iput-object v3, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingIpAddrResult:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mstrPingIpAddrResult:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    new-instance v2, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$4;-><init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;)V

    .line 167
    .local v2, "updatePingResults":Ljava/lang/Runnable;
    new-instance v1, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$5;

    invoke-direct {v1, p0, v0, v2}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity$5;-><init>(Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 174
    .local v1, "ipAddrThread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 175
    return-void
.end method

.method private updateWifiInfo()V
    .locals 13

    .prologue
    const/4 v11, -0x1

    .line 646
    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v9

    .line 647
    .local v9, "wifiInfo":Landroid/net/wifi/WifiInfo;
    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v5

    .line 648
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    const/4 v0, 0x0

    .line 649
    .local v0, "currentChannel":I
    const/4 v1, 0x0

    .line 651
    .local v1, "currentFrequency":I
    if-eqz v5, :cond_2

    .line 652
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v2, v10, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_2

    .line 653
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/ScanResult;

    .line 655
    .local v6, "scanResult":Landroid/net/wifi/ScanResult;
    if-eqz v6, :cond_0

    iget-object v10, v6, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 652
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 659
    :cond_1
    if-eqz v9, :cond_0

    iget-object v10, v6, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 660
    iget v1, v6, Landroid/net/wifi/ScanResult;->frequency:I

    .line 661
    iget-object v10, v6, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    invoke-direct {p0, v1, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->getChannel(ILjava/lang/String;)I

    move-result v0

    .line 667
    .end local v2    # "i":I
    .end local v6    # "scanResult":Landroid/net/wifi/ScanResult;
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    .line 668
    .local v4, "isWiFion":Z
    const/4 v3, 0x0

    .line 669
    .local v3, "isConnected":Z
    if-eqz v4, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mNetworkInfo:Landroid/net/NetworkInfo;

    if-eqz v10, :cond_3

    .line 670
    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v10}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v10

    sget-object v12, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v10, v12, :cond_7

    const/4 v3, 0x1

    .line 672
    :cond_3
    :goto_1
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v10

    :goto_2
    invoke-direct {p0, v3, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->getNetworkInfofromLinkSpeed(ZI)V

    .line 674
    const/4 v7, 0x0

    .line 675
    .local v7, "string":Ljava/lang/String;
    const v10, 0x7f09008a

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 676
    .local v8, "text":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mNetworkInfo:Landroid/net/NetworkInfo;

    if-eqz v10, :cond_a

    .line 677
    if-nez v4, :cond_9

    .line 678
    const-string v7, "State: Power OFF"

    .line 685
    :goto_3
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 687
    const v10, 0x7f09008b

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 688
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SSID: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v9, :cond_b

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v10

    :goto_4
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 689
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 691
    const v10, 0x7f09008c

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 692
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "AP MAC: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v9, :cond_c

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v10

    :goto_5
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 693
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 695
    const v10, 0x7f09008d

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 696
    .restart local v8    # "text":Landroid/widget/TextView;
    if-nez v3, :cond_d

    .line 697
    const-string v7, "BSS CH: N/A"

    .line 704
    :goto_6
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 706
    const v10, 0x7f09008e

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 707
    .restart local v8    # "text":Landroid/widget/TextView;
    if-nez v3, :cond_f

    .line 708
    const-string v7, "WSEC: N/A"

    .line 714
    :goto_7
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 716
    const v10, 0x7f09008f

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 717
    .restart local v8    # "text":Landroid/widget/TextView;
    if-nez v3, :cond_11

    .line 718
    const-string v7, "Hi/Op: N/A"

    .line 725
    :goto_8
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    const v10, 0x7f090090

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 728
    .restart local v8    # "text":Landroid/widget/TextView;
    if-nez v3, :cond_13

    .line 729
    const-string v7, "RSSI: N/A"

    .line 733
    :goto_9
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 735
    const v10, 0x7f090091

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 736
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Modulation: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mModulation:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 737
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 739
    const v10, 0x7f090092

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 740
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CR: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mCodingrate:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 741
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 743
    const v10, 0x7f090093

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 744
    .restart local v8    # "text":Landroid/widget/TextView;
    if-nez v3, :cond_15

    .line 745
    const-string v7, "PER: N/A"

    .line 750
    :goto_a
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 752
    const v10, 0x7f090094

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 753
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "TX: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTxPower:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 754
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 756
    const v10, 0x7f090095

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 757
    .restart local v8    # "text":Landroid/widget/TextView;
    if-nez v3, :cond_16

    .line 758
    const-string v7, "Link Speed: N/A"

    .line 762
    :goto_b
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 764
    const v10, 0x7f090096

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 765
    .restart local v8    # "text":Landroid/widget/TextView;
    const/16 v10, 0x1e

    if-le v0, v10, :cond_5

    sget-object v10, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v12, "802.11b"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    sget-object v10, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    const-string v12, "802.11g"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 766
    :cond_4
    const-string v10, "802.11a"

    sput-object v10, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    .line 767
    :cond_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "NETWORK: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mbgn:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 768
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 770
    const v10, 0x7f090097

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 771
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IP: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v9, :cond_6

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v11

    :cond_6
    invoke-static {v11}, Landroid/text/format/Formatter;->formatIpAddress(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 772
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 774
    const v10, 0x7f090098

    invoke-virtual {p0, v10}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "text":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 775
    .restart local v8    # "text":Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "MAC: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-eqz v9, :cond_18

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v10

    :goto_c
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 776
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 777
    return-void

    .line 670
    .end local v7    # "string":Ljava/lang/String;
    .end local v8    # "text":Landroid/widget/TextView;
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_8
    move v10, v11

    .line 672
    goto/16 :goto_2

    .line 680
    .restart local v7    # "string":Ljava/lang/String;
    .restart local v8    # "text":Landroid/widget/TextView;
    :cond_9
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "State: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v12}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_3

    .line 683
    :cond_a
    const-string v7, "State: DISCONNECTED"

    goto/16 :goto_3

    .line 688
    :cond_b
    const-string v10, "null"

    goto/16 :goto_4

    .line 692
    :cond_c
    const-string v10, "null"

    goto/16 :goto_5

    .line 698
    :cond_d
    if-nez v0, :cond_e

    .line 699
    iget-object v10, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 700
    const-string v7, "BSS CH: Finding CH"

    goto/16 :goto_6

    .line 702
    :cond_e
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "BSS CH: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "  [ Frequency : "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "MHz ]"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_6

    .line 709
    :cond_f
    sget-object v10, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mwSec:Ljava/lang/String;

    if-eqz v10, :cond_10

    sget-object v10, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mwSec:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_10

    .line 710
    const-string v7, "WSEC: Open"

    goto/16 :goto_7

    .line 712
    :cond_10
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "WSEC: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mwSec:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_7

    .line 720
    :cond_11
    if-eqz v9, :cond_12

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getHiddenSSID()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 721
    const-string v7, "Hi/Op: Hidden"

    goto/16 :goto_8

    .line 723
    :cond_12
    const-string v7, "Hi/Op: Open"

    goto/16 :goto_8

    .line 731
    :cond_13
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "RSSI: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v9, :cond_14

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    :goto_d
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_9

    :cond_14
    const-string v10, "null"

    goto :goto_d

    .line 748
    :cond_15
    const-string v7, "PER:  %"

    goto/16 :goto_a

    .line 760
    :cond_16
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Link Speed: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v9, :cond_17

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v10

    :goto_e
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "Mbps"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_b

    :cond_17
    move v10, v11

    goto :goto_e

    .line 775
    :cond_18
    const-string v10, "null"

    goto/16 :goto_c
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    .line 179
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 180
    const/4 v1, 0x0

    .line 181
    .local v1, "in":Ljava/io/BufferedReader;
    const-string v4, "WiFi Information"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 182
    const v4, 0x7f030024

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->setContentView(I)V

    .line 183
    const-string v4, "wifi"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 185
    const v4, 0x7f09009a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ToggleButton;

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveModeBtn:Landroid/widget/ToggleButton;

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveModeBtn:Landroid/widget/ToggleButton;

    invoke-virtual {v4, v7}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 188
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/data/.psm.info"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x2

    invoke-direct {v2, v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    .end local v1    # "in":Ljava/io/BufferedReader;
    .local v2, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveMode:Ljava/lang/String;

    .line 190
    const-string v4, "WiFiDebugScreen"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPowerSaveMode Value= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveMode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 194
    if-eqz v2, :cond_4

    .line 196
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 203
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    sget-object v4, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveMode:Ljava/lang/String;

    if-eqz v4, :cond_1

    sget-object v4, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveMode:Ljava/lang/String;

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 204
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveModeBtn:Landroid/widget/ToggleButton;

    invoke-virtual {v4, v7}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 209
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveModeBtn:Landroid/widget/ToggleButton;

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSavemodeOnClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    const v4, 0x7f09009c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingDestIP:Landroid/widget/EditText;

    .line 214
    const v4, 0x7f09009d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingIpAddrResult:Landroid/widget/TextView;

    .line 215
    const v4, 0x7f09009b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->pingTestButton:Landroid/widget/Button;

    .line 216
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->pingTestButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPingButtonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 220
    .local v3, "pm":Landroid/os/PowerManager;
    const v4, 0x3000001a

    const-string v5, "WiFiDebugScreen"

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 222
    return-void

    .line 197
    .end local v1    # "in":Ljava/io/BufferedReader;
    .end local v3    # "pm":Landroid/os/PowerManager;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 199
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_0

    .line 191
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 192
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v4, "WiFiDebugScreen"

    const-string v5, "File open error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 194
    if-eqz v1, :cond_0

    .line 196
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 197
    :catch_2
    move-exception v0

    .line 198
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 194
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v1, :cond_2

    .line 196
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 199
    :cond_2
    :goto_4
    throw v4

    .line 197
    :catch_3
    move-exception v0

    .line 198
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 206
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mPowerSaveModeBtn:Landroid/widget/ToggleButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_1

    .line 194
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_3

    .line 191
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_2

    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :cond_4
    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 262
    const-string v0, "WiFiDebugScreen"

    const-string v1, "[WiFiInfoActivity] LCD follows Android\'s power policy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 265
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 244
    const-string v0, "WiFiDebugScreen"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPause:Z

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 255
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 256
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 225
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 226
    const-string v1, "WiFiDebugScreen"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mIsPause:Z

    .line 229
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 230
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 231
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 232
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->updateWifiInfo()V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mTimerHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/WifiInfoActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 239
    const-string v1, "WiFiDebugScreen"

    const-string v2, "[WiFiInfoActivity] WakeLock Acquired"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    return-void
.end method
