.class Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;
.super Ljava/lang/Object;
.source "PhoneUtil_TD.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_TD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_MAIN_CMD_CFG:I

.field final OEM_SUB_CMD_DUAL_MODE:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;->OEM_MAIN_CMD_CFG:I

    .line 269
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;->OEM_SUB_CMD_DUAL_MODE:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$1;

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/PhoneUtil_TD$OemCommands;-><init>()V

    return-void
.end method


# virtual methods
.method sendDualModeValue(III)[B
    .locals 7
    .param p1, "maincmd"    # I
    .param p2, "subcmd"    # I
    .param p3, "value"    # I

    .prologue
    .line 272
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 273
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 274
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 277
    .local v3, "fileSize":C
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 278
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 279
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 280
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 281
    :catch_0
    move-exception v2

    .line 282
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "PhoneUtil_TD"

    const-string v5, "getServMEnterData"

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/modemui/util/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v4, 0x0

    goto :goto_0
.end method
