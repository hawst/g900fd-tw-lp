.class Lcom/sec/android/app/servicemodeapp/SysDump$12;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump;->showOTPAlertDialogForAuth()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

.field final synthetic val$d:Ljava/lang/String;

.field final synthetic val$inputOTP:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump;Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3188
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iput-object p2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->val$inputOTP:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->val$d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 3191
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->val$inputOTP:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 3193
    .local v0, "KeyStr":Ljava/lang/String;
    new-instance v1, LLibOTPSecurity/OTPSecurity;

    invoke-direct {v1}, LLibOTPSecurity/OTPSecurity;-><init>()V

    .line 3194
    .local v1, "Otp":LLibOTPSecurity/OTPSecurity;
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->val$d:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, LLibOTPSecurity/OTPSecurity;->CheckOTP(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 3196
    .local v2, "bOptd":Z
    if-eqz v2, :cond_0

    .line 3197
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->settings:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$7900(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 3198
    .local v3, "setAuth":Landroid/content/SharedPreferences$Editor;
    const-string v4, "ril.OTPAuth"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 3199
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3200
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v5, "OTP Authentication enabled!"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 3204
    .end local v3    # "setAuth":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 3203
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/servicemodeapp/SysDump$12;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v5, "OTP Authentication Failed!"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
