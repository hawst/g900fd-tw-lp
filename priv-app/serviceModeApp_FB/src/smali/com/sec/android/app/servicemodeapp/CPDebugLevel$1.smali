.class Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;
.super Ljava/lang/Object;
.source "CPDebugLevel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/servicemodeapp/CPDebugLevel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 46
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    # getter for: Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCPRamdump:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->access$000(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 48
    const-string v2, "KOREA"

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    # getter for: Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->mCountryCode:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->access$100(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "true"

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    # getter for: Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->productship:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->access$200(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    :try_start_0
    const-string v2, "ro.cp_debug_level"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "cpdumping":Ljava/lang/String;
    const-string v2, "0x55FF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    const-string v3, "0x5500"

    # invokes: Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->doRebootNSaveforCP(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->access$300(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    .end local v0    # "cpdumping":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 60
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "CPDebugLevel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exceptoin : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 57
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "cpdumping":Ljava/lang/String;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/CPDebugLevel$1;->this$0:Lcom/sec/android/app/servicemodeapp/CPDebugLevel;

    const-string v3, "0x55FF"

    # invokes: Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->doRebootNSaveforCP(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/servicemodeapp/CPDebugLevel;->access$300(Lcom/sec/android/app/servicemodeapp/CPDebugLevel;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
