.class Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;
.super Ljava/lang/Object;
.source "PhoneUtil_MarvellVIA.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;


# direct methods
.method constructor <init>(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 111
    const v1, 0x7f090028

    if-ne p2, v1, :cond_1

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Z)Z

    .line 115
    const-string v1, "PhoneUtil_Marvell"

    const-string v2, "onCheckedChanged"

    const-string v3, "USB_STATUS1 : VIA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_Marvell"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! USB to MODEM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f090029

    if-ne p2, v1, :cond_0

    .line 123
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    const-string v2, "PDA"

    # invokes: Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->access$200(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Ljava/lang/String;)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA$2;->this$0:Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;->access$302(Lcom/sec/android/app/modemui/activities/PhoneUtil_MarvellVIA;Z)Z

    .line 125
    const-string v1, "PhoneUtil_Marvell"

    const-string v2, "onCheckedChanged"

    const-string v3, "USB_STATUS2 : PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    .line 127
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "PhoneUtil_Marvell"

    const-string v2, "onCheckedChanged"

    const-string v3, "Exception! USB to PDA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
