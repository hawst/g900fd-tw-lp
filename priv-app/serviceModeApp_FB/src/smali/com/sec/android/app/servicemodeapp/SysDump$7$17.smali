.class Lcom/sec/android/app/servicemodeapp/SysDump$7$17;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

.field final synthetic val$sdcard:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 2775
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iput-object p2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->val$sdcard:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 2777
    new-instance v8, Ljava/io/File;

    const-string v15, "/data/log"

    invoke-direct {v8, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2778
    .local v8, "dataLogDirectory":Ljava/io/File;
    new-instance v14, Ljava/io/File;

    const-string v15, "/data/slog"

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2779
    .local v14, "silentLogDirectory":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    const-string v15, "/data/log/err"

    invoke-direct {v5, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2780
    .local v5, "dataCPLogDirectory":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    const-string v15, "/efs/root/ERR"

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2781
    .local v6, "dataCPLogDirectoryEfs":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v15, "/tombstones/mdm"

    invoke-direct {v4, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2782
    .local v4, "dataCPCrashLogDirectory":Ljava/io/File;
    new-instance v12, Ljava/io/File;

    const-string v15, "/storage/extSdCard/log"

    invoke-direct {v12, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2783
    .local v12, "sdcardLogDirectory":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    const-string v15, "/storage/extSdCard/log/cp"

    invoke-direct {v11, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2784
    .local v11, "sdcardCPCrashLogDirectory":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v15, "/data/app/bt.log"

    invoke-direct {v3, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2785
    .local v3, "btlog":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const-string v15, "/data/cp_log"

    invoke-direct {v7, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2787
    .local v7, "dataCPLogNewDirectory":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v15, "/NVM"

    invoke-direct {v1, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2788
    .local v1, "NVMDirectory":Ljava/io/File;
    new-instance v9, Ljava/io/File;

    const-string v15, "data/log/NVM"

    invoke-direct {v9, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2789
    .local v9, "dataNVMDirectory":Ljava/io/File;
    new-instance v13, Ljava/io/File;

    const-string v15, "/storage/extSdCard/log/NVM"

    invoke-direct {v13, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2792
    .local v13, "sdcardNVMDirectory":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_0

    .line 2793
    invoke-virtual {v12}, Ljava/io/File;->mkdir()Z

    .line 2797
    :cond_0
    new-instance v2, Ljava/io/File;

    const-string v15, "/data/system/users/service/data"

    invoke-direct {v2, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2798
    .local v2, "ResetreasonDirectory":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v2, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2813
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v8, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2814
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v14, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2815
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v4, v11}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2816
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v7, v11}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2818
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->isMarvell:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5600()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 2819
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v1, v13}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2820
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v9, v13}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2823
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 2824
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v5, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2827
    :cond_2
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 2828
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v6, v12}, Lcom/sec/android/app/servicemodeapp/SysDump;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 2831
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2832
    const-string v15, "SysDump"

    const-string v16, "btlog.exists == true"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2833
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v16, "/data/app/bt.log"

    const-string v17, "/storage/extSdCard/log/bt.log"

    const-string v18, "bt.log"

    invoke-virtual/range {v15 .. v18}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2836
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->handler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6600(Lcom/sec/android/app/servicemodeapp/SysDump;)Landroid/os/Handler;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2837
    const-string v15, "SysDump"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "broadcast media mounted = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->val$sdcard:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2840
    new-instance v10, Landroid/content/Intent;

    const-string v15, "android.intent.action.MEDIA_MOUNTED"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "file://"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->val$sdcard:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2842
    .local v10, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v15, v10}, Lcom/sec/android/app/servicemodeapp/SysDump;->sendBroadcast(Landroid/content/Intent;)V

    .line 2843
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/servicemodeapp/SysDump$7$17;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v15, v15, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v16, 0x3fa

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2846
    return-void
.end method
