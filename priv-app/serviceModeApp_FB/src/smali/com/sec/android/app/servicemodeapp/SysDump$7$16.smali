.class Lcom/sec/android/app/servicemodeapp/SysDump$7$16;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V
    .locals 0

    .prologue
    .line 2691
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$16;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 2693
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2698
    const-string v1, "KOREA"

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->mCountryCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$6000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->iRamdumpMode:I
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$1700()I

    move-result v1

    if-eqz v1, :cond_1

    .line 2699
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$16;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v2, "Wait..."

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->showProgressDialog(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$200(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Z

    .line 2702
    :cond_1
    const-string v1, "CHM"

    const-string v2, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "CHC"

    const-string v2, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2707
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$16;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->hideProgressDialog()Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 2712
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.provider.Telephony.SECRET_CODE"

    const-string v2, "android_secret_code://CP_RAMDUMP"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2713
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$16;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/servicemodeapp/SysDump;->sendBroadcast(Landroid/content/Intent;)V

    .line 2715
    return-void
.end method
