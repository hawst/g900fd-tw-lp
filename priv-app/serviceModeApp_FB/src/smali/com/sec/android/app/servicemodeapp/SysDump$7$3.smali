.class Lcom/sec/android/app/servicemodeapp/SysDump$7$3;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

.field final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2225
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iput-object p2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->val$input:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 2227
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v2, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->val$input:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2202(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2229
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2230
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v2, "Please choose Interface"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/servicemodeapp/SysDump;->ResultMessage(Ljava/lang/String;)V

    .line 2252
    :goto_0
    return-void

    .line 2234
    :cond_0
    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->wifiOnly:Z
    invoke-static {}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$3900()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->isKOR()Z
    invoke-static {v1}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2235
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v1, v1, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$3;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v3, v3, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->TCPDUMP_INTERFACE:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$2200(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->startStopWiFiTcpdump(ZLjava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4100(Lcom/sec/android/app/servicemodeapp/SysDump;ZLjava/lang/String;)V

    goto :goto_0

    .line 2237
    :cond_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/servicemodeapp/SysDump$7$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/servicemodeapp/SysDump$7$3$1;-><init>(Lcom/sec/android/app/servicemodeapp/SysDump$7$3;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2250
    .local v0, "tcpDumpThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
