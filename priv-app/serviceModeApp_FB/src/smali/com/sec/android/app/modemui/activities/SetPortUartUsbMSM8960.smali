.class public Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;
.super Landroid/app/Activity;
.source "SetPortUartUsbMSM8960.java"


# instance fields
.field private final ACTION_REQUEST_START_FTCLIENT:Ljava/lang/String;

.field private mGoQualcommUSBSettingMenu:Landroid/widget/Button;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbToAp:Z

.field private textUSB:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 66
    const-string v0, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->ACTION_REQUEST_START_FTCLIENT:Ljava/lang/String;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUsbToAp:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->changeUartPath(Ljava/lang/String;)V

    return-void
.end method

.method private changeUartPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 224
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 225
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.factory.aporiented.athandler.AtUartswit.SetUartPath"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const-string v2, "PATH"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->sendBroadcast(Landroid/content/Intent;)V

    .line 228
    const/16 v2, 0x1f4

    invoke-direct {p0, v2}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->getCurrentUARTPath(I)Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "path":Ljava/lang/String;
    return-void
.end method

.method private getCurrentUARTPath(I)Ljava/lang/String;
    .locals 11
    .param p1, "millis"    # I

    .prologue
    const v10, 0x7f090025

    .line 169
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCurrentUARTStatus : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    int-to-long v6, p1

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    const/4 v2, 0x0

    .line 178
    .local v2, "path":Ljava/lang/String;
    const-string v0, ""

    .line 179
    .local v0, "correct_path":Ljava/lang/String;
    const/4 v3, 0x0

    .line 180
    .local v3, "reader":Ljava/io/BufferedReader;
    const-string v0, "data/misc/radio/uart.txt"

    .line 181
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FILE PATH : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    if-eqz v4, :cond_0

    .line 187
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 188
    .local v5, "readerTemp":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 189
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 196
    .end local v5    # "readerTemp":Ljava/lang/String;
    :cond_0
    if-eqz v4, :cond_6

    .line 198
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v3, v4

    .line 206
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_1
    if-eqz v2, :cond_5

    .line 207
    const-string v6, "CP"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 208
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "getCurrentUARTStatus : CP"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUartRadioGroup:Landroid/widget/RadioGroup;

    const v7, 0x7f090024

    invoke-virtual {v6, v7}, Landroid/widget/RadioGroup;->check(I)V

    .line 220
    :cond_2
    :goto_2
    return-object v2

    .line 173
    .end local v0    # "correct_path":Ljava/lang/String;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "correct_path":Ljava/lang/String;
    .restart local v2    # "path":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v1

    .line 200
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Closed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 202
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 192
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 193
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_4
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Read"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 196
    if-eqz v3, :cond_1

    .line 198
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 199
    :catch_3
    move-exception v1

    .line 200
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "Exception! While Buffer Closed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 196
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v3, :cond_3

    .line 198
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 202
    :cond_3
    :goto_5
    throw v6

    .line 199
    :catch_4
    move-exception v1

    .line 200
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "SetPortUartUsbMSM8960"

    const-string v8, "getCurrentUARTPath"

    const-string v9, "Exception! While Buffer Closed"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 210
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    const-string v6, "AP"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 211
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "getCurrentUARTStatus : AP"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUartRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v6, v10}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_2

    .line 215
    :cond_5
    const-string v6, "SetPortUartUsbMSM8960"

    const-string v7, "getCurrentUARTPath"

    const-string v8, "path == null"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v2, "AP"

    .line 217
    iget-object v6, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUartRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v6, v10}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_2

    .line 196
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 192
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_5
    move-exception v1

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_6
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private isOrangeModels()Z
    .locals 4

    .prologue
    .line 336
    const-string v2, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "salescode":Ljava/lang/String;
    const-string v0, "EVR/ORA/TMU/TMP/FTM/IDE/AMN/ONE/ORO/ORS/ORG/OPT/MST"

    .line 339
    .local v0, "customerOrange":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 340
    const/4 v2, 0x1

    .line 342
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isTelstraModels()Z
    .locals 4

    .prologue
    .line 356
    const-string v2, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, "salescode":Ljava/lang/String;
    const-string v0, "TEL"

    .line 359
    .local v0, "customerTEL":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 360
    const/4 v2, 0x1

    .line 362
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isVFGModels()Z
    .locals 4

    .prologue
    .line 346
    const-string v2, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "salescode":Ljava/lang/String;
    const-string v0, "CYV/OMN/TCL/VD2/VDI/VOD"

    .line 349
    .local v0, "customerVFG":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 350
    const/4 v2, 0x1

    .line 352
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private runFtClient()V
    .locals 3

    .prologue
    .line 164
    const-string v0, "SetPortUartUsbMSM8960"

    const-string v1, "runFtClient"

    const-string v2, "send ACTION_REQUEST_START_FTCLIENT"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->sendBroadcast(Landroid/content/Intent;)V

    .line 166
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x8

    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f030011

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->setContentView(I)V

    .line 78
    const-string v0, "SetPortUartUsbMSM8960"

    const-string v1, "onCreate"

    const-string v2, "onCreate()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/modemui/util/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960$1;-><init>(Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 101
    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 103
    const v0, 0x7f09002c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->textUSB:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    const-string v1, "Qualcomm USB Settings"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960$2;-><init>(Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->isOrangeModels()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->isVFGModels()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->isTelstraModels()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->textUSB:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->mGoQualcommUSBSettingMenu:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 158
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->getCurrentUARTPath(I)Ljava/lang/String;

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/modemui/activities/SetPortUartUsbMSM8960;->runFtClient()V

    .line 161
    return-void
.end method
