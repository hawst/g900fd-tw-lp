.class Lcom/sec/android/app/servicemodeapp/SysDump$7$6;
.super Ljava/lang/Object;
.source "SysDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/SysDump$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/SysDump$7;)V
    .locals 0

    .prologue
    .line 2336
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 2338
    new-instance v2, Ljava/io/File;

    const-string v5, "/data/log"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2339
    .local v2, "dataLogDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v5, "/mnt/sdcard/Android/data/btsnoop_hci.log"

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2340
    .local v0, "btsnoop_log":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v5, "/mnt/sdcard/Android/data/btsnoop_hci.log.last"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2341
    .local v1, "btsnoop_log_old":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v5, "/mnt/sdcard/Android/data/[BT]msm_serial_hs.log"

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2342
    .local v3, "hsuart_log":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v5, "/mnt/sdcard/Android/data/[BT]msm_serial_hs.log.old"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2344
    .local v4, "hsuart_log_old":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2345
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 2348
    :cond_0
    const-string v5, "gardaltetmo"

    const-string v6, "ro.product.name"

    const-string v7, "UNKNOWN"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2349
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dumpstate -r > /data/log/dumpState_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getTimeToString()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4500(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->DoShellCmd(Ljava/lang/String;)I

    .line 2356
    :goto_0
    const-string v5, "persist.security.mdm.SElogs"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2357
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getSEAndroidLogs()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4900(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 2359
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getTSPLogs()Z
    invoke-static {v5}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5000(Lcom/sec/android/app/servicemodeapp/SysDump;)Z

    .line 2361
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2362
    const-string v5, "SysDump"

    const-string v6, "btsnoop_hci.log exists!! "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2363
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/btsnoop_hci_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5100(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2364
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "/mnt/sdcard/Android/data/btsnoop_hci.log"

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "btsnoop_hci.log"

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2367
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2368
    const-string v5, "SysDump"

    const-string v6, "btsnoop_hci.log.last exists!! "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2369
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/btsnoop_hci_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5100(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log.last"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2370
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "/mnt/sdcard/Android/data/btsnoop_hci.log.last"

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "btsnoop_hci.log.last"

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2373
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2374
    const-string v5, "SysDump"

    const-string v6, "[BT]msm_serial_hs.log exists!! "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2375
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/[BT]msm_serial_hs_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5100(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2376
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "/mnt/sdcard/Android/data/[BT]msm_serial_hs.log"

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "[BT]msm_serial_hs.log"

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2379
    :cond_4
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2380
    const-string v5, "SysDump"

    const-string v6, "[BT]msm_serial_hs.log.old exists!! "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2381
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/log/[BT]msm_serial_hs_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->sysdump_time:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$5100(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log.old"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4402(Lcom/sec/android/app/servicemodeapp/SysDump;Ljava/lang/String;)Ljava/lang/String;

    .line 2382
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    const-string v6, "/mnt/sdcard/Android/data/[BT]msm_serial_hs.log.old"

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # getter for: Lcom/sec/android/app/servicemodeapp/SysDump;->outFile:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4400(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "[BT]msm_serial_hs.log.old"

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/servicemodeapp/SysDump;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2385
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x3ed

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2386
    return-void

    .line 2352
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v5, v5, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bugreport > /data/log/dumpState_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/servicemodeapp/SysDump$7$6;->this$1:Lcom/sec/android/app/servicemodeapp/SysDump$7;

    iget-object v7, v7, Lcom/sec/android/app/servicemodeapp/SysDump$7;->this$0:Lcom/sec/android/app/servicemodeapp/SysDump;

    # invokes: Lcom/sec/android/app/servicemodeapp/SysDump;->getTimeToString()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/servicemodeapp/SysDump;->access$4500(Lcom/sec/android/app/servicemodeapp/SysDump;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/servicemodeapp/SysDump;->DoShellCmd(Ljava/lang/String;)I

    goto/16 :goto_0
.end method
