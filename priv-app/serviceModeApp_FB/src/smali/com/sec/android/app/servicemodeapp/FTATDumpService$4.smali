.class Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;
.super Ljava/lang/Object;
.source "FTATDumpService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/servicemodeapp/FTATDumpService;->getCopyLog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/servicemodeapp/FTATDumpService;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 252
    new-instance v4, Ljava/io/File;

    const-string v8, "/data/log"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v4, "dataLogDirectory":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v8, "/data/log/err"

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 254
    .local v2, "dataCPLogDirectory":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v8, "/efs/root/ERR"

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 255
    .local v3, "dataCPLogDirectoryEfs":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v8, "/tombstones/mdm"

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 256
    .local v1, "dataCPCrashLogDirectory":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const-string v8, "/mnt/sdcard/log"

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 257
    .local v7, "sdcardLogDirectory":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    const-string v8, "/mnt/sdcard/log/cp"

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 258
    .local v6, "sdcardCPCrashLogDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v8, "/data/app/bt.log"

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .local v0, "btlog":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v8, v4, v7}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 260
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v8, v1, v6}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 262
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 263
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v8, v2, v7}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 266
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 267
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v8, v3, v7}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 270
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 271
    const-string v8, "FTATDumpService"

    const-string v9, "btlog.exists == true"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    const-string v9, "/data/app/bt.log"

    const-string v10, "/mnt/sdcard/log/bt.log"

    const-string v11, "bt.log"

    invoke-virtual {v8, v9, v10, v11}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 275
    :cond_2
    const-string v8, "FTATDumpService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "broadcast media mounted = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "file://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 278
    .local v5, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->sendBroadcast(Landroid/content/Intent;)V

    .line 279
    iget-object v8, p0, Lcom/sec/android/app/servicemodeapp/FTATDumpService$4;->this$0:Lcom/sec/android/app/servicemodeapp/FTATDumpService;

    iget-object v8, v8, Lcom/sec/android/app/servicemodeapp/FTATDumpService;->mHandler:Landroid/os/Handler;

    const/16 v9, 0x3ef

    const-wide/16 v10, 0x2710

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 280
    return-void
.end method
