.class Lcom/android/wallpapercropper/WallpaperCropActivity$1;
.super Ljava/lang/Object;
.source "WallpaperCropActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/wallpapercropper/WallpaperCropActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

.field final synthetic val$imageUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iput-object p2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->val$imageUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iget-boolean v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->finishActivityWhenDone:Z

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->finishActivityWhenDone:Z

    .line 146
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->val$imageUri:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$1;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iget-boolean v3, v3, Lcom/android/wallpapercropper/WallpaperCropActivity;->finishActivityWhenDone:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/wallpapercropper/WallpaperCropActivity;->cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;Z)V

    .line 148
    :cond_0
    return-void
.end method
