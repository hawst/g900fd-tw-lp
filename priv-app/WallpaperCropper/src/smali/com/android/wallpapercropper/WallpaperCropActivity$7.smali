.class Lcom/android/wallpapercropper/WallpaperCropActivity$7;
.super Ljava/lang/Object;
.source "WallpaperCropActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/wallpapercropper/WallpaperCropActivity;->cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

.field final synthetic val$finishActivityWhenDone:Z


# direct methods
.method constructor <init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Z)V
    .locals 0

    .prologue
    .line 441
    iput-object p1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$7;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iput-boolean p2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$7;->val$finishActivityWhenDone:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 443
    iget-boolean v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$7;->val$finishActivityWhenDone:Z

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$7;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/wallpapercropper/WallpaperCropActivity;->setResult(I)V

    .line 445
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$7;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    invoke-virtual {v0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->finish()V

    .line 447
    :cond_0
    return-void
.end method
