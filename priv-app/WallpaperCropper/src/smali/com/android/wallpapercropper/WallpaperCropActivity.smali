.class public Lcom/android/wallpapercropper/WallpaperCropActivity;
.super Landroid/app/Activity;
.source "WallpaperCropActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;,
        Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;
    }
.end annotation


# instance fields
.field finishActivityWhenDone:Z

.field protected mCropView:Lcom/android/wallpapercropper/CropView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 461
    return-void
.end method

.method protected static convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;
    .locals 1
    .param p0, "extension"    # Ljava/lang/String;

    .prologue
    .line 902
    const-string v0, "png"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0
.end method

.method protected static getDefaultWallpaperSize(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;
    .locals 9
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "windowManager"    # Landroid/view/WindowManager;

    .prologue
    .line 273
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 274
    .local v5, "minDims":Landroid/graphics/Point;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 275
    .local v3, "maxDims":Landroid/graphics/Point;
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v5, v3}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 277
    iget v7, v3, Landroid/graphics/Point;->x:I

    iget v8, v3, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 278
    .local v2, "maxDim":I
    iget v7, v5, Landroid/graphics/Point;->x:I

    iget v8, v5, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 280
    .local v4, "minDim":I
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x11

    if-lt v7, v8, :cond_0

    .line 281
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 282
    .local v6, "realSize":Landroid/graphics/Point;
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 283
    iget v7, v6, Landroid/graphics/Point;->x:I

    iget v8, v6, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 284
    iget v7, v6, Landroid/graphics/Point;->x:I

    iget v8, v6, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 291
    .end local v6    # "realSize":Landroid/graphics/Point;
    :cond_0
    invoke-static {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->isScreenLarge(Landroid/content/res/Resources;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 292
    int-to-float v7, v2

    invoke-static {v2, v4}, Lcom/android/wallpapercropper/WallpaperCropActivity;->wallpaperTravelToScreenWidthRatio(II)F

    move-result v8

    mul-float/2addr v7, v8

    float-to-int v1, v7

    .line 293
    .local v1, "defaultWidth":I
    move v0, v2

    .line 298
    .local v0, "defaultHeight":I
    :goto_0
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v7

    .line 295
    .end local v0    # "defaultHeight":I
    .end local v1    # "defaultWidth":I
    :cond_1
    int-to-float v7, v4

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 296
    .restart local v1    # "defaultWidth":I
    move v0, v2

    .restart local v0    # "defaultHeight":I
    goto :goto_0
.end method

.method protected static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "requestFormat"    # Ljava/lang/String;

    .prologue
    .line 906
    if-nez p0, :cond_1

    const-string v0, "jpg"

    .line 909
    .local v0, "outputFormat":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 910
    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "png"

    :goto_1
    return-object v1

    .end local v0    # "outputFormat":Ljava/lang/String;
    :cond_1
    move-object v0, p0

    .line 906
    goto :goto_0

    .line 910
    .restart local v0    # "outputFormat":Ljava/lang/String;
    :cond_2
    const-string v1, "jpg"

    goto :goto_1
.end method

.method private static isScreenLarge(Landroid/content/res/Resources;)Z
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 389
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 390
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v2, 0x2d0

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static wallpaperTravelToScreenWidthRatio(II)F
    .locals 9
    .param p0, "width"    # I
    .param p1, "height"    # I

    .prologue
    .line 248
    int-to-float v7, p0

    int-to-float v8, p1

    div-float v4, v7, v8

    .line 255
    .local v4, "aspectRatio":F
    const v0, 0x3fcccccd    # 1.6f

    .line 256
    .local v0, "ASPECT_RATIO_LANDSCAPE":F
    const/high16 v1, 0x3f200000    # 0.625f

    .line 257
    .local v1, "ASPECT_RATIO_PORTRAIT":F
    const/high16 v2, 0x3fc00000    # 1.5f

    .line 258
    .local v2, "WALLPAPER_WIDTH_TO_SCREEN_RATIO_LANDSCAPE":F
    const v3, 0x3f99999a    # 1.2f

    .line 265
    .local v3, "WALLPAPER_WIDTH_TO_SCREEN_RATIO_PORTRAIT":F
    const v5, 0x3e9d89d7

    .line 268
    .local v5, "x":F
    const v6, 0x3f80fc10

    .line 269
    .local v6, "y":F
    const v7, 0x3e9d89d7

    mul-float/2addr v7, v4

    const v8, 0x3f80fc10

    add-float/2addr v7, v8

    return v7
.end method


# virtual methods
.method protected cropImageAndSetWallpaper(Landroid/net/Uri;Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;Z)V
    .locals 25
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "onBitmapCroppedHandler"    # Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;
    .param p3, "finishActivityWhenDone"    # Z

    .prologue
    .line 396
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v3}, Lcom/android/wallpapercropper/CropView;->getLayoutDirection()I

    move-result v3

    if-nez v3, :cond_1

    const/16 v21, 0x1

    .line 399
    .local v21, "ltr":Z
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v13

    .line 401
    .local v13, "d":Landroid/view/Display;
    new-instance v15, Landroid/graphics/Point;

    invoke-direct {v15}, Landroid/graphics/Point;-><init>()V

    .line 402
    .local v15, "displaySize":Landroid/graphics/Point;
    invoke-virtual {v13, v15}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 403
    iget v3, v15, Landroid/graphics/Point;->x:I

    iget v4, v15, Landroid/graphics/Point;->y:I

    if-ge v3, v4, :cond_2

    const/16 v20, 0x1

    .line 405
    .local v20, "isPortrait":Z
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getDefaultWallpaperSize(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v14

    .line 408
    .local v14, "defaultWallpaperSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v3}, Lcom/android/wallpapercropper/CropView;->getCrop()Landroid/graphics/RectF;

    move-result-object v5

    .line 409
    .local v5, "cropRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v3}, Lcom/android/wallpapercropper/CropView;->getImageRotation()I

    move-result v6

    .line 410
    .local v6, "cropRotation":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v3}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float v12, v3, v4

    .line 412
    .local v12, "cropScale":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v3}, Lcom/android/wallpapercropper/CropView;->getSourceDimensions()Landroid/graphics/Point;

    move-result-object v19

    .line 413
    .local v19, "inSize":Landroid/graphics/Point;
    new-instance v23, Landroid/graphics/Matrix;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Matrix;-><init>()V

    .line 414
    .local v23, "rotateMatrix":Landroid/graphics/Matrix;
    int-to-float v3, v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 415
    const/4 v3, 0x2

    new-array v0, v3, [F

    move-object/from16 v24, v0

    const/4 v3, 0x0

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    aput v4, v24, v3

    const/4 v3, 0x1

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    aput v4, v24, v3

    .line 416
    .local v24, "rotatedInSize":[F
    invoke-virtual/range {v23 .. v24}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 417
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, v24, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v24, v3

    .line 418
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget v4, v24, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v24, v3

    .line 423
    if-eqz v21, :cond_3

    const/4 v3, 0x0

    aget v3, v24, v3

    iget v4, v5, Landroid/graphics/RectF;->right:F

    sub-float v18, v3, v4

    .line 425
    .local v18, "extraSpace":F
    :goto_2
    iget v3, v14, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    div-float/2addr v3, v12

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v4

    sub-float v22, v3, v4

    .line 426
    .local v22, "maxExtraSpace":F
    move/from16 v0, v18

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v18

    .line 427
    iget v3, v5, Landroid/graphics/RectF;->right:F

    add-float v3, v3, v18

    iput v3, v5, Landroid/graphics/RectF;->right:F

    .line 428
    iget v3, v5, Landroid/graphics/RectF;->left:F

    sub-float v3, v3, v18

    iput v3, v5, Landroid/graphics/RectF;->left:F

    .line 431
    iget v3, v14, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    div-float/2addr v3, v12

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float v17, v3, v4

    .line 433
    .local v17, "extraPortraitHeight":F
    const/4 v3, 0x1

    aget v3, v24, v3

    iget v4, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    iget v4, v5, Landroid/graphics/RectF;->top:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float v4, v17, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v16

    .line 435
    .local v16, "expandHeight":F
    iget v3, v5, Landroid/graphics/RectF;->top:F

    sub-float v3, v3, v16

    iput v3, v5, Landroid/graphics/RectF;->top:F

    .line 436
    iget v3, v5, Landroid/graphics/RectF;->bottom:F

    add-float v3, v3, v16

    iput v3, v5, Landroid/graphics/RectF;->bottom:F

    .line 438
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, v12

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 439
    .local v7, "outWidth":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v3, v12

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 441
    .local v8, "outHeight":I
    new-instance v11, Lcom/android/wallpapercropper/WallpaperCropActivity$7;

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v11, v0, v1}, Lcom/android/wallpapercropper/WallpaperCropActivity$7;-><init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Z)V

    .line 449
    .local v11, "onEndCrop":Ljava/lang/Runnable;
    new-instance v2, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v11}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 451
    .local v2, "cropTask":Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;
    if-eqz p2, :cond_0

    .line 452
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->setOnBitmapCropped(Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;)V

    .line 454
    :cond_0
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 455
    return-void

    .line 396
    .end local v2    # "cropTask":Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;
    .end local v5    # "cropRect":Landroid/graphics/RectF;
    .end local v6    # "cropRotation":I
    .end local v7    # "outWidth":I
    .end local v8    # "outHeight":I
    .end local v11    # "onEndCrop":Ljava/lang/Runnable;
    .end local v12    # "cropScale":F
    .end local v13    # "d":Landroid/view/Display;
    .end local v14    # "defaultWallpaperSize":Landroid/graphics/Point;
    .end local v15    # "displaySize":Landroid/graphics/Point;
    .end local v16    # "expandHeight":F
    .end local v17    # "extraPortraitHeight":F
    .end local v18    # "extraSpace":F
    .end local v19    # "inSize":Landroid/graphics/Point;
    .end local v20    # "isPortrait":Z
    .end local v21    # "ltr":Z
    .end local v22    # "maxExtraSpace":F
    .end local v23    # "rotateMatrix":Landroid/graphics/Matrix;
    .end local v24    # "rotatedInSize":[F
    :cond_1
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 403
    .restart local v13    # "d":Landroid/view/Display;
    .restart local v15    # "displaySize":Landroid/graphics/Point;
    .restart local v21    # "ltr":Z
    :cond_2
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 423
    .restart local v5    # "cropRect":Landroid/graphics/RectF;
    .restart local v6    # "cropRotation":I
    .restart local v12    # "cropScale":F
    .restart local v14    # "defaultWallpaperSize":Landroid/graphics/Point;
    .restart local v19    # "inSize":Landroid/graphics/Point;
    .restart local v20    # "isPortrait":Z
    .restart local v23    # "rotateMatrix":Landroid/graphics/Matrix;
    .restart local v24    # "rotatedInSize":[F
    :cond_3
    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    goto/16 :goto_2
.end method

.method public enableRotation()Z
    .locals 2

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method protected init()V
    .locals 19

    .prologue
    .line 91
    const v2, 0x7f030001

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/wallpapercropper/WallpaperCropActivity;->setContentView(I)V

    .line 93
    const v2, 0x7f070002

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/wallpapercropper/WallpaperCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/wallpapercropper/CropView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    .line 96
    .local v13, "cropIntent":Landroid/content/Intent;
    invoke-virtual {v13}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 98
    .local v3, "imageUri":Landroid/net/Uri;
    if-nez v3, :cond_1

    .line 99
    const-string v2, "Launcher3.CropActivity"

    const-string v4, "No URI passed in intent, exiting WallpaperCropActivity"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->finish()V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    const/4 v14, 0x0

    .line 107
    .local v14, "cursor":Landroid/database/Cursor;
    const/16 v10, 0xbb8

    .line 108
    .local v10, "Threshhold_width":I
    const/16 v9, 0xbb8

    .line 113
    .local v9, "Threshhold_height":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 114
    if-eqz v14, :cond_5

    .line 115
    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 116
    const-string v2, "width"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 117
    .local v17, "img_width":I
    const-string v2, "height"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 118
    .local v16, "img_height":I
    const-string v2, "mime_type"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 120
    .local v8, "MimeType":Ljava/lang/String;
    const-string v2, "bmp"

    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    mul-int v2, v17, v16

    const v4, 0x895440

    if-ge v2, v4, :cond_4

    :cond_3
    move/from16 v0, v17

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/16 v4, 0x10

    if-gt v2, v4, :cond_2

    .line 121
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f050002

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v2, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 122
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    if-eqz v14, :cond_0

    .line 131
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 130
    .end local v8    # "MimeType":Ljava/lang/String;
    .end local v16    # "img_height":I
    .end local v17    # "img_width":I
    :cond_5
    if-eqz v14, :cond_6

    .line 131
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 136
    :cond_6
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v11

    .line 137
    .local v11, "actionBar":Landroid/app/ActionBar;
    const/high16 v2, 0x7f030000

    invoke-virtual {v11, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 138
    invoke-virtual {v11}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 139
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/wallpapercropper/WallpaperCropActivity;->finishActivityWhenDone:Z

    .line 140
    invoke-virtual {v11}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    new-instance v4, Lcom/android/wallpapercropper/WallpaperCropActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3}, Lcom/android/wallpapercropper/WallpaperCropActivity$1;-><init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Landroid/net/Uri;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    new-instance v12, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;

    const/16 v2, 0x400

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v3, v2}, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;-><init>(Landroid/content/Context;Landroid/net/Uri;I)V

    .line 154
    .local v12, "bitmapSource":Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;
    new-instance v18, Lcom/android/wallpapercropper/WallpaperCropActivity$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12}, Lcom/android/wallpapercropper/WallpaperCropActivity$2;-><init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;)V

    .line 164
    .local v18, "onLoad":Ljava/lang/Runnable;
    const/4 v2, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v12, v2, v4, v1}, Lcom/android/wallpapercropper/WallpaperCropActivity;->setCropViewTileSource(Lcom/android/photos/BitmapRegionTileSource$BitmapSource;ZZLjava/lang/Runnable;)V

    goto/16 :goto_0

    .line 127
    .end local v11    # "actionBar":Landroid/app/ActionBar;
    .end local v12    # "bitmapSource":Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;
    .end local v18    # "onLoad":Ljava/lang/Runnable;
    :catch_0
    move-exception v15

    .line 128
    .local v15, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    if-eqz v14, :cond_6

    .line 131
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 130
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v14, :cond_7

    .line 131
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->init()V

    .line 85
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->enableRotation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->setRequestedOrientation(I)V

    .line 88
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 188
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v0}, Lcom/android/wallpapercropper/CropView;->destroy()V

    .line 191
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 170
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v0}, Lcom/android/wallpapercropper/CropView;->onPause()V

    .line 173
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 177
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 178
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_switch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity;->onDestroy()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v0}, Lcom/android/wallpapercropper/CropView;->onResume()V

    .line 183
    :cond_1
    return-void
.end method

.method public setCropViewTileSource(Lcom/android/photos/BitmapRegionTileSource$BitmapSource;ZZLjava/lang/Runnable;)V
    .locals 8
    .param p1, "bitmapSource"    # Lcom/android/photos/BitmapRegionTileSource$BitmapSource;
    .param p2, "touchEnabled"    # Z
    .param p3, "moveToLeft"    # Z
    .param p4, "postExecute"    # Ljava/lang/Runnable;

    .prologue
    .line 196
    move-object v4, p0

    .line 197
    .local v4, "context":Landroid/content/Context;
    const v1, 0x7f070003

    invoke-virtual {p0, v1}, Lcom/android/wallpapercropper/WallpaperCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 198
    .local v3, "progressView":Landroid/view/View;
    new-instance v0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/android/wallpapercropper/WallpaperCropActivity$3;-><init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Lcom/android/photos/BitmapRegionTileSource$BitmapSource;Landroid/view/View;Landroid/content/Context;ZZLjava/lang/Runnable;)V

    .line 227
    .local v0, "loadBitmapTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    new-instance v1, Lcom/android/wallpapercropper/WallpaperCropActivity$4;

    invoke-direct {v1, p0, v0, v3}, Lcom/android/wallpapercropper/WallpaperCropActivity$4;-><init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Landroid/os/AsyncTask;Landroid/view/View;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v1, v6, v7}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 234
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 235
    return-void
.end method
