.class Lcom/android/wallpapercropper/WallpaperCropActivity$3;
.super Landroid/os/AsyncTask;
.source "WallpaperCropActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/wallpapercropper/WallpaperCropActivity;->setCropViewTileSource(Lcom/android/photos/BitmapRegionTileSource$BitmapSource;ZZLjava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

.field final synthetic val$bitmapSource:Lcom/android/photos/BitmapRegionTileSource$BitmapSource;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$moveToLeft:Z

.field final synthetic val$postExecute:Ljava/lang/Runnable;

.field final synthetic val$progressView:Landroid/view/View;

.field final synthetic val$touchEnabled:Z


# direct methods
.method constructor <init>(Lcom/android/wallpapercropper/WallpaperCropActivity;Lcom/android/photos/BitmapRegionTileSource$BitmapSource;Landroid/view/View;Landroid/content/Context;ZZLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iput-object p2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$bitmapSource:Lcom/android/photos/BitmapRegionTileSource$BitmapSource;

    iput-object p3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$progressView:Landroid/view/View;

    iput-object p4, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$context:Landroid/content/Context;

    iput-boolean p5, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$touchEnabled:Z

    iput-boolean p6, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$moveToLeft:Z

    iput-object p7, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$postExecute:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 198
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$bitmapSource:Lcom/android/photos/BitmapRegionTileSource$BitmapSource;

    invoke-virtual {v0}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->loadInBackground()Z

    .line 203
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 198
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 5
    .param p1, "arg"    # Ljava/lang/Void;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 207
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$progressView:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 208
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$bitmapSource:Lcom/android/photos/BitmapRegionTileSource$BitmapSource;

    invoke-virtual {v1}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->getLoadingState()Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    move-result-object v1

    sget-object v2, Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;->LOADED:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    if-ne v1, v2, :cond_1

    .line 209
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iget-object v1, v1, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    new-instance v2, Lcom/android/photos/BitmapRegionTileSource;

    iget-object v3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$bitmapSource:Lcom/android/photos/BitmapRegionTileSource$BitmapSource;

    invoke-direct {v2, v3, v4}, Lcom/android/photos/BitmapRegionTileSource;-><init>(Landroid/content/Context;Lcom/android/photos/BitmapRegionTileSource$BitmapSource;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/wallpapercropper/CropView;->setTileSource(Lcom/android/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 211
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iget-object v1, v1, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    iget-boolean v2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$touchEnabled:Z

    invoke-virtual {v1, v2}, Lcom/android/wallpapercropper/CropView;->setTouchEnabled(Z)V

    .line 212
    iget-boolean v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$moveToLeft:Z

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    iget-object v1, v1, Lcom/android/wallpapercropper/WallpaperCropActivity;->mCropView:Lcom/android/wallpapercropper/CropView;

    invoke-virtual {v1}, Lcom/android/wallpapercropper/CropView;->moveToLeft()V

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->this$0:Lcom/android/wallpapercropper/WallpaperCropActivity;

    invoke-virtual {v1}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 216
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 219
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_1
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$postExecute:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 220
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$3;->val$postExecute:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 222
    :cond_2
    return-void
.end method
