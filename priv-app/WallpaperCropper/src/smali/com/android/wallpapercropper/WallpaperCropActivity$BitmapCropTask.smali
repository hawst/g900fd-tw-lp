.class public Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;
.super Landroid/os/AsyncTask;
.source "WallpaperCropActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpapercropper/WallpaperCropActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "BitmapCropTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mCropBounds:Landroid/graphics/RectF;

.field mCroppedBitmap:Landroid/graphics/Bitmap;

.field mInFilePath:Ljava/lang/String;

.field mInImageBytes:[B

.field mInResId:I

.field mInUri:Landroid/net/Uri;

.field mNoCrop:Z

.field mOnBitmapCroppedHandler:Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;

.field mOnEndRunnable:Ljava/lang/Runnable;

.field mOutHeight:I

.field mOutWidth:I

.field mOutputFormat:Ljava/lang/String;

.field mResources:Landroid/content/res/Resources;

.field mRotation:I

.field mSaveCroppedBitmap:Z

.field mSetWallpaper:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "inUri"    # Landroid/net/Uri;
    .param p3, "cropBounds"    # Landroid/graphics/RectF;
    .param p4, "rotation"    # I
    .param p5, "outWidth"    # I
    .param p6, "outHeight"    # I
    .param p7, "setWallpaper"    # Z
    .param p8, "saveCroppedBitmap"    # Z
    .param p9, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 498
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    .line 466
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    .line 467
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 470
    const-string v0, "jpg"

    iput-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    .line 499
    iput-object p1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    .line 500
    iput-object p2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    .line 501
    invoke-direct/range {v0 .. v7}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 503
    return-void
.end method

.method private init(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 0
    .param p1, "cropBounds"    # Landroid/graphics/RectF;
    .param p2, "rotation"    # I
    .param p3, "outWidth"    # I
    .param p4, "outHeight"    # I
    .param p5, "setWallpaper"    # Z
    .param p6, "saveCroppedBitmap"    # Z
    .param p7, "onEndRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 518
    iput p2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    .line 519
    iput p3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    .line 520
    iput p4, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    .line 521
    iput-boolean p5, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    .line 522
    iput-boolean p6, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mSaveCroppedBitmap:Z

    .line 523
    iput-object p7, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    .line 524
    return-void
.end method

.method private regenerateInputStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 540
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    if-nez v1, :cond_0

    .line 541
    const-string v1, "Launcher3.CropActivity"

    const-string v2, "cannot read original file, no input URI, resource ID, or image byte array given"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 545
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 546
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 555
    :catch_0
    move-exception v0

    .line 556
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v1, "Launcher3.CropActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot read file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 548
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 549
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    goto :goto_1

    .line 550
    :cond_2
    iget-object v1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    if-eqz v1, :cond_3

    .line 551
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInImageBytes:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_1

    .line 553
    :cond_3
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public cropBitmap()Z
    .locals 39

    .prologue
    .line 584
    const/4 v15, 0x0

    .line 587
    .local v15, "failure":Z
    const/16 v34, 0x0

    .line 588
    .local v34, "wallpaperManager":Landroid/app/WallpaperManager;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v35, v0

    if-eqz v35, :cond_0

    .line 589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v34

    .line 593
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v35, v0

    if-eqz v35, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mNoCrop:Z

    move/from16 v35, v0

    if-eqz v35, :cond_5

    .line 594
    const/16 v18, 0x0

    .line 596
    .local v18, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v18

    .line 597
    if-eqz v18, :cond_1

    .line 598
    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 599
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 606
    :cond_1
    if-eqz v18, :cond_2

    .line 607
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 613
    :cond_2
    :goto_0
    if-nez v15, :cond_4

    const/16 v35, 0x1

    .line 820
    .end local v18    # "is":Ljava/io/InputStream;
    :goto_1
    return v35

    .line 609
    .restart local v18    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v14

    .line 610
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 601
    .end local v14    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v14

    .line 602
    .local v14, "e":Ljava/io/IOException;
    :try_start_2
    const-string v35, "Launcher3.CropActivity"

    const-string v36, "cannot write stream to wallpaper"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-static {v0, v1, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 603
    const/4 v15, 0x1

    .line 606
    if-eqz v18, :cond_2

    .line 607
    :try_start_3
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 609
    :catch_2
    move-exception v14

    .line 610
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 605
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v35

    .line 606
    if-eqz v18, :cond_3

    .line 607
    :try_start_4
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 611
    :cond_3
    :goto_2
    throw v35

    .line 609
    :catch_3
    move-exception v14

    .line 610
    .restart local v14    # "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 613
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_4
    const/16 v35, 0x0

    goto :goto_1

    .line 616
    .end local v18    # "is":Ljava/io/InputStream;
    :cond_5
    new-instance v30, Landroid/graphics/Rect;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/Rect;-><init>()V

    .line 617
    .local v30, "roundedTrueCrop":Landroid/graphics/Rect;
    new-instance v28, Landroid/graphics/Matrix;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Matrix;-><init>()V

    .line 618
    .local v28, "rotateMatrix":Landroid/graphics/Matrix;
    new-instance v17, Landroid/graphics/Matrix;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Matrix;-><init>()V

    .line 619
    .local v17, "inverseRotateMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v35, v0

    if-lez v35, :cond_7

    .line 620
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move-object/from16 v0, v28

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 621
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v35, v0

    move/from16 v0, v35

    neg-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move-object/from16 v0, v17

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 624
    new-instance v35, Landroid/graphics/RectF;

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    .line 626
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->getImageBounds()Landroid/graphics/Point;

    move-result-object v5

    .line 627
    .local v5, "bounds":Landroid/graphics/Point;
    if-nez v5, :cond_6

    .line 628
    const-string v35, "Launcher3.CropActivity"

    const-string v36, "cannot get bounds for image"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    const/4 v15, 0x1

    .line 630
    const/16 v35, 0x0

    goto/16 :goto_1

    .line 633
    :cond_6
    const/16 v35, 0x2

    move/from16 v0, v35

    new-array v0, v0, [F

    move-object/from16 v29, v0

    const/16 v35, 0x0

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    aput v36, v29, v35

    const/16 v35, 0x1

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    aput v36, v29, v35

    .line 634
    .local v29, "rotatedBounds":[F
    invoke-virtual/range {v28 .. v29}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 635
    const/16 v35, 0x0

    const/16 v36, 0x0

    aget v36, v29, v36

    invoke-static/range {v36 .. v36}, Ljava/lang/Math;->abs(F)F

    move-result v36

    aput v36, v29, v35

    .line 636
    const/16 v35, 0x1

    const/16 v36, 0x1

    aget v36, v29, v36

    invoke-static/range {v36 .. v36}, Ljava/lang/Math;->abs(F)F

    move-result v36

    aput v36, v29, v35

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    aget v36, v29, v36

    move/from16 v0, v36

    neg-float v0, v0

    move/from16 v36, v0

    const/high16 v37, 0x40000000    # 2.0f

    div-float v36, v36, v37

    const/16 v37, 0x1

    aget v37, v29, v37

    move/from16 v0, v37

    neg-float v0, v0

    move/from16 v37, v0

    const/high16 v38, 0x40000000    # 2.0f

    div-float v37, v37, v38

    invoke-virtual/range {v35 .. v37}, Landroid/graphics/RectF;->offset(FF)V

    .line 639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    div-int/lit8 v36, v36, 0x2

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    div-int/lit8 v37, v37, 0x2

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Landroid/graphics/RectF;->offset(FF)V

    .line 644
    .end local v5    # "bounds":Landroid/graphics/Point;
    .end local v29    # "rotatedBounds":[F
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 646
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v35, v0

    if-gez v35, :cond_9

    const/16 v35, 0x0

    :goto_3
    move/from16 v0, v35

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 647
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v35, v0

    if-gez v35, :cond_a

    const/16 v35, 0x0

    :goto_4
    move/from16 v0, v35

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 649
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->width()I

    move-result v35

    if-lez v35, :cond_8

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->height()I

    move-result v35

    if-gtz v35, :cond_b

    .line 650
    :cond_8
    const-string v35, "Launcher3.CropActivity"

    const-string v36, "crop has bad values for full size image"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    const/4 v15, 0x1

    .line 652
    const/16 v35, 0x0

    goto/16 :goto_1

    .line 646
    :cond_9
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v35, v0

    goto :goto_3

    .line 647
    :cond_a
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v35, v0

    goto :goto_4

    .line 656
    :cond_b
    const/16 v31, 0x1

    .line 657
    .local v31, "scaleDownSampleSize":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v35, v0

    if-eqz v35, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v35, v0

    if-eqz v35, :cond_c

    .line 658
    const/16 v35, 0x1

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->width()I

    move-result v36

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v37, v0

    div-int v36, v36, v37

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->height()I

    move-result v37

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v38, v0

    div-int v37, v37, v38

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(II)I

    move-result v36

    invoke-static/range {v35 .. v36}, Ljava/lang/Math;->max(II)I

    move-result v31

    .line 662
    :cond_c
    const/4 v12, 0x0

    .line 663
    .local v12, "decoder":Landroid/graphics/BitmapRegionDecoder;
    const/16 v18, 0x0

    .line 665
    .restart local v18    # "is":Ljava/io/InputStream;
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v18

    .line 666
    if-nez v18, :cond_d

    .line 667
    const-string v35, "Launcher3.CropActivity"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "cannot get input stream for uri="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 668
    const/4 v15, 0x1

    .line 669
    const/16 v35, 0x0

    .line 676
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 677
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 671
    :cond_d
    const/16 v35, 0x0

    :try_start_6
    move-object/from16 v0, v18

    move/from16 v1, v35

    invoke-static {v0, v1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v12

    .line 672
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 676
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 677
    const/16 v18, 0x0

    .line 680
    :goto_5
    const/4 v10, 0x0

    .line 682
    .local v10, "crop":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_f

    .line 684
    :try_start_7
    new-instance v24, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 685
    .local v24, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v35, 0x1

    move/from16 v0, v31

    move/from16 v1, v35

    if-le v0, v1, :cond_e

    .line 686
    move/from16 v0, v31

    move-object/from16 v1, v24

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 688
    :cond_e
    const-string v35, "Launcher3.CropActivity"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "roundedTrueCrop : "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 690
    invoke-virtual {v12}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 693
    .end local v24    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_f
    if-nez v10, :cond_12

    .line 695
    invoke-direct/range {p0 .. p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v18

    .line 696
    const/16 v16, 0x0

    .line 697
    .local v16, "fullSize":Landroid/graphics/Bitmap;
    if-eqz v18, :cond_11

    .line 698
    new-instance v24, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 699
    .restart local v24    # "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v35, 0x1

    move/from16 v0, v31

    move/from16 v1, v35

    if-le v0, v1, :cond_10

    .line 700
    move/from16 v0, v31

    move-object/from16 v1, v24

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 702
    :cond_10
    const/16 v35, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v35

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 704
    .end local v24    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_11
    if-eqz v16, :cond_12

    .line 706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v36, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v36, v36, v37

    move/from16 v0, v36

    move-object/from16 v1, v35

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v36, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v36, v36, v37

    move/from16 v0, v36

    move-object/from16 v1, v35

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v36, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v36, v36, v37

    move/from16 v0, v36

    move-object/from16 v1, v35

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v36, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v37, v0

    div-float v36, v36, v37

    move/from16 v0, v36

    move-object/from16 v1, v35

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCropBounds:Landroid/graphics/RectF;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 712
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v35, v0

    if-gez v35, :cond_13

    const/16 v35, 0x0

    :goto_6
    move/from16 v0, v35

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 713
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v35, v0

    if-gez v35, :cond_14

    const/16 v35, 0x0

    :goto_7
    move/from16 v0, v35

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 714
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v35, v0

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->height()I

    move-result v36

    add-int v35, v35, v36

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v36

    move/from16 v0, v35

    move/from16 v1, v36

    if-gt v0, v1, :cond_12

    .line 715
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v35, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v36, v0

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->width()I

    move-result v37

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->height()I

    move-result v38

    move-object/from16 v0, v16

    move/from16 v1, v35

    move/from16 v2, v36

    move/from16 v3, v37

    move/from16 v4, v38

    invoke-static {v0, v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v10

    .line 726
    .end local v16    # "fullSize":Landroid/graphics/Bitmap;
    :cond_12
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 729
    :goto_8
    if-nez v10, :cond_15

    .line 730
    const-string v35, "Launcher3.CropActivity"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "cannot decode file: "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const/4 v15, 0x1

    .line 732
    const/16 v35, 0x0

    goto/16 :goto_1

    .line 673
    .end local v10    # "crop":Landroid/graphics/Bitmap;
    :catch_4
    move-exception v14

    .line 674
    .local v14, "e":Ljava/io/IOException;
    :try_start_8
    const-string v35, "Launcher3.CropActivity"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "cannot open region decoder for file: "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mInUri:Landroid/net/Uri;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-static {v0, v1, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 676
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 677
    const/16 v18, 0x0

    .line 678
    goto/16 :goto_5

    .line 676
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v35

    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 677
    const/16 v18, 0x0

    throw v35

    .line 712
    .restart local v10    # "crop":Landroid/graphics/Bitmap;
    .restart local v16    # "fullSize":Landroid/graphics/Bitmap;
    :cond_13
    :try_start_9
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v35, v0

    goto/16 :goto_6

    .line 713
    :cond_14
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v35, v0
    :try_end_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto/16 :goto_7

    .line 721
    .end local v16    # "fullSize":Landroid/graphics/Bitmap;
    :catch_5
    move-exception v14

    .line 722
    .local v14, "e":Ljava/lang/OutOfMemoryError;
    :try_start_a
    invoke-virtual {v14}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 726
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_8

    .line 723
    .end local v14    # "e":Ljava/lang/OutOfMemoryError;
    :catch_6
    move-exception v14

    .line 724
    .local v14, "e":Ljava/lang/Exception;
    :try_start_b
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 726
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_8

    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v35

    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v35

    .line 735
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v35, v0

    if-lez v35, :cond_16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v35, v0

    if-gtz v35, :cond_17

    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v35, v0

    if-lez v35, :cond_1a

    .line 736
    :cond_17
    const/16 v35, 0x2

    move/from16 v0, v35

    new-array v13, v0, [F

    const/16 v35, 0x0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v36

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    aput v36, v13, v35

    const/16 v35, 0x1

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v36

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    aput v36, v13, v35

    .line 737
    .local v13, "dimsAfter":[F
    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 738
    const/16 v35, 0x0

    const/16 v36, 0x0

    aget v36, v13, v36

    invoke-static/range {v36 .. v36}, Ljava/lang/Math;->abs(F)F

    move-result v36

    aput v36, v13, v35

    .line 739
    const/16 v35, 0x1

    const/16 v36, 0x1

    aget v36, v13, v36

    invoke-static/range {v36 .. v36}, Ljava/lang/Math;->abs(F)F

    move-result v36

    aput v36, v13, v35

    .line 741
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v35, v0

    if-lez v35, :cond_18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v35, v0

    if-gtz v35, :cond_19

    .line 742
    :cond_18
    const/16 v35, 0x0

    aget v35, v13, v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->round(F)I

    move-result v35

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    .line 743
    const/16 v35, 0x1

    aget v35, v13, v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->round(F)I

    move-result v35

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    .line 746
    :cond_19
    new-instance v11, Landroid/graphics/RectF;

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    aget v37, v13, v37

    const/16 v38, 0x1

    aget v38, v13, v38

    move/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 747
    .local v11, "cropRect":Landroid/graphics/RectF;
    new-instance v27, Landroid/graphics/RectF;

    const/16 v35, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutWidth:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutHeight:I

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    move-object/from16 v0, v27

    move/from16 v1, v35

    move/from16 v2, v36

    move/from16 v3, v37

    move/from16 v4, v38

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 749
    .local v27, "returnRect":Landroid/graphics/RectF;
    new-instance v19, Landroid/graphics/Matrix;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Matrix;-><init>()V

    .line 750
    .local v19, "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v35, v0

    if-nez v35, :cond_1d

    .line 751
    sget-object v35, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v35

    invoke-virtual {v0, v11, v1, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 770
    :goto_9
    :try_start_c
    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->width()F

    move-result v35

    move/from16 v0, v35

    float-to-int v0, v0

    move/from16 v35, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->height()F

    move-result v36

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v36, v0

    sget-object v37, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v35 .. v37}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v32

    .line 772
    .local v32, "tmp":Landroid/graphics/Bitmap;
    if-eqz v32, :cond_1a

    .line 773
    new-instance v6, Landroid/graphics/Canvas;

    move-object/from16 v0, v32

    invoke-direct {v6, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 774
    .local v6, "c":Landroid/graphics/Canvas;
    new-instance v26, Landroid/graphics/Paint;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Paint;-><init>()V

    .line 775
    .local v26, "p":Landroid/graphics/Paint;
    const/16 v35, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 776
    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v6, v10, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_7
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    .line 777
    move-object/from16 v10, v32

    .line 787
    .end local v6    # "c":Landroid/graphics/Canvas;
    .end local v11    # "cropRect":Landroid/graphics/RectF;
    .end local v13    # "dimsAfter":[F
    .end local v19    # "m":Landroid/graphics/Matrix;
    .end local v26    # "p":Landroid/graphics/Paint;
    .end local v27    # "returnRect":Landroid/graphics/RectF;
    .end local v32    # "tmp":Landroid/graphics/Bitmap;
    :cond_1a
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mSaveCroppedBitmap:Z

    move/from16 v35, v0

    if-eqz v35, :cond_1b

    .line 788
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mCroppedBitmap:Landroid/graphics/Bitmap;

    .line 792
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOutputFormat:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-static/range {v35 .. v35}, Lcom/android/wallpapercropper/WallpaperCropActivity;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Lcom/android/wallpapercropper/WallpaperCropActivity;->convertExtensionToCompressFormat(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v9

    .line 796
    .local v9, "cf":Landroid/graphics/Bitmap$CompressFormat;
    new-instance v33, Ljava/io/ByteArrayOutputStream;

    const/16 v35, 0x800

    move-object/from16 v0, v33

    move/from16 v1, v35

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 797
    .local v33, "tmpOut":Ljava/io/ByteArrayOutputStream;
    const/16 v35, 0x5a

    move/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v10, v9, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v35

    if-eqz v35, :cond_1e

    .line 799
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mSetWallpaper:Z

    move/from16 v35, v0

    if-eqz v35, :cond_1c

    if-eqz v34, :cond_1c

    .line 801
    :try_start_d
    invoke-virtual/range {v33 .. v33}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v25

    .line 802
    .local v25, "outByteArray":[B
    new-instance v35, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual/range {v34 .. v35}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOnBitmapCroppedHandler:Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;

    move-object/from16 v35, v0

    if-eqz v35, :cond_1c

    .line 804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOnBitmapCroppedHandler:Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;->onBitmapCropped([B)V
    :try_end_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_9
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 820
    .end local v25    # "outByteArray":[B
    :cond_1c
    :goto_b
    if-nez v15, :cond_1f

    const/16 v35, 0x1

    goto/16 :goto_1

    .line 753
    .end local v9    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .end local v33    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    .restart local v11    # "cropRect":Landroid/graphics/RectF;
    .restart local v13    # "dimsAfter":[F
    .restart local v19    # "m":Landroid/graphics/Matrix;
    .restart local v27    # "returnRect":Landroid/graphics/RectF;
    :cond_1d
    new-instance v20, Landroid/graphics/Matrix;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Matrix;-><init>()V

    .line 754
    .local v20, "m1":Landroid/graphics/Matrix;
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v35

    move/from16 v0, v35

    neg-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    const/high16 v36, 0x40000000    # 2.0f

    div-float v35, v35, v36

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v36

    move/from16 v0, v36

    neg-int v0, v0

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    const/high16 v37, 0x40000000    # 2.0f

    div-float v36, v36, v37

    move-object/from16 v0, v20

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 755
    new-instance v21, Landroid/graphics/Matrix;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Matrix;-><init>()V

    .line 756
    .local v21, "m2":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mRotation:I

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move-object/from16 v0, v21

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 757
    new-instance v22, Landroid/graphics/Matrix;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Matrix;-><init>()V

    .line 758
    .local v22, "m3":Landroid/graphics/Matrix;
    const/16 v35, 0x0

    aget v35, v13, v35

    const/high16 v36, 0x40000000    # 2.0f

    div-float v35, v35, v36

    const/16 v36, 0x1

    aget v36, v13, v36

    const/high16 v37, 0x40000000    # 2.0f

    div-float v36, v36, v37

    move-object/from16 v0, v22

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 759
    new-instance v23, Landroid/graphics/Matrix;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Matrix;-><init>()V

    .line 760
    .local v23, "m4":Landroid/graphics/Matrix;
    sget-object v35, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v35

    invoke-virtual {v0, v11, v1, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 762
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 763
    .local v7, "c1":Landroid/graphics/Matrix;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 764
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 765
    .local v8, "c2":Landroid/graphics/Matrix;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 766
    move-object/from16 v0, v19

    invoke-virtual {v0, v8, v7}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    goto/16 :goto_9

    .line 780
    .end local v7    # "c1":Landroid/graphics/Matrix;
    .end local v8    # "c2":Landroid/graphics/Matrix;
    .end local v20    # "m1":Landroid/graphics/Matrix;
    .end local v21    # "m2":Landroid/graphics/Matrix;
    .end local v22    # "m3":Landroid/graphics/Matrix;
    .end local v23    # "m4":Landroid/graphics/Matrix;
    :catch_7
    move-exception v14

    .line 781
    .local v14, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v14}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_a

    .line 782
    .end local v14    # "e":Ljava/lang/OutOfMemoryError;
    :catch_8
    move-exception v14

    .line 783
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_a

    .line 806
    .end local v11    # "cropRect":Landroid/graphics/RectF;
    .end local v13    # "dimsAfter":[F
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v19    # "m":Landroid/graphics/Matrix;
    .end local v27    # "returnRect":Landroid/graphics/RectF;
    .restart local v9    # "cf":Landroid/graphics/Bitmap$CompressFormat;
    .restart local v33    # "tmpOut":Ljava/io/ByteArrayOutputStream;
    :catch_9
    move-exception v14

    .line 807
    .local v14, "e":Ljava/lang/OutOfMemoryError;
    const/4 v15, 0x1

    .line 808
    invoke-virtual {v14}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_b

    .line 809
    .end local v14    # "e":Ljava/lang/OutOfMemoryError;
    :catch_a
    move-exception v14

    .line 810
    .local v14, "e":Ljava/io/IOException;
    const-string v35, "Launcher3.CropActivity"

    const-string v36, "cannot write stream to wallpaper"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-static {v0, v1, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 811
    const/4 v15, 0x1

    .line 812
    goto/16 :goto_b

    .line 815
    .end local v14    # "e":Ljava/io/IOException;
    :cond_1e
    const-string v35, "Launcher3.CropActivity"

    const-string v36, "cannot compress bitmap"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    const/4 v15, 0x1

    goto/16 :goto_b

    .line 820
    :cond_1f
    const/16 v35, 0x0

    goto/16 :goto_1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 825
    invoke-virtual {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->cropBitmap()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 461
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getImageBounds()Landroid/graphics/Point;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 563
    invoke-direct {p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 564
    .local v0, "is":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 565
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 566
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 567
    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 568
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 569
    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eqz v3, :cond_0

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eqz v3, :cond_0

    .line 570
    new-instance v2, Landroid/graphics/Point;

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 573
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    return-object v2
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 830
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOnEndRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 835
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 836
    iget-object v0, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask$1;

    invoke-direct {v1, p0}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask$1;-><init>(Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 843
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 461
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method public setOnBitmapCropped(Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/android/wallpapercropper/WallpaperCropActivity$BitmapCropTask;->mOnBitmapCroppedHandler:Lcom/android/wallpapercropper/WallpaperCropActivity$OnBitmapCroppedHandler;

    .line 528
    return-void
.end method
