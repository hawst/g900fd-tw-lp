.class public Lcom/android/wallpapercropper/CropView;
.super Lcom/android/photos/views/TiledImageView;
.source "CropView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpapercropper/CropView$TouchCallback;
    }
.end annotation


# instance fields
.field private mCenterX:F

.field private mCenterY:F

.field private mFirstX:F

.field private mFirstY:F

.field mInverseRotateMatrix:Landroid/graphics/Matrix;

.field private mLastX:F

.field private mLastY:F

.field private mMinScale:F

.field mRotateMatrix:Landroid/graphics/Matrix;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mTempAdjustment:[F

.field private mTempCoef:[F

.field private mTempEdges:Landroid/graphics/RectF;

.field private mTempImageDims:[F

.field private mTempPoint:[F

.field private mTempRendererCenter:[F

.field mTouchCallback:Lcom/android/wallpapercropper/CropView$TouchCallback;

.field private mTouchDownTime:J

.field private mTouchEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/wallpapercropper/CropView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x2

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/android/photos/views/TiledImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/wallpapercropper/CropView;->mTouchEnabled:Z

    .line 44
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempEdges:Landroid/graphics/RectF;

    .line 45
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempPoint:[F

    .line 46
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempCoef:[F

    .line 47
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempAdjustment:[F

    .line 48
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempImageDims:[F

    .line 49
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempRendererCenter:[F

    .line 66
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 67
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRotateMatrix:Landroid/graphics/Matrix;

    .line 68
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/wallpapercropper/CropView;->mInverseRotateMatrix:Landroid/graphics/Matrix;

    .line 69
    return-void

    .line 45
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 47
    :array_2
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 48
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 49
    :array_4
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private getEdgesHelper(Landroid/graphics/RectF;)V
    .locals 19
    .param p1, "edgesOut"    # Landroid/graphics/RectF;

    .prologue
    .line 84
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v15, v0

    .line 85
    .local v15, "width":F
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v4, v0

    .line 86
    .local v4, "height":F
    invoke-direct/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getImageDims()[F

    move-result-object v5

    .line 87
    .local v5, "imageDims":[F
    const/16 v16, 0x0

    aget v7, v5, v16

    .line 88
    .local v7, "imageWidth":F
    const/16 v16, 0x1

    aget v6, v5, v16

    .line 90
    .local v6, "imageHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/android/photos/views/TiledImageRenderer$TileSource;->getImageWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    div-float v8, v16, v17

    .line 91
    .local v8, "initialCenterX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/android/photos/views/TiledImageRenderer$TileSource;->getImageHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    div-float v9, v16, v17

    .line 93
    .local v9, "initialCenterY":F
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/wallpapercropper/CropView;->mTempRendererCenter:[F

    .line 94
    .local v11, "rendererCenter":[F
    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    move/from16 v17, v0

    sub-float v17, v17, v8

    aput v17, v11, v16

    .line 95
    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    move/from16 v17, v0

    sub-float v17, v17, v9

    aput v17, v11, v16

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRotateMatrix:Landroid/graphics/Matrix;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 97
    const/16 v16, 0x0

    aget v17, v11, v16

    const/high16 v18, 0x40000000    # 2.0f

    div-float v18, v7, v18

    add-float v17, v17, v18

    aput v17, v11, v16

    .line 98
    const/16 v16, 0x1

    aget v17, v11, v16

    const/high16 v18, 0x40000000    # 2.0f

    div-float v18, v6, v18

    add-float v17, v17, v18

    aput v17, v11, v16

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v13, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 101
    .local v13, "scale":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v15, v16

    const/16 v17, 0x0

    aget v17, v11, v17

    sub-float v16, v16, v17

    sub-float v17, v7, v15

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    add-float v16, v16, v17

    mul-float v16, v16, v13

    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v15, v17

    add-float v2, v16, v17

    .line 103
    .local v2, "centerX":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v4, v16

    const/16 v17, 0x1

    aget v17, v11, v17

    sub-float v16, v16, v17

    sub-float v17, v6, v4

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    add-float v16, v16, v17

    mul-float v16, v16, v13

    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v4, v17

    add-float v3, v16, v17

    .line 105
    .local v3, "centerY":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v7, v16

    mul-float v16, v16, v13

    sub-float v10, v2, v16

    .line 106
    .local v10, "leftEdge":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v7, v16

    mul-float v16, v16, v13

    add-float v12, v2, v16

    .line 107
    .local v12, "rightEdge":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v6, v16

    mul-float v16, v16, v13

    sub-float v14, v3, v16

    .line 108
    .local v14, "topEdge":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v6, v16

    mul-float v16, v16, v13

    add-float v1, v3, v16

    .line 110
    .local v1, "bottomEdge":F
    move-object/from16 v0, p1

    iput v10, v0, Landroid/graphics/RectF;->left:F

    .line 111
    move-object/from16 v0, p1

    iput v12, v0, Landroid/graphics/RectF;->right:F

    .line 112
    move-object/from16 v0, p1

    iput v14, v0, Landroid/graphics/RectF;->top:F

    .line 113
    move-object/from16 v0, p1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 114
    return-void
.end method

.method private getImageDims()[F
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 72
    iget-object v3, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget-object v3, v3, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v3}, Lcom/android/photos/views/TiledImageRenderer$TileSource;->getImageWidth()I

    move-result v3

    int-to-float v2, v3

    .line 73
    .local v2, "imageWidth":F
    iget-object v3, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget-object v3, v3, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v3}, Lcom/android/photos/views/TiledImageRenderer$TileSource;->getImageHeight()I

    move-result v3

    int-to-float v1, v3

    .line 74
    .local v1, "imageHeight":F
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempImageDims:[F

    .line 75
    .local v0, "imageDims":[F
    aput v2, v0, v4

    .line 76
    aput v1, v0, v5

    .line 77
    iget-object v3, p0, Lcom/android/wallpapercropper/CropView;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 78
    aget v3, v0, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    aput v3, v0, v4

    .line 79
    aget v3, v0, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    aput v3, v0, v5

    .line 80
    return-object v0
.end method

.method private updateCenter()V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v1, p0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->centerX:I

    .line 220
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v1, p0, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->centerY:I

    .line 221
    return-void
.end method

.method private updateMinScale(IILcom/android/photos/views/TiledImageRenderer$TileSource;Z)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "source"    # Lcom/android/photos/views/TiledImageRenderer$TileSource;
    .param p4, "resetScale"    # Z

    .prologue
    .line 167
    iget-object v4, p0, Lcom/android/wallpapercropper/CropView;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 168
    if-eqz p4, :cond_0

    .line 169
    :try_start_0
    iget-object v3, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v3, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 171
    :cond_0
    if-eqz p3, :cond_1

    .line 172
    invoke-direct {p0}, Lcom/android/wallpapercropper/CropView;->getImageDims()[F

    move-result-object v0

    .line 173
    .local v0, "imageDims":[F
    const/4 v3, 0x0

    aget v2, v0, v3

    .line 174
    .local v2, "imageWidth":F
    const/4 v3, 0x1

    aget v1, v0, v3

    .line 175
    .local v1, "imageHeight":F
    int-to-float v3, p1

    div-float/2addr v3, v2

    int-to-float v5, p2

    div-float/2addr v5, v1

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, p0, Lcom/android/wallpapercropper/CropView;->mMinScale:F

    .line 176
    iget-object v5, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v6, p0, Lcom/android/wallpapercropper/CropView;->mMinScale:F

    if-eqz p4, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-static {v6, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, v5, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 179
    .end local v0    # "imageDims":[F
    .end local v1    # "imageHeight":F
    .end local v2    # "imageWidth":F
    :cond_1
    monitor-exit v4

    .line 180
    return-void

    .line 176
    .restart local v0    # "imageDims":[F
    .restart local v1    # "imageHeight":F
    .restart local v2    # "imageWidth":F
    :cond_2
    iget-object v3, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v3, v3, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    goto :goto_0

    .line 179
    .end local v0    # "imageDims":[F
    .end local v1    # "imageHeight":F
    .end local v2    # "imageWidth":F
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method


# virtual methods
.method public enableRotation()Z
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public getCrop()Landroid/graphics/RectF;
    .locals 8

    .prologue
    .line 121
    iget-object v7, p0, Lcom/android/wallpapercropper/CropView;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 122
    :try_start_0
    iget-object v4, p0, Lcom/android/wallpapercropper/CropView;->mTempEdges:Landroid/graphics/RectF;

    .line 123
    .local v4, "edges":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget-object v6, v6, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    if-eqz v6, :cond_0

    .line 124
    invoke-direct {p0, v4}, Lcom/android/wallpapercropper/CropView;->getEdgesHelper(Landroid/graphics/RectF;)V

    .line 125
    :cond_0
    iget-object v6, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v5, v6, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 127
    .local v5, "scale":F
    iget v6, v4, Landroid/graphics/RectF;->left:F

    neg-float v6, v6

    div-float v1, v6, v5

    .line 128
    .local v1, "cropLeft":F
    iget v6, v4, Landroid/graphics/RectF;->top:F

    neg-float v6, v6

    div-float v3, v6, v5

    .line 129
    .local v3, "cropTop":F
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v5

    add-float v2, v1, v6

    .line 130
    .local v2, "cropRight":F
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v5

    add-float v0, v3, v6

    .line 132
    .local v0, "cropBottom":F
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v1, v3, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    monitor-exit v7

    return-object v6

    .line 133
    .end local v0    # "cropBottom":F
    .end local v1    # "cropLeft":F
    .end local v2    # "cropRight":F
    .end local v3    # "cropTop":F
    .end local v4    # "edges":Landroid/graphics/RectF;
    .end local v5    # "scale":F
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public getImageRotation()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->rotation:I

    return v0
.end method

.method public getSourceDimensions()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 137
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget-object v1, v1, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v1}, Lcom/android/photos/views/TiledImageRenderer$TileSource;->getImageWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget-object v2, v2, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    invoke-interface {v2}, Lcom/android/photos/views/TiledImageRenderer$TileSource;->getImageHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public moveToLeft()V
    .locals 8

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getHeight()I

    move-result v3

    if-nez v3, :cond_1

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 204
    .local v1, "observer":Landroid/view/ViewTreeObserver;
    new-instance v3, Lcom/android/wallpapercropper/CropView$1;

    invoke-direct {v3, p0}, Lcom/android/wallpapercropper/CropView$1;-><init>(Lcom/android/wallpapercropper/CropView;)V

    invoke-virtual {v1, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 211
    .end local v1    # "observer":Landroid/view/ViewTreeObserver;
    :cond_1
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mTempEdges:Landroid/graphics/RectF;

    .line 212
    .local v0, "edges":Landroid/graphics/RectF;
    invoke-direct {p0, v0}, Lcom/android/wallpapercropper/CropView;->getEdgesHelper(Landroid/graphics/RectF;)V

    .line 213
    iget-object v3, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v2, v3, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 214
    .local v2, "scale":F
    iget v3, p0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    float-to-double v4, v3

    iget v3, v0, Landroid/graphics/RectF;->left:F

    div-float/2addr v3, v2

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    add-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, p0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    .line 215
    invoke-direct {p0}, Lcom/android/wallpapercropper/CropView;->updateCenter()V

    .line 216
    return-void
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v1, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 192
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v1, p0, Lcom/android/wallpapercropper/CropView;->mMinScale:F

    iget-object v2, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v2, v2, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    .line 193
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->invalidate()V

    .line 194
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 184
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 199
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->enableRotation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget-object v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/wallpapercropper/CropView;->updateMinScale(IILcom/android/photos/views/TiledImageRenderer$TileSource;Z)V

    .line 154
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 30
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    .line 234
    .local v4, "action":I
    const/16 v25, 0x6

    move/from16 v0, v25

    if-ne v4, v0, :cond_0

    const/16 v16, 0x1

    .line 235
    .local v16, "pointerUp":Z
    :goto_0
    if-eqz v16, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v18

    .line 238
    .local v18, "skipIndex":I
    :goto_1
    const/16 v21, 0x0

    .local v21, "sumX":F
    const/16 v22, 0x0

    .line 239
    .local v22, "sumY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    .line 240
    .local v8, "count":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    if-ge v12, v8, :cond_3

    .line 241
    move/from16 v0, v18

    if-ne v0, v12, :cond_2

    .line 240
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 234
    .end local v8    # "count":I
    .end local v12    # "i":I
    .end local v16    # "pointerUp":Z
    .end local v18    # "skipIndex":I
    .end local v21    # "sumX":F
    .end local v22    # "sumY":F
    :cond_0
    const/16 v16, 0x0

    goto :goto_0

    .line 235
    .restart local v16    # "pointerUp":Z
    :cond_1
    const/16 v18, -0x1

    goto :goto_1

    .line 243
    .restart local v8    # "count":I
    .restart local v12    # "i":I
    .restart local v18    # "skipIndex":I
    .restart local v21    # "sumX":F
    .restart local v22    # "sumY":F
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/MotionEvent;->getX(I)F

    move-result v25

    add-float v21, v21, v25

    .line 244
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/MotionEvent;->getY(I)F

    move-result v25

    add-float v22, v22, v25

    goto :goto_3

    .line 246
    :cond_3
    if-eqz v16, :cond_5

    add-int/lit8 v10, v8, -0x1

    .line 247
    .local v10, "div":I
    :goto_4
    int-to-float v0, v10

    move/from16 v25, v0

    div-float v23, v21, v25

    .line 248
    .local v23, "x":F
    int-to-float v0, v10

    move/from16 v25, v0

    div-float v24, v22, v25

    .line 250
    .local v24, "y":F
    if-nez v4, :cond_6

    .line 251
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mFirstX:F

    .line 252
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mFirstY:F

    .line 253
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/wallpapercropper/CropView;->mTouchDownTime:J

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchCallback:Lcom/android/wallpapercropper/CropView$TouchCallback;

    move-object/from16 v25, v0

    if-eqz v25, :cond_4

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchCallback:Lcom/android/wallpapercropper/CropView$TouchCallback;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/android/wallpapercropper/CropView$TouchCallback;->onTouchDown()V

    .line 273
    :cond_4
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchEnabled:Z

    move/from16 v25, v0

    if-nez v25, :cond_8

    .line 274
    const/16 v25, 0x1

    .line 328
    :goto_6
    return v25

    .end local v10    # "div":I
    .end local v23    # "x":F
    .end local v24    # "y":F
    :cond_5
    move v10, v8

    .line 246
    goto :goto_4

    .line 257
    .restart local v10    # "div":I
    .restart local v23    # "x":F
    .restart local v24    # "y":F
    :cond_6
    const/16 v25, 0x1

    move/from16 v0, v25

    if-ne v4, v0, :cond_4

    .line 258
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getContext()Landroid/content/Context;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v7

    .line 260
    .local v7, "config":Landroid/view/ViewConfiguration;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mFirstX:F

    move/from16 v25, v0

    sub-float v25, v25, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mFirstX:F

    move/from16 v26, v0

    sub-float v26, v26, v23

    mul-float v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mFirstY:F

    move/from16 v26, v0

    sub-float v26, v26, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mFirstY:F

    move/from16 v27, v0

    sub-float v27, v27, v24

    mul-float v26, v26, v27

    add-float v20, v25, v26

    .line 261
    .local v20, "squaredDist":F
    invoke-virtual {v7}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v25

    invoke-virtual {v7}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v26

    mul-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v19, v0

    .line 262
    .local v19, "slop":F
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 263
    .local v14, "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchCallback:Lcom/android/wallpapercropper/CropView$TouchCallback;

    move-object/from16 v25, v0

    if-eqz v25, :cond_4

    .line 265
    cmpg-float v25, v20, v19

    if-gez v25, :cond_7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchDownTime:J

    move-wide/from16 v26, v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v25

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v28, v0

    add-long v26, v26, v28

    cmp-long v25, v14, v26

    if-gez v25, :cond_7

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchCallback:Lcom/android/wallpapercropper/CropView$TouchCallback;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/android/wallpapercropper/CropView$TouchCallback;->onTap()V

    .line 269
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTouchCallback:Lcom/android/wallpapercropper/CropView$TouchCallback;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/android/wallpapercropper/CropView$TouchCallback;->onTouchUp()V

    goto/16 :goto_5

    .line 277
    .end local v7    # "config":Landroid/view/ViewConfiguration;
    .end local v14    # "now":J
    .end local v19    # "slop":F
    .end local v20    # "squaredDist":F
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mLock:Ljava/lang/Object;

    move-object/from16 v26, v0

    monitor-enter v26

    .line 278
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 279
    packed-switch v4, :pswitch_data_0

    .line 291
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->source:Lcom/android/photos/views/TiledImageRenderer$TileSource;

    move-object/from16 v25, v0

    if-eqz v25, :cond_f

    .line 294
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/wallpapercropper/CropView;->mTempEdges:Landroid/graphics/RectF;

    .line 295
    .local v11, "edges":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/wallpapercropper/CropView;->getEdgesHelper(Landroid/graphics/RectF;)V

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    move/from16 v17, v0

    .line 298
    .local v17, "scale":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/wallpapercropper/CropView;->mTempCoef:[F

    .line 299
    .local v6, "coef":[F
    const/16 v25, 0x0

    const/high16 v27, 0x3f800000    # 1.0f

    aput v27, v6, v25

    .line 300
    const/16 v25, 0x1

    const/high16 v27, 0x3f800000    # 1.0f

    aput v27, v6, v25

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRotateMatrix:Landroid/graphics/Matrix;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 302
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/wallpapercropper/CropView;->mTempAdjustment:[F

    .line 303
    .local v5, "adjustment":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTempAdjustment:[F

    move-object/from16 v25, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    aput v28, v25, v27

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mTempAdjustment:[F

    move-object/from16 v25, v0

    const/16 v27, 0x1

    const/16 v28, 0x0

    aput v28, v25, v27

    .line 305
    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    const/16 v27, 0x0

    cmpl-float v25, v25, v27

    if-lez v25, :cond_c

    .line 306
    const/16 v25, 0x0

    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v27, v0

    div-float v27, v27, v17

    aput v27, v5, v25

    .line 310
    :cond_9
    :goto_8
    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    const/16 v27, 0x0

    cmpl-float v25, v25, v27

    if-lez v25, :cond_d

    .line 311
    const/16 v25, 0x1

    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v27, v0

    div-float v27, v27, v17

    invoke-static/range {v27 .. v27}, Landroid/util/FloatMath;->ceil(F)F

    move-result v27

    aput v27, v5, v25

    .line 315
    :cond_a
    :goto_9
    const/4 v9, 0x0

    .local v9, "dim":I
    :goto_a
    const/16 v25, 0x1

    move/from16 v0, v25

    if-gt v9, v0, :cond_e

    .line 316
    aget v25, v6, v9

    const/16 v27, 0x0

    cmpl-float v25, v25, v27

    if-lez v25, :cond_b

    aget v25, v5, v9

    invoke-static/range {v25 .. v25}, Landroid/util/FloatMath;->ceil(F)F

    move-result v25

    aput v25, v5, v9

    .line 315
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_a

    .line 281
    .end local v5    # "adjustment":[F
    .end local v6    # "coef":[F
    .end local v9    # "dim":I
    .end local v11    # "edges":Landroid/graphics/RectF;
    .end local v17    # "scale":F
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/wallpapercropper/CropView;->mTempPoint:[F

    .line 282
    .local v13, "point":[F
    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mLastX:F

    move/from16 v27, v0

    sub-float v27, v27, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    move/from16 v28, v0

    div-float v27, v27, v28

    aput v27, v13, v25

    .line 283
    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mLastY:F

    move/from16 v27, v0

    sub-float v27, v27, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->scale:F

    move/from16 v28, v0

    div-float v27, v27, v28

    aput v27, v13, v25

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mInverseRotateMatrix:Landroid/graphics/Matrix;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 285
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    move/from16 v25, v0

    const/16 v27, 0x0

    aget v27, v13, v27

    add-float v25, v25, v27

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    .line 286
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    move/from16 v25, v0

    const/16 v27, 0x1

    aget v27, v13, v27

    add-float v25, v25, v27

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    .line 287
    invoke-direct/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->updateCenter()V

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->invalidate()V

    goto/16 :goto_7

    .line 324
    .end local v13    # "point":[F
    :catchall_0
    move-exception v25

    monitor-exit v26
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v25

    .line 307
    .restart local v5    # "adjustment":[F
    .restart local v6    # "coef":[F
    .restart local v11    # "edges":Landroid/graphics/RectF;
    .restart local v17    # "scale":F
    :cond_c
    :try_start_1
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    cmpg-float v25, v25, v27

    if-gez v25, :cond_9

    .line 308
    const/16 v25, 0x0

    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    sub-float v27, v27, v28

    div-float v27, v27, v17

    aput v27, v5, v25

    goto/16 :goto_8

    .line 312
    :cond_d
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getHeight()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    cmpg-float v25, v25, v27

    if-gez v25, :cond_a

    .line 313
    const/16 v25, 0x1

    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->getHeight()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    sub-float v27, v27, v28

    div-float v27, v27, v17

    aput v27, v5, v25

    goto/16 :goto_9

    .line 319
    .restart local v9    # "dim":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpapercropper/CropView;->mInverseRotateMatrix:Landroid/graphics/Matrix;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 320
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    move/from16 v25, v0

    const/16 v27, 0x0

    aget v27, v5, v27

    add-float v25, v25, v27

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    .line 321
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    move/from16 v25, v0

    const/16 v27, 0x1

    aget v27, v5, v27

    add-float v25, v25, v27

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    .line 322
    invoke-direct/range {p0 .. p0}, Lcom/android/wallpapercropper/CropView;->updateCenter()V

    .line 324
    .end local v5    # "adjustment":[F
    .end local v6    # "coef":[F
    .end local v9    # "dim":I
    .end local v11    # "edges":Landroid/graphics/RectF;
    .end local v17    # "scale":F
    :cond_f
    monitor-exit v26
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 326
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mLastX:F

    .line 327
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/wallpapercropper/CropView;->mLastY:F

    .line 328
    const/16 v25, 0x1

    goto/16 :goto_6

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public setTileSource(Lcom/android/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "source"    # Lcom/android/photos/views/TiledImageRenderer$TileSource;
    .param p2, "isReadyCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 141
    invoke-super {p0, p1, p2}, Lcom/android/photos/views/TiledImageView;->setTileSource(Lcom/android/photos/views/TiledImageRenderer$TileSource;Ljava/lang/Runnable;)V

    .line 142
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->centerX:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/wallpapercropper/CropView;->mCenterX:F

    .line 143
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v0, v0, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->centerY:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/wallpapercropper/CropView;->mCenterY:F

    .line 144
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 145
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v1, v1, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->rotation:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 146
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mInverseRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 147
    iget-object v0, p0, Lcom/android/wallpapercropper/CropView;->mInverseRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/wallpapercropper/CropView;->mRenderer:Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;

    iget v1, v1, Lcom/android/photos/views/TiledImageView$ImageRendererWrapper;->rotation:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 148
    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/wallpapercropper/CropView;->getHeight()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/android/wallpapercropper/CropView;->updateMinScale(IILcom/android/photos/views/TiledImageRenderer$TileSource;Z)V

    .line 149
    return-void
.end method

.method public setTouchEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 224
    iput-boolean p1, p0, Lcom/android/wallpapercropper/CropView;->mTouchEnabled:Z

    .line 225
    return-void
.end method
