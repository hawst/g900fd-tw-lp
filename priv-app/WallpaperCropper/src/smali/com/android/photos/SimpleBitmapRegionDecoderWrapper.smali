.class Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;
.super Ljava/lang/Object;
.source "BitmapRegionTileSource.java"

# interfaces
.implements Lcom/android/photos/SimpleBitmapRegionDecoder;


# instance fields
.field mDecoder:Landroid/graphics/BitmapRegionDecoder;


# direct methods
.method private constructor <init>(Landroid/graphics/BitmapRegionDecoder;)V
    .locals 0
    .param p1, "decoder"    # Landroid/graphics/BitmapRegionDecoder;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;->mDecoder:Landroid/graphics/BitmapRegionDecoder;

    .line 56
    return-void
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;
    .locals 5
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z

    .prologue
    const/4 v3, 0x0

    .line 73
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    .line 74
    .local v0, "d":Landroid/graphics/BitmapRegionDecoder;
    if-eqz v0, :cond_0

    .line 75
    new-instance v2, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;

    invoke-direct {v2, v0}, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;-><init>(Landroid/graphics/BitmapRegionDecoder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 83
    .end local v0    # "d":Landroid/graphics/BitmapRegionDecoder;
    :goto_0
    return-object v2

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "BitmapRegionTileSource"

    const-string v4, "getting decoder failed"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v3

    .line 79
    goto :goto_0

    .line 80
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 81
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :cond_0
    move-object v2, v3

    .line 83
    goto :goto_0
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "wantRegion"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;->mDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;->mDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;->mDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v0

    return v0
.end method
