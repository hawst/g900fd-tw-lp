.class Lcom/android/photos/DumbBitmapRegionDecoder;
.super Ljava/lang/Object;
.source "BitmapRegionTileSource.java"

# interfaces
.implements Lcom/android/photos/SimpleBitmapRegionDecoder;


# instance fields
.field mBuffer:Landroid/graphics/Bitmap;

.field mTempCanvas:Landroid/graphics/Canvas;

.field mTempPaint:Landroid/graphics/Paint;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mBuffer:Landroid/graphics/Bitmap;

    .line 102
    return-void
.end method

.method public static newInstance(Ljava/io/InputStream;)Lcom/android/photos/DumbBitmapRegionDecoder;
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 120
    :try_start_0
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 121
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 122
    new-instance v2, Lcom/android/photos/DumbBitmapRegionDecoder;

    invoke-direct {v2, v0}, Lcom/android/photos/DumbBitmapRegionDecoder;-><init>(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 129
    .end local v0    # "b":Landroid/graphics/Bitmap;
    :goto_0
    return-object v2

    .line 124
    :catch_0
    move-exception v1

    .line 125
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 129
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :cond_0
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 126
    :catch_1
    move-exception v1

    .line 127
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "wantRegion"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v3, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 138
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    if-nez v2, :cond_0

    .line 139
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    iput-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    .line 140
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempPaint:Landroid/graphics/Paint;

    .line 141
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 143
    :cond_0
    iget v2, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 144
    .local v1, "sampleSize":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/2addr v2, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/2addr v3, v1

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 148
    .local v0, "newBitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 149
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 150
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    int-to-float v3, v1

    div-float v3, v5, v3

    int-to-float v4, v1

    div-float v4, v5, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->scale(FF)V

    .line 151
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mBuffer:Landroid/graphics/Bitmap;

    iget v4, p1, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p1, Landroid/graphics/Rect;->top:I

    neg-int v5, v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 152
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    .line 153
    iget-object v2, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mTempCanvas:Landroid/graphics/Canvas;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 154
    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/photos/DumbBitmapRegionDecoder;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    return v0
.end method
