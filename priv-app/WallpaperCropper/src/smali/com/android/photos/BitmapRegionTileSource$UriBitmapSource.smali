.class public Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;
.super Lcom/android/photos/BitmapRegionTileSource$BitmapSource;
.source "BitmapRegionTileSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/photos/BitmapRegionTileSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UriBitmapSource"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "previewSize"    # I

    .prologue
    .line 275
    invoke-direct {p0, p3}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;-><init>(I)V

    .line 276
    iput-object p1, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mContext:Landroid/content/Context;

    .line 277
    iput-object p2, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    .line 278
    return-void
.end method

.method private regenerateInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 280
    iget-object v1, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 281
    .local v0, "is":Ljava/io/InputStream;
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v1
.end method


# virtual methods
.method public loadBitmapRegionDecoder()Lcom/android/photos/SimpleBitmapRegionDecoder;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 286
    :try_start_0
    invoke-direct {p0}, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 287
    .local v1, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;->newInstance(Ljava/io/InputStream;Z)Lcom/android/photos/SimpleBitmapRegionDecoderWrapper;

    move-result-object v2

    .line 289
    .local v2, "regionDecoder":Lcom/android/photos/SimpleBitmapRegionDecoder;
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 290
    if-nez v2, :cond_0

    .line 291
    invoke-direct {p0}, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 292
    invoke-static {v1}, Lcom/android/photos/DumbBitmapRegionDecoder;->newInstance(Ljava/io/InputStream;)Lcom/android/photos/DumbBitmapRegionDecoder;

    move-result-object v2

    .line 293
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 301
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "regionDecoder":Lcom/android/photos/SimpleBitmapRegionDecoder;
    :cond_0
    :goto_0
    return-object v2

    .line 296
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v4, "BitmapRegionTileSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to load URI "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v3

    .line 298
    goto :goto_0

    .line 299
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 300
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "BitmapRegionTileSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failure while reading URI "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v3

    .line 301
    goto :goto_0
.end method

.method public loadPreviewBitmap(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v3, 0x0

    .line 307
    :try_start_0
    invoke-direct {p0}, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 308
    .local v2, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    invoke-static {v2, v4, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 309
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 317
    .end local v0    # "b":Landroid/graphics/Bitmap;
    .end local v2    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v0

    .line 311
    :catch_0
    move-exception v1

    .line 312
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v4, "BitmapRegionTileSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to load URI "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v3

    .line 313
    goto :goto_0

    .line 314
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 315
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    move-object v0, v3

    .line 317
    goto :goto_0
.end method

.method public readExif(Lcom/android/gallery3d/exif/ExifInterface;)Z
    .locals 6
    .param p1, "ei"    # Lcom/android/gallery3d/exif/ExifInterface;

    .prologue
    const/4 v2, 0x0

    .line 321
    const/4 v1, 0x0

    .line 323
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-direct {p0}, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->regenerateInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 324
    invoke-virtual {p1, v1}, Lcom/android/gallery3d/exif/ExifInterface;->readExif(Ljava/io/InputStream;)V

    .line 325
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    const/4 v2, 0x1

    .line 334
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_0
    return v2

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    const-string v3, "BitmapRegionTileSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to load URI "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 334
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 330
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 331
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "BitmapRegionTileSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to load URI "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 334
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2
.end method
