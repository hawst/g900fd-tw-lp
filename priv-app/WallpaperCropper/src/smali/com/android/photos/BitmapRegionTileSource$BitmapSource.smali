.class public abstract Lcom/android/photos/BitmapRegionTileSource$BitmapSource;
.super Ljava/lang/Object;
.source "BitmapRegionTileSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/photos/BitmapRegionTileSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BitmapSource"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;
    }
.end annotation


# instance fields
.field private mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

.field private mPreview:Landroid/graphics/Bitmap;

.field private mPreviewSize:I

.field private mRotation:I

.field private mState:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "previewSize"    # I

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    sget-object v0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;->NOT_LOADED:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    iput-object v0, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mState:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    .line 182
    iput p1, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mPreviewSize:I

    .line 183
    return-void
.end method


# virtual methods
.method public getBitmapRegionDecoder()Lcom/android/photos/SimpleBitmapRegionDecoder;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    return-object v0
.end method

.method public getLoadingState()Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mState:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    return-object v0
.end method

.method public getPreviewBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mPreview:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getPreviewSize()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mPreviewSize:I

    return v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mRotation:I

    return v0
.end method

.method public abstract loadBitmapRegionDecoder()Lcom/android/photos/SimpleBitmapRegionDecoder;
.end method

.method public loadInBackground()Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 185
    new-instance v0, Lcom/android/gallery3d/exif/ExifInterface;

    invoke-direct {v0}, Lcom/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 186
    .local v0, "ei":Lcom/android/gallery3d/exif/ExifInterface;
    invoke-virtual {p0, v0}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->readExif(Lcom/android/gallery3d/exif/ExifInterface;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 187
    sget v9, Lcom/android/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    invoke-virtual {v0, v9}, Lcom/android/gallery3d/exif/ExifInterface;->getTagIntValue(I)Ljava/lang/Integer;

    move-result-object v3

    .line 188
    .local v3, "ori":Ljava/lang/Integer;
    if-eqz v3, :cond_0

    .line 189
    invoke-virtual {v3}, Ljava/lang/Integer;->shortValue()S

    move-result v9

    invoke-static {v9}, Lcom/android/gallery3d/exif/ExifInterface;->getRotationForOrientationValue(S)I

    move-result v9

    iput v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mRotation:I

    .line 192
    .end local v3    # "ori":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {p0}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->loadBitmapRegionDecoder()Lcom/android/photos/SimpleBitmapRegionDecoder;

    move-result-object v9

    iput-object v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    .line 193
    iget-object v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    if-nez v9, :cond_1

    .line 194
    sget-object v8, Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;->ERROR_LOADING:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    iput-object v8, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mState:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    .line 211
    :goto_0
    return v7

    .line 197
    :cond_1
    iget-object v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    invoke-interface {v9}, Lcom/android/photos/SimpleBitmapRegionDecoder;->getWidth()I

    move-result v6

    .line 198
    .local v6, "width":I
    iget-object v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    invoke-interface {v9}, Lcom/android/photos/SimpleBitmapRegionDecoder;->getHeight()I

    move-result v1

    .line 199
    .local v1, "height":I
    iget v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mPreviewSize:I

    if-eqz v9, :cond_2

    .line 200
    iget v9, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mPreviewSize:I

    const/16 v10, 0x400

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 201
    .local v4, "previewSize":I
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 202
    .local v2, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v9, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 203
    iput-boolean v8, v2, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 205
    int-to-float v9, v4

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    int-to-float v10, v10

    div-float v5, v9, v10

    .line 206
    .local v5, "scale":F
    invoke-static {v5}, Lcom/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(F)I

    move-result v9

    iput v9, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 207
    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 208
    invoke-virtual {p0, v2}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->loadPreviewBitmap(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mPreview:Landroid/graphics/Bitmap;

    .line 210
    .end local v2    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "previewSize":I
    .end local v5    # "scale":F
    :cond_2
    sget-object v7, Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;->LOADED:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    iput-object v7, p0, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->mState:Lcom/android/photos/BitmapRegionTileSource$BitmapSource$State;

    move v7, v8

    .line 211
    goto :goto_0
.end method

.method public abstract loadPreviewBitmap(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method public abstract readExif(Lcom/android/gallery3d/exif/ExifInterface;)Z
.end method
