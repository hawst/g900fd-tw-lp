.class public Lcom/android/photos/BitmapRegionTileSource;
.super Ljava/lang/Object;
.source "BitmapRegionTileSource.java"

# interfaces
.implements Lcom/android/photos/views/TiledImageRenderer$TileSource;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/photos/BitmapRegionTileSource$UriBitmapSource;,
        Lcom/android/photos/BitmapRegionTileSource$BitmapSource;
    }
.end annotation


# static fields
.field private static final REUSE_BITMAP:Z


# instance fields
.field private mCanvas:Landroid/graphics/Canvas;

.field mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

.field mHeight:I

.field private mOptions:Landroid/graphics/BitmapFactory$Options;

.field private mOverlapRegion:Landroid/graphics/Rect;

.field private mPreview:Lcom/android/gallery3d/glrenderer/BasicTexture;

.field private final mRotation:I

.field mTileSize:I

.field private mWantRegion:Landroid/graphics/Rect;

.field mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 167
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/photos/BitmapRegionTileSource;->REUSE_BITMAP:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/photos/BitmapRegionTileSource$BitmapSource;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Lcom/android/photos/BitmapRegionTileSource$BitmapSource;

    .prologue
    const/16 v4, 0x800

    const/4 v7, 0x1

    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    .line 391
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOverlapRegion:Landroid/graphics/Rect;

    .line 396
    invoke-static {p1}, Lcom/android/photos/views/TiledImageRenderer;->suggestedTileSize(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mTileSize:I

    .line 397
    invoke-virtual {p2}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->getRotation()I

    move-result v2

    iput v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mRotation:I

    .line 398
    invoke-virtual {p2}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->getBitmapRegionDecoder()Lcom/android/photos/SimpleBitmapRegionDecoder;

    move-result-object v2

    iput-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    .line 399
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 400
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    invoke-interface {v2}, Lcom/android/photos/SimpleBitmapRegionDecoder;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mWidth:I

    .line 401
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    invoke-interface {v2}, Lcom/android/photos/SimpleBitmapRegionDecoder;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mHeight:I

    .line 402
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    .line 403
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 404
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 405
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    const/16 v3, 0x4000

    new-array v3, v3, [B

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 406
    invoke-virtual {p2}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->getPreviewSize()I

    move-result v1

    .line 407
    .local v1, "previewSize":I
    if-eqz v1, :cond_0

    .line 408
    const/16 v2, 0x400

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 412
    invoke-direct {p0, p2, v1}, Lcom/android/photos/BitmapRegionTileSource;->decodePreview(Lcom/android/photos/BitmapRegionTileSource$BitmapSource;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 413
    .local v0, "preview":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 414
    const-string v2, "BitmapRegionTileSource"

    const-string v3, "preview is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    .end local v0    # "preview":Landroid/graphics/Bitmap;
    .end local v1    # "previewSize":I
    :cond_0
    :goto_0
    return-void

    .line 415
    .restart local v0    # "preview":Landroid/graphics/Bitmap;
    .restart local v1    # "previewSize":I
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v2, v4, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-gt v2, v4, :cond_2

    .line 416
    new-instance v2, Lcom/android/gallery3d/glrenderer/BitmapTexture;

    invoke-direct {v2, v0}, Lcom/android/gallery3d/glrenderer/BitmapTexture;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mPreview:Lcom/android/gallery3d/glrenderer/BasicTexture;

    goto :goto_0

    .line 418
    :cond_2
    const-string v2, "BitmapRegionTileSource"

    const-string v3, "Failed to create preview of apropriate size!  in: %dx%d, out: %dx%d"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/android/photos/BitmapRegionTileSource;->mWidth:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    iget v5, p0, Lcom/android/photos/BitmapRegionTileSource;->mHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private decodePreview(Lcom/android/photos/BitmapRegionTileSource$BitmapSource;I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "source"    # Lcom/android/photos/BitmapRegionTileSource$BitmapSource;
    .param p2, "targetSize"    # I

    .prologue
    .line 520
    invoke-virtual {p1}, Lcom/android/photos/BitmapRegionTileSource$BitmapSource;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 521
    .local v0, "result":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 522
    const/4 v2, 0x0

    .line 532
    :goto_0
    return-object v2

    .line 527
    :cond_0
    int-to-float v2, p2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 529
    .local v1, "scale":F
    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_1

    .line 530
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/gallery3d/common/BitmapUtils;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 532
    :cond_1
    invoke-static {v0}, Lcom/android/photos/BitmapRegionTileSource;->ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method private static ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 536
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p0

    .line 541
    :goto_0
    return-object v0

    .line 539
    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 540
    .local v0, "newBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method private getTileWithoutReusingBitmap(IIII)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "level"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "tileSize"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 487
    shl-int v2, p4, p1

    .line 488
    .local v2, "t":I
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    add-int v4, p2, v2

    add-int v5, p3, v2

    invoke-virtual {v3, p2, p3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 490
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mOverlapRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/android/photos/BitmapRegionTileSource;->mWidth:I

    iget v5, p0, Lcom/android/photos/BitmapRegionTileSource;->mHeight:I

    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 492
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x1

    shl-int/2addr v4, p1

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 493
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    iget-object v4, p0, Lcom/android/photos/BitmapRegionTileSource;->mOverlapRegion:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-interface {v3, v4, v5}, Lcom/android/photos/SimpleBitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 495
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 496
    const-string v3, "BitmapRegionTileSource"

    const-string v4, "fail in decoding region"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_0
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/photos/BitmapRegionTileSource;->mOverlapRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 512
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .line 503
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p4, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 504
    .local v1, "result":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mCanvas:Landroid/graphics/Canvas;

    if-nez v3, :cond_2

    .line 505
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3}, Landroid/graphics/Canvas;-><init>()V

    iput-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mCanvas:Landroid/graphics/Canvas;

    .line 507
    :cond_2
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v3, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 508
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/android/photos/BitmapRegionTileSource;->mOverlapRegion:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    shr-int/2addr v4, p1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/android/photos/BitmapRegionTileSource;->mOverlapRegion:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    shr-int/2addr v5, p1

    int-to-float v5, v5

    invoke-virtual {v3, v0, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 511
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v3, v7}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object v0, v1

    .line 512
    goto :goto_0
.end method


# virtual methods
.method public getImageHeight()I
    .locals 1

    .prologue
    .line 440
    iget v0, p0, Lcom/android/photos/BitmapRegionTileSource;->mHeight:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/android/photos/BitmapRegionTileSource;->mWidth:I

    return v0
.end method

.method public getPreview()Lcom/android/gallery3d/glrenderer/BasicTexture;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/photos/BitmapRegionTileSource;->mPreview:Lcom/android/gallery3d/glrenderer/BasicTexture;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Lcom/android/photos/BitmapRegionTileSource;->mRotation:I

    return v0
.end method

.method public getTile(IIILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "level"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    .line 455
    invoke-virtual {p0}, Lcom/android/photos/BitmapRegionTileSource;->getTileSize()I

    move-result v1

    .line 456
    .local v1, "tileSize":I
    sget-boolean v2, Lcom/android/photos/BitmapRegionTileSource;->REUSE_BITMAP:Z

    if-nez v2, :cond_0

    .line 457
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/android/photos/BitmapRegionTileSource;->getTileWithoutReusingBitmap(IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 481
    :goto_0
    return-object v2

    .line 460
    :cond_0
    shl-int v0, v1, p1

    .line 461
    .local v0, "t":I
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    add-int v3, p2, v0

    add-int v4, p3, v0

    invoke-virtual {v2, p2, p3, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 463
    if-nez p4, :cond_1

    .line 464
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 467
    :cond_1
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x1

    shl-int/2addr v3, p1

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 468
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iput-object p4, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 471
    :try_start_0
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mDecoder:Lcom/android/photos/SimpleBitmapRegionDecoder;

    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mWantRegion:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-interface {v2, v3, v4}, Lcom/android/photos/SimpleBitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p4

    .line 473
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v2, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v2, p4, :cond_2

    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v2, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 474
    iget-object v2, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iput-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 478
    :cond_2
    if-nez p4, :cond_3

    .line 479
    const-string v2, "BitmapRegionTileSource"

    const-string v3, "fail in decoding region"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v2, p4

    .line 481
    goto :goto_0

    .line 473
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v3, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v3, p4, :cond_4

    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v3, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 474
    iget-object v3, p0, Lcom/android/photos/BitmapRegionTileSource;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iput-object v5, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    :cond_4
    throw v2
.end method

.method public getTileSize()I
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lcom/android/photos/BitmapRegionTileSource;->mTileSize:I

    return v0
.end method
