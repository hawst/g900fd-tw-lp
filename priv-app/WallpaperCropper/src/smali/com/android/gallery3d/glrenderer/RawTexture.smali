.class public Lcom/android/gallery3d/glrenderer/RawTexture;
.super Lcom/android/gallery3d/glrenderer/BasicTexture;
.source "RawTexture.java"


# instance fields
.field private mIsFlipped:Z

.field private final mOpaque:Z


# virtual methods
.method protected getTarget()I
    .locals 1

    .prologue
    .line 71
    const/16 v0, 0xde1

    return v0
.end method

.method public isFlippedVertically()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/android/gallery3d/glrenderer/RawTexture;->mIsFlipped:Z

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/android/gallery3d/glrenderer/RawTexture;->mOpaque:Z

    return v0
.end method

.method protected onBind(Lcom/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 2
    .param p1, "canvas"    # Lcom/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/android/gallery3d/glrenderer/RawTexture;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    .line 60
    :cond_0
    const-string v0, "RawTexture"

    const-string v1, "lost the content due to context change"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v0, 0x0

    goto :goto_0
.end method
