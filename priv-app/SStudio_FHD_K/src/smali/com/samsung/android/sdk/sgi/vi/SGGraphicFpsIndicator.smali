.class public Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;
.super Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;
.source "SGGraphicFpsIndicator.java"


# instance fields
.field avr:I

.field color:I

.field curTimer:J

.field endIndex:I

.field height:I

.field horizontalLine:I

.field mPaint:Landroid/graphics/Paint;

.field mVerString:Ljava/lang/String;

.field max:I

.field min:I

.field points:[I

.field size:I

.field step:I

.field timeLimit:I

.field timer:J

.field verStrWidth:I

.field width:I


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v1, 0x64

    const/4 v9, 0x0

    const-wide/high16 v7, 0x3ff8000000000000L    # 1.5

    const/4 v6, 0x0

    .line 62
    new-instance v2, Landroid/graphics/RectF;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v0

    cmpg-double v0, v4, v7

    if-gez v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    :goto_0
    invoke-direct {v2, v9, v9, v3, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    const v0, 0x3f4ccccd    # 0.8f

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(Landroid/graphics/RectF;F)V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    .line 37
    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->verStrWidth:I

    .line 41
    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    .line 42
    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    .line 43
    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    .line 44
    iput-wide v10, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timer:J

    .line 45
    iput-wide v10, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->curTimer:J

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timeLimit:I

    .line 47
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->step:I

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->color:I

    .line 63
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    .line 64
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v0

    cmpg-double v0, v2, v7

    if-gez v0, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    .line 65
    const/16 v0, 0x3c

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->horizontalLine:I

    .line 66
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->step:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    .line 67
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->points:[I

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 71
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 76
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 77
    return-void

    .line 62
    :cond_0
    const/high16 v0, 0x43160000    # 150.0f

    goto/16 :goto_0

    .line 64
    :cond_1
    const/16 v0, 0x96

    goto :goto_1

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_2
.end method

.method public constructor <init>(IIIIIIF)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 89
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    add-int v3, p1, p3

    int-to-float v3, v3

    add-int v4, p2, p4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-direct {p0, v0, p7}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(Landroid/graphics/RectF;F)V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    .line 37
    iput v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->verStrWidth:I

    .line 41
    iput v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    .line 42
    iput v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    .line 43
    iput v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    .line 44
    iput-wide v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timer:J

    .line 45
    iput-wide v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->curTimer:J

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timeLimit:I

    .line 47
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->step:I

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->color:I

    .line 90
    iput p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    .line 91
    iput p4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    .line 92
    iput p5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->horizontalLine:I

    .line 93
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->step:I

    div-int v0, p3, v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    .line 94
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->points:[I

    .line 95
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    .line 96
    iput p6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->color:I

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 99
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 104
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 105
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_0
.end method

.method private getPoint(I)I
    .locals 2

    .prologue
    .line 173
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    add-int/lit8 v0, v0, -0x14

    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    add-int/lit8 v1, v1, -0x14

    mul-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x3c

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x14

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Bitmap;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 130
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->getFps()F

    move-result v0

    float-to-int v6, v0

    .line 133
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->points:[I

    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    aput v6, v0, v1

    .line 134
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    .line 135
    iget v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    if-lt v0, v1, :cond_0

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    .line 138
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->curTimer:J

    .line 139
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->curTimer:J

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timer:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timeLimit:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 171
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->curTimer:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timer:J

    .line 143
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 144
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 145
    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->horizontalLine:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->getPoint(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->horizontalLine:I

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->getPoint(I)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->endIndex:I

    .line 149
    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->max:I

    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->avr:I

    iput v6, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->min:I

    move v6, v7

    move v2, v1

    move v3, v7

    move v1, v7

    .line 151
    :goto_1
    iget v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    add-int/lit8 v4, v4, -0x1

    if-ge v6, v4, :cond_5

    .line 152
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->points:[I

    aget v4, v4, v2

    .line 153
    add-int/lit8 v9, v2, 0x1

    .line 154
    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->size:I

    if-lt v9, v2, :cond_2

    move v9, v7

    .line 157
    :cond_2
    if-eqz v4, :cond_7

    .line 158
    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->avr:I

    add-int/2addr v2, v4

    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->avr:I

    .line 159
    add-int/lit8 v8, v1, 0x1

    .line 160
    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->max:I

    if-ge v1, v4, :cond_3

    iput v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->max:I

    .line 162
    :cond_3
    :goto_2
    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->min:I

    if-le v1, v4, :cond_4

    iput v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->min:I

    .line 163
    :cond_4
    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->step:I

    add-int v10, v3, v1

    .line 164
    iget v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->step:I

    sub-int v1, v10, v1

    int-to-float v1, v1

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->getPoint(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v10

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->points:[I

    aget v4, v4, v9

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->getPoint(I)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 151
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v2, v9

    move v3, v10

    move v1, v8

    goto :goto_1

    .line 166
    :cond_5
    if-lez v1, :cond_6

    .line 167
    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->avr:I

    div-int v1, v2, v1

    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->avr:I

    .line 168
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "min = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->min:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " avr = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->avr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->max:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    iget v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    add-int/lit8 v3, v3, -0x5

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 170
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->width:I

    iget v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->verStrWidth:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0xa

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->height:I

    add-int/lit8 v3, v3, -0x5

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_7
    move v8, v1

    goto/16 :goto_2
.end method

.method public setShowVersion(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    if-eqz p1, :cond_0

    .line 111
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation()Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;

    move-result-object v0

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMajor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMinor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mPatch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mBuild:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->verStrWidth:I

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->mVerString:Ljava/lang/String;

    .line 116
    iput v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->verStrWidth:I

    goto :goto_0
.end method

.method public setUpdateInterval(I)V
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGraphicFpsIndicator;->timeLimit:I

    .line 124
    return-void
.end method
