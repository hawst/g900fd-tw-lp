.class abstract Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;
.super Ljava/lang/Object;
.source "SGSurfaceListenerBase.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 51
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGSurfaceListenerBase()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;-><init>(JZ)V

    .line 52
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurfaceListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;JZZ)V

    .line 53
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCPtr:J

    .line 33
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCMemOwn:Z

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGSurfaceListenerBase(J)V

    .line 45
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->swigCPtr:J

    .line 47
    :cond_1
    return-void
.end method

.method public abstract onFrameEnd()V
.end method

.method public abstract onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method
