.class final Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;
.super Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;
.source "SGAnimationListenerHolder.java"


# instance fields
.field mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

.field mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 31
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .line 32
    return-void
.end method


# virtual methods
.method public getInterface()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    return-object v0
.end method

.method public onCancelled(I)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 86
    :cond_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 82
    const-string v1, "SGAnimationListener::onCancelled error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDiscarded(I)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    if-eqz v0, :cond_0

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onDiscarded(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 117
    const-string v1, "SGAnimationListener::onDiscarded error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFinished(I)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 104
    :cond_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 100
    const-string v1, "SGAnimationListener::onFinished error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRepeated(I)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 68
    :cond_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 64
    const-string v1, "SGAnimationListener::onRepeated error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStarted(I)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mProvider:Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 50
    :cond_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 46
    const-string v1, "SGAnimationListener::onStarted error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setInterface(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 130
    return-void
.end method
