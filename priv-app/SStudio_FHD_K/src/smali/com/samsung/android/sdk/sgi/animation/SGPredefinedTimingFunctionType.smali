.class public final enum Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;
.super Ljava/lang/Enum;
.source "SGPredefinedTimingFunctionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum QUINT_O_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum QUINT_O_80:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_IO_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_IO_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_IO_60:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_IO_70:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_IO_80:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_IO_90:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_I_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

.field public static final enum SINE_O_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_I_33"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_I_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_O_33"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_O_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_IO_33"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_IO_50"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_IO_60"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_60:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_IO_70"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_70:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_IO_80"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_80:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "SINE_IO_90"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_90:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "QUINT_O_50"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->QUINT_O_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    const-string v1, "QUINT_O_80"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->QUINT_O_80:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    .line 18
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_I_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_O_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_33:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_60:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_70:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_80:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->SINE_IO_90:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->QUINT_O_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->QUINT_O_80:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    return-object v0
.end method
