.class public Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;
.super Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;
.source "SGSimpleFpsIndicator.java"


# instance fields
.field curTimer:J

.field mPaint:Landroid/graphics/Paint;

.field mString:Ljava/lang/String;

.field timeLimit:I

.field timer:J


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 59
    new-instance v2, Landroid/graphics/RectF;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    if-ne v0, v4, :cond_0

    const/high16 v0, 0x428c0000    # 70.0f

    :goto_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    if-ne v1, v4, :cond_1

    const/high16 v1, 0x41f00000    # 30.0f

    :goto_1
    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(Landroid/graphics/RectF;F)V

    .line 44
    iput-wide v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timer:J

    .line 45
    iput-wide v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->curTimer:J

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timeLimit:I

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 72
    :goto_2
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mString:Ljava/lang/String;

    .line 73
    return-void

    .line 59
    :cond_0
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    :cond_1
    const/high16 v1, 0x42200000    # 40.0f

    goto :goto_1

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_2
.end method

.method public constructor <init>(III)V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    .line 86
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    add-int/lit8 v3, p1, 0x64

    int-to-float v3, v3

    add-int/lit8 v4, p2, 0x1e

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(Landroid/graphics/RectF;F)V

    .line 44
    iput-wide v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timer:J

    .line 45
    iput-wide v5, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->curTimer:J

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timeLimit:I

    .line 88
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 96
    :goto_0
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mString:Ljava/lang/String;

    .line 97
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->curTimer:J

    .line 112
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->curTimer:J

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timer:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timeLimit:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->curTimer:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timer:J

    .line 118
    const-string v0, "%.1f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->getFps()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mString:Ljava/lang/String;

    .line 120
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 121
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 123
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->descent()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    rsub-int/lit8 v2, v2, 0xf

    .line 124
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mString:Ljava/lang/String;

    int-to-float v1, v1

    int-to-float v2, v2

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setUpdateInterval(I)V
    .locals 0

    .prologue
    .line 103
    iput p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;->timeLimit:I

    .line 104
    return-void
.end method
