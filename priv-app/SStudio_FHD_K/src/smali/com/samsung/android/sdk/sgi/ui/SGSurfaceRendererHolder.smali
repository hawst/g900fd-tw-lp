.class final Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;
.super Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;
.source "SGSurfaceRendererHolder.java"


# instance fields
.field mRenderer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;->mRenderer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    .line 28
    return-void
.end method


# virtual methods
.method public getInterface()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;->mRenderer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    return-object v0
.end method

.method public onDraw(I)V
    .locals 2

    .prologue
    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;->mRenderer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;->onDraw(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 39
    const-string v1, "SGSurfaceRenderer::onDraw error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setInterface(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;->mRenderer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    .line 51
    return-void
.end method
