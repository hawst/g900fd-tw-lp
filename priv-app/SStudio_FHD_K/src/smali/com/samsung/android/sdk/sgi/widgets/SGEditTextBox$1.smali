.class Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;
.super Ljava/lang/Object;
.source "SGEditTextBox.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->createEditText(Landroid/content/Context;)Landroid/widget/EditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSelectionPointers()V

    .line 402
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->horizontalScrollCheckOnTextChanged()V

    .line 406
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 408
    return-void
.end method
