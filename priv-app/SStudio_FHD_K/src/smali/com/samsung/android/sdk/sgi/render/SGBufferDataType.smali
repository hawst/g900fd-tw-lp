.class public final enum Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;
.super Ljava/lang/Enum;
.source "SGBufferDataType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum BYTE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum DOUBLE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum INT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum SHORT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum UNSIGNED_BYTE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum UNSIGNED_INT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

.field public static final enum UNSIGNED_SHORT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "BYTE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->BYTE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "UNSIGNED_BYTE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->UNSIGNED_BYTE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->SHORT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "UNSIGNED_SHORT"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->UNSIGNED_SHORT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "INT"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->INT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "UNSIGNED_INT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->UNSIGNED_INT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "FLOAT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    const-string v1, "DOUBLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->DOUBLE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->BYTE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->UNSIGNED_BYTE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->SHORT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->UNSIGNED_SHORT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->INT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->UNSIGNED_INT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->DOUBLE:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    return-object v0
.end method
