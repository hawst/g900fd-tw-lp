.class Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;
.super Ljava/lang/Object;
.source "SGWidgetTextureView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->onDraw(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;->this$1:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;->this$1:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$400(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/view/TextureView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;->this$1:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;->this$1:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;

    iget-object v2, v2, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;->this$1:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;

    iget-object v3, v3, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-interface {v0, v1, v2, v3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    .line 117
    :cond_0
    return-void
.end method
