.class public Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGTextureProperty.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGTextureProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getDataFormat()Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;
    .locals 3

    .prologue
    .line 44
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getDataFormat(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDataType()Lcom/samsung/android/sdk/sgi/render/SGTextureDataType;
    .locals 3

    .prologue
    .line 48
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getDataType(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getInternalFormat()Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getInternalFormat(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getMagFilter()Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
    .locals 3

    .prologue
    .line 56
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getMagFilter(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getMinFilter()Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
    .locals 3

    .prologue
    .line 52
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getMinFilter(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWrapR()Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;
    .locals 3

    .prologue
    .line 68
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getWrapR(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWrapS()Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;
    .locals 3

    .prologue
    .line 60
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getWrapS(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWrapT()Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;
    .locals 3

    .prologue
    .line 64
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGTextureProperty_getWrapT(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method
