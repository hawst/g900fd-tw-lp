.class public Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;
.source "SGScrollableArea.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$1;,
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;,
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;
    }
.end annotation


# static fields
.field private static final EPSILON_EVENT:F = 0.001f


# instance fields
.field private horizontalExcess:F

.field private isHorizontalOverScrollStarted:Z

.field private isVerticalOverScrollStarted:Z

.field private leftCorner:F

.field private mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

.field private mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mGlowEnabled:Z

.field private mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

.field private mHoverArea:Landroid/graphics/RectF;

.field private mIsActive:Z

.field private mIsAnimating:Z

.field private mIsScrollIndicatorsEnabled:Z

.field mIsScrolling:Z

.field private mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

.field private mMaxScrollX:F

.field private mMaxScrollY:F

.field private mPosAnimationId:I

.field mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

.field private mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

.field mSGAnimationEventListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mSGScrollEventListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;",
            ">;"
        }
    .end annotation
.end field

.field private mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

.field private mScrollDuration:I

.field private mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

.field private mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

.field private mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field private topCorner:F

.field private verticalExcess:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 1

    .prologue
    .line 60
    const v0, -0x333334

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const v6, -0xffff01

    const/high16 v2, 0x41a00000    # 20.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 71
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    .line 960
    iput v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->horizontalExcess:F

    .line 961
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isHorizontalOverScrollStarted:Z

    .line 963
    iput v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->verticalExcess:F

    .line 964
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isVerticalOverScrollStarted:Z

    .line 1029
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollX:F

    .line 1030
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollY:F

    .line 1032
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    .line 1035
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    const/16 v1, 0x1f4

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    .line 1037
    iput v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    .line 1038
    iput v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    .line 1040
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    .line 1042
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollEventListeners:Ljava/util/List;

    .line 1043
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    .line 1049
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 1051
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    .line 1054
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsActive:Z

    .line 73
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 75
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setChildrenClipping(Z)V

    .line 76
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 77
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v4, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 78
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    const-string v1, "anchor"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setName(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 82
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 85
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setOrientation(Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 89
    iput-boolean v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrollIndicatorsEnabled:Z

    .line 91
    const/high16 v0, 0x40800000    # 4.0f

    .line 92
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {v1, v2, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    .line 94
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    invoke-direct {v2, v3, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {v1, v2, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    .line 95
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {v1, v2, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    .line 96
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setPosition(FF)V

    .line 97
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    invoke-direct {v2, v3, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {v1, v2, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setPosition(FF)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 110
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 113
    iput-boolean v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowEnabled:Z

    .line 115
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContext:Ljava/lang/ref/WeakReference;

    .line 117
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 119
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setHoverable(Z)V

    .line 120
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setFocusable(Z)V

    .line 121
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 122
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPosAnimationId:I

    return v0
.end method

.method private correctDestination(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 5

    .prologue
    const v4, 0x3a83126f    # 0.001f

    const/4 v1, 0x1

    .line 876
    const/4 v0, 0x0

    .line 877
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollX:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_3

    .line 878
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    move v0, v1

    .line 885
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    cmpg-float v2, v2, v3

    if-lez v2, :cond_2

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollY:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_4

    .line 886
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 893
    :goto_1
    return v1

    .line 880
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 881
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    neg-float v0, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    move v0, v1

    .line 882
    goto :goto_0

    .line 888
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_5

    .line 889
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    neg-float v0, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method private fling(FF)V
    .locals 3

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->destinationOnFling(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    const/16 v2, 0x1f4

    invoke-virtual {p0, v1, v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    .line 329
    return-void
.end method

.method private getMaxSize(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 8

    .prologue
    .line 853
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 855
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidgetsCount()I

    move-result v1

    int-to-long v2, v1

    .line 857
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isChildrenClippingEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 872
    :cond_0
    return-object v0

    .line 860
    :cond_1
    const/4 v1, 0x0

    :goto_0
    int-to-long v4, v1

    cmp-long v4, v4, v2

    if-gez v4, :cond_0

    .line 861
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getMaxSize(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    .line 862
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v5

    .line 864
    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v6

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v7

    add-float/2addr v6, v7

    .line 865
    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v5

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v4

    add-float/2addr v4, v5

    .line 867
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v5

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 868
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    .line 869
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v5

    cmpg-float v5, v5, v4

    if-gez v5, :cond_3

    .line 870
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 860
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private hideGlow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 372
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 380
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 381
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 383
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 389
    :cond_1
    :goto_1
    return-void

    .line 374
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 376
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 377
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0

    .line 384
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 386
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 387
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowingAnimation:Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_1
.end method

.method private notifyContentSizeChanged(FF)V
    .locals 2

    .prologue
    .line 991
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;

    .line 992
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;->onContentSizeChanged(FF)Z

    goto :goto_0

    .line 993
    :cond_0
    return-void
.end method

.method private notifyEvent(FF)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 897
    .line 900
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getScrolledPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    .line 902
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    add-float/2addr v0, p1

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    cmpg-float v0, v0, v3

    if-lez v0, :cond_0

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    add-float/2addr v0, v3

    add-float/2addr v0, p1

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    :cond_0
    move v0, p1

    .line 906
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    add-float/2addr v3, p2

    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    cmpg-float v3, v3, v4

    if-lez v3, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    add-float/2addr v2, v3

    add-float/2addr v2, p2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    :cond_1
    move v2, p2

    .line 909
    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3a83126f    # 0.001f

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v1, v3, v1

    if-lez v1, :cond_3

    .line 910
    :cond_2
    invoke-direct {p0, v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->notifySideIsAchieved(FF)V

    .line 912
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->notifyScrollPosChanged(FF)V

    .line 913
    return-void

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private notifyScrollPosChanged(FF)V
    .locals 2

    .prologue
    .line 986
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;

    .line 987
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;->onScrollPositionChanged(FF)Z

    goto :goto_0

    .line 988
    :cond_0
    return-void
.end method

.method private notifySideIsAchieved(FF)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const v1, 0x3a83126f    # 0.001f

    .line 968
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isHorizontalOverScrollStarted:Z

    if-eqz v0, :cond_2

    .line 969
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->horizontalExcess:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->horizontalExcess:F

    .line 975
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isVerticalOverScrollStarted:Z

    if-eqz v0, :cond_3

    .line 976
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->verticalExcess:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->verticalExcess:F

    .line 981
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;

    .line 982
    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->horizontalExcess:F

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->verticalExcess:F

    invoke-interface {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;->onOverScrolled(FF)Z

    goto :goto_2

    .line 971
    :cond_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 972
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isHorizontalOverScrollStarted:Z

    .line 973
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->horizontalExcess:F

    goto :goto_0

    .line 977
    :cond_3
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 978
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isVerticalOverScrollStarted:Z

    .line 979
    iput p2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->verticalExcess:F

    goto :goto_1

    .line 983
    :cond_4
    return-void
.end method

.method private positionUpdate(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)[Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1006
    const v3, 0x3a83126f    # 0.001f

    .line 1008
    const/4 v0, 0x4

    new-array v4, v0, [Z

    .line 1010
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    sub-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    aput-boolean v0, v4, v2

    .line 1011
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    sub-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    move v0, v1

    :goto_1
    aput-boolean v0, v4, v1

    .line 1012
    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v6

    add-float/2addr v0, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v6

    sub-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    move v0, v1

    :goto_2
    aput-boolean v0, v4, v5

    .line 1013
    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v5

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v6

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v3, v5, v3

    if-gez v3, :cond_3

    :goto_3
    aput-boolean v1, v4, v0

    .line 1015
    return-object v4

    :cond_0
    move v0, v2

    .line 1010
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1011
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1012
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1013
    goto :goto_3
.end method

.method private startGlow([Z)V
    .locals 3

    .prologue
    .line 355
    const v0, 0x3e4ccccd    # 0.2f

    .line 356
    const/4 v1, 0x0

    aget-boolean v1, p1, v1

    if-eqz v1, :cond_2

    .line 357
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v2

    add-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 362
    :cond_0
    :goto_0
    const/4 v1, 0x1

    aget-boolean v1, p1, v1

    if-eqz v1, :cond_3

    .line 363
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    .line 367
    :cond_1
    :goto_1
    return-void

    .line 358
    :cond_2
    const/4 v1, 0x2

    aget-boolean v1, p1, v1

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v2

    add-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    goto :goto_0

    .line 364
    :cond_3
    const/4 v1, 0x3

    aget-boolean v1, p1, v1

    if-eqz v1, :cond_1

    .line 365
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getOpacity()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setOpacity(F)V

    goto :goto_1
.end method


# virtual methods
.method public final addListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)Z
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final addScrollableAreaListener(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;)Z
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollEventListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 2

    .prologue
    .line 546
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 547
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->leftCorner:F

    .line 548
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 549
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->topCorner:F

    .line 550
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v0

    .line 551
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->contentChanged()V

    .line 552
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollToWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)Z

    .line 553
    return v0
.end method

.method public final addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I

    move-result v0

    .line 570
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollToWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)Z

    .line 571
    return v0
.end method

.method public final containsChild(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->containsChild(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public contentChanged()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setSize(FF)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getMaxSize(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setContentSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FF)V

    .line 135
    return-void
.end method

.method public dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 230
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isEventActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 320
    :cond_0
    :goto_0
    return v1

    .line 232
    :cond_1
    invoke-static {p1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->createMotionEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Landroid/view/MotionEvent;

    move-result-object v12

    .line 234
    if-eqz v12, :cond_0

    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v12}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 240
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v3, :cond_6

    .line 241
    iput-boolean v11, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsActive:Z

    .line 242
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeAllAnimations()V

    .line 244
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isScrollIndicatorsEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->removeAllAnimations()V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->removeAllAnimations()V

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 313
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v1, :cond_4

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->onUp(Landroid/view/MotionEvent;)V

    .line 316
    :cond_4
    invoke-virtual {v12}, Landroid/view/MotionEvent;->recycle()V

    .line 317
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    if-nez v0, :cond_5

    .line 318
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    :cond_5
    move v1, v11

    .line 320
    goto :goto_0

    .line 250
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v3, :cond_d

    .line 251
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsActive:Z

    if-eqz v0, :cond_c

    .line 252
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    sub-float/2addr v0, v3

    .line 253
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    .line 255
    iget-boolean v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    if-nez v4, :cond_8

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v5

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_7

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_8

    .line 256
    :cond_7
    iput-boolean v11, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    .line 257
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_13

    .line 258
    cmpl-float v4, v0, v2

    if-lez v4, :cond_a

    .line 259
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    sub-float/2addr v0, v4

    move v9, v0

    .line 265
    :goto_2
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_12

    .line 266
    cmpl-float v0, v3, v2

    if-lez v0, :cond_b

    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    sub-float v0, v3, v0

    move v10, v0

    .line 273
    :goto_3
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>()V

    .line 274
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->FINGER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->addPointer(IFFFFLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;FLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;)V

    .line 275
    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move v3, v10

    move v0, v9

    .line 278
    :cond_8
    iget-boolean v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    if-eqz v1, :cond_3

    .line 279
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    invoke-virtual {v1, v0, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->calculateDestinationOnScroll(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    add-float/2addr v1, v2

    neg-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    add-float/2addr v0, v2

    neg-float v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FF)V

    .line 282
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowEnabled:Z

    if-eqz v0, :cond_9

    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->positionUpdate(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)[Z

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->startGlow([Z)V

    .line 286
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    goto/16 :goto_1

    .line 261
    :cond_a
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    add-float/2addr v0, v4

    move v9, v0

    goto :goto_2

    .line 269
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollThreshold:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    add-float/2addr v0, v3

    move v10, v0

    goto :goto_3

    .line 289
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLastPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 290
    iput-boolean v11, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsActive:Z

    goto/16 :goto_1

    .line 293
    :cond_d
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-eq v0, v2, :cond_e

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v2, :cond_3

    .line 294
    :cond_e
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    if-eqz v0, :cond_11

    .line 295
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 296
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    .line 297
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v3

    .line 299
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getWidgetsCount()I

    move-result v0

    if-lez v0, :cond_10

    .line 300
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    .line 301
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v5, v0

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_f

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_10

    .line 302
    :cond_f
    neg-float v0, v2

    neg-float v2, v3

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->fling(FF)V

    .line 305
    :cond_10
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrolling:Z

    .line 306
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 308
    :cond_11
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsActive:Z

    .line 309
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowEnabled:Z

    if-eqz v0, :cond_3

    .line 310
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->hideGlow()V

    goto/16 :goto_1

    :cond_12
    move v10, v3

    goto/16 :goto_3

    :cond_13
    move v9, v0

    goto/16 :goto_2
.end method

.method public final getAnimationListener()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$1;)V

    return-object v0
.end method

.method public final getContentSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public final getHorizontalScrollIndicator()Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    return-object v0
.end method

.method public final getHoverArea()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getScrollAnimationDuration()I
    .locals 1

    .prologue
    .line 793
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    return v0
.end method

.method public final getScrollPolicy()Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    return-object v0
.end method

.method public final getScrolledPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    .line 351
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    mul-float/2addr v1, v3

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    mul-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    return-object v0
.end method

.method public final getVerticalScrollIndicator()Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    return-object v0
.end method

.method public final getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    return-object v0
.end method

.method public final getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v0

    return v0
.end method

.method public final getWidgetsCount()I
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidgetsCount()I

    move-result v0

    return v0
.end method

.method public isGlowEffectEnabled()Z
    .locals 1

    .prologue
    .line 510
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowEnabled:Z

    return v0
.end method

.method public final isScrollIndicatorsEnabled()Z
    .locals 1

    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrollIndicatorsEnabled:Z

    return v0
.end method

.method public onEventActiveChanged()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 213
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>()V

    .line 214
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->FINGER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->addPointer(IFFFFLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;FLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;)V

    .line 215
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidgetsCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    :cond_0
    return-void
.end method

.method public onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 169
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x0

    .line 208
    :goto_0
    return v0

    .line 172
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    .line 173
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 175
    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v2, :cond_2

    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeAllAnimations()V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isScrollIndicatorsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->removeAllAnimations()V

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->removeAllAnimations()V

    .line 208
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 181
    :cond_2
    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v2, :cond_1

    .line 182
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 183
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v4, v4, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto :goto_0

    .line 185
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    .line 186
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v0, v4, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto :goto_0

    .line 188
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto/16 :goto_0

    .line 191
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_6

    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v4, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto/16 :goto_0

    .line 194
    :cond_6
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    neg-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto/16 :goto_0

    .line 197
    :cond_7
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_8

    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    neg-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v0, v4, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto/16 :goto_0

    .line 200
    :cond_8
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_9

    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto/16 :goto_0

    .line 203
    :cond_9
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    neg-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    invoke-virtual {p0, v4, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final removeAllWidgets()V
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeAllWidgets()V

    .line 653
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->contentChanged()V

    .line 654
    return-void
.end method

.method public final removeListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)Z
    .locals 1

    .prologue
    .line 809
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final removeScrollableAreaListener(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaListener;)Z
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollEventListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final removeWidget(I)V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidget(I)V

    .line 594
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->contentChanged()V

    .line 595
    return-void
.end method

.method public final removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 582
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->contentChanged()V

    .line 583
    return-void
.end method

.method public final removeWidgets(II)V
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidgets(II)V

    .line 642
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->contentChanged()V

    .line 643
    return-void
.end method

.method public scrollTo(FF)V
    .locals 3

    .prologue
    .line 405
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 407
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->correctDestination(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z

    move-result v1

    .line 408
    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowEnabled:Z

    if-eqz v1, :cond_0

    .line 409
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->hideGlow()V

    .line 412
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->notifyEvent(FF)V

    .line 414
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 415
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 417
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isScrollIndicatorsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->updateIndicators()V

    .line 419
    :cond_1
    return-void
.end method

.method public scrollTo(FFI)Z
    .locals 3

    .prologue
    .line 459
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->getAnimation()Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    move-result-object v0

    .line 460
    if-lez p3, :cond_0

    .line 461
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 462
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isScrollIndicatorsEnabled()Z

    move-result v1

    .line 463
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setScrollIndicatorsEnabled(Z)V

    .line 464
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FF)V

    .line 465
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getCurrentAnimationId()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPosAnimationId:I

    .line 466
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setScrollIndicatorsEnabled(Z)V

    .line 467
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->updateIndicators()V

    .line 468
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 474
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 469
    :cond_0
    if-nez p3, :cond_1

    .line 470
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FF)V

    goto :goto_0

    .line 472
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Duration of scroll animation cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final scrollToWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 337
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v1, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 338
    :goto_0
    if-eqz p1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne p1, v1, :cond_1

    .line 344
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    invoke-virtual {p0, v1, v0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FFI)Z

    move-result v0

    return v0

    .line 342
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->add(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 338
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object p1

    goto :goto_0
.end method

.method public final setContentSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 746
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 747
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 749
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollX:F

    .line 754
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 755
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollY:F

    .line 759
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 760
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setContentAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 762
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 763
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setContentAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 765
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    neg-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->notifyContentSizeChanged(FF)V

    .line 766
    return-void

    .line 752
    :cond_0
    iput v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollX:F

    goto :goto_0

    .line 757
    :cond_1
    iput v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollY:F

    goto :goto_1
.end method

.method public setGlowBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 428
    const/16 v0, 0x14

    .line 430
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setColor(I)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    int-to-float v2, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(FF)V

    .line 432
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 434
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setColor(I)V

    .line 435
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    int-to-float v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(FF)V

    .line 436
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, p4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 438
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setColor(I)V

    .line 439
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    int-to-float v2, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(FF)V

    .line 440
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setPosition(FF)V

    .line 441
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 443
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setColor(I)V

    .line 444
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(FF)V

    .line 445
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setPosition(FF)V

    .line 446
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 447
    return-void
.end method

.method public setGlowEffectEnabled(Z)V
    .locals 0

    .prologue
    .line 502
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mGlowEnabled:Z

    .line 503
    return-void
.end method

.method public setHorizontalScrollIndicator(Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;)V
    .locals 2

    .prologue
    .line 709
    if-nez p1, :cond_0

    .line 710
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "setHorizontalScrollIndicator can not accept as an argument value null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 711
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    .line 712
    return-void
.end method

.method public final setHoverArea(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 144
    if-nez p1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "setHoverArea can not accept as an argument value null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mHoverArea:Landroid/graphics/RectF;

    .line 147
    return-void
.end method

.method public setScrollAnimationDuration(I)V
    .locals 2

    .prologue
    .line 782
    if-gez p1, :cond_0

    .line 783
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Duration of scroll animation cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 785
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollDuration:I

    .line 786
    return-void
.end method

.method public final setScrollIndicatorsEnabled(Z)V
    .locals 3

    .prologue
    .line 684
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isScrollIndicatorsEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 686
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 687
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollX:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollTo(F)V

    .line 688
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollY:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollTo(F)V

    .line 693
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsScrollIndicatorsEnabled:Z

    .line 694
    return-void

    .line 689
    :cond_1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->isScrollIndicatorsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 691
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public final setScrollPolicy(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)V
    .locals 2

    .prologue
    .line 482
    if-nez p1, :cond_0

    .line 483
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "setScrollPolicy can not accept as an argument value null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGScrollableAreaScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    .line 485
    return-void
.end method

.method public setSize(FF)V
    .locals 2

    .prologue
    .line 832
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(FF)V

    .line 833
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 835
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 836
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 837
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mLeftGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 839
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 840
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    .line 841
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mTopGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 843
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 844
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 845
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mRightGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 847
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 848
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    .line 849
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mBottomGlow:Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 850
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 820
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setSize(FF)V

    .line 821
    return-void
.end method

.method public setVerticalScrollIndicator(Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;)V
    .locals 2

    .prologue
    .line 719
    if-nez p1, :cond_0

    .line 720
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "setVerticalScrollIndicator can not accept as an argument value null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 721
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    .line 722
    return-void
.end method

.method public final swapWidgets(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mWidgetAnchor:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swapWidgets(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 607
    return-void
.end method

.method final updateIndicators()V
    .locals 3

    .prologue
    .line 395
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorHorizontal:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollX:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollTo(F)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mScrollIndicatorVertical:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mContentPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mMaxScrollY:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollTo(F)V

    .line 397
    return-void
.end method
