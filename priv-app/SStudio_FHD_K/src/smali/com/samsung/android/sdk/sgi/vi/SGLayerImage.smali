.class public Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.source "SGLayerImage.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 193
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayerImage()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>(JZ)V

    .line 194
    return-void
.end method

.method public constructor <init>(FFI)V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 107
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 108
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 109
    return-void
.end method

.method public constructor <init>(FFLandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 79
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 81
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 82
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;-><init>(JZ)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 63
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 65
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 50
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 51
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 118
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 119
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 94
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 96
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 97
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCMemOwn:Z

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGLayerImage(J)V

    .line 39
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    .line 41
    :cond_1
    return-void
.end method

.method public getBlendMode()Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;
    .locals 3

    .prologue
    .line 154
    const-class v0, Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_getBlendMode(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_getColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)I

    move-result v0

    return v0
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_getContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 181
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_getContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 189
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_getContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public isAlphaBlendingEnabled()Z
    .locals 2

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)Z

    move-result v0

    return v0
.end method

.method public isPreMultipliedRGBAEnabled()Z
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_isPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)Z

    move-result v0

    return v0
.end method

.method public setAlphaBlendingEnabled(Z)V
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Z)V

    .line 140
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 197
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setBitmap(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Landroid/graphics/Bitmap;)V

    .line 198
    return-void
.end method

.method public setBlendMode(Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;)V
    .locals 3

    .prologue
    .line 147
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 149
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setBlendMode(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;I)V

    .line 151
    return-void
.end method

.method public setColor(I)V
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;I)V

    .line 132
    return-void
.end method

.method public setContentRect(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 166
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Landroid/graphics/RectF;)V

    .line 170
    return-void
.end method

.method public setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 178
    return-void
.end method

.method public setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 185
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 186
    return-void
.end method

.method public setPreMultipliedRGBAEnabled(Z)V
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Z)V

    .line 124
    return-void
.end method

.method public setTexture(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 6

    .prologue
    .line 159
    if-eqz p1, :cond_0

    .line 160
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerImage_setTexture(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 163
    :goto_0
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
