.class Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;
.super Ljava/lang/Object;
.source "SGWidgetVideo.java"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnFrameAvailableListenerImpl"
.end annotation


# instance fields
.field handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)V
    .locals 2

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;->handler:Landroid/os/Handler;

    .line 71
    return-void
.end method


# virtual methods
.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;

    # invokes: Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->onUpdate()V
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->access$000(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)V

    .line 88
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl$1;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
