.class Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.source "SGWidgetSurface.java"


# instance fields
.field mRendererListener:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 65
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGWidgetSurface()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;-><init>(JZ)V

    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JZZ)V

    .line 67
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>(JZ)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;-><init>()V

    .line 48
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->mRendererListener:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;

    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->mRendererListener:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V

    .line 50
    return-void
.end method

.method private init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
    .locals 9

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_init(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V

    .line 71
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCMemOwn:Z

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGWidgetSurface(J)V

    .line 39
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    .line 41
    :cond_1
    return-void
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_getContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 93
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_getContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 101
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_getContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->mRendererListener:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;->getInterface()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    move-result-object v0

    return-object v0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_invalidateSwigExplicitSGWidgetSurface__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V

    goto :goto_0
.end method

.method public invalidate(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 105
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_invalidate__SWIG_0_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_invalidateSwigExplicitSGWidgetSurface__SWIG_0_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public setContentRect(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 78
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_setContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V

    .line 82
    return-void
.end method

.method public setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_setContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 90
    return-void
.end method

.method public setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_setContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 98
    return-void
.end method

.method public setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 2

    .prologue
    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SGWidgetSurface::setRenderer error: parameter renderer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->mRendererListener:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererHolder;->setInterface(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 57
    return-void
.end method

.method public setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
    .locals 6

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetSurface_setRenderer(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V

    .line 75
    return-void
.end method
