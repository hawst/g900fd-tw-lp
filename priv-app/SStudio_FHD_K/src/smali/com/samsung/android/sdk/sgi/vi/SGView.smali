.class public Lcom/samsung/android/sdk/sgi/vi/SGView;
.super Landroid/view/SurfaceView;
.source "SGView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;
    }
.end annotation


# instance fields
.field private mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 47
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->init()V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 58
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->init()V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 70
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->init()V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 80
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 81
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->init()V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 103
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 104
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->init()V

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 91
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 92
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->init()V

    .line 93
    return-void
.end method

.method static synthetic access$001(Lcom/samsung/android/sdk/sgi/vi/SGView;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/vi/SGView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    return-object v0
.end method

.method static synthetic access$201(Lcom/samsung/android/sdk/sgi/vi/SGView;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$301(Lcom/samsung/android/sdk/sgi/vi/SGView;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$401(Lcom/samsung/android/sdk/sgi/vi/SGView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$501(Lcom/samsung/android/sdk/sgi/vi/SGView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$601(Lcom/samsung/android/sdk/sgi/vi/SGView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$701(Lcom/samsung/android/sdk/sgi/vi/SGView;IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 227
    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 228
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 229
    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 230
    return-void
.end method


# virtual methods
.method public attachCurrentThread()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->attachCurrentThread()V

    .line 189
    return-void
.end method

.method public checkInputConnectionProxy(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGView$1;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->checkInputConnectionProxy(Landroid/view/View;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultCheckInputConnectionProxyStrategy;)Z

    move-result v0

    return v0
.end method

.method protected final createHoverEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->createHoverEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    return-object v0
.end method

.method protected final createSGTouchEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->createSGTouchEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    return-object v0
.end method

.method public detachCurrentThread()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->detachCurrentThread()V

    .line 194
    return-void
.end method

.method public getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    return-object v0
.end method

.method public getSurfaceStateListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->getSurfaceStateListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    move-result-object v0

    return-object v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGView$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGView$3;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onHoverEvent(Landroid/view/MotionEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnHoverEventStrategy;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGView;->onKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyEvent(IILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGView$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGView$5;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onKeyEvent(IILandroid/view/KeyEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;)Z

    move-result v0

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGView$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGView$4;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onKeyLongPress(ILandroid/view/KeyEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyLongPress;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGView;->onKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGView;->onKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGView$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGView$2;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onTouchEvent(Landroid/view/MotionEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnTouchEventStrategy;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 156
    if-nez p2, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getRoot()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_0

    .line 163
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setVisibility(Z)V

    .line 167
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->resume()V

    .line 245
    return-void
.end method

.method public setAlpha(F)V
    .locals 1

    .prologue
    .line 253
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 254
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setOpacity(F)V

    .line 255
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setColor(I)V

    .line 142
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 147
    const-string v0, "Sgi"

    const-string v1, "SGView doesn\'t support drawables."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-void
.end method

.method public setForceMemoryManagementEnabled(Z)V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setForceMemoryManagementEnabled(Z)V

    .line 383
    return-void
.end method

.method public setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V

    .line 218
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 175
    if-nez p1, :cond_1

    .line 176
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->resume()V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 180
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->suspend()V

    .line 181
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->setVisibility(I)V

    goto :goto_0
.end method

.method public suspend()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->suspend()V

    .line 237
    return-void
.end method
