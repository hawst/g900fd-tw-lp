.class public Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;
.super Ljava/lang/Object;
.source "SGKeyEvent.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 162
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGKeyEvent__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    .line 163
    return-void
.end method

.method public constructor <init>(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;)V
    .locals 3

    .prologue
    .line 181
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    .line 182
    return-void
.end method

.method public constructor <init>(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V
    .locals 3

    .prologue
    .line 172
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    .line 173
    return-void
.end method

.method public constructor <init>(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 200
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    .line 201
    return-void
.end method

.method public constructor <init>(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V
    .locals 3

    .prologue
    .line 191
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    .line 192
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    .line 33
    return-void
.end method

.method private static SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;)J
    .locals 2

    .prologue
    .line 176
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 177
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ordinal()I

    move-result v0

    invoke-static {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGKeyEvent__SWIG_2(II)J

    move-result-wide v0

    return-wide v0
.end method

.method private static SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)J
    .locals 2

    .prologue
    .line 166
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 167
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 168
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->ordinal()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGKeyEvent__SWIG_1(III)J

    move-result-wide v0

    return-wide v0
.end method

.method private static SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 195
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 196
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ordinal()I

    move-result v0

    invoke-static {p0, v0, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGKeyEvent__SWIG_4(IILjava/util/Date;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static SwigConstructSGKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)J
    .locals 2

    .prologue
    .line 185
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 186
    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 187
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ordinal()I

    move-result v0

    invoke-virtual {p4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->ordinal()I

    move-result v1

    invoke-static {p0, v0, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGKeyEvent__SWIG_3(IILjava/util/Date;Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static convertFlagFromAndroid(I)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;
    .locals 1

    .prologue
    .line 132
    sparse-switch p0, :sswitch_data_0

    .line 157
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->NONE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    :goto_0
    return-object v0

    .line 135
    :sswitch_0
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->CANCELED:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 137
    :sswitch_1
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->CANCELED_LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 139
    :sswitch_2
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->EDITOR_ACTION:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 141
    :sswitch_3
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->FALLBACK:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 143
    :sswitch_4
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->FROM_SYSTEM:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 145
    :sswitch_5
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->KEEP_TOUCH_MODE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 147
    :sswitch_6
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 149
    :sswitch_7
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->SOFT_KEYBOARD:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 151
    :sswitch_8
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->TRACKING:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 153
    :sswitch_9
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->VIRTUAL_HARD_KEY:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 155
    :sswitch_a
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->WOKE_HERE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    goto :goto_0

    .line 132
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_a
        0x2 -> :sswitch_7
        0x4 -> :sswitch_5
        0x8 -> :sswitch_4
        0x10 -> :sswitch_2
        0x20 -> :sswitch_0
        0x40 -> :sswitch_9
        0x80 -> :sswitch_6
        0x100 -> :sswitch_1
        0x200 -> :sswitch_8
        0x400 -> :sswitch_3
    .end sparse-switch
.end method

.method public static final createKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Landroid/view/KeyEvent;
    .locals 13

    .prologue
    const/4 v9, 0x0

    .line 59
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v3, v0, v2

    .line 60
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getEventTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long v1, v0, v3

    .line 61
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getDownTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    sub-long v3, v5, v3

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getKeyAction()Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    move-result-object v5

    .line 70
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    if-ne v5, v0, :cond_0

    .line 72
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_UP:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    if-ne v5, v0, :cond_e

    .line 73
    const/4 v0, 0x1

    .line 74
    :goto_0
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_MULTIPLE:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    if-ne v5, v6, :cond_1

    .line 75
    const/4 v0, 0x2

    .line 76
    :cond_1
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    if-ne v5, v6, :cond_d

    .line 77
    const/4 v5, -0x1

    .line 82
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isNumLockOn()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 83
    const/high16 v0, 0x200000

    .line 85
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isScrollLockOn()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 86
    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    .line 89
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isLeftCtrlPressed()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 90
    or-int/lit16 v0, v0, 0x3000

    .line 93
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isRightCtrlPressed()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 94
    or-int/lit16 v0, v0, 0x5000

    .line 97
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isLeftAltPressed()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 98
    or-int/lit8 v0, v0, 0x12

    .line 101
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isRightAltPressed()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 102
    or-int/lit8 v0, v0, 0x22

    .line 105
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isLeftShiftPressed()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 106
    or-int/lit8 v0, v0, 0x41

    .line 108
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isRightShiftPressed()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 109
    or-int/lit16 v0, v0, 0x81

    .line 112
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isMetaPressed()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 113
    const/high16 v6, 0x30000

    or-int/2addr v0, v6

    .line 115
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isSymPressed()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 116
    or-int/lit8 v0, v0, 0x4

    .line 118
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->isFunctionPressed()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 119
    or-int/lit8 v8, v0, 0x8

    .line 122
    :goto_3
    new-instance v0, Landroid/view/KeyEvent;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getKeyCode()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getRepeatCount()I

    move-result v7

    move v10, v9

    move v11, v9

    move v12, v9

    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    .line 124
    return-object v0

    :cond_b
    move v8, v0

    goto :goto_3

    :cond_c
    move v0, v9

    goto :goto_2

    :cond_d
    move v5, v0

    goto :goto_1

    :cond_e
    move v0, v9

    goto :goto_0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCMemOwn:Z

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGKeyEvent(J)V

    .line 45
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    .line 47
    :cond_1
    return-void
.end method

.method public getCharacter()Ljava/lang/String;
    .locals 2

    .prologue
    .line 355
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getCharacter(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDownTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 371
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getDownTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getEventTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 363
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getEventTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getKeyAction()Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;
    .locals 3

    .prologue
    .line 347
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getKeyAction(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getKeyCode()I
    .locals 2

    .prologue
    .line 343
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getKeyCode(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I

    move-result v0

    return v0
.end method

.method public getKeyFlag()Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;
    .locals 3

    .prologue
    .line 351
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getKeyFlag(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getRepeatCount()I
    .locals 2

    .prologue
    .line 223
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_getRepeatCount(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I

    move-result v0

    return v0
.end method

.method public isCapsLockOn()Z
    .locals 2

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isCapsLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isFunctionPressed()Z
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isFunctionPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isLeftAltPressed()Z
    .locals 2

    .prologue
    .line 243
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isLeftAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isLeftCtrlPressed()Z
    .locals 2

    .prologue
    .line 227
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isLeftCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isLeftShiftPressed()Z
    .locals 2

    .prologue
    .line 235
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isLeftShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isMetaPressed()Z
    .locals 2

    .prologue
    .line 255
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isMetaPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isNumLockOn()Z
    .locals 2

    .prologue
    .line 263
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isNumLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isPrintable()Z
    .locals 2

    .prologue
    .line 279
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isPrintable(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isRightAltPressed()Z
    .locals 2

    .prologue
    .line 247
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isRightAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isRightCtrlPressed()Z
    .locals 2

    .prologue
    .line 231
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isRightCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isRightShiftPressed()Z
    .locals 2

    .prologue
    .line 239
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isRightShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isScrollLockOn()Z
    .locals 2

    .prologue
    .line 271
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isScrollLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isSymPressed()Z
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isSymPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public isSystem()Z
    .locals 2

    .prologue
    .line 275
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_isSystem(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public resetKeyEvent()V
    .locals 2

    .prologue
    .line 339
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_resetKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)V

    .line 340
    return-void
.end method

.method public setCapsLockOn(Z)V
    .locals 2

    .prologue
    .line 323
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setCapsLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 324
    return-void
.end method

.method public setDownTime(Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 367
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setDownTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Ljava/util/Date;)V

    .line 368
    return-void
.end method

.method public setEventTime(Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 359
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setEventTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Ljava/util/Date;)V

    .line 360
    return-void
.end method

.method public setFunctionPressed(Z)V
    .locals 2

    .prologue
    .line 307
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setFunctionPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 308
    return-void
.end method

.method public setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 212
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 214
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ordinal()I

    move-result v4

    move-object v2, p0

    move v3, p1

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setKeyEvent__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;IILjava/util/Date;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method public setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V
    .locals 8

    .prologue
    .line 204
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 205
    :cond_0
    if-nez p5, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 207
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ordinal()I

    move-result v4

    invoke-virtual {p5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->ordinal()I

    move-result v7

    move-object v2, p0

    move v3, p1

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setKeyEvent__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;IILjava/util/Date;Ljava/lang/String;I)V

    .line 209
    return-void
.end method

.method public setLeftAltPressed(Z)V
    .locals 2

    .prologue
    .line 299
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setLeftAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 300
    return-void
.end method

.method public setLeftCtrlPressed(Z)V
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setLeftCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 284
    return-void
.end method

.method public setLeftShiftPressed(Z)V
    .locals 2

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setLeftShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 292
    return-void
.end method

.method public setMetaPressed(Z)V
    .locals 2

    .prologue
    .line 311
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setMetaPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 312
    return-void
.end method

.method public setNumLockOn(Z)V
    .locals 2

    .prologue
    .line 319
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setNumLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 320
    return-void
.end method

.method public setPrintable(Z)V
    .locals 2

    .prologue
    .line 335
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setPrintable(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 336
    return-void
.end method

.method public setRepeatCount(I)V
    .locals 2

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setRepeatCount(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;I)V

    .line 220
    return-void
.end method

.method public setRightAltPressed(Z)V
    .locals 2

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setRightAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 304
    return-void
.end method

.method public setRightCtrlPressed(Z)V
    .locals 2

    .prologue
    .line 287
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setRightCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 288
    return-void
.end method

.method public setRightShiftPressed(Z)V
    .locals 2

    .prologue
    .line 295
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setRightShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 296
    return-void
.end method

.method public setScrollLockOn(Z)V
    .locals 2

    .prologue
    .line 327
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setScrollLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 328
    return-void
.end method

.method public setSymPressed(Z)V
    .locals 2

    .prologue
    .line 315
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setSymPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 316
    return-void
.end method

.method public setSystem(Z)V
    .locals 2

    .prologue
    .line 331
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGKeyEvent_setSystem(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V

    .line 332
    return-void
.end method
