.class final Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;
.source "SGSurfaceChangesDrawnListenerHolder.java"


# instance fields
.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;

.field mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    .line 25
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;

    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceChangesDrawnListenerArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    return-void
.end method


# virtual methods
.method public detachListener()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceChangesDrawnListenerArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public onChangesDrawn()V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->detachListener()V

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;->onChangesDrawn()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 45
    const-string v1, "SGSurfaceChangesDrawnListener::onChangesDrawn error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
