.class public Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGLineWidthProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGLineWidthProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;-><init>(JZ)V

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->init()V

    .line 51
    return-void
.end method

.method public constructor <init>(F)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGLineWidthProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;-><init>(JZ)V

    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->init(F)V

    .line 46
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)V

    .line 59
    return-void
.end method

.method private init(F)V
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;F)V

    .line 55
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;
    .locals 7

    .prologue
    .line 78
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGLineWidthProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getWidth()F
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty_getWidth(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)F

    move-result v0

    return v0
.end method

.method public isLineWidthPropertyEnabled()Z
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty_isLineWidthPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)Z

    move-result v0

    return v0
.end method

.method public setLineWidthPropertyEnabled(Z)V
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty_setLineWidthPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;Z)V

    .line 71
    return-void
.end method

.method public setWidth(F)V
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGLineWidthProperty_setWidth(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;F)V

    .line 67
    return-void
.end method
