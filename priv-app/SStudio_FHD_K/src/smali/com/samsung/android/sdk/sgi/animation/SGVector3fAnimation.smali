.class public final Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
.super Lcom/samsung/android/sdk/sgi/animation/SGPropertyAnimation;
.source "SGVector3fAnimation.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGPropertyAnimation;-><init>(JZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 7

    .prologue
    .line 65
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v6
.end method

.method public addKeyFrame(FLcom/samsung/android/sdk/sgi/base/SGVector3f;)Z
    .locals 7

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;FJLcom/samsung/android/sdk/sgi/base/SGVector3f;)Z

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGVector3fAnimation(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public getEndValue()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getStartValue()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public removeKeyFrame(F)Z
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;F)Z

    move-result v0

    return v0
.end method

.method public setEndValue(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 50
    return-void
.end method

.method public setStartValue(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVector3fAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 42
    return-void
.end method
