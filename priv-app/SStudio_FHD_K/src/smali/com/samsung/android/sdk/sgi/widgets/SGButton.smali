.class public Lcom/samsung/android/sdk/sgi/widgets/SGButton;
.super Lcom/samsung/android/sdk/sgi/widgets/SGLabel;
.source "SGButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGButton$1;,
        Lcom/samsung/android/sdk/sgi/widgets/SGButton$SGStateListeningButton;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, ""

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method private isPointOnButton(FF)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    cmpl-float v0, p2, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processTouch(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    .line 138
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 139
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY(I)F

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->isPointOnButton(FF)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 140
    add-int/lit8 v4, v4, 0x1

    .line 141
    sget-object v5, Lcom/samsung/android/sdk/sgi/widgets/SGButton$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction(I)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 138
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :pswitch_0
    add-int/lit8 v3, v3, 0x1

    .line 144
    goto :goto_1

    .line 147
    :pswitch_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 154
    :cond_1
    if-lez v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->setPressed(Z)Z

    .line 156
    :cond_2
    if-eqz v4, :cond_3

    if-ne v4, v2, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v2, :cond_6

    .line 157
    :cond_5
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->setPressed(Z)Z

    .line 158
    :cond_6
    return-void

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected createComponent(Landroid/content/Context;)Landroid/widget/Button;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGButton$SGStateListeningButton;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton$SGStateListeningButton;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGButton;Landroid/content/Context;)V

    .line 100
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton$SGStateListeningButton;->setGravity(I)V

    .line 102
    return-object v0
.end method

.method protected bridge synthetic createComponent(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->createComponent(Landroid/content/Context;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public isPressed()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isPressed()Z

    move-result v0

    return v0
.end method

.method protected onDrawableStateChanged()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
    .locals 1

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->processTouch(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V

    .line 124
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public setPressed(Z)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->isPressed()Z

    move-result v1

    if-ne v1, p1, :cond_0

    .line 185
    :goto_0
    return v0

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 172
    if-eqz p1, :cond_2

    .line 173
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v1

    .line 174
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 175
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 176
    array-length v3, v1

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [I

    .line 177
    const v4, 0x101009d

    aput v4, v3, v0

    .line 178
    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 179
    add-int/lit8 v4, v0, 0x1

    aget v5, v1, v0

    aput v5, v3, v4

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 181
    :cond_1
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 184
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->invalidate()V

    .line 185
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setupDefaultTextColors(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 84
    const v1, 0x1010036

    aput v1, v0, v3

    .line 85
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->androidComponent:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 89
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 90
    return-void
.end method
