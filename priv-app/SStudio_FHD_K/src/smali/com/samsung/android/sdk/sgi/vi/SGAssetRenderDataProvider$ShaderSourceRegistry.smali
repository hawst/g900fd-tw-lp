.class final Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;
.super Ljava/lang/Object;
.source "SGAssetRenderDataProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ShaderSourceRegistry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;
    }
.end annotation


# static fields
.field private static final DATA_FILE_NAME:Ljava/lang/String; = "shader-source-info"


# instance fields
.field private mAssetManager:Landroid/content/res/AssetManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentVersion:J

.field private mRegistry:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;",
            ">;"
        }
    .end annotation
.end field

.field private mShaderPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mContext:Landroid/content/Context;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mAssetManager:Landroid/content/res/AssetManager;

    .line 56
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mShaderPath:Ljava/lang/String;

    .line 58
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-wide v0, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mCurrentVersion:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    :try_start_1
    new-instance v1, Ljava/io/ObjectInputStream;

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mContext:Landroid/content/Context;

    const-string v2, "shader-source-info"

    invoke-virtual {v0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 65
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mRegistry:Ljava/util/HashMap;

    .line 66
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 70
    :goto_1
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    const-string v0, "SGAssetRenderDataProvider"

    const-string v1, "version: current version is undefined"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mCurrentVersion:J

    goto :goto_0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mRegistry:Ljava/util/HashMap;

    goto :goto_1
.end method

.method private loadShaderBody(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;

    .line 83
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->body:[B

    if-eqz v1, :cond_0

    .line 103
    :goto_0
    return-object v0

    .line 87
    :cond_0
    :try_start_0
    # invokes: Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->loadBuiltinShaderData(Ljava/lang/String;)[B
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->access$000(Ljava/lang/String;)[B

    move-result-object v1

    .line 88
    if-nez v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mShaderPath:Ljava/lang/String;

    invoke-static {v1, v2, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->loadShaderFromAssets(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 91
    :cond_1
    if-nez v0, :cond_2

    .line 92
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mCurrentVersion:J

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;-><init>([BJ)V

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->save()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 102
    const-string v1, "SGAssetRenderDataProvider"

    const-string v2, "shader\'s \'%s\' body load failed: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :cond_2
    :try_start_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mCurrentVersion:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->update([BJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public get(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->loadShaderBody(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->body:[B

    goto :goto_0
.end method

.method public getHash(Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;

    .line 112
    if-eqz v0, :cond_0

    iget-wide v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->version:J

    iget-wide v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mCurrentVersion:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    .line 113
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->loadShaderBody(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;

    move-result-object v0

    .line 114
    :cond_1
    if-nez v0, :cond_2

    .line 115
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 116
    :cond_2
    iget v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->hash:I

    return v0
.end method

.method public save()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 73
    :try_start_0
    new-instance v0, Ljava/io/ObjectOutputStream;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mContext:Landroid/content/Context;

    const-string v2, "shader-source-info"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 75
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 76
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    const-string v1, "SGAssetRenderDataProvider"

    const-string v2, "shader source registry save failed: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
