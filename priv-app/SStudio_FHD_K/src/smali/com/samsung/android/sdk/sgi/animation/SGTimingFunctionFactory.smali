.class public final Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionFactory;
.super Ljava/lang/Object;
.source "SGTimingFunctionFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAccelerateTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 28
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->ordinal()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createAccelerateTimingFunction__SWIG_0(I)J

    move-result-wide v1

    .line 32
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 33
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 34
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method public static createAccelerateTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;F)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 40
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->ordinal()I

    move-result v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createAccelerateTimingFunction__SWIG_1(IF)J

    move-result-wide v1

    .line 44
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 47
    :goto_0
    return-object v0

    .line 45
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method public static createBounceEaseTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGEaseTimingFunctionType;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 52
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGEaseTimingFunctionType;->ordinal()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createBounceEaseTimingFunction(I)J

    move-result-wide v1

    .line 56
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 59
    :goto_0
    return-object v0

    .line 57
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 58
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method public static createCubicBezierTimingFunction(FFFF)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 76
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createCubicBezierTimingFunction__SWIG_1(FFFF)J

    move-result-wide v1

    .line 78
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    .line 79
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 80
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method public static createCubicBezierTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 64
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->ordinal()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createCubicBezierTimingFunction__SWIG_0(I)J

    move-result-wide v1

    .line 68
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 71
    :goto_0
    return-object v0

    .line 69
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 70
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method public static createLinearTimingFunction()Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 85
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createLinearTimingFunction()J

    move-result-wide v1

    .line 87
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 90
    :goto_0
    return-object v0

    .line 88
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 89
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method public static createPredefinedTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 5

    .prologue
    .line 94
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->ordinal()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTimingFunctionFactory_createPredefinedTimingFunction(I)J

    move-result-wide v1

    .line 98
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 101
    :goto_0
    return-object v0

    .line 99
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionHolder;-><init>(JZ)V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    goto :goto_0
.end method

.method private deleteTimingFunctionFactory()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method
