.class public Lcom/samsung/android/sdk/sgi/render/SGProperty;
.super Ljava/lang/Object;
.source "SGProperty.java"


# static fields
.field public static ProgramPropertyName:Ljava/lang/String;


# instance fields
.field protected swigCMemOwn:Z

.field protected swigCPtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "SGProgram"

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->ProgramPropertyName:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 71
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCPtr:J

    .line 28
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGProperty_getHandle(JLcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 54
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/render/SGProperty;

    if-nez v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getHandle()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getHandle()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGProperty(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGProperty;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getHandle()J

    move-result-wide v0

    .line 62
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 63
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method
