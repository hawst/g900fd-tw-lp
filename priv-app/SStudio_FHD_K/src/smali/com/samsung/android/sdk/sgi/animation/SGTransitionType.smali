.class public final enum Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;
.super Ljava/lang/Enum;
.source "SGTransitionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum COVER_WITH_NEXT_PAGE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum CUBE2:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum CURTAIN:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum CURVE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum FADE_IN_OUT:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum FLIP3:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum FOLDING:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum GALLERY:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum PIVOT:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum ROTATE_CUBE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum SCALE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum THROUGH:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum WAVE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

.field public static final enum WIPE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "CURTAIN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->CURTAIN:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "FOLDING"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->FOLDING:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "COVER_WITH_NEXT_PAGE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->COVER_WITH_NEXT_PAGE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "CUBE2"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->CUBE2:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "CURVE"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->CURVE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "FADE_IN_OUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->FADE_IN_OUT:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "FLIP3"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->FLIP3:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "GALLERY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->GALLERY:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "PIVOT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->PIVOT:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "ROTATE_CUBE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->ROTATE_CUBE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 29
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "SCALE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->SCALE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 30
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "THROUGH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->THROUGH:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 31
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "WAVE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->WAVE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 32
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    const-string v1, "WIPE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->WIPE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    .line 18
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->CURTAIN:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->FOLDING:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->COVER_WITH_NEXT_PAGE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->CUBE2:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->CURVE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->FADE_IN_OUT:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->FLIP3:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->GALLERY:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->PIVOT:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->ROTATE_CUBE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->SCALE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->THROUGH:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->WAVE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->WIPE:Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    return-object v0
.end method
