.class public Lcom/samsung/android/sdk/sgi/base/SGVector4f;
.super Ljava/lang/Object;
.source "SGVector4f.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector4f__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector4f__SWIG_2(FFFF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    .line 55
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 63
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->SwigConstructSGVector4f(Landroid/graphics/RectF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 3

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector4f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    .line 51
    return-void
.end method

.method private static SwigConstructSGVector4f(Landroid/graphics/RectF;)J
    .locals 2

    .prologue
    .line 58
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector4f__SWIG_3(Landroid/graphics/RectF;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 139
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_add(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 140
    return-void
.end method

.method public divide(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 151
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_divide(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 152
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGVector4f(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getAsRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getAsRect(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDistance(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)F
    .locals 6

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public getDotProduct(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)F
    .locals 6

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public getW()F
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getW(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getX(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getY(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public getZ()F
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_getZ(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F

    move-result v0

    return v0
.end method

.method public interpolate(Lcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
    .locals 7

    .prologue
    .line 131
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_interpolate(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V

    .line 132
    return-void
.end method

.method public inverse()V
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_inverse(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 124
    return-void
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGVector4f;F)Z
    .locals 7

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)Z

    move-result v0

    return v0
.end method

.method public multiply(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 147
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 148
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 120
    return-void
.end method

.method public scale(F)V
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_scale(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V

    .line 128
    return-void
.end method

.method public set(FFFF)V
    .locals 7

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_set(JLcom/samsung/android/sdk/sgi/base/SGVector4f;FFFF)V

    .line 104
    return-void
.end method

.method public setW(F)V
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_setW(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V

    .line 100
    return-void
.end method

.method public setX(F)V
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_setX(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V

    .line 76
    return-void
.end method

.method public setY(F)V
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_setY(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V

    .line 84
    return-void
.end method

.method public setZ(F)V
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_setZ(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V

    .line 92
    return-void
.end method

.method public subtract(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector4f_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 144
    return-void
.end method
