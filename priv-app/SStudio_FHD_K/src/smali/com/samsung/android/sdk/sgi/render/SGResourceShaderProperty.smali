.class public final Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;
.source "SGResourceShaderProperty.java"


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGResourceShaderProperty__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;-><init>(JZ)V

    .line 41
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 60
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->SwigConstructSGResourceShaderProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;-><init>(JZ)V

    .line 61
    return-void
.end method

.method private static SwigConstructSGResourceShaderProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 55
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->ordinal()I

    move-result v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGResourceShaderProperty__SWIG_1(ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private init(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2, p2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGResourceShaderProperty_init(JLcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;ILjava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;)Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;
    .locals 7

    .prologue
    .line 51
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGResourceShaderProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGResourceShaderProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method
