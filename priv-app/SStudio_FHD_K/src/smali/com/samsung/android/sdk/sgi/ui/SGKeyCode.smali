.class public final Lcom/samsung/android/sdk/sgi/ui/SGKeyCode;
.super Ljava/lang/Object;
.source "SGKeyCode.java"


# static fields
.field public static final CODE_0:I = 0x7

.field public static final CODE_1:I = 0x8

.field public static final CODE_2:I = 0x9

.field public static final CODE_3:I = 0xa

.field public static final CODE_4:I = 0xb

.field public static final CODE_5:I = 0xc

.field public static final CODE_6:I = 0xd

.field public static final CODE_7:I = 0xe

.field public static final CODE_8:I = 0xf

.field public static final CODE_9:I = 0x10

.field public static final CODE_A:I = 0x1d

.field public static final CODE_ALT_LEFT:I = 0x39

.field public static final CODE_ALT_RIGHT:I = 0x3a

.field public static final CODE_APOSTROPHE:I = 0x4b

.field public static final CODE_APP_SWITCH:I = 0xbb

.field public static final CODE_AT:I = 0x4d

.field public static final CODE_B:I = 0x1e

.field public static final CODE_BACK:I = 0x4

.field public static final CODE_BACKSLASH:I = 0x49

.field public static final CODE_BACK_SURROUND:I = 0x2711

.field public static final CODE_BREAK:I = 0x79

.field public static final CODE_BUTTON_A:I = 0x60

.field public static final CODE_BUTTON_B:I = 0x61

.field public static final CODE_BUTTON_C:I = 0x62

.field public static final CODE_BUTTON_L1:I = 0x66

.field public static final CODE_BUTTON_L2:I = 0x68

.field public static final CODE_BUTTON_MODE:I = 0x6e

.field public static final CODE_BUTTON_R1:I = 0x67

.field public static final CODE_BUTTON_R2:I = 0x69

.field public static final CODE_BUTTON_SELECT:I = 0x6d

.field public static final CODE_BUTTON_START:I = 0x6c

.field public static final CODE_BUTTON_THUMBL:I = 0x6a

.field public static final CODE_BUTTON_THUMBR:I = 0x6b

.field public static final CODE_BUTTON_X:I = 0x63

.field public static final CODE_BUTTON_Y:I = 0x64

.field public static final CODE_BUTTON_Z:I = 0x65

.field public static final CODE_C:I = 0x1f

.field public static final CODE_CALL:I = 0x5

.field public static final CODE_CAMERA:I = 0x1b

.field public static final CODE_CAPS_LOCK:I = 0x73

.field public static final CODE_CLEAR:I = 0x1c

.field public static final CODE_CLIPBOARD:I = 0xdd

.field public static final CODE_COMMA:I = 0x37

.field public static final CODE_CTRL_LEFT:I = 0x71

.field public static final CODE_CTRL_RIGHT:I = 0x72

.field public static final CODE_D:I = 0x20

.field public static final CODE_DEL:I = 0x43

.field public static final CODE_DOWN:I = 0x14

.field public static final CODE_DPAD_CENTER:I = 0x17

.field public static final CODE_E:I = 0x21

.field public static final CODE_ENDCALL:I = 0x6

.field public static final CODE_ENTER:I = 0x42

.field public static final CODE_ENVELOPE:I = 0x41

.field public static final CODE_EQUALS:I = 0x46

.field public static final CODE_ESCAPE:I = 0x6f

.field public static final CODE_EXPLORER:I = 0x40

.field public static final CODE_F:I = 0x22

.field public static final CODE_F1:I = 0x83

.field public static final CODE_F10:I = 0x8c

.field public static final CODE_F11:I = 0x8d

.field public static final CODE_F12:I = 0x8e

.field public static final CODE_F2:I = 0x84

.field public static final CODE_F3:I = 0x85

.field public static final CODE_F4:I = 0x86

.field public static final CODE_F5:I = 0x87

.field public static final CODE_F6:I = 0x88

.field public static final CODE_F7:I = 0x89

.field public static final CODE_F8:I = 0x8a

.field public static final CODE_F9:I = 0x8b

.field public static final CODE_FOCUS:I = 0x50

.field public static final CODE_FORWARD_DEL:I = 0x70

.field public static final CODE_FUNCTION:I = 0x77

.field public static final CODE_G:I = 0x23

.field public static final CODE_GRAVE:I = 0x44

.field public static final CODE_H:I = 0x24

.field public static final CODE_HEADSETHOOK:I = 0x4f

.field public static final CODE_HOME:I = 0x3

.field public static final CODE_I:I = 0x25

.field public static final CODE_INSERT:I = 0x7c

.field public static final CODE_J:I = 0x26

.field public static final CODE_K:I = 0x27

.field public static final CODE_L:I = 0x28

.field public static final CODE_LEFT:I = 0x15

.field public static final CODE_LEFT_BRACKET:I = 0x47

.field public static final CODE_M:I = 0x29

.field public static final CODE_MEDIA_FAST_FORWARD:I = 0x5a

.field public static final CODE_MEDIA_NEXT:I = 0x57

.field public static final CODE_MEDIA_PLAY_PAUSE:I = 0x55

.field public static final CODE_MEDIA_PREVIOUS:I = 0x58

.field public static final CODE_MEDIA_REWIND:I = 0x59

.field public static final CODE_MEDIA_STOP:I = 0x56

.field public static final CODE_MENU:I = 0x52

.field public static final CODE_META_LEFT:I = 0x75

.field public static final CODE_META_RIGHT:I = 0x76

.field public static final CODE_MINUS:I = 0x45

.field public static final CODE_MOVE_END:I = 0x7b

.field public static final CODE_MOVE_HOME:I = 0x7a

.field public static final CODE_MUTE:I = 0x5b

.field public static final CODE_N:I = 0x2a

.field public static final CODE_NOTIFICATION:I = 0x53

.field public static final CODE_NUM:I = 0x4e

.field public static final CODE_NUMPAD_0:I = 0x90

.field public static final CODE_NUMPAD_1:I = 0x91

.field public static final CODE_NUMPAD_2:I = 0x92

.field public static final CODE_NUMPAD_3:I = 0x93

.field public static final CODE_NUMPAD_4:I = 0x94

.field public static final CODE_NUMPAD_5:I = 0x95

.field public static final CODE_NUMPAD_6:I = 0x96

.field public static final CODE_NUMPAD_7:I = 0x97

.field public static final CODE_NUMPAD_8:I = 0x98

.field public static final CODE_NUMPAD_9:I = 0x99

.field public static final CODE_NUMPAD_ADD:I = 0x9d

.field public static final CODE_NUMPAD_COMMA:I = 0x9f

.field public static final CODE_NUMPAD_DIVIDE:I = 0x9a

.field public static final CODE_NUMPAD_DOT:I = 0x9e

.field public static final CODE_NUMPAD_ENTER:I = 0xa0

.field public static final CODE_NUMPAD_EQUALS:I = 0xa1

.field public static final CODE_NUMPAD_LEFT_PAREN:I = 0xa2

.field public static final CODE_NUMPAD_MULTIPLY:I = 0x9b

.field public static final CODE_NUMPAD_RIGHT_PAREN:I = 0xa3

.field public static final CODE_NUMPAD_SUBTRACT:I = 0x9c

.field public static final CODE_NUM_LOCK:I = 0x8f

.field public static final CODE_O:I = 0x2b

.field public static final CODE_P:I = 0x2c

.field public static final CODE_PAGE_DOWN:I = 0x5d

.field public static final CODE_PAGE_UP:I = 0x5c

.field public static final CODE_PERIOD:I = 0x38

.field public static final CODE_PICTSYMBOLS:I = 0x5e

.field public static final CODE_PLUS:I = 0x51

.field public static final CODE_POUND:I = 0x12

.field public static final CODE_POWER:I = 0x1a

.field public static final CODE_Q:I = 0x2d

.field public static final CODE_R:I = 0x2e

.field public static final CODE_RIGHT:I = 0x16

.field public static final CODE_RIGHT_BRACKET:I = 0x48

.field public static final CODE_S:I = 0x2f

.field public static final CODE_SCROLL_LOCK:I = 0x74

.field public static final CODE_SEARCH:I = 0x54

.field public static final CODE_SEMICOLON:I = 0x4a

.field public static final CODE_SHIFT_LEFT:I = 0x3b

.field public static final CODE_SHIFT_RIGHT:I = 0x3c

.field public static final CODE_SLASH:I = 0x4c

.field public static final CODE_SOFT_LEFT:I = 0x1

.field public static final CODE_SOFT_RIGHT:I = 0x2

.field public static final CODE_SPACE:I = 0x3e

.field public static final CODE_STAR:I = 0x11

.field public static final CODE_SWITCH_CHARSET:I = 0x5f

.field public static final CODE_SYM:I = 0x3f

.field public static final CODE_T:I = 0x30

.field public static final CODE_TAB:I = 0x3d

.field public static final CODE_U:I = 0x31

.field public static final CODE_UNKNOWN:I = 0x0

.field public static final CODE_UP:I = 0x13

.field public static final CODE_V:I = 0x32

.field public static final CODE_VOLUME_DOWN:I = 0x19

.field public static final CODE_VOLUME_UP:I = 0x18

.field public static final CODE_W:I = 0x33

.field public static final CODE_X:I = 0x34

.field public static final CODE_Y:I = 0x35

.field public static final CODE_Z:I = 0x36


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
