.class public final Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;
.super Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;
.source "SGLinearValueInterpolator.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;-><init>(Z)V

    .line 43
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 44
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;-><init>(JZ)V

    .line 27
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3

    .prologue
    .line 67
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->new_SGLinearValueInterpolator__SWIG_1(Z)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;-><init>(JZ)V

    .line 68
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGLinearValueInterpolator(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public interpolate(FFF)F
    .locals 6

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGLinearValueInterpolator_interpolate__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FFF)F

    move-result v0

    return v0
.end method

.method public interpolate(FLcom/samsung/android/sdk/sgi/base/SGQuaternion;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 11

    .prologue
    .line 63
    new-instance v10, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGLinearValueInterpolator_interpolate__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v10
.end method

.method public interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 11

    .prologue
    .line 51
    new-instance v10, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGLinearValueInterpolator_interpolate__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v10
.end method

.method public interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 11

    .prologue
    .line 55
    new-instance v10, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGLinearValueInterpolator_interpolate__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v10
.end method

.method public interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector4f;Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 11

    .prologue
    .line 59
    new-instance v10, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGLinearValueInterpolator_interpolate__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v10
.end method
