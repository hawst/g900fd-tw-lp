.class Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;
.super Ljava/lang/Object;
.source "SGAssetRenderDataProvider.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShaderItem"
.end annotation


# instance fields
.field public transient body:[B

.field public hash:I

.field public version:J


# direct methods
.method public constructor <init>([BJ)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->body:[B

    .line 37
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->update([BJ)V

    .line 38
    return-void
.end method


# virtual methods
.method public update([BJ)V
    .locals 2

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->body:[B

    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->version:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_0

    .line 42
    iput-wide p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->version:J

    .line 43
    invoke-static {p1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry$ShaderItem;->hash:I

    .line 45
    :cond_0
    return-void
.end method
