.class public Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;
.super Lcom/samsung/android/sdk/sgi/widgets/SGLabel;
.source "SGEditTextBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$3;,
        Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;,
        Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomDoubleClickDetector;,
        Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "EditTextBox"

.field private static final mDefaultCursorWidth:F = 2.0f

.field static mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;


# instance fields
.field gestureDetector:Landroid/view/GestureDetector;

.field mContext:Landroid/content/Context;

.field private mCursorColor:I

.field mCursorGlyphVisible:Z

.field private mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field mCursorVisible:Z

.field mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

.field private mDefaultTextTypeface:Landroid/graphics/Typeface;

.field private mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private mGravity:I

.field private mHighlightColor:I

.field private mHorizontalGravityMask:I

.field mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mIsHideCursor:Z

.field private mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

.field private mTextTypeface:Landroid/graphics/Typeface;

.field private mVerticalGravityMask:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;)V

    .line 82
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    .line 83
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    .line 85
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorVisible:Z

    .line 86
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorGlyphVisible:Z

    .line 90
    const v0, 0x6633b5e5

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHighlightColor:I

    .line 92
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    .line 94
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorColor:I

    .line 95
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mIsHideCursor:Z

    .line 96
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 97
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 201
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->gestureDetector:Landroid/view/GestureDetector;

    .line 442
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 1117
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHorizontalGravityMask:I

    .line 1118
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mVerticalGravityMask:I

    .line 1119
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mGravity:I

    .line 120
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mDefaultSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->initWidget(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 121
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mAutoSizeEnabled:Z

    .line 122
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->makeAutosize()V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 82
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    .line 83
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    .line 85
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorVisible:Z

    .line 86
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorGlyphVisible:Z

    .line 90
    const v0, 0x6633b5e5

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHighlightColor:I

    .line 92
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    .line 94
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorColor:I

    .line 95
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mIsHideCursor:Z

    .line 96
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 97
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 201
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->gestureDetector:Landroid/view/GestureDetector;

    .line 442
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 1117
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHorizontalGravityMask:I

    .line 1118
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mVerticalGravityMask:I

    .line 1119
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mGravity:I

    .line 113
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->initWidget(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 114
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->selectWord()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;II)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onSelectionChangedWrapper(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;IIII)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onScrollChangedWrapper(IIII)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)Landroid/widget/TextView$OnEditorActionListener;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    return-object v0
.end method

.method private static actionFromTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I
    .locals 2

    .prologue
    .line 692
    const/4 v0, 0x3

    .line 693
    if-nez p0, :cond_0

    .line 708
    :goto_0
    return v0

    .line 697
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$3;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 706
    const/4 v0, 0x2

    goto :goto_0

    .line 700
    :pswitch_0
    const/4 v0, 0x0

    .line 701
    goto :goto_0

    .line 703
    :pswitch_1
    const/4 v0, 0x1

    .line 704
    goto :goto_0

    .line 697
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static checkInputConnectionProxy(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 556
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    if-nez v1, :cond_1

    .line 563
    :cond_0
    :goto_0
    return v0

    .line 559
    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    .line 560
    if-eqz v1, :cond_0

    .line 563
    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private customForwardTouchToDetectors(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 817
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->gestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 818
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v5

    .line 819
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v6

    .line 820
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->actionFromTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I

    move-result v4

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v1

    .line 824
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 825
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 826
    if-eqz v0, :cond_0

    move v7, v0

    .line 830
    :cond_0
    return v7
.end method

.method private getRootView()Landroid/view/View;
    .locals 2

    .prologue
    .line 566
    const/4 v0, 0x0

    .line 568
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 569
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getView()Landroid/view/View;

    move-result-object v0

    .line 571
    :cond_0
    return-object v0
.end method

.method private initCursor()V
    .locals 3

    .prologue
    .line 198
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 199
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 200
    return-void
.end method

.method private initDefaultPointers()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 164
    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 165
    const v1, 0x10102c5

    aput v1, v0, v3

    .line 166
    const v1, 0x10102c6

    aput v1, v0, v4

    .line 167
    const v1, 0x10102c7

    aput v1, v0, v5

    .line 168
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 170
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 171
    if-eqz v1, :cond_0

    .line 172
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setupPointerSize(I)V

    .line 173
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setLeftSelectionPointerDrawable(I)V

    .line 175
    :cond_0
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 176
    if-eqz v1, :cond_1

    .line 177
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setRightSelectionPointerDrawable(I)V

    .line 179
    :cond_1
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 180
    if-eqz v1, :cond_2

    .line 181
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorPointerDrawable(I)V

    .line 184
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 185
    return-void
.end method

.method private initGestures()V
    .locals 4

    .prologue
    .line 204
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomDoubleClickDetector;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomDoubleClickDetector;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->gestureDetector:Landroid/view/GestureDetector;

    .line 205
    return-void
.end method

.method private initWidget(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 139
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    .line 140
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 142
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    .line 145
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->addView(Landroid/view/View;)V

    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->invalidate()V

    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->update()V

    .line 149
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setFocusable(Z)V

    .line 150
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->initCursor()V

    .line 151
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->initSelectionPolicy()V

    .line 152
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->initGestures()V

    .line 153
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 154
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setGravity(I)V

    .line 155
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 157
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->initDefaultPointers()V

    .line 158
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setHoverable(Z)V

    .line 160
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 161
    return-void
.end method

.method private isPointOnButton(FF)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1639
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    cmpl-float v0, p2, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static onInputConnectionCreated(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 527
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    if-nez v1, :cond_1

    .line 535
    :cond_0
    :goto_0
    return-object v0

    .line 530
    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 531
    if-eqz v1, :cond_0

    .line 534
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;

    sget-object v2, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;-><init>(Landroid/view/inputmethod/InputConnection;Landroid/widget/EditText;)V

    goto :goto_0
.end method

.method private onScrollChangedWrapper(IIII)V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    .line 337
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursorGlyph()V

    .line 338
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSelectionPointers()V

    .line 339
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onScrollChanged(IIII)V

    .line 340
    return-void
.end method

.method private onSelectionChangedWrapper(II)V
    .locals 2

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    .line 317
    const/4 v0, 0x0

    .line 318
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v1, :cond_0

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionPointersEmptySelection()Z

    move-result v0

    .line 321
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hasSelection()Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 322
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSelectionPointers()V

    .line 324
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onSelectionChanged(II)V

    .line 325
    return-void
.end method

.method private onTextAppearenceChanged()V
    .locals 0

    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->horizontalScrollCheckOnTextChanged()V

    .line 500
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSelectionPointers()V

    .line 501
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    .line 502
    return-void
.end method

.method private restartInput()V
    .locals 2

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 618
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    .line 619
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1, v0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 621
    :cond_0
    return-void
.end method

.method private selectWord()V
    .locals 5

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 224
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getCursorPosition()I

    move-result v1

    .line 225
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v4

    .line 227
    if-nez v4, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    if-lez v1, :cond_3

    add-int/lit8 v0, v1, -0x1

    .line 231
    :goto_1
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    .line 232
    invoke-static {v2}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 235
    :goto_2
    if-lez v0, :cond_8

    .line 236
    if-lt v0, v4, :cond_4

    .line 235
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 230
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 239
    :cond_4
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    .line 240
    invoke-static {v2}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 241
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    .line 245
    :goto_3
    if-ge v1, v4, :cond_7

    move v0, v1

    .line 246
    :goto_4
    if-ge v0, v4, :cond_5

    .line 247
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    .line 248
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 253
    :cond_5
    :goto_5
    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSelection(II)V

    goto :goto_0

    .line 246
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v2, v0

    goto :goto_3
.end method

.method private setCursorPosition(IZ)V
    .locals 1

    .prologue
    .line 1213
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1214
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorVisible(Z)V

    .line 1215
    if-eqz p2, :cond_0

    .line 1217
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1218
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1220
    :cond_0
    return-void
.end method

.method private setPressed(Z)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1617
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isPressed()Z

    move-result v1

    if-ne v1, p1, :cond_0

    .line 1635
    :goto_0
    return v0

    .line 1620
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 1622
    if-eqz p1, :cond_2

    .line 1623
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v1

    .line 1624
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1625
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 1626
    array-length v3, v1

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [I

    .line 1627
    const v4, 0x101009d

    aput v4, v3, v0

    .line 1628
    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 1629
    add-int/lit8 v4, v0, 0x1

    aget v5, v1, v0

    aput v5, v3, v4

    .line 1628
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1631
    :cond_1
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1634
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1635
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startInput()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 658
    sput-object p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    .line 660
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 661
    if-nez v0, :cond_0

    .line 671
    :goto_0
    return-void

    .line 664
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 665
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 666
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 667
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setFocus()Z

    .line 669
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSip()V

    .line 670
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    goto :goto_0
.end method

.method private startSipInput()V
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 594
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setFocus()Z

    .line 595
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->startInput()V

    .line 596
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSip()V

    .line 597
    return-void
.end method

.method private stopInput()V
    .locals 0

    .prologue
    .line 674
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 675
    return-void
.end method

.method private stopSipInput()V
    .locals 0

    .prologue
    .line 599
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSip()V

    .line 600
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->stopInput()V

    .line 601
    return-void
.end method

.method static suspendSip()V
    .locals 1

    .prologue
    .line 636
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-nez v0, :cond_1

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isInFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->clearFocus()V

    .line 642
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    goto :goto_0
.end method

.method private textSettedUp()V
    .locals 2

    .prologue
    .line 1033
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setScrollX(I)V

    .line 1034
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->restartInput()V

    .line 1035
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1036
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1037
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->makeAutosize()V

    .line 1038
    return-void
.end method

.method private static translateTouch(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Landroid/view/View;)V
    .locals 8

    .prologue
    .line 712
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->actionFromTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I

    move-result v4

    .line 716
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v5

    .line 721
    const/4 v6, 0x0

    .line 722
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 725
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 727
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_0
.end method

.method private updateCursor()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 873
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorVisible:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hasSelection()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 874
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursor()V

    .line 875
    iput-boolean v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mIsHideCursor:Z

    .line 910
    :cond_1
    :goto_0
    return-void

    .line 878
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getCursorPosition()I

    move-result v0

    .line 879
    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 881
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 882
    if-eqz v1, :cond_1

    .line 883
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getTotalPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 884
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getTotalPaddingTop()I

    move-result v2

    int-to-float v2, v2

    .line 885
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1, v4}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 886
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    .line 887
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 892
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    .line 893
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 894
    cmpl-float v1, v0, v1

    if-gtz v1, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 897
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursor()V

    .line 898
    iput-boolean v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mIsHideCursor:Z

    goto :goto_0

    .line 901
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mIsHideCursor:Z

    if-eqz v0, :cond_5

    .line 902
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorColor:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showCursor(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    .line 903
    iput-boolean v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mIsHideCursor:Z

    goto/16 :goto_0

    .line 906
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorColor:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->moveCursor(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 511
    :cond_0
    return-void
.end method

.method protected createComponent(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->createEditText(Landroid/content/Context;)Landroid/widget/EditText;

    move-result-object v0

    return-object v0
.end method

.method createEditText(Landroid/content/Context;)Landroid/widget/EditText;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 378
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 380
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 382
    new-instance v2, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;Landroid/content/Context;)V

    .line 383
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 384
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 386
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setWidth(I)V

    .line 387
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHeight(I)V

    .line 389
    invoke-virtual {v2}, Landroid/widget/EditText;->getGravity()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mGravity:I

    .line 391
    invoke-virtual {v2}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mDefaultTextTypeface:Landroid/graphics/Typeface;

    .line 392
    invoke-virtual {v2}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mTextTypeface:Landroid/graphics/Typeface;

    .line 394
    const/16 v0, 0xac

    const/16 v1, 0xe0

    const/16 v3, 0xf4

    invoke-static {v0, v1, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHighlightColor(I)V

    .line 396
    invoke-virtual {v2}, Landroid/widget/EditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTextColor(I)V

    .line 398
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$1;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 413
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$2;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 425
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 440
    return-object v2
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCursorColor()I
    .locals 1

    .prologue
    .line 926
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorColor:I

    return v0
.end method

.method public getCursorPosition()I
    .locals 1

    .prologue
    .line 1236
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1237
    :goto_0
    return v0

    .line 1236
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    goto :goto_0
.end method

.method public getCursorSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    return-object v0
.end method

.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 496
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    return-object v0
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 1137
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mGravity:I

    return v0
.end method

.method public final getHighlightColor()I
    .locals 1

    .prologue
    .line 1459
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHighlightColor:I

    return v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1055
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getHintColor()I
    .locals 1

    .prologue
    .line 1086
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getCurrentHintTextColor()I

    move-result v0

    return v0
.end method

.method public getHintSize()F
    .locals 1

    .prologue
    .line 1332
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getTextSize()F

    move-result v0

    return v0
.end method

.method public getHintTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 1299
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mDefaultTextTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getImeActionId()I
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getImeActionId()I

    move-result v0

    return v0
.end method

.method public getImeActionLabel()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getImeActionLabel()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getImeOptions()I
    .locals 1

    .prologue
    .line 1613
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getImeOptions()I

    move-result v0

    return v0
.end method

.method public getInputType()I
    .locals 1

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getInputType()I

    move-result v0

    return v0
.end method

.method getInternalEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 1071
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public getTextTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mTextTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getTextWidth()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1191
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1204
    :cond_0
    :goto_0
    return v0

    .line 1194
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1197
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 1198
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_0

    .line 1204
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method

.method public hasSelection()Z
    .locals 1

    .prologue
    .line 1410
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1411
    const/4 v0, 0x0

    .line 1413
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->hasSelection()Z

    move-result v0

    goto :goto_0
.end method

.method hideCursorGlyph()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 847
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorGlyphVisible:Z

    if-nez v0, :cond_0

    .line 856
    :goto_0
    return-void

    .line 850
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_1

    .line 851
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 852
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->showCursorGlyph(Z)V

    .line 855
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorGlyphVisible:Z

    goto :goto_0
.end method

.method hideSelectionPointers()V
    .locals 2

    .prologue
    .line 948
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 949
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->showSelectionPointers(Z)V

    .line 951
    :cond_0
    return-void
.end method

.method public hideSip()V
    .locals 3

    .prologue
    .line 626
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 627
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    .line 628
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 629
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 631
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    .line 632
    return-void
.end method

.method horizontalScrollCheckOnTextChanged()V
    .locals 2

    .prologue
    .line 1169
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isTextFitsControlHorizontally()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mGravity:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setGravity(I)V

    .line 1177
    :goto_0
    return-void

    .line 1173
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHorizontalGravityMask:I

    .line 1174
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mVerticalGravityMask:I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setGravityInternal(I)V

    .line 1175
    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHorizontalGravityMask:I

    goto :goto_0
.end method

.method initSelectionPolicy()V
    .locals 2

    .prologue
    .line 257
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;-><init>(Landroid/content/Context;)V

    .line 258
    invoke-interface {v0, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setEditTextDelegate(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V

    .line 259
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSelectionPolicy(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;)V

    .line 260
    return-void
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 868
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 869
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    .line 870
    return-void
.end method

.method public isCursorVisible()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorVisible:Z

    return v0
.end method

.method isTextFitsControlHorizontally()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1180
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getTextWidth()F

    move-result v0

    .line 1181
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 1182
    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    move v0, v1

    .line 1183
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    .line 1184
    return v0

    :cond_0
    move v0, v2

    .line 1182
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1183
    goto :goto_1
.end method

.method public isTextSelectable()Z
    .locals 1

    .prologue
    .line 1439
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1440
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionEnabled()Z

    move-result v0

    .line 1442
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method makeAutosize()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1040
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mAutoSizeEnabled:Z

    if-nez v0, :cond_0

    .line 1050
    :goto_0
    return-void

    .line 1043
    :cond_0
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1044
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1045
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 1046
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 1047
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getTextWidth()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 1048
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1049
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mAutoSizeEnabled:Z

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 547
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getScrollX()I

    move-result v0

    .line 685
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getScrollY()I

    move-result v1

    .line 687
    int-to-float v0, v0

    neg-float v0, v0

    int-to-float v1, v1

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 688
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->draw(Landroid/graphics/Canvas;)V

    .line 689
    return-void
.end method

.method public onFocusChanged()V
    .locals 1

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 580
    if-nez v0, :cond_0

    .line 591
    :goto_0
    return-void

    .line 583
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isInFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->startSipInput()V

    goto :goto_0

    .line 587
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->stopSipInput()V

    .line 588
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    .line 589
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSelectionPointers()V

    goto :goto_0
.end method

.method public onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 804
    if-nez p1, :cond_1

    .line 814
    :cond_0
    :goto_0
    return v0

    .line 807
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v1, v2, :cond_0

    .line 810
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isInFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 813
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setFocus()Z

    .line 814
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 736
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    .line 742
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 743
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY(I)F

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isPointOnButton(FF)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 744
    add-int/lit8 v4, v4, 0x1

    .line 745
    sget-object v5, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$3;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction(I)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 742
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 747
    :pswitch_0
    add-int/lit8 v3, v3, 0x1

    .line 748
    goto :goto_1

    .line 751
    :pswitch_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 758
    :cond_1
    if-lez v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 759
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setPressed(Z)Z

    .line 760
    :cond_2
    if-nez v4, :cond_5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 761
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setPressed(Z)Z

    .line 767
    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 768
    if-nez v0, :cond_6

    .line 796
    :cond_4
    :goto_3
    return v7

    .line 762
    :cond_5
    sub-int v0, v4, v2

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 763
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setPressed(Z)Z

    goto :goto_2

    .line 773
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v1, :cond_8

    .line 775
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eq v0, p0, :cond_7

    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v0, :cond_7

    .line 776
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->clearFocus()V

    .line 778
    :cond_7
    sput-object p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mFocusedEditTextBox:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    .line 779
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setFocus()Z

    .line 780
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 782
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSip()V

    .line 784
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->translateTouch(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Landroid/view/View;)V

    .line 785
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->customForwardTouchToDetectors(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 786
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 787
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v1, :cond_4

    .line 788
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 789
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hasSelection()Z

    move-result v1

    if-nez v1, :cond_9

    if-eqz v0, :cond_9

    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v0, v7, :cond_a

    .line 790
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    goto :goto_3

    .line 793
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showCursorGlyph()V

    goto :goto_3

    .line 745
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 520
    :cond_0
    return-void
.end method

.method public selectAll()V
    .locals 1

    .prologue
    .line 1093
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1101
    :goto_0
    return-void

    .line 1096
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 1097
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1098
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSelectionPointers()V

    .line 1099
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    .line 1100
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    goto :goto_0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 998
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 999
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1000
    return-void
.end method

.method public setCursorColor(I)V
    .locals 0

    .prologue
    .line 917
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorColor:I

    .line 918
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    .line 919
    return-void
.end method

.method public setCursorPointerDrawable(I)V
    .locals 1

    .prologue
    .line 1553
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setCursorPointerDrawable(I)V

    .line 1556
    :cond_0
    return-void
.end method

.method public setCursorPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1563
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setCursorPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1565
    :cond_0
    return-void
.end method

.method public setCursorPosition(I)V
    .locals 1

    .prologue
    .line 1228
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorPosition(IZ)V

    .line 1229
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 0

    .prologue
    .line 358
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorVisible:Z

    .line 359
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    .line 360
    return-void
.end method

.method public setCursorWidth(F)V
    .locals 1

    .prologue
    .line 943
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    .line 944
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    .line 945
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 1420
    if-nez p1, :cond_0

    .line 1421
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    .line 1422
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSelectionPointers()V

    .line 1424
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setEnabled(Z)V

    .line 1425
    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    .prologue
    .line 488
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 489
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 490
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 491
    return-void
.end method

.method public setGravity(I)V
    .locals 0

    .prologue
    .line 1128
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setGravityInternal(I)V

    .line 1129
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1130
    return-void
.end method

.method setGravityInternal(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1142
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    if-nez v0, :cond_0

    .line 1166
    :goto_0
    return-void

    .line 1145
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mGravity:I

    .line 1146
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSelectionPointers()V

    .line 1147
    and-int/lit8 v0, p1, 0x7

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHorizontalGravityMask:I

    .line 1148
    and-int/lit8 v0, p1, 0x70

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mVerticalGravityMask:I

    .line 1149
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHorizontalGravityMask:I

    if-eqz v0, :cond_2

    .line 1151
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isTextFitsControlHorizontally()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1152
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setGravity(I)V

    .line 1161
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {v0, v2, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->measure(II)V

    .line 1162
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->layout(IIII)V

    .line 1163
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSelectionPointers()V

    .line 1164
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursorGlyph()V

    .line 1165
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->updateCursor()V

    goto :goto_0

    .line 1155
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mVerticalGravityMask:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setGravity(I)V

    goto :goto_1

    .line 1159
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setGravity(I)V

    goto :goto_1
.end method

.method public setHighlightColor(I)V
    .locals 1

    .prologue
    .line 1449
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mHighlightColor:I

    .line 1450
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHighlightColor(I)V

    .line 1451
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1452
    return-void
.end method

.method public setHint(I)V
    .locals 1

    .prologue
    .line 1029
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    .line 1030
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->textSettedUp()V

    .line 1031
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1021
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1022
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->textSettedUp()V

    .line 1023
    return-void
.end method

.method public setHintColor(I)V
    .locals 1

    .prologue
    .line 1078
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1079
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1080
    return-void
.end method

.method public setHintSize(F)V
    .locals 2

    .prologue
    .line 1340
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 1341
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The size of text cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1344
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1345
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1346
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->makeAutosize()V

    .line 1347
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1348
    return-void
.end method

.method public setHintTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1272
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setHintTypeface(Landroid/graphics/Typeface;I)V

    .line 1273
    return-void
.end method

.method public setHintTypeface(Landroid/graphics/Typeface;I)V
    .locals 2

    .prologue
    .line 1288
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mDefaultTextTypeface:Landroid/graphics/Typeface;

    .line 1289
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mDefaultTextTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1290
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1291
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1292
    return-void
.end method

.method public setImeActionLabel(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 470
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 471
    return-void
.end method

.method public setImeOptions(I)V
    .locals 1

    .prologue
    .line 1605
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1606
    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setInputType(I)V

    .line 454
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 455
    return-void
.end method

.method public setLeftSelectionPointerDrawable(I)V
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1528
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setLeftSelectionPointerDrawable(I)V

    .line 1530
    :cond_0
    return-void
.end method

.method public setLeftSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1571
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1572
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setLeftSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1574
    :cond_0
    return-void
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 448
    return-void
.end method

.method public setPadding(IIII)V
    .locals 1

    .prologue
    .line 1111
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 1112
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->makeAutosize()V

    .line 1113
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1114
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1115
    return-void
.end method

.method public setPointersContainer(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 1

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1518
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setPointersContainer(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1520
    :cond_0
    return-void
.end method

.method public setPointersSize(FF)V
    .locals 1

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setPointersSize(FF)V

    .line 1596
    :cond_0
    return-void
.end method

.method public setRightSelectionPointerDrawable(I)V
    .locals 1

    .prologue
    .line 1536
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1537
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setRightSelectionPointerDrawable(I)V

    .line 1539
    :cond_0
    return-void
.end method

.method public setRightSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1580
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1581
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setRightSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1583
    :cond_0
    return-void
.end method

.method public setSelection(II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1470
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v1, :cond_1

    .line 1471
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1495
    :cond_0
    :goto_0
    return-void

    .line 1475
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1477
    if-ge p2, p1, :cond_5

    .line 1481
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1482
    if-le p1, v1, :cond_4

    .line 1485
    :goto_2
    if-gez p2, :cond_2

    move p2, v0

    .line 1488
    :cond_2
    if-gez v1, :cond_3

    .line 1491
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Landroid/widget/EditText;->setSelection(II)V

    .line 1492
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSelectionPointers()V

    .line 1493
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideCursorGlyph()V

    .line 1494
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v1, p1

    goto :goto_2

    :cond_5
    move v2, p2

    move p2, p1

    move p1, v2

    goto :goto_1
.end method

.method public setSelectionPointersInversionEnabled(Z)V
    .locals 1

    .prologue
    .line 1505
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1506
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setPointersInversionEnabled(Z)V

    .line 1508
    :cond_0
    return-void
.end method

.method setSelectionPolicy(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    .line 105
    return-void
.end method

.method public final setSize(FF)V
    .locals 4

    .prologue
    .line 982
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setSize(FF)V

    .line 983
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_1

    .line 992
    :cond_0
    :goto_0
    return-void

    .line 986
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 987
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->invalidate()V

    .line 988
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCustomLayout:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomLayout;->update()V

    .line 989
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 990
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mAutoSizeEnabled:Z

    .line 991
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->textSettedUp()V

    goto :goto_0
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 972
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSize(FF)V

    .line 973
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 1014
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(I)V

    .line 1015
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->textSettedUp()V

    .line 1016
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1006
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1007
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->textSettedUp()V

    .line 1008
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 1062
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1063
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1064
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1065
    return-void
.end method

.method public setTextIsSelectable(Z)V
    .locals 1

    .prologue
    .line 1430
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 1431
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->setSelectionEnabled(Z)V

    .line 1433
    :cond_0
    return-void
.end method

.method public setTextSize(IF)V
    .locals 2

    .prologue
    .line 1317
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    .line 1318
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The size of text cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1321
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1322
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1323
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->makeAutosize()V

    .line 1324
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1325
    return-void
.end method

.method public setTextStrike(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1383
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getCursorPosition()I

    move-result v2

    .line 1384
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1385
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->saveSelectionState()V

    .line 1387
    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1388
    if-eqz p1, :cond_3

    .line 1389
    new-instance v0, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v0}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1397
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1398
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1399
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorPosition(I)V

    .line 1400
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1401
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->restoreSelectionState()V

    .line 1403
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1404
    return-void

    .line 1392
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const-class v4, Landroid/text/style/StrikethroughSpan;

    invoke-virtual {v3, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/StrikethroughSpan;

    .line 1393
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 1394
    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 1393
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setTextUnderline(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1355
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getCursorPosition()I

    move-result v2

    .line 1356
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1357
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->saveSelectionState()V

    .line 1359
    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1360
    if-eqz p1, :cond_3

    .line 1361
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/16 v5, 0x12

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1369
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1370
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1371
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorPosition(I)V

    .line 1372
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->isSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1373
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->restoreSelectionState()V

    .line 1375
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1376
    return-void

    .line 1364
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const-class v4, Landroid/text/style/UnderlineSpan;

    invoke-virtual {v3, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/UnderlineSpan;

    .line 1365
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 1366
    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 1365
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setTypeFace(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1244
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1245
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1246
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1247
    return-void
.end method

.method public setTypeFace(Landroid/graphics/Typeface;I)V
    .locals 1

    .prologue
    .line 1262
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1263
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onTextAppearenceChanged()V

    .line 1264
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 1265
    return-void
.end method

.method setupDefaultTextColors(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 129
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 130
    const v1, 0x1010036

    aput v1, v0, v3

    .line 131
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->androidComponent:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 136
    return-void
.end method

.method setupPointerSize(I)V
    .locals 3

    .prologue
    .line 187
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 188
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 189
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 190
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 191
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 192
    int-to-float v0, v0

    int-to-float v2, v2

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setPointersSize(FF)V

    .line 193
    if-eqz v1, :cond_0

    .line 194
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 196
    :cond_0
    return-void
.end method

.method showCursorGlyph()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 834
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 837
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 839
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->showCursorGlyph(Z)V

    .line 840
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorGlyphVisible:Z

    .line 842
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->showSelectionPointers(Z)V

    goto :goto_0
.end method

.method showSelectionPointers()V
    .locals 2

    .prologue
    .line 954
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hasSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 956
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->showSelectionPointers(Z)V

    .line 959
    :cond_0
    return-void
.end method

.method showSelectionPointersForced()V
    .locals 2

    .prologue
    .line 961
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mSelectionPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;->showSelectionPointers(Z)V

    .line 964
    :cond_0
    return-void
.end method

.method public showSip()V
    .locals 3

    .prologue
    .line 606
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 607
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 608
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 609
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 610
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1, v0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 613
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 615
    :cond_0
    return-void
.end method

.method updateCursorGlyph()V
    .locals 1

    .prologue
    .line 859
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->mCursorGlyphVisible:Z

    if-eqz v0, :cond_0

    .line 860
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showCursorGlyph()V

    .line 862
    :cond_0
    return-void
.end method

.method public updateSip()V
    .locals 1

    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isInFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSip()V

    .line 656
    :goto_0
    return-void

    .line 654
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hideSip()V

    goto :goto_0
.end method
