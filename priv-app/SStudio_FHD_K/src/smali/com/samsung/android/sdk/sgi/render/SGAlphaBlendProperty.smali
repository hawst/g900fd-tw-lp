.class public Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGAlphaBlendProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGAlphaBlendProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;-><init>(JZ)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->init()V

    .line 42
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_init(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)V

    .line 46
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;
    .locals 7

    .prologue
    .line 123
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGAlphaBlendProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getBlendColor()Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getBlendColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v0
.end method

.method public getBlendEquationAlpha()Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;
    .locals 3

    .prologue
    .line 81
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getBlendEquationAlpha(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getBlendEquationColor()Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;
    .locals 3

    .prologue
    .line 77
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getBlendEquationColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDestinationFactor()Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 3

    .prologue
    .line 57
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getDestinationFactor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDestinationFactorAlpha()Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 3

    .prologue
    .line 73
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getDestinationFactorAlpha(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDestinationFactorColor()Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 3

    .prologue
    .line 65
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getDestinationFactorColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getSourceFactor()Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 3

    .prologue
    .line 53
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getSourceFactor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getSourceFactorAlpha()Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 3

    .prologue
    .line 69
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getSourceFactorAlpha(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getSourceFactorColor()Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 3

    .prologue
    .line 61
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_getSourceFactorColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isAlphaBlendingEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)Z

    move-result v0

    return v0
.end method

.method public setAlphaBlendingEnabled(Z)V
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;Z)V

    .line 120
    return-void
.end method

.method public setBlendColor(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_setBlendColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 116
    return-void
.end method

.method public setBlendEquation(Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;)V
    .locals 4

    .prologue
    .line 107
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 108
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 110
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->ordinal()I

    move-result v3

    invoke-static {v0, v1, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_setBlendEquation(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;II)V

    .line 112
    return-void
.end method

.method public setFactors(Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;)V
    .locals 4

    .prologue
    .line 89
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 90
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 92
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ordinal()I

    move-result v3

    invoke-static {v0, v1, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_setFactors(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;II)V

    .line 94
    return-void
.end method

.method public setFactorsSeparate(Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;)V
    .locals 7

    .prologue
    .line 97
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 98
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 99
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 100
    :cond_2
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 102
    :cond_3
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ordinal()I

    move-result v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ordinal()I

    move-result v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ordinal()I

    move-result v5

    invoke-virtual {p4}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ordinal()I

    move-result v6

    move-object v2, p0

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGAlphaBlendProperty_setFactorsSeparate(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;IIII)V

    .line 104
    return-void
.end method
