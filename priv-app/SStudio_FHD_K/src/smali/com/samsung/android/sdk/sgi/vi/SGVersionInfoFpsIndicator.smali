.class public Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;
.super Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;
.source "SGVersionInfoFpsIndicator.java"


# instance fields
.field isDrawed:Z

.field mPaint:Landroid/graphics/Paint;

.field mString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 47
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(Landroid/graphics/RectF;F)V

    .line 35
    iput-boolean v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->isDrawed:Z

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 55
    :goto_0
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation()Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;

    move-result-object v0

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMajor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMinor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mPatch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mBuild:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v0

    .line 60
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/high16 v2, 0x41a00000    # 20.0f

    add-float/2addr v0, v2

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-direct {v1, v0, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 61
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v3, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 62
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_0
.end method

.method public constructor <init>(III)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 75
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(Landroid/graphics/RectF;F)V

    .line 35
    iput-boolean v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->isDrawed:Z

    .line 76
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 83
    :goto_0
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation()Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;

    move-result-object v0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMajor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMinor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mPatch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mBuild:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v0

    .line 86
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/high16 v2, 0x41a00000    # 20.0f

    add-float/2addr v0, v2

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-direct {v1, v0, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 87
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 88
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 94
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->isDrawed:Z

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->isDrawed:Z

    .line 96
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 98
    const/16 v1, 0x96

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 99
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->descent()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    rsub-int/lit8 v2, v2, 0xf

    .line 100
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mString:Ljava/lang/String;

    int-to-float v1, v1

    int-to-float v2, v2

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInfoFpsIndicator;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
