.class public final enum Lcom/samsung/android/sdk/sgi/render/SGShaderType;
.super Ljava/lang/Enum;
.source "SGShaderType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGShaderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGShaderType;

.field public static final enum FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

.field public static final enum VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v1, "VERTEX"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v1, "FRAGMENT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGShaderType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGShaderType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGShaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    return-object v0
.end method
