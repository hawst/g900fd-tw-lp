.class public Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGShaderProperty.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGShaderProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getDataType()Lcom/samsung/android/sdk/sgi/render/SGShaderDataType;
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGShaderDataType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGShaderDataType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGShaderProperty_getDataType(JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getShaderType()Lcom/samsung/android/sdk/sgi/render/SGShaderType;
    .locals 3

    .prologue
    .line 44
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGShaderProperty_getShaderType(JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method
