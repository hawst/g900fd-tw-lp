.class public final enum Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;
.super Ljava/lang/Enum;
.source "SGBlendEquation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

.field public static final enum ADD:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

.field public static final enum MAX:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

.field public static final enum MIN:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

.field public static final enum REVERSE_SUBTRACT:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

.field public static final enum SUBTRACT:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->ADD:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    const-string v1, "SUBTRACT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->SUBTRACT:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    const-string v1, "REVERSE_SUBTRACT"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->REVERSE_SUBTRACT:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    const-string v1, "MIN"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->MIN:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    const-string v1, "MAX"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->MAX:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->ADD:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->SUBTRACT:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->REVERSE_SUBTRACT:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->MIN:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->MAX:Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendEquation;

    return-object v0
.end method
