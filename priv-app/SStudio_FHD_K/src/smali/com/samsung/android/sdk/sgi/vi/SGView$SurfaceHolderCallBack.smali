.class Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;
.super Ljava/lang/Object;
.source "SGView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/vi/SGView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SurfaceHolderCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGView;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->access$100(Lcom/samsung/android/sdk/sgi/vi/SGView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceChanged(II)V

    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->access$100(Lcom/samsung/android/sdk/sgi/vi/SGView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setResumed(Z)V

    .line 212
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 205
    const/4 v0, -0x3

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 206
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->access$100(Lcom/samsung/android/sdk/sgi/vi/SGView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceAvailable(Landroid/view/Surface;)V

    .line 208
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->access$100(Lcom/samsung/android/sdk/sgi/vi/SGView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setSuspended(Z)V

    .line 202
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGView$SurfaceHolderCallBack;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->access$100(Lcom/samsung/android/sdk/sgi/vi/SGView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceDestroyed()V

    .line 203
    return-void
.end method
