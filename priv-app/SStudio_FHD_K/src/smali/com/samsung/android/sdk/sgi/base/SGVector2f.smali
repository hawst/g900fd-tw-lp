.class public Lcom/samsung/android/sdk/sgi/base/SGVector2f;
.super Ljava/lang/Object;
.source "SGVector2f.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector2f__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector2f__SWIG_2(FF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    .line 55
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 3

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector2f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_add(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 111
    return-void
.end method

.method public divide(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_divide(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 123
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGVector2f(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getDistance(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)F
    .locals 6

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F

    move-result v0

    return v0
.end method

.method public getDotProduct(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)F
    .locals 6

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F

    move-result v0

    return v0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F

    move-result v0

    return v0
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_getX(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_getY(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F

    move-result v0

    return v0
.end method

.method public interpolate(Lcom/samsung/android/sdk/sgi/base/SGVector2f;F)V
    .locals 7

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_interpolate(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V

    .line 103
    return-void
.end method

.method public inverse()V
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_inverse(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 95
    return-void
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGVector2f;F)Z
    .locals 7

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)Z

    move-result v0

    return v0
.end method

.method public multiply(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 119
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 91
    return-void
.end method

.method public scale(F)V
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_scale(JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V

    .line 99
    return-void
.end method

.method public set(FF)V
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_set(JLcom/samsung/android/sdk/sgi/base/SGVector2f;FF)V

    .line 59
    return-void
.end method

.method public setX(F)V
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_setX(JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V

    .line 67
    return-void
.end method

.method public setY(F)V
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_setY(JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V

    .line 75
    return-void
.end method

.method public subtract(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2f_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 115
    return-void
.end method
