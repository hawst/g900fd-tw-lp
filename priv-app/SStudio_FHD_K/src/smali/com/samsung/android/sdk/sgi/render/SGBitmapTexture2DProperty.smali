.class public Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;
.source "SGBitmapTexture2DProperty.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGTextureProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V
    .locals 3

    .prologue
    .line 102
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->SwigConstructSGBitmapTexture2DProperty(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(JZ)V

    .line 103
    return-void
.end method

.method private static SwigConstructSGBitmapTexture2DProperty(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)J
    .locals 2

    .prologue
    .line 96
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 97
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->ordinal()I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGBitmapTexture2DProperty(II)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    .locals 7

    .prologue
    .line 92
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGBitmapTexture2DProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getDataFormat()Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;
    .locals 3

    .prologue
    .line 44
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getDataFormat(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDataType()Lcom/samsung/android/sdk/sgi/render/SGTextureDataType;
    .locals 3

    .prologue
    .line 48
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getDataType(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getHeight(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v0

    return v0
.end method

.method public getInternalFormat()Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getInternalFormat(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getMagFilter()Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
    .locals 3

    .prologue
    .line 56
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getMagFilter(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getMinFilter()Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
    .locals 3

    .prologue
    .line 52
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getMinFilter(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getWidth(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v0

    return v0
.end method

.method public getWrapS()Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;
    .locals 3

    .prologue
    .line 60
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getWrapS(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWrapT()Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;
    .locals 3

    .prologue
    .line 64
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_getWrapT(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isGenerateMipmapsEnabled()Z
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_isGenerateMipmapsEnabled(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)Z

    move-result v0

    return v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_setBitmap(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;Landroid/graphics/Bitmap;)V

    .line 108
    return-void
.end method

.method public setGenerateMipmapsEnabled(Z)V
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_setGenerateMipmapsEnabled(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;Z)V

    .line 89
    return-void
.end method

.method public setWrapType(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V
    .locals 4

    .prologue
    .line 80
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 81
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 83
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->ordinal()I

    move-result v3

    invoke-static {v0, v1, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBitmapTexture2DProperty_setWrapType(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;II)V

    .line 85
    return-void
.end method
