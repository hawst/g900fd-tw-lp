.class final Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;
.super Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;
.source "SGPropertyScreenshotListenerHolder.java"


# instance fields
.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;

.field mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 32
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;

    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method


# virtual methods
.method public detachListener()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public onCompleted(J)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 45
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGProperty;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;->onCompleted(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 52
    const-string v1, "SGPropertyScreenshotListener::onCompleted error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
