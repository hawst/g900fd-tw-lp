.class public final enum Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;
.super Ljava/lang/Enum;
.source "SGRotationOrder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

.field public static final enum XYZ:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

.field public static final enum XZY:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

.field public static final enum YXZ:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

.field public static final enum YZX:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

.field public static final enum ZXY:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

.field public static final enum ZYX:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    const-string v1, "XYZ"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->XYZ:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    const-string v1, "XZY"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->XZY:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    const-string v1, "YXZ"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->YXZ:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    const-string v1, "YZX"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->YZX:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    const-string v1, "ZXY"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ZXY:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    const-string v1, "ZYX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ZYX:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    .line 18
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    sget-object v1, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->XYZ:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->XZY:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->YXZ:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->YZX:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ZXY:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ZYX:Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->$VALUES:[Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->$VALUES:[Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;

    return-object v0
.end method
