.class Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;
.source "SGEditTextSelectionPointerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer$1;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "EditTextBox"

.field private static final mDefaultTouchTreshold:F = 5.0f


# instance fields
.field mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

.field mBitmap:Landroid/graphics/Bitmap;

.field mBitmapRect:Landroid/graphics/Rect;

.field mContext:Landroid/content/Context;

.field mDrawable:Landroid/graphics/drawable/Drawable;

.field mEndPositionX:F

.field private mIndex:I

.field private mLocalCoordinates:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field mPaint:Landroid/graphics/Paint;

.field mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

.field mSizeRect:Landroid/graphics/Rect;

.field mStartPositionX:F

.field private mTextPosition:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/content/Context;Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 719
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 699
    iput v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mIndex:I

    .line 700
    iput v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mTextPosition:I

    .line 701
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    .line 702
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    .line 703
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 704
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    .line 705
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mContext:Landroid/content/Context;

    .line 706
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPaint:Landroid/graphics/Paint;

    .line 707
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 709
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    .line 711
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmapRect:Landroid/graphics/Rect;

    .line 712
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mSizeRect:Landroid/graphics/Rect;

    .line 713
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mLocalCoordinates:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 720
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mContext:Landroid/content/Context;

    .line 721
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->initBackground(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 722
    iput-object p3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    .line 723
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setFocusable(Z)V

    .line 724
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mSizeRect:Landroid/graphics/Rect;

    .line 725
    return-void
.end method

.method private initBackground(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 834
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 835
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 836
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 837
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 838
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 839
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 858
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mIndex:I

    return v0
.end method

.method getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mLocalCoordinates:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    return-object v0
.end method

.method getOnTouchStartPosition()F
    .locals 1

    .prologue
    .line 831
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    return v0
.end method

.method public getTextPosition()I
    .locals 1

    .prologue
    .line 862
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mTextPosition:I

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 780
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmapRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mSizeRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 783
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 784
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 786
    :cond_1
    return-void
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 790
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    if-nez v2, :cond_0

    .line 791
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGDefaultSelectionPointer: null listener!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 793
    :cond_0
    if-nez p1, :cond_1

    .line 825
    :goto_0
    return v0

    .line 796
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawX()F

    move-result v2

    .line 797
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawY()F

    move-result v3

    .line 798
    sget-object v4, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :cond_2
    :goto_1
    move v0, v1

    .line 825
    goto :goto_0

    .line 799
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v4

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;->onPointerDown(FFI)V

    .line 800
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    .line 801
    const-string v0, "EditTextBox"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DOWN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 804
    :pswitch_1
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    .line 805
    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    iget v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40a00000    # 5.0f

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_2

    .line 809
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    .line 810
    sget-object v4, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    cmpl-float v0, v4, v0

    if-nez v0, :cond_3

    .line 811
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v4

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;->onPointerDown(FFI)V

    .line 815
    :cond_3
    const-string v0, "EditTextBox"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MOVE: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " prev: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " delta: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    iget v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    sub-float/2addr v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ptr: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    iget v5, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    sub-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v5

    invoke-interface {v0, v2, v4, v3, v5}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;->onPointerRequestPosition(FFFI)V

    goto/16 :goto_1

    .line 818
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v4

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;->onPointerUp(FFI)V

    .line 819
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mEndPositionX:F

    .line 820
    const-string v0, "EditTextBox"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UP: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 798
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 743
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 744
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 745
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mSizeRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 746
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 747
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 749
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    .line 751
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->invalidate()V

    .line 752
    return-void
.end method

.method public setDrawableID(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 755
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 757
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    .line 758
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    .line 759
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 760
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBackgroundLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 761
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 762
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPaint:Landroid/graphics/Paint;

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 765
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 766
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mBitmapRect:Landroid/graphics/Rect;

    .line 768
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->invalidate()V

    .line 769
    return-void
.end method

.method public setIndex(I)V
    .locals 0

    .prologue
    .line 854
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mIndex:I

    .line 855
    return-void
.end method

.method setLocalCoordinates(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 850
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mLocalCoordinates:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 851
    return-void
.end method

.method setOnTouchStartPosition(F)V
    .locals 0

    .prologue
    .line 828
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mStartPositionX:F

    .line 829
    return-void
.end method

.method public setPosition(FF)V
    .locals 1

    .prologue
    .line 842
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 843
    return-void
.end method

.method public setSize(FF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 729
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->setSize(FF)V

    .line 730
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mSizeRect:Landroid/graphics/Rect;

    .line 731
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mSizeRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 734
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->invalidate()V

    .line 735
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 739
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setSize(FF)V

    .line 740
    return-void
.end method

.method public setTextPosition(I)V
    .locals 0

    .prologue
    .line 866
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mTextPosition:I

    .line 867
    return-void
.end method
