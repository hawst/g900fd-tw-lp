.class public final enum Lcom/samsung/android/sdk/sgi/render/SGFrontType;
.super Ljava/lang/Enum;
.source "SGFrontType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGFrontType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGFrontType;

.field public static final enum CCW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

.field public static final enum CW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    const-string v1, "CCW"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFrontType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->CCW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    const-string v1, "CW"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGFrontType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->CW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->CCW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->CW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGFrontType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGFrontType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGFrontType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    return-object v0
.end method
