.class public final Lcom/samsung/android/sdk/sgi/render/SGCompressedTextureFactory;
.super Ljava/lang/Object;
.source "SGCompressedTextureFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createTexture(Landroid/content/res/Resources;I)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v1, 0x0

    .line 56
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/render/SGCompressedTextureFactory;->createTexture(Ljava/nio/channels/FileChannel;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_1
    throw v0
.end method

.method public static createTexture(Ljava/io/FileDescriptor;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v2, 0x0

    .line 38
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/render/SGCompressedTextureFactory;->createTexture(Ljava/nio/channels/FileChannel;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 42
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public static createTexture(Ljava/nio/channels/FileChannel;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCompressedTextureFactory_createTexture(Ljava/nio/channels/FileChannel;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(JZ)V

    return-object v0
.end method

.method private delete()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method
