.class Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;
.super Ljava/lang/Object;
.source "SGEditTextSelectionPointerController.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setPointersSize(FFLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 908
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 909
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    if-eqz v2, :cond_0

    .line 910
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v2, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    goto :goto_0

    .line 913
    :cond_1
    return-void
.end method

.method public showCursorGlyph(ZLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 898
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 905
    :cond_0
    :goto_0
    return-void

    .line 901
    :cond_1
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 902
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    if-eqz v1, :cond_0

    .line 903
    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v0

    invoke-interface {v1, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;->onPointerShow(ZI)V

    goto :goto_0
.end method

.method public showSelectionPointers(ZLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 883
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 884
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    if-eqz v2, :cond_0

    .line 888
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 889
    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->mPointerListener:Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getIndex()I

    move-result v0

    invoke-interface {v2, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;->onPointerShow(ZI)V

    goto :goto_0

    .line 893
    :cond_1
    return-void
.end method
