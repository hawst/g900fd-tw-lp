.class public Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
.super Lcom/samsung/android/sdk/sgi/render/SGBuffer;
.source "SGVertexBuffer.java"


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 48
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGVertexBuffer__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(JZ)V

    .line 49
    return-void
.end method

.method public constructor <init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V
    .locals 3

    .prologue
    .line 83
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->SwigConstructSGVertexBuffer(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(JZ)V

    .line 84
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private static SwigConstructSGVertexBuffer(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)J
    .locals 2

    .prologue
    .line 77
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 78
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 79
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->ordinal()I

    move-result v1

    invoke-static {p0, v0, v1, p3}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGVertexBuffer__SWIG_1(IIII)J

    move-result-wide v0

    return-wide v0
.end method

.method private init(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V
    .locals 7

    .prologue
    .line 52
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->ordinal()I

    move-result v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->ordinal()I

    move-result v5

    move-object v2, p0

    move v3, p1

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVertexBuffer_init(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;IIII)V

    .line 57
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .locals 7

    .prologue
    .line 73
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGBuffer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVertexBuffer___assign__(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGVertexBuffer(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getComponentsPerElement()I
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVertexBuffer_getComponentsPerElement(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)I

    move-result v0

    return v0
.end method

.method public getFloatBuffer()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getVertexCount()I
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVertexBuffer_getVertexCount(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)I

    move-result v0

    return v0
.end method

.method public setVertexCount(I)V
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVertexBuffer_setVertexCount(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;I)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->buffer:Ljava/nio/ByteBuffer;

    .line 66
    return-void
.end method
