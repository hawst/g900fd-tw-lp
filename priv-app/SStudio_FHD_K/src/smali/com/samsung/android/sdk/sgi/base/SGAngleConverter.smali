.class public final Lcom/samsung/android/sdk/sgi/base/SGAngleConverter;
.super Ljava/lang/Object;
.source "SGAngleConverter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deg2Rad(D)D
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGAngleConverter_deg2Rad(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private delete()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public static rad2Deg(D)D
    .locals 2

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGAngleConverter_rad2Deg(D)D

    move-result-wide v0

    return-wide v0
.end method
