.class final Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;
.super Ljava/lang/Object;
.source "SGScrollableArea.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SGAnimationEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)V
    .locals 0

    .prologue
    .line 915
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$1;)V
    .locals 0

    .prologue
    .line 915
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)V

    return-void
.end method


# virtual methods
.method public onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 931
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    # getter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPosAnimationId:I
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->access$200(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    new-instance v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$1;)V

    iput-object v1, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    .line 934
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->getCurrentValue(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V

    .line 935
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iput-object v3, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    .line 936
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->access$102(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Z)Z

    .line 937
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 938
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V

    goto :goto_0

    .line 940
    :cond_0
    return-void
.end method

.method public onDiscarded(I)V
    .locals 0

    .prologue
    .line 957
    return-void
.end method

.method public onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 944
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    # getter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPosAnimationId:I
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->access$200(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 946
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    new-instance v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$1;)V

    iput-object v1, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    .line 947
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->getCurrentValue(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V

    .line 948
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iput-object v3, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mPositionReceiver:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;

    .line 949
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->access$102(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Z)Z

    .line 950
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 951
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V

    goto :goto_0

    .line 953
    :cond_0
    return-void
.end method

.method public onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 2

    .prologue
    .line 925
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 926
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V

    goto :goto_0

    .line 927
    :cond_0
    return-void
.end method

.method public onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 2

    .prologue
    .line 918
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->access$102(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Z)Z

    .line 919
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$SGAnimationEventListener;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mSGAnimationEventListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 920
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;->onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V

    goto :goto_0

    .line 921
    :cond_0
    return-void
.end method
