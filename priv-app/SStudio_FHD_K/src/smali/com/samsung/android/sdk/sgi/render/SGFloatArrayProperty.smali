.class public Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGFloatArrayProperty.java"


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGFloatArrayProperty__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 66
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGFloatArrayProperty__SWIG_1(I)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;-><init>(JZ)V

    .line 67
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private getBuffer()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatArrayProperty_getBuffer(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private init(I)V
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatArrayProperty_init(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;I)V

    .line 55
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;)Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;
    .locals 7

    .prologue
    .line 62
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatArrayProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGFloatArrayProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getFloatBuffer()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->getBuffer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 46
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    return-object v0
.end method

.method public setSize(I)V
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatArrayProperty_setSize(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;I)V

    .line 59
    return-void
.end method
