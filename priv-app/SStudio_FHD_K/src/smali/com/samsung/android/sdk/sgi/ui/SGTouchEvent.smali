.class public Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
.super Ljava/lang/Object;
.source "SGTouchEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;,
        Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;,
        Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;
    }
.end annotation


# static fields
.field private static lastMotionEvent:Landroid/view/MotionEvent;


# instance fields
.field private mCachedAction:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;

.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->lastMotionEvent:Landroid/view/MotionEvent;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 173
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGTouchEvent__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    .line 174
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->mCachedAction:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;

    .line 28
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCMemOwn:Z

    .line 29
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V
    .locals 3

    .prologue
    .line 177
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGTouchEvent__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    .line 178
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Z)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V

    .line 70
    if-eqz p2, :cond_0

    .line 71
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->cacheAction(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 73
    :cond_0
    return-void
.end method

.method public static final createMotionEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Landroid/view/MotionEvent;
    .locals 2

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->lastMotionEvent:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    .line 167
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->lastMotionEvent:Landroid/view/MotionEvent;

    invoke-static {v0}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 169
    :cond_0
    return-object v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J
    .locals 2

    .prologue
    .line 33
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    goto :goto_0
.end method

.method private getDefaultAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;
    .locals 3

    .prologue
    .line 189
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getDefaultAction(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private setA(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V
    .locals 3

    .prologue
    .line 197
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 199
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, p1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_setA(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)V

    .line 201
    return-void
.end method


# virtual methods
.method public addPointer(IFFFFLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;FLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;)V
    .locals 11

    .prologue
    .line 181
    if-nez p6, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 182
    :cond_0
    if-nez p8, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 184
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-virtual/range {p6 .. p6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v8

    invoke-virtual/range {p8 .. p8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->ordinal()I

    move-result v10

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move/from16 v9, p7

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_addPointer(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;IFFFFIFI)V

    .line 186
    return-void
.end method

.method public appendHistoryEntry(Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 276
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_appendHistoryEntry(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Ljava/util/Date;)V

    .line 277
    return-void
.end method

.method public appendPointerToHistory(FFF)V
    .locals 6

    .prologue
    .line 280
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_appendPointerToHistory(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;FFF)V

    .line 281
    return-void
.end method

.method public cacheAction(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->mCachedAction:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;

    .line 104
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 38
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCMemOwn:Z

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGTouchEvent(J)V

    .line 42
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    .line 44
    :cond_1
    return-void
.end method

.method public getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->mCachedAction:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->mCachedAction:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$CachedAction;->get()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getDefaultAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    goto :goto_0
.end method

.method public getAction(I)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;
    .locals 3

    .prologue
    .line 193
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getAction(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDownTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 268
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getDownTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getHistoricalEventTime(I)Ljava/util/Date;
    .locals 2

    .prologue
    .line 288
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getHistoricalEventTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getHistoricalPressure(II)F
    .locals 2

    .prologue
    .line 300
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getHistoricalPressure(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)F

    move-result v0

    return v0
.end method

.method public getHistoricalX(II)F
    .locals 2

    .prologue
    .line 292
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getHistoricalX(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)F

    move-result v0

    return v0
.end method

.method public getHistoricalY(II)F
    .locals 2

    .prologue
    .line 296
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getHistoricalY(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)F

    move-result v0

    return v0
.end method

.method public getHistorySize()I
    .locals 2

    .prologue
    .line 284
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getHistorySize(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I

    move-result v0

    return v0
.end method

.method public getPointerCount()I
    .locals 2

    .prologue
    .line 272
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getPointerCount(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I

    move-result v0

    return v0
.end method

.method public getPointerId(I)I
    .locals 2

    .prologue
    .line 244
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getPointerId(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)I

    move-result v0

    return v0
.end method

.method public getPressure()F
    .locals 2

    .prologue
    .line 248
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getPressure__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F

    move-result v0

    return v0
.end method

.method public getPressure(I)F
    .locals 2

    .prologue
    .line 252
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getPressure__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F

    move-result v0

    return v0
.end method

.method public getRawX()F
    .locals 2

    .prologue
    .line 228
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getRawX__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F

    move-result v0

    return v0
.end method

.method public getRawX(I)F
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getRawX__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F

    move-result v0

    return v0
.end method

.method public getRawY()F
    .locals 2

    .prologue
    .line 236
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getRawY__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F

    move-result v0

    return v0
.end method

.method public getRawY(I)F
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getRawY__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F

    move-result v0

    return v0
.end method

.method public getTouchTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 256
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getTouchTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getX__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F

    move-result v0

    return v0
.end method

.method public getX(I)F
    .locals 2

    .prologue
    .line 208
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getX__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 212
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getY__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F

    move-result v0

    return v0
.end method

.method public getY(I)F
    .locals 2

    .prologue
    .line 216
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_getY__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F

    move-result v0

    return v0
.end method

.method public setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V
    .locals 0

    .prologue
    .line 92
    if-nez p1, :cond_0

    .line 93
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->cacheAction(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 96
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setA(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 97
    return-void
.end method

.method public setDownTime(Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_setDownTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Ljava/util/Date;)V

    .line 265
    return-void
.end method

.method public setTouchTime(Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 260
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_setTouchTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Ljava/util/Date;)V

    .line 261
    return-void
.end method

.method public setX(IF)V
    .locals 2

    .prologue
    .line 220
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_setX(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;IF)V

    .line 221
    return-void
.end method

.method public setY(IF)V
    .locals 2

    .prologue
    .line 224
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGTouchEvent_setY(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;IF)V

    .line 225
    return-void
.end method
