.class final Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "SGGeometryGeneratorHolder.java"


# direct methods
.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>(JZ)V

    .line 21
    return-void
.end method


# virtual methods
.method protected generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 10

    .prologue
    .line 25
    new-instance v9, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move v7, p3

    move v8, p4

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGenerator_generate(JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;FJLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v9, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(JZ)V

    return-object v9
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 6

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGenerator_isBelongsToGeometry(JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z

    move-result v0

    return v0
.end method
