.class public Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;
.super Ljava/lang/Object;
.source "SGVisualValueReceiver.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 47
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->new_SGVisualValueReceiver()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;-><init>(JZ)V

    .line 48
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JZZ)V

    .line 49
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCMemOwn:Z

    .line 28
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    .line 29
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)J
    .locals 2

    .prologue
    .line 32
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 36
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCMemOwn:Z

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGVisualValueReceiver(J)V

    .line 41
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    .line 43
    :cond_1
    return-void
.end method

.method public onContentRect(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onContentRect(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onContentRectSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    goto :goto_0
.end method

.method public onContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onContentRectScale(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 93
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onContentRectScaleSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    goto :goto_0
.end method

.method public onCustomProperty(Ljava/lang/String;F)V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;F)V

    .line 101
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;F)V

    goto :goto_0
.end method

.method public onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 7

    .prologue
    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomProperty__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    goto :goto_0
.end method

.method public onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 7

    .prologue
    .line 104
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 105
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    goto :goto_0
.end method

.method public onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 7

    .prologue
    .line 108
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomProperty__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    goto :goto_0
.end method

.method public onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 7

    .prologue
    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomProperty__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    goto :goto_0
.end method

.method public onGeometryGeneratorParam(F)V
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V

    .line 97
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onGeometryGeneratorParamSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V

    goto :goto_0
.end method

.method public onOpacity(F)V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onOpacity(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V

    .line 81
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onOpacitySwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V

    goto :goto_0
.end method

.method public onOther()V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onOther(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V

    .line 121
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onOtherSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V

    goto :goto_0
.end method

.method public onPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onPosition(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onPositionSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    goto :goto_0
.end method

.method public onPositionPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onPositionPivot(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onPositionPivotSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    goto :goto_0
.end method

.method public onRotation(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 60
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onRotation(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onRotationSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    goto :goto_0
.end method

.method public onRotationPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onRotationPivot(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onRotationPivotSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    goto :goto_0
.end method

.method public onScale(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 68
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onScale(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onScaleSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    goto :goto_0
.end method

.method public onScalePivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 72
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onScalePivot(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 73
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onScalePivotSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    goto :goto_0
.end method

.method public onSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onSize(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 77
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onSizeSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    goto :goto_0
.end method

.method public onSpriteRect(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 84
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onSpriteRect(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGVisualValueReceiver_onSpriteRectSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    goto :goto_0
.end method
