.class public Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
.super Ljava/lang/Object;
.source "SGMatrix4f.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGMatrix4f__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(FFFFFFFFFFFFFFFF)V
    .locals 3

    .prologue
    .line 50
    invoke-static/range {p1 .. p16}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGMatrix4f__SWIG_1(FFFFFFFFFFFFFFFF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 3

    .prologue
    .line 58
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGMatrix4f__SWIG_3(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector4f;Lcom/samsung/android/sdk/sgi/base/SGVector4f;Lcom/samsung/android/sdk/sgi/base/SGVector4f;Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 12

    .prologue
    .line 54
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v6

    invoke-static/range {p4 .. p4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v9

    move-object v2, p1

    move-object v5, p2

    move-object v8, p3

    move-object/from16 v11, p4

    invoke-static/range {v0 .. v11}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGMatrix4f__SWIG_2(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    .line 55
    return-void
.end method

.method public static createLookAtLH(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 10

    .prologue
    .line 252
    new-instance v9, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createLookAtLH(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v9, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v9
.end method

.method public static createLookAtRH(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 10

    .prologue
    .line 248
    new-instance v9, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createLookAtRH(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v9, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v9
.end method

.method public static createOrthoLH(FFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 244
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createOrthoLH__SWIG_1(FFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createOrthoLH(FFFFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 240
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static/range {p0 .. p5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createOrthoLH__SWIG_0(FFFFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createOrthoRH(FFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 236
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createOrthoRH__SWIG_1(FFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createOrthoRH(FFFFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 232
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static/range {p0 .. p5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createOrthoRH__SWIG_0(FFFFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createPerspectiveFovLH(FFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 276
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createPerspectiveFovLH(FFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createPerspectiveFovRH(FFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 272
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createPerspectiveFovRH(FFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createPerspectiveLH(FFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 268
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createPerspectiveLH__SWIG_1(FFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createPerspectiveLH(FFFFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 264
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static/range {p0 .. p5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createPerspectiveLH__SWIG_0(FFFFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createPerspectiveRH(FFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 260
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createPerspectiveRH__SWIG_1(FFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createPerspectiveRH(FFFFFF)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 256
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static/range {p0 .. p5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createPerspectiveRH__SWIG_0(FFFFFF)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotation(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 142
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v1

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createRotation__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotation(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 146
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 148
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ordinal()I

    move-result v3

    invoke-static {v1, v2, p0, v3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createRotation__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationAxis(Lcom/samsung/android/sdk/sgi/base/SGVector3f;F)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 188
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v1

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createRotationAxis(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationX(F)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 153
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createRotationX(F)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationY(F)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 157
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createRotationY(F)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationZ(F)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_createRotationZ(F)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    goto :goto_0
.end method

.method public static getIdentity()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getIdentity()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 300
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_add(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 301
    return-void
.end method

.method public divide(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 312
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_divide(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 313
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGMatrix4f(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getColumn(I)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 4

    .prologue
    .line 94
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getColumn(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v0
.end method

.method public getDeterminant()F
    .locals 2

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getDeterminant(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)F

    move-result v0

    return v0
.end method

.method public getElement(I)F
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getElement__SWIG_2(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;I)F

    move-result v0

    return v0
.end method

.method public getElement(II)F
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getElement__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;II)F

    move-result v0

    return v0
.end method

.method public getFullTranslation()Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 4

    .prologue
    .line 212
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getFullTranslation(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v0
.end method

.method public getQuaternion()Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 284
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getQuaternion(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public getRow(I)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 4

    .prologue
    .line 102
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getRow(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v0
.end method

.method public getTrace()F
    .locals 2

    .prologue
    .line 288
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getTrace(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)F

    move-result v0

    return v0
.end method

.method public getTranslation()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 208
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_getTranslation(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public interpolateLineary(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
    .locals 7

    .prologue
    .line 216
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_interpolateLineary(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V

    .line 217
    return-void
.end method

.method public interpolateSpherically(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
    .locals 7

    .prologue
    .line 220
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_interpolateSpherically(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V

    .line 221
    return-void
.end method

.method public inverse()V
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_inverse(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 119
    return-void
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Z
    .locals 6

    .prologue
    .line 296
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_isEqual__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Z

    move-result v0

    return v0
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)Z
    .locals 7

    .prologue
    .line 292
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_isEqual__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)Z

    move-result v0

    return v0
.end method

.method public isIdentity()Z
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_isIdentity(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Z

    move-result v0

    return v0
.end method

.method public multiply(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 308
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_multiply(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 309
    return-void
.end method

.method public multiplyByElements(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 280
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_multiplyByElements(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 281
    return-void
.end method

.method public rotate(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotate__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 178
    return-void
.end method

.method public rotate(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;)V
    .locals 7

    .prologue
    .line 181
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 183
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ordinal()I

    move-result v6

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotate__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V

    .line 185
    return-void
.end method

.method public rotateAxis(Lcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
    .locals 7

    .prologue
    .line 192
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateAxis(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 193
    return-void
.end method

.method public rotateQuaternion(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 7

    .prologue
    .line 138
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateQuaternion(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v6
.end method

.method public rotateVector(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 7

    .prologue
    .line 130
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateVector__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v6
.end method

.method public rotateVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 7

    .prologue
    .line 134
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateVector__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v6
.end method

.method public rotateX(F)V
    .locals 2

    .prologue
    .line 165
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateX(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V

    .line 166
    return-void
.end method

.method public rotateY(F)V
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateY(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V

    .line 170
    return-void
.end method

.method public rotateZ(F)V
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_rotateZ(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V

    .line 174
    return-void
.end method

.method public scale(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 228
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_scale__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 229
    return-void
.end method

.method public scale(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 6

    .prologue
    .line 224
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_scale__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 225
    return-void
.end method

.method public set(FFFFFFFFFFFFFFFF)V
    .locals 20

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move/from16 v12, p9

    move/from16 v13, p10

    move/from16 v14, p11

    move/from16 v15, p12

    move/from16 v16, p13

    move/from16 v17, p14

    move/from16 v18, p15

    move/from16 v19, p16

    invoke-static/range {v1 .. v19}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_set__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;FFFFFFFFFFFFFFFF)V

    .line 71
    return-void
.end method

.method public set(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_set__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 67
    return-void
.end method

.method public setColumn(ILcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 7

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_setColumn(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IJLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 99
    return-void
.end method

.method public setElement(IF)V
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_setElement__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IF)V

    .line 91
    return-void
.end method

.method public setElement(IIF)V
    .locals 6

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_setElement__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IIF)V

    .line 87
    return-void
.end method

.method public setRow(ILcom/samsung/android/sdk/sgi/base/SGVector4f;)V
    .locals 7

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_setRow(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IJLcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 107
    return-void
.end method

.method public subtract(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 304
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_subtract(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 305
    return-void
.end method

.method public transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 7

    .prologue
    .line 122
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_transformVector__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v6
.end method

.method public transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 7

    .prologue
    .line 126
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_transformVector__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v6
.end method

.method public translate(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 196
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_translate(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 197
    return-void
.end method

.method public translateVector(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 7

    .prologue
    .line 200
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_translateVector__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v6
.end method

.method public translateVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
    .locals 7

    .prologue
    .line 204
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_translateVector__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    return-object v6
.end method

.method public transpose()V
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGMatrix4f_transpose(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 75
    return-void
.end method
