.class Lcom/samsung/android/sdk/sgi/render/SGJNI;
.super Ljava/lang/Object;
.source "SGJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 21
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->initLibrary()V

    .line 271
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->swig_module_init()V

    .line 272
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native PROGRAM_PROPERTY_NAME_get()Ljava/lang/String;
.end method

.method public static final native SGAlphaBlendProperty_SWIGUpcast(J)J
.end method

.method public static final native SGAlphaBlendProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)J
.end method

.method public static final native SGAlphaBlendProperty_getBlendColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)J
.end method

.method public static final native SGAlphaBlendProperty_getBlendEquationAlpha(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getBlendEquationColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getDestinationFactor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getDestinationFactorAlpha(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getDestinationFactorColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getSourceFactor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getSourceFactorAlpha(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_getSourceFactorColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)I
.end method

.method public static final native SGAlphaBlendProperty_init(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)V
.end method

.method public static final native SGAlphaBlendProperty_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;)Z
.end method

.method public static final native SGAlphaBlendProperty_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;Z)V
.end method

.method public static final native SGAlphaBlendProperty_setBlendColor(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGAlphaBlendProperty_setBlendEquation(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;II)V
.end method

.method public static final native SGAlphaBlendProperty_setFactors(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;II)V
.end method

.method public static final native SGAlphaBlendProperty_setFactorsSeparate(JLcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;IIII)V
.end method

.method public static final native SGBitmapTexture2DProperty_SWIGUpcast(J)J
.end method

.method public static final native SGBitmapTexture2DProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)J
.end method

.method public static final native SGBitmapTexture2DProperty_getDataFormat(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getDataType(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getHeight(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getInternalFormat(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getMagFilter(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getMinFilter(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getWidth(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getWrapS(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_getWrapT(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)I
.end method

.method public static final native SGBitmapTexture2DProperty_isGenerateMipmapsEnabled(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;)Z
.end method

.method public static final native SGBitmapTexture2DProperty_setBitmap(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGBitmapTexture2DProperty_setGenerateMipmapsEnabled(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;Z)V
.end method

.method public static final native SGBitmapTexture2DProperty_setWrapType(JLcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;II)V
.end method

.method public static final native SGBuffer_getBuffer(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)Ljava/lang/Object;
.end method

.method public static final native SGBuffer_getDataType(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)I
.end method

.method public static final native SGBuffer_getHandle(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)J
.end method

.method public static final native SGBuffer_getUsageType(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)I
.end method

.method public static final native SGColorMaskProperty_SWIGUpcast(J)J
.end method

.method public static final native SGColorMaskProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)J
.end method

.method public static final native SGColorMaskProperty_getWriteStateA(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z
.end method

.method public static final native SGColorMaskProperty_getWriteStateB(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z
.end method

.method public static final native SGColorMaskProperty_getWriteStateG(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z
.end method

.method public static final native SGColorMaskProperty_getWriteStateR(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z
.end method

.method public static final native SGColorMaskProperty_init(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)V
.end method

.method public static final native SGColorMaskProperty_isColorMaskingEnabled(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z
.end method

.method public static final native SGColorMaskProperty_setColorMaskingEnabled(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;Z)V
.end method

.method public static final native SGColorMaskProperty_setWriteState(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;ZZZZ)V
.end method

.method public static final native SGCompressedTextureFactory_createTexture(Ljava/nio/channels/FileChannel;)J
.end method

.method public static final native SGCullFaceProperty_SWIGUpcast(J)J
.end method

.method public static final native SGCullFaceProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)J
.end method

.method public static final native SGCullFaceProperty_getCullType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)I
.end method

.method public static final native SGCullFaceProperty_getFrontType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)I
.end method

.method public static final native SGCullFaceProperty_init(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)V
.end method

.method public static final native SGCullFaceProperty_isFaceCullingEnabled(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)Z
.end method

.method public static final native SGCullFaceProperty_setCullType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;I)V
.end method

.method public static final native SGCullFaceProperty_setFaceCullingEnabled(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;Z)V
.end method

.method public static final native SGCullFaceProperty_setFrontType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;I)V
.end method

.method public static final native SGDepthProperty_SWIGUpcast(J)J
.end method

.method public static final native SGDepthProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)J
.end method

.method public static final native SGDepthProperty_init(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)V
.end method

.method public static final native SGDepthProperty_isDepthTestEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)Z
.end method

.method public static final native SGDepthProperty_isWriteEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)Z
.end method

.method public static final native SGDepthProperty_setDepthTestEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;Z)V
.end method

.method public static final native SGDepthProperty_setWriteEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;Z)V
.end method

.method public static final native SGFloatArrayProperty_SWIGUpcast(J)J
.end method

.method public static final native SGFloatArrayProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;)J
.end method

.method public static final native SGFloatArrayProperty_getBuffer(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;)Ljava/lang/Object;
.end method

.method public static final native SGFloatArrayProperty_init(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;I)V
.end method

.method public static final native SGFloatArrayProperty_setSize(JLcom/samsung/android/sdk/sgi/render/SGFloatArrayProperty;I)V
.end method

.method public static final native SGFloatProperty_SWIGUpcast(J)J
.end method

.method public static final native SGFloatProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;)J
.end method

.method public static final native SGFloatProperty_get(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;)F
.end method

.method public static final native SGFloatProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;)V
.end method

.method public static final native SGFloatProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;F)V
.end method

.method public static final native SGFloatProperty_set(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;F)V
.end method

.method public static final native SGGeometry___assign__(JLcom/samsung/android/sdk/sgi/render/SGGeometry;JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J
.end method

.method public static final native SGGeometry_addBuffer(JLcom/samsung/android/sdk/sgi/render/SGGeometry;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGBuffer;)V
.end method

.method public static final native SGGeometry_getBoundingBox(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J
.end method

.method public static final native SGGeometry_getBufferName(JLcom/samsung/android/sdk/sgi/render/SGGeometry;I)Ljava/lang/String;
.end method

.method public static final native SGGeometry_getBuffersCount(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)I
.end method

.method public static final native SGGeometry_getHandle(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J
.end method

.method public static final native SGGeometry_getIndexBuffer(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J
.end method

.method public static final native SGGeometry_getPrimitiveType(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)I
.end method

.method public static final native SGGeometry_getVertexBuffer__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGGeometry;Ljava/lang/String;)J
.end method

.method public static final native SGGeometry_getVertexBuffer__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGGeometry;I)J
.end method

.method public static final native SGGeometry_init(JLcom/samsung/android/sdk/sgi/render/SGGeometry;I)V
.end method

.method public static final native SGGeometry_setBoundingBox(JLcom/samsung/android/sdk/sgi/render/SGGeometry;JLcom/samsung/android/sdk/sgi/base/SGBox3f;)V
.end method

.method public static final native SGIndexBuffer_SWIGUpcast(J)J
.end method

.method public static final native SGIndexBuffer___assign__(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)J
.end method

.method public static final native SGIndexBuffer_getPrimitiveType(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)I
.end method

.method public static final native SGIndexBuffer_init(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;III)V
.end method

.method public static final native SGIndexBuffer_setSize(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;I)V
.end method

.method public static final native SGLineWidthProperty_SWIGUpcast(J)J
.end method

.method public static final native SGLineWidthProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)J
.end method

.method public static final native SGLineWidthProperty_getWidth(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)F
.end method

.method public static final native SGLineWidthProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;F)V
.end method

.method public static final native SGLineWidthProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)V
.end method

.method public static final native SGLineWidthProperty_isLineWidthPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;)Z
.end method

.method public static final native SGLineWidthProperty_setLineWidthPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;Z)V
.end method

.method public static final native SGLineWidthProperty_setWidth(JLcom/samsung/android/sdk/sgi/render/SGLineWidthProperty;F)V
.end method

.method public static final native SGMatrix4fProperty_SWIGUpcast(J)J
.end method

.method public static final native SGMatrix4fProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)J
.end method

.method public static final native SGMatrix4fProperty_get(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)J
.end method

.method public static final native SGMatrix4fProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)V
.end method

.method public static final native SGMatrix4fProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4fProperty_set__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4fProperty_set__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;FFFFFFFFFFFFFFFF)V
.end method

.method public static final native SGProperty_getHandle(JLcom/samsung/android/sdk/sgi/render/SGProperty;)J
.end method

.method public static final native SGRenderDataProvider_change_ownership(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;JZ)V
.end method

.method public static final native SGRenderDataProvider_director_connect(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;JZZ)V
.end method

.method public static final native SGRenderDataProvider_loadBuiltinShaderData(Ljava/lang/String;)[B
.end method

.method public static final native SGRenderDataProvider_loadProgram(JLcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Ljava/lang/String;Ljava/lang/String;)[B
.end method

.method public static final native SGRenderDataProvider_loadShaderData(JLcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Ljava/lang/String;)[B
.end method

.method public static final native SGRenderDataProvider_saveProgram(JLcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Ljava/lang/String;Ljava/lang/String;[B)V
.end method

.method public static final native SGResourceShaderProperty_SWIGUpcast(J)J
.end method

.method public static final native SGResourceShaderProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;)J
.end method

.method public static final native SGResourceShaderProperty_init(JLcom/samsung/android/sdk/sgi/render/SGResourceShaderProperty;ILjava/lang/String;)V
.end method

.method public static final native SGShaderProgramProperty_SWIGUpcast(J)J
.end method

.method public static final native SGShaderProgramProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)J
.end method

.method public static final native SGShaderProgramProperty_getShader(JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;I)Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;
.end method

.method public static final native SGShaderProgramProperty_init(JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V
.end method

.method public static final native SGShaderProperty_SWIGUpcast(J)J
.end method

.method public static final native SGShaderProperty_getDataType(JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)I
.end method

.method public static final native SGShaderProperty_getShaderType(JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)I
.end method

.method public static final native SGStencilProperty_SWIGUpcast(J)J
.end method

.method public static final native SGStencilProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)J
.end method

.method public static final native SGStencilProperty_getFunction(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_getGlobalMask(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_getMask(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_getOperationDepthFail(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_getOperationDepthPass(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_getOperationStencilFail(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_getReference(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I
.end method

.method public static final native SGStencilProperty_init(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)V
.end method

.method public static final native SGStencilProperty_isStencilPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)Z
.end method

.method public static final native SGStencilProperty_setFunction(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;III)V
.end method

.method public static final native SGStencilProperty_setGlobalMask(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;I)V
.end method

.method public static final native SGStencilProperty_setStencilOperation(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;III)V
.end method

.method public static final native SGStencilProperty_setStencilPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;Z)V
.end method

.method public static final native SGStringShaderProperty_SWIGUpcast(J)J
.end method

.method public static final native SGStringShaderProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;)J
.end method

.method public static final native SGStringShaderProperty_init(JLcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;ILjava/lang/String;)V
.end method

.method public static final native SGTextureProperty_SWIGUpcast(J)J
.end method

.method public static final native SGTextureProperty_getDataFormat(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getDataType(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getInternalFormat(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getMagFilter(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getMinFilter(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getWrapR(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getWrapS(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGTextureProperty_getWrapT(JLcom/samsung/android/sdk/sgi/render/SGTextureProperty;)I
.end method

.method public static final native SGVector2fProperty_SWIGUpcast(J)J
.end method

.method public static final native SGVector2fProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;)J
.end method

.method public static final native SGVector2fProperty_get(JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;)J
.end method

.method public static final native SGVector2fProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;)V
.end method

.method public static final native SGVector2fProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2fProperty_set__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2fProperty_set__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector2fProperty;FF)V
.end method

.method public static final native SGVector3fProperty_SWIGUpcast(J)J
.end method

.method public static final native SGVector3fProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)J
.end method

.method public static final native SGVector3fProperty_get(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)J
.end method

.method public static final native SGVector3fProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)V
.end method

.method public static final native SGVector3fProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3fProperty_set__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3fProperty_set__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;FFF)V
.end method

.method public static final native SGVector4fProperty_SWIGUpcast(J)J
.end method

.method public static final native SGVector4fProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;)J
.end method

.method public static final native SGVector4fProperty_get(JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;)J
.end method

.method public static final native SGVector4fProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;)V
.end method

.method public static final native SGVector4fProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4fProperty_set__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4fProperty_set__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector4fProperty;FFFF)V
.end method

.method public static final native SGVertexBuffer_SWIGUpcast(J)J
.end method

.method public static final native SGVertexBuffer___assign__(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)J
.end method

.method public static final native SGVertexBuffer_getComponentsPerElement(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)I
.end method

.method public static final native SGVertexBuffer_getVertexCount(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;)I
.end method

.method public static final native SGVertexBuffer_init(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;IIII)V
.end method

.method public static final native SGVertexBuffer_setVertexCount(JLcom/samsung/android/sdk/sgi/render/SGVertexBuffer;I)V
.end method

.method public static SwigDirector_SGRenderDataProvider_loadProgram(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->loadProgram(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static SwigDirector_SGRenderDataProvider_loadShaderData(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->loadShaderData(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static SwigDirector_SGRenderDataProvider_saveProgram(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 266
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->saveProgram(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 267
    return-void
.end method

.method public static final native delete_SGAlphaBlendProperty(J)V
.end method

.method public static final native delete_SGBitmapTexture2DProperty(J)V
.end method

.method public static final native delete_SGBuffer(J)V
.end method

.method public static final native delete_SGColorMaskProperty(J)V
.end method

.method public static final native delete_SGCompressedTextureFactory(J)V
.end method

.method public static final native delete_SGCullFaceProperty(J)V
.end method

.method public static final native delete_SGDepthProperty(J)V
.end method

.method public static final native delete_SGFloatArrayProperty(J)V
.end method

.method public static final native delete_SGFloatProperty(J)V
.end method

.method public static final native delete_SGGeometry(J)V
.end method

.method public static final native delete_SGIndexBuffer(J)V
.end method

.method public static final native delete_SGLineWidthProperty(J)V
.end method

.method public static final native delete_SGMatrix4fProperty(J)V
.end method

.method public static final native delete_SGProperty(J)V
.end method

.method public static final native delete_SGRenderDataProvider(J)V
.end method

.method public static final native delete_SGResourceShaderProperty(J)V
.end method

.method public static final native delete_SGShaderProgramProperty(J)V
.end method

.method public static final native delete_SGShaderProperty(J)V
.end method

.method public static final native delete_SGStencilProperty(J)V
.end method

.method public static final native delete_SGStringShaderProperty(J)V
.end method

.method public static final native delete_SGTextureProperty(J)V
.end method

.method public static final native delete_SGVector2fProperty(J)V
.end method

.method public static final native delete_SGVector3fProperty(J)V
.end method

.method public static final native delete_SGVector4fProperty(J)V
.end method

.method public static final native delete_SGVertexBuffer(J)V
.end method

.method public static final native new_SGAlphaBlendProperty()J
.end method

.method public static final native new_SGBitmapTexture2DProperty(II)J
.end method

.method public static final native new_SGBuffer()J
.end method

.method public static final native new_SGColorMaskProperty()J
.end method

.method public static final native new_SGCullFaceProperty()J
.end method

.method public static final native new_SGDepthProperty()J
.end method

.method public static final native new_SGFloatArrayProperty__SWIG_0()J
.end method

.method public static final native new_SGFloatArrayProperty__SWIG_1(I)J
.end method

.method public static final native new_SGFloatProperty()J
.end method

.method public static final native new_SGGeometry__SWIG_0()J
.end method

.method public static final native new_SGGeometry__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)J
.end method

.method public static final native new_SGGeometry__SWIG_2(I)J
.end method

.method public static final native new_SGIndexBuffer__SWIG_0()J
.end method

.method public static final native new_SGIndexBuffer__SWIG_1(III)J
.end method

.method public static final native new_SGLineWidthProperty()J
.end method

.method public static final native new_SGMatrix4fProperty()J
.end method

.method public static final native new_SGProperty()J
.end method

.method public static final native new_SGRenderDataProvider()J
.end method

.method public static final native new_SGResourceShaderProperty__SWIG_0()J
.end method

.method public static final native new_SGResourceShaderProperty__SWIG_1(ILjava/lang/String;)J
.end method

.method public static final native new_SGShaderProgramProperty__SWIG_0()J
.end method

.method public static final native new_SGShaderProgramProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)J
.end method

.method public static final native new_SGStencilProperty()J
.end method

.method public static final native new_SGStringShaderProperty__SWIG_0()J
.end method

.method public static final native new_SGStringShaderProperty__SWIG_1(ILjava/lang/String;)J
.end method

.method public static final native new_SGVector2fProperty()J
.end method

.method public static final native new_SGVector3fProperty()J
.end method

.method public static final native new_SGVector4fProperty()J
.end method

.method public static final native new_SGVertexBuffer__SWIG_0()J
.end method

.method public static final native new_SGVertexBuffer__SWIG_1(IIII)J
.end method

.method private static final native swig_module_init()V
.end method
