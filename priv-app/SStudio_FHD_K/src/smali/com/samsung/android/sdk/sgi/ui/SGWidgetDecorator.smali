.class public final Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;
.super Ljava/lang/Object;
.source "SGWidgetDecorator.java"


# instance fields
.field private mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field private swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 392
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGWidgetDecorator()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;-><init>(JZ)V

    .line 393
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCMemOwn:Z

    .line 33
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;-><init>()V

    .line 387
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->init(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 388
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 389
    return-void
.end method

.method private addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 6

    .prologue
    .line 420
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_addL__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    return v0
.end method

.method private addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
    .locals 7

    .prologue
    .line 424
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_addL__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)I

    move-result v0

    return v0
.end method

.method private bringLToF(I)V
    .locals 2

    .prologue
    .line 448
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_bringLToF__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V

    .line 449
    return-void
.end method

.method private bringLToF(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 6

    .prologue
    .line 452
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_bringLToF__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 453
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)J
    .locals 2

    .prologue
    .line 37
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 508
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getHandle(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)Z
    .locals 7

    .prologue
    .line 404
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)Z

    move-result v0

    return v0
.end method

.method private init(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 396
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_init(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 397
    return-void
.end method

.method private removeAllL()V
    .locals 2

    .prologue
    .line 464
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeAllL(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)V

    .line 465
    return-void
.end method

.method private removeL(I)V
    .locals 2

    .prologue
    .line 472
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeL__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V

    .line 473
    return-void
.end method

.method private removeL(II)V
    .locals 2

    .prologue
    .line 468
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeL__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;II)V

    .line 469
    return-void
.end method

.method private removeL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 6

    .prologue
    .line 428
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeL__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 429
    return-void
.end method

.method private sendLToB(I)V
    .locals 2

    .prologue
    .line 456
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_sendLToB__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V

    .line 457
    return-void
.end method

.method private sendLToB(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 6

    .prologue
    .line 460
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_sendLToB__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 461
    return-void
.end method

.method private setGeometryGeneratorNative(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
    .locals 6

    .prologue
    .line 432
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_setGeometryGeneratorNative(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 433
    return-void
.end method

.method private swapL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 9

    .prologue
    .line 444
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_swapL(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 445
    return-void
.end method


# virtual methods
.method public addFilter(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)V
    .locals 6

    .prologue
    .line 476
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_addFilter(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V

    .line 477
    return-void
.end method

.method public addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 3

    .prologue
    .line 146
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_2

    .line 152
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mChildArray"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 153
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 154
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 155
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    .line 170
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    return v0

    .line 157
    :catch_0
    move-exception v0

    .line 159
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 161
    throw v1

    .line 166
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
    .locals 3

    .prologue
    .line 186
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_2

    .line 192
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mChildArray"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 193
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 194
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 195
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :cond_1
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I

    move-result v0

    .line 210
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 211
    return v0

    .line 197
    :catch_0
    move-exception v0

    .line 199
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 201
    throw v1

    .line 206
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 207
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 133
    return-void
.end method

.method public bringLayerToFront(I)V
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 299
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "index is out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->bringLToF(I)V

    .line 301
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 302
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    return-void
.end method

.method public bringLayerToFront(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 3

    .prologue
    .line 326
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 328
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    .line 329
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->bringLToF(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 330
    if-eqz v0, :cond_2

    .line 334
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mChildArray"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 335
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 336
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 337
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 341
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 342
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 343
    throw v1

    .line 346
    :cond_2
    if-eqz v1, :cond_1

    iget-object v0, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 56
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 58
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->getWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->getWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 42
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCMemOwn:Z

    .line 44
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGWidgetDecorator(J)V

    .line 46
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    .line 48
    :cond_1
    return-void
.end method

.method public findLayerById(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 2

    .prologue
    .line 527
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_findLayerById(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public findLayerByName(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 2

    .prologue
    .line 523
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_findLayerByName(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public getFilter(I)Lcom/samsung/android/sdk/sgi/vi/SGFilter;
    .locals 4

    .prologue
    .line 480
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getFilter(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;-><init>(JZ)V

    return-object v0
.end method

.method public getFilterCount()I
    .locals 2

    .prologue
    .line 484
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getFilterCount(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)I

    move-result v0

    return v0
.end method

.method public getGeometryGenerator()Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mGeometryGenerator:Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;

    return-object v0
.end method

.method public getGeometryGeneratorParam()F
    .locals 2

    .prologue
    .line 440
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)F

    move-result v0

    return v0
.end method

.method public getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    return-object v0
.end method

.method public getLayer(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 114
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLayerIndex(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getLayerPosition(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->getLayerIndex(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    return v0
.end method

.method public getLayersCount()I
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getProgramProperty()Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 5

    .prologue
    .line 516
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getProgramProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)J

    move-result-wide v1

    .line 517
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    .line 518
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(JZ)V

    .line 519
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)Z
    .locals 6

    .prologue
    .line 416
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getProgramProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)Z

    move-result v0

    return v0
.end method

.method public getProperty(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 512
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_getProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public getWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->getHandle()J

    move-result-wide v0

    .line 65
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 66
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public removeAllFilters()V
    .locals 2

    .prologue
    .line 496
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeAllFilters(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)V

    .line 497
    return-void
.end method

.method public removeAllLayers()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->removeAllL()V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 263
    :cond_0
    return-void
.end method

.method public removeFilter(I)V
    .locals 2

    .prologue
    .line 492
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeFilter__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V

    .line 493
    return-void
.end method

.method public removeFilter(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)V
    .locals 6

    .prologue
    .line 488
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeFilter__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V

    .line 489
    return-void
.end method

.method public removeLayer(I)V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->removeL(I)V

    .line 235
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 236
    return-void
.end method

.method public removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 1

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->removeL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method public removeLayers(II)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 247
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->removeL(II)V

    move v0, p1

    .line 248
    :goto_0
    if-gt v0, p2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_0
    return-void
.end method

.method public removeProperty(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 408
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_removeProperty(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;)V

    .line 409
    return-void
.end method

.method public resetFilterFrameBufferSize()V
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_resetFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)V

    .line 505
    return-void
.end method

.method public sendLayerToBack(I)V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 312
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "index is out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->sendLToB(I)V

    .line 314
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 315
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 317
    return-void
.end method

.method public sendLayerToBack(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 3

    .prologue
    .line 357
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 359
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    .line 360
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->sendLToB(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 361
    if-eqz v0, :cond_2

    .line 365
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mChildArray"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 366
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 367
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 368
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 379
    return-void

    .line 370
    :catch_0
    move-exception v0

    .line 372
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 374
    throw v1

    .line 377
    :cond_2
    if-eqz v1, :cond_1

    iget-object v0, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setFilterFrameBufferSize(FF)V
    .locals 2

    .prologue
    .line 500
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_setFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;FF)V

    .line 501
    return-void
.end method

.method public setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->setGeometryGeneratorNative(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mGeometryGenerator:Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;

    .line 88
    return-void
.end method

.method public setGeometryGeneratorParam(F)V
    .locals 2

    .prologue
    .line 436
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_setGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;F)V

    .line 437
    return-void
.end method

.method public setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V
    .locals 6

    .prologue
    .line 412
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_setProgramProperty(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 413
    return-void
.end method

.method public setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 7

    .prologue
    .line 400
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetDecorator_setProperty(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 401
    return-void
.end method

.method public swapLayers(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 3

    .prologue
    .line 278
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->swapL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 280
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 281
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v2, v2, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    invoke-static {v2, v0, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 282
    return-void
.end method
