.class Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "SGEditTextBoxWrappedConnection.java"


# instance fields
.field private mEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputConnection;Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;->mEditText:Landroid/widget/EditText;

    .line 32
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;->mEditText:Landroid/widget/EditText;

    .line 33
    return-void
.end method


# virtual methods
.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 37
    const/16 v1, 0x43

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;->mEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBoxWrappedConnection;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 41
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/inputmethod/InputConnectionWrapper;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
