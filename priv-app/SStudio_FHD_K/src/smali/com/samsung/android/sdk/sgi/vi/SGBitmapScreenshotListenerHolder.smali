.class final Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;
.source "SGBitmapScreenshotListenerHolder.java"


# instance fields
.field mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

.field mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 34
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

    .line 35
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    .line 42
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method


# virtual methods
.method public detachListener()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onCompleted(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;->onCompleted(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "SGBitmapScreenshotListener::onCompleted error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
