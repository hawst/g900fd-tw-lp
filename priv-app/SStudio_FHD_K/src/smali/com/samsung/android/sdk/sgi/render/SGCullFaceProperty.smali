.class public Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGCullFaceProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGCullFaceProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;-><init>(JZ)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->init()V

    .line 42
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_init(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)V

    .line 46
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;
    .locals 7

    .prologue
    .line 79
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGCullFaceProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getCullType()Lcom/samsung/android/sdk/sgi/render/SGCullType;
    .locals 3

    .prologue
    .line 53
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGCullType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGCullType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_getCullType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getFrontType()Lcom/samsung/android/sdk/sgi/render/SGFrontType;
    .locals 3

    .prologue
    .line 57
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_getFrontType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isFaceCullingEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_isFaceCullingEnabled(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;)Z

    move-result v0

    return v0
.end method

.method public setCullType(Lcom/samsung/android/sdk/sgi/render/SGCullType;)V
    .locals 3

    .prologue
    .line 65
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 67
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGCullType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_setCullType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;I)V

    .line 69
    return-void
.end method

.method public setFaceCullingEnabled(Z)V
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_setFaceCullingEnabled(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;Z)V

    .line 62
    return-void
.end method

.method public setFrontType(Lcom/samsung/android/sdk/sgi/render/SGFrontType;)V
    .locals 3

    .prologue
    .line 72
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 74
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGCullFaceProperty_setFrontType(JLcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;I)V

    .line 76
    return-void
.end method
