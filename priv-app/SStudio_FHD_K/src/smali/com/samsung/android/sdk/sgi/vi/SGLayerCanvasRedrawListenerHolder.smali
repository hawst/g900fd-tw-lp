.class final Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;
.source "SGLayerCanvasRedrawListenerHolder.java"


# instance fields
.field mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 31
    return-void
.end method


# virtual methods
.method public getInterface()Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;

    return-object v0
.end method

.method public onDraw(JLandroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;->onDraw(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 42
    const-string v1, "SGLayerCanvasRedrawListener::onDraw error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setInterface(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;

    .line 54
    return-void
.end method
