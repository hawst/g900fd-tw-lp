.class public final enum Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
.super Ljava/lang/Enum;
.source "SGBlendFactor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum CONSTANT_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum CONSTANT_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum DST_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum DST_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE_MINUS_CONSTANT_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE_MINUS_CONSTANT_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE_MINUS_DST_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE_MINUS_DST_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE_MINUS_SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ONE_MINUS_SRC_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum SRC_ALPHA_SATURATE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum SRC_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

.field public static final enum ZERO:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ZERO"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ZERO:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "SRC_COLOR"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->SRC_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE_MINUS_SRC_COLOR"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_SRC_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "DST_COLOR"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->DST_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE_MINUS_DST_COLOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_DST_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "SRC_ALPHA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE_MINUS_SRC_ALPHA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "DST_ALPHA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->DST_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE_MINUS_DST_ALPHA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_DST_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 29
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "CONSTANT_COLOR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->CONSTANT_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 30
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE_MINUS_CONSTANT_COLOR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_CONSTANT_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 31
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "CONSTANT_ALPHA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->CONSTANT_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 32
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "ONE_MINUS_CONSTANT_ALPHA"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_CONSTANT_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 33
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    const-string v1, "SRC_ALPHA_SATURATE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->SRC_ALPHA_SATURATE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    .line 18
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ZERO:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->SRC_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_SRC_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->DST_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_DST_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->DST_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_DST_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->CONSTANT_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_CONSTANT_COLOR:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->CONSTANT_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_CONSTANT_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->SRC_ALPHA_SATURATE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    return-object v0
.end method
