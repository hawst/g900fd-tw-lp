.class public final enum Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;
.super Ljava/lang/Enum;
.source "SGBufferUsageType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum DYNAMIC_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum DYNAMIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum DYNAMIC_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum STATIC_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum STATIC_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum STREAM_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum STREAM_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

.field public static final enum STREAM_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "STREAM_DRAW"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STREAM_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "STREAM_READ"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STREAM_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "STREAM_COPY"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STREAM_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "STATIC_DRAW"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "STATIC_READ"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "STATIC_COPY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "DYNAMIC_DRAW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->DYNAMIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "DYNAMIC_READ"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->DYNAMIC_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const-string v1, "DYNAMIC_COPY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->DYNAMIC_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 18
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STREAM_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STREAM_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STREAM_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->DYNAMIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->DYNAMIC_READ:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->DYNAMIC_COPY:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    return-object v0
.end method
