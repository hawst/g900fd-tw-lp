.class public final enum Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;
.super Ljava/lang/Enum;
.source "SGTextureDataFormat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum BGR:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum BGRA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum DEPTH_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum LUMINANCE:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum LUMINANCE_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum RGB:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

.field public static final enum RGBA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "RGB"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->RGB:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "BGR"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->BGR:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "RGBA"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->RGBA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "BGRA"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->BGRA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "LUMINANCE"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->LUMINANCE:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "LUMINANCE_ALPHA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->LUMINANCE_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    const-string v1, "DEPTH_COMPONENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->DEPTH_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    .line 18
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->RGB:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->BGR:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->RGBA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->BGRA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->LUMINANCE:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->LUMINANCE_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->DEPTH_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureDataFormat;

    return-object v0
.end method
