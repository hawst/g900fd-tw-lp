.class public Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;
.super Lcom/samsung/android/sdk/sgi/base/SGRegistrator;
.source "SGMemoryRegistrator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$1;,
        Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;,
        Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$MemoryRegistratorHolder;
    }
.end annotation


# instance fields
.field private mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;-><init>()V

    .line 53
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$1;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$MemoryRegistratorHolder;->HOLDER_INSTANCE:Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    return-object v0
.end method


# virtual methods
.method public AddToManagementList(J)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;

    .line 94
    if-eqz v0, :cond_2

    .line 96
    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->weak:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    .line 97
    if-eqz v2, :cond_1

    .line 99
    iget v3, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    .line 100
    iget v3, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    if-ne v3, v1, :cond_0

    .line 101
    iput-object v2, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->strong:Ljava/lang/Object;

    :cond_0
    move v0, v1

    .line 107
    :goto_0
    return v0

    .line 104
    :cond_1
    const-string v0, "SGMemoryRegistrator"

    const-string v1, "Trying to add collected object"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    :cond_2
    const-string v0, "SGMemoryRegistrator"

    const-string v1, "Trying to add unregistered object"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public Deregister(J)Z
    .locals 2

    .prologue
    .line 80
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetObjectByPointer(J)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 132
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    move-object v0, v1

    .line 137
    :goto_0
    return-object v0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;

    .line 135
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->weak:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_1

    .line 136
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->weak:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 137
    goto :goto_0
.end method

.method public Register(Ljava/lang/Object;J)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 63
    if-eqz p1, :cond_0

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-eqz v1, :cond_0

    .line 65
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;-><init>(Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;)V

    .line 66
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->weak:Ljava/lang/ref/WeakReference;

    .line 67
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 69
    const-string v1, "SGMemoryRegistrator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicate key when registering "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public RemoveFromManagementList(J)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->mRegistrionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;

    .line 115
    if-eqz v0, :cond_1

    .line 117
    iget v3, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    if-ne v3, v1, :cond_0

    .line 119
    iput v2, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    .line 120
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->strong:Ljava/lang/Object;

    move v0, v1

    .line 126
    :goto_0
    return v0

    .line 123
    :cond_0
    iget v1, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    if-lez v1, :cond_1

    .line 124
    iget v1, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator$ReferenceHolder;->counter:I

    :cond_1
    move v0, v2

    .line 126
    goto :goto_0
.end method
