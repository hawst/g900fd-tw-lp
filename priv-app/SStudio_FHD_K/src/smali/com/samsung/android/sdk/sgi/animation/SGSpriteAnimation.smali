.class public final Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;
.super Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
.source "SGSpriteAnimation.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;-><init>(JZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;
    .locals 7

    .prologue
    .line 81
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;-><init>(JZ)V

    return-object v6
.end method

.method public addKeyFrame(FI)Z
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;FI)Z

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGSpriteAnimation(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public getEndIndex()I
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_getEndIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)I

    move-result v0

    return v0
.end method

.method public getFrameSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_getFrameSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getSourceSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_getSourceSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getStartIndex()I
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_getStartIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)I

    move-result v0

    return v0
.end method

.method public removeKeyFrame(F)Z
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;F)Z

    move-result v0

    return v0
.end method

.method public setEndIndex(I)V
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_setEndIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;I)V

    .line 74
    return-void
.end method

.method public setFrameSize(FF)V
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_setFrameSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;FF)V

    .line 50
    return-void
.end method

.method public setSourceSize(FF)V
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_setSourceSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;FF)V

    .line 42
    return-void
.end method

.method public setStartIndex(I)V
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGSpriteAnimation_setStartIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;I)V

    .line 66
    return-void
.end method
