.class public final enum Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;
.super Ljava/lang/Enum;
.source "SGCubicBezierTimingFunctionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

.field public static final enum EASE_IN:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

.field public static final enum EASE_IN_OUT:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

.field public static final enum EASE_OUT:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

.field public static final enum EASE_OUT_IN:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    const-string v1, "EASE_IN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_IN:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    const-string v1, "EASE_OUT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_OUT:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    const-string v1, "EASE_IN_OUT"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_IN_OUT:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    const-string v1, "EASE_OUT_IN"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_OUT_IN:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_IN:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_OUT:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_IN_OUT:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->EASE_OUT_IN:Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGCubicBezierTimingFunctionType;

    return-object v0
.end method
