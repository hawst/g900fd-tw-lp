.class public final Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
.super Ljava/lang/Object;
.source "SGAnimationTransaction.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 96
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->new_SGAnimationTransaction()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>(JZ)V

    .line 97
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->init()V

    .line 98
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCMemOwn:Z

    .line 28
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    .line 29
    return-void
.end method

.method private getAnimationListenerNative()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;
    .locals 2

    .prologue
    .line 208
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getAnimationListenerNative(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    move-result-object v0

    return-object v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)J
    .locals 2

    .prologue
    .line 32
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    goto :goto_0
.end method

.method public static getDefaultDuration()I
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getDefaultDuration()I

    move-result v0

    return v0
.end method

.method public static getDefaultEnabled()Z
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getDefaultEnabled()Z

    move-result v0

    return v0
.end method

.method public static getDefaultOffset()I
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getDefaultOffset()I

    move-result v0

    return v0
.end method

.method public static getDefaultTimingFunction()Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 1

    .prologue
    .line 216
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getDefaultTimingFunction()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;

    return-object v0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 220
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getHandle(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)J

    move-result-wide v0

    return-wide v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_init(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)V

    .line 102
    return-void
.end method

.method public static isDefaultDeferredStartEnabled()Z
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_isDefaultDeferredStartEnabled()Z

    move-result v0

    return v0
.end method

.method private setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V
    .locals 6

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setAnimationListener(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 130
    return-void
.end method

.method public static setDefaultDeferredStartEnabled(Z)V
    .locals 0

    .prologue
    .line 196
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDefaultDeferredStartEnabled(Z)V

    .line 197
    return-void
.end method

.method public static setDefaultDuration(I)V
    .locals 0

    .prologue
    .line 165
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDefaultDuration(I)V

    .line 166
    return-void
.end method

.method public static setDefaultEnabled(Z)V
    .locals 0

    .prologue
    .line 157
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDefaultEnabled(Z)V

    .line 158
    return-void
.end method

.method public static setDefaultOffset(I)V
    .locals 0

    .prologue
    .line 173
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDefaultOffset(I)V

    .line 174
    return-void
.end method

.method public static setDefaultTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
    .locals 2

    .prologue
    .line 192
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)J

    move-result-wide v0

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDefaultTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V

    .line 193
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    .locals 7

    .prologue
    .line 204
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction___assign__(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>(JZ)V

    return-object v6
.end method

.method public begin()Z
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_begin(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z

    move-result v0

    return v0
.end method

.method public enableSynchronizedStart(Z)V
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_enableSynchronizedStart(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;Z)V

    .line 150
    return-void
.end method

.method public end()Z
    .locals 2

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_end(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 50
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    if-nez v1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getHandle()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getHandle()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 36
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCMemOwn:Z

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGAnimationTransaction(J)V

    .line 41
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    .line 43
    :cond_1
    return-void
.end method

.method public getAnimationListener()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getAnimationListenerNative()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->getInterface()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentAnimationId()I
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getCurrentAnimationId(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I

    move-result v0

    return v0
.end method

.method public getOffset()I
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I

    move-result v0

    return v0
.end method

.method public getOnSuspendBehaviour()Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;
    .locals 3

    .prologue
    .line 188
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTimingFunction()Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 2

    .prologue
    .line 212
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_getTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getHandle()J

    move-result-wide v0

    .line 58
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 59
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public isDeferredStartEnabled()Z
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_isDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z

    move-result v0

    return v0
.end method

.method public isSynchronizedStartEnabled()Z
    .locals 2

    .prologue
    .line 153
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_isSynchronizedStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z

    move-result v0

    return v0
.end method

.method public setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->getAnimationListenerNative()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->setInterface(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 81
    :goto_0
    return-void

    .line 78
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 79
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    goto :goto_0
.end method

.method public setDeferredStartEnabled(Z)V
    .locals 2

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;Z)V

    .line 142
    return-void
.end method

.method public setDuration(I)V
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;I)V

    .line 114
    return-void
.end method

.method public setOffset(I)V
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;I)V

    .line 122
    return-void
.end method

.method public setOnSuspendBehaviour(Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;)V
    .locals 3

    .prologue
    .line 181
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 183
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;I)V

    .line 185
    return-void
.end method

.method public setTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
    .locals 6

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationTransaction_setTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V

    .line 134
    return-void
.end method
