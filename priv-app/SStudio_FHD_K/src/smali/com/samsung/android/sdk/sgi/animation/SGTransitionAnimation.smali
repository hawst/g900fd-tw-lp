.class public final Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;
.super Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
.source "SGTransitionAnimation.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;-><init>(JZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;
    .locals 7

    .prologue
    .line 65
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;-><init>(JZ)V

    return-object v6
.end method

.method public enableAlphaBlending(Z)V
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_enableAlphaBlending(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;Z)V

    .line 58
    return-void
.end method

.method public enableSynchronizedStart(Z)V
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_enableSynchronizedStart(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;Z)V

    .line 50
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGTransitionAnimation(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public getTransitionDirection()Lcom/samsung/android/sdk/sgi/animation/SGTransitionDirectionType;
    .locals 3

    .prologue
    .line 41
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionDirectionType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGTransitionDirectionType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_getTransitionDirection(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTransitionType()Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;
    .locals 3

    .prologue
    .line 45
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_getTransitionType(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isAlphaBlendingEnabled()Z
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)Z

    move-result v0

    return v0
.end method

.method public isSynchronizedStartEnabled()Z
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_isSynchronizedStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)Z

    move-result v0

    return v0
.end method

.method public overrideSourceTexture(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 6

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_overrideSourceTexture(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 70
    return-void
.end method

.method public overrideTargetTexture(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 6

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGTransitionAnimation_overrideTargetTexture(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 74
    return-void
.end method
