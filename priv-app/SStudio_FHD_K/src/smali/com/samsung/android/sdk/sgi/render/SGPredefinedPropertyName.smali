.class public final Lcom/samsung/android/sdk/sgi/render/SGPredefinedPropertyName;
.super Ljava/lang/Object;
.source "SGPredefinedPropertyName.java"


# static fields
.field public static final AlphaBlend:Ljava/lang/String; = "SGAlphaBlend"

.field public static final Color:Ljava/lang/String; = "SGColor"

.field public static final Normals:Ljava/lang/String; = "SGNormals"

.field public static final Opacity:Ljava/lang/String; = "SGOpacity"

.field public static final Positions:Ljava/lang/String; = "SGPositions"

.field public static final Program:Ljava/lang/String; = "SGProgram"

.field public static final Texture:Ljava/lang/String; = "SGTexture"

.field public static final TextureCoords:Ljava/lang/String; = "SGTextureCoords"

.field public static final TextureRect:Ljava/lang/String; = "SGTextureRect"

.field public static final View:Ljava/lang/String; = "SGView"

.field public static final Viewport:Ljava/lang/String; = "SGViewport"

.field public static final World:Ljava/lang/String; = "SGWorld"

.field public static final WorldInverseTranspose:Ljava/lang/String; = "SGWorldInverseTranspose"

.field public static final WorldScale:Ljava/lang/String; = "SGWorldScale"

.field public static final WorldViewInverse:Ljava/lang/String; = "SGWorldViewInverse"

.field public static final WorldViewProjection:Ljava/lang/String; = "SGWorldViewProjection"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
