.class public Lcom/samsung/android/sdk/sgi/render/SGGeometry;
.super Ljava/lang/Object;
.source "SGGeometry.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGGeometry__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(JZ)V

    .line 66
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V
    .locals 3

    .prologue
    .line 113
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGBuffer;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGGeometry__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(JZ)V

    .line 114
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;)V
    .locals 3

    .prologue
    .line 122
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->SwigConstructSGGeometry(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(JZ)V

    .line 123
    return-void
.end method

.method private static SwigConstructSGGeometry(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;)J
    .locals 2

    .prologue
    .line 117
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->ordinal()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGGeometry__SWIG_2(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/render/SGGeometry;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getHandle(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGGeometry;)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 7

    .prologue
    .line 108
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry___assign__(JLcom/samsung/android/sdk/sgi/render/SGGeometry;JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(JZ)V

    return-object v6
.end method

.method public addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V
    .locals 7

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGBuffer;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_addBuffer(JLcom/samsung/android/sdk/sgi/render/SGGeometry;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 105
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 49
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    if-nez v1, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->getHandle()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->getHandle()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGGeometry(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getBoundingBox()Lcom/samsung/android/sdk/sgi/base/SGBox3f;
    .locals 4

    .prologue
    .line 100
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getBoundingBox(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGBox3f;-><init>(JZ)V

    return-object v0
.end method

.method public getBufferName(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getBufferName(JLcom/samsung/android/sdk/sgi/render/SGGeometry;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBuffersCount()I
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getBuffersCount(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)I

    move-result v0

    return v0
.end method

.method public getIndexBuffer()Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    .locals 5

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getIndexBuffer(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v1

    .line 131
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(JZ)V

    goto :goto_0
.end method

.method public getPrimitiveType()Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;
    .locals 3

    .prologue
    .line 92
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getPrimitiveType(JLcom/samsung/android/sdk/sgi/render/SGGeometry;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getVertexBuffer(I)Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .locals 4

    .prologue
    .line 80
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getVertexBuffer__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGGeometry;I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(JZ)V

    return-object v0
.end method

.method public getVertexBuffer(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .locals 4

    .prologue
    .line 76
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {v1, v2, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_getVertexBuffer__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGGeometry;Ljava/lang/String;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(JZ)V

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->getHandle()J

    move-result-wide v0

    .line 57
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 58
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 60
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public init(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;)V
    .locals 3

    .prologue
    .line 69
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 71
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_init(JLcom/samsung/android/sdk/sgi/render/SGGeometry;I)V

    .line 73
    return-void
.end method

.method public setBoundingBox(Lcom/samsung/android/sdk/sgi/base/SGBox3f;)V
    .locals 6

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGBox3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGGeometry_setBoundingBox(JLcom/samsung/android/sdk/sgi/render/SGGeometry;JLcom/samsung/android/sdk/sgi/base/SGBox3f;)V

    .line 97
    return-void
.end method
