.class public final Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;
.source "SGStringShaderProperty.java"


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGStringShaderProperty__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(JZ)V

    .line 41
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 60
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->SwigConstructSGStringShaderProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(JZ)V

    .line 61
    return-void
.end method

.method private static SwigConstructSGStringShaderProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 55
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->ordinal()I

    move-result v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGStringShaderProperty__SWIG_1(ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private init(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2, p2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStringShaderProperty_init(JLcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;ILjava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;)Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    .locals 7

    .prologue
    .line 51
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStringShaderProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGStringShaderProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method
