.class public Lcom/samsung/android/sdk/sgi/base/SGVector3f;
.super Ljava/lang/Object;
.source "SGVector3f.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector3f__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector3f__SWIG_2(FFF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    .line 55
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 3

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector3f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public static createCrossed(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 7

    .prologue
    .line 118
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_createCrossed(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v6
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_add(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 127
    return-void
.end method

.method public crossProduct(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_crossProduct(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 115
    return-void
.end method

.method public divide(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_divide(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 139
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGVector3f(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getDistance(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)F
    .locals 6

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F

    move-result v0

    return v0
.end method

.method public getDotProduct(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)F
    .locals 6

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F

    move-result v0

    return v0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F

    move-result v0

    return v0
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_getX(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_getY(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F

    move-result v0

    return v0
.end method

.method public getZ()F
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_getZ(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F

    move-result v0

    return v0
.end method

.method public interpolate(Lcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
    .locals 7

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_interpolate(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 111
    return-void
.end method

.method public inverse()V
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_inverse(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 103
    return-void
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGVector3f;F)Z
    .locals 7

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)Z

    move-result v0

    return v0
.end method

.method public multiply(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 135
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 99
    return-void
.end method

.method public scale(F)V
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_scale(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 107
    return-void
.end method

.method public set(FFF)V
    .locals 6

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_set(JLcom/samsung/android/sdk/sgi/base/SGVector3f;FFF)V

    .line 83
    return-void
.end method

.method public setX(F)V
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_setX(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 63
    return-void
.end method

.method public setY(F)V
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_setY(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 71
    return-void
.end method

.method public setZ(F)V
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_setZ(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 79
    return-void
.end method

.method public subtract(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector3f_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 131
    return-void
.end method
