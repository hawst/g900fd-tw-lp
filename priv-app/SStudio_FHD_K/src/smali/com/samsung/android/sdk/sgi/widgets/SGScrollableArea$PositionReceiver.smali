.class Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;
.super Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;
.source "SGScrollableArea.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositionReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)V
    .locals 0

    .prologue
    .line 997
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$1;)V
    .locals 0

    .prologue
    .line 997
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)V

    return-void
.end method


# virtual methods
.method public onPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 3

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    # getter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->mIsAnimating:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->access$100(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea$PositionReceiver;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FF)V

    .line 1002
    :cond_0
    return-void
.end method
