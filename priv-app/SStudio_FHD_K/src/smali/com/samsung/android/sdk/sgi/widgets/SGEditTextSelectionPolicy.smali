.class interface abstract Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;
.super Ljava/lang/Object;
.source "SGEditTextSelectionPointerController.java"


# virtual methods
.method public abstract getEditTextDelegate()Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;
.end method

.method public abstract getSelectionPointerContainer()Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.end method

.method public abstract isSelectionEnabled()Z
.end method

.method public abstract isSelectionPointersEmptySelection()Z
.end method

.method public abstract restoreSelectionState()V
.end method

.method public abstract saveSelectionState()V
.end method

.method public abstract setCursorPointerDrawable(I)V
.end method

.method public abstract setCursorPointerDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setEditTextDelegate(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V
.end method

.method public abstract setLeftSelectionPointerDrawable(I)V
.end method

.method public abstract setLeftSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setPointersContainer(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public abstract setPointersInversionEnabled(Z)V
.end method

.method public abstract setPointersSize(FF)V
.end method

.method public abstract setRightSelectionPointerDrawable(I)V
.end method

.method public abstract setRightSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setSelectionEnabled(Z)V
.end method

.method public abstract setSelectionPointerList(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract showCursorGlyph(Z)V
.end method

.method public abstract showSelectionPointers(Z)V
.end method
