.class public Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;
.source "SGWidgetCamera.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$2;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$Drawer;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;
    }
.end annotation


# instance fields
.field private frames:Ljava/util/concurrent/atomic/AtomicInteger;

.field private imageRectTransform:[F

.field private isCameraOK:Z

.field private mCamera:Landroid/hardware/Camera;

.field private mCurrentCamera:I

.field private mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

.field private mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;

.field private mSetBufferSizeMethod:Ljava/lang/reflect/Method;

.field private mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTextureView:Landroid/view/TextureView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/view/TextureView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 215
    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 60
    iput-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->isCameraOK:Z

    .line 62
    iput v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCurrentCamera:I

    .line 90
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;

    .line 216
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    if-gt p3, v0, :cond_0

    if-gez p3, :cond_1

    .line 217
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid Camera ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_1
    iput p3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCurrentCamera:I

    .line 219
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->obtainCallMethods()V

    .line 220
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->setTextureView(Landroid/view/TextureView;)V

    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 222
    iget v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCurrentCamera:I

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    .line 223
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->onUpdate()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private obtainCallMethods()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 303
    :try_start_0
    const-class v0, Landroid/view/TextureView;

    const-string v1, "setSurfaceTexture"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/graphics/SurfaceTexture;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    .line 304
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    .line 305
    const-class v1, Landroid/graphics/SurfaceTexture;

    const-string v2, "setDefaultBufferSize"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 308
    iput-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    .line 309
    iput-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    .line 310
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "API level 16 is minimal for using this class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onUpdate()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 282
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->imageRectTransform:[F

    invoke-virtual {v0, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 285
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    move v2, v1

    .line 286
    :goto_0
    if-ge v2, v8, :cond_1

    move v0, v1

    .line 287
    :goto_1
    if-ge v0, v8, :cond_0

    .line 288
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->imageRectTransform:[F

    mul-int/lit8 v5, v2, 0x4

    add-int/2addr v5, v0

    aget v4, v4, v5

    invoke-virtual {v3, v0, v2, v4}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IIF)V

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 286
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 290
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0, v6, v6, v6, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 291
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v0

    .line 292
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v1, v7, v7, v6, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 293
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v1

    .line 295
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v4

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v1

    invoke-direct {v2, v3, v0, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->setContentRect(Landroid/graphics/RectF;)V

    .line 296
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->invalidate()V

    .line 297
    return-void
.end method

.method private declared-synchronized resizeSurfaceTexture(II)V
    .locals 5

    .prologue
    .line 316
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 320
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    .line 322
    if-eqz v0, :cond_0

    .line 323
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1, p1, p2}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 324
    :catch_0
    move-exception v0

    .line 326
    :try_start_2
    const-string v1, "SGSurfaceRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on setDefaultBufferSize into SurfaceTexture, - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 327
    :catch_1
    move-exception v0

    .line 329
    :try_start_3
    const-string v1, "SGSurfaceRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on setDefaultBufferSize into SurfaceTexture, - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic finalize()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->finalize()V

    return-void
.end method

.method public final getCamera()Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public bridge synthetic getContentRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invalidate()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate()V

    return-void
.end method

.method public bridge synthetic invalidate(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate(Landroid/graphics/RectF;)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 191
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->isCameraOK:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->releaseCamera()V

    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 269
    invoke-static {p1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->createMotionEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 270
    if-eqz v1, :cond_0

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 273
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 277
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseCamera()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->isCameraOK:Z

    .line 162
    :cond_0
    return-void
.end method

.method public setActiveCamera(I)V
    .locals 2

    .prologue
    .line 171
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 172
    iget v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCurrentCamera:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    rem-int p1, v0, v1

    .line 173
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCurrentCamera:I

    if-ne p1, v0, :cond_1

    .line 185
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 177
    invoke-static {p1}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 181
    iput p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mCurrentCamera:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public bridge synthetic setContentRect(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRect(Landroid/graphics/RectF;)V

    return-void
.end method

.method public bridge synthetic setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    return-void
.end method

.method public bridge synthetic setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    return-void
.end method

.method public bridge synthetic setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    return-void
.end method

.method public bridge synthetic setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V

    return-void
.end method

.method public setSize(FF)V
    .locals 2

    .prologue
    .line 233
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setSize(FF)V

    .line 234
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->resizeSurfaceTexture(II)V

    .line 235
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 244
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 245
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->resizeSurfaceTexture(II)V

    .line 246
    return-void
.end method

.method public setTextureView(Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 252
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$Drawer;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$Drawer;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    .line 253
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 254
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    .line 255
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 256
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->imageRectTransform:[F

    .line 258
    :cond_0
    return-void
.end method

.method public setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V
    .locals 2

    .prologue
    .line 335
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    .line 336
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$2;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGWidgetVisibility:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 338
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_1

    .line 341
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->invalidate()V

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$OnFrameAvailableListenerImpl;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    goto :goto_0

    .line 347
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera$1;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    goto :goto_0

    .line 357
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCamera;->mTextureView:Landroid/view/TextureView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    goto :goto_0

    .line 336
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
