.class public Lcom/samsung/android/sdk/sgi/base/SGRegistrator;
.super Ljava/lang/Object;
.source "SGRegistrator.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 66
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGRegistrator()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;-><init>(JZ)V

    .line 67
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGRegistrator_director_connect(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;JZZ)V

    .line 68
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    .line 28
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public AddToManagementList(J)Z
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGRegistrator_AddToManagementList(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z

    move-result v0

    return v0
.end method

.method public Deregister(J)Z
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGRegistrator_Deregister(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z

    move-result v0

    return v0
.end method

.method public GetObjectByPointer(J)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGRegistrator_GetObjectByPointer(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public Register(Ljava/lang/Object;J)Z
    .locals 6

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGRegistrator_Register(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;Ljava/lang/Object;J)Z

    move-result v0

    return v0
.end method

.method public RemoveFromManagementList(J)Z
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGRegistrator_RemoveFromManagementList(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGRegistrator(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method
