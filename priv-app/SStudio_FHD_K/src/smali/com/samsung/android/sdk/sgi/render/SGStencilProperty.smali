.class public Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGStencilProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGStencilProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;-><init>(JZ)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->init()V

    .line 42
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_init(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)V

    .line 46
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;)Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;
    .locals 7

    .prologue
    .line 105
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGStencilProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getFunction()Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;
    .locals 3

    .prologue
    .line 57
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getFunction(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getGlobalMask()I
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getGlobalMask(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v0

    return v0
.end method

.method public getMask()I
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getMask(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v0

    return v0
.end method

.method public getOperationDepthFail()Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;
    .locals 3

    .prologue
    .line 73
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getOperationDepthFail(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getOperationDepthPass()Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;
    .locals 3

    .prologue
    .line 77
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getOperationDepthPass(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getOperationStencilFail()Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;
    .locals 3

    .prologue
    .line 69
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getOperationStencilFail(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getReference()I
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_getReference(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)I

    move-result v0

    return v0
.end method

.method public isStencilPropertyEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_isStencilPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;)Z

    move-result v0

    return v0
.end method

.method public setFunction(Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;II)V
    .locals 6

    .prologue
    .line 89
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 91
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->ordinal()I

    move-result v3

    move-object v2, p0

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_setFunction(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;III)V

    .line 93
    return-void
.end method

.method public setGlobalMask(I)V
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_setGlobalMask(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;I)V

    .line 86
    return-void
.end method

.method public setStencilOperation(Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;)V
    .locals 6

    .prologue
    .line 96
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 97
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 98
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 100
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;->ordinal()I

    move-result v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;->ordinal()I

    move-result v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/render/SGStencilOperation;->ordinal()I

    move-result v5

    move-object v2, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_setStencilOperation(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;III)V

    .line 102
    return-void
.end method

.method public setStencilPropertyEnabled(Z)V
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGStencilProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGStencilProperty_setStencilPropertyEnabled(JLcom/samsung/android/sdk/sgi/render/SGStencilProperty;Z)V

    .line 82
    return-void
.end method
