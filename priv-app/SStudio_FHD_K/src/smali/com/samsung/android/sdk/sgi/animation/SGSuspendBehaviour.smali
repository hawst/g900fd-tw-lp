.class public final enum Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;
.super Ljava/lang/Enum;
.source "SGSuspendBehaviour.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

.field public static final enum CANCEL:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

.field public static final enum FINISH:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

.field public static final enum NONE:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

.field public static final enum PAUSE:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->NONE:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->PAUSE:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->CANCEL:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    const-string v1, "FINISH"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->FINISH:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->NONE:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->PAUSE:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->CANCEL:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->FINISH:Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    return-object v0
.end method
