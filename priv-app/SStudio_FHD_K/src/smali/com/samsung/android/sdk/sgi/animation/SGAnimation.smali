.class public Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
.super Ljava/lang/Object;
.source "SGAnimation.java"


# instance fields
.field protected swigCMemOwn:Z

.field protected swigCPtr:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCMemOwn:Z

    .line 28
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    .line 29
    return-void
.end method

.method private getAnimationListenerNative()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;
    .locals 2

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getAnimationListenerNative(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    move-result-object v0

    return-object v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J
    .locals 2

    .prologue
    .line 32
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getHandle(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v0

    return-wide v0
.end method

.method private setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V
    .locals 6

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setAnimationListener(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    .line 159
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 50
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;

    if-nez v1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getHandle()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getHandle()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 36
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCMemOwn:Z

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGAnimation(J)V

    .line 41
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    .line 43
    :cond_1
    return-void
.end method

.method public getAnimationListener()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getAnimationListenerNative()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->getInterface()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDirection()Lcom/samsung/android/sdk/sgi/animation/SGAnimationDirectionType;
    .locals 3

    .prologue
    .line 135
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationDirectionType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGAnimationDirectionType;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getDirection(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public getFillAfterMode()Lcom/samsung/android/sdk/sgi/animation/SGFillAfterMode;
    .locals 3

    .prologue
    .line 154
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGFillAfterMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGFillAfterMode;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getFillAfterMode(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getOffset()I
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public getOnSuspendBehaviour()Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;
    .locals 3

    .prologue
    .line 177
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;

    iget-wide v1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v1, v2, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getRepeatCount()I
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getRepeatCount(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public getTimingFunction()Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
    .locals 2

    .prologue
    .line 197
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;

    move-result-object v0

    return-object v0
.end method

.method public getValueInterpolator()Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_getValueInterpolator(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getHandle()J

    move-result-wide v0

    .line 58
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 59
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public isAutoReverseEnabled()Z
    .locals 2

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_isAutoReverseEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Z

    move-result v0

    return v0
.end method

.method public isDeferredStartEnabled()Z
    .locals 2

    .prologue
    .line 166
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_isDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Z

    move-result v0

    return v0
.end method

.method public isFillBeforeEnabled()Z
    .locals 2

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_isFillBeforeEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Z

    move-result v0

    return v0
.end method

.method public setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getAnimationListenerNative()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;->setInterface(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 82
    :goto_0
    return-void

    .line 79
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 80
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V

    goto :goto_0
.end method

.method public setAutoReverseEnabled(Z)V
    .locals 2

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setAutoReverseEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;Z)V

    .line 121
    return-void
.end method

.method public setDeferredStartEnabled(Z)V
    .locals 2

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;Z)V

    .line 163
    return-void
.end method

.method public setDirection(Lcom/samsung/android/sdk/sgi/animation/SGAnimationDirectionType;)V
    .locals 3

    .prologue
    .line 128
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 130
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationDirectionType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setDirection(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V

    .line 132
    return-void
.end method

.method public setDuration(I)V
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V

    .line 97
    return-void
.end method

.method public setFillAfterMode(Lcom/samsung/android/sdk/sgi/animation/SGFillAfterMode;)V
    .locals 3

    .prologue
    .line 147
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 149
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/animation/SGFillAfterMode;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setFillAfterMode(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V

    .line 151
    return-void
.end method

.method public setFillBeforeEnabled(Z)V
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setFillBeforeEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;Z)V

    .line 140
    return-void
.end method

.method public setOffset(I)V
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V

    .line 105
    return-void
.end method

.method public setOnSuspendBehaviour(Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;)V
    .locals 3

    .prologue
    .line 170
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 172
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/animation/SGSuspendBehaviour;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V

    .line 174
    return-void
.end method

.method public setRepeatCount(I)V
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setRepeatCount(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V

    .line 113
    return-void
.end method

.method public setTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
    .locals 6

    .prologue
    .line 185
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V

    .line 186
    return-void
.end method

.method public setValueInterpolator(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)V
    .locals 6

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimation_setValueInterpolator(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)V

    .line 182
    return-void
.end method
