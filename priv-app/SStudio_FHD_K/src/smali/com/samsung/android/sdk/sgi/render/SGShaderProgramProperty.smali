.class public Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGShaderProgramProperty.java"


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGShaderProgramProperty__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(JZ)V

    .line 41
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V
    .locals 6

    .prologue
    .line 59
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v0

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGShaderProgramProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(JZ)V

    .line 60
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 7

    .prologue
    .line 55
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGShaderProgramProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGShaderProgramProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getShader(Lcom/samsung/android/sdk/sgi/render/SGShaderType;)Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;
    .locals 3

    .prologue
    .line 48
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGShaderProgramProperty_getShader(JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;I)Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V
    .locals 9

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGShaderProgramProperty_init(JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;JLcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    .line 45
    return-void
.end method
