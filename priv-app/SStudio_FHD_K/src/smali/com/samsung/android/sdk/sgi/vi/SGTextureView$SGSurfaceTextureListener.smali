.class Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;
.super Ljava/lang/Object;
.source "SGTextureView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/vi/SGTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SGSurfaceTextureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$102(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;Z)Z

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceAvailable(Landroid/view/Surface;)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceChanged(II)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setResumed(Z)V

    .line 218
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setSuspended(Z)V

    .line 226
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceDestroyed()V

    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;->this$0:Lcom/samsung/android/sdk/sgi/vi/SGTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->surfaceChanged(II)V

    .line 222
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 231
    return-void
.end method
