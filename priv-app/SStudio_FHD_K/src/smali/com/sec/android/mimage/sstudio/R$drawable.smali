.class public final Lcom/sec/android/mimage/sstudio/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/sstudio/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_bg:I = 0x7f020000

.field public static final action_bar_icon_back:I = 0x7f020001

.field public static final action_bar_icon_back_dim:I = 0x7f020002

.field public static final action_bar_icon_back_focus:I = 0x7f020003

.field public static final action_bar_icon_back_lmr:I = 0x7f020004

.field public static final action_bar_icon_back_press:I = 0x7f020005

.field public static final action_bar_icon_back_press_lmr:I = 0x7f020006

.field public static final actionbar_text_color:I = 0x7f020007

.field public static final actioon_bar_back_btn:I = 0x7f020008

.field public static final actioon_bar_back_btn_lmr:I = 0x7f020009

.field public static final background_lmr:I = 0x7f02000a

.field public static final background_main_button:I = 0x7f02000b

.field public static final blue:I = 0x7f020051

.field public static final collagestudio:I = 0x7f02000c

.field public static final collagestudio_land:I = 0x7f02000d

.field public static final lmr_collagestudio_land:I = 0x7f02000e

.field public static final lmr_magicshotstudio_land:I = 0x7f02000f

.field public static final lmr_photostudio_land:I = 0x7f020010

.field public static final lmr_videoclipstudio_land:I = 0x7f020011

.field public static final lmr_videotrimstudio_land:I = 0x7f020012

.field public static final magicshotstudio:I = 0x7f020013

.field public static final magicshotstudio_land:I = 0x7f020014

.field public static final photo_quick_button:I = 0x7f020015

.field public static final photo_quick_button_frame:I = 0x7f020016

.field public static final photo_quick_button_frame_land:I = 0x7f020017

.field public static final photo_quick_button_frame_press:I = 0x7f020018

.field public static final photo_quick_button_frame_press_land:I = 0x7f020019

.field public static final photo_quick_button_land:I = 0x7f02001a

.field public static final photo_quick_button_mask:I = 0x7f02001b

.field public static final photo_quick_button_mask_land:I = 0x7f02001c

.field public static final photostudio:I = 0x7f02001d

.field public static final photostudio_land:I = 0x7f02001e

.field public static final s_main_icon_collage:I = 0x7f02001f

.field public static final s_main_icon_collage_dim:I = 0x7f020020

.field public static final s_main_icon_collage_dim_lmr:I = 0x7f020021

.field public static final s_main_icon_collage_l:I = 0x7f020022

.field public static final s_main_icon_collage_l_dim:I = 0x7f020023

.field public static final s_main_icon_collage_lmr:I = 0x7f020024

.field public static final s_main_icon_magic:I = 0x7f020025

.field public static final s_main_icon_magic_dim:I = 0x7f020026

.field public static final s_main_icon_magic_dim_lmr:I = 0x7f020027

.field public static final s_main_icon_magic_l:I = 0x7f020028

.field public static final s_main_icon_magic_l_dim:I = 0x7f020029

.field public static final s_main_icon_magic_lmr:I = 0x7f02002a

.field public static final s_main_icon_photo:I = 0x7f02002b

.field public static final s_main_icon_photo_dim:I = 0x7f02002c

.field public static final s_main_icon_photo_dim_lmr:I = 0x7f02002d

.field public static final s_main_icon_photo_l:I = 0x7f02002e

.field public static final s_main_icon_photo_l_dim:I = 0x7f02002f

.field public static final s_main_icon_photo_lmr:I = 0x7f020030

.field public static final s_main_icon_video_clip:I = 0x7f020031

.field public static final s_main_icon_video_clip_dim:I = 0x7f020032

.field public static final s_main_icon_video_clip_dim_lmr:I = 0x7f020033

.field public static final s_main_icon_video_clip_l:I = 0x7f020034

.field public static final s_main_icon_video_clip_l_dim:I = 0x7f020035

.field public static final s_main_icon_video_clip_lmr:I = 0x7f020036

.field public static final s_main_icon_video_trim:I = 0x7f020037

.field public static final s_main_icon_video_trim_dim:I = 0x7f020038

.field public static final s_main_icon_video_trim_dim_lmr:I = 0x7f020039

.field public static final s_main_icon_video_trim_l:I = 0x7f02003a

.field public static final s_main_icon_video_trim_l_dim:I = 0x7f02003b

.field public static final s_main_icon_video_trim_lmr:I = 0x7f02003c

.field public static final s_main_list_bg:I = 0x7f02003d

.field public static final s_main_list_bg_dim:I = 0x7f02003e

.field public static final s_main_list_bg_focus:I = 0x7f02003f

.field public static final s_main_list_bg_press:I = 0x7f020040

.field public static final s_studio_main_default:I = 0x7f020041

.field public static final s_studio_main_default_land:I = 0x7f020042

.field public static final s_studio_main_default_pattern_bg:I = 0x7f020043

.field public static final s_studio_main_default_pattern_bg_land:I = 0x7f020044

.field public static final s_studio_main_default_pattern_bg_land_lmr:I = 0x7f020045

.field public static final s_studio_main_default_pattern_bg_lmr:I = 0x7f020046

.field public static final s_studio_main_icon:I = 0x7f020047

.field public static final selection_image_badge:I = 0x7f020048

.field public static final selection_image_frame_1:I = 0x7f020049

.field public static final selection_image_frame_2:I = 0x7f02004a

.field public static final selection_image_frame_3:I = 0x7f02004b

.field public static final selection_image_mask:I = 0x7f02004c

.field public static final semitransparent_white:I = 0x7f020052

.field public static final videoclipstudio:I = 0x7f02004d

.field public static final videoclipstudio_land:I = 0x7f02004e

.field public static final videotrimstudio:I = 0x7f02004f

.field public static final videotrimstudio_land:I = 0x7f020050


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
