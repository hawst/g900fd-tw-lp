.class public Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;
.super Lcom/samsung/android/sdk/sgi/vi/SGTextureView;
.source "SGTextureViewBlurred.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDuration:I

.field private mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

.field private mFrameCount:I

.field private mHandler:Landroid/os/Handler;

.field private mIsChangedBitmap:Z

.field private mIsNeedsUpdateBlurr:Z

.field private mIsPaused:Z

.field private mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

.field private mRunnable:Ljava/lang/Runnable;

.field private mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

.field private mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;Lcom/sec/android/mimage/blureffect/SGListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "configuration"    # Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    .param p3, "sgListener"    # Lcom/sec/android/mimage/blureffect/SGListener;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    .line 16
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    .line 17
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    .line 18
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 22
    iput-boolean v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    .line 24
    iput-boolean v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    .line 31
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    .line 32
    iput v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    .line 34
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    .line 35
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    .line 40
    iput-object p3, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    .line 72
    new-instance v0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;-><init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    .line 88
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    .line 89
    new-instance v0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$2;-><init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->startAnimationOnResume()V

    .line 100
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;Z)V
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/GaussianLayer;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/FakeLayer;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    return v0
.end method


# virtual methods
.method public hideViews()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    const-string v0, "hideViews"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setVisibility(Z)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setVisibility(Z)V

    .line 205
    :cond_0
    return-void
.end method

.method public onChangesDrawn()V
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/blureffect/SGListener;->onDrawFinished()V

    .line 174
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->invalidate()V

    .line 175
    return-void
.end method

.method public onFrameEnd()V
    .locals 2

    .prologue
    .line 179
    iget v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Frame Count "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 181
    iget v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/blureffect/SGListener;->onFrameEnd()V

    .line 186
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 208
    iput-boolean v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setVisibility(Z)V

    .line 212
    :cond_0
    return-void
.end method

.method public onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 3
    .param p1, "size"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "----- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setSize(FF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setSize(FF)V

    .line 150
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    if-eqz v0, :cond_1

    .line 151
    const-string v0, "mIsPaused is true"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->startBlur()V

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_1
    const-string v0, "mIsPaused is false"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDuration(I)V
    .locals 1
    .param p1, "duration"    # I

    .prologue
    .line 215
    if-gtz p1, :cond_0

    .line 216
    const-string v0, "Duration is invalid value!"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 221
    :goto_0
    return-void

    .line 220
    :cond_0
    iput p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 103
    const-string v0, "setImageBitmap"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/sec/android/mimage/blureffect/GaussianLayer;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/blureffect/GaussianLayer;-><init>(F)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 112
    new-instance v0, Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-direct {v0}, Lcom/sec/android/mimage/blureffect/FakeLayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 116
    :cond_0
    if-eqz p1, :cond_3

    .line 117
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 123
    :cond_1
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 124
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 130
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    if-eqz v0, :cond_3

    .line 134
    const-string v0, "before SGSurfaceStateListener"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V

    .line 138
    :cond_3
    return-void
.end method

.method public startAnimationOnResume()V
    .locals 4

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;)V

    .line 226
    const-string v0, "Runnable postAtTime"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 229
    return-void
.end method

.method public startBlur()V
    .locals 2

    .prologue
    .line 189
    const-string v0, "startBlur"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 191
    const-string v0, "mFakeLayer is not null"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    iget v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->startBlur(I)V

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    const-string v0, "mFakeLayer is null"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    goto :goto_0
.end method
