.class Lcom/sec/android/mimage/sstudio/StudioActivity$4;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$4;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    .line 1522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 1525
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$4;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$4;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1526
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1527
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1528
    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1529
    const-string v0, "multi-pick"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1530
    const-string v0, "*/*"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1531
    const-string v0, "pick-min-item"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1532
    const-string v0, "pick-max-item"

    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1533
    const-string v0, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1534
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$4;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    const/16 v2, 0x60

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$4;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoClipStudioInstall:Z
    invoke-static {v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$9(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z

    move-result v3

    const-string v4, "samsungapps://ProductDetail/com.sec.android.app.storycam"

    .line 1535
    const v5, 0x7f060183

    .line 1534
    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$4(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Intent;IZLjava/lang/String;I)V

    .line 1537
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
