.class Lcom/sec/android/mimage/sstudio/StudioActivity$12;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$12;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    .line 1659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1663
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 1664
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$12;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$19(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/high16 v1, -0x4d000000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 1671
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1665
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1666
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$12;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$19(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    .line 1667
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1668
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$12;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$20(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1669
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$12;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$19(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method
