.class Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StudioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 2352
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 2355
    if-eqz p2, :cond_0

    .line 2356
    const-string v2, "Debug"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BROADCAST RECEIVED: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2357
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2358
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 2360
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 2368
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 2361
    .restart local v1    # "i":I
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$1(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2362
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2363
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->finish()V

    .line 2360
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
