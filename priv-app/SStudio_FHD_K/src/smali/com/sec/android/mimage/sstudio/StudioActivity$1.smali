.class Lcom/sec/android/mimage/sstudio/StudioActivity$1;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$1;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    .line 1476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$1;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$1;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1480
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1481
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1482
    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1483
    const-string v0, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1484
    const-string v0, "image/*"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1485
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$1;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    const/16 v2, 0x64

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$1;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mBPhotoEditorInstall:Z
    invoke-static {v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$3(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z

    move-result v3

    const-string v4, "samsungapps://ProductDetail/com.sec.android.mimage.photoretouching"

    .line 1486
    const v5, 0x7f060181

    .line 1485
    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$4(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Intent;IZLjava/lang/String;I)V

    .line 1488
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
