.class Lcom/sec/android/mimage/sstudio/StudioActivity$11;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$11;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    .line 1636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 1640
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$11;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$11;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->isPressed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1641
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1642
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1644
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1645
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1646
    const-string v1, "multi-pick"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1647
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$11;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I
    invoke-static {v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$18(Lcom/sec/android/mimage/sstudio/StudioActivity;)I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$11;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I
    invoke-static {v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$18(Lcom/sec/android/mimage/sstudio/StudioActivity;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1648
    :cond_0
    const-string v1, "pick-max-item"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1653
    :goto_0
    const-string v1, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1654
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1655
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$11;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    const/16 v2, 0x5d

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1657
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void

    .line 1650
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_2
    const-string v1, "pick-max-item"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method
