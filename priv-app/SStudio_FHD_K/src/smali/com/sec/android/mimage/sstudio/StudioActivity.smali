.class public Lcom/sec/android/mimage/sstudio/StudioActivity;
.super Landroid/app/Activity;
.source "StudioActivity.java"

# interfaces
.implements Lcom/sec/android/mimage/blureffect/SGListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

.field public static final DIALOG_DEFAULT_TEXT_COLOR_LIGHT_THEME:I = -0x1000000

.field static final MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final BROKEN_IMAGE:I

.field private final CENTER_THUMBNAIL_SIZE:I

.field private final COLLAGE_STUDIO_PACKAGE_NAME:Ljava/lang/String;

.field private final Collage:I

.field private final DEVICE_NAME:Ljava/lang/String;

.field private final EXTERNAL_STORAGE:Ljava/lang/String;

.field private final GALLERY_CLASS_NAME:Ljava/lang/String;

.field private final GALLERY_PKG_NAME:Ljava/lang/String;

.field private final MAGIC_SHOT:I

.field private final MULTI_IMAGE:I

.field private final MagicShot:I

.field private final NO_IMAGE_DEVICE:I

.field private final NO_SELECT_IMAGE:I

.field private final ONE_IMAGE:I

.field private final ONE_VIDEO:I

.field private final PHOTOEDITOR_CLASS_NAME:Ljava/lang/String;

.field private final PHOTOEDITOR_PACKAGE_NAME:Ljava/lang/String;

.field private final PHOTOEDITOR_URL:Ljava/lang/String;

.field private final PhotoEditor:I

.field private final RETURN_FROM_COLLAGE:I

.field private final RETURN_FROM_OTHERS:I

.field private final RE_SSTUDIO:I

.field private final SAVE_DIR:Ljava/lang/String;

.field private final SHOT_MORE_STUDIO_CLASS_NAME:Ljava/lang/String;

.field private final SHOT_MORE_STUDIO_PACKAGE_NAME:Ljava/lang/String;

.field private final SHOT_MORE_URL:Ljava/lang/String;

.field private final SIZE:I

.field private final THUMBNAIL_SIZE:I

.field private final TOO_MANY_IMAGE:I

.field private final VIDEOCLIP_URL:Ljava/lang/String;

.field private final VIDEOTRIM_URL:Ljava/lang/String;

.field private final VIDEO_CLIP:I

.field private final VIDEO_CLIP_STUDIO_CLASS_NAME:Ljava/lang/String;

.field private final VIDEO_CLIP_STUDIO_PACKAGE_NAME:Ljava/lang/String;

.field private final VIDEO_TRIM_STUDIO_PACKAGE_NAME:Ljava/lang/String;

.field private final VideoClip:I

.field private final VideoTrim:I

.field private bMagicShow:Ljava/lang/String;

.field private editor:Landroid/content/SharedPreferences$Editor;

.field private isFailureDecode:Z

.field private isFirstVideo:Z

.field private isNoSelectPic:Z

.field private mActionBarLinear:Landroid/widget/LinearLayout;

.field private mActionBarTV:Landroid/widget/TextView;

.field private mBCollageStudioInstall:Z

.field private mBPhotoEditorInstall:Z

.field private mBShotMoreStudioInstall:Z

.field private mBVideoClipStudioInstall:Z

.field private mBVideoTrimStudioInstall:Z

.field private mBitmap_masking:Landroid/graphics/Bitmap;

.field private mBitmap_masking_2:Landroid/graphics/Bitmap;

.field private mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

.field private mCollageStudioImg:Landroid/widget/ImageView;

.field private mCollageStudioLayout:Landroid/widget/LinearLayout;

.field private mCollageStudioTV:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mCountBackgroundImage:Landroid/widget/ImageView;

.field private mFrameBitmap:Landroid/graphics/Bitmap;

.field private mImageCount:I

.field private mMagicShotImg:Landroid/widget/ImageView;

.field private mMagicShotStudioLayout:Landroid/widget/LinearLayout;

.field private mMagicShotStudioTV:Landroid/widget/TextView;

.field private mMagicshotCount:I

.field private mMainBitmap:Landroid/graphics/Bitmap;

.field private mMainImage:Landroid/widget/ImageView;

.field private mMode:I

.field private mPatternImg:Landroid/widget/ImageView;

.field private mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

.field private mPhotoRightImageFrame:Landroid/widget/ImageView;

.field private mPhotoStudioImg:Landroid/widget/ImageView;

.field private mPhotoStudioLayout:Landroid/widget/LinearLayout;

.field private mPhotoStudioNextImage:Landroid/widget/ImageView;

.field private mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

.field private mPhotoStudioTV:Landroid/widget/TextView;

.field private mReceiver:Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;

.field private mRecentUri:Landroid/net/Uri;

.field private mSStudioSubTV:Landroid/widget/TextView;

.field private mSStudioTV:Landroid/widget/TextView;

.field private mTextViewCnt:Landroid/widget/TextView;

.field private mThumbNailBitmap:Landroid/graphics/Bitmap;

.field private mThumbNailFrame:Landroid/widget/ImageView;

.field private mThumbNailImage:Landroid/widget/ImageView;

.field private mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mUriOnPause:Landroid/net/Uri;

.field private mVideoClipImg:Landroid/widget/ImageView;

.field private mVideoClipStudioLayout:Landroid/widget/LinearLayout;

.field private mVideoClipStudioTV:Landroid/widget/TextView;

.field private mVideoCount:I

.field private mVideoFrameBitmap:Landroid/graphics/Bitmap;

.field private mVideoTrimImg:Landroid/widget/ImageView;

.field private mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

.field private mVideoTrimStudioTV:Landroid/widget/TextView;

.field private prefs:Landroid/content/SharedPreferences;

.field private real_sum:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 211
    const-string v0, "#fff5f5f5"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    .line 1873
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1874
    const-string v2, "_data"

    aput-object v2, v0, v1

    .line 1873
    sput-object v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    .line 1874
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 107
    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    .line 111
    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    .line 120
    iput v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    .line 121
    iput v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    .line 122
    iput v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    .line 123
    iput v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    .line 125
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    .line 126
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z

    .line 130
    iput v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->NO_SELECT_IMAGE:I

    .line 131
    iput v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->ONE_IMAGE:I

    .line 132
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->MULTI_IMAGE:I

    .line 133
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->ONE_VIDEO:I

    .line 134
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VIDEO_CLIP:I

    .line 135
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->NO_IMAGE_DEVICE:I

    .line 136
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->MAGIC_SHOT:I

    .line 137
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->TOO_MANY_IMAGE:I

    .line 139
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->BROKEN_IMAGE:I

    .line 141
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->PhotoEditor:I

    .line 142
    const/16 v0, 0x63

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->Collage:I

    .line 144
    const/16 v0, 0x61

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->MagicShot:I

    .line 145
    const/16 v0, 0x60

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VideoClip:I

    .line 146
    const/16 v0, 0x5f

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VideoTrim:I

    .line 148
    const/16 v0, 0x5e

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->RETURN_FROM_OTHERS:I

    .line 149
    const/16 v0, 0x5d

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->RE_SSTUDIO:I

    .line 150
    const/16 v0, 0x5c

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->RETURN_FROM_COLLAGE:I

    .line 152
    const/16 v0, 0x438

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SIZE:I

    .line 153
    const/16 v0, 0xe0

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->THUMBNAIL_SIZE:I

    .line 155
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->CENTER_THUMBNAIL_SIZE:I

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 175
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->EXTERNAL_STORAGE:Ljava/lang/String;

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->EXTERNAL_STORAGE:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Studio"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    .line 183
    const-string v0, "samsungapps://ProductDetail/com.sec.android.mimage.photoretouching"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->PHOTOEDITOR_URL:Ljava/lang/String;

    .line 184
    const-string v0, "samsungapps://ProductDetail/com.arcsoft.magicshotstudio"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SHOT_MORE_URL:Ljava/lang/String;

    .line 185
    const-string v0, "samsungapps://ProductDetail/com.sec.android.app.storycam"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VIDEOCLIP_URL:Ljava/lang/String;

    .line 186
    const-string v0, "samsungapps://ProductDetail/com.lifevibes.trimapp"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VIDEOTRIM_URL:Ljava/lang/String;

    .line 188
    const-string v0, "com.sec.android.mimage.photoretouching"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->PHOTOEDITOR_PACKAGE_NAME:Ljava/lang/String;

    .line 189
    const-string v0, "com.sec.android.mimage.photoretouching.PhotoRetouching"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->PHOTOEDITOR_CLASS_NAME:Ljava/lang/String;

    .line 190
    const-string v0, "com.sec.android.mimage.photoretouching.multigrid"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->COLLAGE_STUDIO_PACKAGE_NAME:Ljava/lang/String;

    .line 191
    const-string v0, "com.arcsoft.magicshotstudio"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SHOT_MORE_STUDIO_PACKAGE_NAME:Ljava/lang/String;

    .line 192
    const-string v0, "com.arcsoft.magicshotstudio.Main"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SHOT_MORE_STUDIO_CLASS_NAME:Ljava/lang/String;

    .line 193
    const-string v0, "com.sec.android.app.storycam"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VIDEO_CLIP_STUDIO_PACKAGE_NAME:Ljava/lang/String;

    .line 194
    const-string v0, "com.sec.android.app.storycam.activity.VideoEditorLiteActivity"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VIDEO_CLIP_STUDIO_CLASS_NAME:Ljava/lang/String;

    .line 195
    const-string v0, "com.lifevibes.trimapp"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->VIDEO_TRIM_STUDIO_PACKAGE_NAME:Ljava/lang/String;

    .line 197
    const-string v0, "com.sec.android.gallery3d"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->GALLERY_PKG_NAME:Ljava/lang/String;

    .line 198
    const-string v0, "com.sec.android.gallery3d.app.Gallery"

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->GALLERY_CLASS_NAME:Ljava/lang/String;

    .line 200
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    .line 202
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBPhotoEditorInstall:Z

    .line 203
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBCollageStudioInstall:Z

    .line 204
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBShotMoreStudioInstall:Z

    .line 205
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoClipStudioInstall:Z

    .line 206
    iput-boolean v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoTrimStudioInstall:Z

    .line 209
    iput-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFailureDecode:Z

    .line 82
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1885
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/sstudio/StudioActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2389
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoTrimStudioInstall:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFailureDecode:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2248
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showToast(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/sstudio/StudioActivity;)I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    return v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 2206
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->goShotMoreStudio()V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1876
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getFilePathFromUriforVideo(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/sstudio/StudioActivity;)I
    .locals 1

    .prologue
    .line 613
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v0

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->prefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->editor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->editor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBPhotoEditorInstall:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Intent;IZLjava/lang/String;I)V
    .locals 0

    .prologue
    .line 2267
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBCollageStudioInstall:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->bMagicShow:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 2136
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 2196
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->goMagicShotThroughGallery()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoClipStudioInstall:Z

    return v0
.end method

.method private addBlurView()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 2408
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    if-eqz v3, :cond_1

    .line 2409
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v3}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2410
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v3}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 2411
    .local v2, "parent":Landroid/widget/FrameLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2413
    .end local v2    # "parent":Landroid/widget/FrameLayout;
    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    .line 2415
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    if-nez v3, :cond_2

    .line 2416
    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    invoke-direct {v1}, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;-><init>()V

    .line 2418
    .local v1, "contextConfiguration":Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    .end local v1    # "contextConfiguration":Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    invoke-direct {v1}, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;-><init>()V

    .line 2419
    .restart local v1    # "contextConfiguration":Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    iput-boolean v6, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 2420
    iput v4, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 2421
    iput v4, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    .line 2422
    iput v4, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    .line 2423
    iput v4, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 2424
    iput v5, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    .line 2425
    iput v5, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    .line 2426
    iput v5, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    .line 2427
    iput v5, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    .line 2428
    iput v5, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    .line 2429
    iput-boolean v6, v1, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 2431
    new-instance v3, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-direct {v3, p0, v1, p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;Lcom/sec/android/mimage/blureffect/SGListener;)V

    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    .line 2433
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->setDuration(I)V

    .line 2434
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    iget-object v4, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2437
    .end local v1    # "contextConfiguration":Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    if-eqz v3, :cond_4

    .line 2438
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v3}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2439
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v3}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 2440
    .restart local v2    # "parent":Landroid/widget/FrameLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2442
    .end local v2    # "parent":Landroid/widget/FrameLayout;
    :cond_3
    const/high16 v3, 0x7f090000

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2443
    .local v0, "blurredGroup":Landroid/widget/FrameLayout;
    if-eqz v0, :cond_4

    .line 2444
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v0, v3, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 2447
    .end local v0    # "blurredGroup":Landroid/widget/FrameLayout;
    :cond_4
    return-void
.end method

.method private brokenImage()V
    .locals 2

    .prologue
    .line 468
    const-string v0, "DEBUG"

    const-string v1, "decoding fail. Redecoding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020041

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 470
    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 472
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 477
    :cond_0
    return-void
.end method

.method private checkAppInstalled()V
    .locals 1

    .prologue
    .line 280
    const-string v0, "com.sec.android.mimage.photoretouching"

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->checkInstallPackage(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBPhotoEditorInstall:Z

    .line 281
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBPhotoEditorInstall:Z

    iput-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBCollageStudioInstall:Z

    .line 282
    const-string v0, "com.arcsoft.magicshotstudio"

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->checkInstallPackage(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBShotMoreStudioInstall:Z

    .line 283
    const-string v0, "com.sec.android.app.storycam"

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->checkInstallPackage(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoClipStudioInstall:Z

    .line 284
    const-string v0, "com.lifevibes.trimapp"

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->checkInstallPackage(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoTrimStudioInstall:Z

    .line 285
    return-void
.end method

.method private checkInstallPackage(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2258
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2259
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2260
    const/4 v2, 0x1

    .line 2263
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v2

    .line 2261
    :catch_0
    move-exception v0

    .line 2262
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "DEBUG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NameNotFoundException :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2263
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private createListener()V
    .locals 2

    .prologue
    .line 1470
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHovered(Z)V

    .line 1471
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoRightImageFrame:Landroid/widget/ImageView;

    const-string v1, "Thumbnail image"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1472
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 1473
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    const-string v1, "Thumbnail image"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1474
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 1475
    iget-boolean v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    if-eqz v0, :cond_0

    .line 1476
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$1;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1491
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$2;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1510
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$3;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1522
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$4;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1540
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$5;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1676
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoRightImageFrame:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$13;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$13;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$14;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$14;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702
    return-void

    .line 1557
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$6;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1573
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$7;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1590
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$8;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1601
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$9;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1619
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$10;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1636
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$11;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1659
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/mimage/sstudio/StudioActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$12;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private decodeImageFromQJpg(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    .locals 32
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bDecode_q_p"    # Z

    .prologue
    .line 644
    if-nez p1, :cond_1

    .line 645
    const/4 v9, 0x0

    .line 766
    :cond_0
    :goto_0
    return-object v9

    .line 649
    :cond_1
    new-instance v17, Landroid/util/DisplayMetrics;

    invoke-direct/range {v17 .. v17}, Landroid/util/DisplayMetrics;-><init>()V

    .line 650
    .local v17, "displaymetrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 652
    move-object/from16 v0, v17

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v22, v0

    .line 653
    .local v22, "mViewWidth":I
    move-object/from16 v0, v17

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    .line 655
    .local v21, "mViewHeight":I
    move/from16 v0, v22

    move/from16 v1, v21

    if-le v0, v1, :cond_7

    .line 656
    move/from16 v22, v21

    .line 661
    :goto_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 663
    .local v3, "path":Ljava/lang/String;
    const/16 v28, 0x0

    .line 665
    .local v28, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v28

    .line 671
    :goto_2
    new-instance v23, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 672
    .local v23, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v10, 0x1

    move-object/from16 v0, v23

    iput-boolean v10, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 673
    move-object/from16 v0, v23

    invoke-static {v3, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 675
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sub-int v10, v10, v21

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 676
    sub-int v11, v11, v22

    .line 675
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    if-lt v10, v11, :cond_8

    const/16 v27, 0x1

    .line 677
    .local v27, "scaleByHeight":Z
    :goto_3
    if-eqz v27, :cond_9

    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int v10, v10, v21

    :goto_4
    int-to-double v0, v10

    move-wide/from16 v25, v0

    .line 680
    .local v25, "sampleSize":D
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->log(D)D

    move-result-wide v30

    div-double v12, v12, v30

    invoke-static {v12, v13}, Ljava/lang/Math;->floor(D)D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v0, v10

    move/from16 v20, v0

    .line 681
    .local v20, "inSampleSize":I
    if-nez v20, :cond_2

    .line 682
    const/16 v20, 0x1

    .line 684
    :cond_2
    move/from16 v0, v20

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 686
    const/4 v5, 0x0

    .local v5, "left":I
    const/4 v6, 0x0

    .local v6, "right":I
    const/4 v7, 0x0

    .local v7, "top":I
    const/4 v8, 0x0

    .line 688
    .local v8, "bottom":I
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v10, v11, :cond_a

    .line 689
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sub-int/2addr v10, v11

    div-int/lit8 v5, v10, 0x2

    .line 690
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    add-int v6, v10, v11

    .line 691
    const/4 v7, 0x0

    .line 692
    move-object/from16 v0, v23

    iget v8, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 701
    :goto_5
    if-gez v5, :cond_3

    .line 702
    const/4 v5, 0x0

    .line 703
    :cond_3
    if-gez v7, :cond_4

    .line 704
    const/4 v7, 0x0

    .line 705
    :cond_4
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lt v6, v10, :cond_5

    .line 706
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    add-int/lit8 v6, v10, -0x1

    .line 707
    :cond_5
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lt v8, v10, :cond_6

    .line 708
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    add-int/lit8 v8, v10, -0x1

    .line 710
    :cond_6
    new-instance v4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v4}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 711
    .local v4, "Quramoptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    const/4 v10, 0x7

    iput v10, v4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 712
    move/from16 v0, v20

    iput v0, v4, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inSampleSize:I

    .line 715
    if-eqz p2, :cond_c

    .line 717
    sub-int v29, v6, v5

    .line 718
    .local v29, "width":I
    sub-int v19, v8, v7

    .line 720
    .local v19, "height":I
    if-lez v29, :cond_b

    if-lez v19, :cond_b

    .line 721
    invoke-static/range {v3 .. v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->partialDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 724
    .local v9, "ret":Landroid/graphics/Bitmap;
    if-nez v9, :cond_d

    .line 725
    const-string v10, "Quram S_Studio"

    const-string v11, "decodeImageFromQJpg :ret Bitmap null"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 658
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "Quramoptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .end local v5    # "left":I
    .end local v6    # "right":I
    .end local v7    # "top":I
    .end local v8    # "bottom":I
    .end local v9    # "ret":Landroid/graphics/Bitmap;
    .end local v19    # "height":I
    .end local v20    # "inSampleSize":I
    .end local v23    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v25    # "sampleSize":D
    .end local v27    # "scaleByHeight":Z
    .end local v28    # "stream":Ljava/io/InputStream;
    .end local v29    # "width":I
    :cond_7
    move/from16 v21, v22

    goto/16 :goto_1

    .line 666
    .restart local v3    # "path":Ljava/lang/String;
    .restart local v28    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v18

    .line 668
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_2

    .line 675
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .restart local v23    # "opts":Landroid/graphics/BitmapFactory$Options;
    :cond_8
    const/16 v27, 0x0

    goto/16 :goto_3

    .line 677
    .restart local v27    # "scaleByHeight":Z
    :cond_9
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 678
    div-int v10, v10, v22

    goto/16 :goto_4

    .line 695
    .restart local v5    # "left":I
    .restart local v6    # "right":I
    .restart local v7    # "top":I
    .restart local v8    # "bottom":I
    .restart local v20    # "inSampleSize":I
    .restart local v25    # "sampleSize":D
    :cond_a
    const/4 v5, 0x0

    .line 696
    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 697
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    sub-int/2addr v10, v11

    div-int/lit8 v7, v10, 0x2

    .line 698
    move-object/from16 v0, v23

    iget v10, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    move-object/from16 v0, v23

    iget v11, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    add-int v8, v10, v11

    goto :goto_5

    .line 730
    .restart local v4    # "Quramoptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .restart local v19    # "height":I
    .restart local v29    # "width":I
    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 734
    .end local v19    # "height":I
    .end local v29    # "width":I
    :cond_c
    move-object/from16 v0, v28

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-static {v0, v1, v2, v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeStream(Ljava/io/InputStream;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 736
    .restart local v9    # "ret":Landroid/graphics/Bitmap;
    if-nez v9, :cond_d

    .line 737
    const-string v10, "Quram S_Studio"

    const-string v11, "decodeImageFromQJpg :ret Bitmap null"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 742
    :cond_d
    const/16 v24, -0x1

    .line 743
    .local v24, "rotate":I
    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    .line 744
    .local v14, "m":Landroid/graphics/Matrix;
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRotateDegree(Landroid/net/Uri;)I

    move-result v24

    .line 745
    if-eqz v24, :cond_e

    .line 746
    const/16 v10, 0x5a

    move/from16 v0, v24

    if-ne v0, v10, :cond_f

    .line 747
    const/high16 v10, 0x42b40000    # 90.0f

    invoke-virtual {v14, v10}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 755
    :cond_e
    :goto_6
    if-eqz v24, :cond_0

    .line 756
    const/16 v16, 0x0

    .line 761
    .local v16, "b":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    const/4 v15, 0x1

    invoke-static/range {v9 .. v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 763
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 764
    move-object/from16 v9, v16

    goto/16 :goto_0

    .line 748
    .end local v16    # "b":Landroid/graphics/Bitmap;
    :cond_f
    const/16 v10, 0xb4

    move/from16 v0, v24

    if-ne v0, v10, :cond_10

    .line 749
    const/high16 v10, 0x43340000    # 180.0f

    invoke-virtual {v14, v10}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_6

    .line 750
    :cond_10
    const/16 v10, 0x10e

    move/from16 v0, v24

    if-ne v0, v10, :cond_11

    .line 751
    const/high16 v10, 0x43870000    # 270.0f

    invoke-virtual {v14, v10}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_6

    .line 753
    :cond_11
    const/16 v24, 0x0

    goto :goto_6
.end method

.method private decoding()V
    .locals 13

    .prologue
    const/16 v12, 0xe0

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x438

    .line 480
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    iget v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    add-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    .line 481
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    packed-switch v6, :pswitch_data_0

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 483
    :pswitch_0
    iget-boolean v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z

    if-eqz v6, :cond_4

    .line 484
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 485
    .local v5, "videoPath":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 486
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto :goto_0

    .line 489
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getVideoFrame(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 491
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 492
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto :goto_0

    .line 495
    :cond_3
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 496
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->addBlurView()V

    goto :goto_0

    .line 499
    .end local v5    # "videoPath":Ljava/lang/String;
    :cond_4
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isJpeg(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 500
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    invoke-direct {p0, v6, v10}, Lcom/sec/android/mimage/sstudio/StudioActivity;->decodeImageFromQJpg(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_5

    .line 501
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    invoke-direct {p0, v6, v7, v8, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 506
    :cond_5
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 507
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto :goto_0

    .line 504
    :cond_7
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    invoke-direct {p0, v6, v7, v8, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 510
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 511
    const v7, 0x7f020047

    .line 510
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    .line 512
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 513
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->addBlurView()V

    .line 515
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-static {v6, v12, v12, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 517
    .local v4, "scaledBitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 518
    const v7, 0x7f02001b

    .line 517
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 519
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v4, v6, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 520
    .local v3, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-ne v6, v7, :cond_9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    if-eq v6, v7, :cond_a

    .line 521
    :cond_9
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 522
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 521
    invoke-static {v0, v6, v7, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 523
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 528
    :goto_2
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 529
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 530
    .local v2, "paint":Landroid/graphics/Paint;
    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 531
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 532
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v6, v11, v11, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 533
    invoke-direct {p0, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 534
    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 525
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "paint":Landroid/graphics/Paint;
    :cond_a
    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 526
    const/4 v0, 0x0

    goto :goto_2

    .line 537
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "result":Landroid/graphics/Bitmap;
    .end local v4    # "scaledBitmap":Landroid/graphics/Bitmap;
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 538
    const v7, 0x7f020041

    .line 537
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 539
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 540
    const v7, 0x7f020047

    .line 539
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    .line 541
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    .line 543
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 550
    :pswitch_2
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    if-nez v6, :cond_d

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isJpeg(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 551
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v6, v10}, Lcom/sec/android/mimage/sstudio/StudioActivity;->decodeImageFromQJpg(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_b

    .line 552
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v7, v6, v8, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 557
    :cond_b
    :goto_3
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 558
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto/16 :goto_0

    .line 555
    :cond_d
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v7, v6, v8, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    goto :goto_3

    .line 561
    :cond_e
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 562
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->addBlurView()V

    .line 563
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->frameImageDecoding(I)V

    goto/16 :goto_0

    .line 567
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 568
    .restart local v5    # "videoPath":Ljava/lang/String;
    if-nez v5, :cond_f

    .line 569
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto/16 :goto_0

    .line 572
    :cond_f
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getVideoFrame(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 574
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 575
    :cond_10
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto/16 :goto_0

    .line 578
    :cond_11
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 579
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->addBlurView()V

    .line 580
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->frameImageDecoding(I)V

    goto/16 :goto_0

    .line 585
    .end local v5    # "videoPath":Ljava/lang/String;
    :pswitch_4
    iget-boolean v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z

    if-eqz v6, :cond_13

    .line 586
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getVideoFrame(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 587
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 588
    :cond_12
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto/16 :goto_0

    .line 592
    :cond_13
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    if-nez v6, :cond_16

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isJpeg(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 593
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v6, v10}, Lcom/sec/android/mimage/sstudio/StudioActivity;->decodeImageFromQJpg(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_14

    .line 594
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 595
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 594
    invoke-direct {p0, v7, v6, v8, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 599
    :cond_14
    :goto_4
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_15

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 600
    :cond_15
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->brokenImage()V

    goto/16 :goto_0

    .line 597
    :cond_16
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-direct {p0, v7, v6, v8, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    goto :goto_4

    .line 604
    :cond_17
    iget-object v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 605
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->addBlurView()V

    .line 606
    iget v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->frameImageDecoding(I)V

    goto/16 :goto_0

    .line 481
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private deviceCheck()I
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "kmini"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "mega2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "vasta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "a7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "hestia"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "a5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "e7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "e5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 618
    :cond_0
    const/4 v0, 0x1

    .line 628
    :goto_0
    return v0

    .line 619
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->DEVICE_NAME:Ljava/lang/String;

    const-string v1, "levilte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 620
    const/4 v0, 0x2

    goto :goto_0

    .line 624
    :cond_2
    invoke-static {}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isLdevice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 625
    const/4 v0, 0x3

    goto :goto_0

    .line 628
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private frameImageDecoding(I)V
    .locals 10
    .param p1, "count"    # I

    .prologue
    const/16 v7, 0xc8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2095
    if-ne p1, v9, :cond_2

    .line 2096
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2097
    const v6, 0x7f020049

    .line 2096
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    .line 2106
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-static {v5, v7, v7, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2109
    .local v4, "scaledBitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2110
    const v6, 0x7f02004c

    .line 2109
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2112
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 2113
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2112
    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2115
    .local v3, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-ne v5, v6, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-eq v5, v6, :cond_4

    .line 2116
    :cond_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 2117
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 2116
    invoke-static {v0, v5, v6, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    .line 2118
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2124
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2125
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 2126
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2127
    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2128
    const/4 v5, 0x0

    invoke-virtual {v1, v4, v8, v8, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2129
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v5, v8, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2131
    invoke-direct {p0, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2133
    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    .line 2134
    return-void

    .line 2098
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "paint":Landroid/graphics/Paint;
    .end local v3    # "result":Landroid/graphics/Bitmap;
    .end local v4    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_2
    const/4 v5, 0x2

    if-ne p1, v5, :cond_3

    .line 2099
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2100
    const v6, 0x7f02004a

    .line 2099
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 2101
    :cond_3
    const/4 v5, 0x3

    if-lt p1, v5, :cond_0

    .line 2102
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2103
    const v6, 0x7f02004b

    .line 2102
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 2120
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "result":Landroid/graphics/Bitmap;
    .restart local v4    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_4
    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    .line 2121
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 35
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "viewWidth"    # I
    .param p4, "viewHeight"    # I

    .prologue
    .line 1203
    if-nez p2, :cond_0

    .line 1204
    const/4 v15, 0x0

    .line 1328
    :goto_0
    return-object v15

    .line 1205
    :cond_0
    const/4 v15, 0x0

    .line 1206
    .local v15, "b":Landroid/graphics/Bitmap;
    new-instance v23, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1207
    .local v23, "optsResolution":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    move-object/from16 v0, v23

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1208
    const/16 v29, 0x0

    .line 1210
    .local v29, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v29

    .line 1215
    const/4 v3, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-static {v0, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1217
    :try_start_1
    invoke-virtual/range {v29 .. v29}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1223
    move-object/from16 v0, v23

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1224
    .local v5, "imageWidth":I
    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1226
    .local v6, "imageHeight":I
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sub-int v3, v3, p4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1227
    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    sub-int v4, v4, p3

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 1226
    if-lt v3, v4, :cond_2

    const/16 v28, 0x1

    .line 1228
    .local v28, "scaleByHeight":Z
    :goto_1
    if-eqz v28, :cond_3

    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int v3, v3, p4

    :goto_2
    int-to-double v0, v3

    move-wide/from16 v25, v0

    .line 1231
    .local v25, "sampleSize":D
    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->log(D)D

    move-result-wide v31

    const-wide/high16 v33, 0x4000000000000000L    # 2.0

    invoke-static/range {v33 .. v34}, Ljava/lang/Math;->log(D)D

    move-result-wide v33

    div-double v31, v31, v33

    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->floor(D)D

    move-result-wide v31

    move-wide/from16 v0, v31

    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-int v0, v3

    move/from16 v22, v0

    .line 1232
    .local v22, "inSampleSize":I
    if-nez v22, :cond_1

    .line 1233
    const/16 v22, 0x1

    .line 1235
    :cond_1
    move/from16 v0, v22

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1236
    const/4 v3, 0x0

    move-object/from16 v0, v23

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1237
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v23

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1238
    const/4 v3, 0x1

    move-object/from16 v0, v23

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 1240
    :try_start_2
    invoke-virtual/range {p1 .. p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v29

    .line 1245
    const/4 v3, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-static {v0, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1246
    .local v2, "temp":Landroid/graphics/Bitmap;
    if-nez v2, :cond_4

    .line 1247
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1211
    .end local v2    # "temp":Landroid/graphics/Bitmap;
    .end local v5    # "imageWidth":I
    .end local v6    # "imageHeight":I
    .end local v22    # "inSampleSize":I
    .end local v25    # "sampleSize":D
    .end local v28    # "scaleByHeight":Z
    :catch_0
    move-exception v20

    .line 1212
    .local v20, "e2":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    .line 1213
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1218
    .end local v20    # "e2":Ljava/lang/Exception;
    :catch_1
    move-exception v19

    .line 1219
    .local v19, "e1":Ljava/io/IOException;
    invoke-virtual/range {v19 .. v19}, Ljava/io/IOException;->printStackTrace()V

    .line 1220
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1226
    .end local v19    # "e1":Ljava/io/IOException;
    .restart local v5    # "imageWidth":I
    .restart local v6    # "imageHeight":I
    :cond_2
    const/16 v28, 0x0

    goto :goto_1

    .line 1229
    .restart local v28    # "scaleByHeight":Z
    :cond_3
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int v3, v3, p3

    goto :goto_2

    .line 1241
    .restart local v22    # "inSampleSize":I
    .restart local v25    # "sampleSize":D
    :catch_2
    move-exception v18

    .line 1242
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 1243
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1248
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "temp":Landroid/graphics/Bitmap;
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1249
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 1251
    .local v16, "bm":Landroid/graphics/Bitmap;
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1252
    .local v17, "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1253
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1254
    move-object/from16 v2, v16

    .line 1258
    .end local v16    # "bm":Landroid/graphics/Bitmap;
    .end local v17    # "canvas":Landroid/graphics/Canvas;
    :cond_5
    :try_start_3
    invoke-virtual/range {v29 .. v29}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1263
    if-nez v2, :cond_6

    .line 1264
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1259
    :catch_3
    move-exception v18

    .line 1260
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 1261
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1266
    .end local v18    # "e":Ljava/io/IOException;
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1267
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 1268
    if-lez v5, :cond_7

    if-gtz v6, :cond_8

    .line 1269
    :cond_7
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1271
    :cond_8
    const/16 v24, -0x1

    .line 1272
    .local v24, "rotate":I
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 1274
    .local v7, "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRotateDegree(Landroid/net/Uri;)I

    move-result v24

    .line 1275
    if-eqz v24, :cond_9

    .line 1276
    const/16 v3, 0x5a

    move/from16 v0, v24

    if-ne v0, v3, :cond_d

    .line 1277
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1285
    :cond_9
    :goto_3
    if-eqz v24, :cond_a

    .line 1286
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 1287
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1288
    move-object v2, v15

    .line 1290
    :cond_a
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1291
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 1293
    move/from16 v0, p3

    int-to-float v3, v0

    move/from16 v0, p4

    int-to-float v4, v0

    div-float v30, v3, v4

    .line 1294
    .local v30, "viewRatio":F
    int-to-float v3, v5

    int-to-float v4, v6

    div-float v21, v3, v4

    .line 1297
    .local v21, "imgRatio":F
    const/4 v9, 0x0

    .line 1298
    .local v9, "startX":I
    const/4 v10, 0x0

    .line 1299
    .local v10, "startY":I
    move v12, v6

    .line 1300
    .local v12, "resizeHgt":I
    move v11, v5

    .line 1301
    .local v11, "resizeWdt":I
    cmpl-float v3, v30, v21

    if-lez v3, :cond_10

    .line 1302
    mul-int v3, p4, v5

    int-to-float v3, v3

    move/from16 v0, p3

    int-to-float v4, v0

    div-float/2addr v3, v4

    float-to-int v12, v3

    .line 1303
    sub-int v3, v6, v12

    div-int/lit8 v10, v3, 0x2

    .line 1304
    int-to-float v3, v5

    move/from16 v0, p3

    int-to-float v4, v0

    div-float v27, v3, v4

    .line 1310
    .local v27, "scale":F
    :goto_4
    if-eqz v11, :cond_b

    if-nez v12, :cond_c

    .line 1311
    :cond_b
    if-eqz v2, :cond_c

    .line 1312
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1313
    const/4 v2, 0x0

    .line 1316
    :cond_c
    move/from16 v0, v27

    float-to-double v3, v0

    const-wide/high16 v31, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v3, v31

    if-eqz v3, :cond_11

    .line 1317
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 1318
    .local v13, "mm":Landroid/graphics/Matrix;
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v3, v3, v27

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v27

    invoke-virtual {v13, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1319
    const/4 v14, 0x0

    move-object v8, v2

    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 1320
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1321
    const/4 v2, 0x0

    .line 1322
    goto/16 :goto_0

    .line 1278
    .end local v9    # "startX":I
    .end local v10    # "startY":I
    .end local v11    # "resizeWdt":I
    .end local v12    # "resizeHgt":I
    .end local v13    # "mm":Landroid/graphics/Matrix;
    .end local v21    # "imgRatio":F
    .end local v27    # "scale":F
    .end local v30    # "viewRatio":F
    :cond_d
    const/16 v3, 0xb4

    move/from16 v0, v24

    if-ne v0, v3, :cond_e

    .line 1279
    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_3

    .line 1280
    :cond_e
    const/16 v3, 0x10e

    move/from16 v0, v24

    if-ne v0, v3, :cond_f

    .line 1281
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto/16 :goto_3

    .line 1283
    :cond_f
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 1306
    .restart local v9    # "startX":I
    .restart local v10    # "startY":I
    .restart local v11    # "resizeWdt":I
    .restart local v12    # "resizeHgt":I
    .restart local v21    # "imgRatio":F
    .restart local v30    # "viewRatio":F
    :cond_10
    int-to-float v3, v6

    mul-float v3, v3, v30

    float-to-int v11, v3

    .line 1307
    sub-int v3, v5, v11

    div-int/lit8 v9, v3, 0x2

    .line 1308
    int-to-float v3, v6

    move/from16 v0, p4

    int-to-float v4, v0

    div-float v27, v3, v4

    .restart local v27    # "scale":F
    goto :goto_4

    .line 1323
    :cond_11
    invoke-static {v2, v9, v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 1324
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1325
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private getContentType(Landroid/net/Uri;)V
    .locals 12
    .param p1, "mUri"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x1

    .line 1432
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_0

    .line 1467
    return-void

    .line 1433
    :cond_0
    const/4 p1, 0x0

    .line 1434
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "mUri":Landroid/net/Uri;
    check-cast p1, Landroid/net/Uri;

    .line 1435
    .restart local p1    # "mUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 1437
    .local v6, "c":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "sef_file_type"

    aput-object v1, v2, v0

    .line 1438
    .local v2, "proj":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1439
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1440
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1441
    const-string v0, "sef_file_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 1442
    .local v9, "index":I
    if-ltz v9, :cond_4

    .line 1443
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x830

    if-ne v0, v1, :cond_4

    .line 1444
    iget v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1452
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1453
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1432
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v9    # "index":I
    :cond_1
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1449
    :catch_0
    move-exception v7

    .line 1450
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1452
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1453
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1457
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 1459
    .local v10, "temp":Ljava/lang/String;
    const-string v0, "images"

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1460
    iget v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    goto :goto_1

    .line 1451
    .end local v10    # "temp":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 1452
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1453
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1455
    :cond_3
    throw v0

    .line 1452
    .restart local v2    # "proj":[Ljava/lang/String;
    :cond_4
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1453
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1461
    .end local v2    # "proj":[Ljava/lang/String;
    .restart local v10    # "temp":Ljava/lang/String;
    :cond_5
    const-string v0, "video"

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1462
    if-nez v8, :cond_6

    .line 1463
    iput-boolean v11, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z

    .line 1464
    :cond_6
    iget v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    goto :goto_1
.end method

.method private getFilePathFromUriforVideo(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 1877
    const/4 v7, 0x0

    .line 1878
    .local v7, "result":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/android/mimage/sstudio/StudioActivity;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1879
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1880
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1881
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1882
    return-object v7
.end method

.method private getImage(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 345
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 347
    .local v1, "extras":Landroid/os/Bundle;
    const-string v3, "selectedCount"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 349
    .local v0, "count":I
    iput-boolean v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    .line 350
    if-lez v0, :cond_0

    .line 352
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showStudioNotAvailableToast()V

    .line 359
    const-string v3, "selectedItems"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    .line 360
    iput-boolean v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    .line 361
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 362
    .local v2, "mUri":Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 363
    iput-boolean v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    .line 368
    .end local v2    # "mUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 366
    .restart local v2    # "mUri":Landroid/net/Uri;
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getContentType(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p1, "contentURI"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x0

    .line 1886
    if-nez p1, :cond_0

    move-object v9, v10

    .line 1910
    :goto_0
    return-object v9

    .line 1891
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1893
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 1896
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1897
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1899
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 1901
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1902
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1903
    .local v8, "idx":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1904
    .local v9, "ret":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1907
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "idx":I
    .end local v9    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1908
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    const-string v0, "DEBUG"

    .line 1909
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "S_Studio getRealPathFromURI , IllegalArgumentException e: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1908
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, v10

    .line 1910
    goto :goto_0
.end method

.method private declared-synchronized getRecentUri()Landroid/net/Uri;
    .locals 24

    .prologue
    .line 1368
    monitor-enter p0

    const-wide/16 v15, 0x0

    .local v15, "dateTakenImg":J
    const-wide/16 v17, 0x0

    .line 1369
    .local v17, "dateTakenVideo":J
    const/16 v22, 0x0

    .local v22, "uriImg":Landroid/net/Uri;
    const/16 v23, 0x0

    .line 1372
    .local v23, "uriVdo":Landroid/net/Uri;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1373
    .local v3, "uriImages":Landroid/net/Uri;
    if-eqz v3, :cond_1

    .line 1374
    const/4 v14, 0x0

    .line 1375
    .local v14, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "mime_type"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    .line 1376
    const-string v5, "orientation"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "datetaken"

    aput-object v5, v4, v2

    .line 1377
    .local v4, "projection":[Ljava/lang/String;
    const-string v7, "datetaken DESC"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1379
    .local v7, "orderClause":Ljava/lang/String;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    .line 1386
    :goto_0
    if-eqz v14, :cond_0

    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1387
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1388
    .local v20, "id":J
    move-wide/from16 v0, v20

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v22

    .line 1390
    const-string v2, "datetaken"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1389
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 1391
    const-string v2, "DEBUG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "dateTaken is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide v0, v15

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1392
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1394
    .end local v20    # "id":J
    :cond_0
    const/4 v14, 0x0

    .line 1395
    const/4 v4, 0x0

    .line 1399
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v7    # "orderClause":Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    :cond_1
    sget-object v9, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1400
    .local v9, "uriVideos":Landroid/net/Uri;
    if-eqz v9, :cond_2

    .line 1401
    const/4 v14, 0x0

    .line 1402
    .restart local v14    # "cursor":Landroid/database/Cursor;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "mime_type"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    .line 1403
    const-string v5, "datetaken"

    aput-object v5, v4, v2

    .line 1404
    .restart local v4    # "projection":[Ljava/lang/String;
    const-string v7, "datetaken DESC"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1406
    .restart local v7    # "orderClause":Ljava/lang/String;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v10, v4

    move-object v13, v7

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v14

    .line 1412
    :goto_1
    if-eqz v14, :cond_2

    :try_start_4
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1413
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1414
    .restart local v20    # "id":J
    move-wide/from16 v0, v20

    invoke-static {v9, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v23

    .line 1416
    const-string v2, "datetaken"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1415
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 1417
    const-string v2, "DEBUG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "dateTaken is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v17

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1422
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v7    # "orderClause":Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    .end local v20    # "id":J
    :cond_2
    cmp-long v2, v15, v17

    if-lez v2, :cond_3

    .line 1426
    .end local v22    # "uriImg":Landroid/net/Uri;
    :goto_2
    monitor-exit p0

    return-object v22

    .line 1382
    .end local v9    # "uriVideos":Landroid/net/Uri;
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v7    # "orderClause":Ljava/lang/String;
    .restart local v14    # "cursor":Landroid/database/Cursor;
    .restart local v22    # "uriImg":Landroid/net/Uri;
    :catch_0
    move-exception v19

    .line 1383
    .local v19, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    const-string v2, "DEBUG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "S_Studio getRecentUri , e"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 1368
    .end local v3    # "uriImages":Landroid/net/Uri;
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v7    # "orderClause":Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    .end local v19    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1408
    .restart local v3    # "uriImages":Landroid/net/Uri;
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v7    # "orderClause":Ljava/lang/String;
    .restart local v9    # "uriVideos":Landroid/net/Uri;
    .restart local v14    # "cursor":Landroid/database/Cursor;
    :catch_1
    move-exception v19

    .line 1409
    .restart local v19    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    const-string v2, "DEBUG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "S_Studio getRecentUri , e"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1410
    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .line 1425
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v7    # "orderClause":Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    .end local v19    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object/from16 v22, v23

    .line 1426
    goto :goto_2
.end method

.method private getRotateDegree(Landroid/net/Uri;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, -0x1

    .line 1332
    if-eqz p1, :cond_0

    .line 1334
    const/4 v2, -0x1

    .line 1337
    .local v2, "exifOrientation":I
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/mnt/sdcard"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1338
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 1342
    .local v3, "path":Ljava/lang/String;
    :goto_0
    if-nez v3, :cond_2

    .line 1363
    .end local v2    # "exifOrientation":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_0
    :goto_1
    return v4

    .line 1340
    .restart local v2    # "exifOrientation":I
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_0

    .line 1345
    :cond_2
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, v3}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1346
    .local v1, "exif":Landroid/media/ExifInterface;
    if-eqz v1, :cond_0

    .line 1347
    const-string v5, "Orientation"

    .line 1348
    const/4 v6, 0x1

    .line 1347
    invoke-virtual {v1, v5, v6}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1349
    const/4 v4, 0x6

    if-ne v2, v4, :cond_3

    .line 1350
    const/16 v4, 0x5a

    goto :goto_1

    .line 1351
    :cond_3
    const/4 v4, 0x3

    if-ne v2, v4, :cond_4

    .line 1352
    const/16 v4, 0xb4

    goto :goto_1

    .line 1353
    :cond_4
    const/16 v4, 0x8

    if-ne v2, v4, :cond_5

    .line 1354
    const/16 v4, 0x10e

    goto :goto_1

    .line 1356
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 1358
    .end local v1    # "exif":Landroid/media/ExifInterface;
    :catch_0
    move-exception v0

    .line 1359
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private getVideoFrame(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1970
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1971
    .local v3, "mt":Landroid/media/MediaMetadataRetriever;
    const/4 v0, 0x0

    .line 1973
    .local v0, "bm":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1974
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    .line 1975
    invoke-virtual {v3, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1976
    const-wide/32 v5, 0x5f5e100

    invoke-virtual {v3, v5, v6}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1990
    :cond_0
    if-eqz v3, :cond_1

    .line 1991
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_1
    move-object v4, v0

    .line 1988
    .end local v2    # "f":Ljava/io/File;
    :cond_2
    :goto_0
    return-object v4

    .line 1980
    :catch_0
    move-exception v1

    .line 1981
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v5, "DEBUG"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "S_Studio getVideoFrame , IllegalArgumentException e: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1982
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    .line 1983
    const v6, 0x7f0601d8

    .line 1982
    invoke-direct {p0, v5, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showToast(Landroid/content/Context;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1990
    if-eqz v3, :cond_2

    .line 1991
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 1985
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 1986
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    .line 1987
    const v6, 0x7f0601d8

    .line 1986
    invoke-direct {p0, v5, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showToast(Landroid/content/Context;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1990
    if-eqz v3, :cond_2

    .line 1991
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 1989
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 1990
    if-eqz v3, :cond_3

    .line 1991
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1993
    :cond_3
    throw v4
.end method

.method private goMagicShotThroughGallery()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2197
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2198
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2199
    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2200
    const-string v0, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2201
    const-string v0, "onlyMagic"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2202
    const-string v0, "image/*"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2203
    const/16 v2, 0x61

    iget-boolean v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBShotMoreStudioInstall:Z

    const-string v4, "samsungapps://ProductDetail/com.arcsoft.magicshotstudio"

    const v5, 0x7f0601c4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    .line 2204
    return-void
.end method

.method private goShotMoreStudio()V
    .locals 20

    .prologue
    .line 2207
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2208
    .local v3, "intent":Landroid/content/Intent;
    new-instance v11, Landroid/content/ComponentName;

    const-string v2, "com.arcsoft.magicshotstudio"

    .line 2209
    const-string v4, "com.arcsoft.magicshotstudio.Main"

    .line 2208
    invoke-direct {v11, v2, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2210
    .local v11, "component":Landroid/content/ComponentName;
    invoke-virtual {v3, v11}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2211
    const-string v2, "selectedItems"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/net/Uri;

    .line 2213
    .local v19, "uri":Landroid/net/Uri;
    const-string v2, "selectedCount"

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2215
    const-string v2, "yyyyMMddkkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2216
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 2217
    .local v12, "dataFormat":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2218
    const-string v4, "BestFace_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2217
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2219
    .local v8, "bestFace":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2220
    const-string v4, "BestPhoto_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2219
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2221
    .local v9, "bestPhoto":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2222
    const-string v4, "Eraser_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2221
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 2223
    .local v16, "piclear":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2224
    const-string v4, "Drama_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2223
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 2225
    .local v15, "picaction":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2226
    const-string v4, "Picmotion_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2225
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 2227
    .local v17, "picmotion":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 2228
    .local v18, "real_path":Ljava/lang/String;
    const-string v2, "sef_file_name"

    move-object/from16 v0, v18

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2229
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2230
    .local v10, "bmOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v10, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2231
    move-object/from16 v0, v18

    invoke-static {v0, v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2233
    iget v14, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 2234
    .local v14, "mwidth":I
    iget v13, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 2236
    .local v13, "mheight":I
    const-string v2, "image_width"

    invoke-virtual {v3, v2, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2237
    const-string v2, "image_height"

    invoke-virtual {v3, v2, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2238
    const-string v2, "bestface"

    invoke-virtual {v3, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2239
    const-string v2, "eraser"

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2240
    const-string v2, "drama"

    invoke-virtual {v3, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2241
    const-string v2, "bestphoto"

    invoke-virtual {v3, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2242
    const-string v2, "picmotion"

    move-object/from16 v0, v17

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2244
    const/16 v4, 0x5e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBShotMoreStudioInstall:Z

    const-string v6, "samsungapps://ProductDetail/com.arcsoft.magicshotstudio"

    .line 2245
    const v7, 0x7f0601c4

    move-object/from16 v2, p0

    .line 2244
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    .line 2246
    return-void
.end method

.method private insertLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 2390
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    .line 2391
    const-string v3, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    .line 2390
    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    .line 2391
    if-eqz v2, :cond_0

    .line 2392
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2393
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394
    const-string v2, "feature"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2396
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2398
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2399
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2400
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2401
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2403
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private isJpeg(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 633
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 634
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 635
    const-string v1, "jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "JPG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 636
    const-string v1, "JPEG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 637
    :cond_0
    const/4 v1, 0x1

    .line 639
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLdevice()Z
    .locals 2

    .prologue
    .line 2508
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2509
    .local v0, "currentapiVersion":I
    const/16 v1, 0x13

    if-le v0, v1, :cond_0

    .line 2510
    const/4 v1, 0x1

    .line 2512
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLightThemeRequired()Z
    .locals 3

    .prologue
    .line 2485
    const/4 v0, 0x0

    .line 2487
    .local v0, "result":Z
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "tr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2488
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "tb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2489
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "a5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2490
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "a7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2491
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "e5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2492
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "e7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2493
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "SCL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2494
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "SC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2495
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "hestia"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2496
    invoke-static {}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isLdevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2498
    :cond_0
    const/4 v0, 0x1

    .line 2499
    :cond_1
    return v0
.end method

.method private recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1944
    if-eqz p1, :cond_1

    .line 1945
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1946
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1948
    :cond_0
    const/4 p1, 0x0

    .line 1950
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private recycleBitmap2(Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 1954
    const/4 v0, 0x0

    .line 1955
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_1

    .line 1956
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1958
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v2, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 1959
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1960
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1961
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1962
    const/4 v0, 0x0

    .line 1965
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1967
    :cond_1
    return-void
.end method

.method private removeSideMarginOfAlertDialogButton(Landroid/app/Dialog;)V
    .locals 7
    .param p1, "d"    # Landroid/app/Dialog;

    .prologue
    const/4 v6, 0x0

    .line 2336
    move-object v4, p1

    check-cast v4, Landroid/app/AlertDialog;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .local v3, "posi":Landroid/widget/Button;
    move-object v4, p1

    .line 2337
    check-cast v4, Landroid/app/AlertDialog;

    const/4 v5, -0x2

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 2338
    .local v1, "nega":Landroid/widget/Button;
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "d":Landroid/app/Dialog;
    const/4 v4, -0x3

    invoke-virtual {p1, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 2340
    .local v2, "neut":Landroid/widget/Button;
    invoke-virtual {v3}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2341
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2342
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2344
    if-eqz v3, :cond_0

    .line 2345
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2346
    :cond_0
    if-eqz v1, :cond_1

    .line 2347
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2348
    :cond_1
    if-eqz v2, :cond_2

    .line 2349
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2350
    :cond_2
    return-void
.end method

.method private setDim(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1, "tv"    # Landroid/widget/TextView;
    .param p2, "flag"    # Z

    .prologue
    .line 1997
    if-eqz p2, :cond_0

    .line 1998
    const/high16 v0, 0x59000000

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2002
    :goto_0
    return-void

    .line 2000
    :cond_0
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private setLayoutId()V
    .locals 4

    .prologue
    .line 288
    const v1, 0x7f090001

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    .line 289
    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    .line 290
    const v1, 0x7f090006

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    .line 291
    const v1, 0x7f090007

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    .line 292
    const v1, 0x7f090008

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    .line 293
    const v1, 0x7f090009

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    .line 295
    const v1, 0x7f090011

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    .line 296
    const v1, 0x7f090010

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    .line 297
    const v1, 0x7f090012

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoRightImageFrame:Landroid/widget/ImageView;

    .line 299
    const v1, 0x7f09000e

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    .line 300
    const v1, 0x7f090014

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    .line 301
    const v1, 0x7f090017

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    .line 302
    const v1, 0x7f09001a

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    .line 303
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    .line 305
    const v1, 0x7f09000d

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    .line 306
    const v1, 0x7f090013

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    .line 307
    const v1, 0x7f090016

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    .line 308
    const v1, 0x7f090019

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    .line 309
    const v1, 0x7f09001c

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    .line 311
    const v1, 0x7f09000f

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    .line 312
    const v1, 0x7f090015

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    .line 313
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    .line 314
    const v1, 0x7f09001b

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    .line 315
    const v1, 0x7f09001e

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    .line 316
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 317
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 318
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 320
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 323
    const v1, 0x7f09000a

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mSStudioTV:Landroid/widget/TextView;

    .line 324
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mSStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceRegular(Landroid/widget/TextView;)V

    .line 327
    :cond_0
    const v1, 0x7f09000b

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mSStudioSubTV:Landroid/widget/TextView;

    .line 329
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mSStudioSubTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mSStudioSubTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTextViewWidth(Landroid/widget/TextView;)V

    .line 332
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarTV:Landroid/widget/TextView;

    .line 333
    const-string v1, "patek"

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isDevice(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 335
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 336
    const v2, 0x7f05000d

    .line 335
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 337
    .local v0, "textsize":I
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarTV:Landroid/widget/TextView;

    const/4 v2, 0x0

    int-to-float v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 339
    .end local v0    # "textsize":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarTV:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceLight(Landroid/widget/TextView;)V

    .line 340
    const v1, 0x7f090003

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;

    .line 341
    return-void
.end method

.method private setMode()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x7

    const/4 v3, 0x1

    .line 372
    iget-boolean v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    if-eqz v1, :cond_1

    .line 373
    iput v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    .line 374
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRecentUri()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    .line 375
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mRecentUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 376
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    .line 429
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showStudioNotAvailableToast()V

    .line 431
    return-void

    .line 377
    :cond_1
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    if-nez v1, :cond_2

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    if-nez v1, :cond_2

    .line 379
    const/4 v1, 0x6

    iput v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 383
    :cond_2
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    if-nez v1, :cond_7

    .line 384
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    add-int v0, v1, v2

    .line 385
    .local v0, "sum":I
    if-nez v0, :cond_3

    .line 386
    const-string v1, "DEBUG"

    const-string v2, "wrong image count result... isNoSelectPic becoming true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iput v5, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    .line 388
    iput-boolean v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    goto :goto_0

    .line 389
    :cond_3
    if-lt v0, v6, :cond_4

    const/4 v1, 0x6

    if-gt v0, v1, :cond_4

    .line 391
    iput v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 394
    :cond_4
    if-lt v0, v4, :cond_5

    const/16 v1, 0xf

    if-gt v0, v1, :cond_5

    .line 395
    iput v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 397
    :cond_5
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    if-ne v1, v3, :cond_6

    .line 398
    iput v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 401
    :cond_6
    iput v4, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 403
    .end local v0    # "sum":I
    :cond_7
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    add-int v0, v1, v2

    .line 404
    .restart local v0    # "sum":I
    if-le v0, v3, :cond_8

    const/16 v1, 0xf

    if-gt v0, v1, :cond_8

    .line 405
    iput v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 406
    :cond_8
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    if-ne v1, v3, :cond_9

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    if-nez v1, :cond_9

    .line 407
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0

    .line 409
    :cond_9
    iput v4, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    goto :goto_0
.end method

.method private setTextViewWidth(Landroid/widget/TextView;)V
    .locals 13
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 2020
    const v11, 0x7f0601ac

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setText(I)V

    .line 2021
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2022
    .local v8, "str":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget-object v7, v11, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2023
    .local v7, "locale":Ljava/util/Locale;
    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2025
    .local v5, "language":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2026
    const v12, 0x7f050004

    .line 2025
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2027
    .local v2, "font_size_normal":I
    iget-object v11, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2028
    const v12, 0x7f050005

    .line 2027
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2029
    .local v3, "font_size_too_long":I
    invoke-static {}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isLdevice()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2031
    iget-object v11, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2032
    const v12, 0x7f050006

    .line 2031
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move v2, v3

    .line 2034
    :cond_0
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/widget/TextView;->measure(II)V

    .line 2036
    const-string v11, "en"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2037
    const-string v11, " "

    invoke-virtual {v8, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2038
    .local v1, "first":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 2039
    .local v0, "bLong":Z
    const/4 v6, 0x0

    .line 2040
    .local v6, "line_count_sum":I
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10}, Ljava/lang/String;-><init>()V

    .line 2041
    .local v10, "text":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v11, v1

    if-lt v4, v11, :cond_1

    .line 2057
    if-eqz v0, :cond_5

    .line 2058
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2060
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_4

    .line 2061
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setLines(I)V

    .line 2066
    :goto_1
    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 2067
    .local v9, "ta":Landroid/text/TextUtils$TruncateAt;
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2068
    const/4 v11, 0x0

    int-to-float v12, v3

    invoke-virtual {p1, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2072
    .end local v9    # "ta":Landroid/text/TextUtils$TruncateAt;
    :goto_2
    invoke-virtual {p1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2092
    .end local v0    # "bLong":Z
    .end local v1    # "first":[Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "line_count_sum":I
    .end local v10    # "text":Ljava/lang/String;
    :goto_3
    return-void

    .line 2043
    .restart local v0    # "bLong":Z
    .restart local v1    # "first":[Ljava/lang/String;
    .restart local v4    # "i":I
    .restart local v6    # "line_count_sum":I
    .restart local v10    # "text":Ljava/lang/String;
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v12, v1, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2044
    const/4 v11, 0x2

    if-ne v4, v11, :cond_3

    .line 2045
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2050
    :goto_4
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/widget/TextView;->measure(II)V

    .line 2051
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineCount()I

    move-result v11

    add-int/2addr v6, v11

    .line 2053
    const/4 v11, 0x5

    if-le v6, v11, :cond_2

    .line 2054
    const/4 v0, 0x1

    .line 2041
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2047
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_4

    .line 2063
    :cond_4
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setLines(I)V

    goto :goto_1

    .line 2070
    :cond_5
    const/4 v11, 0x0

    int-to-float v12, v2

    invoke-virtual {p1, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_2

    .line 2075
    .end local v0    # "bLong":Z
    .end local v1    # "first":[Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "line_count_sum":I
    .end local v10    # "text":Ljava/lang/String;
    :cond_6
    const v11, 0x7f09000b

    invoke-virtual {p0, v11}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .end local p1    # "tv":Landroid/widget/TextView;
    check-cast p1, Landroid/widget/TextView;

    .line 2076
    .restart local p1    # "tv":Landroid/widget/TextView;
    const v11, 0x7f0601ac

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setText(I)V

    .line 2078
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_7

    .line 2079
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setLines(I)V

    .line 2080
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2081
    const v12, 0x7f05000b

    .line 2080
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setHeight(I)V

    .line 2089
    :goto_5
    const/4 v11, 0x0

    int-to-float v12, v2

    invoke-virtual {p1, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_3

    .line 2083
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2084
    const v12, 0x7f05000c

    .line 2083
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setHeight(I)V

    .line 2085
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setLines(I)V

    .line 2086
    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 2087
    .restart local v9    # "ta":Landroid/text/TextUtils$TruncateAt;
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_5
.end method

.method private setTypeFaceBold(Landroid/widget/TextView;)V
    .locals 3
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 2015
    const-string v1, "sans-serif"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2016
    .local v0, "mTypeFace":Landroid/graphics/Typeface;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2017
    return-void
.end method

.method private setTypeFaceLight(Landroid/widget/TextView;)V
    .locals 3
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 2005
    const-string v1, "sec-roboto-light"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2006
    .local v0, "mTypeFace":Landroid/graphics/Typeface;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2007
    return-void
.end method

.method private setTypeFaceRegular(Landroid/widget/TextView;)V
    .locals 3
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 2010
    const-string v1, "sans-serif"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2011
    .local v0, "mTypeFace":Landroid/graphics/Typeface;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2012
    return-void
.end method

.method private settingView()V
    .locals 9

    .prologue
    const/high16 v8, 0x41400000    # 12.0f

    const/4 v7, 0x4

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 772
    const/4 v1, 0x0

    .line 773
    .local v1, "isImageBroken":Z
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-ne v2, v6, :cond_0

    .line 776
    const/4 v1, 0x1

    .line 777
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->frameImageDecoding(I)V

    .line 778
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 779
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 780
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 781
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 782
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 784
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setMode()V

    .line 786
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 787
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 788
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    packed-switch v2, :pswitch_data_0

    .line 1138
    :goto_0
    if-eqz v1, :cond_1

    .line 1139
    iput v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    .line 1140
    :cond_1
    return-void

    .line 792
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 793
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 794
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 795
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 797
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 798
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 800
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 801
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 802
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 803
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 804
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 806
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 807
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 808
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 809
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 810
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 812
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 813
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 814
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 815
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 816
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 818
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 819
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 820
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 821
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 822
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 827
    :pswitch_1
    if-nez v1, :cond_2

    .line 828
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 829
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 830
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 831
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 832
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 834
    iget-boolean v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z

    if-eqz v2, :cond_3

    .line 835
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 836
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 844
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 845
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 846
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 847
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 848
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 850
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 851
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 852
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 853
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 854
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 856
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 857
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 858
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 859
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 860
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 862
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 863
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 864
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 865
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 866
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 838
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 839
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 840
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 841
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 871
    :pswitch_2
    if-nez v1, :cond_4

    .line 872
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 873
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 874
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 875
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 876
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 877
    .local v0, "cnt":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 878
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 879
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceBold(Landroid/widget/TextView;)V

    .line 881
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 882
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 884
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 885
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 886
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 887
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 888
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 890
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 891
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 892
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 893
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 894
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 896
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 897
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 898
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 899
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 900
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 902
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 903
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 904
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 905
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 906
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 911
    .end local v0    # "cnt":Ljava/lang/String;
    :pswitch_3
    if-nez v1, :cond_5

    .line 912
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 913
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 914
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 915
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 916
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 917
    .restart local v0    # "cnt":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0xa

    if-ge v2, v3, :cond_6

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_6

    .line 918
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 919
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 927
    :goto_2
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceBold(Landroid/widget/TextView;)V

    .line 929
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 930
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 932
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 933
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 934
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 935
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 936
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 938
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 939
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 940
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 941
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 942
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 944
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 945
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 946
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 947
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 948
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 950
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 951
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 952
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 953
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 954
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 920
    :cond_6
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_7

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0xa

    if-lt v2, v3, :cond_7

    .line 921
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    const/high16 v3, 0x41300000    # 11.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 922
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 924
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 925
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 959
    .end local v0    # "cnt":Ljava/lang/String;
    :pswitch_4
    if-nez v1, :cond_8

    .line 960
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 961
    :cond_8
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 962
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 963
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 964
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 965
    .restart local v0    # "cnt":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 966
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 967
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceBold(Landroid/widget/TextView;)V

    .line 969
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 970
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 972
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 973
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 974
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 975
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 976
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 978
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 979
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 980
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 981
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 982
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 984
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 985
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 986
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 987
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 988
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 990
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 991
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 992
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 993
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 994
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 999
    .end local v0    # "cnt":Ljava/lang/String;
    :pswitch_5
    if-nez v1, :cond_9

    .line 1000
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1001
    :cond_9
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1002
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1003
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1004
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1005
    .restart local v0    # "cnt":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    if-lt v2, v5, :cond_a

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0xa

    if-ge v2, v3, :cond_a

    .line 1006
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1007
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1015
    :goto_3
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceBold(Landroid/widget/TextView;)V

    .line 1017
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1018
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1020
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1021
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1022
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1023
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1024
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1026
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1027
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1028
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1029
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1030
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1032
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1033
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1034
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1035
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1036
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1038
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1039
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1040
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1041
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1042
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 1008
    :cond_a
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0xa

    if-lt v2, v3, :cond_b

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_b

    .line 1009
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    const/high16 v3, 0x41300000    # 11.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1010
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1012
    :cond_b
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1013
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1047
    .end local v0    # "cnt":Ljava/lang/String;
    :pswitch_6
    if-nez v1, :cond_c

    .line 1048
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1049
    :cond_c
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1050
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1051
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1052
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1053
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1054
    .restart local v0    # "cnt":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1055
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceBold(Landroid/widget/TextView;)V

    .line 1057
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1058
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1060
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1061
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1062
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1063
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1064
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1066
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1067
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1068
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1069
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1070
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1072
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1073
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1074
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1075
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1076
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1078
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1079
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1080
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1081
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1082
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 1087
    .end local v0    # "cnt":Ljava/lang/String;
    :pswitch_7
    if-nez v1, :cond_d

    .line 1088
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1089
    :cond_d
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1090
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1091
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCountBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1092
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1093
    .restart local v0    # "cnt":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    if-lt v2, v5, :cond_e

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0xa

    if-ge v2, v3, :cond_e

    .line 1094
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1095
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1103
    :goto_4
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTypeFaceBold(Landroid/widget/TextView;)V

    .line 1105
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1106
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoQuickButtonFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1108
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1109
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1110
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1111
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1112
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1114
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1115
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1116
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1117
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1118
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1120
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1121
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1122
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1123
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1124
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1126
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1127
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1128
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1129
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    .line 1130
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimStudioTV:Landroid/widget/TextView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setDim(Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    .line 1096
    :cond_e
    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0xa

    if-lt v2, v3, :cond_f

    iget v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_f

    .line 1097
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    const/high16 v3, 0x41300000    # 11.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1098
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1100
    :cond_f
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1101
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mTextViewCnt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 788
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private showAlertDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/high16 v6, -0x1000000

    .line 2137
    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 2140
    .local v3, "vi":Landroid/view/LayoutInflater;
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 2142
    const v4, 0x7f030005

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2152
    .local v1, "intro_layout":Landroid/widget/LinearLayout;
    :goto_0
    const v4, 0x7f090023

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2153
    .local v0, "checkbox":Landroid/widget/CheckBox;
    const v4, 0x7f090021

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2154
    .local v2, "textView":Landroid/widget/TextView;
    const v4, 0x7f0601c5

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2156
    invoke-static {}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isLightThemeRequired()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2157
    sget v4, Lcom/sec/android/mimage/sstudio/StudioActivity;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 2158
    sget v4, Lcom/sec/android/mimage/sstudio/StudioActivity;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2164
    :goto_1
    new-instance v4, Lcom/sec/android/mimage/sstudio/StudioActivity$15;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$15;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2173
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0601c4

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 2174
    const v5, 0x7f060007

    new-instance v6, Lcom/sec/android/mimage/sstudio/StudioActivity$16;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity$16;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 2193
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2194
    return-void

    .line 2146
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    .end local v1    # "intro_layout":Landroid/widget/LinearLayout;
    .end local v2    # "textView":Landroid/widget/TextView;
    :cond_0
    const v4, 0x7f030004

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .restart local v1    # "intro_layout":Landroid/widget/LinearLayout;
    goto :goto_0

    .line 2160
    .restart local v0    # "checkbox":Landroid/widget/CheckBox;
    .restart local v2    # "textView":Landroid/widget/TextView;
    :cond_1
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 2161
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method private showDownloadablePopup(ILjava/lang/String;)V
    .locals 12
    .param p1, "titleId"    # I
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x8

    const/high16 v10, -0x1000000

    .line 2277
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2279
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v8, "layout_inflater"

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 2280
    .local v7, "vi":Landroid/view/LayoutInflater;
    const v8, 0x7f030004

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 2281
    .local v4, "intro_layout":Landroid/widget/LinearLayout;
    const v8, 0x7f090023

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 2282
    .local v1, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v1, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2285
    const v8, 0x7f090022

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2284
    check-cast v2, Landroid/widget/LinearLayout;

    .line 2286
    .local v2, "checkboxLinear":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2288
    const v8, 0x7f090021

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 2290
    .local v6, "textView":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isLightThemeRequired()Z

    move-result v8

    if-nez v8, :cond_0

    .line 2291
    sget v8, Lcom/sec/android/mimage/sstudio/StudioActivity;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 2292
    sget v8, Lcom/sec/android/mimage/sstudio/StudioActivity;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2298
    :goto_0
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2300
    iget-object v8, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2302
    .local v5, "res":Landroid/content/res/Resources;
    const v8, 0x7f0601dd

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 2303
    iget-object v11, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v11, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v11, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 2301
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2305
    const v8, 0x7f0601de

    new-instance v9, Lcom/sec/android/mimage/sstudio/StudioActivity$17;

    invoke-direct {v9, p0, p2}, Lcom/sec/android/mimage/sstudio/StudioActivity$17;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2321
    const v8, 0x7f060009

    new-instance v9, Lcom/sec/android/mimage/sstudio/StudioActivity$18;

    invoke-direct {v9, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$18;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    invoke-virtual {v0, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2329
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 2330
    .local v3, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 2332
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->removeSideMarginOfAlertDialogButton(Landroid/app/Dialog;)V

    .line 2333
    return-void

    .line 2294
    .end local v3    # "dialog":Landroid/app/AlertDialog;
    .end local v5    # "res":Landroid/content/res/Resources;
    :cond_0
    invoke-virtual {v1, v10}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 2295
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showStudioNotAvailableToast()V
    .locals 5

    .prologue
    const v4, 0x7f0601d8

    const/4 v3, 0x7

    const/4 v2, 0x5

    .line 437
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v0

    .line 439
    .local v0, "deviceType":I
    packed-switch v0, :pswitch_data_0

    .line 455
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-ne v1, v3, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 443
    :pswitch_0
    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eq v1, v3, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 447
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 459
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 460
    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 439
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private showToast(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I

    .prologue
    .line 2249
    const/4 v0, 0x0

    .line 2250
    .local v0, "mToast":Landroid/widget/Toast;
    if-eqz p1, :cond_0

    .line 2251
    const/4 v1, 0x1

    invoke-static {p1, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2252
    :cond_0
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2253
    return-void
.end method

.method private startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "result"    # I
    .param p3, "flag"    # Z
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "titleId"    # I

    .prologue
    .line 2269
    if-eqz p3, :cond_0

    .line 2270
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2274
    :goto_0
    return-void

    .line 2272
    :cond_0
    invoke-direct {p0, p5, p4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->showDownloadablePopup(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public isDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 2503
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2476
    if-nez p1, :cond_0

    .line 2477
    const/4 v0, 0x0

    .line 2479
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 24
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1707
    invoke-super/range {p0 .. p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1709
    if-eqz p3, :cond_0

    .line 1710
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_9

    .line 1711
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFailureDecode:Z

    .line 1712
    const/16 v2, 0x64

    move/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 1713
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    .line 1714
    .local v23, "uri":Landroid/net/Uri;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 1715
    .local v11, "bundle":Landroid/os/Bundle;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1718
    .local v16, "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez v11, :cond_1

    .line 1871
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v23    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 1720
    .restart local v11    # "bundle":Landroid/os/Bundle;
    .restart local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v23    # "uri":Landroid/net/Uri;
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1722
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1723
    .local v3, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.mimage.photoretouching"

    const-string v4, "com.sec.android.mimage.photoretouching.PhotoRetouching"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1724
    const-string v2, "selectedItems"

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1725
    const-string v2, "selectedCount"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1726
    const-string v2, "fromStudio"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1727
    const-string v2, "android.intent.action.EDIT"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1728
    const/16 v4, 0x5e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBPhotoEditorInstall:Z

    .line 1729
    const-string v6, "samsungapps://ProductDetail/com.sec.android.mimage.photoretouching"

    const v7, 0x7f060181

    move-object/from16 v2, p0

    .line 1728
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    goto :goto_0

    .line 1730
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v23    # "uri":Landroid/net/Uri;
    :cond_2
    const/16 v2, 0x63

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 1731
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 1732
    .restart local v11    # "bundle":Landroid/os/Bundle;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1735
    .restart local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v2, "selectedCount"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 1736
    .local v13, "count":I
    const-string v2, "selectedItems"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    .line 1738
    new-instance v3, Landroid/content/Intent;

    const-string v2, "com.sec.android.mimage.photoretouching.multigrid"

    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1739
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v2, "selectedItems"

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1740
    const-string v2, "selectedCount"

    invoke-virtual {v3, v2, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1741
    const/16 v4, 0x5e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBCollageStudioInstall:Z

    .line 1742
    const-string v6, "samsungapps://ProductDetail/com.sec.android.mimage.photoretouching"

    const v7, 0x7f060182

    move-object/from16 v2, p0

    .line 1741
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    goto :goto_0

    .line 1744
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v13    # "count":I
    .end local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_3
    const/16 v2, 0x61

    move/from16 v0, p1

    if-ne v0, v2, :cond_4

    .line 1745
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    .line 1746
    .restart local v23    # "uri":Landroid/net/Uri;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 1747
    .restart local v11    # "bundle":Landroid/os/Bundle;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1750
    .restart local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v11, :cond_0

    .line 1752
    const/4 v2, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1754
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1755
    .restart local v3    # "intent":Landroid/content/Intent;
    new-instance v12, Landroid/content/ComponentName;

    const-string v2, "com.arcsoft.magicshotstudio"

    .line 1756
    const-string v4, "com.arcsoft.magicshotstudio.Main"

    .line 1755
    invoke-direct {v12, v2, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    .local v12, "component":Landroid/content/ComponentName;
    invoke-virtual {v3, v12}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1758
    const-string v2, "selectedItems"

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1759
    const-string v2, "selectedCount"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1761
    const-string v2, "yyyyMMddkkmmss"

    .line 1762
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1761
    invoke-static {v2, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1762
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1763
    .local v14, "dataFormat":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1764
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "BestFace_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1763
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1765
    .local v8, "bestFace":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1766
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "BestPhoto_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1765
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1767
    .local v9, "bestPhoto":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1768
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Eraser_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1767
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1769
    .local v20, "piclear":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1770
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Drama_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1769
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1771
    .local v19, "picaction":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1772
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Picmotion_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".jpg"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1771
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 1773
    .local v21, "picmotion":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v22

    .line 1774
    .local v22, "real_path":Ljava/lang/String;
    const-string v2, "sef_file_name"

    move-object/from16 v0, v22

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1775
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1776
    .local v10, "bmOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v10, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1777
    move-object/from16 v0, v22

    invoke-static {v0, v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1779
    const-string v2, "image_width"

    iget v4, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1780
    const-string v2, "image_height"

    iget v4, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1781
    const-string v2, "bestface"

    invoke-virtual {v3, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1782
    const-string v2, "eraser"

    move-object/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783
    const-string v2, "drama"

    move-object/from16 v0, v19

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1784
    const-string v2, "bestphoto"

    invoke-virtual {v3, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1785
    const-string v2, "picmotion"

    move-object/from16 v0, v21

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1787
    const/16 v4, 0x5e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBShotMoreStudioInstall:Z

    .line 1788
    const-string v6, "samsungapps://ProductDetail/com.arcsoft.magicshotstudio"

    const v7, 0x7f0601c4

    move-object/from16 v2, p0

    .line 1787
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    goto/16 :goto_0

    .line 1789
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v8    # "bestFace":Ljava/lang/String;
    .end local v9    # "bestPhoto":Ljava/lang/String;
    .end local v10    # "bmOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v12    # "component":Landroid/content/ComponentName;
    .end local v14    # "dataFormat":Ljava/lang/String;
    .end local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v19    # "picaction":Ljava/lang/String;
    .end local v20    # "piclear":Ljava/lang/String;
    .end local v21    # "picmotion":Ljava/lang/String;
    .end local v22    # "real_path":Ljava/lang/String;
    .end local v23    # "uri":Landroid/net/Uri;
    :cond_4
    const/16 v2, 0x60

    move/from16 v0, p1

    if-ne v0, v2, :cond_5

    .line 1790
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1791
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v2, "*/*"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1792
    const-string v2, "android.intent.action.EDIT"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1793
    const-string v2, "com.sec.android.app.storycam"

    .line 1794
    const-string v4, "com.sec.android.app.storycam.activity.VideoEditorLiteActivity"

    .line 1793
    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1795
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 1796
    .restart local v11    # "bundle":Landroid/os/Bundle;
    const-string v2, "selectedItems"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    .line 1797
    .restart local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1799
    const-string v2, "android.intent.extra.STREAM"

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1801
    const/16 v4, 0x5e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoClipStudioInstall:Z

    .line 1802
    const-string v6, "samsungapps://ProductDetail/com.sec.android.app.storycam"

    const v7, 0x7f060183

    move-object/from16 v2, p0

    .line 1801
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    goto/16 :goto_0

    .line 1803
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v16    # "mAddedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_5
    const/16 v2, 0x5f

    move/from16 v0, p1

    if-ne v0, v2, :cond_6

    .line 1804
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    .line 1805
    .restart local v23    # "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getFilePathFromUriforVideo(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 1806
    .local v18, "path":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v2, "android.intent.action.TRIM"

    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1807
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v2, "com.lifevibes.trimapp"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1809
    :try_start_0
    const-string v2, "uri"

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1813
    :goto_1
    const/16 v4, 0x5e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoTrimStudioInstall:Z

    .line 1814
    const-string v6, "samsungapps://ProductDetail/com.lifevibes.trimapp"

    const v7, 0x7f060184

    move-object/from16 v2, p0

    .line 1813
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V

    goto/16 :goto_0

    .line 1810
    :catch_0
    move-exception v15

    .line 1811
    .local v15, "e":Ljava/lang/NullPointerException;
    const-string v2, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "S_Studio NullPointerException "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1815
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v15    # "e":Ljava/lang/NullPointerException;
    .end local v18    # "path":Ljava/lang/String;
    .end local v23    # "uri":Landroid/net/Uri;
    :cond_6
    const/16 v2, 0x5d

    move/from16 v0, p1

    if-ne v0, v2, :cond_0

    .line 1816
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 1817
    .restart local v11    # "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    if-eqz v2, :cond_7

    .line 1818
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1819
    :cond_7
    const-string v2, "selectedItems"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    .line 1821
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 1822
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    .line 1823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

    .line 1824
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoFrameBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoFrameBitmap:Landroid/graphics/Bitmap;

    .line 1825
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    .line 1826
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1827
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 1828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    .line 1829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1831
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mImageCount:I

    .line 1832
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoCount:I

    .line 1833
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicshotCount:I

    .line 1834
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFirstVideo:Z

    .line 1836
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/net/Uri;

    .line 1837
    .local v17, "mUri":Landroid/net/Uri;
    if-nez v17, :cond_8

    .line 1838
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    .line 1839
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V

    .line 1856
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setMode()V

    .line 1857
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->decoding()V

    .line 1858
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->settingView()V

    goto/16 :goto_0

    .line 1853
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z

    .line 1854
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getContentType(Landroid/net/Uri;)V

    goto :goto_2

    .line 1865
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v17    # "mUri":Landroid/net/Uri;
    :cond_9
    if-nez p2, :cond_0

    .line 1866
    const/16 v2, 0x5c

    move/from16 v0, p1

    if-ne v0, v2, :cond_0

    const-string v2, "DECODE_FAILURE"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1867
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFailureDecode:Z

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 11
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x5

    .line 1145
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1146
    const/4 v7, 0x6

    new-array v2, v7, [I

    fill-array-data v2, :array_0

    .line 1148
    .local v2, "layoutkeys":[I
    const/4 v6, 0x0

    .line 1149
    .local v6, "str":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, v2

    if-lt v1, v7, :cond_4

    .line 1157
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_6

    .line 1158
    const v7, 0x7f030001

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    .line 1168
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setLayoutId()V

    .line 1170
    iget v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eq v7, v10, :cond_1

    iget v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eq v7, v9, :cond_1

    .line 1171
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    if-eqz v7, :cond_1

    .line 1172
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v7}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1173
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v7}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 1174
    .local v5, "parent":Landroid/widget/FrameLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v5, v7}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1176
    .end local v5    # "parent":Landroid/widget/FrameLayout;
    :cond_0
    const/high16 v7, 0x7f090000

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1177
    .local v0, "blurredGroup":Landroid/widget/FrameLayout;
    if-eqz v0, :cond_1

    .line 1178
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 1182
    .end local v0    # "blurredGroup":Landroid/widget/FrameLayout;
    :cond_1
    iget v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-eq v7, v10, :cond_2

    iget v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMode:I

    if-ne v7, v9, :cond_3

    .line 1184
    :cond_2
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    if-eqz v7, :cond_3

    .line 1186
    iget-object v7, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPatternImg:Landroid/widget/ImageView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1190
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V

    .line 1191
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->settingView()V

    .line 1193
    const/4 v1, 0x0

    :goto_3
    array-length v7, v2

    if-lt v1, v7, :cond_9

    .line 1199
    return-void

    .line 1150
    :cond_4
    aget v7, v2, v1

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1151
    .local v3, "lvFocus":Landroid/widget/LinearLayout;
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1152
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "str":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 1153
    .restart local v6    # "str":Ljava/lang/String;
    goto :goto_1

    .line 1149
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1159
    .end local v3    # "lvFocus":Landroid/widget/LinearLayout;
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_7

    .line 1160
    const v7, 0x7f030003

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    goto :goto_2

    .line 1161
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_8

    .line 1163
    const v7, 0x7f030002

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    goto/16 :goto_2

    .line 1166
    :cond_8
    const/high16 v7, 0x7f030000

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    goto/16 :goto_2

    .line 1194
    :cond_9
    aget v7, v2, v1

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 1195
    .local v4, "newFocus":Landroid/widget/LinearLayout;
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v7

    if-ne v6, v7, :cond_a

    .line 1196
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 1193
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1146
    nop

    :array_0
    .array-data 4
        0x7f09000d
        0x7f090013
        0x7f090016
        0x7f090019
        0x7f09001c
        0x7f090003
    .end array-data
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v7, 0x100000

    const/16 v5, 0x400

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 216
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 218
    invoke-static {}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isLightThemeRequired()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 219
    const v3, 0x7f070003

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setTheme(I)V

    .line 220
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/sstudio/StudioActivity;->requestWindowFeature(I)Z

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 222
    .local v2, "win":Landroid/view/Window;
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 223
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v3, v7

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 224
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 225
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v3, v7

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 226
    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 230
    iput-object p0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;

    .line 232
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 233
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    .line 244
    :goto_0
    const-string v3, "magicshow"

    invoke-virtual {p0, v3, v6}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->prefs:Landroid/content/SharedPreferences;

    .line 245
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->prefs:Landroid/content/SharedPreferences;

    const-string v4, "magicshow"

    const-string v5, "false"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->bMagicShow:Ljava/lang/String;

    .line 247
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setLayoutId()V

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->getImage(Landroid/content/Intent;)V

    .line 249
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V

    .line 250
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setMode()V

    .line 251
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->decoding()V

    .line 252
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->settingView()V

    .line 254
    new-instance v3, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;-><init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    iput-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mReceiver:Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;

    .line 255
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 256
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v3, "file"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 257
    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mReceiver:Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 258
    iput-boolean v6, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->isFailureDecode:Z

    .line 259
    return-void

    .line 234
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 235
    const v3, 0x7f030003

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    goto :goto_0

    .line 237
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->deviceCheck()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 239
    const v3, 0x7f030002

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    goto :goto_0

    .line 241
    :cond_3
    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->setContentView(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 1917
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1919
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainBitmap:Landroid/graphics/Bitmap;

    .line 1920
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailBitmap:Landroid/graphics/Bitmap;

    .line 1921
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioQuickBitmap:Landroid/graphics/Bitmap;

    .line 1922
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoFrameBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoFrameBitmap:Landroid/graphics/Bitmap;

    .line 1923
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mFrameBitmap:Landroid/graphics/Bitmap;

    .line 1924
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailFrame:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1926
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 1927
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBitmap_masking_2:Landroid/graphics/Bitmap;

    .line 1929
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mThumbNailImage:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1931
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioNextImage:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1933
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoRightImageFrame:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1934
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mPhotoStudioImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1935
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mCollageStudioImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1936
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMagicShotImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1937
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoClipImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1938
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mVideoTrimImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->recycleBitmap2(Landroid/widget/ImageView;)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mReceiver:Lcom/sec/android/mimage/sstudio/StudioActivity$LocalBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1940
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1941
    return-void
.end method

.method public onDrawFinished()V
    .locals 1

    .prologue
    .line 2452
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->startBlur()V

    .line 2453
    return-void
.end method

.method public onFrameEnd()V
    .locals 2

    .prologue
    .line 2462
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2463
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2465
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 275
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 276
    const-string v0, " onPause"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 263
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 264
    const-string v0, " onResume"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mMainImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->mBlurredView:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    invoke-virtual {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->startAnimationOnResume()V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "magicshow"

    const-string v2, "false"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity;->bMagicShow:Ljava/lang/String;

    .line 270
    invoke-direct {p0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->checkAppInstalled()V

    .line 271
    return-void
.end method

.method public onStartBlurAnimation()V
    .locals 0

    .prologue
    .line 2458
    return-void
.end method

.method public setHovering(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 2467
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2468
    if-eqz p2, :cond_0

    .line 2470
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setHovered(Z)V

    .line 2471
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 2474
    :cond_0
    return-void
.end method
