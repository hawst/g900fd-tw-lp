.class Lcom/sec/android/mimage/sstudio/StudioActivity$16;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->showAlertDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

.field private final synthetic val$checkbox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    iput-object p2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->val$checkbox:Landroid/widget/CheckBox;

    .line 2174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2179
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->prefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$22(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$23(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/SharedPreferences$Editor;)V

    .line 2180
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->val$checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2181
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$24(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "magicshow"

    const-string v2, "true"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2185
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$24(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2187
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->isNoSelectPic:Z
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$25(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2188
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->goMagicShotThroughGallery()V
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$8(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    .line 2192
    :goto_1
    return-void

    .line 2183
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$24(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "magicshow"

    const-string v2, "false"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 2190
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$16;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->goShotMoreStudio()V
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$16(Lcom/sec/android/mimage/sstudio/StudioActivity;)V

    goto :goto_1
.end method
