.class Lcom/sec/android/mimage/sstudio/StudioActivity$17;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->showDownloadablePopup(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

.field private final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$17;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    iput-object p2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$17;->val$url:Ljava/lang/String;

    .line 2305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2311
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2312
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$17;->val$url:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2313
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2314
    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$17;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$13(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2319
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 2315
    :catch_0
    move-exception v0

    .line 2317
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
