.class public Lcom/sec/android/mimage/blureffect/Define;
.super Ljava/lang/Object;
.source "Define.java"


# static fields
.field public static DEBUG:Z = false

.field public static final DURATION:I = 0x3e8

.field public static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/mimage/blureffect/Define;->DEBUG:Z

    .line 9
    const-string v0, "Blur Effect"

    sput-object v0, Lcom/sec/android/mimage/blureffect/Define;->TAG:Ljava/lang/String;

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addLog(Ljava/lang/String;)V
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 14
    sget-boolean v0, Lcom/sec/android/mimage/blureffect/Define;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 15
    sget-object v0, Lcom/sec/android/mimage/blureffect/Define;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 16
    :cond_0
    return-void
.end method
