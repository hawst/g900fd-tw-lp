.class Lcom/sec/android/mimage/sstudio/StudioActivity$10;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    .line 1619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1622
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1623
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    const-string v2, "com.sec.android.app.videoplayer"

    const-string v3, "TRIM"

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->insertLog(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v2, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$10(Lcom/sec/android/mimage/sstudio/StudioActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 1625
    .local v7, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->getFilePathFromUriforVideo(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0, v7}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$17(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 1626
    .local v6, "path":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.TRIM"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1627
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 1628
    const-string v0, "com.lifevibes.trimapp"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1629
    const-string v0, "uri"

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1630
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    const/16 v2, 0x5e

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$10;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mBVideoTrimStudioInstall:Z
    invoke-static {v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$11(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z

    move-result v3

    .line 1631
    const-string v4, "samsungapps://ProductDetail/com.lifevibes.trimapp"

    const v5, 0x7f060184

    .line 1630
    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$4(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Intent;IZLjava/lang/String;I)V

    .line 1634
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method
