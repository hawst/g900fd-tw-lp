.class Lcom/sec/android/mimage/blureffect/GaussianLayer$1;
.super Ljava/lang/Object;
.source "GaussianLayer.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/blureffect/GaussianLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/blureffect/GaussianLayer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 8
    .param p1, "arg0"    # Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .prologue
    const/high16 v7, 0x41700000    # 15.0f

    const v6, 0x3e4ccccd    # 0.2f

    .line 324
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    if-nez v2, :cond_0

    .line 325
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    new-instance v3, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    invoke-static {v2, v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$1(Lcom/sec/android/mimage/blureffect/GaussianLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 326
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 329
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F
    invoke-static {v4}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$2(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F

    move-result v4

    mul-float/2addr v4, v6

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F
    invoke-static {v5}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$3(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F

    move-result v5

    mul-float/2addr v5, v6

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 330
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setOpacity(F)V

    .line 334
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mRadius:F
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$4(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F

    move-result v2

    .line 335
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    div-float/2addr v2, v3

    div-float/2addr v2, v7

    .line 334
    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(F)V

    .line 336
    .local v0, "deltaX":Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "radius"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "SGTexture"

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    iget-object v4, v4, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 338
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$5(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 339
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$6(Lcom/sec/android/mimage/blureffect/GaussianLayer;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 344
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mRadius:F
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$4(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F

    move-result v2

    .line 345
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    div-float/2addr v2, v3

    div-float/2addr v2, v7

    .line 344
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(F)V

    .line 346
    .local v1, "deltaY":Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "radius"

    invoke-virtual {v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 347
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "SGTexture"

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_x_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;
    invoke-static {v4}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$7(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 348
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mVProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$8(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 349
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$9(Lcom/sec/android/mimage/blureffect/GaussianLayer;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 352
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F
    invoke-static {v4}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$2(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F
    invoke-static {v5}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$3(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 354
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "textureBlured"

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_y_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;
    invoke-static {v4}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$10(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 355
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "SGTexture"

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    iget-object v4, v4, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 356
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    const-string v3, "uBlend"

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    invoke-static {v4}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$11(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 357
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$12(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 359
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 360
    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;->this$0:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    # getter for: Lcom/sec/android/mimage/blureffect/GaussianLayer;->mContentRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->access$13(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    .line 361
    return-void
.end method
