.class Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;
.super Ljava/lang/Object;
.source "SGTextureViewBlurred.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;Lcom/sec/android/mimage/blureffect/SGListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResumed()V
    .locals 2

    .prologue
    .line 80
    const-string v0, "SGSurfaceStateListener onResumed"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$1(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$2(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/GaussianLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->updateBlurredImage()V

    .line 85
    :cond_0
    return-void
.end method

.method public onSuspended()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "SGSurfaceStateListener onSuspended"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 76
    return-void
.end method
