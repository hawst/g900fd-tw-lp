.class Lcom/sec/android/mimage/sstudio/StudioActivity$7;
.super Ljava/lang/Object;
.source "StudioActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/sstudio/StudioActivity;->createListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/sstudio/StudioActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    .line 1573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mActionBarLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$2(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1577
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->isFailureDecode:Z
    invoke-static {v0}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$12(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1578
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$13(Lcom/sec/android/mimage/sstudio/StudioActivity;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0601b9

    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->showToast(Landroid/content/Context;I)V
    invoke-static {v0, v2, v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$14(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Context;I)V

    .line 1588
    :cond_0
    :goto_0
    return-void

    .line 1580
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.sec.android.mimage.photoretouching.multigrid"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1581
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "selectedItems"

    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$0(Lcom/sec/android/mimage/sstudio/StudioActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1582
    const-string v0, "selectedCount"

    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$15(Lcom/sec/android/mimage/sstudio/StudioActivity;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1583
    const-string v0, "selectedCount"

    iget-object v2, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->real_sum:I
    invoke-static {v2}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$15(Lcom/sec/android/mimage/sstudio/StudioActivity;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1584
    iget-object v0, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    const/16 v2, 0x5c

    iget-object v3, p0, Lcom/sec/android/mimage/sstudio/StudioActivity$7;->this$0:Lcom/sec/android/mimage/sstudio/StudioActivity;

    # getter for: Lcom/sec/android/mimage/sstudio/StudioActivity;->mBCollageStudioInstall:Z
    invoke-static {v3}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$5(Lcom/sec/android/mimage/sstudio/StudioActivity;)Z

    move-result v3

    .line 1585
    const-string v4, "samsungapps://ProductDetail/com.sec.android.mimage.photoretouching"

    const v5, 0x7f060182

    .line 1584
    # invokes: Lcom/sec/android/mimage/sstudio/StudioActivity;->startActivity(Landroid/content/Intent;IZLjava/lang/String;I)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/sstudio/StudioActivity;->access$4(Lcom/sec/android/mimage/sstudio/StudioActivity;Landroid/content/Intent;IZLjava/lang/String;I)V

    goto :goto_0
.end method
