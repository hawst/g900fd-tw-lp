.class public Lcom/sec/android/mimage/blureffect/FakeLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "FakeLayer.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;


# instance fields
.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mContentRect:Landroid/graphics/RectF;

.field private mIsAnimation:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 15
    iput v1, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    .line 16
    iput v1, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    .line 17
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mContentRect:Landroid/graphics/RectF;

    .line 18
    iput-boolean v1, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mIsAnimation:Z

    .line 13
    return-void
.end method

.method private setFitBitmapRatio()V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    .line 72
    iget v7, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    int-to-float v7, v7

    cmpl-float v7, v7, v9

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    int-to-float v7, v7

    cmpl-float v7, v7, v9

    if-nez v7, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v7

    cmpl-float v7, v7, v9

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    cmpl-float v7, v7, v9

    if-eqz v7, :cond_0

    .line 80
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v9, v9, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 82
    .local v4, "rect":Landroid/graphics/RectF;
    iget v7, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    int-to-float v8, v8

    div-float v0, v7, v8

    .line 83
    .local v0, "bitmap_ratio":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    div-float v2, v7, v8

    .line 85
    .local v2, "layerImage_ratio":F
    cmpg-float v7, v0, v2

    if-gez v7, :cond_3

    .line 86
    iget v7, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    mul-float/2addr v7, v8

    div-float/2addr v7, v12

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    div-float/2addr v7, v8

    sub-float v3, v11, v7

    .line 87
    .local v3, "left":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    add-float v5, v3, v7

    .line 89
    .local v5, "right":F
    invoke-virtual {v4, v3, v9, v5, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 104
    .end local v3    # "left":F
    .end local v5    # "right":F
    :cond_2
    :goto_1
    iput-object v4, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mContentRect:Landroid/graphics/RectF;

    .line 105
    iget-object v7, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mContentRect:Landroid/graphics/RectF;

    invoke-super {p0, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 91
    :cond_3
    cmpl-float v7, v0, v2

    if-lez v7, :cond_2

    .line 92
    iget v7, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    mul-float/2addr v7, v8

    div-float/2addr v7, v12

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    div-float/2addr v7, v8

    sub-float v6, v11, v7

    .line 93
    .local v6, "top":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    add-float v1, v6, v7

    .line 95
    .local v1, "bottom":F
    invoke-virtual {v4, v9, v6, v10, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method


# virtual methods
.method public getAnimation()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mIsAnimation:Z

    return v0
.end method

.method public onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 133
    const-string v0, "animation cancel"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public onDiscarded(I)V
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 128
    const-string v0, "animation discard"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 129
    return-void
.end method

.method public onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 122
    const-string v0, "animation finish"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 123
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setAnimation(Z)V

    .line 124
    return-void
.end method

.method public onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 117
    const-string v0, "animation repeat"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 110
    const-string v0, "animation start"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 111
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setAnimation(Z)V

    .line 113
    return-void
.end method

.method public setAnimation(Z)V
    .locals 2
    .param p1, "isAnimation"    # Z

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setAnimation "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 23
    iput-boolean p1, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mIsAnimation:Z

    .line 24
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapWidth:I

    .line 45
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mBitmapHeight:I

    .line 47
    invoke-direct {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setFitBitmapRatio()V

    .line 49
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 50
    return-void
.end method

.method public setSize(FF)V
    .locals 0
    .param p1, "arg0"    # F
    .param p2, "arg1"    # F

    .prologue
    .line 32
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 33
    invoke-direct {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setFitBitmapRatio()V

    .line 34
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0
    .param p1, "arg0"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 39
    invoke-direct {p0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setFitBitmapRatio()V

    .line 40
    return-void
.end method

.method public startBlur(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 54
    iget-boolean v1, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mIsAnimation:Z

    if-eqz v1, :cond_0

    .line 55
    const-string v1, "mIsAnimation is true"

    invoke-static {v1}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 69
    :goto_0
    return-void

    .line 59
    :cond_0
    const-string v1, "!!!!!!!!!!!!!! FakeLayer startBlur"

    invoke-static {v1}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 61
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationFactory;->createOpacityAnimation()Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    move-result-object v0

    .line 62
    .local v0, "opacityAni":Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setStartValue(F)V

    .line 63
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setEndValue(F)V

    .line 64
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setDuration(I)V

    .line 65
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 66
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    .line 68
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/blureffect/FakeLayer;->mIsAnimation:Z

    goto :goto_0
.end method
