.class public Lcom/msc/seclib/GroupConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field auth_type:C

.field cc:Ljava/lang/String;

.field email:Ljava/lang/String;

.field fwk_target:C

.field group_id:Ljava/lang/String;

.field guid:Ljava/lang/String;

.field mcc:I

.field server_type:C

.field token:Ljava/lang/String;

.field token_secret:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuth_type()C
    .locals 1

    .prologue
    .line 59
    iget-char v0, p0, Lcom/msc/seclib/GroupConfig;->auth_type:C

    return v0
.end method

.method public getCc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/msc/seclib/GroupConfig;->cc:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/msc/seclib/GroupConfig;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getFwk_target()C
    .locals 1

    .prologue
    .line 43
    iget-char v0, p0, Lcom/msc/seclib/GroupConfig;->fwk_target:C

    return v0
.end method

.method public getGroup_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/msc/seclib/GroupConfig;->group_id:Ljava/lang/String;

    return-object v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/msc/seclib/GroupConfig;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getMcc()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/msc/seclib/GroupConfig;->mcc:I

    return v0
.end method

.method public getServer_type()C
    .locals 1

    .prologue
    .line 19
    iget-char v0, p0, Lcom/msc/seclib/GroupConfig;->server_type:C

    return v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/msc/seclib/GroupConfig;->token:Ljava/lang/String;

    return-object v0
.end method

.method public getToken_secret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/msc/seclib/GroupConfig;->token_secret:Ljava/lang/String;

    return-object v0
.end method

.method public setAuth_type(C)V
    .locals 0

    .prologue
    .line 63
    iput-char p1, p0, Lcom/msc/seclib/GroupConfig;->auth_type:C

    .line 64
    return-void
.end method

.method public setCc(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/msc/seclib/GroupConfig;->cc:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/msc/seclib/GroupConfig;->email:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setFwk_target(C)V
    .locals 0

    .prologue
    .line 47
    iput-char p1, p0, Lcom/msc/seclib/GroupConfig;->fwk_target:C

    .line 48
    return-void
.end method

.method public setGroup_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/msc/seclib/GroupConfig;->group_id:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/msc/seclib/GroupConfig;->guid:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setMcc(I)V
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/msc/seclib/GroupConfig;->mcc:I

    .line 32
    return-void
.end method

.method public setServer_type(C)V
    .locals 0

    .prologue
    .line 23
    iput-char p1, p0, Lcom/msc/seclib/GroupConfig;->server_type:C

    .line 24
    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/msc/seclib/GroupConfig;->token:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setToken_secret(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/msc/seclib/GroupConfig;->token_secret:Ljava/lang/String;

    .line 80
    return-void
.end method
