.class public Lcom/msc/seclib/PeerInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x3ee7053bb22b45b5L


# instance fields
.field group_id:Ljava/lang/String;

.field instance_id:Ljava/lang/String;

.field is_fw:Z

.field local_ip:Ljava/lang/String;

.field mapped_ip:Ljava/lang/String;

.field mapped_port:I

.field nat_type:S

.field network_type:S

.field peer_id:Ljava/lang/String;

.field peer_status:S

.field service_port:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private nullOrSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<Secret>"

    goto :goto_0
.end method


# virtual methods
.method public getGroup_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/msc/seclib/PeerInfo;->group_id:Ljava/lang/String;

    return-object v0
.end method

.method public getInstance_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/msc/seclib/PeerInfo;->instance_id:Ljava/lang/String;

    return-object v0
.end method

.method public getLocal_ip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/msc/seclib/PeerInfo;->local_ip:Ljava/lang/String;

    return-object v0
.end method

.method public getMapped_ip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/msc/seclib/PeerInfo;->mapped_ip:Ljava/lang/String;

    return-object v0
.end method

.method public getMapped_port()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/msc/seclib/PeerInfo;->mapped_port:I

    return v0
.end method

.method public getNat_type()S
    .locals 1

    .prologue
    .line 149
    iget-short v0, p0, Lcom/msc/seclib/PeerInfo;->nat_type:S

    return v0
.end method

.method public getNetwork_type()S
    .locals 1

    .prologue
    .line 176
    iget-short v0, p0, Lcom/msc/seclib/PeerInfo;->network_type:S

    return v0
.end method

.method public getPeer_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/msc/seclib/PeerInfo;->peer_id:Ljava/lang/String;

    return-object v0
.end method

.method public getPeer_status()S
    .locals 1

    .prologue
    .line 74
    iget-short v0, p0, Lcom/msc/seclib/PeerInfo;->peer_status:S

    return v0
.end method

.method public getService_port()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/msc/seclib/PeerInfo;->service_port:I

    return v0
.end method

.method public isfw()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/msc/seclib/PeerInfo;->is_fw:Z

    return v0
.end method

.method public setGroup_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/msc/seclib/PeerInfo;->group_id:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setInstance_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/msc/seclib/PeerInfo;->instance_id:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setLocal_ip(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/msc/seclib/PeerInfo;->local_ip:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setMapped_ip(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/msc/seclib/PeerInfo;->mapped_ip:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public setMapped_port(I)V
    .locals 0

    .prologue
    .line 142
    iput p1, p0, Lcom/msc/seclib/PeerInfo;->mapped_port:I

    .line 143
    return-void
.end method

.method public setNat_type(S)V
    .locals 0

    .prologue
    .line 157
    iput-short p1, p0, Lcom/msc/seclib/PeerInfo;->nat_type:S

    .line 158
    return-void
.end method

.method public setNetwork_type(S)V
    .locals 0

    .prologue
    .line 180
    iput-short p1, p0, Lcom/msc/seclib/PeerInfo;->network_type:S

    .line 181
    return-void
.end method

.method public setPeer_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/msc/seclib/PeerInfo;->peer_id:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setPeer_status(S)V
    .locals 0

    .prologue
    .line 82
    iput-short p1, p0, Lcom/msc/seclib/PeerInfo;->peer_status:S

    .line 83
    return-void
.end method

.method public setService_port(I)V
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/msc/seclib/PeerInfo;->service_port:I

    .line 113
    return-void
.end method

.method public setfw(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/msc/seclib/PeerInfo;->is_fw:Z

    .line 173
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PeerInfo[peer_id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/msc/seclib/PeerInfo;->peer_id:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/msc/seclib/PeerInfo;->nullOrSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", group_id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/seclib/PeerInfo;->group_id:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/msc/seclib/PeerInfo;->nullOrSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", instance_id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/msc/seclib/PeerInfo;->instance_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", peer_status:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/msc/seclib/PeerInfo;->peer_status:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", network_type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/msc/seclib/PeerInfo;->network_type:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nat_type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/msc/seclib/PeerInfo;->nat_type:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
