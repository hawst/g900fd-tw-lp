.class public interface abstract Lcom/msc/seclib/SecLibCallbacks;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I
.end method

.method public abstract netChangedNotify(C)V
.end method

.method public abstract peerConnNotify(Lcom/msc/seclib/PeerInfo;IC)I
.end method

.method public abstract presConnNotify()V
.end method

.method public abstract presDisconnNotify(I)I
.end method

.method public abstract smsNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract smsNotifyEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract terminateNotify()I
.end method
