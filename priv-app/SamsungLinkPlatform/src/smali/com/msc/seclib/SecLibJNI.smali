.class public Lcom/msc/seclib/SecLibJNI;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel; = null

.field private static final TAG:Ljava/lang/String; = "mfl_nts_SecLibJNI"

.field private static bNoSecLib:Z

.field private static callback_:Lcom/msc/seclib/SecLibCallbacks;

.field private static instance_:Lcom/msc/seclib/SecLibJNI;

.field private static final logger:Lorg/slf4j/Logger;

.field private static retry_flag_:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    const-class v0, Lcom/msc/seclib/SecLibJNI;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 18
    sput-object v0, Lcom/msc/seclib/SecLibJNI;->logger:Lorg/slf4j/Logger;

    const-string v1, "SecLibJNI static init"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/msc/seclib/SecLibJNI;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 23
    new-instance v0, Lcom/msc/seclib/SecLibJNI;

    invoke-direct {v0}, Lcom/msc/seclib/SecLibJNI;-><init>()V

    sput-object v0, Lcom/msc/seclib/SecLibJNI;->instance_:Lcom/msc/seclib/SecLibJNI;

    .line 24
    new-instance v0, Lcom/msc/seclib/SecLibCallbacksDefaultImpl;

    invoke-direct {v0}, Lcom/msc/seclib/SecLibCallbacksDefaultImpl;-><init>()V

    sput-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    .line 25
    const/4 v0, -0x1

    sput v0, Lcom/msc/seclib/SecLibJNI;->retry_flag_:I

    .line 27
    sput-boolean v2, Lcom/msc/seclib/SecLibJNI;->bNoSecLib:Z

    .line 32
    :try_start_0
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->logger:Lorg/slf4j/Logger;

    const-string v1, "static init: loading SLPF_scone_stub"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 33
    const-string v0, "SLPF_scone_stub"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 34
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->logger:Lorg/slf4j/Logger;

    const-string v1, "static init: loaded SLPF_scone_stub"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lcom/msc/seclib/SecLibJNI;->bNoSecLib:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    new-instance v1, Ljava/io/File;

    const-string v2, "/system/lib/libscone_stub.so"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->logger:Lorg/slf4j/Logger;

    const-string v1, "static init: loading scone_stub"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 41
    :try_start_1
    const-string v0, "scone_stub"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 42
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->logger:Lorg/slf4j/Logger;

    const-string v1, "static init: loaded scone_stub"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/msc/seclib/SecLibJNI;->bNoSecLib:Z
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 45
    :catch_1
    move-exception v0

    sput-boolean v3, Lcom/msc/seclib/SecLibJNI;->bNoSecLib:Z

    .line 46
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->logger:Lorg/slf4j/Logger;

    const-string v1, "SCS error(UnsatisfiedLinkError): no scs stub so files!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_1
    sput-boolean v3, Lcom/msc/seclib/SecLibJNI;->bNoSecLib:Z

    .line 50
    sget-object v1, Lcom/msc/seclib/SecLibJNI;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_0

    .line 51
    const-string v1, "mfl_nts_SecLibJNI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::could not load NTS library: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public static getInstance()Lcom/msc/seclib/SecLibJNI;
    .locals 1

    .prologue
    .line 62
    sget-boolean v0, Lcom/msc/seclib/SecLibJNI;->bNoSecLib:Z

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->instance_:Lcom/msc/seclib/SecLibJNI;

    goto :goto_0
.end method

.method private groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0, p1}, Lcom/msc/seclib/SecLibCallbacks;->groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I

    move-result v0

    return v0
.end method

.method private netChangedNotify(C)V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0, p1}, Lcom/msc/seclib/SecLibCallbacks;->netChangedNotify(C)V

    .line 114
    return-void
.end method

.method private peerConnNotify(Lcom/msc/seclib/PeerInfo;IC)I
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0, p1, p2, p3}, Lcom/msc/seclib/SecLibCallbacks;->peerConnNotify(Lcom/msc/seclib/PeerInfo;IC)I

    move-result v0

    return v0
.end method

.method private presConnNotify()V
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0}, Lcom/msc/seclib/SecLibCallbacks;->presConnNotify()V

    .line 102
    return-void
.end method

.method private presDisconnNotify(I)I
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0, p1}, Lcom/msc/seclib/SecLibCallbacks;->presDisconnNotify(I)I

    move-result v0

    .line 97
    sput v0, Lcom/msc/seclib/SecLibJNI;->retry_flag_:I

    return v0
.end method

.method public static registerCallback(Lcom/msc/seclib/SecLibCallbacks;)V
    .locals 0

    .prologue
    .line 82
    sput-object p0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    .line 83
    return-void
.end method

.method private smsNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/msc/seclib/SecLibCallbacks;->smsNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 106
    return-void
.end method

.method private smsNotifyEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 109
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/msc/seclib/SecLibCallbacks;->smsNotifyEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    return-void
.end method

.method private terminateNotify()I
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/msc/seclib/SecLibJNI;->callback_:Lcom/msc/seclib/SecLibCallbacks;

    invoke-interface {v0}, Lcom/msc/seclib/SecLibCallbacks;->terminateNotify()I

    move-result v0

    return v0
.end method


# virtual methods
.method public native addGroup(Lcom/msc/seclib/GroupConfig;)I
.end method

.method public native addGroup_ex(Lcom/msc/seclib/GroupConfig;Ljava/lang/String;C)I
.end method

.method public native close(II)I
.end method

.method public native connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/seclib/ConnInfo;)I
.end method

.method public native connect_ex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;CLcom/msc/seclib/ConnInfo;)I
.end method

.method public native connect_udp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/seclib/ConnInfo;)I
.end method

.method public native getConfig(Ljava/lang/String;Lcom/msc/seclib/CoreConfig;)I
.end method

.method public native getConnList(Ljava/lang/String;Ljava/util/Vector;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Vector",
            "<",
            "Lcom/msc/seclib/ConnInfo;",
            ">;)I"
        }
    .end annotation
.end method

.method public native getDeviceType()I
.end method

.method public native getGroupList(Ljava/util/Vector;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation
.end method

.method public native getGroupPeerList(Ljava/lang/String;Ljava/util/Vector;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Vector",
            "<",
            "Lcom/msc/seclib/PeerInfo;",
            ">;)I"
        }
    .end annotation
.end method

.method public native getInstancePeerList(Ljava/lang/String;Ljava/lang/String;Ljava/util/Vector;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Vector",
            "<",
            "Lcom/msc/seclib/PeerInfo;",
            ">;)I"
        }
    .end annotation
.end method

.method public native getIsLoggedin()Z
.end method

.method public native getLocalPeerList(Ljava/util/Vector;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/msc/seclib/PeerInfo;",
            ">;)I"
        }
    .end annotation
.end method

.method public native getOSType()I
.end method

.method public native getPeerInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/seclib/PeerInfo;)I
.end method

.method public native getStatus()I
.end method

.method public native getVersion()Ljava/lang/String;
.end method

.method public native initializeCore(Lcom/msc/seclib/CoreConfig;)I
.end method

.method public native recv(I[BII)I
.end method

.method public native removeGroup(Ljava/lang/String;)I
.end method

.method public native send(I[BIZI)I
.end method

.method public native sendOffset(I[BIIIZI)I
.end method

.method public native sendSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public native sendSMS_ex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public native terminateCore()V
.end method
