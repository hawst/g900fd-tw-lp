.class public Lcom/msc/seclib/CoreConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field app_secret:Ljava/lang/String;

.field appid:Ljava/lang/String;

.field auth_type:C

.field cc:Ljava/lang/String;

.field ctimeout_sec_plain_udp:I

.field ctimeout_sec_tcp:I

.field ctimeout_sec_turn:I

.field ctimeout_sec_udp:I

.field email:Ljava/lang/String;

.field fwk_target:C

.field group_id:Ljava/lang/String;

.field guid:Ljava/lang/String;

.field instance_id:Ljava/lang/String;

.field log_level:I

.field log_path:Ljava/lang/String;

.field mcc:I

.field peer_id:Ljava/lang/String;

.field presence_domain:Ljava/lang/String;

.field presence_port:C

.field proxy_port_instance:C

.field server_type:C

.field service_port:C

.field stun_domain:Ljava/lang/String;

.field stun_port:C

.field token:Ljava/lang/String;

.field token_secret:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApp_secret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->app_secret:Ljava/lang/String;

    return-object v0
.end method

.method public getAppid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->appid:Ljava/lang/String;

    return-object v0
.end method

.method public getAuth_type()C
    .locals 1

    .prologue
    .line 187
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->auth_type:C

    return v0
.end method

.method public getCc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->cc:Ljava/lang/String;

    return-object v0
.end method

.method public getCtimeout_sec_plain_udp()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_plain_udp:I

    return v0
.end method

.method public getCtimeout_sec_tcp()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_tcp:I

    return v0
.end method

.method public getCtimeout_sec_turn()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_turn:I

    return v0
.end method

.method public getCtimeout_sec_udp()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_udp:I

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getFwk_target()C
    .locals 1

    .prologue
    .line 269
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->fwk_target:C

    return v0
.end method

.method public getGroup_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->group_id:Ljava/lang/String;

    return-object v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getInstance_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->instance_id:Ljava/lang/String;

    return-object v0
.end method

.method public getLog_level()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/msc/seclib/CoreConfig;->log_level:I

    return v0
.end method

.method public getLog_path()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->log_path:Ljava/lang/String;

    return-object v0
.end method

.method public getMcc()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/msc/seclib/CoreConfig;->mcc:I

    return v0
.end method

.method public getPeer_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->peer_id:Ljava/lang/String;

    return-object v0
.end method

.method public getPresence_domain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->presence_domain:Ljava/lang/String;

    return-object v0
.end method

.method public getPresence_port()C
    .locals 1

    .prologue
    .line 131
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->presence_port:C

    return v0
.end method

.method public getProxy_port_instance()C
    .locals 1

    .prologue
    .line 245
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->proxy_port_instance:C

    return v0
.end method

.method public getServer_type()C
    .locals 1

    .prologue
    .line 277
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->server_type:C

    return v0
.end method

.method public getService_port()C
    .locals 1

    .prologue
    .line 119
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->service_port:C

    return v0
.end method

.method public getStun_domain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->stun_domain:Ljava/lang/String;

    return-object v0
.end method

.method public getStun_port()C
    .locals 1

    .prologue
    .line 139
    iget-char v0, p0, Lcom/msc/seclib/CoreConfig;->stun_port:C

    return v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->token:Ljava/lang/String;

    return-object v0
.end method

.method public getToken_secret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/msc/seclib/CoreConfig;->token_secret:Ljava/lang/String;

    return-object v0
.end method

.method public setApp_secret(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->app_secret:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public setAppid(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->appid:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public setAuth_type(C)V
    .locals 0

    .prologue
    .line 191
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->auth_type:C

    .line 192
    return-void
.end method

.method public setCc(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->cc:Ljava/lang/String;

    .line 266
    return-void
.end method

.method public setCtimeout_sec_plain_udp(I)V
    .locals 0

    .prologue
    .line 183
    iput p1, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_plain_udp:I

    .line 184
    return-void
.end method

.method public setCtimeout_sec_tcp(I)V
    .locals 0

    .prologue
    .line 159
    iput p1, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_tcp:I

    .line 160
    return-void
.end method

.method public setCtimeout_sec_turn(I)V
    .locals 0

    .prologue
    .line 175
    iput p1, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_turn:I

    .line 176
    return-void
.end method

.method public setCtimeout_sec_udp(I)V
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_udp:I

    .line 168
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->email:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public setFwk_target(C)V
    .locals 0

    .prologue
    .line 273
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->fwk_target:C

    .line 274
    return-void
.end method

.method public setGroup_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->group_id:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->guid:Ljava/lang/String;

    .line 242
    return-void
.end method

.method public setInstance_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->instance_id:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setLog_level(I)V
    .locals 0

    .prologue
    .line 151
    iput p1, p0, Lcom/msc/seclib/CoreConfig;->log_level:I

    .line 152
    return-void
.end method

.method public setLog_path(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->log_path:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setMcc(I)V
    .locals 0

    .prologue
    .line 257
    iput p1, p0, Lcom/msc/seclib/CoreConfig;->mcc:I

    .line 258
    return-void
.end method

.method public setPeer_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->peer_id:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setPresence_domain(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->presence_domain:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setPresence_port(C)V
    .locals 0

    .prologue
    .line 135
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->presence_port:C

    .line 136
    return-void
.end method

.method public setProxy_port_instance(C)V
    .locals 0

    .prologue
    .line 249
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->proxy_port_instance:C

    .line 250
    return-void
.end method

.method public setServer_type(C)V
    .locals 0

    .prologue
    .line 281
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->server_type:C

    .line 282
    return-void
.end method

.method public setService_port(C)V
    .locals 0

    .prologue
    .line 127
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->service_port:C

    .line 128
    return-void
.end method

.method public setStun_domain(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->stun_domain:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setStun_port(C)V
    .locals 0

    .prologue
    .line 143
    iput-char p1, p0, Lcom/msc/seclib/CoreConfig;->stun_port:C

    .line 144
    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->token:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setToken_secret(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/msc/seclib/CoreConfig;->token_secret:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    const-string v1, "\n group_id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->group_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v1, "\n instance_id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->instance_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    const-string v1, "\n peer_id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->peer_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const-string v1, "\n presence_domain : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->presence_domain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string v1, "\n stun_domain : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->stun_domain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string v1, "\n service_port : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->service_port:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 300
    const-string v1, "\n presence_port : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->presence_port:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 301
    const-string v1, "\n stun_port : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->stun_port:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 302
    const-string v1, "\n log_level : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/msc/seclib/CoreConfig;->log_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 303
    const-string v1, "\n ctimeout_sec_tcp : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_tcp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 304
    const-string v1, "\n ctimeout_sec_udp : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_udp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 305
    const-string v1, "\n ctimeout_sec_plain_udp : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/msc/seclib/CoreConfig;->ctimeout_sec_plain_udp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 306
    const-string v1, "\n log_path : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->log_path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const-string v1, "\n auth_type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->auth_type:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 308
    const-string v1, "\n token : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const-string v1, "\n token_secret : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->token_secret:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v1, "\n email : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string v1, "\n appid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->appid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string v1, "\n guid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string v1, "\n proxy_port_instance : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->proxy_port_instance:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 314
    const-string v1, "\n mcc : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/msc/seclib/CoreConfig;->mcc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 315
    const-string v1, "\n cc : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/msc/seclib/CoreConfig;->cc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string v1, "\n fwk_target : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->fwk_target:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 317
    const-string v1, "\n server_type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lcom/msc/seclib/CoreConfig;->server_type:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 319
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
