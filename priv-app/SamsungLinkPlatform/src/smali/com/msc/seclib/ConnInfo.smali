.class public Lcom/msc/seclib/ConnInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field conn_id:I

.field conn_type:C

.field remote_peer_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getConn_id()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/msc/seclib/ConnInfo;->conn_id:I

    return v0
.end method

.method public getConn_type()C
    .locals 1

    .prologue
    .line 29
    iget-char v0, p0, Lcom/msc/seclib/ConnInfo;->conn_type:C

    return v0
.end method

.method public getRemote_peer_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/msc/seclib/ConnInfo;->remote_peer_id:Ljava/lang/String;

    return-object v0
.end method

.method public setConn_id(I)V
    .locals 0

    .prologue
    .line 22
    iput p1, p0, Lcom/msc/seclib/ConnInfo;->conn_id:I

    .line 23
    return-void
.end method

.method public setConn_type(C)V
    .locals 0

    .prologue
    .line 37
    iput-char p1, p0, Lcom/msc/seclib/ConnInfo;->conn_type:C

    .line 38
    return-void
.end method

.method public setRemote_peer_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/msc/seclib/ConnInfo;->remote_peer_id:Ljava/lang/String;

    .line 53
    return-void
.end method
