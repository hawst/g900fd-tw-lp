.class public Lcom/msc/seclib/SecLibCallbacksDefaultImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/msc/seclib/SecLibCallbacks;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    return v0
.end method

.method public netChangedNotify(C)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public peerConnNotify(Lcom/msc/seclib/PeerInfo;IC)I
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    return v0
.end method

.method public presConnNotify()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method public presDisconnNotify(I)I
    .locals 1

    .prologue
    .line 18
    const/4 v0, -0x1

    return v0
.end method

.method public smsNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public smsNotifyEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public terminateNotify()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method
