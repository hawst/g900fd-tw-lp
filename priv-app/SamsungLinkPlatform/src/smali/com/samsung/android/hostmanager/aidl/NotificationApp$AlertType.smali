.class public final enum Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/hostmanager/aidl/NotificationApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AlertType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

.field public static final enum b:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

.field public static final enum c:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

.field private static final synthetic d:[Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    const-string v1, "FULL_SCREEN_POPUP"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->a:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    .line 18
    new-instance v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    const-string v1, "MINI_POPUP"

    invoke-direct {v0, v1, v3, v3}, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->b:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    .line 19
    new-instance v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    const-string v1, "NO_ALERT"

    invoke-direct {v0, v1, v4, v4}, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->c:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    sget-object v1, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->a:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->b:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->c:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->d:[Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->value:I

    .line 25
    return-void
.end method

.method public static a(I)Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;
    .locals 1

    .prologue
    .line 32
    packed-switch p0, :pswitch_data_0

    .line 40
    sget-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->a:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    :goto_0
    return-object v0

    .line 34
    :pswitch_0
    sget-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->a:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    goto :goto_0

    .line 36
    :pswitch_1
    sget-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->b:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    goto :goto_0

    .line 38
    :pswitch_2
    sget-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->c:Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->d:[Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/android/hostmanager/aidl/NotificationApp$AlertType;->value:I

    return v0
.end method
