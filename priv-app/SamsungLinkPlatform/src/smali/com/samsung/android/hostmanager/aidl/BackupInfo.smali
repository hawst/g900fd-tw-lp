.class public Lcom/samsung/android/hostmanager/aidl/BackupInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/hostmanager/aidl/BackupInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/samsung/android/hostmanager/aidl/BackupInfo$1;

    invoke-direct {v0}, Lcom/samsung/android/hostmanager/aidl/BackupInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 63
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 65
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->a:Ljava/lang/String;

    .line 67
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->b:Ljava/lang/String;

    .line 69
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->c:Ljava/lang/String;

    .line 71
    const/4 v1, 0x3

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->d:Ljava/lang/String;

    .line 73
    const/4 v1, 0x4

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->e:Ljava/lang/String;

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 79
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 80
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 81
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->a:Ljava/lang/String;

    .line 82
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->b:Ljava/lang/String;

    .line 83
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->c:Ljava/lang/String;

    .line 84
    const/4 v1, 0x3

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->d:Ljava/lang/String;

    .line 85
    const/4 v1, 0x4

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->e:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 93
    iget-object v2, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/samsung/android/hostmanager/aidl/BackupInfo;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 92
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 96
    return-void
.end method
