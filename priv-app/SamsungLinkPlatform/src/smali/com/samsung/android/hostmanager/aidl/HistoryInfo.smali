.class public Lcom/samsung/android/hostmanager/aidl/HistoryInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/hostmanager/aidl/HistoryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo$1;

    invoke-direct {v0}, Lcom/samsung/android/hostmanager/aidl/HistoryInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 204
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 148
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 150
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->a:Ljava/lang/String;

    .line 152
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->b:Ljava/lang/String;

    .line 154
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->c:Ljava/lang/String;

    .line 156
    const/4 v1, 0x3

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->d:Ljava/lang/String;

    .line 158
    const/4 v1, 0x4

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->e:Ljava/lang/String;

    .line 160
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v0, v1, v2

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/samsung/android/hostmanager/aidl/HistoryInfo;->e:Ljava/lang/String;

    aput-object v2, v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 176
    return-void
.end method
