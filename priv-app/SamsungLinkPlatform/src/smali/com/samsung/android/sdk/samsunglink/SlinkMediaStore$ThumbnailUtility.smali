.class final Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ThumbnailUtility"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    }
.end annotation


# static fields
.field private static final THUMB_PROJECTION:[Ljava/lang/String;

.field private static final sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1142
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 1144
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "thumb_width"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "thumb_height"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "full_width"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "full_height"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->THUMB_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1124
    return-void
.end method

.method public static cancelThumbnailRequest(Landroid/content/ContentResolver;Ljava/lang/String;JJ)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 1417
    new-instance v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    move-wide v2, p2

    move-wide v4, p4

    move v7, v6

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;-><init>(JJII)V

    .line 1419
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    .line 1420
    if-eqz v0, :cond_0

    .line 1421
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1422
    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$102(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Z)Z

    .line 1424
    invoke-static {p2, p3, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1425
    const-string v1, "cancel"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1428
    const-string v1, "group_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1431
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v2, v8

    move-object v3, v8

    move-object v4, v8

    move-object v5, v8

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1432
    if-eqz v0, :cond_0

    .line 1433
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1436
    :cond_0
    return-void
.end method

.method public static getThumbnailImage(Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 18

    .prologue
    .line 1264
    const/16 v16, 0x0

    .line 1266
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v5 .. v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;-><init>(JJII)V

    .line 1267
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    move/from16 v15, p9

    .line 1269
    :try_start_0
    invoke-static/range {v5 .. v15}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->getThumbnailUri(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Landroid/net/Uri;

    move-result-object v4

    .line 1280
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v8

    .line 1282
    const/4 v6, 0x0

    .line 1284
    :try_start_1
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$100(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v7

    if-nez v7, :cond_6

    move-object/from16 v0, p10

    iget-boolean v7, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v7, :cond_6

    .line 1285
    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    if-lez v7, :cond_0

    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    if-gtz v7, :cond_4

    .line 1286
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 1287
    if-nez v6, :cond_3

    .line 1288
    :try_start_2
    sget-boolean v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v4, :cond_1

    .line 1289
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to open inputStream for bitmap. origId:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1292
    :cond_1
    if-eqz v6, :cond_2

    .line 1398
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1406
    :cond_2
    :goto_0
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    const/4 v4, 0x0

    .line 1409
    :goto_1
    return-object v4

    .line 1294
    :cond_3
    const/4 v4, 0x1

    :try_start_4
    move-object/from16 v0, p10

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1295
    const/4 v4, 0x0

    move-object/from16 v0, p10

    invoke-static {v6, v4, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1296
    move-object/from16 v0, p10

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v4, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    .line 1297
    move-object/from16 v0, p10

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v4, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    .line 1299
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 1301
    move-wide/from16 v0, p2

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 1304
    const-string v7, "width"

    iget v9, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v7, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1307
    const-string v7, "height"

    iget v9, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v7, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1310
    const-string v7, "skip_cache_get"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v7, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1313
    const-string v7, "skip_cache_put"

    invoke-static/range {p9 .. p9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v7, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1316
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 1320
    :cond_4
    const/4 v7, 0x1

    .line 1321
    iget v9, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    iget v10, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 1323
    if-le v9, v8, :cond_5

    .line 1324
    int-to-float v7, v9

    int-to-float v9, v8

    div-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1327
    :cond_5
    const/4 v9, 0x1

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1329
    const/4 v9, 0x0

    move-object/from16 v0, p10

    iput-boolean v9, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1330
    move-object/from16 v0, p10

    iput v7, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1332
    sget-boolean v9, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v9, :cond_6

    .line 1333
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$200()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "::getThumbnailImage scaleFactor:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " requested:"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v10, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v10, 0x78

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v10, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " bitmap:"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v10, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v10, 0x78

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v10, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    :cond_6
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$100(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 1347
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v7

    .line 1349
    const/4 v4, 0x0

    :try_start_5
    move-object/from16 v0, p10

    invoke-static {v7, v4, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v6

    .line 1350
    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 1351
    if-eqz v6, :cond_a

    .line 1353
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1356
    if-le v4, v8, :cond_a

    .line 1357
    int-to-float v9, v8

    int-to-float v10, v4

    div-float/2addr v9, v10

    .line 1362
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    if-ne v4, v10, :cond_8

    .line 1364
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    float-to-int v4, v4

    .line 1370
    :goto_2
    if-lez v8, :cond_a

    if-lez v4, :cond_a

    .line 1371
    const/4 v9, 0x0

    invoke-static {v6, v8, v4, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1376
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1396
    :goto_3
    if-eqz v7, :cond_7

    .line 1398
    :try_start_7
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1406
    :cond_7
    :goto_4
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1366
    :cond_8
    :try_start_8
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_9
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    float-to-int v4, v4

    move/from16 v17, v8

    move v8, v4

    move/from16 v4, v17

    .line 1367
    goto :goto_2

    .line 1381
    :catch_0
    move-exception v4

    move-object/from16 v6, v16

    .line 1382
    :goto_5
    :try_start_9
    sget-boolean v8, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v8, :cond_a

    .line 1383
    if-eqz v6, :cond_9

    .line 1384
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unable to rescale bitmap. origId:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v6

    goto :goto_3

    .line 1386
    :cond_9
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unable to decode bitmap. origId:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :cond_a
    move-object v4, v6

    goto :goto_3

    .line 1391
    :catch_1
    move-exception v4

    move-object v7, v6

    move-object v6, v4

    move-object/from16 v4, v16

    .line 1392
    :goto_6
    :try_start_a
    sget-boolean v8, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v8, :cond_b

    .line 1393
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unable to decode bitmap. origId:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 1396
    :cond_b
    if-eqz v7, :cond_7

    .line 1398
    :try_start_b
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_4

    .line 1401
    :catch_2
    move-exception v6

    goto :goto_4

    .line 1396
    :catchall_0
    move-exception v4

    :goto_7
    if-eqz v6, :cond_c

    .line 1398
    :try_start_c
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 1401
    :cond_c
    :goto_8
    :try_start_d
    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1406
    :catchall_1
    move-exception v4

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    throw v4

    :catch_3
    move-exception v4

    goto/16 :goto_0

    .line 1401
    :catch_4
    move-exception v6

    goto/16 :goto_4

    :catch_5
    move-exception v6

    goto :goto_8

    .line 1396
    :catchall_2
    move-exception v4

    move-object v6, v7

    goto :goto_7

    .line 1391
    :catch_6
    move-exception v4

    move-object v7, v6

    move-object v6, v4

    move-object/from16 v4, v16

    goto :goto_6

    :catch_7
    move-exception v4

    move-object v6, v4

    move-object/from16 v4, v16

    goto :goto_6

    :catch_8
    move-exception v4

    move-object/from16 v17, v4

    move-object v4, v6

    move-object/from16 v6, v17

    goto :goto_6

    .line 1381
    :catch_9
    move-exception v4

    goto/16 :goto_5

    :cond_d
    move-object/from16 v4, v16

    move-object v7, v6

    goto/16 :goto_3
.end method

.method public static getThumbnailInputStream(Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Ljava/io/InputStream;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1161
    new-instance v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;-><init>(JJII)V

    .line 1162
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    .line 1164
    :try_start_0
    invoke-static/range {v1 .. v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->getThumbnailUri(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Landroid/net/Uri;

    move-result-object v2

    .line 1174
    const/4 v0, 0x0

    .line 1175
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$100(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1176
    invoke-virtual {p0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1187
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    :goto_0
    return-object v0

    .line 1179
    :catch_0
    move-exception v0

    .line 1180
    :try_start_1
    sget-boolean v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v2, :cond_1

    .line 1181
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ThumbnailUtility.getThumbnailUri failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1183
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$000(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    throw v0
.end method

.method private static getThumbnailUri(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Landroid/net/Uri;
    .locals 11

    .prologue
    .line 1202
    invoke-static {p3, p4, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1204
    const/4 v7, -0x1

    .line 1205
    const/4 v6, -0x1

    .line 1207
    if-nez p9, :cond_1

    .line 1208
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->THUMB_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1209
    if-eqz v3, :cond_1

    .line 1210
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1211
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1212
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1213
    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    .line 1214
    const/4 v4, 0x3

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    .line 1215
    const/4 v4, 0x4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    .line 1216
    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    .line 1218
    iget v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    if-lez v4, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    if-lez v4, :cond_0

    .line 1219
    iget v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    int-to-long v4, v4

    iget v6, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    .line 1220
    iget v6, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    int-to-long v6, v6

    iget v8, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    int-to-long v8, v8

    mul-long/2addr v6, v8

    .line 1221
    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 1222
    iget v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    iput v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    .line 1223
    iget v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    iput v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    .line 1228
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move v6, v0

    move v7, v2

    .line 1232
    :cond_1
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$100(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1233
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 1234
    const-string v0, "width"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1237
    const-string v0, "height"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1240
    const-string v2, "skip_cache_get"

    if-nez p9, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    if-gt v0, v7, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    if-le v0, v6, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1245
    const-string v0, "skip_cache_put"

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1248
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1251
    :cond_3
    return-object v1

    .line 1240
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move v0, v6

    move v2, v7

    goto :goto_0
.end method
