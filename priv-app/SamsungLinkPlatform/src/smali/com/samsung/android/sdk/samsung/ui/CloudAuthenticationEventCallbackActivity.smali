.class public Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const-string v0, "mfl_CloudAuthenticationEventCallbackActivity"

    const-string v1, "CloudAuthenticationEventCallbackActivity launched."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    if-eqz p1, :cond_0

    .line 62
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 35
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkStorageUtils.EXTRA_ORIGINAL_INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 36
    sget-object v1, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->a:Lorg/slf4j/Logger;

    const-string v2, "::onCreate: {}"

    invoke-static {v0}, Lcom/mfluent/asp/common/util/IntentHelper;->intentToString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 38
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 39
    if-gtz v1, :cond_1

    .line 40
    sget-object v0, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->a:Lorg/slf4j/Logger;

    const-string v2, "::onCreate: Cloud Login Failed - Invalid device key: {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->finish()V

    goto :goto_0

    .line 45
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 46
    if-nez v2, :cond_2

    .line 47
    sget-object v0, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->a:Lorg/slf4j/Logger;

    const-string v2, "::onCreate: Cloud Login Failed - Device not found. Id: {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->finish()V

    goto :goto_0

    .line 52
    :cond_2
    const-string v1, "errorCode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->a:Lorg/slf4j/Logger;

    const-string v3, "::onCreate: Cloud Login Failed - Device: {} Reason: {}"

    invoke-interface {v1, v3, v2, v0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 54
    const-string v1, "EmailNotVerified"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    invoke-static {p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/StorageTypeHelper;->b(Lcom/mfluent/asp/datamodel/Device;)I

    move-result v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v6, v0, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsung/ui/CloudAuthenticationEventCallbackActivity;->finish()V

    goto :goto_0
.end method
