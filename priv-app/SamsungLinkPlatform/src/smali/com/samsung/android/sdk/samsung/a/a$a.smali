.class final Lcom/samsung/android/sdk/samsung/a/a$a;
.super Lcom/samsung/android/hostmanager/aidl/b$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsung/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsung/a/a;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/samsung/a/a;)V
    .locals 0

    .prologue
    .line 1063
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/a/a$a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-direct {p0}, Lcom/samsung/android/hostmanager/aidl/b$a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/samsung/a/a;B)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a$a;-><init>(Lcom/samsung/android/sdk/samsung/a/a;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1092
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): Address = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): BatteryLevel = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): BinaryVersion = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): ConnectStatus = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): TransportType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): DeviceType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): ExternalMemoryRemain = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): ExternalMemoryTotal = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): MemoryRemain = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): MemoryTotal = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): ModelName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): Pakcagename = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onWearableStatusInfo(): PlatformName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a/a;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;)V

    .line 1116
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1068
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::mHMListener::onReceiveDeviceConnected: deviceID ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    new-instance v0, Landroid/content/Intent;

    const-string v1, "SAMSUNG_LINK_ACTION_RECEIVE_WEARABLE_DEVICE_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1071
    const-string v1, "WearableDeviceManager.receive_wearable_device_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1073
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a$a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/a/a;->g(Lcom/samsung/android/sdk/samsung/a/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1074
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1075
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1080
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::mHMListener::onReceiveDeviceDisconnected: deviceID ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    new-instance v0, Landroid/content/Intent;

    const-string v1, "SAMSUNG_LINK_ACTION_RECEIVE_WEARABLE_DEVICE_DISCONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1083
    const-string v1, "WearableDeviceManager.receive_wearable_device_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1085
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a$a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/a/a;->g(Lcom/samsung/android/sdk/samsung/a/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1086
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1087
    return-void
.end method
