.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final bitmap:Landroid/graphics/Bitmap;

.field private final requestedMaxHeight:I

.field private final requestedMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 23
    iput p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxWidth:I

    .line 24
    iput p3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxHeight:I

    .line 25
    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRequestedMaxHeight()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxHeight:I

    return v0
.end method

.method public getRequestedMaxWidth()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxWidth:I

    return v0
.end method
