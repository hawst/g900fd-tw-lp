.class public final Lcom/samsung/android/sdk/samsunglink/R$color;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_bg_color:I = 0x7f060009

.field public static final actionbar_item_focus_color:I = 0x7f060010

.field public static final actionbar_light_subtitle_text_color:I = 0x7f06000f

.field public static final actionbar_light_title_text_color:I = 0x7f06000e

.field public static final actionbar_subtitle_text_color:I = 0x7f06000d

.field public static final actionbar_title_text_color:I = 0x7f06000c

.field public static final actionbutton_bg_color:I = 0x7f060008

.field public static final actionmode_bg_color:I = 0x7f06000a

.field public static final actionmode_bg_underline_color:I = 0x7f06000b

.field public static final content_list_divider:I = 0x7f060007

.field public static final dialog_button_default:I = 0x7f06001c

.field public static final dialog_button_disable:I = 0x7f06001e

.field public static final dialog_button_press:I = 0x7f06001d

.field public static final dialog_button_text_disable_color:I = 0x7f060028

.field public static final dialog_button_text_normal_color:I = 0x7f060027

.field public static final dialog_message_text_color:I = 0x7f06001f

.field public static final divider_line:I = 0x7f060006

.field public static final drawer_option_bg_pressed_color:I = 0x7f060012

.field public static final file_path_text_color:I = 0x7f06002d

.field public static final info_storage_defalut_btn_color:I = 0x7f060019

.field public static final info_storage_focused_btn_color:I = 0x7f06001b

.field public static final info_storage_press_btn_color:I = 0x7f06001a

.field public static final intro_signin_button_text_color:I = 0x7f060032

.field public static final intro_subtext_color:I = 0x7f060031

.field public static final list_bg_color_platf:I = 0x7f060001

.field public static final list_bg_color_platf_white:I = 0x7f060000

.field public static final list_bg_color_uiapp:I = 0x7f060002

.field public static final list_separator_bg_color:I = 0x7f06002a

.field public static final list_separator_bg_color_file:I = 0x7f06002c

.field public static final list_separator_text_color:I = 0x7f06002b

.field public static final loading_info_cell_text_color:I = 0x7f060033

.field public static final music_2d_separator_bg_color:I = 0x7f06002e

.field public static final music_2d_separator_text_color:I = 0x7f06002f

.field public static final music_2d_time_color:I = 0x7f060030

.field public static final normal_text_color:I = 0x7f060003

.field public static final platf_sendto_device_capacity_disable_color:I = 0x7f060025

.field public static final platf_sendto_device_capacity_normal_color:I = 0x7f060024

.field public static final platf_sendto_device_name_disable_color:I = 0x7f060023

.field public static final platf_sendto_device_name_normal_color:I = 0x7f060022

.field public static final registration_guide_Light_text_color:I = 0x7f060021

.field public static final registration_guide_default_text_color:I = 0x7f060020

.field public static final secondary_text_color:I = 0x7f060029

.field public static final sendto_background_color:I = 0x7f060026

.field public static final settings_description_and_sub_text:I = 0x7f060005

.field public static final settings_list_items_text:I = 0x7f060004

.field public static final title_back_text_normal:I = 0x7f060016

.field public static final title_back_text_normal_sel:I = 0x7f060015

.field public static final title_back_text_press:I = 0x7f060014

.field public static final title_back_text_selected:I = 0x7f060013

.field public static final title_tab_text_normal:I = 0x7f060018

.field public static final title_tab_text_selected:I = 0x7f060017

.field public static final video_progress_shadow:I = 0x7f060011


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
