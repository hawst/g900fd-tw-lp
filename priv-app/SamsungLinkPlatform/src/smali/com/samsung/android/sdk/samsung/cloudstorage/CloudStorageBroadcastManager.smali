.class public Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static a:Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 19
    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_FAILURE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 20
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;
    .locals 2

    .prologue
    .line 23
    const-class v1, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;->a:Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;->a:Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;

    .line 26
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;->a:Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string v1, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_FAILURE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkConstants.ACTION_CLOUD_AUTHENTICATION_FAILURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 49
    const-string v1, "com.samsung.android.sdk.samsunglink.permission.PRIVATE_ACCESS"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 51
    :cond_0
    return-void
.end method
