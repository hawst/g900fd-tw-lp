.class Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;
.super Landroid/content/CursorLoader;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SlinkViewerCursorLoader"
.end annotation


# instance fields
.field private mCancellationSignal:Landroid/os/CancellationSignal;

.field private final mObserver:Landroid/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mViewIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0, p1}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 327
    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mViewIntent:Landroid/content/Intent;

    .line 328
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 329
    return-void
.end method


# virtual methods
.method public cancelLoadInBackground()V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Landroid/content/CursorLoader;->cancelLoadInBackground()V

    .line 361
    monitor-enter p0

    .line 362
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 365
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 334
    monitor-enter p0

    .line 335
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->isLoadInBackgroundCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Landroid/os/OperationCanceledException;

    invoke-direct {v0}, Landroid/os/OperationCanceledException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 338
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 339
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mViewIntent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getCursorFromViewIntent(Landroid/content/Intent;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    .line 344
    if-eqz v0, :cond_1

    .line 346
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 347
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 351
    :cond_1
    monitor-enter p0

    .line 352
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 353
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-object v0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 351
    :catchall_2
    move-exception v0

    monitor-enter p0

    .line 352
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 353
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils$SlinkViewerCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
