.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImagesUriBatch"
.end annotation


# instance fields
.field private final mHttpProxyUri:Landroid/net/Uri;

.field private final mLocalUri:Landroid/net/Uri;

.field private final mSameAccessPointUri:Landroid/net/Uri;

.field private final mScsUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1579
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mHttpProxyUri:Landroid/net/Uri;

    .line 1580
    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mScsUri:Landroid/net/Uri;

    .line 1581
    iput-object p3, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mSameAccessPointUri:Landroid/net/Uri;

    .line 1582
    iput-object p4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mLocalUri:Landroid/net/Uri;

    .line 1583
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$1;)V
    .locals 0

    .prologue
    .line 1567
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public getHttpProxyUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1586
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mHttpProxyUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getLocalUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getSameAccessPointUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mSameAccessPointUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getScsUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->mScsUri:Landroid/net/Uri;

    return-object v0
.end method
