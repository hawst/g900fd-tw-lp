.class public final Lcom/samsung/android/sdk/samsung/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsung/b$b;,
        Lcom/samsung/android/sdk/samsung/b$a;
    }
.end annotation


# static fields
.field private static a:Lcom/samsung/android/sdk/samsung/b$a;

.field private static b:Lcom/samsung/android/sdk/samsung/b;


# instance fields
.field private c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/locks/ReentrantLock;

.field private e:Landroid/database/sqlite/SQLiteDatabase;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/samsung/b;->b:Lcom/samsung/android/sdk/samsung/b;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->c:Landroid/content/Context;

    .line 102
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    .line 104
    const-string v0, "dummdirectory"

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    .line 106
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsung/b;->g:J

    .line 107
    iput-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    .line 202
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/b;->c:Landroid/content/Context;

    .line 203
    sget-object v0, Lcom/samsung/android/sdk/samsung/b;->a:Lcom/samsung/android/sdk/samsung/b$a;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Lcom/samsung/android/sdk/samsung/b$a;

    const-string v1, "asp_cache.db"

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/sdk/samsung/b$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/android/sdk/samsung/b;->a:Lcom/samsung/android/sdk/samsung/b$a;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 207
    sget-object v0, Lcom/samsung/android/sdk/samsung/b;->a:Lcom/samsung/android/sdk/samsung/b$a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/b$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->c:Landroid/content/Context;

    const-string v1, "netres_cache"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_2

    .line 211
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    .line 212
    const-string v0, "INFO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mBaseDirName="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_2
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 22
    invoke-static {p0}, Lcom/samsung/android/sdk/samsung/b;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/samsung/android/sdk/samsung/b;->b:Lcom/samsung/android/sdk/samsung/b;

    if-nez v0, :cond_0

    .line 194
    new-instance v0, Lcom/samsung/android/sdk/samsung/b;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/b;-><init>(Landroid/content/Context;)V

    .line 195
    sput-object v0, Lcom/samsung/android/sdk/samsung/b;->b:Lcom/samsung/android/sdk/samsung/b;

    .line 197
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/b;->b:Lcom/samsung/android/sdk/samsung/b;

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)J
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 152
    .line 155
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 157
    if-nez v5, :cond_0

    move-wide v0, v2

    .line 174
    :goto_0
    return-wide v0

    .line 160
    :cond_0
    array-length v6, v5

    move v4, v1

    move v0, v1

    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v7, v5, v4

    .line 161
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_1

    .line 162
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v2, v8

    .line 164
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 165
    const/16 v7, 0x3e8

    if-le v0, v7, :cond_2

    .line 168
    const-wide/16 v8, 0x64

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 160
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    move-wide v0, v2

    .line 173
    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    move-object v10, v0

    move-wide v0, v2

    move-object v2, v10

    .line 172
    const-string v3, "mfl_CacheManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "error during CheckCacheBackgroudJob e="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/android/sdk/samsung/b;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method static synthetic c(Lcom/samsung/android/sdk/samsung/b;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    if-eqz p1, :cond_0

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;I)J
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    .line 269
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v6, v2, v4

    .line 272
    if-nez p1, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-wide v0

    .line 275
    :cond_1
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/samsung/b;->c(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT * FROM cache_table WHERE KEY = \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 277
    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 280
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 281
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 282
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 283
    const-string v3, "updated_time"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 287
    :goto_1
    if-eqz v2, :cond_2

    .line 288
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 296
    :goto_2
    cmp-long v2, v6, v4

    if-lez v2, :cond_0

    .line 297
    sub-long v0, v6, v4

    goto :goto_0

    .line 285
    :cond_3
    :try_start_2
    const-string v3, "INFO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getCheckTime can\'t find url="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide v4, v0

    goto :goto_1

    .line 291
    :catch_0
    move-exception v2

    move-wide v4, v0

    .line 292
    :goto_3
    :try_start_3
    const-string v3, "mfl_CacheManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "erro during getCheckTime, "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 294
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 291
    :catch_1
    move-exception v2

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/io/ByteArrayOutputStream;)J
    .locals 15

    .prologue
    .line 340
    const-wide/16 v4, -0x1

    .line 341
    const-wide/16 v6, -0x1

    .line 342
    const/4 v2, 0x0

    .line 346
    if-eqz p1, :cond_0

    if-nez p4, :cond_1

    .line 347
    :cond_0
    const-wide/16 v2, -0x1

    .line 398
    :goto_0
    return-wide v2

    .line 349
    :cond_1
    invoke-static/range {p1 .. p3}, Lcom/samsung/android/sdk/samsung/b;->c(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 350
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SELECT * FROM cache_table WHERE KEY = \""

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\";"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 351
    iget-object v9, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v9}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 354
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v10, 0x0

    invoke-virtual {v9, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 355
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_2

    .line 356
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 357
    const-string v2, "filename"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 358
    const-string v9, "last_modified_time"

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 360
    :cond_2
    if-eqz v8, :cond_3

    .line 361
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 363
    :cond_3
    if-eqz v2, :cond_4

    .line 365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 366
    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 367
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 368
    const-string v11, "checked_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v10, v11, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 369
    iget-object v8, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v9, "cache_table"

    const-string v11, "KEY=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v3, v12, v13

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-object v14, v2

    move-wide v2, v6

    move-object v6, v14

    .line 377
    :goto_1
    if-nez v6, :cond_5

    .line 378
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 371
    :catch_0
    move-exception v2

    .line 372
    const/4 v3, 0x0

    .line 373
    :try_start_1
    const-string v8, "mfl_CacheManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "erro during getCachedFile, "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-object v14, v3

    move-wide v2, v6

    move-object v6, v14

    .line 376
    goto :goto_1

    .line 375
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .line 381
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 383
    :try_start_2
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 384
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 385
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 386
    const/16 v7, 0x2004

    new-array v7, v7, [B

    .line 388
    :goto_2
    invoke-virtual {v6, v7}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_6

    .line 389
    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v9, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 394
    :catch_1
    move-exception v2

    move-object v14, v2

    move-wide v2, v4

    move-object v4, v14

    .line 395
    :goto_3
    const-string v5, "mfl_CacheManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "can\'t read cache file to memory buffer e="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 392
    :cond_6
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 394
    :catch_2
    move-exception v4

    goto :goto_3

    :cond_7
    move-wide v2, v4

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 217
    if-nez p1, :cond_0

    .line 262
    :goto_0
    return-void

    .line 225
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT * FROM cache_table WHERE uri = \""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND device_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 234
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 236
    :cond_1
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 237
    const-string v1, "filename"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 239
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 240
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 258
    :catch_0
    move-exception v0

    .line 259
    :try_start_1
    const-string v1, "mfl_CacheManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "exception during deleteCache e="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 245
    :cond_2
    if-eqz v0, :cond_3

    .line 246
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_3
    const-string v0, "uri =? AND device_id =?"

    .line 251
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 257
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "cache_table"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IJLjava/io/ByteArrayOutputStream;Ljava/lang/String;)Z
    .locals 16

    .prologue
    .line 402
    const/4 v2, 0x1

    .line 405
    const-wide/16 v6, 0x0

    .line 406
    const/4 v3, 0x0

    .line 410
    if-eqz p1, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-ltz v4, :cond_0

    if-eqz p6, :cond_0

    if-nez p7, :cond_2

    .line 411
    :cond_0
    const/4 v2, 0x0

    .line 468
    :cond_1
    :goto_0
    return v2

    .line 413
    :cond_2
    invoke-static/range {p1 .. p3}, Lcom/samsung/android/sdk/samsung/b;->c(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 415
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 418
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/data_"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ".tmp"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move v14, v4

    move-object v4, v5

    move v5, v14

    :goto_1
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/samsung/b;->f:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "/data_"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "_"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ".tmp"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v8

    goto :goto_1

    .line 419
    :cond_3
    :try_start_1
    invoke-virtual/range {p6 .. p6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 420
    array-length v5, v3

    int-to-long v6, v5

    .line 421
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 422
    invoke-virtual {v5, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 423
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 432
    :goto_2
    if-eqz v2, :cond_1

    if-eqz v4, :cond_1

    .line 436
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 437
    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    .line 438
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 439
    const-string v5, "KEY"

    invoke-virtual {v3, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v5, "filename"

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v4, "last_modified_time"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 442
    const-string v4, "updated_time"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 443
    const-string v4, "checked_time"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 444
    const-string v4, "mime_type"

    move-object/from16 v0, p7

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v4, "device_id"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 446
    const-string v4, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const-string v4, "original_size"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 448
    const-string v4, "cached_size"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 449
    if-eqz p2, :cond_4

    .line 450
    const-string v4, "uri_param"

    move-object/from16 v0, p2

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 454
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "cache_table"

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v6, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v4

    long-to-int v4, v4

    .line 455
    const/4 v5, -0x1

    if-ne v4, v5, :cond_5

    .line 456
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "cache_table"

    const-string v6, "KEY=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 462
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 464
    :goto_3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    invoke-interface {v4}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    invoke-interface {v4}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v4

    if-nez v4, :cond_9

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/samsung/b;->g:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_a

    const/4 v3, 0x1

    :cond_7
    :goto_4
    if-eqz v3, :cond_8

    invoke-static {}, Lcom/mfluent/asp/util/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    new-instance v6, Lcom/samsung/android/sdk/samsung/b$b;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v7}, Lcom/samsung/android/sdk/samsung/b$b;-><init>(Lcom/samsung/android/sdk/samsung/b;B)V

    invoke-interface {v3, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    :cond_8
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/android/sdk/samsung/b;->g:J

    .line 465
    :cond_9
    if-nez v2, :cond_1

    goto/16 :goto_0

    .line 424
    :catch_0
    move-exception v2

    move-wide v4, v6

    :goto_5
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 426
    const/4 v2, 0x0

    .line 427
    const-string v6, "mfl_CacheManager"

    const-string v7, "error during saveCache"

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 429
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-wide v6, v4

    move-object v4, v3

    .line 430
    goto/16 :goto_2

    .line 429
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .line 458
    :catch_1
    move-exception v2

    .line 459
    :try_start_4
    const-string v3, "mfl_CacheManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "error during update cache DB e="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 460
    const/4 v2, 0x0

    .line 462
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_3

    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .line 464
    :cond_a
    const-wide/32 v6, 0x2932e00

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/android/sdk/samsung/b;->g:J

    add-long/2addr v6, v8

    cmp-long v6, v4, v6

    if-lez v6, :cond_7

    const/4 v3, 0x1

    goto :goto_4

    .line 424
    :catch_2
    move-exception v2

    move-object v3, v4

    move-wide v4, v6

    goto :goto_5
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 304
    .line 308
    if-nez p1, :cond_0

    .line 336
    :goto_0
    return-void

    .line 311
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/samsung/b;->c(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 312
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SELECT * FROM cache_table WHERE KEY = \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 313
    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 316
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 317
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 318
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 319
    const-string v0, "filename"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 321
    :cond_1
    if-eqz v2, :cond_2

    .line 322
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 324
    :cond_2
    if-eqz v0, :cond_3

    .line 325
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 326
    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 327
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 328
    const-string v4, "updated_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 329
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/b;->e:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "cache_table"

    const-string v4, "KEY=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    :try_start_1
    const-string v1, "mfl_CacheManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "erro during setCheckTime, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method protected final finalize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 180
    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/b;->a:Lcom/samsung/android/sdk/samsung/b$a;

    if-eqz v0, :cond_0

    .line 181
    sget-object v0, Lcom/samsung/android/sdk/samsung/b;->a:Lcom/samsung/android/sdk/samsung/b$a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/b$a;->close()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b;->h:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_1
    :goto_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 190
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 187
    const-string v1, "mfl_CacheManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error in finalize e="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
