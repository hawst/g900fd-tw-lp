.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$GeolocationColumns;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GeolocationColumns"
.end annotation


# static fields
.field public static final GEO_LOC_COUNTRY:Ljava/lang/String; = "geo_loc_country"

.field public static final GEO_LOC_FEATURE:Ljava/lang/String; = "geo_loc_feature"

.field public static final GEO_LOC_LOCALE:Ljava/lang/String; = "geo_loc_locale"

.field public static final GEO_LOC_LOCALITY:Ljava/lang/String; = "geo_loc_locality"

.field public static final GEO_LOC_PREMISES:Ljava/lang/String; = "geo_loc_premises"

.field public static final GEO_LOC_PROVINCE:Ljava/lang/String; = "geo_loc_province"

.field public static final GEO_LOC_SUB_LOCALITY:Ljava/lang/String; = "geo_loc_sub_locality"

.field public static final GEO_LOC_SUB_PROVINCE:Ljava/lang/String; = "geo_loc_sub_province"

.field public static final GEO_LOC_SUB_THOROUGHFARE:Ljava/lang/String; = "geo_loc_sub_thoroughfare"

.field public static final GEO_LOC_THOROUGHFARE:Ljava/lang/String; = "geo_loc_thoroughfare"
