.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ArchivedMediaColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$BaseSamsungLinkColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ArchivedMediaColumns"
.end annotation


# static fields
.field public static final DATE_TAKEN:Ljava/lang/String; = "datetaken"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "_display_name"

.field public static final SIZE:Ljava/lang/String; = "_size"
