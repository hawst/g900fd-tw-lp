.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumArt"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "album_art"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2449
    const-string v0, "album_art"

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;->CONTENT_TYPE:Ljava/lang/String;

    .line 2454
    const-string v0, "album_art"

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    .line 2460
    const-string v0, "album_art"

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelThumbnailRequest(Landroid/content/ContentResolver;JJ)V
    .locals 7

    .prologue
    .line 2614
    const-string v1, "album_art"

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->cancelThumbnailRequest(Landroid/content/ContentResolver;Ljava/lang/String;JJ)V

    .line 2615
    return-void
.end method

.method public static getEntryUri(J)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 2470
    const-string v0, "album_art"

    invoke-static {p0, p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getThumbnail(Landroid/content/ContentResolver;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    .line 2523
    const-string v1, "album_art"

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->getThumbnailImage(Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static openThumbnailInputStream(Landroid/content/ContentResolver;JJIIZZ)Ljava/io/InputStream;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2586
    const-string v1, "album_art"

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->getThumbnailInputStream(Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
