.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$GenresColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/provider/MediaStore$Audio$GenresColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$BaseSamsungLinkColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GenresColumns"
.end annotation


# static fields
.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ARTIST_ID:Ljava/lang/String; = "artist_id"

.field public static final DUP_REDUCED_COUNT:Ljava/lang/String; = "dup_reduced_count"

.field public static final GENRE_KEY:Ljava/lang/String; = "genre_key"

.field public static final THUMB_DATA:Ljava/lang/String; = "thumb_data"

.field public static final TITLE:Ljava/lang/String; = "title"
