.class public final enum Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IconSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

.field public static final enum LARGE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

.field public static final enum SMALL:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 494
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->SMALL:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    .line 499
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->LARGE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    .line 490
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->SMALL:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->LARGE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 490
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;
    .locals 1

    .prologue
    .line 490
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;
    .locals 1

    .prologue
    .line 490
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    return-object v0
.end method
