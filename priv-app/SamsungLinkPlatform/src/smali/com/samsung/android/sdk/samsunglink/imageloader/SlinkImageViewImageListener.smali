.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;


# instance fields
.field private final mErrorResource:I

.field private mImageLoaded:Z

.field private final mImageView:Landroid/widget/ImageView;

.field private final mLoadingResource:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;II)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    .line 35
    iput p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mLoadingResource:I

    .line 36
    iput p3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mErrorResource:I

    .line 37
    return-void
.end method

.method private hasImageLoaded()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onImageFailedToLoad()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->hasImageLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    .line 94
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mErrorResource:I

    if-eqz v0, :cond_0

    .line 95
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mErrorResource:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->setImageResource(I)V

    .line 98
    :cond_0
    return-void
.end method

.method private setImageAndOrientation(Landroid/graphics/Bitmap;I)V
    .locals 3

    .prologue
    .line 105
    if-nez p2, :cond_0

    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 111
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    .line 112
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setImageResource(I)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    .line 88
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 89
    return-void
.end method


# virtual methods
.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public onErrorResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->onImageFailedToLoad()V

    goto :goto_0
.end method

.method public onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 45
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mLoadingResource:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->setImageResource(I)V

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    .line 48
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getRequest()Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 49
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getRequest()Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getOrientation()I

    move-result v0

    .line 51
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->setImageAndOrientation(Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method

.method public updateImageContainer(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    .line 75
    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->cancelRequest()V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 84
    return-void
.end method
