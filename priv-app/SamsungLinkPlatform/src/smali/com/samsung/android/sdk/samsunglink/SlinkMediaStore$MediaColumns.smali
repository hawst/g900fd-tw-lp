.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$MediaColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/provider/MediaStore$MediaColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$BaseSamsungLinkColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaColumns"
.end annotation


# static fields
.field public static final BURST_DUP_ID:Ljava/lang/String; = "burst_dup_id"

.field public static final BURST_ID:Ljava/lang/String; = "burst_id"

.field public static final DEVICE_PRIORITY:Ljava/lang/String; = "device_priority"

.field public static final DUP_ID:Ljava/lang/String; = "dup_id"

.field public static final FILE_DIGEST:Ljava/lang/String; = "file_digest"

.field public static final INDEX_CHAR:Ljava/lang/String; = "index_char"

.field public static final LOCAL_DATA:Ljava/lang/String; = "local_data"

.field public static final LOCAL_ID:Ljava/lang/String; = "local_id"

.field public static final LOCAL_SOURCE_MEDIA_ID:Ljava/lang/String; = "local_source_media_id"

.field public static final LOCAL_THUMB_DATA:Ljava/lang/String; = "local_thumb_data"

.field public static final LOCAL_THUMB_HEIGHT:Ljava/lang/String; = "local_thumb_height"

.field public static final LOCAL_THUMB_WIDTH:Ljava/lang/String; = "local_thumb_width"

.field public static final MEDIA_TYPE:Ljava/lang/String; = "media_type"

.field public static final PHYSICAL_TYPE:Ljava/lang/String; = "physical_type"

.field public static final THUMB_DATA:Ljava/lang/String; = "thumb_data"

.field public static final THUMB_HEIGHT:Ljava/lang/String; = "thumb_height"

.field public static final THUMB_WIDTH:Ljava/lang/String; = "thumb_width"

.field public static final TRANSPORT_TYPE:Ljava/lang/String; = "transport_type"
