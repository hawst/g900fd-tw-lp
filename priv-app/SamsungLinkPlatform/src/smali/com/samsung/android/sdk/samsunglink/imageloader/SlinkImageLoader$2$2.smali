.class Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 341
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v4, v4}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;-><init>(Landroid/graphics/Bitmap;II)V

    .line 342
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iget-object v1, v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->this$0:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$700(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iget-object v2, v2, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$imageContainer:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getCacheKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;->putBitmapInfo(Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;)V

    .line 346
    sget-boolean v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v0, :cond_0

    .line 347
    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded device icon bitmap for device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iget-wide v2, v2, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$deviceId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iget-object v0, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$imageContainer:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->val$bitmap:Landroid/graphics/Bitmap;

    # setter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$302(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 350
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iget-object v0, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$imageContainer:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->mListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$500(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;->this$1:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    iget-object v1, v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$imageContainer:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    invoke-interface {v0, v1, v4}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;->onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 351
    return-void
.end method
