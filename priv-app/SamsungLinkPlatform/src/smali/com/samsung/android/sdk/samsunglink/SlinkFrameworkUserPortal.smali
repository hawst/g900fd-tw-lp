.class public Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    .line 46
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
    .locals 3

    .prologue
    .line 29
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 30
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 32
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    if-nez v0, :cond_1

    .line 33
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    .line 35
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public deregisterDevice(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 100
    .line 102
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.DeregisterDevice.NAME"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 112
    :goto_0
    if-nez v0, :cond_0

    .line 113
    const-string v0, "slinklib"

    const-string v1, "::deregisterDevice result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v0, 0x0

    .line 116
    :goto_1
    return v0

    .line 109
    :catch_0
    move-exception v1

    const-string v1, "slinklib"

    const-string v2, "::deregisterDevice maybe platform is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 116
    :cond_0
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method

.method public reportUsageStat(I)V
    .locals 5

    .prologue
    .line 147
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SendUsageStat.NAME"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    .line 154
    :catch_0
    move-exception v0

    const-string v0, "slinklib"

    const-string v1, "::reportUsageStat maybe platform is disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendWakeupPushInBackground()V
    .locals 5

    .prologue
    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SendWakeupPushInBackground.NAME"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_0
    return-void

    .line 128
    :catch_0
    move-exception v0

    const-string v0, "slinklib"

    const-string v1, "::sendWakeupPushInBackground maybe platform is disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDeviceInfo()V
    .locals 5

    .prologue
    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetDeviceInfo.NAME"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    const-string v0, "slinklib"

    const-string v1, "::setDeviceInfo maybe platform is disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public turnOnDevice(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 80
    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.TurnOnDevice.NAME"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, p1, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 92
    :goto_0
    if-nez v0, :cond_0

    .line 93
    const-string v0, "slinklib"

    const-string v1, "::turnOnDevice result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const/4 v0, 0x0

    .line 96
    :goto_1
    return v0

    .line 89
    :catch_0
    move-exception v1

    const-string v1, "slinklib"

    const-string v2, "::turnOnDevice maybe platform is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 96
    :cond_0
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method

.method public updateuserDeviceName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 50
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    const-string v2, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_NAME"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v2, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_IMEI"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.UpdateUserDeviceName.NAME"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 72
    :goto_0
    if-nez v0, :cond_0

    .line 73
    const-string v0, "slinklib"

    const-string v1, "::updateuserDeviceName result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_1
    return v0

    .line 66
    :catch_0
    move-exception v1

    const-string v1, "slinklib"

    const-string v2, "::updateuserDeviceName this.context.getContentResolver().call null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 69
    :catch_1
    move-exception v1

    const-string v1, "slinklib"

    const-string v2, "::updateuserDeviceName maybe platform is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 76
    :cond_0
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method
