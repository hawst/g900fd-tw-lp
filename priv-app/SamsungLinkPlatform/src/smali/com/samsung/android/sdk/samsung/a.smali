.class public Lcom/samsung/android/sdk/samsung/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:J

.field static b:J

.field private static final c:Lorg/slf4j/Logger;

.field private static d:Lcom/samsung/android/sdk/samsung/a;

.field private static f:Landroid/content/BroadcastReceiver;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 24
    const-class v0, Lcom/samsung/android/sdk/samsung/a;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsung/a;->c:Lorg/slf4j/Logger;

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/samsung/a;->d:Lcom/samsung/android/sdk/samsung/a;

    .line 67
    new-instance v0, Lcom/samsung/android/sdk/samsung/a$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsung/a$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsung/a;->f:Landroid/content/BroadcastReceiver;

    .line 85
    sput-wide v2, Lcom/samsung/android/sdk/samsung/a;->a:J

    .line 86
    sput-wide v2, Lcom/samsung/android/sdk/samsung/a;->b:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v4, p0, Lcom/samsung/android/sdk/samsung/a;->e:I

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 37
    sget-object v1, Lcom/samsung/android/sdk/samsung/a;->f:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CHECK_SIOP_LEVEL"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/samsung/android/sdk/samsung/a;->c:Lorg/slf4j/Logger;

    const-string v2, "::HeatManager: registered SIOP receiver - stickyIntent: {}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    if-eqz v0, :cond_0

    const-string v1, "siop_level_broadcast"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const-string v1, "siop_level_broadcast"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsung/a;->e:I

    .line 42
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a;
    .locals 2

    .prologue
    .line 45
    const-class v1, Lcom/samsung/android/sdk/samsung/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/a;->d:Lcom/samsung/android/sdk/samsung/a;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/samsung/android/sdk/samsung/a;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsung/a;->d:Lcom/samsung/android/sdk/samsung/a;

    .line 48
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/a;->d:Lcom/samsung/android/sdk/samsung/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c()I
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 92
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "/proc/stat"

    const-string v6, "r"

    invoke-direct {v2, v0, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 94
    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 97
    const/4 v2, 0x5

    :try_start_2
    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 98
    const/4 v2, 0x2

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    const/4 v2, 0x4

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    const/4 v2, 0x5

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    const/4 v2, 0x6

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    const/4 v2, 0x7

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    const/16 v2, 0x8

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    add-long v12, v6, v8

    .line 108
    sget-wide v6, Lcom/samsung/android/sdk/samsung/a;->a:J

    cmp-long v0, v6, v4

    if-ltz v0, :cond_0

    sget-wide v6, Lcom/samsung/android/sdk/samsung/a;->b:J

    cmp-long v0, v6, v4

    if-gez v0, :cond_4

    .line 109
    :cond_0
    sput-wide v12, Lcom/samsung/android/sdk/samsung/a;->a:J

    .line 110
    sput-wide v10, Lcom/samsung/android/sdk/samsung/a;->b:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 111
    const/16 v0, 0xa

    move-wide v8, v4

    move-wide v10, v4

    .line 118
    :goto_0
    cmp-long v2, v10, v8

    if-lez v2, :cond_6

    .line 119
    sub-long v6, v10, v8

    .line 122
    :goto_1
    cmp-long v2, v10, v4

    if-lez v2, :cond_1

    .line 123
    const-wide/16 v4, 0x64

    mul-long/2addr v4, v6

    :try_start_3
    div-long/2addr v4, v10

    .line 125
    :cond_1
    const-string v2, "INFO"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "cpu="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%, idle_val="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",total_val="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 126
    const-wide/16 v2, 0x46

    cmp-long v2, v4, v2

    if-lez v2, :cond_2

    .line 127
    const/16 v0, 0x14

    .line 140
    :cond_2
    :goto_2
    sget v2, Lcom/mfluent/asp/sync/g;->b:I

    if-lez v2, :cond_3

    sget v2, Lcom/mfluent/asp/sync/g;->b:I

    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_3

    move v0, v1

    .line 143
    :cond_3
    return v0

    .line 113
    :cond_4
    :try_start_4
    sget-wide v6, Lcom/samsung/android/sdk/samsung/a;->a:J

    sub-long v8, v12, v6

    .line 114
    sget-wide v6, Lcom/samsung/android/sdk/samsung/a;->b:J

    sub-long v6, v10, v6

    .line 115
    sput-wide v12, Lcom/samsung/android/sdk/samsung/a;->a:J

    .line 116
    sput-wide v10, Lcom/samsung/android/sdk/samsung/a;->b:J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-wide v10, v8

    move v0, v1

    move-wide v8, v6

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    move-object v2, v0

    move v0, v1

    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 132
    if-eqz v3, :cond_2

    .line 134
    :try_start_6
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 137
    :catch_1
    move-exception v2

    goto :goto_2

    .line 132
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v3, :cond_5

    .line 134
    :try_start_7
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 137
    :cond_5
    :goto_5
    throw v0

    :catch_2
    move-exception v1

    goto :goto_5

    .line 132
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_4

    .line 134
    :catch_3
    move-exception v0

    move-object v3, v2

    move-object v2, v0

    move v0, v1

    goto :goto_3

    :catch_4
    move-exception v2

    goto :goto_3

    :cond_6
    move-wide v6, v4

    goto :goto_1
.end method

.method public static d()Z
    .locals 8

    .prologue
    .line 165
    :try_start_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 166
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 167
    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 168
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 169
    sget-object v0, Lcom/samsung/android/sdk/samsung/a;->c:Lorg/slf4j/Logger;

    const-string v2, "::isMemoryLow() : {} availableMem:{}MB"

    iget-boolean v3, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-wide v4, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/32 v6, 0x100000

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 170
    iget-boolean v0, v1, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_0
    return v0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    sget-object v1, Lcom/samsung/android/sdk/samsung/a;->c:Lorg/slf4j/Logger;

    const-string v2, "Exception at ::isMemoryLow()"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e()Lcom/samsung/android/sdk/samsung/a;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/sdk/samsung/a;->d:Lcom/samsung/android/sdk/samsung/a;

    return-object v0
.end method

.method static synthetic f()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/sdk/samsung/a;->c:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/android/sdk/samsung/a;->e:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/android/sdk/samsung/a;->e:I

    .line 57
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    sget v2, Lcom/mfluent/asp/sync/g;->b:I

    if-lez v2, :cond_2

    sget v2, Lcom/mfluent/asp/sync/g;->b:I

    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_2

    .line 61
    iget v2, p0, Lcom/samsung/android/sdk/samsung/a;->e:I

    const/4 v3, 0x4

    if-lt v2, v3, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/samsung/a;->e:I

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
