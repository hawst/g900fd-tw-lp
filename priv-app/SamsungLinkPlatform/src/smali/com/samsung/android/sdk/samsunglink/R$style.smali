.class public final Lcom/samsung/android/sdk/samsunglink/R$style;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final ASPBase:I = 0x7f0b002a

.field public static final ASPDialogWhenLarge:I = 0x7f0b002c

.field public static final ASPNoActionBar:I = 0x7f0b002b

.field public static final ASP_ActionBar:I = 0x7f0b0023

.field public static final ASP_ActionButton:I = 0x7f0b0024

.field public static final ASP_ActionMode:I = 0x7f0b0027

.field public static final ASP_CloseMode:I = 0x7f0b0025

.field public static final ASP_OverFlow:I = 0x7f0b0026

.field public static final ASP_TabBar:I = 0x7f0b0021

.field public static final ASP_TabView:I = 0x7f0b0022

.field public static final ASP_Theme:I = 0x7f0b002d

.field public static final ASP_Theme_DialogWhenLarge:I = 0x7f0b002f

.field public static final ASP_Theme_DialogWhenLarge_Transparent:I = 0x7f0b0030

.field public static final ASP_Theme_NoActionBar:I = 0x7f0b0032

.field public static final ASP_Theme_NoActionBarTitle:I = 0x7f0b0031

.field public static final ASP_Theme_NoActionBar_Transparent:I = 0x7f0b0033

.field public static final ASP_Theme_Settings:I = 0x7f0b0034

.field public static final ASP_Theme_Transparent:I = 0x7f0b0035

.field public static final ASP_Theme_Transparent_Exported:I = 0x7f0b0036

.field public static final ASP_Theme_Transparent_Sendto:I = 0x7f0b002e

.field public static final ActionBarSubTitle:I = 0x7f0b0029

.field public static final ActionBarTitle:I = 0x7f0b0028

.field public static final SLPFBase:I = 0x7f0b0007

.field public static final SLPFBase_Light:I = 0x7f0b0006

.field public static final SLPFDialogWhenLarge:I = 0x7f0b000b

.field public static final SLPFDialogWhenLarge_Light:I = 0x7f0b000a

.field public static final SLPFNoActionBar:I = 0x7f0b0009

.field public static final SLPFNoActionBar_Light:I = 0x7f0b0008

.field public static final SLPF_ActionBar:I = 0x7f0b0003

.field public static final SLPF_ActionBarTitle:I = 0x7f0b0001

.field public static final SLPF_ActionBarTitle_Light:I = 0x7f0b0000

.field public static final SLPF_ActionBar_Light:I = 0x7f0b0002

.field public static final SLPF_OverFlow:I = 0x7f0b0005

.field public static final SLPF_OverFlow_Light:I = 0x7f0b0004

.field public static final SLPF_Theme:I = 0x7f0b000c

.field public static final SLPF_Theme_Default_Transparent:I = 0x7f0b0010

.field public static final SLPF_Theme_Default_Transparent_Light:I = 0x7f0b0011

.field public static final SLPF_Theme_Default_Transparent_NoBackground:I = 0x7f0b0012

.field public static final SLPF_Theme_Default_Transparent_NoBackground_Light:I = 0x7f0b0013

.field public static final SLPF_Theme_DialogWhenLarge:I = 0x7f0b000e

.field public static final SLPF_Theme_DialogWhenLarge_Light:I = 0x7f0b000f

.field public static final SLPF_Theme_Light:I = 0x7f0b000d

.field public static final SLPF_Theme_NoActionBar:I = 0x7f0b0016

.field public static final SLPF_Theme_NoActionBarTitle:I = 0x7f0b0014

.field public static final SLPF_Theme_NoActionBarTitle_Light:I = 0x7f0b0015

.field public static final SLPF_Theme_NoActionBar_Light:I = 0x7f0b0017

.field public static final SLPF_Theme_NoActionBar_Transparent:I = 0x7f0b0018

.field public static final SLPF_Theme_NoActionBar_Transparent_Light:I = 0x7f0b0019

.field public static final SLPF_Theme_Search:I = 0x7f0b0020

.field public static final SLPF_Theme_Settings:I = 0x7f0b001a

.field public static final SLPF_Theme_Settings_Light:I = 0x7f0b001b

.field public static final SLPF_Theme_Transparent:I = 0x7f0b001c

.field public static final SLPF_Theme_Transparent_Exported:I = 0x7f0b001e

.field public static final SLPF_Theme_Transparent_Exported_Light:I = 0x7f0b001f

.field public static final SLPF_Theme_Transparent_Light:I = 0x7f0b001d

.field public static final account_full_text_style:I = 0x7f0b004c

.field public static final asp_progress_style:I = 0x7f0b0045

.field public static final customTheme:I = 0x7f0b004e

.field public static final dialog_style:I = 0x7f0b004b

.field public static final info_content_label_text_style:I = 0x7f0b003a

.field public static final info_line_view_style:I = 0x7f0b004a

.field public static final info_main_label_text_style:I = 0x7f0b0039

.field public static final info_value_text_style:I = 0x7f0b003b

.field public static final list_button_style:I = 0x7f0b003c

.field public static final settings_capacity_text:I = 0x7f0b0043

.field public static final settings_description_and_sub_text_style:I = 0x7f0b003e

.field public static final settings_divider:I = 0x7f0b0041

.field public static final settings_text_gravity:I = 0x7f0b0044

.field public static final settings_used_capacity_text:I = 0x7f0b0042

.field public static final storage_info_description_style:I = 0x7f0b0048

.field public static final storage_info_email_label_style:I = 0x7f0b0046

.field public static final storage_info_email_style:I = 0x7f0b0047

.field public static final storage_info_getmore_link_style:I = 0x7f0b0049

.field public static final storage_service_login_edit_text_style:I = 0x7f0b003f

.field public static final storage_service_signup_link_text_style:I = 0x7f0b0040

.field public static final subtitle_bar_title:I = 0x7f0b0038

.field public static final tab_strip:I = 0x7f0b004d

.field public static final title_bar_title:I = 0x7f0b0037

.field public static final tw_dialog_button_style_uiapp:I = 0x7f0b003d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
