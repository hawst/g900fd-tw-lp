.class public Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ACTION_CONFIRM_SAMSUNG_LINK_USAGE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_CONFIRM_SAMSUNG_LINK_USAGE"

.field public static final ACTION_LOCAL_MEDIA_REVERSE_GEO_SERVICE_STARTER:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.LOCAL_MEDIA_REVERSE_GEO_SERVICE_STARTER"

.field public static final ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE"

.field public static final ACTION_SDK_SIGNIN:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SDK_SIGNIN"

.field public static final BROADCAST_AUTH_CHANGED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_AUTH_CHANGED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BROADCAST_SIGNIN_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_SIGNIN_STATE_CHANGED"

.field public static final BROADCAST_TIME_DIFF_ERROR:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_TIME_DIFF_ERROR"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXTRA_SDK_SIGNIN_APP_LABEL:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.EXTRA_SDK_SIGNIN_APP_LABEL"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXTRA_SIGNIN_STATE_SIGNED_IN:Ljava/lang/String; = "SlinkSignInUtils.EXTRA_SIGNIN_STATE_SIGNED_IN"

.field private static final PLATFORM_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink"

.field private static final SAMSUNG_ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field public static final SIGN_IN_RESULT_ACCOUNT_FULL:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    .line 139
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    .locals 3

    .prologue
    .line 122
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 123
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 125
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    if-nez v0, :cond_1

    .line 126
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    .line 128
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public createSamsungLinkUsageConfirmationIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 209
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_CONFIRM_SAMSUNG_LINK_USAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.ui.ConfirmSamsungLinkUsageActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 215
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 216
    return-object v0
.end method

.method public createSignInActivityIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 180
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 181
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::createSignInActivityIntent context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :goto_0
    return-object v0

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 185
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 186
    if-eqz v2, :cond_1

    if-nez v1, :cond_2

    .line 188
    :cond_1
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SDK_SIGNIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 190
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.samsung.android.sdk.samsunglink"

    const-string v4, "com.mfluent.asp.ui.SignInActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 193
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.EXTRA_SDK_SIGNIN_APP_LABEL"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 194
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    move-object v0, v1

    .line 196
    goto :goto_0

    .line 186
    :cond_2
    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public getAuthInfo()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 303
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 304
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::getAuthInfo context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :goto_0
    return-object v0

    .line 309
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAuthInfo.NAME"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 319
    :goto_1
    if-nez v1, :cond_1

    .line 320
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::getAuthInfo result is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 316
    :catch_0
    move-exception v1

    const-string v1, "slinklib"

    const-string v2, "::getAuthInfo maybe platform is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_1

    .line 323
    :cond_1
    const-string v0, "method_result"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPlatformUpgradeIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 363
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 365
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.ui.UpgradeActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 368
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 369
    return-object v0
.end method

.method public getPlatformVersionCode()Ljava/lang/Integer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 379
    .line 381
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 392
    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return-object v0

    .line 382
    :catch_0
    move-exception v1

    .line 384
    sget-boolean v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v2, :cond_0

    .line 385
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v3, "::getPlatformVersion did not find Samsung Link Platform package: com.samsung.android.sdk.samsunglink"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v1, v0

    .line 391
    goto :goto_0

    .line 388
    :catch_1
    move-exception v1

    .line 390
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v3, "::getPlatformVersionCode null pointer exception "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v0

    goto :goto_0

    .line 392
    :cond_1
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1
.end method

.method public getSamsungAccountSignInIntent()Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 269
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 270
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::getSamsungAccountSignInIntent context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :goto_0
    return-object v0

    .line 275
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSamsungAccountSignInIntent.NAME"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 285
    :goto_1
    if-nez v1, :cond_1

    .line 286
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::getSamsungAccountSignInIntent result is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 282
    :catch_0
    move-exception v1

    const-string v1, "slinklib"

    const-string v2, "::getSamsungAccountSignInIntent maybe platform is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_1

    .line 290
    :cond_1
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSamsungAccountSignInIntent.RESULT_INTENT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 291
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public isPlatformEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 401
    .line 403
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v3, "com.samsung.android.sdk.samsunglink"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 410
    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 405
    :catch_0
    move-exception v1

    move-object v1, v2

    .line 409
    goto :goto_0

    .line 406
    :catch_1
    move-exception v1

    .line 408
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v4, "::getPlatformVersionCode null pointer exception "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    goto :goto_0
.end method

.method public isSignedIn()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 147
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-nez v1, :cond_1

    .line 150
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::isSignedIn context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :goto_0
    return v0

    .line 154
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetSignInStatus.NAME"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 164
    if-nez v1, :cond_2

    .line 165
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::isSignedIn result is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 159
    :catch_0
    move-exception v1

    .line 160
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v3, "::isSignedIn IllegalArgumentException"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 168
    :cond_2
    const-string v0, "method_result"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isUpgradeAvailable()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 333
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 334
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::isUpgradeAvailable context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :goto_0
    return v0

    .line 339
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsUpgradeAvailable.NAME"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 349
    :goto_1
    if-nez v1, :cond_1

    .line 350
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v2, "::isUpgradeAvailable result is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 346
    :catch_0
    move-exception v2

    const-string v2, "slinklib"

    const-string v3, "::isUpgradeAvailable maybe platform is disabled"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 353
    :cond_1
    const-string v0, "method_result"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public samsungAccountExists()Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public signIn()V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->signIn(Z)V

    .line 235
    return-void
.end method

.method public signIn(Z)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 246
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 247
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SignIn.INTENT_ARG_POST_FAILURE_NOTIFICATION"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SignIn.NAME"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :goto_0
    return-void

    .line 258
    :catch_0
    move-exception v0

    const-string v0, "slinklib"

    const-string v1, "::signIn maybe platform is disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startReverseGeocodeService()V
    .locals 4

    .prologue
    .line 417
    sget-boolean v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v0, :cond_0

    .line 418
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v1, "::startReverseGeocodeService"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 422
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->TAG:Ljava/lang/String;

    const-string v1, "::startReverseGeocodeService context is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    :goto_0
    return-void

    .line 425
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.LOCAL_MEDIA_REVERSE_GEO_SERVICE_STARTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 427
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.sdk.samsunglink"

    const-string v3, "com.mfluent.asp.sync.LocalMediaReverseGeoLocStarterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 431
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->addAppIdToIntent(Landroid/content/Intent;)V

    .line 432
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
