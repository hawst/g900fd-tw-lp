.class final Lcom/samsung/android/sdk/samsung/a$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsung/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 71
    invoke-static {}, Lcom/samsung/android/sdk/samsung/a;->e()Lcom/samsung/android/sdk/samsung/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/samsung/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a;

    move-result-object v0

    .line 75
    const-string v1, "siop_level_broadcast"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    const-string v1, "siop_level_broadcast"

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/a;->a()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 77
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsung/a;->a(I)V

    .line 78
    invoke-static {}, Lcom/samsung/android/sdk/samsung/a;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v2, "::onReceive: set SIOP level {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/samsung/a;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::onReceive: SIOP level not found"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
