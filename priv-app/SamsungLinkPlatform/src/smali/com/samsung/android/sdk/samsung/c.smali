.class public final Lcom/samsung/android/sdk/samsung/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/samsung/android/sdk/samsung/c;


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

.field private final d:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

.field private final e:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-nez p1, :cond_0

    .line 23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 26
    const-string v1, "slpf_pref_20"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->b:Landroid/content/SharedPreferences;

    .line 27
    new-instance v0, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/c;->b:Landroid/content/SharedPreferences;

    const-string v2, "video_quality_settings_enabled"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Boolean;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->c:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    .line 29
    new-instance v0, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/c;->b:Landroid/content/SharedPreferences;

    const-string v2, "service_info_settings_checked"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Boolean;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->d:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    .line 30
    new-instance v0, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/c;->b:Landroid/content/SharedPreferences;

    const-string v2, "notification_changed"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Boolean;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->e:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    .line 31
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/c;
    .locals 2

    .prologue
    .line 34
    const-class v1, Lcom/samsung/android/sdk/samsung/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/c;->a:Lcom/samsung/android/sdk/samsung/c;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/samsung/android/sdk/samsung/c;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsung/c;->a:Lcom/samsung/android/sdk/samsung/c;

    .line 37
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/c;->a:Lcom/samsung/android/sdk/samsung/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->c:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;->setValue(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->c:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b(Z)V
    .locals 2

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->d:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;->setValue(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->d:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 2

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->e:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;->setValue(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/c;->e:Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/prefs/BooleanPersistedField;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
