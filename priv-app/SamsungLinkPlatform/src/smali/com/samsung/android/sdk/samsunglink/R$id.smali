.class public final Lcom/samsung/android/sdk/samsunglink/R$id;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final TextView01:I = 0x7f090066

.field public static final TextView02:I = 0x7f090064

.field public static final account_full_scroll:I = 0x7f090051

.field public static final auto_upload_linearlayout:I = 0x7f090004

.field public static final banner:I = 0x7f09002f

.field public static final banner_msg:I = 0x7f090033

.field public static final basic_divider_1:I = 0x7f09001f

.field public static final basic_divider_2:I = 0x7f090021

.field public static final basic_negative_button:I = 0x7f09001e

.field public static final basic_neutral_button:I = 0x7f090020

.field public static final basic_positive_button:I = 0x7f090022

.field public static final button_layout:I = 0x7f09001d

.field public static final button_next_id:I = 0x7f0900c6

.field public static final cancel:I = 0x7f0900cf

.field public static final cancel_all:I = 0x7f0900cc

.field public static final cancel_button:I = 0x7f090031

.field public static final cancel_button_id:I = 0x7f090015

.field public static final cancel_button_text:I = 0x7f090016

.field public static final cb_password_change_show_password:I = 0x7f09006a

.field public static final cb_password_confirm_show_password:I = 0x7f09006e

.field public static final cb_password_setup_show_password:I = 0x7f090072

.field public static final cell5:I = 0x7f090007

.field public static final cell6:I = 0x7f090008

.field public static final cell7:I = 0x7f090009

.field public static final com_mfluent_asp_ui_hover_GenericActionHoverListener_IdleTimeoutChecker:I = 0x7f09000a

.field public static final com_mfluent_asp_util_bitmap_ImageWorker_tag:I = 0x7f090000

.field public static final complete_main_layout:I = 0x7f09004e

.field public static final completed_transfer_remove_title:I = 0x7f09004f

.field public static final content_separator:I = 0x7f09004d

.field public static final custom_layout:I = 0x7f09001c

.field public static final deregister:I = 0x7f0900ce

.field public static final deregister_button:I = 0x7f090076

.field public static final details_loading_layout:I = 0x7f09005a

.field public static final device1:I = 0x7f090054

.field public static final device1_layout:I = 0x7f09007e

.field public static final device2:I = 0x7f090055

.field public static final device2_layout:I = 0x7f09007f

.field public static final device3:I = 0x7f090056

.field public static final device3_layout:I = 0x7f090080

.field public static final device4:I = 0x7f090057

.field public static final device4_layout:I = 0x7f090081

.field public static final device5:I = 0x7f090058

.field public static final device5_layout:I = 0x7f090082

.field public static final device6:I = 0x7f090059

.field public static final device6_layout:I = 0x7f090083

.field public static final deviceItemsLayout:I = 0x7f090052

.field public static final device_capacity:I = 0x7f09008a

.field public static final device_image:I = 0x7f090011

.field public static final device_image_layout:I = 0x7f090089

.field public static final device_layout:I = 0x7f090088

.field public static final device_name:I = 0x7f090012

.field public static final device_network:I = 0x7f090075

.field public static final device_sub_text:I = 0x7f090013

.field public static final divider:I = 0x7f090028

.field public static final divider_lines:I = 0x7f090005

.field public static final divider_lines_full:I = 0x7f090006

.field public static final do_not_show_checkbox_id:I = 0x7f09002e

.field public static final do_you_want_to_signup_message:I = 0x7f090095

.field public static final done_button_id:I = 0x7f090017

.field public static final done_button_text:I = 0x7f090018

.field public static final download_button:I = 0x7f090032

.field public static final email_label_id:I = 0x7f090090

.field public static final empty:I = 0x7f09003a

.field public static final error_main_layout:I = 0x7f090050

.field public static final et_password_change_input_confirm:I = 0x7f090069

.field public static final et_password_change_input_new:I = 0x7f090067

.field public static final et_password_change_input_old:I = 0x7f090065

.field public static final et_password_confirm_input:I = 0x7f09006d

.field public static final et_password_setup_input:I = 0x7f090070

.field public static final et_password_setup_input_confirm:I = 0x7f090071

.field public static final file_transfer_fragment:I = 0x7f0900ca

.field public static final file_transfer_in_progress:I = 0x7f09000d

.field public static final header:I = 0x7f09003d

.field public static final header_content:I = 0x7f090041

.field public static final header_title:I = 0x7f09003e

.field public static final itemText:I = 0x7f090023

.field public static final item_header:I = 0x7f090027

.field public static final levelCheck:I = 0x7f090026

.field public static final levelSeek:I = 0x7f090025

.field public static final levelText:I = 0x7f090024

.field public static final licenseInfoWebView:I = 0x7f09005c

.field public static final linearLayout_cancel_signup_btns_id:I = 0x7f0900b7

.field public static final linearLayout_terms_conditions_checkbox_id:I = 0x7f0900b3

.field public static final linearLayout_terms_conditions_text_id:I = 0x7f0900b5

.field public static final list:I = 0x7f090039

.field public static final list_id:I = 0x7f090029

.field public static final loader_archivetransfer:I = 0x7f09000c

.field public static final loader_fileTransfer:I = 0x7f09000b

.field public static final loader_searchAllContents:I = 0x7f090001

.field public static final loading:I = 0x7f09002b

.field public static final loading_device_status_spinner:I = 0x7f090073

.field public static final loading_layout:I = 0x7f09002a

.field public static final loading_progress:I = 0x7f09005f

.field public static final loading_text:I = 0x7f090060

.field public static final main_frame:I = 0x7f09008b

.field public static final main_layout:I = 0x7f090078

.field public static final maintenance_time:I = 0x7f09002d

.field public static final message1:I = 0x7f090037

.field public static final message2:I = 0x7f090038

.field public static final message_text:I = 0x7f09001b

.field public static final ongoing_transfer_remove_title:I = 0x7f090040

.field public static final option_next:I = 0x7f0900d1

.field public static final option_switch:I = 0x7f0900d0

.field public static final overlay_dialog_layout:I = 0x7f09005e

.field public static final progress_main_layout:I = 0x7f09003b

.field public static final progressbar:I = 0x7f09006b

.field public static final radio_button:I = 0x7f090014

.field public static final realtabcontent:I = 0x7f0900cb

.field public static final reason_why_not_supported:I = 0x7f090087

.field public static final refresh_button:I = 0x7f090074

.field public static final register_pc_text_2:I = 0x7f090077

.field public static final registered_devices_bottom:I = 0x7f09007d

.field public static final registered_devices_header:I = 0x7f090079

.field public static final relativeLayout_cancel_signin_btns_id:I = 0x7f090096

.field public static final relogin_samsung_account_notification_prompt:I = 0x7f090010

.field public static final retry_all:I = 0x7f0900cd

.field public static final scrollView1:I = 0x7f090061

.field public static final send_to_container:I = 0x7f09007b

.field public static final send_to_contents_text:I = 0x7f09007c

.field public static final send_to_text:I = 0x7f09007a

.field public static final settings_storage_list_item_img_id:I = 0x7f09009b

.field public static final settings_storage_list_item_subtext_id:I = 0x7f09009d

.field public static final settings_storage_list_item_text_id:I = 0x7f09009c

.field public static final settings_storage_signin_button_id:I = 0x7f09009f

.field public static final settings_storage_signin_button_layout:I = 0x7f09009e

.field public static final settings_storage_signin_service_msg_id:I = 0x7f0900a0

.field public static final signin_button:I = 0x7f09009a

.field public static final signup_button:I = 0x7f0900ba

.field public static final storage1_layout:I = 0x7f090084

.field public static final storage2_layout:I = 0x7f090085

.field public static final storage3_layout:I = 0x7f090086

.field public static final storage_email_not_registered_error_id:I = 0x7f09008f

.field public static final storage_email_required_error_id:I = 0x7f09008e

.field public static final storage_general_signin_error_id:I = 0x7f0900a5

.field public static final storage_image_view_id:I = 0x7f0900a2

.field public static final storage_password_required_error_id:I = 0x7f090092

.field public static final storage_service_cancel_view_id:I = 0x7f0900bb

.field public static final storage_service_image_view_id:I = 0x7f09008d

.field public static final storage_service_logo_image_view_id:I = 0x7f0900bf

.field public static final storage_service_signin_btn_id:I = 0x7f090098

.field public static final storage_service_signin_cancel_btn_id:I = 0x7f090097

.field public static final storage_service_signin_email_editText_id:I = 0x7f090091

.field public static final storage_service_signin_linearLayout:I = 0x7f09008c

.field public static final storage_service_signin_password_editText_id:I = 0x7f090094

.field public static final storage_service_signin_password_label_id:I = 0x7f090093

.field public static final storage_service_signin_signup_text:I = 0x7f0900be

.field public static final storage_service_signin_signup_view_id:I = 0x7f0900bc

.field public static final storage_service_signin_signup_view_ok:I = 0x7f0900bd

.field public static final storage_service_signup_download_btn_id:I = 0x7f0900c4

.field public static final storage_service_signup_marketing_notification_id:I = 0x7f0900c2

.field public static final storage_service_signup_skip_btn_id:I = 0x7f0900c3

.field public static final storage_service_signup_success_label_id:I = 0x7f0900c0

.field public static final storage_success_webview_id:I = 0x7f0900c1

.field public static final storage_sugarsync_account_not_yet_activated_error_id:I = 0x7f0900a8

.field public static final storage_sugarsync_agree_to_terms_error_id:I = 0x7f0900b2

.field public static final storage_sugarsync_email_domain_invalid_error_id:I = 0x7f0900a7

.field public static final storage_sugarsync_email_invalid_error_id:I = 0x7f0900a6

.field public static final storage_sugarsync_email_required_error_id:I = 0x7f0900a3

.field public static final storage_sugarsync_email_user_already_exists_error_id:I = 0x7f0900a4

.field public static final storage_sugarsync_email_user_already_registered_error_id:I = 0x7f0900a9

.field public static final storage_sugarsync_password_format_error_id:I = 0x7f0900ac

.field public static final storage_sugarsync_password_required_error_id:I = 0x7f0900ab

.field public static final storage_sugarsync_passwords_not_matched_id:I = 0x7f0900ad

.field public static final storage_sugarsync_signup_email_editText_id:I = 0x7f0900aa

.field public static final storage_sugersync_signup_confirm_password_editText_id:I = 0x7f0900b1

.field public static final storage_sugersync_signup_confirm_password_label_id:I = 0x7f0900b0

.field public static final storage_sugersync_signup_password_editText_id:I = 0x7f0900af

.field public static final storage_sugersync_signup_password_label_id:I = 0x7f0900ae

.field public static final sugarsync_logo_id:I = 0x7f0900c5

.field public static final sugarsync_signup_btn_id:I = 0x7f0900b9

.field public static final sugarsync_signup_cancel_btn_id:I = 0x7f0900b8

.field public static final sugarsync_signup_linearLayout:I = 0x7f0900a1

.field public static final svu_uploader_error_icon:I = 0x7f0900c7

.field public static final svu_uploader_error_text:I = 0x7f0900c9

.field public static final svu_uploader_error_title:I = 0x7f0900c8

.field public static final terms_and_conditions_checkbox_id:I = 0x7f0900b4

.field public static final terms_and_conditions_i_agree_text_id:I = 0x7f0900b6

.field public static final textView1:I = 0x7f090053

.field public static final textView2:I = 0x7f090062

.field public static final textView3:I = 0x7f090063

.field public static final textView4:I = 0x7f090068

.field public static final textview:I = 0x7f09002c

.field public static final title:I = 0x7f09001a

.field public static final title_layout:I = 0x7f090019

.field public static final title_text:I = 0x7f090099

.field public static final tooltip:I = 0x7f09004b

.field public static final tooltip_bg_layout:I = 0x7f09004c

.field public static final top_overlay:I = 0x7f09006c

.field public static final trans_intro_title:I = 0x7f090030

.field public static final transfer_arrow:I = 0x7f090049

.field public static final transfer_button:I = 0x7f09003c

.field public static final transfer_destination:I = 0x7f09004a

.field public static final transfer_icon:I = 0x7f090042

.field public static final transfer_layout:I = 0x7f090043

.field public static final transfer_progress:I = 0x7f090036

.field public static final transfer_progress_text:I = 0x7f090035

.field public static final transfer_removeButton:I = 0x7f09003f

.field public static final transfer_size:I = 0x7f090046

.field public static final transfer_source:I = 0x7f090048

.field public static final transfer_source_destination_layout:I = 0x7f090047

.field public static final transfer_status:I = 0x7f090045

.field public static final transfer_status_layout:I = 0x7f090044

.field public static final transfer_text:I = 0x7f090034

.field public static final tutorial_overlay_dands_header:I = 0x7f090003

.field public static final tutorial_overlay_recent_contents_header:I = 0x7f090002

.field public static final tv_password_confirm_forgot:I = 0x7f09006f

.field public static final update_notification_prompt:I = 0x7f09000e

.field public static final verify_samsung_account_notification_prompt:I = 0x7f09000f

.field public static final viewpager:I = 0x7f09005b

.field public static final webViewPlaceholder:I = 0x7f09005d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
