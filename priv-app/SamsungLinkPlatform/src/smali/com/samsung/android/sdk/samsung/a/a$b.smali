.class final Lcom/samsung/android/sdk/samsung/a/a$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsung/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsung/a/a;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/samsung/a/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput-object p2, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->b:Ljava/lang/String;

    .line 261
    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 265
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onServiceConnected(): ComponentName : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onServiceConnected(): IBinder : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {p2}, Lcom/samsung/android/hostmanager/aidl/d$a;->a(Landroid/os/IBinder;)Lcom/samsung/android/hostmanager/aidl/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/samsung/android/hostmanager/aidl/d;)Lcom/samsung/android/hostmanager/aidl/d;

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    new-instance v1, Lcom/samsung/android/sdk/samsung/a/a$a;

    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/samsung/a/a$a;-><init>(Lcom/samsung/android/sdk/samsung/a/a;B)V

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/samsung/android/hostmanager/aidl/b;)Lcom/samsung/android/hostmanager/aidl/b;

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->b(Lcom/samsung/android/sdk/samsung/a/a;)Lcom/samsung/android/hostmanager/aidl/d;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;)Lcom/samsung/android/hostmanager/aidl/b;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v2}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;)Lcom/samsung/android/hostmanager/aidl/b;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/hostmanager/aidl/d;->a(Ljava/lang/String;Lcom/samsung/android/hostmanager/aidl/b;)Z

    move-result v0

    .line 278
    if-eqz v0, :cond_1

    .line 279
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::registerEXAPPListener() is success"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->c(Lcom/samsung/android/sdk/samsung/a/a;)Z

    .line 286
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->b:Ljava/lang/String;

    const-string v1, "getWearableList"

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->d(Lcom/samsung/android/sdk/samsung/a/a;)Z

    .line 301
    :cond_0
    :goto_1
    monitor-exit p0

    .line 307
    :goto_2
    return-void

    .line 281
    :cond_1
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::registerEXAPPListener() is fail"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 301
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 305
    :catch_0
    move-exception v0

    .line 303
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::onServiceConnected() failed :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 290
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;Ljava/lang/String;)Z

    move-result v0

    .line 292
    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 295
    :try_start_4
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->f(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 297
    :try_start_5
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 311
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onServiceDisconnected(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 315
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->f(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 320
    monitor-enter p0

    .line 321
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/a/a;->a()V

    .line 322
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 317
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a$b;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 322
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
