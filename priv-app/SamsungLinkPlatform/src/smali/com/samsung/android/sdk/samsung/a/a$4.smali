.class final Lcom/samsung/android/sdk/samsung/a/a$4;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/samsung/a/a;->b(Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/mfluent/asp/datamodel/Device;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsung/a/a;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/samsung/a/a;)V
    .locals 0

    .prologue
    .line 737
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/a/a$4;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 737
    check-cast p1, [Lcom/mfluent/asp/datamodel/Device;

    aget-object v1, p1, v0

    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doInBackground:AsyncTask: updateWearableDeviceNameInUP("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a$4;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$4;->a:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsung/a/a;->b(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_1

    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::updateWearableDeviceNameInUP(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : updated in UP server!!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::updateWearableDeviceNameInUP(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : fail to update UP server!!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
