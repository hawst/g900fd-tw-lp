.class public final Lcom/samsung/android/sdk/samsunglink/R$styleable;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final SLPFCommonUIStyles:[I

.field public static final SLPFCommonUIStyles_full_account_device_name_color:I = 0xb

.field public static final SLPFCommonUIStyles_full_account_device_sub_text_color:I = 0xc

.field public static final SLPFCommonUIStyles_full_account_header_text_color:I = 0xa

.field public static final SLPFCommonUIStyles_full_account_list_divider_color:I = 0xd

.field public static final SLPFCommonUIStyles_is_ui_theme:I = 0x0

.field public static final SLPFCommonUIStyles_list_bg_color:I = 0x1

.field public static final SLPFCommonUIStyles_registration_guide_text_color:I = 0x2

.field public static final SLPFCommonUIStyles_sendto_background:I = 0x3

.field public static final SLPFCommonUIStyles_sendto_bg_circle_selector:I = 0x9

.field public static final SLPFCommonUIStyles_sendto_contents_text_color:I = 0x5

.field public static final SLPFCommonUIStyles_sendto_device_capacity_color:I = 0x8

.field public static final SLPFCommonUIStyles_sendto_device_name_color:I = 0x7

.field public static final SLPFCommonUIStyles_sendto_device_network_color:I = 0x6

.field public static final SLPFCommonUIStyles_sendto_text_color:I = 0x4

.field public static final SLPFCommonUIStyles_transfer_arrow:I = 0x15

.field public static final SLPFCommonUIStyles_transfer_content_list_color:I = 0x1b

.field public static final SLPFCommonUIStyles_transfer_content_list_header_color:I = 0x1c

.field public static final SLPFCommonUIStyles_transfer_content_list_header_separator_color:I = 0x1d

.field public static final SLPFCommonUIStyles_transfer_content_separator_color:I = 0x16

.field public static final SLPFCommonUIStyles_transfer_empty_icon:I = 0xe

.field public static final SLPFCommonUIStyles_transfer_empty_text_color:I = 0xf

.field public static final SLPFCommonUIStyles_transfer_header_butotn_text_color:I = 0x12

.field public static final SLPFCommonUIStyles_transfer_header_button_bg:I = 0x11

.field public static final SLPFCommonUIStyles_transfer_header_title_color:I = 0x10

.field public static final SLPFCommonUIStyles_transfer_item_text_color:I = 0x13

.field public static final SLPFCommonUIStyles_transfer_status_text_color:I = 0x14

.field public static final SLPFCommonUIStyles_transfer_tooltip_arrow:I = 0x18

.field public static final SLPFCommonUIStyles_transfer_tooltip_bg:I = 0x17

.field public static final SLPFCommonUIStyles_transfer_tooltip_cancel:I = 0x1a

.field public static final SLPFCommonUIStyles_transfer_tooltip_text_color:I = 0x19


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3266
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/R$styleable;->SLPFCommonUIStyles:[I

    return-void

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
