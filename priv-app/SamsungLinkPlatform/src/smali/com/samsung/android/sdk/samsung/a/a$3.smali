.class final Lcom/samsung/android/sdk/samsung/a/a$3;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/samsung/a/a;->c(Lcom/mfluent/asp/datamodel/Device;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/mfluent/asp/datamodel/Device;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic b:Lcom/samsung/android/sdk/samsung/a/a;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 652
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/a/a$3;->b:Lcom/samsung/android/sdk/samsung/a/a;

    iput-object p2, p0, Lcom/samsung/android/sdk/samsung/a/a$3;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a([Lcom/mfluent/asp/datamodel/Device;)Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 656
    aget-object v2, p1, v1

    .line 658
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doInBackground:AsyncTask: addWearableDeviceInUP("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a$3;->b:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->g(Lcom/samsung/android/sdk/samsung/a/a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    .line 665
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/a/a$3;->b:Lcom/samsung/android/sdk/samsung/a/a;

    invoke-static {v3}, Lcom/samsung/android/sdk/samsung/a/a;->g(Lcom/samsung/android/sdk/samsung/a/a;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v3

    .line 666
    iget-object v4, p0, Lcom/samsung/android/sdk/samsung/a/a$3;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3, v4, v0}, Lcom/mfluent/asp/b/i;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/sec/pcw/hybrid/b/b;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 671
    :goto_0
    if-eqz v0, :cond_0

    .line 672
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::addWearableDeviceInUP("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") add in UP server!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 667
    :catch_0
    move-exception v0

    .line 668
    const-string v3, "mfl_WearableDeviceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::addWearableDeviceInUP : Trouble disable because "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 674
    :cond_0
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::addWearableDeviceInUP("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") fail to add UP server!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 652
    check-cast p1, [Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a$3;->a([Lcom/mfluent/asp/datamodel/Device;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
