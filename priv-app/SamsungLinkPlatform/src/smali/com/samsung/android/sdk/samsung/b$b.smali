.class final Lcom/samsung/android/sdk/samsung/b$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsung/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsung/b;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/samsung/b;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/samsung/b;B)V
    .locals 0

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/b$b;-><init>(Lcom/samsung/android/sdk/samsung/b;)V

    return-void
.end method

.method private a()Ljava/lang/Void;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/32 v4, 0x3200000

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 494
    .line 499
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/b;->a(Lcom/samsung/android/sdk/samsung/b;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/b;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 502
    const-wide/32 v8, 0x6400000

    cmp-long v1, v2, v8

    if-gez v1, :cond_0

    .line 503
    const-string v0, "INFO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "CacheBackgroudJob: no need to delete cache files nTotalSize="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    :goto_0
    return-object v6

    .line 506
    :cond_0
    const-string v1, "INFO"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "CacheBackgroudJob: start job nTotalSize="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", MAX_NETRES_CACHE_SIZE=104857600"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v6

    move v7, v0

    .line 508
    :goto_1
    cmp-long v0, v2, v4

    if-ltz v0, :cond_4

    const/16 v0, 0x3e8

    if-ge v7, v0, :cond_4

    .line 511
    const-string v0, "SELECT KEY, filename, MIN(checked_time) FROM cache_table;"

    .line 512
    iget-object v8, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v8}, Lcom/samsung/android/sdk/samsung/b;->b(Lcom/samsung/android/sdk/samsung/b;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 515
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v8}, Lcom/samsung/android/sdk/samsung/b;->c(Lcom/samsung/android/sdk/samsung/b;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 516
    if-eqz v8, :cond_5

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 517
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 518
    const-string v0, "filename"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 519
    const-string v9, "KEY"

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    .line 521
    :goto_2
    if-eqz v8, :cond_1

    .line 522
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 524
    :cond_1
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 526
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v9}, Lcom/samsung/android/sdk/samsung/b;->a(Lcom/samsung/android/sdk/samsung/b;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 527
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 528
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 529
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v10

    sub-long/2addr v2, v10

    .line 530
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 533
    :cond_2
    const-string v8, "KEY =?"

    .line 534
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    .line 535
    iget-object v10, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v10}, Lcom/samsung/android/sdk/samsung/b;->c(Lcom/samsung/android/sdk/samsung/b;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    const-string v11, "cache_table"

    invoke-virtual {v10, v11, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 539
    const-string v8, "INFO"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "CacheBackgroudJob: delete strPath="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ",nTotalSize="

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 544
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/b;->b(Lcom/samsung/android/sdk/samsung/b;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 546
    :goto_3
    add-int/lit8 v1, v7, 0x1

    .line 547
    rem-int/lit8 v7, v1, 0xa

    const/16 v8, 0x9

    if-ne v7, v8, :cond_6

    .line 548
    const-wide/16 v8, 0x64

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    move v7, v1

    move-object v1, v0

    goto/16 :goto_1

    .line 544
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/b;->b(Lcom/samsung/android/sdk/samsung/b;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 551
    :cond_4
    const-string v0, "INFO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CheckCacheBackgroudJob reducing job done nLoopCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 540
    :catch_0
    move-exception v1

    .line 541
    :goto_4
    :try_start_2
    const-string v2, "mfl_CacheManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "CheckCacheBackgroudJob error="

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 544
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/b;->b(Lcom/samsung/android/sdk/samsung/b;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-wide v2, v4

    .line 545
    goto :goto_3

    .line 544
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/b$b;->a:Lcom/samsung/android/sdk/samsung/b;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsung/b;->b(Lcom/samsung/android/sdk/samsung/b;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 540
    :catch_1
    move-exception v0

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    goto :goto_4

    :cond_5
    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_2

    :cond_6
    move v7, v1

    move-object v1, v0

    goto/16 :goto_1
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsung/b$b;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
