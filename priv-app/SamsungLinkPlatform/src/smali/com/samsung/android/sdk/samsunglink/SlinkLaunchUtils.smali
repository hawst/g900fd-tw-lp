.class public Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils$LaunchToContentTypes;
    }
.end annotation


# static fields
.field public static final BROADCAST_SAMSUNG_LINK_EXITED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.BROADCAST_SAMSUNG_LINK_EXITED"

.field public static final BROADCAST_SAMSUNG_LINK_STARTED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.BROADCAST_SAMSUNG_LINK_STARTED"

.field public static final EXTRA_LAUNCH_TO_CONTENT_TYPE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_CONTENT_TYPE"

.field public static final EXTRA_LAUNCH_TO_DEVICE_ID:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_DEVICE_ID"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static createLaunchToDeviceIntent(JJ)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.ui.MobileChargesNotificationActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.pcw"

    const-string v3, "com.mfluent.asp.ui.MobileChargesNotificationActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 64
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_DEVICE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 65
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_CONTENT_TYPE"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 67
    return-object v0
.end method
