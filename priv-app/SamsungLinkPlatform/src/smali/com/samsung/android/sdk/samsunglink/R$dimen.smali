.class public final Lcom/samsung/android/sdk/samsunglink/R$dimen;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final aboutservice_bottom_margin_1:I = 0x7f0700b2

.field public static final aboutservice_bottom_margin_2:I = 0x7f0700b6

.field public static final account_full_cell_device_image_size:I = 0x7f070055

.field public static final account_full_cell_height:I = 0x7f070014

.field public static final account_full_cell_image_margin_left:I = 0x7f070016

.field public static final account_full_cell_image_margin_right:I = 0x7f070017

.field public static final account_full_cell_layout_min_height:I = 0x7f070015

.field public static final account_full_cell_radio_button_margin_right:I = 0x7f07001b

.field public static final account_full_cell_radio_button_size:I = 0x7f07001a

.field public static final account_full_cell_subtext_size:I = 0x7f070019

.field public static final account_full_cell_text_size:I = 0x7f070018

.field public static final account_full_divider_margin_bottom:I = 0x7f070052

.field public static final account_full_text_margin_bottom:I = 0x7f070054

.field public static final account_full_text_margin_left:I = 0x7f070050

.field public static final account_full_text_margin_right:I = 0x7f070051

.field public static final account_full_text_margin_top:I = 0x7f070053

.field public static final account_full_text_min_height:I = 0x7f07004f

.field public static final account_full_text_size:I = 0x7f07004e

.field public static final account_logo_left_margin:I = 0x7f0701d7

.field public static final account_name_text_size:I = 0x7f0701d9

.field public static final account_service_text_size:I = 0x7f0701d8

.field public static final action_bar_title_land:I = 0x7f070191

.field public static final action_button_height:I = 0x7f0700e4

.field public static final action_button_padding:I = 0x7f0700e5

.field public static final actionbar_subtitle_text_size:I = 0x7f07001f

.field public static final actionbar_title_text_size:I = 0x7f07001e

.field public static final album_art_dimen:I = 0x7f0701b7

.field public static final album_art_left_margin:I = 0x7f07008d

.field public static final album_art_top_margin:I = 0x7f0701b8

.field public static final appinapp_init_width:I = 0x7f07018f

.field public static final appinapp_min_width:I = 0x7f070190

.field public static final artist_frag_padding_between_tablet:I = 0x7f07022f

.field public static final asp_progress_style_layout_height:I = 0x7f0701c1

.field public static final asp_progress_style_layout_width:I = 0x7f0701c0

.field public static final attach_device_capacity_size:I = 0x7f0701de

.field public static final attach_device_name_size:I = 0x7f0701dd

.field public static final attach_header_text_size:I = 0x7f0701dc

.field public static final auto_upload_button_width:I = 0x7f07015c

.field public static final auto_upload_image_arrow_height:I = 0x7f07015f

.field public static final auto_upload_image_height:I = 0x7f070081

.field public static final auto_upload_image_illust_height:I = 0x7f070160

.field public static final auto_upload_image_text_size:I = 0x7f070161

.field public static final auto_upload_margin:I = 0x7f07015b

.field public static final auto_upload_mobile_data_height:I = 0x7f070164

.field public static final auto_upload_mobile_data_text_size:I = 0x7f070165

.field public static final auto_upload_spinner_height:I = 0x7f07015e

.field public static final auto_upload_spinner_width:I = 0x7f07015d

.field public static final auto_upload_type_button_height:I = 0x7f070162

.field public static final auto_upload_type_button_width:I = 0x7f070163

.field public static final button_layout_left_margin:I = 0x7f0701b6

.field public static final button_text_size:I = 0x7f070186

.field public static final capacity_info_between_margin:I = 0x7f07026e

.field public static final capacity_info_height:I = 0x7f07026b

.field public static final capacity_info_min_width:I = 0x7f07026c

.field public static final capacity_info_text_size:I = 0x7f07026d

.field public static final circle_capacity_text_size:I = 0x7f070268

.field public static final circle_view_device_icon_height:I = 0x7f070269

.field public static final circle_view_device_icon_margin_top:I = 0x7f07026a

.field public static final circle_view_height:I = 0x7f070264

.field public static final circle_view_padding_top:I = 0x7f070266

.field public static final content_bottom_margin:I = 0x7f070178

.field public static final content_inner_margin:I = 0x7f07017a

.field public static final content_outer_margin:I = 0x7f070179

.field public static final content_top_margin:I = 0x7f070177

.field public static final contents_tab_height:I = 0x7f0700e6

.field public static final contents_tab_image_height:I = 0x7f0700e7

.field public static final controls_bottom_margin:I = 0x7f0701bb

.field public static final controls_button_dimen:I = 0x7f0701bc

.field public static final controls_button_margin_1:I = 0x7f070090

.field public static final controls_button_margin_2:I = 0x7f070091

.field public static final controls_layout_height:I = 0x7f0701b9

.field public static final controls_layout_width:I = 0x7f07008e

.field public static final controls_right_margin:I = 0x7f07008f

.field public static final controls_top_margin:I = 0x7f0701ba

.field public static final cross_device_delete_cell_height:I = 0x7f0701e1

.field public static final cross_device_delete_checkbox_right_margin:I = 0x7f070297

.field public static final cross_device_delete_list_height:I = 0x7f0701e3

.field public static final cross_device_dialog_width:I = 0x7f0701e2

.field public static final device_allcontent_name_text_size:I = 0x7f0701db

.field public static final device_bottom_margin:I = 0x7f07014b

.field public static final device_cell_device_name_text_size:I = 0x7f070174

.field public static final device_cell_image_to_text_padding:I = 0x7f070173

.field public static final device_cell_padding_left:I = 0x7f070171

.field public static final device_cell_padding_right:I = 0x7f070172

.field public static final device_cell_subtext_size:I = 0x7f070175

.field public static final device_height:I = 0x7f070132

.field public static final device_help_bottom_margin:I = 0x7f07014e

.field public static final device_help_width:I = 0x7f070131

.field public static final device_image_height:I = 0x7f07014a

.field public static final device_image_width:I = 0x7f070149

.field public static final device_info_circle_diameter:I = 0x7f07025e

.field public static final device_info_circle_small_diameter:I = 0x7f070260

.field public static final device_info_circle_small_stroke_width:I = 0x7f07025f

.field public static final device_info_circle_stroke_width:I = 0x7f07025d

.field public static final device_info_storage_min_width:I = 0x7f0701e5

.field public static final device_name_text_size:I = 0x7f0701da

.field public static final device_section_height:I = 0x7f07013f

.field public static final device_section_height_softkey:I = 0x7f070141

.field public static final device_width:I = 0x7f070130

.field public static final dialog_text_bottom_margin:I = 0x7f070197

.field public static final dialog_text_side_margin:I = 0x7f070198

.field public static final dialog_text_size:I = 0x7f070199

.field public static final dialog_text_top_margin:I = 0x7f070196

.field public static final disclaimer_text_width:I = 0x7f07024e

.field public static final dlna_thumbnail_height:I = 0x7f070005

.field public static final dlna_thumbnail_width:I = 0x7f070004

.field public static final do_not_show_again_checkbox_to_text_padding:I = 0x7f070008

.field public static final document_list_name_width:I = 0x7f070242

.field public static final document_list_subtext_width:I = 0x7f070243

.field public static final document_player_tap_screen_text_size:I = 0x7f07012c

.field public static final download_app_btn_text_size:I = 0x7f070112

.field public static final download_app_btn_width:I = 0x7f070111

.field public static final download_banner_description_one_margin_left:I = 0x7f070030

.field public static final download_banner_description_one_margin_right:I = 0x7f070031

.field public static final download_dialog_left_padding:I = 0x7f07001d

.field public static final drawer_add_storage_height:I = 0x7f07028a

.field public static final drawer_add_storage_width:I = 0x7f070289

.field public static final drawer_device_item_width:I = 0x7f0700b5

.field public static final drawer_device_layout_height:I = 0x7f070283

.field public static final drawer_edge_grab_width:I = 0x7f0701c4

.field public static final drawer_help_textsize:I = 0x7f070280

.field public static final drawer_image_right_padding:I = 0x7f0701c7

.field public static final drawer_item_margin:I = 0x7f070003

.field public static final drawer_item_margin_right:I = 0x7f0701c6

.field public static final drawer_learn_more_textsize:I = 0x7f070281

.field public static final drawer_logo_layout_height:I = 0x7f070282

.field public static final drawer_open_width:I = 0x7f0701c3

.field public static final drawer_option_button_height:I = 0x7f070285

.field public static final drawer_option_button_left_right_padding:I = 0x7f070287

.field public static final drawer_option_button_top_bottom_padding:I = 0x7f070288

.field public static final drawer_option_button_width:I = 0x7f070286

.field public static final drawer_refresh_width:I = 0x7f070002

.field public static final drawer_section_header_margin:I = 0x7f0701c5

.field public static final drawer_shadow_width:I = 0x7f0701c8

.field public static final drawer_spinner_width:I = 0x7f070284

.field public static final emptyList_image_size:I = 0x7f0700ea

.field public static final emptyList_image_top:I = 0x7f070069

.field public static final emptyList_text1_size:I = 0x7f0700eb

.field public static final emptyList_text1_top:I = 0x7f07006a

.field public static final emptyList_text2_size:I = 0x7f0700ec

.field public static final emptyList_text2_top:I = 0x7f07006b

.field public static final emptyList_text_side:I = 0x7f0700ed

.field public static final empty_list_top_margin:I = 0x7f0700c8

.field public static final file_list_cell_height:I = 0x7f0700e8

.field public static final file_list_item_name_size:I = 0x7f070101

.field public static final file_list_item_subname_size:I = 0x7f070102

.field public static final file_list_path_top_margin:I = 0x7f070244

.field public static final file_path_text_size:I = 0x7f0700cf

.field public static final file_thumbnail_size:I = 0x7f070100

.field public static final full_account_header_text_top_margin:I = 0x7f070021

.field public static final genre_cell_height:I = 0x7f07023d

.field public static final genre_cell_width:I = 0x7f07023c

.field public static final genre_paddingLeft_tablet:I = 0x7f07023e

.field public static final genre_paddingRight_tablet:I = 0x7f07023f

.field public static final genre_paddingTop_tablet:I = 0x7f070240

.field public static final genre_padding_between_tablet:I = 0x7f070241

.field public static final home_header_progress_bar:I = 0x7f070001

.field public static final home_land_left_pane_width:I = 0x7f07013a

.field public static final home_land_left_pane_width_softkey:I = 0x7f07013c

.field public static final home_land_right_pane_width:I = 0x7f07013b

.field public static final home_land_right_pane_width_softkey:I = 0x7f07013d

.field public static final home_section_header_height:I = 0x7f07012d

.field public static final home_section_header_padding:I = 0x7f07012f

.field public static final home_section_header_text_size:I = 0x7f07012e

.field public static final homesync_vault_login_checkbox_margin_top:I = 0x7f07005d

.field public static final homesync_vault_login_layout2_margin_top:I = 0x7f07005c

.field public static final homesync_vault_login_layout_margin_left:I = 0x7f070056

.field public static final homesync_vault_login_layout_margin_right:I = 0x7f070057

.field public static final homesync_vault_login_textview1_margin_top:I = 0x7f070058

.field public static final homesync_vault_login_textview1_size:I = 0x7f070059

.field public static final homesync_vault_login_textview2_margin_top:I = 0x7f07005a

.field public static final homesync_vault_login_textview2_size:I = 0x7f07005b

.field public static final image_dimen:I = 0x7f0701a7

.field public static final image_hover_total_margin:I = 0x7f070192

.field public static final image_layout_dimen:I = 0x7f0701a6

.field public static final image_right_margin:I = 0x7f0701a9

.field public static final image_top_margin:I = 0x7f0701a8

.field public static final info_settings_text_size:I = 0x7f07026f

.field public static final intro_bg_gradient:I = 0x7f07018e

.field public static final intro_bottom_margin_1:I = 0x7f0700b1

.field public static final intro_dots_bottom_margin:I = 0x7f070094

.field public static final intro_horizontal_scroll_height:I = 0x7f07018d

.field public static final intro_image_height:I = 0x7f07018a

.field public static final intro_image_layout_height:I = 0x7f070189

.field public static final intro_image_margin_right:I = 0x7f070095

.field public static final intro_image_margin_top:I = 0x7f070097

.field public static final intro_image_width:I = 0x7f070298

.field public static final intro_logo_text_size:I = 0x7f07027c

.field public static final intro_text1_size:I = 0x7f07018b

.field public static final intro_text3_size:I = 0x7f07018c

.field public static final intro_text_margin_top:I = 0x7f070096

.field public static final intro_text_width:I = 0x7f0701ff

.field public static final intro_video_button_bottom_margin:I = 0x7f070184

.field public static final intro_video_button_text_size:I = 0x7f07027b

.field public static final list_cell_checkbox_margin_left:I = 0x7f0700d5

.field public static final list_cell_checkbox_margin_right:I = 0x7f0700d6

.field public static final list_cell_height:I = 0x7f0700d9

.field public static final list_cell_height_photo_location:I = 0x7f0700da

.field public static final list_cell_margin:I = 0x7f070000

.field public static final list_cell_margin_photo_location:I = 0x7f0700db

.field public static final list_cell_name_font:I = 0x7f0700e0

.field public static final list_cell_new_icon_bottom_margin:I = 0x7f0700d4

.field public static final list_cell_new_icon_margin:I = 0x7f0700d2

.field public static final list_cell_new_icon_size:I = 0x7f0700d1

.field public static final list_cell_new_icon_top_margin:I = 0x7f0700d3

.field public static final list_cell_photo_location_subtitle_tize:I = 0x7f0700dd

.field public static final list_cell_photo_location_title_tize:I = 0x7f0700dc

.field public static final list_cell_playing_height:I = 0x7f0700d8

.field public static final list_cell_right_panel_margin_right:I = 0x7f0700d7

.field public static final list_cell_right_panel_width:I = 0x7f0700e2

.field public static final list_cell_subname_font:I = 0x7f0700e1

.field public static final list_cell_text_margin_left:I = 0x7f0700de

.field public static final list_text_cell_height:I = 0x7f0700e9

.field public static final list_thumbnail_size:I = 0x7f0700df

.field public static final loading_info_cell_height:I = 0x7f07016b

.field public static final loading_info_cell_non_spinner_padding_top:I = 0x7f07016e

.field public static final loading_info_cell_padding_left:I = 0x7f07016c

.field public static final loading_info_cell_padding_right:I = 0x7f07016d

.field public static final loading_info_cell_spinner_size:I = 0x7f070170

.field public static final loading_info_cell_text_size:I = 0x7f07016f

.field public static final lock_image_top_margin:I = 0x7f070082

.field public static final lock_left_margin:I = 0x7f070169

.field public static final lock_right_margin:I = 0x7f07016a

.field public static final login_button_height:I = 0x7f070182

.field public static final login_button_margin:I = 0x7f070183

.field public static final login_button_width:I = 0x7f07027a

.field public static final login_margin:I = 0x7f070093

.field public static final login_margin_top:I = 0x7f070279

.field public static final metro_cell_large:I = 0x7f070062

.field public static final metro_cell_large_softkey:I = 0x7f070067

.field public static final metro_cell_layout_margin:I = 0x7f070061

.field public static final metro_cell_small:I = 0x7f070063

.field public static final metro_cell_small_softkey:I = 0x7f070068

.field public static final metro_cell_tab_loc:I = 0x7f070064

.field public static final metro_cell_thumb_sz:I = 0x7f070060

.field public static final metro_empty_margin:I = 0x7f0700c1

.field public static final metro_frag_padding_left:I = 0x7f070065

.field public static final metro_frag_padding_left_softkey:I = 0x7f0700c2

.field public static final metro_frag_padding_right:I = 0x7f070066

.field public static final metro_frag_padding_right_softkey:I = 0x7f0700c3

.field public static final music_2d_list_73_height:I = 0x7f0701ec

.field public static final music_2d_list_height:I = 0x7f0701eb

.field public static final music_2d_main_content_height:I = 0x7f0701e6

.field public static final music_2d_main_content_img:I = 0x7f0701e7

.field public static final music_2d_main_content_width:I = 0x7f07022b

.field public static final music_2d_main_padding_left:I = 0x7f0701e8

.field public static final music_2d_main_padding_right:I = 0x7f0701e9

.field public static final music_2d_main_padding_top:I = 0x7f07022d

.field public static final music_2d_main_text_margin_left:I = 0x7f0701ea

.field public static final music_2d_main_text_padding_top:I = 0x7f07022c

.field public static final music_2d_separator_image_padding:I = 0x7f0700fb

.field public static final music_2d_separator_image_padding_left:I = 0x7f0700fc

.field public static final music_2d_separator_text_margin_left:I = 0x7f0700fd

.field public static final music_2d_separator_text_margin_right:I = 0x7f0700fe

.field public static final music_2depth_album_sub_text:I = 0x7f070232

.field public static final music_2depth_album_text:I = 0x7f070231

.field public static final music_2depth_top_cell_image_size_tablet:I = 0x7f0700fa

.field public static final music_cell_height:I = 0x7f070071

.field public static final music_cell_separator_height:I = 0x7f0701df

.field public static final music_cell_seperator_sub_title_font:I = 0x7f0701e0

.field public static final music_cell_thumb_height:I = 0x7f07006f

.field public static final music_cell_thumb_sz_tablet:I = 0x7f070230

.field public static final music_cell_thumb_width:I = 0x7f07006e

.field public static final music_cell_width:I = 0x7f070070

.field public static final music_frag_paddingLeft_tablet:I = 0x7f070072

.field public static final music_frag_paddingRight_tablet:I = 0x7f070073

.field public static final music_frag_paddingTop_tablet:I = 0x7f070074

.field public static final music_frag_padding_between_tablet:I = 0x7f070075

.field public static final music_player_album_height:I = 0x7f0700a2

.field public static final music_player_album_height_land:I = 0x7f070117

.field public static final music_player_album_layout_height:I = 0x7f0700a0

.field public static final music_player_album_layout_height_land:I = 0x7f070115

.field public static final music_player_album_layout_width:I = 0x7f07009f

.field public static final music_player_album_layout_width_land:I = 0x7f070114

.field public static final music_player_album_width:I = 0x7f0700a1

.field public static final music_player_album_width_land:I = 0x7f070116

.field public static final music_player_middle_spacer:I = 0x7f07011a

.field public static final music_player_middle_spacer_land:I = 0x7f070118

.field public static final music_player_top_spacer:I = 0x7f07009e

.field public static final music_player_top_spacer_land:I = 0x7f070113

.field public static final music_player_top_spacer_softkey:I = 0x7f070119

.field public static final network_text_left_margin:I = 0x7f07014c

.field public static final network_text_top_margin:I = 0x7f07014d

.field public static final password_checkbox_top_margin:I = 0x7f07001c

.field public static final pc_bar_top_margin:I = 0x7f07017f

.field public static final pc_cap_padding_bottom:I = 0x7f070181

.field public static final pc_cap_size:I = 0x7f070180

.field public static final pc_info_circle_layout_height:I = 0x7f070270

.field public static final percentage_text_size:I = 0x7f0701af

.field public static final photo_cell:I = 0x7f0700ef

.field public static final photo_cell_burst_margin_right:I = 0x7f0700f9

.field public static final photo_cell_burst_margin_top:I = 0x7f0700f8

.field public static final photo_cell_burst_size:I = 0x7f0700f7

.field public static final photo_cell_checkbox_margin_right:I = 0x7f0700f6

.field public static final photo_cell_checkbox_margin_top:I = 0x7f0700f5

.field public static final photo_cell_checkbox_size:I = 0x7f0700f4

.field public static final photo_cell_layout_margin:I = 0x7f0700f2

.field public static final photo_cell_new_icon_size:I = 0x7f0700d0

.field public static final photo_cell_softkey:I = 0x7f0700f3

.field public static final photo_cell_thumb_sz:I = 0x7f0700ee

.field public static final photo_frag_paddingLeft:I = 0x7f0700f0

.field public static final photo_frag_paddingRight:I = 0x7f0700f1

.field public static final photo_frag_softkey_paddingLeft:I = 0x7f07006c

.field public static final photo_frag_softkey_paddingRight:I = 0x7f07006d

.field public static final photo_hover_bottom_margin:I = 0x7f070195

.field public static final photo_hover_side_margins:I = 0x7f070194

.field public static final photo_hover_top_margin:I = 0x7f070193

.field public static final photo_list_fragment_top_margin:I = 0x7f07022e

.field public static final platform_download_banner_layout_height:I = 0x7f070025

.field public static final platform_download_banner_text:I = 0x7f070028

.field public static final platform_download_banner_text_drawable_padding:I = 0x7f070029

.field public static final platform_download_banner_text_height:I = 0x7f070026

.field public static final platform_download_banner_text_margin:I = 0x7f070027

.field public static final platform_popup_cancel_button_margin_left:I = 0x7f07002b

.field public static final platform_popup_cancel_button_margin_right:I = 0x7f07002c

.field public static final platform_popup_cancel_button_margin_top:I = 0x7f07002d

.field public static final platform_popup_cancel_button_size:I = 0x7f07002a

.field public static final platform_popup_download_button_margin_left:I = 0x7f07002f

.field public static final platform_popup_download_button_size:I = 0x7f07002e

.field public static final play_icon_left_margin:I = 0x7f070188

.field public static final play_icon_size:I = 0x7f070187

.field public static final player_bottom_controls_layout_height:I = 0x7f0700aa

.field public static final player_bottom_controls_layout_hover_padding:I = 0x7f07024d

.field public static final player_bottom_controls_layout_hover_size:I = 0x7f0700ab

.field public static final player_changedsp_button_height:I = 0x7f0700a5

.field public static final player_changedsp_button_width:I = 0x7f0700a4

.field public static final player_changedsp_left_margin:I = 0x7f070245

.field public static final player_controller_side_margin:I = 0x7f07024a

.field public static final player_music_volume_height:I = 0x7f070122

.field public static final player_music_volume_height_land:I = 0x7f070123

.field public static final player_music_volume_width:I = 0x7f070121

.field public static final player_play_pause_button_size:I = 0x7f07024c

.field public static final player_rew_ff_button_size:I = 0x7f07024b

.field public static final player_seek_bar_layout_height:I = 0x7f070254

.field public static final player_seek_bar_max_height:I = 0x7f070255

.field public static final player_seek_bar_padding:I = 0x7f070257

.field public static final player_seek_bar_thumb_offset:I = 0x7f070256

.field public static final player_share_button_height:I = 0x7f0700a9

.field public static final player_share_button_width:I = 0x7f0700a8

.field public static final player_share_right_margin:I = 0x7f070247

.field public static final player_subtext_size:I = 0x7f070248

.field public static final player_time_text_margin_edge:I = 0x7f070253

.field public static final player_time_text_margin_top:I = 0x7f070252

.field public static final player_time_text_size:I = 0x7f070251

.field public static final player_title_text_size:I = 0x7f070249

.field public static final player_top_controller_height:I = 0x7f0700a3

.field public static final player_top_header_full_height:I = 0x7f070127

.field public static final player_top_header_small_height:I = 0x7f070128

.field public static final player_video_volume_height:I = 0x7f070125

.field public static final player_video_volume_height_land:I = 0x7f070126

.field public static final player_video_volume_width:I = 0x7f070124

.field public static final player_volume_button_height:I = 0x7f0700a7

.field public static final player_volume_button_width:I = 0x7f0700a6

.field public static final player_volume_controller_height:I = 0x7f0700ad

.field public static final player_volume_controller_text_size:I = 0x7f07024f

.field public static final player_volume_controller_top_margin:I = 0x7f0700ae

.field public static final player_volume_controller_width:I = 0x7f0700ac

.field public static final player_volume_right_margin:I = 0x7f070246

.field public static final player_volume_slider_max_height:I = 0x7f070250

.field public static final player_volume_top_margin:I = 0x7f070129

.field public static final player_volume_top_margin_land:I = 0x7f07012a

.field public static final progress_bar_height:I = 0x7f0701b0

.field public static final quick_panel_left_margin:I = 0x7f0701ab

.field public static final quick_panel_right_margin:I = 0x7f0701ac

.field public static final quick_panel_top_margin:I = 0x7f0701aa

.field public static final recent_cell_large:I = 0x7f0700b8

.field public static final recent_cell_layout_margin:I = 0x7f0700bb

.field public static final recent_cell_small:I = 0x7f07005f

.field public static final recent_cell_softkey_large:I = 0x7f0700bc

.field public static final recent_cell_softkey_small:I = 0x7f0700bd

.field public static final recent_cell_thumb_sz:I = 0x7f0700b7

.field public static final recent_empty_margin:I = 0x7f0700c0

.field public static final recent_frag_paddingLeft:I = 0x7f0700b9

.field public static final recent_frag_paddingRight:I = 0x7f0700ba

.field public static final recent_frag_softkey_paddingLeft:I = 0x7f0700be

.field public static final recent_frag_softkey_paddingRight:I = 0x7f0700bf

.field public static final register_pc_help_image_height:I = 0x7f0701c2

.field public static final register_storage_bottom_margin:I = 0x7f0701e4

.field public static final register_storage_btn_height:I = 0x7f070007

.field public static final register_storage_btn_width:I = 0x7f070006

.field public static final rename_dialog_edit_text_margin:I = 0x7f070151

.field public static final save_to_margin:I = 0x7f07015a

.field public static final saveto_device_image_height:I = 0x7f070259

.field public static final saveto_device_image_width:I = 0x7f07025a

.field public static final saveto_divider_height:I = 0x7f07025b

.field public static final saveto_divider_margin:I = 0x7f07025c

.field public static final search_actionbar_height:I = 0x7f070083

.field public static final search_cell_top_margin:I = 0x7f070205

.field public static final search_child_large_text_size:I = 0x7f070208

.field public static final search_child_normal_text_size:I = 0x7f070209

.field public static final search_default_filter_manual_text_padding:I = 0x7f07021f

.field public static final search_default_filter_manual_text_size:I = 0x7f07021e

.field public static final search_default_filter_text_size:I = 0x7f07021d

.field public static final search_default_img_layout_height:I = 0x7f07021b

.field public static final search_default_img_layout_padding_top:I = 0x7f07021c

.field public static final search_filter_btn_size:I = 0x7f070202

.field public static final search_filter_item_height:I = 0x7f070216

.field public static final search_filter_item_img_size:I = 0x7f070219

.field public static final search_filter_item_left_padding:I = 0x7f070217

.field public static final search_filter_item_right_padding:I = 0x7f070218

.field public static final search_filter_location_height:I = 0x7f070222

.field public static final search_filter_text_left_margin:I = 0x7f070223

.field public static final search_filter_text_padding:I = 0x7f070224

.field public static final search_filter_text_size:I = 0x7f07021a

.field public static final search_group_height:I = 0x7f070206

.field public static final search_layout_padding_left:I = 0x7f070200

.field public static final search_layout_padding_right:I = 0x7f070201

.field public static final search_left_margin:I = 0x7f0701a4

.field public static final search_list_emptyview_padding_top:I = 0x7f070220

.field public static final search_list_emptyview_text_size:I = 0x7f070221

.field public static final search_list_item_height:I = 0x7f0701a2

.field public static final search_name_width:I = 0x7f07027d

.field public static final search_no_result_width:I = 0x7f070084

.field public static final search_padding_between_group:I = 0x7f070207

.field public static final search_photo_cell_height:I = 0x7f070086

.field public static final search_photo_cell_thumb_height:I = 0x7f070088

.field public static final search_photo_cell_thumb_width:I = 0x7f070087

.field public static final search_photo_cell_width:I = 0x7f070085

.field public static final search_photo_padding_between_cell:I = 0x7f07020f

.field public static final search_recent_edit_list_item_divider_margin:I = 0x7f070228

.field public static final search_recent_edit_list_item_height:I = 0x7f070226

.field public static final search_recent_edit_list_item_padding:I = 0x7f070227

.field public static final search_recent_list_edit_text1_size:I = 0x7f070214

.field public static final search_recent_list_edit_text2_size:I = 0x7f070215

.field public static final search_recent_list_item_height:I = 0x7f070210

.field public static final search_recent_list_text1_size:I = 0x7f070212

.field public static final search_recent_list_text2_size:I = 0x7f070213

.field public static final search_recent_list_text_margin:I = 0x7f070211

.field public static final search_right_margin:I = 0x7f0701a5

.field public static final search_spinner_dropdown_item_min_width:I = 0x7f07020b

.field public static final search_spinner_min_width:I = 0x7f07020a

.field public static final search_subtext1_width:I = 0x7f07027e

.field public static final search_subtext2_width:I = 0x7f07027f

.field public static final search_thumbnail_size:I = 0x7f0701a3

.field public static final search_top_margin:I = 0x7f070204

.field public static final search_video_cell_height:I = 0x7f07008a

.field public static final search_video_cell_thumb_height:I = 0x7f07008c

.field public static final search_video_cell_thumb_width:I = 0x7f07008b

.field public static final search_video_cell_width:I = 0x7f070089

.field public static final search_video_padding_between_cell:I = 0x7f07020e

.field public static final search_view_cursor_padding_left:I = 0x7f070225

.field public static final search_view_left_margin:I = 0x7f070203

.field public static final section_header_height:I = 0x7f070262

.field public static final section_header_padding:I = 0x7f070263

.field public static final section_header_top_margin:I = 0x7f070261

.field public static final select_all_back_button_margin_left:I = 0x7f0700cd

.field public static final select_all_back_button_margin_right:I = 0x7f0700ce

.field public static final select_all_back_button_width:I = 0x7f0700cc

.field public static final select_all_cell_font:I = 0x7f0700c9

.field public static final select_all_cell_height:I = 0x7f0700c7

.field public static final select_all_cell_text_margin_left:I = 0x7f0700ca

.field public static final select_all_cell_text_margin_left_photo:I = 0x7f0700cb

.field public static final select_all_spinner_dropdown_min_width:I = 0x7f07022a

.field public static final select_all_spinner_min_width:I = 0x7f070229

.field public static final select_all_sub_height:I = 0x7f0700e3

.field public static final separator_font:I = 0x7f0700c5

.field public static final separator_height:I = 0x7f0700c4

.field public static final separator_side_margin:I = 0x7f0700c6

.field public static final setting_storage_signin_button_height:I = 0x7f0700b0

.field public static final setting_storage_signin_button_width:I = 0x7f0700af

.field public static final settings_saveto_buttons_height:I = 0x7f070080

.field public static final settings_split_left_pane:I = 0x7f070258

.field public static final settings_storage_button_top_margin_land:I = 0x7f070159

.field public static final settings_storage_image_height:I = 0x7f070158

.field public static final settings_storage_image_spacing:I = 0x7f07007f

.field public static final settings_switch_right_margin:I = 0x7f07011b

.field public static final settings_text_size:I = 0x7f07014f

.field public static final settings_top_margin:I = 0x7f070150

.field public static final signin_button_bottom_margin:I = 0x7f070185

.field public static final small_circle_view_height:I = 0x7f070265

.field public static final small_circle_view_padding_top:I = 0x7f070267

.field public static final song_text_left_margin:I = 0x7f0701be

.field public static final song_text_right_margin:I = 0x7f0701bd

.field public static final song_text_size:I = 0x7f0701bf

.field public static final spinner_bottom_margin:I = 0x7f07020d

.field public static final spinner_top_margin:I = 0x7f07020c

.field public static final split_view_width:I = 0x7f070176

.field public static final storage_auto_upload_text_size:I = 0x7f07017e

.field public static final storage_autoupload_image_height:I = 0x7f070143

.field public static final storage_autoupload_image_marginLeft:I = 0x7f070144

.field public static final storage_autoupload_image_marginTop:I = 0x7f070145

.field public static final storage_autoupload_image_width:I = 0x7f070142

.field public static final storage_bottom_margin:I = 0x7f070157

.field public static final storage_capacity_text_size:I = 0x7f070137

.field public static final storage_capacity_text_size_single:I = 0x7f070138

.field public static final storage_height:I = 0x7f070134

.field public static final storage_height_softkey:I = 0x7f070135

.field public static final storage_image_height:I = 0x7f070147

.field public static final storage_image_marginTop:I = 0x7f070148

.field public static final storage_image_width:I = 0x7f070146

.field public static final storage_info_auto_upload_text:I = 0x7f070271

.field public static final storage_info_usage_text_size:I = 0x7f07017d

.field public static final storage_label_text_size:I = 0x7f07000e

.field public static final storage_margin:I = 0x7f070156

.field public static final storage_name_text_marginLeft:I = 0x7f070139

.field public static final storage_name_text_marginRight:I = 0x7f0701fe

.field public static final storage_name_text_size:I = 0x7f070136

.field public static final storage_path_label_paddingleft:I = 0x7f0700b3

.field public static final storage_path_label_size:I = 0x7f07017b

.field public static final storage_path_text_size:I = 0x7f07017c

.field public static final storage_section_height:I = 0x7f07013e

.field public static final storage_section_height_softkey:I = 0x7f070140

.field public static final storage_signin_logo_height:I = 0x7f07000b

.field public static final storage_signin_logo_margin_bottom:I = 0x7f07000d

.field public static final storage_signin_logo_width:I = 0x7f07000c

.field public static final storage_signin_side_margin:I = 0x7f07000a

.field public static final storage_signin_signup_link_padding_bottom:I = 0x7f070010

.field public static final storage_signin_signup_link_padding_top:I = 0x7f07000f

.field public static final storage_signin_top_margin:I = 0x7f070009

.field public static final storage_text_device_name_limit_land:I = 0x7f07011f

.field public static final storage_text_device_name_limit_land_softkey:I = 0x7f070120

.field public static final storage_text_three_device:I = 0x7f07011e

.field public static final storage_text_two_device:I = 0x7f07011d

.field public static final storage_width:I = 0x7f070133

.field public static final sugarsync_button_height:I = 0x7f070278

.field public static final sugarsync_description_text_bottom_margin:I = 0x7f070277

.field public static final sugarsync_description_text_size:I = 0x7f070276

.field public static final sugarsync_icon_right_margin:I = 0x7f070274

.field public static final sugarsync_icon_size:I = 0x7f070273

.field public static final sugarsync_text_size:I = 0x7f070275

.field public static final sugarsync_title_layout_height:I = 0x7f070272

.field public static final tablet_music_listview_2depth_name_width:I = 0x7f070239

.field public static final tablet_music_listview_2depth_subname_width:I = 0x7f07023a

.field public static final tablet_music_listview_2depth_text_margin:I = 0x7f07023b

.field public static final tablet_music_listview_albumname_width:I = 0x7f070236

.field public static final tablet_music_listview_genre_2depth_name_width:I = 0x7f070237

.field public static final tablet_music_listview_genre_2depth_subname_width:I = 0x7f070238

.field public static final tablet_music_listview_name_width:I = 0x7f070234

.field public static final tablet_music_listview_subname_width:I = 0x7f070235

.field public static final tablet_music_listview_text_margin:I = 0x7f070233

.field public static final tabstrip_height:I = 0x7f0701d4

.field public static final tabstrip_padding_top:I = 0x7f0701d6

.field public static final tabstrip_text_size:I = 0x7f0701d5

.field public static final time_layout_height:I = 0x7f0701b2

.field public static final time_left_margin:I = 0x7f0701b4

.field public static final time_right_margin:I = 0x7f0701b3

.field public static final time_text_size:I = 0x7f0701b5

.field public static final title_bar_action_item_height:I = 0x7f0701d2

.field public static final title_bar_action_item_text_size:I = 0x7f0701d3

.field public static final title_bar_action_item_width:I = 0x7f0701d1

.field public static final title_bar_height:I = 0x7f070092

.field public static final title_bar_tab_height:I = 0x7f070013

.field public static final title_bar_tab_height_infopage:I = 0x7f07028d

.field public static final title_layout_height:I = 0x7f0701ad

.field public static final title_tab_separator_thickness:I = 0x7f0701cf

.field public static final title_tab_strip_full_underline_height:I = 0x7f0701cb

.field public static final title_tab_strip_height:I = 0x7f0701c9

.field public static final title_tab_strip_selected_underline_height:I = 0x7f0701ca

.field public static final title_tab_strip_text_margin:I = 0x7f07028b

.field public static final title_tab_strip_text_size:I = 0x7f07028c

.field public static final title_tab_strip_title_offset:I = 0x7f0701cd

.field public static final title_tab_strip_title_padding:I = 0x7f0701cc

.field public static final title_tab_strip_vertical_separator:I = 0x7f0701ce

.field public static final title_tab_view_fadeout:I = 0x7f0701d0

.field public static final title_text_size:I = 0x7f0701ae

.field public static final toast_bottom_offset:I = 0x7f070076

.field public static final transfer_arrow_margin:I = 0x7f07003e

.field public static final transfer_arrow_width:I = 0x7f07003d

.field public static final transfer_button_size:I = 0x7f07003f

.field public static final transfer_empty_text_drawable_padding:I = 0x7f070022

.field public static final transfer_empty_text_size:I = 0x7f070024

.field public static final transfer_header_title_text_size:I = 0x7f070032

.field public static final transfer_icon_size:I = 0x7f070036

.field public static final transfer_item_title_text_size:I = 0x7f070023

.field public static final transfer_layout_margin_bottom:I = 0x7f07003a

.field public static final transfer_layout_margin_left:I = 0x7f070037

.field public static final transfer_layout_margin_right:I = 0x7f070038

.field public static final transfer_layout_margin_top:I = 0x7f070039

.field public static final transfer_layout_text_margin_bottom:I = 0x7f07003b

.field public static final transfer_progress_height:I = 0x7f07003c

.field public static final transfer_removebutton_height:I = 0x7f070034

.field public static final transfer_removebutton_text_size:I = 0x7f070035

.field public static final transfer_removebutton_width:I = 0x7f070033

.field public static final transfer_text_size:I = 0x7f0701b1

.field public static final transfer_tooltip_arrow_height:I = 0x7f070043

.field public static final transfer_tooltip_arrow_margin_left:I = 0x7f070045

.field public static final transfer_tooltip_arrow_width:I = 0x7f070042

.field public static final transfer_tooltip_bg_margin_top:I = 0x7f070044

.field public static final transfer_tooltip_cancel_button_margin_right:I = 0x7f07004c

.field public static final transfer_tooltip_cancel_button_margin_top:I = 0x7f07004d

.field public static final transfer_tooltip_cancel_button_size:I = 0x7f07004b

.field public static final transfer_tooltip_layout_height:I = 0x7f070041

.field public static final transfer_tooltip_layout_margin_left:I = 0x7f070046

.field public static final transfer_tooltip_layout_width:I = 0x7f070040

.field public static final transfer_tooltip_text_margin_bottom:I = 0x7f070049

.field public static final transfer_tooltip_text_margin_left:I = 0x7f070047

.field public static final transfer_tooltip_text_margin_top:I = 0x7f070048

.field public static final transfer_tooltip_text_size:I = 0x7f07004a

.field public static final tutorial_arrow_margin_right:I = 0x7f0700b4

.field public static final tutorial_arrow_margin_top:I = 0x7f070296

.field public static final tutorial_body_top_margin:I = 0x7f07010f

.field public static final tutorial_button_bottom_margin:I = 0x7f070110

.field public static final tutorial_button_height:I = 0x7f070293

.field public static final tutorial_button_text_size:I = 0x7f070295

.field public static final tutorial_button_top_margin:I = 0x7f070292

.field public static final tutorial_button_width:I = 0x7f070294

.field public static final tutorial_checkbox_height:I = 0x7f07010a

.field public static final tutorial_checkbox_margin:I = 0x7f07010b

.field public static final tutorial_checkbox_padding_left:I = 0x7f07010c

.field public static final tutorial_close_button_left_margin:I = 0x7f07028f

.field public static final tutorial_close_button_size:I = 0x7f07009c

.field public static final tutorial_close_button_top_margin:I = 0x7f07028e

.field public static final tutorial_contents_more_text_margin_top:I = 0x7f07009d

.field public static final tutorial_header_logo_height:I = 0x7f07010d

.field public static final tutorial_header_title_size:I = 0x7f07010e

.field public static final tutorial_height:I = 0x7f070104

.field public static final tutorial_info_text_margin_left:I = 0x7f07009b

.field public static final tutorial_info_text_margin_top:I = 0x7f07009a

.field public static final tutorial_info_text_width:I = 0x7f070099

.field public static final tutorial_inner_width:I = 0x7f070107

.field public static final tutorial_margin_bottom:I = 0x7f070106

.field public static final tutorial_margin_top:I = 0x7f070105

.field public static final tutorial_select_close_btn_height:I = 0x7f0701ee

.field public static final tutorial_select_close_btn_margin_bottom:I = 0x7f0701ef

.field public static final tutorial_select_close_btn_margin_bottom_land:I = 0x7f0701f7

.field public static final tutorial_select_close_btn_margin_right:I = 0x7f0701f0

.field public static final tutorial_select_close_btn_margin_right_land:I = 0x7f0701f8

.field public static final tutorial_select_close_btn_width:I = 0x7f0701ed

.field public static final tutorial_select_icon_height:I = 0x7f0701f2

.field public static final tutorial_select_icon_height_land:I = 0x7f0701fa

.field public static final tutorial_select_icon_width:I = 0x7f0701f1

.field public static final tutorial_select_icon_width_land:I = 0x7f0701f9

.field public static final tutorial_select_info_text_height:I = 0x7f0701f4

.field public static final tutorial_select_info_text_height_land:I = 0x7f0701fc

.field public static final tutorial_select_info_text_margin_left:I = 0x7f0701f5

.field public static final tutorial_select_info_text_margin_left_land:I = 0x7f0701fd

.field public static final tutorial_select_info_text_width:I = 0x7f0701f3

.field public static final tutorial_select_info_text_width_land:I = 0x7f0701fb

.field public static final tutorial_select_text_size:I = 0x7f0701f6

.field public static final tutorial_text_size:I = 0x7f070291

.field public static final tutorial_text_top_margin:I = 0x7f070290

.field public static final tutorial_text_width:I = 0x7f070098

.field public static final tutorial_topbar_text_margin_left:I = 0x7f070109

.field public static final tutorial_topbar_text_margin_top:I = 0x7f070108

.field public static final tutorial_width:I = 0x7f070103

.field public static final tw_dialog_button_bottom_margin:I = 0x7f07019c

.field public static final tw_dialog_button_height:I = 0x7f07019a

.field public static final tw_dialog_button_left_margin:I = 0x7f07019d

.field public static final tw_dialog_button_multi_gutter_margin:I = 0x7f070012

.field public static final tw_dialog_button_outer_height:I = 0x7f070011

.field public static final tw_dialog_button_right_margin:I = 0x7f07019e

.field public static final tw_dialog_button_text_size:I = 0x7f07019f

.field public static final tw_dialog_button_top_margin:I = 0x7f07019b

.field public static final tw_list_button_height:I = 0x7f0701a0

.field public static final tw_list_button_text_size:I = 0x7f0701a1

.field public static final type_button_divider_height:I = 0x7f070168

.field public static final type_button_text_margin:I = 0x7f070166

.field public static final type_button_text_size:I = 0x7f070167

.field public static final ui_width:I = 0x7f07005e

.field public static final up_left_margin:I = 0x7f070020

.field public static final video_cell_height:I = 0x7f07007a

.field public static final video_cell_thumb_height:I = 0x7f070078

.field public static final video_cell_thumb_width:I = 0x7f070077

.field public static final video_cell_width:I = 0x7f070079

.field public static final video_frag_paddingLeft_tablet:I = 0x7f07007b

.field public static final video_frag_paddingRight_tablet:I = 0x7f07007c

.field public static final video_frag_paddingTop_tablet:I = 0x7f07007d

.field public static final video_frag_padding_between_tablet:I = 0x7f07007e

.field public static final video_frag_padding_newIcon_tablet:I = 0x7f0700ff

.field public static final video_opt_image_height:I = 0x7f070154

.field public static final video_opt_left_margin:I = 0x7f070152

.field public static final video_opt_right_margin:I = 0x7f070153

.field public static final video_opt_text_bottom_margin:I = 0x7f070155

.field public static final video_player_loading_text_size:I = 0x7f07012b

.field public static final viewpager_gutter_margin:I = 0x7f07011c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
