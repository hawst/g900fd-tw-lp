.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private albumId:J

.field private localData:Ljava/lang/String;

.field private localSourceAlbumId:J

.field private localSourceMediaId:J

.field private localThumbData:Ljava/lang/String;

.field private localThumbHeight:I

.field private localThumbWidth:I

.field private maxHeight:I

.field private maxWidth:I

.field private mediaHeight:I

.field private mediaType:I

.field private mediaWidth:I

.field private orientation:I

.field private priority:I

.field private rowId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 404
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 405
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 406
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 407
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 408
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 410
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 411
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 412
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 413
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V
    .locals 2

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 166
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 167
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 168
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 169
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 170
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 171
    iget-object v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 172
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 173
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 174
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 175
    iget-object v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbData:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbData:Ljava/lang/String;

    .line 176
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbWidth:I

    .line 177
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbHeight:I

    .line 178
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaWidth:I

    .line 179
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaHeight:I

    .line 180
    return-void
.end method

.method public static createFromCursor(Landroid/database/Cursor;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->createFromCursorInternal(Landroid/database/Cursor;IIIZ)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v0

    return-object v0
.end method

.method public static createFromCursorByFileBrowser(Landroid/database/Cursor;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->createFromCursorInternal(Landroid/database/Cursor;IIIZ)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v0

    return-object v0
.end method

.method private static createFromCursorInternal(Landroid/database/Cursor;IIIZ)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0xc

    .line 65
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>()V

    .line 66
    iput p1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 67
    iput p2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 68
    iput p3, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 70
    if-eqz p4, :cond_b

    .line 71
    const-string v1, "asp_media_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 78
    :goto_0
    const-string v1, "media_type"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 81
    iget v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 82
    const-string v1, "orientation"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 87
    :cond_0
    const-string v1, "local_source_media_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 88
    if-ltz v1, :cond_1

    .line 89
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 90
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 91
    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 95
    :cond_1
    const-string v1, "local_data"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 96
    if-ltz v1, :cond_2

    .line 97
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 100
    :cond_2
    const-string v1, "local_thumb_data"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 101
    if-ltz v1, :cond_3

    .line 102
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbData:Ljava/lang/String;

    .line 105
    :cond_3
    const-string v1, "width"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 106
    if-ltz v1, :cond_4

    .line 107
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaWidth:I

    .line 109
    :cond_4
    const-string v1, "height"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 110
    if-ltz v1, :cond_5

    .line 111
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaHeight:I

    .line 114
    :cond_5
    const-string v1, "local_thumb_width"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 115
    if-ltz v1, :cond_6

    .line 116
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbWidth:I

    .line 119
    :cond_6
    const-string v1, "local_thumb_height"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 120
    if-ltz v1, :cond_7

    .line 121
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbHeight:I

    .line 124
    :cond_7
    iget v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_8

    iget v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    if-eq v1, v4, :cond_8

    iget v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/16 v2, 0xd

    if-eq v1, v2, :cond_8

    iget v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/16 v2, 0xe

    if-ne v1, v2, :cond_a

    .line 129
    :cond_8
    iget v1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    if-eq v1, v4, :cond_9

    .line 130
    const-string v1, "album_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 134
    :cond_9
    const-string v1, "local_source_album_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 136
    if-ltz v1, :cond_a

    .line 137
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 138
    cmp-long v1, v2, v6

    if-lez v1, :cond_a

    .line 139
    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 144
    :cond_a
    return-object v0

    .line 74
    :cond_b
    const-string v1, "_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    goto/16 :goto_0
.end method

.method public static createFromRequest(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V

    .line 154
    iput p1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 155
    iput p2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 156
    iput p3, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 158
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    return v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 261
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    return-wide v0
.end method

.method public getLocalData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalSourceAlbumId()J
    .locals 2

    .prologue
    .line 311
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    return-wide v0
.end method

.method public getLocalSourceMediaId()J
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    return-wide v0
.end method

.method public getLocalThumbData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbData:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalThumbHeight()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbHeight:I

    return v0
.end method

.method public getLocalThumbWidth()I
    .locals 1

    .prologue
    .line 291
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localThumbWidth:I

    return v0
.end method

.method public getMaxHeight()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    return v0
.end method

.method public getMediaHeight()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaHeight:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    return v0
.end method

.method public getMediaWidth()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaWidth:I

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    return v0
.end method

.method public getRowId()J
    .locals 2

    .prologue
    .line 218
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SlinkImageRequest [rowId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", priority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 390
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 391
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 392
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 393
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 394
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 395
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 397
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 398
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 399
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 400
    return-void
.end method
