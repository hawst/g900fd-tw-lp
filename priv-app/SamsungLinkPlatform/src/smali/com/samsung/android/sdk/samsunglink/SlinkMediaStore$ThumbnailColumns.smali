.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$BaseSamsungLinkColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ThumbnailColumns"
.end annotation


# static fields
.field public static final FULL_HEIGHT:Ljava/lang/String; = "full_height"

.field public static final FULL_WIDTH:Ljava/lang/String; = "full_width"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ORIENTATION:Ljava/lang/String; = "orientation"

.field public static final QUERY_STR_CACHE_ONLY:Ljava/lang/String; = "cache_only"

.field public static final QUERY_STR_CANCEL:Ljava/lang/String; = "cancel"

.field public static final QUERY_STR_GROUP_ID:Ljava/lang/String; = "group_id"

.field public static final QUERY_STR_HEIGHT:Ljava/lang/String; = "height"

.field public static final QUERY_STR_SKIP_CACHE_GET:Ljava/lang/String; = "skip_cache_get"

.field public static final QUERY_STR_SKIP_CACHE_PUT:Ljava/lang/String; = "skip_cache_put"

.field public static final QUERY_STR_WIDTH:Ljava/lang/String; = "width"

.field public static final SIZE:Ljava/lang/String; = "_size"

.field public static final THUMB_HEIGHT:Ljava/lang/String; = "thumb_height"

.field public static final THUMB_WIDTH:Ljava/lang/String; = "thumb_width"

.field public static final WIDTH:Ljava/lang/String; = "width"
