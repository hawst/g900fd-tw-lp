.class public Lcom/samsung/android/sdk/samsung/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsung/a/a$a;,
        Lcom/samsung/android/sdk/samsung/a/a$b;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/samsung/android/sdk/samsung/a/a;

.field private static final g:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;

.field private final e:Ljava/util/concurrent/locks/Condition;

.field private f:Ljava/lang/Thread;

.field private h:Lcom/mfluent/asp/datamodel/Device;

.field private i:Z

.field private j:Lcom/samsung/android/sdk/samsung/a/a$b;

.field private k:Lcom/samsung/android/hostmanager/aidl/d;

.field private l:Lcom/samsung/android/hostmanager/aidl/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/samsung/a/a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    .line 88
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lcom/samsung/android/sdk/samsung/a/a;->g:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 86
    iput-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->f:Ljava/lang/Thread;

    .line 103
    iput-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->h:Lcom/mfluent/asp/datamodel/Device;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->i:Z

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->d:Ljava/util/concurrent/locks/Condition;

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->e:Ljava/util/concurrent/locks/Condition;

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/a/a;)Lcom/samsung/android/hostmanager/aidl/b;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->l:Lcom/samsung/android/hostmanager/aidl/b;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/samsung/android/hostmanager/aidl/b;)Lcom/samsung/android/hostmanager/aidl/b;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/a/a;->l:Lcom/samsung/android/hostmanager/aidl/b;

    return-object p1
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/samsung/android/hostmanager/aidl/d;)Lcom/samsung/android/hostmanager/aidl/d;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a/a;
    .locals 2

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsung/a/a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    if-nez v0, :cond_1

    .line 65
    const-class v1, Lcom/samsung/android/sdk/samsung/a/a;

    monitor-enter v1

    .line 66
    :try_start_0
    new-instance v0, Lcom/samsung/android/sdk/samsung/a/a;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/a/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsung/a/a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    .line 67
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsung/a/a;->a:Lcom/samsung/android/sdk/samsung/a/a;

    return-object v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 4

    .prologue
    .line 524
    .line 526
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->P()V

    .line 528
    new-instance v0, Lcom/samsung/android/sdk/samsung/a/a$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/a/a$1;-><init>(Lcom/samsung/android/sdk/samsung/a/a;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/samsung/a/a$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 560
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;)V
    .locals 8

    .prologue
    .line 51
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::updateWearableDeviceByStatusInfo is called."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    const/4 v1, 0x0

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v3, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->g(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->h()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    :goto_2
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->i()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->e(I)V

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableStatusInfo;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v1, v4, v5}, Lcom/mfluent/asp/datamodel/Device;->setUsedCapacityInBytes(J)V

    invoke-virtual {v1, v6, v7}, Lcom/mfluent/asp/datamodel/Device;->setCapacityInBytes(J)V

    new-instance v4, Lcom/samsung/android/sdk/samsung/a/a$5;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/samsung/a/a$5;-><init>(Lcom/samsung/android/sdk/samsung/a/a;)V

    sget-object v5, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/samsung/a/a$5;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/android/sdk/samsung/a/a$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/a/a$6;-><init>(Lcom/samsung/android/sdk/samsung/a/a;)V

    sget-object v4, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/sdk/samsung/a/a$6;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    if-eqz v2, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/samsung/a/a$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsung/a/a$7;-><init>(Lcom/samsung/android/sdk/samsung/a/a;)V

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/samsung/a/a$7;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_3
    iput-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->h:Lcom/mfluent/asp/datamodel/Device;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_3
    return-void

    :sswitch_0
    :try_start_1
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->BLUETOOTH:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_3

    :sswitch_1
    :try_start_3
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signal()V

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :sswitch_2
    :try_start_4
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    :cond_4
    move-object v0, v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;)Z
    .locals 8

    .prologue
    .line 441
    new-instance v1, Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    .line 445
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->c()Ljava/lang/String;

    move-result-object v2

    .line 446
    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->l(Ljava/lang/String;)V

    .line 449
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->g(Ljava/lang/String;)V

    .line 451
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 452
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    .line 458
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 475
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 479
    :goto_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    .line 482
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 483
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v5, v6, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 487
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v3, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 489
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_1

    .line 461
    :pswitch_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_0

    .line 466
    :pswitch_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_0

    .line 471
    :pswitch_2
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_0

    .line 493
    :cond_1
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    .line 497
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 505
    const/4 v0, 0x0

    .line 519
    :goto_2
    return v0

    .line 508
    :cond_2
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->m(Ljava/lang/String;)V

    .line 511
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v0

    .line 513
    if-eqz v0, :cond_3

    .line 514
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::addWearableDeviceInDB(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : added in DB!!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 516
    :cond_3
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::addWearableDeviceInDB(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : NOT added in DB!!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 458
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->b(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/samsung/a/a;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/samsung/android/sdk/samsung/a/a;)Lcom/samsung/android/hostmanager/aidl/d;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1022
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 1024
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 1025
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    :try_start_0
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::sendWearableDeviceNetworkStatusRequest : to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1033
    const-string v4, "uniqueId"

    invoke-virtual {v1, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1034
    const-string v4, "networkMode"

    invoke-virtual {v1, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1035
    const-string v4, "hostPeerId"

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1037
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    const-string v4, "/api/pCloud/device/networkModeChanged"

    invoke-static {v0, v4, v1}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1039
    const-string v4, "mfl_WearableDeviceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::sendWearableDeviceNetworkStatusRequest: Got json response: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1041
    :catch_0
    move-exception v1

    .line 1042
    const-string v4, "mfl_WearableDeviceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::Trouble in sending device/networkModeChanged/ to "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1046
    :cond_1
    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 230
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 233
    :try_start_0
    const-string v2, "com.samsung.android.hostmanager"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    .line 234
    if-eqz v2, :cond_0

    .line 235
    const-string v2, "com.samsung.android.hostmanager"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 236
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const v2, 0x7e02271f

    if-lt v1, v2, :cond_0

    .line 238
    const/4 v0, 0x1

    .line 249
    :cond_0
    :goto_0
    return v0

    .line 248
    :catch_0
    move-exception v1

    const-string v1, "mfl_WearableDeviceManager"

    const-string v2, "::hasHostManager Package com.samsung.android.hostmanager not found."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 606
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/samsung/a/a$2;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/sdk/samsung/a/a$2;-><init>(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)V

    sget-object v3, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/samsung/a/a$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 637
    :goto_0
    return v0

    .line 635
    :catch_0
    move-exception v0

    .line 636
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::disableWearableDeviceInUP : Trouble saving settings to server because "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 637
    goto :goto_0
.end method

.method private b(Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;)Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 691
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    .line 693
    const/4 v0, 0x0

    .line 695
    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 696
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 699
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v4, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 701
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    .line 703
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 709
    invoke-virtual {p1}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->b()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 726
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 730
    :goto_1
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v6

    .line 732
    if-eqz v6, :cond_0

    .line 733
    const-string v6, "mfl_WearableDeviceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::updateWearableDeviceInDB(): "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " : Updated in DB!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    if-nez v1, :cond_1

    .line 737
    new-instance v1, Lcom/samsung/android/sdk/samsung/a/a$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/samsung/a/a$4;-><init>(Lcom/samsung/android/sdk/samsung/a/a;)V

    sget-object v6, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v7, v3, [Lcom/mfluent/asp/datamodel/Device;

    aput-object v0, v7, v2

    invoke-virtual {v1, v6, v7}, Lcom/samsung/android/sdk/samsung/a/a$4;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-object v1, v0

    goto/16 :goto_0

    .line 712
    :pswitch_0
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_1

    .line 717
    :pswitch_1
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_1

    .line 722
    :pswitch_2
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_1

    .line 769
    :cond_0
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::updateWearableDeviceInDB(): "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " : NOT Updated in DB!!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v1, v0

    .line 771
    goto/16 :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v6, v7, :cond_3

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 774
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v4, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 777
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/mfluent/asp/datamodel/Device;)V

    :cond_3
    move-object v0, v1

    move-object v1, v0

    .line 779
    goto/16 :goto_0

    .line 781
    :cond_4
    if-nez v1, :cond_5

    .line 782
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::updateWearableDeviceInDB(): target is not in DB!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 785
    :goto_2
    return v0

    :cond_5
    move v0, v3

    goto :goto_2

    .line 709
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->d(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 164
    .line 166
    monitor-enter p0

    .line 167
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/samsung/a/a;->i:Z

    if-eqz v2, :cond_6

    move v2, v0

    .line 170
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    if-eqz v2, :cond_2

    .line 174
    monitor-enter p0

    .line 175
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    if-nez v2, :cond_0

    .line 176
    const-string v0, "mfl_WearableDeviceManager"

    const-string v2, "::Bind: Alreday bound, but mIUHostManager is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v0, v1

    .line 223
    :goto_1
    return v0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 179
    :cond_0
    monitor-exit p0

    .line 181
    const-string v1, "WearableDeviceManager.getWearableList"

    invoke-static {p1, v1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsung/a/a;->c()Z

    goto :goto_1

    .line 179
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 186
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->d(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 192
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsung/a/a;->b()Z

    move-result v0

    .line 194
    if-nez v0, :cond_5

    .line 196
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 198
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v5, v6, :cond_3

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v2, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/mfluent/asp/datamodel/Device;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 202
    :catch_0
    move-exception v0

    .line 203
    const-string v2, "mfl_WearableDeviceManager"

    const-string v3, "::bind:Trouble in checking HostManager installation: "

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 204
    goto :goto_1

    :cond_4
    move v0, v1

    .line 200
    goto :goto_1

    .line 207
    :cond_5
    const-string v0, "mfl_WearableDeviceManager"

    const-string v2, "::Bind: IUHostManager service"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.hostmanager.service.IUHostManager"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    new-instance v2, Lcom/samsung/android/sdk/samsung/a/a$b;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/sdk/samsung/a/a$b;-><init>(Lcom/samsung/android/sdk/samsung/a/a;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a;->j:Lcom/samsung/android/sdk/samsung/a/a$b;

    .line 220
    :try_start_3
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/samsung/a/a;->j:Lcom/samsung/android/sdk/samsung/a/a$b;

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v0

    goto/16 :goto_1

    .line 221
    :catch_1
    move-exception v0

    .line 222
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::bind Exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 223
    goto/16 :goto_1

    :cond_6
    move v2, v1

    goto/16 :goto_0
.end method

.method private c()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 356
    monitor-enter p0

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    if-nez v0, :cond_0

    .line 358
    const-string v0, "mfl_WearableDeviceManager"

    const-string v2, "::getWearableList: mIUHostManager is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 415
    :goto_0
    return v0

    .line 361
    :cond_0
    monitor-exit p0

    .line 364
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    invoke-interface {v0}, Lcom/samsung/android/hostmanager/aidl/d;->y()Ljava/util/List;

    move-result-object v0

    .line 368
    if-nez v0, :cond_1

    .line 369
    const-string v0, "mfl_WearableDeviceManager"

    const-string v2, "::getWearableList(): List is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v1

    .line 370
    goto :goto_0

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 373
    :cond_1
    :try_start_2
    const-string v3, "mfl_WearableDeviceManager"

    const-string v4, "::getWearableList() is success"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;

    .line 376
    const-string v4, "mfl_WearableDeviceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getWearableList(): device.getBtName() = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v4, "mfl_WearableDeviceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getWearableList(): device.getDeviceId() = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string v4, "mfl_WearableDeviceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getWearableList(): device.getPackageName() = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v4, "mfl_WearableDeviceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getWearableList(): device.getConnectStatus() = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->b()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->b()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 382
    const-string v4, "mfl_WearableDeviceManager"

    const-string v5, "::getWearableList(): device.getConnectStatus() = BT connect"

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_2
    :goto_2
    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->e()Z

    move-result v4

    .line 393
    if-nez v4, :cond_5

    .line 394
    const-string v0, "mfl_WearableDeviceManager"

    const-string v2, "::getWearableList(): device.isSupportWearableStatus() = false (not support)"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 395
    goto/16 :goto_0

    .line 383
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->b()I

    move-result v4

    if-ne v4, v2, :cond_4

    .line 384
    const-string v4, "mfl_WearableDeviceManager"

    const-string v5, "::getWearableList(): device.getConnectStatus() = BT disconnect"

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 412
    :catch_0
    move-exception v0

    .line 413
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getWearableList: error is occurred : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 415
    goto/16 :goto_0

    .line 385
    :cond_4
    :try_start_3
    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->b()I

    move-result v4

    if-nez v4, :cond_2

    .line 386
    const-string v4, "mfl_WearableDeviceManager"

    const-string v5, "::getWearableList(): device.getConnectStatus() = BT unpaired"

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 398
    :cond_5
    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/a/a;->c(Ljava/lang/String;)Z

    move-result v4

    .line 400
    if-eqz v4, :cond_6

    .line 402
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;)Z

    .line 409
    :goto_3
    invoke-virtual {v0}, Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsung/a/a;->d(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 405
    :cond_6
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsung/a/a;->b(Lcom/samsung/android/hostmanager/aidl/WearableConnectInfo;)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    :cond_7
    move v0, v2

    .line 411
    goto/16 :goto_0
.end method

.method private c(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 644
    iget-object v2, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v2

    .line 646
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 647
    :cond_0
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::addWearableDeviceInUP("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "):: has not AuthInfo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 685
    :goto_0
    return v0

    .line 652
    :cond_1
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/samsung/a/a$3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/sdk/samsung/a/a$3;-><init>(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)V

    sget-object v3, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/samsung/a/a$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 683
    :catch_0
    move-exception v0

    .line 684
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::addWearableDeviceInUP : Trouble saving settings to server because "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 685
    goto :goto_0
.end method

.method static synthetic c(Lcom/samsung/android/sdk/samsung/a/a;)Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->e(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    return v0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 421
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 423
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 424
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 426
    :try_start_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 436
    :goto_0
    return v0

    .line 429
    :catch_0
    move-exception v0

    .line 430
    const-string v2, "mfl_WearableDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::checkNewWearableDevice(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 431
    goto :goto_0

    .line 436
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 4

    .prologue
    .line 791
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 792
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MAC:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 795
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    .line 796
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/b/i;->c(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 800
    :goto_0
    return v0

    .line 798
    :catch_0
    move-exception v0

    .line 799
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::updateWearableDeviceNameInUP : Trouble saving settings to server because "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 800
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/samsung/android/sdk/samsung/a/a;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsung/a/a;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/samsung/android/sdk/samsung/a/a;Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->c(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    return v0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 806
    monitor-enter p0

    .line 807
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    if-nez v1, :cond_0

    .line 808
    const-string v1, "mfl_WearableDeviceManager"

    const-string v2, "::getWearableInfoByDeviceId: mIUHostManager is null"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828
    :goto_0
    return v0

    .line 811
    :cond_0
    monitor-exit p0

    .line 815
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->h:Lcom/mfluent/asp/datamodel/Device;

    .line 817
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    invoke-interface {v1, p1}, Lcom/samsung/android/hostmanager/aidl/d;->B(Ljava/lang/String;)Z

    move-result v1

    .line 818
    if-eqz v1, :cond_1

    .line 819
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getWearableInfoByDeviceId("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is success"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 820
    const/4 v0, 0x1

    goto :goto_0

    .line 811
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 822
    :cond_1
    :try_start_2
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getWearableInfoByDeviceId("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 824
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method private e(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 4

    .prologue
    .line 1050
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1051
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MAC:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1054
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    .line 1055
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/b/i;->d(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1059
    :goto_0
    return v0

    .line 1057
    :catch_0
    move-exception v0

    .line 1058
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::updateWearableDeviceIdInUP : Trouble saving settings to server because "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1059
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/samsung/android/sdk/samsung/a/a;)Ljava/util/concurrent/locks/Condition;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->d:Ljava/util/concurrent/locks/Condition;

    return-object v0
.end method

.method static synthetic g(Lcom/samsung/android/sdk/samsung/a/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    .locals 4

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->f:Ljava/lang/Thread;

    if-nez v0, :cond_2

    .line 125
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->f:Ljava/lang/Thread;

    .line 127
    const-string v0, "mfl_WearableDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::getWearableDevice: Call bind: request: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 131
    if-eqz v0, :cond_0

    .line 133
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->d:Ljava/util/concurrent/locks/Condition;

    sget-wide v2, Lcom/samsung/android/sdk/samsung/a/a;->g:J

    invoke-interface {v0, v2, v3}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 134
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::getWearableDevice: Timed out (onWearableStatusInfo)"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->f:Ljava/lang/Thread;

    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 153
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->h:Lcom/mfluent/asp/datamodel/Device;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 159
    return-object v0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    :try_start_3
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getWearableDevice: mWearableDeviceInfoResultCondition interrupted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 145
    :cond_2
    :goto_2
    :try_start_4
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->f:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 148
    :catch_1
    move-exception v0

    .line 149
    :try_start_5
    const-string v1, "mfl_WearableDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getWearableDevice: mWearableDeviceInfoWaitCondition interrupted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 328
    monitor-enter p0

    .line 329
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->i:Z

    if-eqz v0, :cond_1

    .line 330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->i:Z

    .line 332
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 335
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->l:Lcom/samsung/android/hostmanager/aidl/b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/hostmanager/aidl/d;->z(Ljava/lang/String;)Z

    move-result v0

    .line 337
    if-eqz v0, :cond_2

    .line 338
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::unRegisterEXAPPListener() is success"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->k:Lcom/samsung/android/hostmanager/aidl/d;

    .line 349
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->j:Lcom/samsung/android/sdk/samsung/a/a$b;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 351
    :cond_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    .line 340
    :cond_2
    :try_start_3
    const-string v0, "mfl_WearableDeviceManager"

    const-string v1, "::unRegisterEXAPPListener() is fail"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 343
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 833
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 840
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    .line 842
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 843
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 845
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 847
    if-nez p2, :cond_1

    .line 848
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 850
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v3

    .line 852
    if-eqz v3, :cond_2

    .line 853
    const-string v3, "mfl_WearableDeviceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::updateWearableDeviceNetworkMode(): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : Updated in DB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/samsung/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_1
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/samsung/a/a;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 865
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 857
    :cond_2
    :try_start_1
    const-string v3, "mfl_WearableDeviceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::updateWearableDeviceNetworkMode(): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ": NOT Updated in DB!!"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 865
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/samsung/a/a;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 866
    return-void
.end method
