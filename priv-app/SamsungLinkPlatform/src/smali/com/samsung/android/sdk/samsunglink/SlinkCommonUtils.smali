.class public Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mSInstance:Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;


# instance fields
.field private mClientApp:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->NONE:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mClientApp:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;
    .locals 2

    .prologue
    .line 30
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mSInstance:Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mSInstance:Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;

    .line 33
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mSInstance:Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addAppIdToIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mClientApp:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->NONE:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    if-ne v0, v1, :cond_0

    .line 48
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getClientApp(I)Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mClientApp:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    .line 50
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::addAppIdToIntent() ClientApp: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mClientApp:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const-string v0, "com.samsung.android.sdk.samsunglink.APPLICATION_ID"

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mClientApp:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    return-void
.end method

.method public getClientApp(I)Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;
    .locals 4

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->getProcessNameFromPid(I)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 65
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->TAG:Ljava/lang/String;

    const-string v1, "::getClientApp() : Unkown Client App"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->NONE:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    .line 87
    :goto_0
    return-object v0

    .line 69
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getClientApp() : processName : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.pcw"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->SLINK_UI_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.gallery3d"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 74
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->GALLERY_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.samsung.everglades.video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.videoplayer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 77
    :cond_3
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->VIDEO_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    goto :goto_0

    .line 78
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.music"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 79
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->MUSIC_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    goto :goto_0

    .line 80
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.example"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 82
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->SLINK_PLATFORM_SAMPLE_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    goto :goto_0

    .line 85
    :cond_6
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->TAG:Ljava/lang/String;

    const-string v1, "::getClientApp() : Unkown Client App"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->NONE:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    goto :goto_0
.end method

.method public getProcessId()I
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    return v0
.end method

.method public getProcessNameFromPid(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkCommonUtils;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 94
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 97
    iget v2, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v2, p1, :cond_0

    .line 98
    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 101
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
