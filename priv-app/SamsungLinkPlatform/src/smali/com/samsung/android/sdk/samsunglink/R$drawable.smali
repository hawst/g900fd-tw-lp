.class public final Lcom/samsung/android/sdk/samsunglink/R$drawable;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbtn_icon_cancel:I = 0x7f020000

.field public static final actionbtn_icon_deregister_dis:I = 0x7f020001

.field public static final actionbtn_icon_deregister_press:I = 0x7f020002

.field public static final actionbtn_icon_more:I = 0x7f020003

.field public static final actionbtn_icon_ok:I = 0x7f020004

.field public static final actionbtn_icon_ok_dim:I = 0x7f020005

.field public static final actionbtn_icon_ok_selector:I = 0x7f020006

.field public static final actionbtn_text_done_selector:I = 0x7f020007

.field public static final actionmode_bg_shape:I = 0x7f020008

.field public static final activity_picker_bg_activated:I = 0x7f020009

.field public static final activity_picker_bg_focused:I = 0x7f02000a

.field public static final asp_progress_bar_small_rotate:I = 0x7f02000b

.field public static final btn_line_platform_dim:I = 0x7f02000c

.field public static final btn_line_platform_dim_white:I = 0x7f02000d

.field public static final btn_line_platform_foc:I = 0x7f02000e

.field public static final btn_line_platform_foc_white:I = 0x7f02000f

.field public static final btn_line_platform_nor:I = 0x7f020010

.field public static final btn_line_platform_nor_white:I = 0x7f020011

.field public static final btn_line_platform_pre:I = 0x7f020012

.field public static final btn_line_platform_pre_white:I = 0x7f020013

.field public static final btn_line_platform_selector:I = 0x7f020014

.field public static final btn_line_platform_selector_white:I = 0x7f020015

.field public static final btn_line_text_selector:I = 0x7f020016

.field public static final btn_line_white_disable:I = 0x7f020017

.field public static final btn_line_white_disabled_focus:I = 0x7f020018

.field public static final btn_line_white_focus:I = 0x7f020019

.field public static final btn_line_white_nor:I = 0x7f02001a

.field public static final btn_line_white_press:I = 0x7f02001b

.field public static final btn_line_white_selector:I = 0x7f02001c

.field public static final btn_transfer_remove:I = 0x7f02001d

.field public static final circle_sendto:I = 0x7f02001e

.field public static final circle_sendto_d:I = 0x7f02001f

.field public static final content_list_bg_selector:I = 0x7f020020

.field public static final content_list_bg_selector_platf:I = 0x7f020021

.field public static final content_list_bg_selector_platf_white:I = 0x7f020022

.field public static final delete_storage_button_selector:I = 0x7f020023

.field public static final device_blueray_dim:I = 0x7f020024

.field public static final device_blueray_dis:I = 0x7f020025

.field public static final device_blueray_nor:I = 0x7f020026

.field public static final device_blueray_nor_w:I = 0x7f020027

.field public static final device_camera_dim:I = 0x7f020028

.field public static final device_camera_dis:I = 0x7f020029

.field public static final device_camera_nor:I = 0x7f02002a

.field public static final device_camera_nor_w:I = 0x7f02002b

.field public static final device_homesync_dim:I = 0x7f02002c

.field public static final device_homesync_dis:I = 0x7f02002d

.field public static final device_homesync_nor:I = 0x7f02002e

.field public static final device_homesync_nor_w:I = 0x7f02002f

.field public static final device_pc_dim:I = 0x7f020030

.field public static final device_pc_dis:I = 0x7f020031

.field public static final device_pc_nor:I = 0x7f020032

.field public static final device_pc_nor_w:I = 0x7f020033

.field public static final device_phone_dim:I = 0x7f020034

.field public static final device_phone_dis:I = 0x7f020035

.field public static final device_phone_nor:I = 0x7f020036

.field public static final device_phone_nor_w:I = 0x7f020037

.field public static final device_tab_dim:I = 0x7f020038

.field public static final device_tab_dis:I = 0x7f020039

.field public static final device_tab_nor:I = 0x7f02003a

.field public static final device_tab_nor_w:I = 0x7f02003b

.field public static final device_tv_dim:I = 0x7f02003c

.field public static final device_tv_dis:I = 0x7f02003d

.field public static final device_tv_nor:I = 0x7f02003e

.field public static final device_tv_nor_w:I = 0x7f02003f

.field public static final device_unknown_dim:I = 0x7f020040

.field public static final device_unknown_dis:I = 0x7f020041

.field public static final device_unknown_nor:I = 0x7f020042

.field public static final device_unknown_nor_w:I = 0x7f020043

.field public static final dialog_button_text_selector:I = 0x7f020044

.field public static final drawer_3g4g_nor:I = 0x7f020045

.field public static final drawer_option_icn_bg_selector:I = 0x7f020046

.field public static final drawer_wifi_nor:I = 0x7f020047

.field public static final drawer_wlan_nor:I = 0x7f020048

.field public static final edit_text_signin_bg:I = 0x7f020049

.field public static final header_line_divide:I = 0x7f02004a

.field public static final highlight_overlay:I = 0x7f02004b

.field public static final highlight_overlay_file_transfer:I = 0x7f02004c

.field public static final highlight_overlay_file_transfer_list:I = 0x7f02004d

.field public static final highlight_overlay_file_transfer_plt_list:I = 0x7f02004e

.field public static final ic_more_option_overflow:I = 0x7f02004f

.field public static final ic_more_option_refresh_d:I = 0x7f020050

.field public static final ic_more_option_refresh_n:I = 0x7f020051

.field public static final ic_more_option_refresh_selector:I = 0x7f020052

.field public static final icn_refresh_device:I = 0x7f020053

.field public static final img_search_no_contents:I = 0x7f020054

.field public static final info_device_blueray_dim:I = 0x7f020055

.field public static final info_device_blueray_nor:I = 0x7f020056

.field public static final info_device_camera_dim:I = 0x7f020057

.field public static final info_device_camera_nor:I = 0x7f020058

.field public static final info_device_homesync_dim:I = 0x7f020059

.field public static final info_device_homesync_nor:I = 0x7f02005a

.field public static final info_device_homesync_pivate_dim:I = 0x7f02005b

.field public static final info_device_homesync_pivate_nor:I = 0x7f02005c

.field public static final info_device_pc_dim:I = 0x7f02005d

.field public static final info_device_pc_nor:I = 0x7f02005e

.field public static final info_device_phone_dim:I = 0x7f02005f

.field public static final info_device_phone_nor:I = 0x7f020060

.field public static final info_device_tab_dim:I = 0x7f020061

.field public static final info_device_tab_nor:I = 0x7f020062

.field public static final info_device_tv_dim:I = 0x7f020063

.field public static final info_device_tv_nor:I = 0x7f020064

.field public static final info_device_unknown_dim:I = 0x7f020065

.field public static final info_device_unknown_nor:I = 0x7f020066

.field public static final info_icon_doc:I = 0x7f020067

.field public static final info_icon_file:I = 0x7f020068

.field public static final info_icon_music:I = 0x7f020069

.field public static final info_icon_photo:I = 0x7f02006a

.field public static final info_icon_video:I = 0x7f02006b

.field public static final info_storage_button_selector:I = 0x7f02006c

.field public static final land_icn_cancel:I = 0x7f02006d

.field public static final land_icn_cancel_dis:I = 0x7f02006e

.field public static final land_icn_cancel_selector:I = 0x7f02006f

.field public static final link_list_disabled_focused_holo_light:I = 0x7f020070

.field public static final link_list_disabled_holo_light:I = 0x7f020071

.field public static final link_list_focused_holo_light:I = 0x7f020072

.field public static final link_list_pressed_holo_light:I = 0x7f020073

.field public static final link_list_selected_holo_light:I = 0x7f020074

.field public static final list_longpressed_holo:I = 0x7f020075

.field public static final list_pressed_holo_light:I = 0x7f020076

.field public static final list_selector_disabled_holo_light:I = 0x7f020077

.field public static final mux_ic_samusnglink:I = 0x7f020078

.field public static final myfiles_list_amr:I = 0x7f020079

.field public static final myfiles_list_amr_l:I = 0x7f02007a

.field public static final myfiles_list_apk:I = 0x7f02007b

.field public static final myfiles_list_apk_l:I = 0x7f02007c

.field public static final myfiles_list_broken:I = 0x7f02007d

.field public static final myfiles_list_broken_l:I = 0x7f02007e

.field public static final myfiles_list_doc:I = 0x7f02007f

.field public static final myfiles_list_doc_l:I = 0x7f020080

.field public static final myfiles_list_document:I = 0x7f020081

.field public static final myfiles_list_document_l:I = 0x7f020082

.field public static final myfiles_list_eml:I = 0x7f020083

.field public static final myfiles_list_eml_att:I = 0x7f020084

.field public static final myfiles_list_eml_att_l:I = 0x7f020085

.field public static final myfiles_list_eml_l:I = 0x7f020086

.field public static final myfiles_list_etc:I = 0x7f020087

.field public static final myfiles_list_etc_l:I = 0x7f020088

.field public static final myfiles_list_folder:I = 0x7f020089

.field public static final myfiles_list_folder_cue:I = 0x7f02008a

.field public static final myfiles_list_folder_cue_l:I = 0x7f02008b

.field public static final myfiles_list_folder_d:I = 0x7f02008c

.field public static final myfiles_list_folder_l:I = 0x7f02008d

.field public static final myfiles_list_folder_l_d:I = 0x7f02008e

.field public static final myfiles_list_folder_open:I = 0x7f02008f

.field public static final myfiles_list_folder_open_l:I = 0x7f020090

.field public static final myfiles_list_folder_open_sdcard:I = 0x7f020091

.field public static final myfiles_list_folder_open_sdcard_l:I = 0x7f020092

.field public static final myfiles_list_folder_sdcard:I = 0x7f020093

.field public static final myfiles_list_folder_sdcard_l:I = 0x7f020094

.field public static final myfiles_list_html:I = 0x7f020095

.field public static final myfiles_list_html_l:I = 0x7f020096

.field public static final myfiles_list_hwp:I = 0x7f020097

.field public static final myfiles_list_hwp_l:I = 0x7f020098

.field public static final myfiles_list_mp4:I = 0x7f020099

.field public static final myfiles_list_mp4_l:I = 0x7f02009a

.field public static final myfiles_list_music:I = 0x7f02009b

.field public static final myfiles_list_music_l:I = 0x7f02009c

.field public static final myfiles_list_pdf:I = 0x7f02009d

.field public static final myfiles_list_pdf_l:I = 0x7f02009e

.field public static final myfiles_list_play:I = 0x7f02009f

.field public static final myfiles_list_play_l:I = 0x7f0200a0

.field public static final myfiles_list_ppt:I = 0x7f0200a1

.field public static final myfiles_list_ppt_l:I = 0x7f0200a2

.field public static final myfiles_list_sdcard:I = 0x7f0200a3

.field public static final myfiles_list_snb:I = 0x7f0200a4

.field public static final myfiles_list_snb_l:I = 0x7f0200a5

.field public static final myfiles_list_spd:I = 0x7f0200a6

.field public static final myfiles_list_spd_l:I = 0x7f0200a7

.field public static final myfiles_list_storyalbum:I = 0x7f0200a8

.field public static final myfiles_list_storyalbum_l:I = 0x7f0200a9

.field public static final myfiles_list_task:I = 0x7f0200aa

.field public static final myfiles_list_task_l:I = 0x7f0200ab

.field public static final myfiles_list_theme:I = 0x7f0200ac

.field public static final myfiles_list_theme_l:I = 0x7f0200ad

.field public static final myfiles_list_txt:I = 0x7f0200ae

.field public static final myfiles_list_txt_l:I = 0x7f0200af

.field public static final myfiles_list_unsupport:I = 0x7f0200b0

.field public static final myfiles_list_unsupport_l:I = 0x7f0200b1

.field public static final myfiles_list_vcalendar:I = 0x7f0200b2

.field public static final myfiles_list_vcalendar_l:I = 0x7f0200b3

.field public static final myfiles_list_vcard:I = 0x7f0200b4

.field public static final myfiles_list_vcard_l:I = 0x7f0200b5

.field public static final myfiles_list_video:I = 0x7f0200b6

.field public static final myfiles_list_video_l:I = 0x7f0200b7

.field public static final myfiles_list_vtext:I = 0x7f0200b8

.field public static final myfiles_list_vtext_l:I = 0x7f0200b9

.field public static final myfiles_list_xls:I = 0x7f0200ba

.field public static final myfiles_list_xls_l:I = 0x7f0200bb

.field public static final myfiles_root_device:I = 0x7f0200bc

.field public static final myfiles_root_device_l:I = 0x7f0200bd

.field public static final myfiles_root_sdcard:I = 0x7f0200be

.field public static final myfiles_root_sdcard_l:I = 0x7f0200bf

.field public static final myfiles_root_usb:I = 0x7f0200c0

.field public static final myfiles_root_usb_l:I = 0x7f0200c1

.field public static final plat_allshare_logo:I = 0x7f0200c2

.field public static final plat_cancel_nor:I = 0x7f0200c3

.field public static final plat_cancel_pre:I = 0x7f0200c4

.field public static final plat_cancel_pre_2_white:I = 0x7f0200c5

.field public static final plat_cancel_pre_w:I = 0x7f0200c6

.field public static final plat_cancel_pre_white:I = 0x7f0200c7

.field public static final plat_device_blueray_dim:I = 0x7f0200c8

.field public static final plat_device_blueray_nor:I = 0x7f0200c9

.field public static final plat_device_camera_dim:I = 0x7f0200ca

.field public static final plat_device_camera_nor:I = 0x7f0200cb

.field public static final plat_device_homesync_dim:I = 0x7f0200cc

.field public static final plat_device_homesync_nor:I = 0x7f0200cd

.field public static final plat_device_homesync_pivate_dim:I = 0x7f0200ce

.field public static final plat_device_homesync_pivate_nor:I = 0x7f0200cf

.field public static final plat_device_pc_dim:I = 0x7f0200d0

.field public static final plat_device_pc_nor:I = 0x7f0200d1

.field public static final plat_device_phone_dim:I = 0x7f0200d2

.field public static final plat_device_phone_nor:I = 0x7f0200d3

.field public static final plat_device_tab_dim:I = 0x7f0200d4

.field public static final plat_device_tab_nor:I = 0x7f0200d5

.field public static final plat_device_tv_dim:I = 0x7f0200d6

.field public static final plat_device_tv_nor:I = 0x7f0200d7

.field public static final plat_device_unknown_dim:I = 0x7f0200d8

.field public static final plat_device_unknown_nor:I = 0x7f0200d9

.field public static final plat_no_transfer:I = 0x7f0200da

.field public static final plat_no_transfer_white:I = 0x7f0200db

.field public static final plat_transfer_down_nor:I = 0x7f0200dc

.field public static final plat_transfer_down_nor_white:I = 0x7f0200dd

.field public static final plat_transfer_down_pre:I = 0x7f0200de

.field public static final plat_transfer_down_pre_white:I = 0x7f0200df

.field public static final plat_transfer_popup_bg:I = 0x7f0200e0

.field public static final plat_transfer_popup_bg_w:I = 0x7f0200e1

.field public static final platf_sendto_device_capacity_color_selector_white:I = 0x7f0200e2

.field public static final platf_sendto_device_disable_capacity_color_selector_white:I = 0x7f0200e3

.field public static final platf_sendto_device_disable_name_color_selector_white:I = 0x7f0200e4

.field public static final platf_sendto_device_name_color_selector_white:I = 0x7f0200e5

.field public static final platf_transfer_tip_bg_w_white:I = 0x7f0200e6

.field public static final platf_transfer_tip_picker_w_white:I = 0x7f0200e7

.field public static final platform_arrow:I = 0x7f0200e8

.field public static final platform_banner_download_button_selector:I = 0x7f0200e9

.field public static final platform_btn_line_text_selector:I = 0x7f0200ea

.field public static final platform_btn_line_text_selector_white:I = 0x7f0200eb

.field public static final platform_circle_sendto:I = 0x7f0200ec

.field public static final platform_circle_sendto_d:I = 0x7f0200ed

.field public static final platform_close_dim:I = 0x7f0200ee

.field public static final platform_close_foc:I = 0x7f0200ef

.field public static final platform_close_nor:I = 0x7f0200f0

.field public static final platform_close_pre:I = 0x7f0200f1

.field public static final platform_close_pre_white:I = 0x7f0200f2

.field public static final platform_delete_dim:I = 0x7f0200f3

.field public static final platform_delete_foc:I = 0x7f0200f4

.field public static final platform_delete_nor:I = 0x7f0200f5

.field public static final platform_delete_pre:I = 0x7f0200f6

.field public static final platform_delete_pre_white:I = 0x7f0200f7

.field public static final platform_icn:I = 0x7f0200f8

.field public static final platform_icn_small:I = 0x7f0200f9

.field public static final platform_img_w:I = 0x7f0200fa

.field public static final platform_popup_cancel_button_selector:I = 0x7f0200fb

.field public static final platform_popup_cancel_button_selector_uiapp:I = 0x7f0200fc

.field public static final platform_popup_cancel_button_selector_white:I = 0x7f0200fd

.field public static final platform_refresh_dim:I = 0x7f0200fe

.field public static final platform_refresh_foc:I = 0x7f0200ff

.field public static final platform_refresh_nor:I = 0x7f020100

.field public static final platform_refresh_pre:I = 0x7f020101

.field public static final platform_refresh_pre_white:I = 0x7f020102

.field public static final platform_transfer:I = 0x7f020103

.field public static final quickpanel_transfer_start:I = 0x7f020104

.field public static final quickpanel_transfer_start_b:I = 0x7f020105

.field public static final registered_storage_dreg_action_selector:I = 0x7f020106

.field public static final samsungaccount_icon:I = 0x7f020107

.field public static final second_depth_device_private:I = 0x7f020108

.field public static final second_depth_device_private_dim:I = 0x7f020109

.field public static final second_depth_device_tv:I = 0x7f02010a

.field public static final send_to_lollipop_list_selector:I = 0x7f02010b

.field public static final sendto_bg_circle_focus_selector:I = 0x7f02010c

.field public static final sendto_bg_circle_focus_shape:I = 0x7f02010d

.field public static final sendto_bg_circle_selector:I = 0x7f02010e

.field public static final sendto_bg_circle_selector_uiapp:I = 0x7f02010f

.field public static final sendto_btn_bluray_selector:I = 0x7f020110

.field public static final sendto_btn_camera_selector:I = 0x7f020111

.field public static final sendto_btn_homesync_selector:I = 0x7f020112

.field public static final sendto_btn_pc_selector:I = 0x7f020113

.field public static final sendto_btn_phone_selector:I = 0x7f020114

.field public static final sendto_btn_tab_selector:I = 0x7f020115

.field public static final sendto_btn_tv_selector:I = 0x7f020116

.field public static final sendto_text_capacity_selector:I = 0x7f020117

.field public static final sendto_text_capacity_selector_white:I = 0x7f020118

.field public static final sendto_text_devicename_selector:I = 0x7f020119

.field public static final sendto_text_devicename_selector_white:I = 0x7f02011a

.field public static final set_deregister_d:I = 0x7f02011b

.field public static final set_deregister_d_f:I = 0x7f02011c

.field public static final set_deregister_f:I = 0x7f02011d

.field public static final set_deregister_n:I = 0x7f02011e

.field public static final set_deregister_p:I = 0x7f02011f

.field public static final setting_videoquality_illust:I = 0x7f020120

.field public static final setting_videoquality_illust_land:I = 0x7f020121

.field public static final slink_cancel_nor:I = 0x7f020122

.field public static final slink_device_ic_blueray:I = 0x7f020123

.field public static final slink_device_ic_blueray_dim:I = 0x7f020124

.field public static final slink_device_ic_camera:I = 0x7f020125

.field public static final slink_device_ic_camera_dim:I = 0x7f020126

.field public static final slink_device_ic_gear:I = 0x7f020127

.field public static final slink_device_ic_gear_dim:I = 0x7f020128

.field public static final slink_device_ic_homesync:I = 0x7f020129

.field public static final slink_device_ic_homesync_dim:I = 0x7f02012a

.field public static final slink_device_ic_homesync_private:I = 0x7f02012b

.field public static final slink_device_ic_homesync_private_dim:I = 0x7f02012c

.field public static final slink_device_ic_pc:I = 0x7f02012d

.field public static final slink_device_ic_pc_dim:I = 0x7f02012e

.field public static final slink_device_ic_phone:I = 0x7f02012f

.field public static final slink_device_ic_phone_dim:I = 0x7f020130

.field public static final slink_device_ic_tab:I = 0x7f020131

.field public static final slink_device_ic_tab_dim:I = 0x7f020132

.field public static final slink_device_ic_tv:I = 0x7f020133

.field public static final slink_device_ic_tv_dim:I = 0x7f020134

.field public static final slink_device_ic_unknown:I = 0x7f020135

.field public static final slink_device_ic_unknown_dim:I = 0x7f020136

.field public static final slink_trans_arrow:I = 0x7f020137

.field public static final slink_trans_down_nor:I = 0x7f020138

.field public static final slink_trans_player_close:I = 0x7f020139

.field public static final slink_trans_player_delete:I = 0x7f02013a

.field public static final slink_trans_player_pre_close:I = 0x7f02013b

.field public static final slink_trans_player_pre_delete:I = 0x7f02013c

.field public static final slink_trans_player_pre_refresh:I = 0x7f02013d

.field public static final slink_trans_player_refresh:I = 0x7f02013e

.field public static final stat_notify_gallery:I = 0x7f02013f

.field public static final storageicon_sugarsync_348x144:I = 0x7f020140

.field public static final sugarsync_link_text_color_selector:I = 0x7f020141

.field public static final tab_bg_focus:I = 0x7f020142

.field public static final tab_bg_nor:I = 0x7f020143

.field public static final tab_bg_press:I = 0x7f020144

.field public static final tab_bg_sel:I = 0x7f020145

.field public static final title_back_text_focus_shape:I = 0x7f020146

.field public static final title_back_text_focus_shape_r:I = 0x7f020147

.field public static final trans_icon_doc:I = 0x7f020148

.field public static final trans_icon_doc_w:I = 0x7f020149

.field public static final trans_icon_file:I = 0x7f02014a

.field public static final trans_icon_file_w:I = 0x7f02014b

.field public static final trans_icon_music:I = 0x7f02014c

.field public static final trans_icon_music_w:I = 0x7f02014d

.field public static final trans_icon_photo:I = 0x7f02014e

.field public static final trans_icon_photo_w:I = 0x7f02014f

.field public static final trans_icon_video:I = 0x7f020150

.field public static final trans_icon_video_w:I = 0x7f020151

.field public static final trans_player_close:I = 0x7f020152

.field public static final trans_player_close_focus:I = 0x7f020153

.field public static final trans_player_close_p:I = 0x7f020154

.field public static final trans_player_close_press:I = 0x7f020155

.field public static final trans_player_delete:I = 0x7f020156

.field public static final trans_player_delete_p:I = 0x7f020157

.field public static final trans_player_refresh:I = 0x7f020158

.field public static final trans_player_refresh_focus:I = 0x7f020159

.field public static final trans_player_refresh_p:I = 0x7f02015a

.field public static final trans_player_refresh_press:I = 0x7f02015b

.field public static final transfer_arrow:I = 0x7f02015c

.field public static final transfer_cancel_selector:I = 0x7f02015d

.field public static final transfer_cancel_selector_uiapp:I = 0x7f02015e

.field public static final transfer_cancel_selector_white:I = 0x7f02015f

.field public static final transfer_lollipop_list_selector:I = 0x7f020160

.field public static final transfer_remove_selector:I = 0x7f020161

.field public static final transfer_remove_selector_uiapp:I = 0x7f020162

.field public static final transfer_remove_selector_white:I = 0x7f020163

.field public static final transfer_retry_selector:I = 0x7f020164

.field public static final transfer_retry_selector_uiapp:I = 0x7f020165

.field public static final transfer_retry_selector_white:I = 0x7f020166

.field public static final transfer_tip_bg:I = 0x7f020167

.field public static final transfer_tip_bg_w:I = 0x7f020168

.field public static final transfer_tip_picker:I = 0x7f020169

.field public static final transfer_tip_picker_w:I = 0x7f02016a

.field public static final transfer_title_back_text_focus_shape:I = 0x7f02016b

.field public static final tw_btn_default_default_holo_light:I = 0x7f02016c

.field public static final tw_btn_default_disabled_focused_holo_light:I = 0x7f02016d

.field public static final tw_btn_default_disabled_holo_light:I = 0x7f02016e

.field public static final tw_btn_default_focused_holo_light:I = 0x7f02016f

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f020170

.field public static final tw_button_text_color_selector:I = 0x7f020171

.field public static final tw_buttonbarbutton_selector_uiapp:I = 0x7f020172

.field public static final tw_dialog_bottom_bg_holo_light:I = 0x7f020173

.field public static final tw_dialog_title_holo_light:I = 0x7f020174

.field public static final tw_divider_vertical_holo_light_white:I = 0x7f020175

.field public static final tw_ic_ab_back_holo_dark:I = 0x7f020176

.field public static final tw_ic_ab_back_mtrl:I = 0x7f020177

.field public static final tw_ic_menu_moreoverflow_mtrl:I = 0x7f020178

.field public static final tw_list_button_selector:I = 0x7f020179

.field public static final tw_list_disabled_focused_holo_light:I = 0x7f02017a

.field public static final tw_list_disabled_holo_light:I = 0x7f02017b

.field public static final tw_list_divider_holo_dark:I = 0x7f02017c

.field public static final tw_list_divider_holo_light:I = 0x7f02017d

.field public static final tw_list_focused_holo_light:I = 0x7f02017e

.field public static final tw_list_longpressed_holo_light:I = 0x7f02017f

.field public static final tw_list_pressed_holo_light:I = 0x7f020180

.field public static final tw_list_selected_holo_light:I = 0x7f020181

.field public static final tw_widget_progressbar_small_holo_light:I = 0x7f020182

.field public static final uiapp_tab_bg_selector:I = 0x7f020183


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
