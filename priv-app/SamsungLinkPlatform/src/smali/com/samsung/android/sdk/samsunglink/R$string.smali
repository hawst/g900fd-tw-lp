.class public final Lcom/samsung/android/sdk/samsunglink/R$string;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Edit:I = 0x7f0a03ec

.field public static final L_more_options:I = 0x7f0a03e9

.field public static final _CODE:I = 0x7f0a001e

.field public static final _LANGUAGE:I = 0x7f0a001f

.field public static final accepted_confirm:I = 0x7f0a03b2

.field public static final access_to_samsunglink:I = 0x7f0a03d5

.field public static final access_to_samsunglink_detail:I = 0x7f0a03d6

.field public static final access_upper_folders_is_not_allowed:I = 0x7f0a0315

.field public static final add_button:I = 0x7f0a013a

.field public static final add_devices:I = 0x7f0a011e

.field public static final add_service:I = 0x7f0a02c4

.field public static final add_service_auto_upload:I = 0x7f0a0351

.field public static final add_storages:I = 0x7f0a011d

.field public static final agree_to_terms_and_conditions:I = 0x7f0a013b

.field public static final alert:I = 0x7f0a0253

.field public static final all:I = 0x7f0a0211

.field public static final all_devices:I = 0x7f0a02c3

.field public static final all_devices_transfer:I = 0x7f0a03f8

.field public static final all_file_transfer_cancel:I = 0x7f0a03c4

.field public static final allow_access_of_other_devices:I = 0x7f0a0316

.field public static final app_dialog_msg_force_update:I = 0x7f0a013c

.field public static final app_dialog_msg_update:I = 0x7f0a013d

.field public static final app_in_app_exit_dialog:I = 0x7f0a023e

.field public static final app_menu_detail:I = 0x7f0a013e

.field public static final app_menu_filter:I = 0x7f0a0338

.field public static final app_menu_home:I = 0x7f0a013f

.field public static final app_menu_info:I = 0x7f0a0140

.field public static final app_menu_myaccount:I = 0x7f0a0141

.field public static final app_menu_near:I = 0x7f0a0142

.field public static final app_menu_refresh:I = 0x7f0a0143

.field public static final app_menu_search:I = 0x7f0a0144

.field public static final app_menu_settings:I = 0x7f0a0145

.field public static final app_menu_signout:I = 0x7f0a0146

.field public static final app_menu_sort:I = 0x7f0a0147

.field public static final app_menu_storage_info:I = 0x7f0a0148

.field public static final app_name:I = 0x7f0a0149

.field public static final app_name_launcher:I = 0x7f0a0405

.field public static final app_token_fail:I = 0x7f0a00f7

.field public static final artist_seperator_subtitle:I = 0x7f0a02b4

.field public static final artist_seperator_subtitle_web_storage:I = 0x7f0a0318

.field public static final asp_thumbnail_prefetcher_service_description:I = 0x7f0a040c

.field public static final asp_thumbnail_prefetcher_service_label:I = 0x7f0a040d

.field public static final auto_archive_all_contents:I = 0x7f0a0334

.field public static final auto_archive_body:I = 0x7f0a0332

.field public static final auto_archive_from_now:I = 0x7f0a0333

.field public static final auto_archive_frome_selectedDate:I = 0x7f0a0416

.field public static final auto_archive_time_popup_title:I = 0x7f0a0335

.field public static final auto_upload_body:I = 0x7f0a01c6

.field public static final auto_upload_body_for_homesync:I = 0x7f0a0275

.field public static final auto_upload_contents_label:I = 0x7f0a01df

.field public static final auto_upload_do_change:I = 0x7f0a014a

.field public static final auto_upload_failed:I = 0x7f0a01ad

.field public static final auto_upload_no_storage_multi_select:I = 0x7f0a014b

.field public static final auto_upload_no_storage_register:I = 0x7f0a014c

.field public static final auto_upload_set_storage:I = 0x7f0a014d

.field public static final auto_upload_set_storage_photo_video:I = 0x7f0a0325

.field public static final auto_upload_set_storage_video:I = 0x7f0a0324

.field public static final auto_upload_storage:I = 0x7f0a01de

.field public static final auto_upload_temporary_fail:I = 0x7f0a0287

.field public static final auto_upload_temporary_fail_homesync:I = 0x7f0a0288

.field public static final auto_upload_update_feature_homesync:I = 0x7f0a0304

.field public static final auto_upload_update_feature_pc:I = 0x7f0a0305

.field public static final auto_upload_wi_fi_camera_notification:I = 0x7f0a0239

.field public static final auto_uploading_completed:I = 0x7f0a033d

.field public static final auto_uploading_error:I = 0x7f0a033b

.field public static final base_url:I = 0x7f0a0000

.field public static final base_url_chn:I = 0x7f0a0002

.field public static final base_url_chn_stg:I = 0x7f0a0006

.field public static final base_url_stg:I = 0x7f0a0004

.field public static final battery_level:I = 0x7f0a02ee

.field public static final battery_low:I = 0x7f0a0303

.field public static final buffering_str:I = 0x7f0a012d

.field public static final cancel:I = 0x7f0a014e

.field public static final cancel_all_action:I = 0x7f0a02bc

.field public static final change_device_name:I = 0x7f0a019b

.field public static final change_password:I = 0x7f0a027a

.field public static final change_password_1:I = 0x7f0a0261

.field public static final change_password_2:I = 0x7f0a0262

.field public static final clear_aciton:I = 0x7f0a032f

.field public static final clear_completed_tasks:I = 0x7f0a0339

.field public static final clear_history_aciton:I = 0x7f0a02be

.field public static final clear_history_prompt:I = 0x7f0a029b

.field public static final clear_transfers_message:I = 0x7f0a02bb

.field public static final close_popup:I = 0x7f0a014f

.field public static final cloud_auth_unknown:I = 0x7f0a02fa

.field public static final cloud_sup_setting_del2:I = 0x7f0a00bb

.field public static final cn_url_bss:I = 0x7f0a0010

.field public static final cn_url_bss_stg:I = 0x7f0a001a

.field public static final cn_url_fwk:I = 0x7f0a000d

.field public static final cn_url_fwk_stg:I = 0x7f0a0017

.field public static final cn_url_mg:I = 0x7f0a0011

.field public static final cn_url_mg_stg:I = 0x7f0a001b

.field public static final cn_url_sw:I = 0x7f0a000e

.field public static final cn_url_sw_stg:I = 0x7f0a0018

.field public static final cn_url_www:I = 0x7f0a000f

.field public static final cn_url_www_stg:I = 0x7f0a0019

.field public static final coming_soon:I = 0x7f0a023d

.field public static final common_common_1:I = 0x7f0a003b

.field public static final common_connect:I = 0x7f0a0245

.field public static final common_doc_tab:I = 0x7f0a0273

.field public static final common_edit:I = 0x7f0a0032

.field public static final common_edit2:I = 0x7f0a0021

.field public static final common_error_server:I = 0x7f0a0041

.field public static final common_file_not_support:I = 0x7f0a0284

.field public static final common_file_size:I = 0x7f0a0023

.field public static final common_folder_name:I = 0x7f0a0031

.field public static final common_folder_setting:I = 0x7f0a0030

.field public static final common_home:I = 0x7f0a0024

.field public static final common_info_conn:I = 0x7f0a0020

.field public static final common_later:I = 0x7f0a0244

.field public static final common_link_faq:I = 0x7f0a0039

.field public static final common_link_next:I = 0x7f0a003a

.field public static final common_list_2:I = 0x7f0a0040

.field public static final common_mycloud:I = 0x7f0a002e

.field public static final common_mydevice:I = 0x7f0a002f

.field public static final common_no_network:I = 0x7f0a0042

.field public static final common_not_again:I = 0x7f0a002d

.field public static final common_off:I = 0x7f0a0036

.field public static final common_on:I = 0x7f0a0035

.field public static final common_others_tab:I = 0x7f0a0274

.field public static final common_photos:I = 0x7f0a0033

.field public static final common_popup_allow:I = 0x7f0a035a

.field public static final common_popup_apply:I = 0x7f0a0330

.field public static final common_popup_cancel:I = 0x7f0a0027

.field public static final common_popup_close:I = 0x7f0a0025

.field public static final common_popup_confirm:I = 0x7f0a0026

.field public static final common_popup_no:I = 0x7f0a002b

.field public static final common_popup_notification:I = 0x7f0a0029

.field public static final common_popup_notnow:I = 0x7f0a028a

.field public static final common_popup_replay:I = 0x7f0a02f2

.field public static final common_popup_retry:I = 0x7f0a002c

.field public static final common_popup_save:I = 0x7f0a0028

.field public static final common_popup_yes:I = 0x7f0a002a

.field public static final common_refresh:I = 0x7f0a003f

.field public static final common_screen_ratio:I = 0x7f0a02ac

.field public static final common_service_name:I = 0x7f0a0037

.field public static final common_sign_name:I = 0x7f0a0038

.field public static final common_storageheader_1:I = 0x7f0a003d

.field public static final common_storageheader_2:I = 0x7f0a003e

.field public static final common_total:I = 0x7f0a0034

.field public static final complete_action_using:I = 0x7f0a01ea

.field public static final confirm_password:I = 0x7f0a0266

.field public static final confirm_password_1:I = 0x7f0a0263

.field public static final confirm_password_2:I = 0x7f0a0268

.field public static final connect_via_roaming_network:I = 0x7f0a031f

.field public static final connecting_to_network:I = 0x7f0a01ee

.field public static final connection_status_changed:I = 0x7f0a03dd

.field public static final content_aggregator_service_description:I = 0x7f0a0406

.field public static final content_aggregator_service_label:I = 0x7f0a0407

.field public static final content_duration:I = 0x7f0a0137

.field public static final cross_device_delete_message:I = 0x7f0a0317

.field public static final current_password:I = 0x7f0a026e

.field public static final d2s_token_err_msg:I = 0x7f0a01f1

.field public static final data_limit_noti_title:I = 0x7f0a02ff

.field public static final data_roaming_warning:I = 0x7f0a031a

.field public static final date_format_month_day_year:I = 0x7f0a02b6

.field public static final date_format_month_year:I = 0x7f0a02b5

.field public static final date_take:I = 0x7f0a0136

.field public static final delete_inprogress:I = 0x7f0a00af

.field public static final delete_prompt_confirm:I = 0x7f0a00ad

.field public static final delete_prompt_permanent:I = 0x7f0a01eb

.field public static final delete_register_guide_confirm:I = 0x7f0a034f

.field public static final delete_register_msg:I = 0x7f0a036d

.field public static final delete_selected_device_only:I = 0x7f0a03cd

.field public static final dereg_a_device:I = 0x7f0a010c

.field public static final dereg_device:I = 0x7f0a010d

.field public static final dereg_device_autoupload_enabled:I = 0x7f0a010e

.field public static final dereg_device_autoupload_in_progress:I = 0x7f0a0110

.field public static final dereg_device_file_transfer_in_progress:I = 0x7f0a010f

.field public static final dereg_device_network_error:I = 0x7f0a0111

.field public static final dereg_spc_invalid:I = 0x7f0a0259

.field public static final deregister_bd_error:I = 0x7f0a0285

.field public static final deregister_error_bluray:I = 0x7f0a0290

.field public static final deregister_error_homesync:I = 0x7f0a0291

.field public static final deregister_homesync:I = 0x7f0a025d

.field public static final deregister_sync_off:I = 0x7f0a03b0

.field public static final deregistered_toast_msg:I = 0x7f0a0348

.field public static final details:I = 0x7f0a0150

.field public static final details_added_date:I = 0x7f0a02d2

.field public static final details_album:I = 0x7f0a02d9

.field public static final details_artist:I = 0x7f0a02d8

.field public static final details_file_name:I = 0x7f0a02d0

.field public static final details_file_size:I = 0x7f0a02d1

.field public static final details_folder_name:I = 0x7f0a0320

.field public static final details_location:I = 0x7f0a02da

.field public static final details_modified_date:I = 0x7f0a02d3

.field public static final details_playing_time:I = 0x7f0a02d6

.field public static final details_resolution:I = 0x7f0a02d5

.field public static final details_song_title:I = 0x7f0a02d7

.field public static final details_taken_date:I = 0x7f0a02d4

.field public static final details_track_length:I = 0x7f0a029e

.field public static final details_track_number:I = 0x7f0a029f

.field public static final device_b_cap:I = 0x7f0a03fb

.field public static final device_connect:I = 0x7f0a00e2

.field public static final device_gb_cap:I = 0x7f0a03fc

.field public static final device_info:I = 0x7f0a0199

.field public static final device_info_failed_comm_error:I = 0x7f0a03f1

.field public static final device_info_failed_general:I = 0x7f0a03d7

.field public static final device_kb_cap:I = 0x7f0a03fd

.field public static final device_mb_cap:I = 0x7f0a03fe

.field public static final device_name_setting:I = 0x7f0a00e0

.field public static final device_pc_step1_button:I = 0x7f0a00e6

.field public static final device_pc_step1_title:I = 0x7f0a00e5

.field public static final device_pc_step2_button:I = 0x7f0a00e9

.field public static final device_pc_step2_title:I = 0x7f0a00e8

.field public static final device_pc_unable_step2:I = 0x7f0a00e7

.field public static final device_settings:I = 0x7f0a019a

.field public static final devicename:I = 0x7f0a0309

.field public static final dialog_alert_title:I = 0x7f0a03ff

.field public static final dialog_msg_update:I = 0x7f0a028b

.field public static final display_total_cap_for_all_storage:I = 0x7f0a0195

.field public static final dlna_device_default_alias:I = 0x7f0a0400

.field public static final dlna_device_list_both_networks_3g:I = 0x7f0a0151

.field public static final dlna_device_list_connected_3g:I = 0x7f0a0152

.field public static final dlna_device_list_no_devices:I = 0x7f0a0153

.field public static final dlna_device_list_no_wifi:I = 0x7f0a0154

.field public static final dlna_device_list_notification:I = 0x7f0a0155

.field public static final dlna_device_preparing:I = 0x7f0a01d7

.field public static final dlna_ff_rew_not_supported:I = 0x7f0a023a

.field public static final dlna_file_sharing:I = 0x7f0a012c

.field public static final dlna_http_user_agent:I = 0x7f0a0401

.field public static final dlna_loading:I = 0x7f0a0156

.field public static final dlna_my_device:I = 0x7f0a0157

.field public static final dlna_refresh:I = 0x7f0a0158

.field public static final dlna_samsung_tv_model:I = 0x7f0a0402

.field public static final dlna_samsung_tv_near1:I = 0x7f0a0159

.field public static final dlna_samsung_tv_near2:I = 0x7f0a015a

.field public static final dlna_samsung_tv_near3a_select:I = 0x7f0a01a4

.field public static final dlna_samsung_tv_near3b_press:I = 0x7f0a01a5

.field public static final do_not_open_again:I = 0x7f0a015b

.field public static final document_player_tap_screen:I = 0x7f0a02ec

.field public static final dont_show_again:I = 0x7f0a01fe

.field public static final download_app:I = 0x7f0a012a

.field public static final download_banner_description_one:I = 0x7f0a03b9

.field public static final download_banner_description_two:I = 0x7f0a03ba

.field public static final download_cancel:I = 0x7f0a015c

.field public static final download_complete:I = 0x7f0a015d

.field public static final download_error:I = 0x7f0a015e

.field public static final download_information:I = 0x7f0a024f

.field public static final download_preparing:I = 0x7f0a03c1

.field public static final download_scanning:I = 0x7f0a015f

.field public static final downloading:I = 0x7f0a024e

.field public static final downloading_cancel_message:I = 0x7f0a0160

.field public static final downloading_complete_message:I = 0x7f0a0161

.field public static final downloading_error_message:I = 0x7f0a0162

.field public static final downloading_files:I = 0x7f0a01d2

.field public static final downloading_message:I = 0x7f0a0163

.field public static final drawer_close:I = 0x7f0a03f7

.field public static final drawer_open:I = 0x7f0a03f6

.field public static final drawer_search_category:I = 0x7f0a03e4

.field public static final drawer_search_location:I = 0x7f0a03e3

.field public static final drawer_search_time:I = 0x7f0a03e2

.field public static final during_call:I = 0x7f0a023f

.field public static final email_required_field:I = 0x7f0a0164

.field public static final emoticon_error:I = 0x7f0a0323

.field public static final enable:I = 0x7f0a020c

.field public static final enable_samsunglink_platform:I = 0x7f0a03d2

.field public static final enter_password:I = 0x7f0a0267

.field public static final error_play_video_error_occurred:I = 0x7f0a021d

.field public static final error_share_via_filesize:I = 0x7f0a0212

.field public static final error_share_via_max_count:I = 0x7f0a0213

.field public static final error_transcoding_codec_error:I = 0x7f0a021a

.field public static final error_transcoding_drm_protected:I = 0x7f0a021b

.field public static final error_transcoding_not_enough_resources:I = 0x7f0a021c

.field public static final exceeds_file_number:I = 0x7f0a0272

.field public static final exit_app_prompt:I = 0x7f0a0103

.field public static final expand:I = 0x7f0a0307

.field public static final external_signin_prompt:I = 0x7f0a035c

.field public static final external_signin_prompt_header:I = 0x7f0a035b

.field public static final external_signin_sub_prompt:I = 0x7f0a035d

.field public static final file_device_root:I = 0x7f0a02ad

.field public static final file_name:I = 0x7f0a0022

.field public static final file_not_exist:I = 0x7f0a03bb

.field public static final file_sdcard_root:I = 0x7f0a02ae

.field public static final file_size:I = 0x7f0a0165

.field public static final file_transfer_completed:I = 0x7f0a0341

.field public static final file_transfer_completed_at_date:I = 0x7f0a03a9

.field public static final file_transfer_progress:I = 0x7f0a0340

.field public static final file_transfer_remove_button:I = 0x7f0a034c

.field public static final filebrowser_open_prompt:I = 0x7f0a0166

.field public static final filebrowser_share_prompt:I = 0x7f0a0302

.field public static final filebrowser_unknown_action:I = 0x7f0a0167

.field public static final filebrowser_unknown_viewers:I = 0x7f0a0168

.field public static final filebrowser_up:I = 0x7f0a0169

.field public static final files_folder_no_admin:I = 0x7f0a019f

.field public static final files_tab:I = 0x7f0a016a

.field public static final first_use_dialog_body:I = 0x7f0a016b

.field public static final first_use_dialog_title:I = 0x7f0a016c

.field public static final force_update_to_use_normally:I = 0x7f0a03d1

.field public static final full_video:I = 0x7f0a01c2

.field public static final fwk_url:I = 0x7f0a0001

.field public static final fwk_url_chn:I = 0x7f0a0003

.field public static final fwk_url_chn_stg:I = 0x7f0a0007

.field public static final fwk_url_stg:I = 0x7f0a0005

.field public static final gallery_layer_guide_details:I = 0x7f0a03af

.field public static final gallery_layer_guide_text:I = 0x7f0a03ae

.field public static final genre_second_depth_line_2_format:I = 0x7f0a03f0

.field public static final global_url_bss:I = 0x7f0a000b

.field public static final global_url_bss_stg:I = 0x7f0a0015

.field public static final global_url_fwk:I = 0x7f0a0008

.field public static final global_url_fwk_stg:I = 0x7f0a0012

.field public static final global_url_mg:I = 0x7f0a000c

.field public static final global_url_mg_stg:I = 0x7f0a0016

.field public static final global_url_sw:I = 0x7f0a0009

.field public static final global_url_sw_stg:I = 0x7f0a0013

.field public static final global_url_www:I = 0x7f0a000a

.field public static final global_url_www_stg:I = 0x7f0a0014

.field public static final guide_contents_other_media:I = 0x7f0a01ce

.field public static final guide_contents_play_photo:I = 0x7f0a01cc

.field public static final guide_contents_play_tv:I = 0x7f0a01cd

.field public static final guide_contents_send_photo:I = 0x7f0a01ca

.field public static final guide_contents_upload_photo:I = 0x7f0a01cb

.field public static final guide_home_devices:I = 0x7f0a01d1

.field public static final guide_home_pc:I = 0x7f0a01cf

.field public static final guide_home_storage:I = 0x7f0a01d0

.field public static final guide_tutorial:I = 0x7f0a01c9

.field public static final header_completed:I = 0x7f0a02c0

.field public static final header_ongoing:I = 0x7f0a02c1

.field public static final help_page_contact_us:I = 0x7f0a0347

.field public static final help_page_faq:I = 0x7f0a0346

.field public static final help_page_my_question:I = 0x7f0a0345

.field public static final help_page_service_introduction:I = 0x7f0a0344

.field public static final home_charge:I = 0x7f0a0043

.field public static final home_connect_via_mobile_networks:I = 0x7f0a0248

.field public static final home_data_usage_applies:I = 0x7f0a028d

.field public static final home_intro_movie:I = 0x7f0a0044

.field public static final home_login:I = 0x7f0a0045

.field public static final home_no_network_connection:I = 0x7f0a0249

.field public static final home_noti_check_connection:I = 0x7f0a0051

.field public static final home_noti_check_network:I = 0x7f0a0052

.field public static final home_privacy_policy:I = 0x7f0a003c

.field public static final home_reg:I = 0x7f0a0046

.field public static final home_samsungcloud:I = 0x7f0a0047

.field public static final home_samsunglinkplatform:I = 0x7f0a0048

.field public static final home_start:I = 0x7f0a0049

.field public static final home_verizon_charge:I = 0x7f0a028c

.field public static final homesync_details:I = 0x7f0a026c

.field public static final homesync_notification_password_invalid_error:I = 0x7f0a038e

.field public static final homesync_notification_password_verification_error:I = 0x7f0a0391

.field public static final homesync_others_encrypted:I = 0x7f0a0277

.field public static final homesync_others_personal:I = 0x7f0a0278

.field public static final homesync_others_shared:I = 0x7f0a0279

.field public static final homesync_pc_poweroff:I = 0x7f0a0293

.field public static final homesync_pc_poweroff_error_network:I = 0x7f0a0299

.field public static final homesync_pc_poweroff_error_transfer:I = 0x7f0a0297

.field public static final homesync_pc_poweroff_error_using:I = 0x7f0a0298

.field public static final homesync_pc_poweroff_wait:I = 0x7f0a0380

.field public static final homesync_pc_wakeup:I = 0x7f0a0292

.field public static final homesync_pc_wakeup_error_network:I = 0x7f0a0295

.field public static final homesync_pc_wakeup_error_retry:I = 0x7f0a0296

.field public static final homesync_pc_wakeup_wait:I = 0x7f0a0294

.field public static final homesync_please_restart_pc:I = 0x7f0a0283

.field public static final homesync_private_folder:I = 0x7f0a0219

.field public static final homesync_remote:I = 0x7f0a0218

.field public static final info_email_id_label:I = 0x7f0a0129

.field public static final info_id_label:I = 0x7f0a0237

.field public static final information_country_info_title:I = 0x7f0a03db

.field public static final install:I = 0x7f0a024c

.field public static final install_warning:I = 0x7f0a0256

.field public static final install_warning_kor:I = 0x7f0a0258

.field public static final intro_connect_allshareplay:I = 0x7f0a00ca

.field public static final intro_content_anywhere:I = 0x7f0a00cb

.field public static final intro_done:I = 0x7f0a00d5

.field public static final intro_easy_way:I = 0x7f0a00de

.field public static final intro_enjoy_your_content:I = 0x7f0a00c9

.field public static final intro_fastway_learn:I = 0x7f0a00dd

.field public static final intro_guide_down:I = 0x7f0a00df

.field public static final intro_manage_device:I = 0x7f0a00cc

.field public static final intro_not_signedup:I = 0x7f0a00e3

.field public static final intro_pc_camera_info:I = 0x7f0a00d8

.field public static final intro_pc_start:I = 0x7f0a00da

.field public static final intro_pc_upgrade_use:I = 0x7f0a00db

.field public static final intro_pc_ver:I = 0x7f0a00dc

.field public static final intro_pc_video:I = 0x7f0a00d9

.field public static final intro_picture_take_can_save_storage:I = 0x7f0a00d4

.field public static final intro_play_content_using_change_player:I = 0x7f0a00d3

.field public static final intro_register_mobile_description_1:I = 0x7f0a03a5

.field public static final intro_register_mobile_description_2:I = 0x7f0a03a6

.field public static final intro_register_mobile_description_3:I = 0x7f0a03a7

.field public static final intro_register_pc:I = 0x7f0a00d1

.field public static final intro_register_pc_description_1:I = 0x7f0a0395

.field public static final intro_register_pc_description_2:I = 0x7f0a0396

.field public static final intro_register_pc_description_3:I = 0x7f0a0397

.field public static final intro_register_pc_description_4:I = 0x7f0a0398

.field public static final intro_register_pc_you_can_play_without_download:I = 0x7f0a00d2

.field public static final intro_register_tv_description_1:I = 0x7f0a0399

.field public static final intro_register_tv_description_10:I = 0x7f0a03a2

.field public static final intro_register_tv_description_11:I = 0x7f0a03a3

.field public static final intro_register_tv_description_12:I = 0x7f0a03a4

.field public static final intro_register_tv_description_2:I = 0x7f0a039a

.field public static final intro_register_tv_description_3:I = 0x7f0a039b

.field public static final intro_register_tv_description_4:I = 0x7f0a039c

.field public static final intro_register_tv_description_5:I = 0x7f0a039d

.field public static final intro_register_tv_description_6:I = 0x7f0a039e

.field public static final intro_register_tv_description_7:I = 0x7f0a039f

.field public static final intro_register_tv_description_8:I = 0x7f0a03a0

.field public static final intro_register_tv_description_9:I = 0x7f0a03a1

.field public static final intro_remote_device_large_screen:I = 0x7f0a00ce

.field public static final intro_save_your_photo:I = 0x7f0a00cf

.field public static final intro_share_playcontent:I = 0x7f0a00c8

.field public static final intro_success_sign_browser_continue:I = 0x7f0a00e4

.field public static final intro_watch_video2:I = 0x7f0a00cd

.field public static final intro_wireless_transfer_music_device:I = 0x7f0a00d0

.field public static final introduction_device_total_count:I = 0x7f0a0375

.field public static final introduction_new_page_description:I = 0x7f0a0374

.field public static final introduction_new_page_device_loading:I = 0x7f0a0373

.field public static final introduction_samsunglink_in_version:I = 0x7f0a03d4

.field public static final joindrop_add_message_01:I = 0x7f0a005f

.field public static final joinsugar_add_message_02:I = 0x7f0a01d5

.field public static final joinsugar_download:I = 0x7f0a0059

.field public static final joinsugar_email:I = 0x7f0a0055

.field public static final joinsugar_email_exist:I = 0x7f0a005b

.field public static final joinsugar_email_invalid:I = 0x7f0a005a

.field public static final joinsugar_html:I = 0x7f0a02f4

.field public static final joinsugar_onlysugar_app:I = 0x7f0a0062

.field public static final joinsugar_onlysugar_app_info:I = 0x7f0a0063

.field public static final joinsugar_onlysugar_info:I = 0x7f0a0061

.field public static final joinsugar_pass_length:I = 0x7f0a005e

.field public static final joinsugar_pass_mandatory:I = 0x7f0a005c

.field public static final joinsugar_pass_mismatch:I = 0x7f0a005d

.field public static final joinsugar_required:I = 0x7f0a0056

.field public static final joinsugar_skip:I = 0x7f0a0058

.field public static final joinsugar_sugarsync_signup:I = 0x7f0a01d6

.field public static final joinsugar_terms_policy_agree:I = 0x7f0a0060

.field public static final joinsugar_vdisk_succeed:I = 0x7f0a0057

.field public static final linked_devices:I = 0x7f0a02c5

.field public static final linked_service:I = 0x7f0a02c6

.field public static final list_device_4:I = 0x7f0a0098

.field public static final list_device_4_1:I = 0x7f0a0099

.field public static final list_device_5:I = 0x7f0a00b2

.field public static final list_device_9:I = 0x7f0a00b3

.field public static final list_documents:I = 0x7f0a02cc

.field public static final list_file:I = 0x7f0a0096

.field public static final list_menu_select_all:I = 0x7f0a009b

.field public static final list_music:I = 0x7f0a0094

.field public static final list_no_date:I = 0x7f0a009d

.field public static final list_order_album:I = 0x7f0a0089

.field public static final list_order_artist:I = 0x7f0a008a

.field public static final list_order_artist_atoz:I = 0x7f0a0085

.field public static final list_order_artist_ztoa:I = 0x7f0a0086

.field public static final list_order_by_latest_date:I = 0x7f0a0106

.field public static final list_order_date:I = 0x7f0a0088

.field public static final list_order_file:I = 0x7f0a0090

.field public static final list_order_filesize:I = 0x7f0a008f

.field public static final list_order_filetype:I = 0x7f0a0091

.field public static final list_order_filetype_1:I = 0x7f0a009c

.field public static final list_order_genre:I = 0x7f0a008b

.field public static final list_order_location_a_z:I = 0x7f0a02b7

.field public static final list_order_name:I = 0x7f0a008d

.field public static final list_order_oldest:I = 0x7f0a008e

.field public static final list_order_recent:I = 0x7f0a0087

.field public static final list_order_recently_created:I = 0x7f0a01a0

.field public static final list_order_title:I = 0x7f0a008c

.field public static final list_order_ztoa:I = 0x7f0a0084

.field public static final list_photo:I = 0x7f0a0093

.field public static final list_search:I = 0x7f0a0092

.field public static final list_search_album_guide:I = 0x7f0a01ba

.field public static final list_search_artist_guide:I = 0x7f0a01b8

.field public static final list_search_file_guide:I = 0x7f0a01bc

.field public static final list_search_genre_guide:I = 0x7f0a01b9

.field public static final list_search_music_guide:I = 0x7f0a01b7

.field public static final list_search_pc_movie_guide:I = 0x7f0a01bb

.field public static final list_search_pc_photo_guide:I = 0x7f0a01b6

.field public static final list_songs:I = 0x7f0a009a

.field public static final list_sw_ver:I = 0x7f0a0097

.field public static final list_video:I = 0x7f0a0095

.field public static final listview:I = 0x7f0a0308

.field public static final loading_contents_file_list:I = 0x7f0a025a

.field public static final loading_file_list:I = 0x7f0a01ed

.field public static final loading_video:I = 0x7f0a023c

.field public static final loads_only_contents_do_not_show_again_notification:I = 0x7f0a0107

.field public static final loads_only_contents_do_not_show_again_notification_old:I = 0x7f0a0108

.field public static final local_media_digest_service_description:I = 0x7f0a0408

.field public static final local_media_digest_service_label:I = 0x7f0a0409

.field public static final local_media_rev_geo_loc_service_description:I = 0x7f0a040a

.field public static final local_media_rev_geo_loc_service_label:I = 0x7f0a040b

.field public static final location:I = 0x7f0a029c

.field public static final locked:I = 0x7f0a01be

.field public static final login1_subtitle:I = 0x7f0a02a2

.field public static final login1_subtitle2:I = 0x7f0a02a3

.field public static final login1_subtitle2_html:I = 0x7f0a016d

.field public static final login1_title:I = 0x7f0a02a1

.field public static final login2_subtitle:I = 0x7f0a02a5

.field public static final login2_title:I = 0x7f0a02a4

.field public static final login3_subtitle:I = 0x7f0a02a7

.field public static final login3_title:I = 0x7f0a02a6

.field public static final login5_subtitle:I = 0x7f0a02a9

.field public static final login5_title:I = 0x7f0a02a8

.field public static final login_lock:I = 0x7f0a004a

.field public static final login_lock_desc:I = 0x7f0a004c

.field public static final login_lock_setting:I = 0x7f0a004b

.field public static final main_add_device:I = 0x7f0a004e

.field public static final main_add_storage:I = 0x7f0a004f

.field public static final main_cert:I = 0x7f0a0054

.field public static final main_noti_try_again:I = 0x7f0a00b4

.field public static final main_noti_unsupported_file:I = 0x7f0a0217

.field public static final main_software_cancel:I = 0x7f0a00b1

.field public static final main_software_klite:I = 0x7f0a00c7

.field public static final main_update:I = 0x7f0a0050

.field public static final manual_install:I = 0x7f0a00c6

.field public static final max_char_reached:I = 0x7f0a02ed

.field public static final max_number_storages_reached:I = 0x7f0a0121

.field public static final message_mobile_data_is_disabled:I = 0x7f0a0251

.field public static final message_no_device:I = 0x7f0a00b0

.field public static final more_options:I = 0x7f0a03a8

.field public static final move_up:I = 0x7f0a0214

.field public static final msg_request_sp_sign:I = 0x7f0a00f6

.field public static final multi_play_error_in_use:I = 0x7f0a03bd

.field public static final multiple_source_selection:I = 0x7f0a03dc

.field public static final mum_sub_error:I = 0x7f0a0286

.field public static final music_album:I = 0x7f0a0100

.field public static final music_artist:I = 0x7f0a0101

.field public static final music_file_share_error:I = 0x7f0a03b5

.field public static final music_genre:I = 0x7f0a0102

.field public static final music_pause:I = 0x7f0a016e

.field public static final music_play:I = 0x7f0a016f

.field public static final music_store_link:I = 0x7f0a02c8

.field public static final music_tab:I = 0x7f0a0170

.field public static final music_title:I = 0x7f0a0171

.field public static final mycomputer:I = 0x7f0a00e1

.field public static final n_selected:I = 0x7f0a02ce

.field public static final name:I = 0x7f0a029d

.field public static final ndrive_japan_service_stop:I = 0x7f0a041f

.field public static final net_2g:I = 0x7f0a011a

.field public static final net_3g:I = 0x7f0a0119

.field public static final net_lte:I = 0x7f0a011c

.field public static final net_wifi:I = 0x7f0a011b

.field public static final network_is_currently_unavailable:I = 0x7f0a0254

.field public static final network_type_lte:I = 0x7f0a041d

.field public static final network_type_mobile:I = 0x7f0a041b

.field public static final network_type_mobile_2g:I = 0x7f0a041c

.field public static final network_type_off:I = 0x7f0a041e

.field public static final network_type_wifi:I = 0x7f0a0419

.field public static final network_type_wifi_china:I = 0x7f0a041a

.field public static final new_device_name:I = 0x7f0a0172

.field public static final new_password:I = 0x7f0a026f

.field public static final next:I = 0x7f0a01e5

.field public static final no_Location_filters:I = 0x7f0a03f9

.field public static final no_additional_storages_available:I = 0x7f0a01d8

.field public static final no_available_devices:I = 0x7f0a025b

.field public static final no_file_tranfers:I = 0x7f0a02bf

.field public static final no_location_filters:I = 0x7f0a0361

.field public static final no_network_airplane:I = 0x7f0a02fb

.field public static final no_network_connection:I = 0x7f0a0250

.field public static final no_network_data_limit:I = 0x7f0a02fe

.field public static final no_network_mobile_data_off:I = 0x7f0a02fc

.field public static final no_network_roaming:I = 0x7f0a02fd

.field public static final no_results_found_for:I = 0x7f0a02ca

.field public static final no_saved_content:I = 0x7f0a03ac

.field public static final no_search_result_for_filter:I = 0x7f0a03c8

.field public static final no_sendto_device_or_storage:I = 0x7f0a037a

.field public static final no_sendto_devices:I = 0x7f0a0173

.field public static final no_signal_found:I = 0x7f0a0242

.field public static final no_storage_pc:I = 0x7f0a0131

.field public static final not_connected:I = 0x7f0a03aa

.field public static final not_selected:I = 0x7f0a01c7

.field public static final not_sufficient_memory_for_sendto:I = 0x7f0a0174

.field public static final not_supported_app_delete:I = 0x7f0a03d3

.field public static final noti_playback_not_smooth:I = 0x7f0a012e

.field public static final noti_register_device:I = 0x7f0a03c0

.field public static final notification_bluetooth_available_error:I = 0x7f0a027d

.field public static final notification_bluetooth_detect_error:I = 0x7f0a025f

.field public static final notification_bluetooth_enable_error:I = 0x7f0a025e

.field public static final notification_bluetooth_user_busy:I = 0x7f0a0280

.field public static final notification_homesync_connection_error:I = 0x7f0a027e

.field public static final notification_homesync_deregistered:I = 0x7f0a0270

.field public static final notification_mirroring_error:I = 0x7f0a0289

.field public static final notification_password_characters_error:I = 0x7f0a027b

.field public static final notification_password_donotmatch_error:I = 0x7f0a0265

.field public static final notification_password_invalid_error:I = 0x7f0a0269

.field public static final notification_password_oldnewsame_error:I = 0x7f0a027c

.field public static final notification_password_recovery_error:I = 0x7f0a0281

.field public static final notification_password_server_error:I = 0x7f0a027f

.field public static final notification_password_verification_error:I = 0x7f0a026a

.field public static final notification_storage_provider:I = 0x7f0a01e9

.field public static final notification_title_transferred:I = 0x7f0a033e

.field public static final notification_title_transferring:I = 0x7f0a033f

.field public static final notification_token_verification_error:I = 0x7f0a026b

.field public static final notification_video_play_error_opt:I = 0x7f0a0282

.field public static final notifications_auto_upload_text:I = 0x7f0a0355

.field public static final notifications_service_info_text:I = 0x7f0a0356

.field public static final notify_url:I = 0x7f0a001d

.field public static final notify_version_url:I = 0x7f0a001c

.field public static final num_photos:I = 0x7f0a032d

.field public static final number_registered_devices_plural:I = 0x7f0a035f

.field public static final number_registered_devices_singular:I = 0x7f0a035e

.field public static final ok_2:I = 0x7f0a0206

.field public static final on_off_switch:I = 0x7f0a01bd

.field public static final one_selected:I = 0x7f0a02cf

.field public static final operate_mainenance_time:I = 0x7f0a00ea

.field public static final optimized:I = 0x7f0a01c1

.field public static final option_cancel:I = 0x7f0a0175

.field public static final option_change_display:I = 0x7f0a01e3

.field public static final option_change_private_password:I = 0x7f0a026d

.field public static final option_changedisplay:I = 0x7f0a012b

.field public static final option_changedisplay_button:I = 0x7f0a0176

.field public static final option_delete:I = 0x7f0a0104

.field public static final option_delete_button:I = 0x7f0a0105

.field public static final option_done:I = 0x7f0a0177

.field public static final option_download:I = 0x7f0a012f

.field public static final option_download_button:I = 0x7f0a0178

.field public static final option_editmode_button:I = 0x7f0a02eb

.field public static final option_jump_to_button:I = 0x7f0a0179

.field public static final option_menu_notices:I = 0x7f0a034a

.field public static final option_play_button:I = 0x7f0a017a

.field public static final option_select:I = 0x7f0a0319

.field public static final option_sendto:I = 0x7f0a017b

.field public static final option_sendto_button:I = 0x7f0a017c

.field public static final option_sharevia:I = 0x7f0a0130

.field public static final option_sharevia_button:I = 0x7f0a017d

.field public static final option_transfer_status:I = 0x7f0a029a

.field public static final password_confirmation:I = 0x7f0a017e

.field public static final password_required_field:I = 0x7f0a017f

.field public static final password_setup:I = 0x7f0a0260

.field public static final paused:I = 0x7f0a03cc

.field public static final pc_noti_del_perm:I = 0x7f0a034b

.field public static final permdesc_broadcast_samsung_link:I = 0x7f0a03f3

.field public static final permdesc_samsung_link_general_access:I = 0x7f0a03f5

.field public static final permlabel_broadcast_samsung_link:I = 0x7f0a03f2

.field public static final permlabel_samsung_link_general_access:I = 0x7f0a03f4

.field public static final photo_date_taken_grouping_day:I = 0x7f0a03ef

.field public static final photo_date_taken_grouping_month:I = 0x7f0a03ed

.field public static final photo_date_taken_grouping_year:I = 0x7f0a03ee

.field public static final photo_unable_to_load:I = 0x7f0a01f3

.field public static final photo_unknow_location:I = 0x7f0a0342

.field public static final photos_tab:I = 0x7f0a0180

.field public static final platform_install_failed:I = 0x7f0a03ca

.field public static final platform_update_force:I = 0x7f0a03c7

.field public static final platform_update_general:I = 0x7f0a03c6

.field public static final play_network_error:I = 0x7f0a028f

.field public static final play_no_content:I = 0x7f0a00a9

.field public static final play_no_content_del:I = 0x7f0a00aa

.field public static final play_noti_del_perm:I = 0x7f0a00ac

.field public static final play_noti_no_del:I = 0x7f0a00ae

.field public static final play_noti_noplay:I = 0x7f0a00ab

.field public static final playback_error_local:I = 0x7f0a01e1

.field public static final plus_register_storage:I = 0x7f0a01c8

.field public static final popup_rename:I = 0x7f0a0181

.field public static final press_back_to_close:I = 0x7f0a0238

.field public static final previous:I = 0x7f0a01e4

.field public static final previous_transfer_error_msg:I = 0x7f0a0246

.field public static final promotion_cancel_later:I = 0x7f0a0200

.field public static final promotion_dialog_link1_add:I = 0x7f0a01fc

.field public static final promotion_dialog_link2_add:I = 0x7f0a01fd

.field public static final promotion_dialog_success_link_question_add:I = 0x7f0a0204

.field public static final promotion_dialog_success_text1:I = 0x7f0a0201

.field public static final promotion_dialog_success_text2:I = 0x7f0a0202

.field public static final promotion_dialog_success_text3:I = 0x7f0a0203

.field public static final promotion_dialog_terms_dontagree:I = 0x7f0a0208

.field public static final promotion_dialog_terms_link:I = 0x7f0a0207

.field public static final promotion_dialog_text1_add:I = 0x7f0a01f6

.field public static final promotion_dialog_text2_add:I = 0x7f0a01fb

.field public static final promotion_dialog_text3:I = 0x7f0a01f7

.field public static final promotion_dialog_text4:I = 0x7f0a01f8

.field public static final promotion_dialog_text5:I = 0x7f0a01f9

.field public static final promotion_dialog_text6:I = 0x7f0a01fa

.field public static final promotion_enter:I = 0x7f0a01ff

.field public static final promotion_event_title:I = 0x7f0a0209

.field public static final promotion_title_add:I = 0x7f0a01f5

.field public static final provider_permission:I = 0x7f0a03fa

.field public static final purchase_storage_msg:I = 0x7f0a01e0

.field public static final quck_index_characters:I = 0x7f0a0182

.field public static final quick_panel_auto_upload_failed:I = 0x7f0a01a1

.field public static final quick_panel_auto_upload_no_storage:I = 0x7f0a01a2

.field public static final quick_panel_auto_upload_photo_video_unknown:I = 0x7f0a0337

.field public static final quick_panel_completed:I = 0x7f0a02c2

.field public static final quickpanel_auto_upload_text:I = 0x7f0a0358

.field public static final quickpanel_auto_upload_title:I = 0x7f0a0357

.field public static final quickpanel_transfer_cancel:I = 0x7f0a02f8

.field public static final quickpanel_transfer_completed:I = 0x7f0a02f7

.field public static final quickpanel_transfer_error:I = 0x7f0a02f9

.field public static final rcv_0000_msg:I = 0x7f0a040e

.field public static final rcv_0200_msg:I = 0x7f0a040f

.field public static final rcv_0201_msg:I = 0x7f0a0410

.field public static final rcv_0202_msg:I = 0x7f0a0411

.field public static final rcv_0203_msg:I = 0x7f0a0412

.field public static final rcv_0204_msg:I = 0x7f0a0413

.field public static final rcv_0210_msg:I = 0x7f0a0414

.field public static final re_enable_data:I = 0x7f0a0300

.field public static final recent_no_contents:I = 0x7f0a0183

.field public static final recent_view_music_player_no_network:I = 0x7f0a01db

.field public static final recv_start_msg:I = 0x7f0a00ec

.field public static final redirect_china_service_sign_in_again:I = 0x7f0a004d

.field public static final register_add_device:I = 0x7f0a0073

.field public static final register_auto_upload:I = 0x7f0a007c

.field public static final register_change_device:I = 0x7f0a007b

.field public static final register_change_device_vzw:I = 0x7f0a03df

.field public static final register_device_name:I = 0x7f0a0074

.field public static final register_device_name_setting:I = 0x7f0a0075

.field public static final register_error_reregister:I = 0x7f0a03b3

.field public static final register_error_reregister_button:I = 0x7f0a03b4

.field public static final register_external:I = 0x7f0a0078

.field public static final register_guide:I = 0x7f0a034d

.field public static final register_help_activity_title:I = 0x7f0a036e

.field public static final register_help_guide_number_format:I = 0x7f0a0418

.field public static final register_help_sub_title:I = 0x7f0a0417

.field public static final register_help_subtitle:I = 0x7f0a036c

.field public static final register_help_title:I = 0x7f0a036b

.field public static final register_information_storage_is_total_storage:I = 0x7f0a007a

.field public static final register_internal:I = 0x7f0a0077

.field public static final register_pc_help_text:I = 0x7f0a0184

.field public static final register_photo:I = 0x7f0a007f

.field public static final register_photo_movie:I = 0x7f0a0080

.field public static final register_photo_video:I = 0x7f0a0081

.field public static final register_save_loc:I = 0x7f0a0076

.field public static final register_save_loc_setting:I = 0x7f0a010b

.field public static final register_set_auto_upload:I = 0x7f0a0082

.field public static final register_show_total_space_storage:I = 0x7f0a0079

.field public static final register_status_auto_upload:I = 0x7f0a0083

.field public static final register_this_device_samsunglink:I = 0x7f0a0377

.field public static final register_this_device_samsunglink_global:I = 0x7f0a03e0

.field public static final register_this_phone_samsunglink_vzw:I = 0x7f0a03de

.field public static final register_title_mobile:I = 0x7f0a036f

.field public static final register_title_pc:I = 0x7f0a0370

.field public static final register_title_tv:I = 0x7f0a0371

.field public static final register_video:I = 0x7f0a02cb

.field public static final register_wifi:I = 0x7f0a007d

.field public static final register_wifi_3g_lte:I = 0x7f0a007e

.field public static final register_your_phone_vzw:I = 0x7f0a03e1

.field public static final registered_devices:I = 0x7f0a0194

.field public static final registered_storages:I = 0x7f0a011f

.field public static final remote_device_data_charge:I = 0x7f0a0185

.field public static final remote_music_device_not_connected:I = 0x7f0a01da

.field public static final remove_storage:I = 0x7f0a01d3

.field public static final reregister_sync_on:I = 0x7f0a03b1

.field public static final restart_all_action:I = 0x7f0a032e

.field public static final resume_playing:I = 0x7f0a01e6

.field public static final retry_all_action:I = 0x7f0a02bd

.field public static final samsung_account_disable:I = 0x7f0a0322

.field public static final samsung_hub_link:I = 0x7f0a02c7

.field public static final samsung_link_devices:I = 0x7f0a03ea

.field public static final samsung_tv:I = 0x7f0a0415

.field public static final samsung_tv_detect:I = 0x7f0a00d6

.field public static final samsungapps_url:I = 0x7f0a0257

.field public static final samsunglink_information:I = 0x7f0a024b

.field public static final save_to_text:I = 0x7f0a01c3

.field public static final screen_locked:I = 0x7f0a0240

.field public static final sd_card_cant_delete_plural:I = 0x7f0a03c2

.field public static final sd_card_cant_delete_singular:I = 0x7f0a03c3

.field public static final sdcard:I = 0x7f0a01c4

.field public static final search_clear_history:I = 0x7f0a03e5

.field public static final search_filter_apply:I = 0x7f0a0365

.field public static final search_filter_category:I = 0x7f0a0363

.field public static final search_filter_date:I = 0x7f0a0364

.field public static final search_filter_description:I = 0x7f0a0362

.field public static final search_filter_past_seven_days:I = 0x7f0a0368

.field public static final search_filter_past_thirty_days:I = 0x7f0a0367

.field public static final search_filter_today:I = 0x7f0a036a

.field public static final search_filter_yesterday:I = 0x7f0a0369

.field public static final search_history:I = 0x7f0a03e6

.field public static final search_main_string:I = 0x7f0a028e

.field public static final search_max_word_msg:I = 0x7f0a0366

.field public static final search_samsunglink_description:I = 0x7f0a03ad

.field public static final search_show_more_result:I = 0x7f0a0372

.field public static final search_word_list_delete:I = 0x7f0a03b6

.field public static final see_list:I = 0x7f0a0205

.field public static final select_a_device:I = 0x7f0a01e7

.field public static final select_a_device_to_receive_content:I = 0x7f0a03d0

.field public static final select_items:I = 0x7f0a03eb

.field public static final send_connect:I = 0x7f0a00a2

.field public static final send_connect_detail:I = 0x7f0a019c

.field public static final send_del_after_copy:I = 0x7f0a009f

.field public static final send_files:I = 0x7f0a00a1

.field public static final send_noti_maximum:I = 0x7f0a038b

.field public static final send_noti_shortage_device:I = 0x7f0a00a3

.field public static final send_noti_skip:I = 0x7f0a00a5

.field public static final send_noti_transfer_error:I = 0x7f0a00a4

.field public static final send_noti_transfer_max_msg:I = 0x7f0a01a3

.field public static final send_noti_transfer_maximum:I = 0x7f0a01ac

.field public static final send_receive_select:I = 0x7f0a033a

.field public static final send_select_target:I = 0x7f0a009e

.field public static final send_selected_file:I = 0x7f0a00a0

.field public static final send_to_description:I = 0x7f0a0301

.field public static final send_to_homesync:I = 0x7f0a0271

.field public static final sendto_file_info:I = 0x7f0a01f4

.field public static final sendto_load_files_dialog_error:I = 0x7f0a0215

.field public static final sendto_load_files_error:I = 0x7f0a0216

.field public static final sendto_non_wifi:I = 0x7f0a01dd

.field public static final setting_about:I = 0x7f0a00b6

.field public static final setting_autosave:I = 0x7f0a0186

.field public static final setting_del:I = 0x7f0a00ba

.field public static final setting_del2:I = 0x7f0a0187

.field public static final setting_del3:I = 0x7f0a00bd

.field public static final setting_del4:I = 0x7f0a0053

.field public static final setting_folder_saving:I = 0x7f0a00be

.field public static final setting_info_loading:I = 0x7f0a00c0

.field public static final setting_info_net_error:I = 0x7f0a00c3

.field public static final setting_msg_for_add_storage:I = 0x7f0a0116

.field public static final setting_no_reg:I = 0x7f0a00b7

.field public static final setting_no_reg_storage_non_plural:I = 0x7f0a0115

.field public static final setting_path:I = 0x7f0a0193

.field public static final setting_right:I = 0x7f0a0196

.field public static final setting_right2:I = 0x7f0a00b9

.field public static final setting_set:I = 0x7f0a00b5

.field public static final setting_signout:I = 0x7f0a00b8

.field public static final setting_storage_mobile1:I = 0x7f0a00bf

.field public static final setting_unknown:I = 0x7f0a00c5

.field public static final setting_up_net2:I = 0x7f0a00c1

.field public static final setting_up_target:I = 0x7f0a00bc

.field public static final setting_up_target2:I = 0x7f0a00c2

.field public static final setting_version:I = 0x7f0a0349

.field public static final setting_version_first:I = 0x7f0a0197

.field public static final setting_version_second:I = 0x7f0a0198

.field public static final setting_video_quality:I = 0x7f0a00c4

.field public static final settings_auto_archive:I = 0x7f0a0331

.field public static final settings_auto_upload:I = 0x7f0a01b2

.field public static final settings_customer_support:I = 0x7f0a0188

.field public static final settings_file_sharing:I = 0x7f0a020a

.field public static final settings_my_device_name:I = 0x7f0a01b0

.field public static final settings_my_storages:I = 0x7f0a01af

.field public static final settings_notifications:I = 0x7f0a0353

.field public static final settings_notifications_service_info:I = 0x7f0a0354

.field public static final settings_password_lock:I = 0x7f0a01b4

.field public static final settings_save_to:I = 0x7f0a01b1

.field public static final settings_storages_view_text:I = 0x7f0a01b5

.field public static final settings_turn_on_file_sharing_settings:I = 0x7f0a020b

.field public static final settings_video_body_text:I = 0x7f0a01c0

.field public static final settings_video_qual:I = 0x7f0a01b3

.field public static final share_noti_maximum:I = 0x7f0a03ab

.field public static final share_pass:I = 0x7f0a00a7

.field public static final share_select_device:I = 0x7f0a00a8

.field public static final share_storage:I = 0x7f0a00a6

.field public static final show_password:I = 0x7f0a0264

.field public static final signin_session_expired:I = 0x7f0a025c

.field public static final signup_email_hint:I = 0x7f0a0403

.field public static final signup_terms_and_conditions:I = 0x7f0a0189

.field public static final size:I = 0x7f0a024d

.field public static final size_in_mb:I = 0x7f0a0255

.field public static final smart_pause_enabled_toast_msg:I = 0x7f0a0243

.field public static final spc_delete_notice:I = 0x7f0a01ec

.field public static final start_video_over:I = 0x7f0a01ef

.field public static final starting_file_download:I = 0x7f0a018a

.field public static final status_canceled:I = 0x7f0a02ba

.field public static final status_canceling:I = 0x7f0a02b2

.field public static final status_complete:I = 0x7f0a02b1

.field public static final status_error:I = 0x7f0a02b9

.field public static final status_in_progress:I = 0x7f0a02b8

.field public static final status_pending:I = 0x7f0a02af

.field public static final status_sending:I = 0x7f0a02b0

.field public static final status_skipped:I = 0x7f0a0383

.field public static final stop_playing_appinapp:I = 0x7f0a0241

.field public static final storage_acct_already_registered:I = 0x7f0a0123

.field public static final storage_acct_email_domain_invalid:I = 0x7f0a0122

.field public static final storage_acct_not_yet_activiated:I = 0x7f0a0124

.field public static final storage_app_info:I = 0x7f0a01d9

.field public static final storage_autosave_photo:I = 0x7f0a006d

.field public static final storage_autosave_photo_video:I = 0x7f0a006c

.field public static final storage_autosave_setting:I = 0x7f0a0064

.field public static final storage_autosave_video:I = 0x7f0a006e

.field public static final storage_baidu_name:I = 0x7f0a0343

.field public static final storage_cloud_soon:I = 0x7f0a0072

.field public static final storage_device_setting:I = 0x7f0a0066

.field public static final storage_fail_connect:I = 0x7f0a0070

.field public static final storage_info:I = 0x7f0a0127

.field public static final storage_info_size_label:I = 0x7f0a0128

.field public static final storage_is_full:I = 0x7f0a01ae

.field public static final storage_logout:I = 0x7f0a018b

.field public static final storage_mobile_autosave_off:I = 0x7f0a0067

.field public static final storage_mobile_autosave_photo:I = 0x7f0a0068

.field public static final storage_mobile_autosave_video:I = 0x7f0a0069

.field public static final storage_mobile_save_pc:I = 0x7f0a006a

.field public static final storage_mobile_save_pc_video:I = 0x7f0a006b

.field public static final storage_ndrive_agree:I = 0x7f0a020e

.field public static final storage_ndrive_agree_jp:I = 0x7f0a020f

.field public static final storage_no_account_join:I = 0x7f0a018c

.field public static final storage_no_reg_save_loc:I = 0x7f0a010a

.field public static final storage_no_use:I = 0x7f0a006f

.field public static final storage_pc:I = 0x7f0a0350

.field public static final storage_reg:I = 0x7f0a0109

.field public static final storage_service_sign_out_dialog:I = 0x7f0a0120

.field public static final storage_service_sign_out_dialog_with_autoupload:I = 0x7f0a0114

.field public static final storage_service_sign_out_dialog_with_autoupload_and_file_xfer:I = 0x7f0a0112

.field public static final storage_service_sign_out_dialog_with_file_xfer_in_progress:I = 0x7f0a0113

.field public static final storage_sign_in_register_storage:I = 0x7f0a01d4

.field public static final storage_signin:I = 0x7f0a018d

.field public static final storage_signin_email_or_pw_incorrect_error:I = 0x7f0a019e

.field public static final storage_signup:I = 0x7f0a018e

.field public static final storage_signup_successfully_completed:I = 0x7f0a0125

.field public static final storage_signup_welcome_title:I = 0x7f0a0126

.field public static final storage_sugarsync:I = 0x7f0a0071

.field public static final storage_unregistered:I = 0x7f0a0352

.field public static final storage_web_select:I = 0x7f0a0065

.field public static final storeat:I = 0x7f0a030a

.field public static final sugar_sync_account_prompt:I = 0x7f0a01e8

.field public static final sugarsync_storage_service:I = 0x7f0a0404

.field public static final sva_receive_fail_full:I = 0x7f0a00fe

.field public static final sva_send_fail_full:I = 0x7f0a00fd

.field public static final sva_send_multi_cancel:I = 0x7f0a00fb

.field public static final sva_send_multi_complete:I = 0x7f0a00f9

.field public static final sva_send_multi_fail:I = 0x7f0a00fa

.field public static final sva_send_multi_running:I = 0x7f0a00f8

.field public static final sva_send_single_wait:I = 0x7f0a00fc

.field public static final sva_uploader_device_invalid:I = 0x7f0a00ff

.field public static final sva_uploader_full_local_storage:I = 0x7f0a00f2

.field public static final sva_uploader_no_samsung_account:I = 0x7f0a00f3

.field public static final sva_uploader_samsung_account_removed:I = 0x7f0a00f5

.field public static final sva_uploader_storage_filesize_exceed:I = 0x7f0a00f1

.field public static final sva_uploader_storage_full:I = 0x7f0a00f0

.field public static final sva_uploader_storage_not_signin:I = 0x7f0a00f4

.field public static final svu_uploader_samsung_account_removed:I = 0x7f0a01f0

.field public static final swipe_toast_devices:I = 0x7f0a01a7

.field public static final swipe_toast_recent_contents:I = 0x7f0a01a6

.field public static final sync_subtitles_menu:I = 0x7f0a02aa

.field public static final sync_subtitles_sec:I = 0x7f0a02ab

.field public static final system_notification_title:I = 0x7f0a01dc

.field public static final talkback_checkbox:I = 0x7f0a022e

.field public static final talkback_disabled:I = 0x7f0a0223

.field public static final talkback_dropdown_list:I = 0x7f0a0232

.field public static final talkback_edit_box:I = 0x7f0a022b

.field public static final talkback_editing:I = 0x7f0a022c

.field public static final talkback_fit_to_screen:I = 0x7f0a0229

.field public static final talkback_list:I = 0x7f0a0226

.field public static final talkback_music_ff:I = 0x7f0a0236

.field public static final talkback_music_repeat:I = 0x7f0a02ea

.field public static final talkback_music_rew:I = 0x7f0a0235

.field public static final talkback_music_shuffle:I = 0x7f0a02e9

.field public static final talkback_not_checked:I = 0x7f0a022f

.field public static final talkback_not_selected:I = 0x7f0a0233

.field public static final talkback_popup_play:I = 0x7f0a022a

.field public static final talkback_repeat_1:I = 0x7f0a0311

.field public static final talkback_repeat_all:I = 0x7f0a0310

.field public static final talkback_repeat_off:I = 0x7f0a030f

.field public static final talkback_selected:I = 0x7f0a0224

.field public static final talkback_shuffle_off:I = 0x7f0a0312

.field public static final talkback_shuffle_on:I = 0x7f0a0313

.field public static final talkback_tab:I = 0x7f0a022d

.field public static final talkback_tickbox:I = 0x7f0a0220

.field public static final talkback_ticked:I = 0x7f0a0222

.field public static final talkback_title:I = 0x7f0a0225

.field public static final talkback_toggle:I = 0x7f0a0230

.field public static final talkback_toggle_button:I = 0x7f0a0231

.field public static final talkback_unticked:I = 0x7f0a0221

.field public static final talkback_video_rotate:I = 0x7f0a0314

.field public static final talkback_video_zoom_change:I = 0x7f0a0234

.field public static final talkback_volume:I = 0x7f0a0227

.field public static final talkback_volume_mute:I = 0x7f0a0228

.field public static final talkbaclk_button:I = 0x7f0a021f

.field public static final temp_code_001:I = 0x7f0a0359

.field public static final temp_code_002:I = 0x7f0a0360

.field public static final temp_code_003:I = 0x7f0a0376

.field public static final temp_code_006:I = 0x7f0a0379

.field public static final temp_code_008:I = 0x7f0a037b

.field public static final temp_code_009:I = 0x7f0a037c

.field public static final temp_code_010:I = 0x7f0a037d

.field public static final temp_code_011:I = 0x7f0a037e

.field public static final temp_code_012:I = 0x7f0a037f

.field public static final temp_code_013:I = 0x7f0a0381

.field public static final temp_code_014:I = 0x7f0a0382

.field public static final temp_code_016:I = 0x7f0a0384

.field public static final temp_code_017:I = 0x7f0a0385

.field public static final temp_code_018:I = 0x7f0a0386

.field public static final temp_code_019:I = 0x7f0a0387

.field public static final temp_code_020:I = 0x7f0a0388

.field public static final temp_code_021:I = 0x7f0a0389

.field public static final temp_code_022:I = 0x7f0a038a

.field public static final temp_code_024:I = 0x7f0a038c

.field public static final temp_code_025:I = 0x7f0a038d

.field public static final temp_code_027:I = 0x7f0a038f

.field public static final temp_code_028:I = 0x7f0a0390

.field public static final temp_code_030:I = 0x7f0a0392

.field public static final temp_code_034:I = 0x7f0a0393

.field public static final terms_conditions_update_detail:I = 0x7f0a03da

.field public static final terms_conditions_update_title:I = 0x7f0a03d8

.field public static final threebox_error_from_remote:I = 0x7f0a031c

.field public static final threebox_error_from_remote_bd:I = 0x7f0a031e

.field public static final threebox_error_storage_to_storage:I = 0x7f0a0321

.field public static final threebox_error_to_remote:I = 0x7f0a031b

.field public static final threebox_error_to_remote_bd:I = 0x7f0a031d

.field public static final title_my_recent_contents:I = 0x7f0a0118

.field public static final title_my_recent_contents_plural:I = 0x7f0a01e2

.field public static final title_storages_devices:I = 0x7f0a0117

.field public static final trans_start_msg:I = 0x7f0a00eb

.field public static final transfer_cancel_confirm:I = 0x7f0a034e

.field public static final transfer_cancel_title_msg:I = 0x7f0a00ef

.field public static final transfer_complete_title_msg:I = 0x7f0a00ed

.field public static final transfer_error:I = 0x7f0a033c

.field public static final transfer_error_file_size_exceeds:I = 0x7f0a0247

.field public static final transfer_error_file_size_exceeds_upload:I = 0x7f0a024a

.field public static final transfer_error_title_msg:I = 0x7f0a00ee

.field public static final transfer_skipped:I = 0x7f0a03ce

.field public static final transfer_title:I = 0x7f0a02b3

.field public static final transferring_cancel_message:I = 0x7f0a0135

.field public static final transferring_complete_message:I = 0x7f0a0133

.field public static final transferring_error_message:I = 0x7f0a0134

.field public static final transferring_message:I = 0x7f0a0132

.field public static final transferring_status_plurar:I = 0x7f0a03be

.field public static final transferring_status_singular:I = 0x7f0a03bf

.field public static final tutorial:I = 0x7f0a00d7

.field public static final tutorial18_allcontents_text1:I = 0x7f0a02f6

.field public static final tutorial18_more_text1:I = 0x7f0a02f5

.field public static final tutorial_allcontents_text1:I = 0x7f0a02e7

.field public static final tutorial_allcontents_text2:I = 0x7f0a02e8

.field public static final tutorial_con1_text1:I = 0x7f0a02e1

.field public static final tutorial_con1_text2:I = 0x7f0a02e2

.field public static final tutorial_con2_text1:I = 0x7f0a02e3

.field public static final tutorial_con2_text2:I = 0x7f0a02e4

.field public static final tutorial_con3_text1:I = 0x7f0a02e5

.field public static final tutorial_con3_text2:I = 0x7f0a02e6

.field public static final tutorial_drawer1_text1:I = 0x7f0a02db

.field public static final tutorial_drawer1_text2:I = 0x7f0a02dc

.field public static final tutorial_drawer1_text3:I = 0x7f0a02dd

.field public static final tutorial_guide_for_delete:I = 0x7f0a0394

.field public static final tutorial_guide_for_play:I = 0x7f0a0378

.field public static final tutorial_menukey_text1:I = 0x7f0a02ef

.field public static final tutorial_menukey_text2:I = 0x7f0a02f0

.field public static final tutorial_menukey_text3:I = 0x7f0a02f1

.field public static final tutorial_more_text1:I = 0x7f0a02de

.field public static final tutorial_more_text2:I = 0x7f0a02df

.field public static final tutorial_more_text3:I = 0x7f0a02e0

.field public static final tv_found_help_text:I = 0x7f0a021e

.field public static final tv_help_text1:I = 0x7f0a01a9

.field public static final tv_help_text2:I = 0x7f0a01aa

.field public static final tv_help_title:I = 0x7f0a01a8

.field public static final tv_jump_to_settings:I = 0x7f0a01ab

.field public static final uhd_no_streaming:I = 0x7f0a03c5

.field public static final unable_play_codec_error:I = 0x7f0a03b8

.field public static final unable_play_no_resource:I = 0x7f0a03b7

.field public static final unable_to_attach_file_sign_in:I = 0x7f0a0210

.field public static final unable_to_play_due_to_unsupported_file_type:I = 0x7f0a020d

.field public static final unable_to_send_file_device_deregistered:I = 0x7f0a03cf

.field public static final unable_to_send_file_sign_in:I = 0x7f0a0276

.field public static final unable_to_sign_in:I = 0x7f0a03bc

.field public static final unavailable_update:I = 0x7f0a0252

.field public static final unknown_album:I = 0x7f0a0139

.field public static final unknown_artist:I = 0x7f0a0138

.field public static final unknown_delete_result:I = 0x7f0a019d

.field public static final unknown_genre:I = 0x7f0a01f2

.field public static final unknown_location:I = 0x7f0a02cd

.field public static final unlocked:I = 0x7f0a01bf

.field public static final unselect_all:I = 0x7f0a02a0

.field public static final update:I = 0x7f0a03c9

.field public static final update_in_progress:I = 0x7f0a03cb

.field public static final use_after_set_storage:I = 0x7f0a018f

.field public static final use_mobile_data:I = 0x7f0a01c5

.field public static final userguide_how_help_text1:I = 0x7f0a030c

.field public static final userguide_how_help_text2:I = 0x7f0a030d

.field public static final userguide_how_help_text3:I = 0x7f0a030e

.field public static final userguide_how_help_title:I = 0x7f0a030b

.field public static final verify_samsung_account:I = 0x7f0a03d9

.field public static final video_pause:I = 0x7f0a0190

.field public static final video_play:I = 0x7f0a0191

.field public static final video_store_link:I = 0x7f0a02c9

.field public static final video_tab:I = 0x7f0a0192

.field public static final view_less:I = 0x7f0a03e8

.field public static final view_more:I = 0x7f0a03e7

.field public static final wait_samsunglink_db_toast_msg:I = 0x7f0a0336

.field public static final warning:I = 0x7f0a0306

.field public static final wifi_not_connected_wifi_only:I = 0x7f0a0326

.field public static final windows_phone_noti:I = 0x7f0a02f3

.field public static final wlan_connection:I = 0x7f0a0328

.field public static final wlan_connection_permission:I = 0x7f0a0327

.field public static final wlan_no_connection:I = 0x7f0a032c

.field public static final wlan_not_available:I = 0x7f0a032a

.field public static final wlan_turned_off:I = 0x7f0a032b

.field public static final wlan_unable_connect:I = 0x7f0a0329

.field public static final year_format:I = 0x7f0a023b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
