.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;
.super Landroid/graphics/drawable/BitmapDrawable;
.source "SourceFile"


# instance fields
.field private final mOrientation:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 21
    iput p3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->mOrientation:I

    .line 22
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 29
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->mOrientation:I

    rem-int/lit16 v0, v0, 0x168

    if-nez v0, :cond_0

    .line 30
    invoke-super {p0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 47
    :goto_0
    return-void

    .line 34
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    .line 38
    neg-float v3, v1

    neg-float v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 39
    iget v3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->mOrientation:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 40
    iget v3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->mOrientation:I

    rem-int/lit16 v3, v3, 0xb4

    if-nez v3, :cond_1

    .line 41
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 46
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_1
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->mOrientation:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    .line 67
    invoke-super {p0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    .line 69
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;->mOrientation:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    .line 55
    invoke-super {p0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method
