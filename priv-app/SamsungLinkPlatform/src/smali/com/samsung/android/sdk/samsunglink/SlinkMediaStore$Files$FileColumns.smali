.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files$FileColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$GeolocationColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$MediaColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileColumns"
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final ARTIST_ID:Ljava/lang/String; = "artist_id"

.field public static final BOOKMARK:Ljava/lang/String; = "bookmark"

.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final CAPTION_TYPE:Ljava/lang/String; = "caption_type"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final COMPOSER:Ljava/lang/String; = "composer"

.field public static final DATE_TAKEN:Ljava/lang/String; = "datetaken"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final GROUP_ID:Ljava/lang/String; = "group_id"

.field public static final IS_ALARM:Ljava/lang/String; = "is_alarm"

.field public static final IS_MUSIC:Ljava/lang/String; = "is_music"

.field public static final IS_NOTIFICATION:Ljava/lang/String; = "is_notification"

.field public static final IS_PERSONAL:Ljava/lang/String; = "is_personal"

.field public static final IS_PLAYED:Ljava/lang/String; = "isPlayed"

.field public static final IS_PODCAST:Ljava/lang/String; = "is_podcast"

.field public static final IS_PRIVATE:Ljava/lang/String; = "isprivate"

.field public static final IS_RINGTONE:Ljava/lang/String; = "is_ringtone"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MEDIA_TYPE_ALBUM:I = 0xc

.field public static final MEDIA_TYPE_ARTIST:I = 0xd

.field public static final MEDIA_TYPE_AUDIO:I = 0x2

.field public static final MEDIA_TYPE_DOCUMENT:I = 0xf

.field public static final MEDIA_TYPE_GENRE:I = 0xe

.field public static final MEDIA_TYPE_IMAGE:I = 0x1

.field public static final MEDIA_TYPE_NONE:I = 0x0

.field public static final MEDIA_TYPE_VIDEO:I = 0x3

.field public static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final ORIENTATION:Ljava/lang/String; = "orientation"

.field public static final PICASA_ID:Ljava/lang/String; = "picasa_id"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field public static final RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final THUMB_DATA:Ljava/lang/String; = "thumb_data"

.field public static final TITLE_KEY:Ljava/lang/String; = "title_key"

.field public static final TRACK:Ljava/lang/String; = "track"

.field public static final YEAR:Ljava/lang/String; = "year"
