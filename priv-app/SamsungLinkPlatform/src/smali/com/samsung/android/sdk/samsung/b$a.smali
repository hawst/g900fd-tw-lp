.class final Lcom/samsung/android/sdk/samsung/b$a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsung/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 474
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 475
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 479
    const-string v0, "CREATE TABLE cache_table(KEY TEXT PRIMARY KEY, updated_time INTEGER, checked_time INTEGER, last_modified_time INTEGER, asp_id INTEGER, filename TEXT, original_size INTEGER, cached_size INTEGER, original_width INTEGER, original_height INTEGER, original_orientation INTEGER, mime_type TEXT, media_type INTEGER, device_id INTEGER, uri TEXT, uri_param TEXT, name TEXT, etc TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 484
    const-string v0, "DROP TABLE IF EXISTS cache_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 485
    const-string v0, "CREATE TABLE cache_table(KEY TEXT PRIMARY KEY, updated_time INTEGER, checked_time INTEGER, last_modified_time INTEGER, asp_id INTEGER, filename TEXT, original_size INTEGER, cached_size INTEGER, original_width INTEGER, original_height INTEGER, original_orientation INTEGER, mime_type TEXT, media_type INTEGER, device_id INTEGER, uri TEXT, uri_param TEXT, name TEXT, etc TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 486
    return-void
.end method
