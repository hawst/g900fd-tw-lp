.class public Lcom/tmi/transcode/Transcoder;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public static a()I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 25
    const-string v1, "/system/lib/libtranscoder_client.so"

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 26
    const-string v1, "Transcoder"

    const-string v2, ">>>>>>Transcoder NOT Found!"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :goto_0
    return v0

    .line 28
    :cond_0
    const-string v1, "Transcoder"

    const-string v2, ">>>>>>Transcoder Found! Now trying to load..."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    packed-switch v1, :pswitch_data_0

    .line 52
    :pswitch_0
    const-string v1, "Transcoder"

    const-string v2, ">>>>>>Transcoder Not Supported on this Android version!"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v1

    .line 58
    const-string v2, "Transcoder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::could not load Transcoder library: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/UnsatisfiedLinkError;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 37
    :pswitch_1
    :try_start_1
    const-string v1, "SLPF_transcoder_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 55
    :goto_1
    const-string v1, "transcoder_client"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x0

    goto :goto_0

    .line 41
    :pswitch_2
    const-string v1, "SLPF_transcoder_jni_jb2"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1

    .line 45
    :pswitch_3
    const-string v1, "SLPF_transcoder_jni_kk"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1

    .line 48
    :pswitch_4
    const-string v1, "SLPF_transcoder_jni_ll"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 31
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public native create()I
.end method

.method public native getTranscodingBlockSize()I
.end method

.method public native getTranscodingCurOffset()I
.end method

.method public native getTranscodingPercent()I
.end method

.method public native getTranscodingTotalSize()I
.end method

.method public native release()I
.end method

.method public native seek(II)I
.end method

.method public native setParam(IIIILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native start()I
.end method

.method public native stop()I
.end method
