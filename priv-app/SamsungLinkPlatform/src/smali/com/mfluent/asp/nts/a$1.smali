.class final Lcom/mfluent/asp/nts/a$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/nts/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/nts/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/nts/a;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 103
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-static {v0}, Lcom/mfluent/asp/nts/a;->a(Lcom/mfluent/asp/nts/a;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 104
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-static {v2}, Lcom/mfluent/asp/nts/a;->a(Lcom/mfluent/asp/nts/a;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 109
    :goto_0
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v2

    .line 110
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "networkStateChangedListener::onReceive: Inactivity Timer: {}/{} NTSLockService.isBound(): {}"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-wide v8, Lcom/mfluent/asp/nts/a;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "networkStateChangedListener::onReceive: updating wifi lock state"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 117
    iget-object v3, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-static {v3}, Lcom/mfluent/asp/nts/a;->b(Lcom/mfluent/asp/nts/a;)V

    .line 119
    sget-wide v4, Lcom/mfluent/asp/nts/a;->f:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    if-eqz v2, :cond_1

    .line 120
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Need to try to stay up"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "networkStateChangedListener::onReceive: core already initialized - updating presence"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/a;->d()V

    .line 133
    :cond_1
    :goto_1
    return-void

    .line 106
    :cond_2
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 125
    :cond_3
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq v0, v1, :cond_4

    .line 127
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "networkStateChangedListener::onReceive: initializing core in background "

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$1;->a:Lcom/mfluent/asp/nts/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/a;->h()V

    goto :goto_1

    .line 130
    :cond_4
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "networkStateChangedListener::onReceive: not initializing core because network mode is OFF or not signed in."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method
