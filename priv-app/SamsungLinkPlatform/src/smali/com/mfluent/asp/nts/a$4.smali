.class final Lcom/mfluent/asp/nts/a$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/nts/a;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/nts/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/nts/a;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/mfluent/asp/nts/a$4;->a:Lcom/mfluent/asp/nts/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 211
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$4;->a:Lcom/mfluent/asp/nts/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 218
    invoke-static {}, Lcom/mfluent/asp/ASPApplication;->f()Z

    move-result v0

    .line 219
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v1

    .line 220
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "NTSKeepAliveCheck::run: isForeground: {} audioPlayer. NTSLockService.isBound(): {}"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 222
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 227
    :cond_2
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 228
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 229
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 230
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_3

    .line 232
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    const-string v1, "api/pCloud/device/connection/keepAlive"

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/nts/b;->c(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 237
    :catch_0
    move-exception v1

    .line 238
    :try_start_2
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "NTSKeepAliveCheck::run: Trouble sending keepalive to {}, {}"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v5, v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 241
    :catch_1
    move-exception v0

    .line 242
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "NTSKeepAliveCheck::run: Trouble sending keepalive to {}"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
