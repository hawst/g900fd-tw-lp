.class public Lcom/mfluent/asp/nts/NonOkHttpResponseException;
.super Ljava/io/IOException;
.source "SourceFile"


# static fields
.field private static final serialVersionUID:J = -0x17bf640af88e9d1cL


# instance fields
.field private final responseBody:Ljava/lang/String;

.field private final responseStatusCode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 17
    iput p2, p0, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->responseStatusCode:I

    .line 18
    iput-object p3, p0, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->responseBody:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->responseStatusCode:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->responseBody:Ljava/lang/String;

    return-object v0
.end method
