.class public Lcom/mfluent/asp/nts/a;
.super Lpcloud/net/nat/c;
.source "SourceFile"


# static fields
.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:J

.field private static final j:Lorg/slf4j/Logger;

.field private static final m:J

.field private static final n:J

.field private static final o:J


# instance fields
.field final a:C

.field final b:C

.field private k:J

.field private final l:Ljava/util/concurrent/ScheduledExecutorService;

.field private final p:Landroid/content/BroadcastReceiver;

.field private q:Z

.field private final r:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xa

    .line 63
    const-class v0, Lcom/mfluent/asp/nts/a;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 65
    sput-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v1, "LifecycleManagingNetworkTraversal static init"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/nts/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".NTS_CONNECT_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/nts/a;->c:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/nts/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".PEER_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/nts/a;->d:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/nts/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".PRESENCE_CONNECTED_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/nts/a;->e:Ljava/lang/String;

    .line 84
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/nts/a;->f:J

    .line 86
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/nts/a;->m:J

    .line 89
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/nts/a;->n:J

    .line 91
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/nts/a;->o:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 155
    invoke-direct {p0, p1}, Lpcloud/net/nat/c;-><init>(Landroid/content/Context;)V

    .line 68
    iput-char v1, p0, Lcom/mfluent/asp/nts/a;->a:C

    .line 69
    const/4 v0, 0x2

    iput-char v0, p0, Lcom/mfluent/asp/nts/a;->b:C

    .line 82
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/nts/a;->l:Ljava/util/concurrent/ScheduledExecutorService;

    .line 96
    new-instance v0, Lcom/mfluent/asp/nts/a$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/nts/a$1;-><init>(Lcom/mfluent/asp/nts/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/nts/a;->p:Landroid/content/BroadcastReceiver;

    .line 136
    iput-boolean v1, p0, Lcom/mfluent/asp/nts/a;->q:Z

    .line 137
    new-instance v0, Lcom/mfluent/asp/nts/a$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/nts/a$2;-><init>(Lcom/mfluent/asp/nts/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/nts/a;->r:Landroid/content/ServiceConnection;

    .line 156
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->l:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/mfluent/asp/nts/a$3;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/nts/a$3;-><init>(Lcom/mfluent/asp/nts/a;)V

    sget-wide v2, Lcom/mfluent/asp/nts/a;->m:J

    sget-wide v4, Lcom/mfluent/asp/nts/a;->m:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 206
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->l:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/mfluent/asp/nts/a$4;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/nts/a$4;-><init>(Lcom/mfluent/asp/nts/a;)V

    sget-wide v2, Lcom/mfluent/asp/nts/a;->o:J

    sget-wide v4, Lcom/mfluent/asp/nts/a;->o:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 247
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/nts/a;->p:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/a/a;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 250
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/nts/a;)J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/mfluent/asp/nts/a;->k:J

    return-wide v0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 534
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    invoke-interface {v0, p0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 535
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/nts/a;)V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->k()V

    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/nts/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/mfluent/asp/nts/a;)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/nts/a;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/mfluent/asp/nts/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic f()J
    .locals 2

    .prologue
    .line 61
    sget-wide v0, Lcom/mfluent/asp/nts/a;->n:J

    return-wide v0
.end method


# virtual methods
.method public final a(Lpcloud/net/nat/a;[BIIZ)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 301
    if-eqz p5, :cond_0

    .line 302
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/nts/a;->k:J

    .line 305
    :cond_0
    invoke-super/range {p0 .. p5}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/a;[BIIZ)I

    move-result v0

    return v0
.end method

.method public final a(Lpcloud/net/nat/a;[BIIZZ)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    if-eqz p6, :cond_0

    .line 311
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/nts/a;->k:J

    .line 314
    :cond_0
    invoke-super/range {p0 .. p6}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/a;[BIIZZ)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;II)Lpcloud/net/nat/a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->b()V

    .line 256
    invoke-super {p0, p1, p2, p3}, Lpcloud/net/nat/c;->a(Ljava/lang/String;II)Lpcloud/net/nat/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 257
    :catch_0
    move-exception v0

    .line 258
    iget-object v1, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/mfluent/asp/nts/a;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 259
    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v1, "::terminateCore: Terminating NTS core"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 296
    invoke-super {p0}, Lpcloud/net/nat/c;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    monitor-exit p0

    return-void

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/msc/seclib/PeerInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->b()V

    .line 278
    invoke-super {p0, p1}, Lpcloud/net/nat/c;->a(Ljava/util/Vector;)V

    .line 279
    return-void
.end method

.method public final declared-synchronized a(ZZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    const-string v0, "::initialize():NTS is already initialized"

    invoke-static {v0}, Lcom/mfluent/asp/nts/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    :goto_0
    monitor-exit p0

    return-void

    .line 335
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/util/t;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/t;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/util/t;->a(I)Lcom/mfluent/asp/util/t$d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 338
    :try_start_2
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    .line 339
    invoke-virtual {v0}, Lcom/mfluent/asp/a;->b()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    .line 343
    if-nez v0, :cond_1

    .line 344
    const-string v0, "::initialize(): returning authInfo is null"

    invoke-static {v0}, Lcom/mfluent/asp/nts/a;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 470
    :try_start_3
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 348
    :cond_1
    :try_start_4
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 350
    if-nez p1, :cond_2

    .line 351
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v3

    .line 352
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v4}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Ljava/util/List;

    move-result-object v4

    .line 353
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 354
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v2, "::initialize: Not initalizing because there are no remote SLINK devices and no tv devices"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 470
    :try_start_5
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 359
    :cond_2
    :try_start_6
    sget-object v3, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v4, "::initialize: Initializing NTS core"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 361
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 362
    if-nez v2, :cond_3

    .line 363
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t initialize NTS when there is no local device"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 470
    :catchall_1
    move-exception v0

    :try_start_7
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 365
    :cond_3
    :try_start_8
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    .line 367
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 368
    new-instance v0, Ljava/io/IOException;

    const-string v2, "No local device peer id"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :cond_4
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v3

    .line 376
    new-instance v4, Lcom/msc/seclib/CoreConfig;

    invoke-direct {v4}, Lcom/msc/seclib/CoreConfig;-><init>()V

    .line 377
    invoke-virtual {v4, v3}, Lcom/msc/seclib/CoreConfig;->setGroup_id(Ljava/lang/String;)V

    .line 378
    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setPeer_id(Ljava/lang/String;)V

    .line 381
    sget-object v5, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::initialize: NTS login-"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 383
    const/16 v2, 0x13ba

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setService_port(C)V

    .line 384
    invoke-static {}, Lcom/sec/pcw/util/e;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 385
    const/4 v2, 0x5

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setLog_level(I)V

    .line 386
    const-string v2, "/mnt/sdcard/AllSharePlayLog"

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setLog_path(Ljava/lang/String;)V

    .line 392
    :goto_1
    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setCtimeout_sec_tcp(I)V

    .line 393
    const/4 v2, 0x3

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setCtimeout_sec_udp(I)V

    .line 394
    const/16 v2, 0xa

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setCtimeout_sec_turn(I)V

    .line 395
    const/4 v2, 0x3

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setCtimeout_sec_plain_udp(I)V

    .line 397
    const-string v2, "c7hc8m4900"

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setAppid(Ljava/lang/String;)V

    .line 399
    const-string v2, ""

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setApp_secret(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v4, v3}, Lcom/msc/seclib/CoreConfig;->setGuid(Ljava/lang/String;)V

    .line 402
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->j()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 403
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setAuth_type(C)V

    .line 404
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setToken(Ljava/lang/String;)V

    .line 406
    const-string v2, ""

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setToken_secret(Ljava/lang/String;)V

    .line 413
    :goto_2
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setEmail(Ljava/lang/String;)V

    .line 416
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->g()I

    move-result v2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setMcc(I)V

    .line 417
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setCc(Ljava/lang/String;)V

    .line 423
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setFwk_target(C)V

    .line 428
    invoke-static {}, Lcom/sec/pcw/util/Common;->a()Lcom/sec/pcw/util/Common$ServerType;

    move-result-object v0

    .line 429
    const-string v2, "SL_STAGING"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "serverType : "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    sget-object v2, Lcom/sec/pcw/util/Common$ServerType;->b:Lcom/sec/pcw/util/Common$ServerType;

    invoke-virtual {v0, v2}, Lcom/sec/pcw/util/Common$ServerType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 431
    const-string v0, "SL_STAGING"

    const-string v2, "setServer_type : 1"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setServer_type(C)V

    .line 438
    :goto_3
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 439
    const-string v2, "slpf_msc_qa_20"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 440
    const-string v2, "OverrideSCSServerTypeToStaging"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 441
    if-eqz v0, :cond_5

    .line 442
    sget-object v2, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "MSC QA: Overring SCS Server_Type Staging because OverrideSCSServerTypeToStaging is set to "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 443
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setServer_type(C)V

    .line 446
    :cond_5
    const-string v0, "c7hc8m4900"

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setInstance_id(Ljava/lang/String;)V

    .line 447
    const/16 v0, 0x235a

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setProxy_port_instance(C)V

    .line 449
    if-eqz p2, :cond_6

    .line 450
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mfluent/asp/nts/a;->k:J

    .line 453
    :cond_6
    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 456
    :try_start_9
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/nts/a;->a(Lcom/msc/seclib/CoreConfig;)V
    :try_end_9
    .catch Ljava/net/ConnectException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 464
    :try_start_a
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v3, "nts initialized"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 467
    const-string v0, "SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 468
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 470
    :try_start_b
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 388
    :cond_7
    const/4 v2, 0x1

    :try_start_c
    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setLog_level(I)V

    .line 389
    const-string v2, "/dev/null"

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setLog_path(Ljava/lang/String;)V

    .line 390
    new-instance v2, Ljava/io/File;

    const-string v5, "/mnt/sdcard/AllSharePlayLog"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mfluent/asp/util/b;->a(Ljava/io/File;)V

    goto/16 :goto_1

    .line 408
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setAuth_type(C)V

    .line 409
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setToken(Ljava/lang/String;)V

    .line 410
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/msc/seclib/CoreConfig;->setToken_secret(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 434
    :cond_9
    const-string v0, "SL_STAGING"

    const-string v2, "setServer_type : 0"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/msc/seclib/CoreConfig;->setServer_type(C)V

    goto/16 :goto_3

    .line 457
    :catch_0
    move-exception v3

    .line 458
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_FAILED"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 459
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 461
    throw v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1
.end method

.method public final declared-synchronized b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/nts/a;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    monitor-exit p0

    return-void

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 487
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v1, "onInitializedStateChanged {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 490
    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 491
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 492
    const-string v1, "SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 493
    iget-object v1, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 498
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    const-class v3, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 502
    iget-boolean v0, p0, Lcom/mfluent/asp/nts/a;->q:Z

    if-nez v0, :cond_0

    .line 503
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v1, "Binding to ContentAggregatorService."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    const-class v3, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/mfluent/asp/nts/a;->r:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 512
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-boolean v0, p0, Lcom/mfluent/asp/nts/a;->q:Z

    if-eqz v0, :cond_2

    .line 508
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v1, "Unbinding from ContentAggregatorService."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/mfluent/asp/nts/a;->r:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 516
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    const-string v1, "onPresenceConnectedStateChanged {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/nts/a;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 518
    iget-object v0, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/nts/a;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 519
    return-void
.end method

.method public groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I
    .locals 3

    .prologue
    .line 476
    sget-object v0, Lcom/mfluent/asp/nts/a;->j:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::groupPeerStatusNotify(): PeerInfo:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 478
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/mfluent/asp/nts/a;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 479
    const-string v1, "PEER_INFO_EXTRA"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 480
    iget-object v1, p0, Lcom/mfluent/asp/nts/a;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 482
    invoke-super {p0, p1}, Lpcloud/net/nat/c;->groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I

    move-result v0

    return v0
.end method

.method public netChangedNotify(C)V
    .locals 0

    .prologue
    .line 523
    return-void
.end method

.method public smsNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 527
    return-void
.end method

.method public smsNotifyEx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 531
    return-void
.end method
