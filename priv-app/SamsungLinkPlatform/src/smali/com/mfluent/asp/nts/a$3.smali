.class final Lcom/mfluent/asp/nts/a$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/nts/a;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/nts/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/nts/a;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/mfluent/asp/nts/a$3;->a:Lcom/mfluent/asp/nts/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 161
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/nts/a$3;->a:Lcom/mfluent/asp/nts/a;

    invoke-virtual {v2}, Lcom/mfluent/asp/nts/a;->i()Z

    move-result v2

    if-nez v2, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v5, p0, Lcom/mfluent/asp/nts/a$3;->a:Lcom/mfluent/asp/nts/a;

    invoke-static {v5}, Lcom/mfluent/asp/nts/a;->a(Lcom/mfluent/asp/nts/a;)J

    move-result-wide v6

    sub-long v6, v2, v6

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/mfluent/asp/nts/a;->f()J

    move-result-wide v2

    :goto_1
    cmp-long v5, v6, v2

    if-lez v5, :cond_3

    :goto_2
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v5, "NTSInactivityCheck::run: Inactivity Timer: {}/{} NTSLockService.isBound(): {} Use Safety Latch: {}"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v8, v9

    const/4 v6, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v6

    const/4 v2, 0x2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v8, v2

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v8, v2

    invoke-interface {v1, v5, v8}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$3;->a:Lcom/mfluent/asp/nts/a;

    invoke-static {v0}, Lcom/mfluent/asp/nts/a;->e(Lcom/mfluent/asp/nts/a;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_WAKE_LOCKS_REVOKED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 172
    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "NTSInactivityCheck::run: Shutting down nts due to inactivity"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/mfluent/asp/nts/a$3;->a:Lcom/mfluent/asp/nts/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/nts/a;->e()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "NTSInactivityCheck::run: Trouble terminating nts due to inactivity timeout"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_2
    :try_start_1
    sget-wide v2, Lcom/mfluent/asp/nts/a;->f:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
