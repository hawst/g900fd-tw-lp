.class public final Lcom/mfluent/asp/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/c$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lorg/apache/commons/collections/BidiMap;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 87
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "CASE "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    const/4 v1, 0x1

    .line 90
    invoke-static {}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->values()[Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    move v2, v0

    move v3, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v0, v5, v2

    .line 91
    invoke-static {v0}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getExtensionsForAspDocumentType(Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;)Ljava/util/Collection;

    move-result-object v1

    .line 92
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected all document types to have extensions defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    const-string v0, "WHEN "

    .line 96
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v1, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 97
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "(_display_name LIKE \'%."

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string v0, "|| "

    move-object v1, v0

    .line 99
    goto :goto_1

    .line 100
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "THEN "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    add-int/lit8 v1, v3, 0x1

    .line 90
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_0

    .line 103
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ELSE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " END"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const/16 v0, 0x2c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_display_name COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/c;->a:Ljava/lang/String;

    .line 107
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    .line 35
    new-instance v0, Lorg/apache/commons/collections/bidimap/DualHashBidiMap;

    invoke-direct {v0}, Lorg/apache/commons/collections/bidimap/DualHashBidiMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    .line 38
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "OLD"

    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "RECENT"

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ASC_DATE"

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "DESC_DATE"

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ATOZ"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ZTOA"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ASC_SIZE"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "DESC_SIZE"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ASC_TIME"

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "DESC_TIME"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ASC_ALBUM"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "DESC_ALBUM"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ASC_ARTIST"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "DESC_ARTIST"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "ASC_GENRE"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "DESC_GENRE"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    const-string v1, "FILE_TYPE"

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "OLD"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "RECENT"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "ATOZ"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "ZTOA"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->SIZE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "ASC_SIZE"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->SIZE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "DESC_SIZE"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DURATION_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "ASC_TIME"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DURATION_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "DESC_TIME"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->TYPE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v2, "FILE_TYPE"

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/collections/BidiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/mfluent/asp/c;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/c;
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/mfluent/asp/c$a;->a()Lcom/mfluent/asp/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/mfluent/asp/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 71
    if-nez v0, :cond_0

    .line 72
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 74
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFileSortType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    invoke-interface {v0, p1}, Lorg/apache/commons/collections/BidiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/c;->c:Lorg/apache/commons/collections/BidiMap;

    invoke-interface {v0, p1}, Lorg/apache/commons/collections/BidiMap;->getKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    return-object v0
.end method
