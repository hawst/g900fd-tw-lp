.class public final Lcom/mfluent/asp/a$b;
.super Lcom/msc/a/a/a$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/a;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/a;)V
    .locals 0

    .prologue
    .line 1098
    iput-object p1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    invoke-direct {p0}, Lcom/msc/a/a/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IZLandroid/os/Bundle;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 1102
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    .line 1103
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "(TID: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") ISACallback :: onReceiveAccessToken :: resultData:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1108
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "(TID: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") ISACallback :: onReceiveAccessToken :: isSuccess : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111
    :cond_1
    const/4 v0, 0x0

    .line 1112
    if-eqz p2, :cond_3

    .line 1113
    iget-object v0, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mfluent/asp/a;->b(Lcom/mfluent/asp/a;Z)Z

    .line 1115
    new-instance v0, Lcom/sec/pcw/hybrid/b/b$a;

    invoke-direct {v0}, Lcom/sec/pcw/hybrid/b/b$a;-><init>()V

    .line 1117
    const-string v1, "access_token"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->c:Ljava/lang/String;

    .line 1118
    const-string v1, "user_id"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->a:Ljava/lang/String;

    .line 1119
    iget-object v1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    invoke-static {v1}, Lcom/mfluent/asp/a;->g(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/c/a;->b()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->b:Ljava/lang/String;

    .line 1122
    :try_start_0
    const-string v1, "mcc"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1123
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->h:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1127
    :goto_0
    const-string v1, "server_url"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->e:Ljava/lang/String;

    .line 1130
    const-string v1, "cc"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    .line 1132
    const-string v1, "device_physical_address_text"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->g:Ljava/lang/String;

    .line 1153
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    iget-object v1, v1, Lcom/mfluent/asp/a;->d:Lcom/mfluent/asp/a$c;

    invoke-virtual {v1}, Lcom/mfluent/asp/a$c;->b()V

    .line 1155
    iget-object v1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    invoke-static {v1, v0}, Lcom/mfluent/asp/a;->a(Lcom/mfluent/asp/a;Lcom/sec/pcw/hybrid/b/b$a;)V

    .line 1156
    return-void

    .line 1138
    :cond_3
    iget-object v1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/mfluent/asp/a;->b(Lcom/mfluent/asp/a;Z)Z

    .line 1139
    const-string v1, "error_code"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1140
    const-string v4, "error_message"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1142
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1143
    const-string v5, "mfl_AccessManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "(TID: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") ISACallback :: onReceiveAccessToken :: errorCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    const-string v5, "mfl_AccessManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "(TID: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") ISACallback :: onReceiveAccessToken :: errorMessage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    :cond_4
    const-string v2, "SAC_0204"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1148
    iget-object v1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    invoke-static {v1}, Lcom/mfluent/asp/a;->h(Lcom/mfluent/asp/a;)V

    goto :goto_1

    .line 1149
    :cond_5
    const-string v2, "SAC_0203"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "SAC_0205"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "SAC_0401"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "SAC_0402"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1150
    :cond_6
    iget-object v1, p0, Lcom/mfluent/asp/a$b;->a:Lcom/mfluent/asp/a;

    invoke-static {v1}, Lcom/mfluent/asp/a;->i(Lcom/mfluent/asp/a;)V

    goto/16 :goto_1

    .line 1124
    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method

.method public final b(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1160
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161
    const-string v0, "mfl_AccessManager"

    const-string v1, "ISACallback :: onReceiveChecklistValidation ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    :cond_0
    return-void
.end method

.method public final c(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1168
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169
    const-string v0, "mfl_AccessManager"

    const-string v1, "ISACallback :: onReceiveDisclaimerAgreement ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    :cond_0
    return-void
.end method

.method public final d(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1176
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177
    const-string v0, "mfl_AccessManager"

    const-string v1, "ISACallback :: onReceiveAuthCode ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    :cond_0
    return-void
.end method

.method public final e(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1184
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1185
    const-string v0, "mfl_AccessManager"

    const-string v1, "ISACallback :: onReceiveSCloudAccessToken ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    :cond_0
    return-void
.end method

.method public final f(IZLandroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1192
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193
    const-string v0, "mfl_AccessManager"

    const-string v1, "ISACallback :: onReceivePasswordConfirmation ::"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    :cond_0
    return-void
.end method
