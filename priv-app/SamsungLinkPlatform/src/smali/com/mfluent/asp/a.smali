.class public Lcom/mfluent/asp/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/a$b;,
        Lcom/mfluent/asp/a$c;,
        Lcom/mfluent/asp/a$a;
    }
.end annotation


# static fields
.field private static e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final f:Lorg/slf4j/Logger;

.field private static final g:Ljava/net/URI;

.field private static h:Lcom/mfluent/asp/a;

.field private static final w:J


# instance fields
.field private final A:Landroid/content/BroadcastReceiver;

.field a:Lcom/msc/a/a/b;

.field b:Lcom/mfluent/asp/a$b;

.field c:Ljava/lang/String;

.field final d:Lcom/mfluent/asp/a$c;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final j:Landroid/os/Handler;

.field private k:Z

.field private final l:Landroid/content/Context;

.field private final m:Lcom/sec/pcw/hybrid/a/a;

.field private final n:Lcom/sec/pcw/hybrid/c/a;

.field private final o:Lcom/sec/pcw/hybrid/b/a;

.field private final p:Ljava/util/concurrent/locks/ReentrantLock;

.field private final q:Ljava/util/concurrent/locks/Condition;

.field private final r:Ljava/util/concurrent/locks/Condition;

.field private s:Lcom/sec/pcw/hybrid/b/b;

.field private t:Lcom/sec/pcw/hybrid/b/b;

.field private u:Ljava/lang/Thread;

.field private v:Z

.field private x:Z

.field private y:Z

.field private final z:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 85
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_TRANSPORT:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 86
    const-class v0, Lcom/mfluent/asp/a;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    .line 92
    const-string v0, "http://m.allshareplay.com"

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/a;->g:Ljava/net/URI;

    .line 103
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/a;->h:Lcom/mfluent/asp/a;

    .line 144
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/a;->w:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/a;->k:Z

    .line 126
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    .line 138
    iput-object v2, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    .line 139
    iput-object v2, p0, Lcom/mfluent/asp/a;->t:Lcom/sec/pcw/hybrid/b/b;

    .line 140
    iput-object v2, p0, Lcom/mfluent/asp/a;->u:Ljava/lang/Thread;

    .line 142
    iput-boolean v1, p0, Lcom/mfluent/asp/a;->v:Z

    .line 152
    iput-boolean v1, p0, Lcom/mfluent/asp/a;->x:Z

    .line 153
    iput-boolean v1, p0, Lcom/mfluent/asp/a;->y:Z

    .line 154
    new-instance v0, Lcom/mfluent/asp/a$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/a$1;-><init>(Lcom/mfluent/asp/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/a;->z:Landroid/content/BroadcastReceiver;

    .line 596
    new-instance v0, Lcom/mfluent/asp/a$4;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/a$4;-><init>(Lcom/mfluent/asp/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/a;->A:Landroid/content/BroadcastReceiver;

    .line 921
    new-instance v0, Lcom/mfluent/asp/a$c;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/a$c;-><init>(Lcom/mfluent/asp/a;B)V

    iput-object v0, p0, Lcom/mfluent/asp/a;->d:Lcom/mfluent/asp/a$c;

    .line 189
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    .line 190
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/a;->j:Landroid/os/Handler;

    .line 193
    invoke-static {}, Lcom/sec/pcw/hybrid/a/a;->a()Lcom/sec/pcw/hybrid/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a;->m:Lcom/sec/pcw/hybrid/a/a;

    .line 196
    new-instance v0, Lcom/sec/pcw/hybrid/b/a;

    const-string v1, "ASPSigninApp"

    const-string v2, "c7hc8m4900"

    const-string v3, "B5B9B48012665C4F1914C52B4B6DD2F4"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/hybrid/b/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/a;->o:Lcom/sec/pcw/hybrid/b/a;

    .line 197
    new-instance v0, Lcom/sec/pcw/hybrid/c/a;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/mfluent/asp/a;->o:Lcom/sec/pcw/hybrid/b/a;

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/hybrid/c/a;-><init>(Landroid/content/Context;Lcom/sec/pcw/hybrid/b/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/a;->n:Lcom/sec/pcw/hybrid/c/a;

    .line 198
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a;->q:Ljava/util/concurrent/locks/Condition;

    .line 199
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a;->r:Ljava/util/concurrent/locks/Condition;

    .line 201
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/a;
    .locals 2

    .prologue
    .line 110
    const-class v1, Lcom/mfluent/asp/a;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 111
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 113
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/a;->h:Lcom/mfluent/asp/a;

    if-nez v0, :cond_1

    .line 114
    new-instance v0, Lcom/mfluent/asp/a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/a;->h:Lcom/mfluent/asp/a;

    .line 117
    :cond_1
    sget-object v0, Lcom/mfluent/asp/a;->h:Lcom/mfluent/asp/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/b/b;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/a;->c(Z)Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 362
    const/4 v1, 0x0

    .line 364
    :try_start_0
    invoke-static {p1}, Lcom/sec/pcw/server/NativeCall;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 367
    :try_start_1
    invoke-static {p0, v1}, Lcom/sec/pcw/service/account/a;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 372
    :goto_0
    return-object v0

    .line 368
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 369
    :goto_1
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "encryptAES - Exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 368
    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_1
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1449
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    const-string v1, "::processSignInComplete: processing and clearing notification requests"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1450
    iget-object v0, p0, Lcom/mfluent/asp/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    .line 1451
    iget-boolean v1, p0, Lcom/mfluent/asp/a;->y:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1452
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    const-string v1, "::showGallerySpecificAccountFullNotification: Posting special Gallery Account Full Notification"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f02013f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v2, 0x7f0a0048

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v2, 0x7f0a03c0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-class v3, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v2, 0x10008000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, v4, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v2, "com.mfluent.asp.AccessManager.FAILED_SIGN_IN_NOTIFICATION_TAG"

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1456
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.AccessManager.BROADCAST_SIGN_IN_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.mfluent.asp.AccessManager.EXTRA_SIGN_IN_RESULT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1459
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 1460
    iget-object v1, p0, Lcom/mfluent/asp/a;->z:Landroid/content/BroadcastReceiver;

    monitor-enter v1

    .line 1461
    :try_start_0
    iget-boolean v2, p0, Lcom/mfluent/asp/a;->x:Z

    if-eqz v2, :cond_1

    .line 1462
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mfluent/asp/a;->x:Z

    .line 1463
    iget-object v2, p0, Lcom/mfluent/asp/a;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1465
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/mfluent/asp/a;I)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/mfluent/asp/a;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/a;Lcom/sec/pcw/hybrid/b/b$a;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 82
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "(TID:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") ::processReceivedAuthInfo result:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v8, 0x3

    invoke-virtual {v5, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "mfl_AccessManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "(TID:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") ::processReceivedAuthInfo: detected empty peerId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v5, p1, Lcom/sec/pcw/hybrid/b/b$a;->g:Ljava/lang/String;

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    sget-object v5, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v8, 0x3

    invoke-virtual {v5, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "mfl_AccessManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "(TID:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") ::processReceivedAuthInfo : using HW device id to generated peerId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/sec/pcw/service/account/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/pcw/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/mfluent/asp/datamodel/Device;->c(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    :cond_3
    :goto_0
    if-eqz p1, :cond_a

    iget-object v3, p1, Lcom/sec/pcw/hybrid/b/b$a;->a:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p1, Lcom/sec/pcw/hybrid/b/b$a;->c:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    move v3, v2

    :goto_1
    if-eqz v3, :cond_a

    iget v3, p1, Lcom/sec/pcw/hybrid/b/b$a;->h:I

    const/16 v4, 0x1cc

    if-eq v3, v4, :cond_4

    const-string v3, "CHN"

    iget-object v4, p1, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "CHU"

    iget-object v4, p1, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "CHM"

    iget-object v4, p1, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "CTC"

    iget-object v4, p1, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "CHZ"

    iget-object v4, p1, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_4
    iget-object v3, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v4, 0x7f0a0002

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v5, 0x7f0a0003

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    const-string v5, "SL_STAGING"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "(TID:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") processReceivedAuthInfo baseUrl : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "SL_STAGING"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "(TID:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") processReceivedAuthInfo fwkUrl : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/pcw/service/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/pcw/service/d/b;->b()Ljava/lang/String;

    move-result-object v5

    const-string v8, "cn"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "cn"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_5
    move v0, v2

    :goto_3
    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v2, "slpf_pref_20"

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "baseUrl"

    const-string v5, ""

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "fwkUrl"

    const-string v8, ""

    invoke-interface {v1, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "chinaCSC"

    const/4 v9, 0x0

    invoke-interface {v1, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-static {v2, v4}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v5, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    if-eq v8, v0, :cond_7

    :cond_6
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "baseUrl"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "fwkUrl"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "chinaCSC"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_7
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ISACallback :: onReceiveAccessToken :: [AccessTokenAIDL :: BASE URL] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ISACallback :: onReceiveAccessToken :: [AccessTokenAIDL :: FWK URL]  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ISACallback :: onReceiveAccessToken :: sendBroadcase(BROADCAST_SSO_TOKEN_SUCCESS)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1}, Lcom/sec/pcw/hybrid/b/b$a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/a;->t:Lcom/sec/pcw/hybrid/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_a
    iget-object v0, p0, Lcom/mfluent/asp/a;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :cond_b
    :try_start_1
    sget-object v5, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v8, 0x3

    invoke-virtual {v5, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string v5, "mfl_AccessManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "(TID:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") ::processReceivedAuthInfo : using SA device id to generated peerId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v5, p1, Lcom/sec/pcw/hybrid/b/b$a;->g:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/pcw/service/account/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/pcw/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/mfluent/asp/datamodel/Device;->c(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signal()V

    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_d
    move v3, v1

    goto/16 :goto_1

    :cond_e
    :try_start_2
    sget-boolean v3, Lcom/sec/pcw/util/Common;->u:Z

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v4, 0x7f0a0004

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v5, 0x7f0a0005

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    :cond_f
    iget-object v3, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const/high16 v4, 0x7f0a0000

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v5, 0x7f0a0001

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    goto/16 :goto_2

    :cond_10
    move v0, v1

    goto/16 :goto_3
.end method

.method static synthetic a(Lcom/mfluent/asp/a;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/mfluent/asp/a;->d(Z)V

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 377
    const/4 v1, 0x0

    .line 380
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/pcw/service/account/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 382
    :try_start_1
    invoke-static {v1}, Lcom/sec/pcw/server/NativeCall;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 387
    :goto_0
    return-object v0

    .line 383
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 384
    :goto_1
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "decryptAES - Exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 383
    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_1
.end method

.method static synthetic b(Lcom/mfluent/asp/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/a;->c(Z)Lcom/sec/pcw/hybrid/b/b;

    invoke-direct {p0, v1}, Lcom/mfluent/asp/a;->d(Z)V

    invoke-virtual {p0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/pcw/service/push/b;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/b;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/pcw/service/push/b;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/DeviceListSyncManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.SIGN_IN_SYNC"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v1}, Lcom/mfluent/asp/a;->a(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/a;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/mfluent/asp/a;->y:Z

    return p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x3

    .line 699
    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v2, "slpf_pref_20"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 700
    const-string v2, "com.mfluent.asp.AccessManager.LAST_FWK_EMAIL"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 702
    if-eqz v2, :cond_0

    .line 703
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/mfluent/asp/a;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 705
    :cond_0
    invoke-static {p1, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 777
    :cond_1
    :goto_0
    return v6

    .line 708
    :cond_2
    sget-object v2, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 709
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::checkLoginUserToFWK email is different old: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " new:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    :cond_3
    const-string v0, "sup/user/checkloginuser"

    .line 713
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 715
    const-string v3, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><ValidateRequestVO><guid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</guid></ValidateRequestVO>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/mfluent/asp/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 729
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "<result>success</result>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "<token>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 730
    sget-object v2, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_4

    .line 731
    const-string v2, "mfl_AccessManager"

    const-string v3, "::checkLoginUserToFWK: [FWK - Result] : SUCCESS !!!"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 733
    :cond_4
    :try_start_1
    const-string v2, "<token>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x7

    const-string v3, "</token>"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 742
    :goto_1
    :try_start_2
    sget-object v2, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_5

    .line 743
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::checkLoginUserToFWK: [Token] : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    :cond_5
    new-instance v2, Ljava/net/HttpCookie;

    const-string v3, "GUID"

    invoke-direct {v2, v3, p2}, Ljava/net/HttpCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "m.allshareplay.com"

    invoke-virtual {v2, v3}, Ljava/net/HttpCookie;->setDomain(Ljava/lang/String;)V

    invoke-static {}, Lcom/mfluent/asp/util/l;->a()Lcom/mfluent/asp/util/l;

    move-result-object v3

    iget-object v3, v3, Lcom/mfluent/asp/util/l;->a:Ljava/net/CookieStore;

    sget-object v4, Lcom/mfluent/asp/a;->g:Ljava/net/URI;

    invoke-interface {v3, v4, v2}, Ljava/net/CookieStore;->add(Ljava/net/URI;Ljava/net/HttpCookie;)V

    new-instance v2, Ljava/net/HttpCookie;

    const-string v3, "token"

    invoke-direct {v2, v3, v0}, Ljava/net/HttpCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "m.allshareplay.com"

    invoke-virtual {v2, v0}, Ljava/net/HttpCookie;->setDomain(Ljava/lang/String;)V

    invoke-static {}, Lcom/mfluent/asp/util/l;->a()Lcom/mfluent/asp/util/l;

    move-result-object v0

    iget-object v0, v0, Lcom/mfluent/asp/util/l;->a:Ljava/net/CookieStore;

    sget-object v3, Lcom/mfluent/asp/a;->g:Ljava/net/URI;

    invoke-interface {v0, v3, v2}, Ljava/net/CookieStore;->add(Ljava/net/URI;Ljava/net/HttpCookie;)V

    .line 748
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 749
    const-string v1, "com.mfluent.asp.AccessManager.LAST_FWK_EMAIL"

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 750
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 772
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 774
    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_1

    .line 775
    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::checkLoginUserToFWK: [FWK - Result] : Exception !!!\n==> "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 736
    :catch_1
    move-exception v0

    .line 737
    :try_start_3
    sget-object v2, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_6

    .line 738
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::checkLoginUserToFWK:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_6
    const-string v0, ""

    goto/16 :goto_1

    .line 754
    :cond_7
    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_1

    .line 755
    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::checkLoginUserToFWK: [FWK - Result] : FAIL !!! result="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/mfluent/asp/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    return-object v0
.end method

.method private c(Z)Lcom/sec/pcw/hybrid/b/b;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x2

    .line 217
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 221
    const-string v0, "AccessManager"

    const-string v3, "getAuthInfo is a blocking call and should NEVER be called from the application\'s main thread."

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    if-nez v0, :cond_3

    .line 227
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    iget-object v0, p0, Lcom/mfluent/asp/a;->u:Ljava/lang/Thread;

    if-nez v0, :cond_12

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a;->u:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mfluent/asp/a;->n:Lcom/sec/pcw/hybrid/c/a;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/a;->t:Lcom/sec/pcw/hybrid/b/b;

    if-eqz v0, :cond_4

    move v3, v1

    :goto_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v7, 0x10000

    invoke-virtual {v0, v6, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "doesSupportAIDLInterface :: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_7

    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ::fetchAuthInfoFromSamsungAccount: try to login through AIDL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/mfluent/asp/a;->d:Lcom/mfluent/asp/a$c;

    invoke-virtual {v2}, Lcom/mfluent/asp/a$c;->a()Z

    move-result v2

    const-string v4, "mfl_AccessManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "(TID: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") ::getSamsungAccountAIDL bind() result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    :try_start_1
    const-string v2, "mfl_AccessManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "(TID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") ::getSamsungAccountAIDL waiting for async processing..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/a;->q:Ljava/util/concurrent/locks/Condition;

    sget-wide v4, Lcom/mfluent/asp/a;->w:J

    invoke-interface {v2, v4, v5}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v4

    cmp-long v2, v4, v10

    if-gtz v2, :cond_6

    const-string v2, "mfl_AccessManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "(TID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") ::getSamsungAccountAIDL timed out."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    new-instance v0, Lcom/mfluent/asp/a$a;

    invoke-direct {v0, p0, v3}, Lcom/mfluent/asp/a$a;-><init>(Lcom/mfluent/asp/a;Z)V

    invoke-virtual {v0}, Lcom/mfluent/asp/a$a;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/a;->u:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mfluent/asp/a;->r:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 229
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 231
    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 234
    return-object v0

    :cond_4
    move v3, v2

    .line 227
    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    :try_start_3
    const-string v2, "mfl_AccessManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "(TID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") ::getSamsungAccountAIDL execution resuming after Condition signal."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v2

    :try_start_4
    const-string v2, "mfl_AccessManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "(TID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") ::getSamsungAccountAIDL interrupted."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 231
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 227
    :cond_7
    :try_start_5
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ::fetchAuthInfoFromSamsungAccount: try to login through Broadcast"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mfluent/asp/a;->v:Z

    if-nez v0, :cond_e

    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_8

    const-string v0, "mfl_AccessManager"

    const-string v1, "::registerAuthInfoUpdateReceiver: [ START ] ##"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-boolean v0, p0, Lcom/mfluent/asp/a;->v:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/mfluent/asp/a;->A:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_d

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.osp.ACCESSTOKEN.RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_9

    const-string v1, "mfl_AccessManager"

    const-string v2, "::registerAuthInfoUpdateReceiver: [ mAuthInfoReceiver 1 ] : com.osp.ACCESSTOKEN.RESPONSE"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const-string v1, "com.osp.ACCESSTOKEN.FAIL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_a

    const-string v1, "mfl_AccessManager"

    const-string v2, "::registerAuthInfoUpdateReceiver: [ mAuthInfoReceiver 2 ] : com.osp.ACCESSTOKEN.FAIL"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_b

    const-string v1, "mfl_AccessManager"

    const-string v2, "::registerAuthInfoUpdateReceiver: [ mAuthInfoReceiver 4 ] : android.intent.action.REGISTRATION_COMPLETED"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const-string v1, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_c

    const-string v1, "mfl_AccessManager"

    const-string v2, "::registerAuthInfoUpdateReceiver: [ mAuthInfoReceiver 5 ] : com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/mfluent/asp/a;->A:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/a;->v:Z

    :cond_d
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_e

    const-string v0, "mfl_AccessManager"

    const-string v1, "::registerAuthInfoUpdateReceiver: register AuthInfoUpdateReceiver [ END ] ##"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lcom/mfluent/asp/a;->n:Lcom/sec/pcw/hybrid/c/a;

    iget-object v1, p0, Lcom/mfluent/asp/a;->t:Lcom/sec/pcw/hybrid/b/b;

    invoke-virtual {v0, v1}, Lcom/sec/pcw/hybrid/c/a;->a(Lcom/sec/pcw/hybrid/b/b;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v0, p0, Lcom/mfluent/asp/a;->q:Ljava/util/concurrent/locks/Condition;

    sget-wide v4, Lcom/mfluent/asp/a;->w:J

    invoke-interface {v0, v4, v5}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v0

    cmp-long v0, v0, v10

    if-gtz v0, :cond_f

    const-string v0, "mfl_AccessManager"

    const-string v1, "::getSamsungAccountTokenBroadcast timed out."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_f
    :goto_4
    :try_start_7
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_10

    const-string v0, "mfl_AccessManager"

    const-string v1, "::unregisterAuthInfoUpdateReceiver: unregister AuthInfoUpdateReceiver [ START ] ##"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    iget-boolean v0, p0, Lcom/mfluent/asp/a;->v:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/mfluent/asp/a;->A:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_11

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/a;->v:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/mfluent/asp/a;->A:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_11
    :goto_5
    :try_start_9
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_1

    const-string v0, "mfl_AccessManager"

    const-string v1, "::unregisterAuthInfoUpdateReceiver: unregister AuthInfoUpdateReceiver [ END ] ##"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const-string v0, "mfl_AccessManager"

    const-string v1, "::getSamsungAccountTokenBroadcast interrupted."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    :cond_12
    :goto_6
    :try_start_a
    iget-object v0, p0, Lcom/mfluent/asp/a;->u:Ljava/lang/Thread;

    if-eqz v0, :cond_13

    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ::fetchAuthInfoFromSamsungAccount waiting for TID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/a;->u:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to finish fetch."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/a;->r:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V

    goto :goto_6

    :catch_2
    move-exception v0

    goto/16 :goto_3

    :cond_13
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ::fetchAuthInfoFromSamsungAccount resuming normally after Process Condition signalled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    :catch_3
    move-exception v0

    goto :goto_5
.end method

.method static synthetic d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method private d(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 278
    invoke-virtual {p0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v1

    .line 280
    if-eqz v1, :cond_2

    .line 281
    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/mfluent/asp/a;->b(Ljava/lang/String;Ljava/lang/String;)Z

    .line 283
    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v3, "slpf_pref_20"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 284
    const-string v3, "com.mfluent.asp.AccessManager.LAST_TOKEN_VALIDATION_GUID"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 286
    if-eqz v3, :cond_0

    .line 287
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/mfluent/asp/a;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    .line 294
    :try_start_0
    invoke-virtual {v1}, Lcom/mfluent/asp/b/i;->f()Z

    move-result v1

    if-nez v1, :cond_3

    .line 314
    :cond_2
    :goto_0
    return-void

    .line 297
    :cond_3
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 298
    const-string v2, "com.mfluent.asp.AccessManager.LAST_TOKEN_VALIDATION_GUID"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 299
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    if-eqz v0, :cond_2

    .line 308
    new-instance v0, Lcom/sec/pcw/service/push/b;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/b;-><init>(Landroid/content/Context;)V

    .line 310
    invoke-virtual {v0}, Lcom/sec/pcw/service/push/b;->c()V

    goto :goto_0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    sget-object v1, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 302
    const-string v1, "mfl_AccessManager"

    const-string v2, "UserPortalManager.getDeviceUser failed."

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/Condition;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/a;->q:Ljava/util/concurrent/locks/Condition;

    return-object v0
.end method

.method static synthetic f(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/b/b;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/a;->t:Lcom/sec/pcw/hybrid/b/b;

    return-object v0
.end method

.method static synthetic g(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/c/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/a;->n:Lcom/sec/pcw/hybrid/c/a;

    return-object v0
.end method

.method static synthetic h(Lcom/mfluent/asp/a;)V
    .locals 9

    .prologue
    const v8, 0x7f0a03da

    const v7, 0x7f09000f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 82
    const-string v0, "mfl_AccessManager"

    const-string v1, "postVerifySamsungAccountNotification"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mfluent/asp/a;->k:Z

    if-eqz v0, :cond_1

    iput-boolean v5, p0, Lcom/mfluent/asp/a;->k:Z

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "mfl_AccessManager"

    const-string v3, "getVerifySamsungAccountIntent"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.samsungaccount.emailvalidate"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "user_id"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/mfluent/asp/a;->o:Lcom/sec/pcw/hybrid/b/a;

    if-eqz v4, :cond_0

    const-string v4, "client_id"

    iget-object v5, p0, Lcom/mfluent/asp/a;->o:Lcom/sec/pcw/hybrid/b/a;

    invoke-virtual {v5}, Lcom/sec/pcw/hybrid/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "client_secret"

    iget-object v5, p0, Lcom/mfluent/asp/a;->o:Lcom/sec/pcw/hybrid/b/a;

    invoke-virtual {v5}, Lcom/sec/pcw/hybrid/b/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v4, "additional"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "theme"

    const-string v4, "light"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v3, 0x8000000

    invoke-static {v0, v7, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020107

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    const v2, 0x7f0200f9

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v2, 0x7f0a03d8

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/ASPApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/ASPApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_1
    return-void
.end method

.method static synthetic i()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic i(Lcom/mfluent/asp/a;)V
    .locals 6

    .prologue
    const v5, 0x7f0a0054

    const v4, 0x7f090010

    .line 82
    const-string v0, "mfl_AccessManager"

    const-string v1, "postReloginNotification"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mfluent/asp/a;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/a;->k:Z

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.settings.SYNC_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v4, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020107

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    const v2, 0x7f0200f9

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v2, 0x7f0a03d9

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/ASPApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/ASPApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method static synthetic j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/sec/pcw/hybrid/b/b;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 784
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/mfluent/asp/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 789
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 790
    const-string v1, "slpf_pref_20"

    invoke-virtual {v0, v1, v4}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 791
    sget-boolean v1, Lcom/sec/pcw/util/Common;->u:Z

    if-eqz v1, :cond_0

    .line 793
    const-string v1, "fwkUrl"

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v3, 0x7f0a0005

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 798
    :goto_0
    const-string v1, "SL_STAGING"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executeSecurityPost : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 801
    invoke-virtual {p0, v0, p2, p3}, Lcom/mfluent/asp/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 802
    return-object v0

    .line 795
    :cond_0
    const-string v1, "fwkUrl"

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v3, 0x7f0a0001

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 412
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    const-string v1, "::login: postSpecialGalleryNotification = {}"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 413
    if-eqz p1, :cond_0

    .line 414
    iget-object v0, p0, Lcom/mfluent/asp/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 416
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/a;->z:Landroid/content/BroadcastReceiver;

    monitor-enter v1

    .line 421
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    const-string v2, "::hideGallerySpecificAccountFullNotification: Hiding special Gallery Account Full NotificationManager"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v2, "com.mfluent.asp.AccessManager.FAILED_SIGN_IN_NOTIFICATION_TAG"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 422
    iget-boolean v0, p0, Lcom/mfluent/asp/a;->x:Z

    if-eqz v0, :cond_1

    .line 423
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    const-string v2, "::login: Not calling processASPLogin because login thread is already active"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 438
    :goto_0
    monitor-exit v1

    return-void

    .line 425
    :cond_1
    sget-object v0, Lcom/mfluent/asp/a;->f:Lorg/slf4j/Logger;

    const-string v2, "::login: Starting processASPLogin thread"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/a;->x:Z

    .line 427
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/a;->z:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.mfluent.asp.sync.DeviceListSyncManager.BROADCAST_SIGN_IN_SYNC_FINISHED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 430
    new-instance v0, Lcom/mfluent/asp/a$3;

    const-string v2, "AccessManager.login"

    invoke-direct {v0, p0, v2}, Lcom/mfluent/asp/a$3;-><init>(Lcom/mfluent/asp/a;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/a$3;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Lcom/sec/pcw/hybrid/b/b;
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/a;->c(Z)Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const v8, 0x7f0a01f1

    const/4 v7, 0x3

    .line 807
    const-string v6, ""

    .line 810
    invoke-direct {p0, v9}, Lcom/mfluent/asp/a;->c(Z)Lcom/sec/pcw/hybrid/b/b;

    move-result-object v3

    .line 812
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    :cond_0
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_1

    .line 814
    const-string v0, "mfl_AccessManager"

    const-string v1, "::executeSecurityPost: requesting for access token."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :cond_1
    const/4 v0, 0x0

    .line 874
    :goto_0
    return-object v0

    .line 820
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/a;->m:Lcom/sec/pcw/hybrid/a/a;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/mfluent/asp/a;->o:Lcom/sec/pcw/hybrid/b/a;

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/a;->b()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/pcw/hybrid/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/pcw/hybrid/b/b;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 822
    if-eqz v0, :cond_c

    .line 823
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 825
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 826
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 827
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 829
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 830
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 831
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_4

    .line 832
    const-string v0, "mfl_AccessManager"

    const-string v1, "::executeSecurityPost:[Response] : EMPTY"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    :cond_4
    const-string v0, ""

    goto :goto_0

    .line 836
    :cond_5
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x2

    if-gt v0, v2, :cond_6

    .line 837
    const-string v0, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::executeSecurityPost:[Response] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    :cond_6
    const-string v0, "<error>"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 842
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_7

    .line 843
    const-string v0, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::executeSecurityPost: received response with error : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/16 v4, 0xb

    invoke-static {v3, v4}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    :cond_7
    const-string v0, "<code>SSF-2010</code>"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "session is not valid"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 853
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_8

    .line 854
    const-string v0, "mfl_AccessManager"

    const-string v2, "::executeSecurityPost:  requesting access token..."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    :cond_8
    invoke-virtual {p0}, Lcom/mfluent/asp/a;->c()V

    move-object v0, v1

    goto/16 :goto_0

    .line 859
    :cond_9
    const-string v0, "<code>SSF-4000</code>"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 861
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_a

    .line 862
    const-string v0, "mfl_AccessManager"

    const-string v2, "::executeSecurityPost:  requesting access token..."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    :cond_a
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020078

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const v3, 0x7f0a0048

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v2, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v2, v0}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v2

    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v8, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_b
    move-object v0, v1

    .line 868
    goto/16 :goto_0

    .line 869
    :cond_c
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_d

    .line 870
    const-string v0, "mfl_AccessManager"

    const-string v1, "::executeSecurityPost:[Response] : null"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object v0, v6

    goto/16 :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1416
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v1, "slpf_pref_20"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1417
    const-string v1, "com.mfluent.asp.AccessManager.PREF_IS_SIGNED_IN"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1419
    if-nez p1, :cond_0

    .line 1420
    invoke-virtual {p0}, Lcom/mfluent/asp/a;->d()V

    .line 1421
    const-string v2, "mfl_AccessManager"

    const-string v3, "resetAccessCredential called by setSignedIn"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    :cond_0
    if-eq v1, p1, :cond_1

    .line 1425
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1426
    const-string v1, "com.mfluent.asp.AccessManager.PREF_IS_SIGNED_IN"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1427
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1428
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_SIGNIN_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1429
    const-string v1, "SlinkSignInUtils.EXTRA_SIGNIN_STATE_SIGNED_IN"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1430
    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1432
    new-instance v0, Lcom/mfluent/asp/a$5;

    invoke-direct {v0, p0, p1}, Lcom/mfluent/asp/a$5;-><init>(Lcom/mfluent/asp/a;Z)V

    invoke-static {v0}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/lang/Runnable;)Z

    .line 1445
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 238
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 240
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::invalidateAuthInfo authInfo:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    iput-object v0, p0, Lcom/mfluent/asp/a;->t:Lcom/sec/pcw/hybrid/b/b;

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    .line 246
    new-instance v0, Lcom/mfluent/asp/a$2;

    const-string v1, "AccessManager::invalidateAuthInfo"

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/a$2;-><init>(Lcom/mfluent/asp/a;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/a$2;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 256
    return-void

    .line 255
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 391
    sget-object v0, Lcom/mfluent/asp/a;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    const-string v0, "mfl_AccessManager"

    const-string v1, "resetAccessCredential"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/util/a;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 396
    const-string v0, "mfl_AccessManager"

    const-string v1, "::No samsung account - unRegistraion PUSH"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    new-instance v0, Lcom/sec/pcw/service/push/b;

    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/b;->b()V

    .line 401
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 403
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;

    if-eqz v0, :cond_1

    .line 404
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/a;->s:Lcom/sec/pcw/hybrid/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 408
    return-void

    .line 399
    :cond_2
    const-string v0, "mfl_AccessManager"

    const-string v1, "::Has samsung account - do not unRegistration PUSH"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 443
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 444
    iget-object v1, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v2, "slpf_pref_20"

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 445
    const-string v2, "china"

    const-string v3, "chinaCSC"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 447
    invoke-virtual {p0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v1

    .line 448
    if-eqz v1, :cond_0

    .line 449
    const-string v2, "guid"

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 450
    const-string v2, "emailID"

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 451
    const-string v2, "mcc"

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->g()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 452
    const-string v2, "cc"

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 453
    const-string v2, "serverUrl"

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 454
    const-string v2, "token"

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 459
    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Lcom/mfluent/asp/a;->n:Lcom/sec/pcw/hybrid/c/a;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/c/a;->b()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1408
    iget-object v0, p0, Lcom/mfluent/asp/a;->n:Lcom/sec/pcw/hybrid/c/a;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/c/a;->c()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1412
    iget-object v0, p0, Lcom/mfluent/asp/a;->l:Landroid/content/Context;

    const-string v1, "slpf_pref_20"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "com.mfluent.asp.AccessManager.PREF_IS_SIGNED_IN"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
