.class final Lcom/mfluent/asp/a$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/a;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/mfluent/asp/a$1;->a:Lcom/mfluent/asp/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 158
    const-string v1, "com.mfluent.asp.sync.DeviceListSyncManager.EXTRA_SIGN_IN_RESULT"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 159
    invoke-static {}, Lcom/mfluent/asp/a;->i()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "::onReceive: Received BROADCAST_SIGN_IN_SYNC_FINISHED - EXTRA_SIGN_IN_RESULT={}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    packed-switch v1, :pswitch_data_0

    .line 175
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected sign in result from the DeviceListSyncManager"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :pswitch_0
    const/4 v0, 0x0

    .line 178
    :goto_0
    :pswitch_1
    iget-object v1, p0, Lcom/mfluent/asp/a$1;->a:Lcom/mfluent/asp/a;

    invoke-static {v1, v0}, Lcom/mfluent/asp/a;->a(Lcom/mfluent/asp/a;I)V

    .line 179
    return-void

    .line 167
    :pswitch_2
    const/4 v0, 0x2

    .line 168
    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
