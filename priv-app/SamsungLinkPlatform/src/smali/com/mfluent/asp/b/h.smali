.class public final Lcom/mfluent/asp/b/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:J

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean v0, p0, Lcom/mfluent/asp/b/h;->q:Z

    .line 28
    iput-boolean v0, p0, Lcom/mfluent/asp/b/h;->r:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/mfluent/asp/b/h;->a:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/mfluent/asp/b/h;->a:I

    .line 45
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/mfluent/asp/b/h;->p:J

    .line 134
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->d:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/mfluent/asp/b/h;->b:Z

    .line 53
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 243
    iput p1, p0, Lcom/mfluent/asp/b/h;->j:I

    .line 244
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->e:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/mfluent/asp/b/h;->c:Z

    .line 61
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/mfluent/asp/b/h;->b:Z

    return v0
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 251
    iput p1, p0, Lcom/mfluent/asp/b/h;->k:I

    .line 252
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->f:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 165
    iput-boolean p1, p0, Lcom/mfluent/asp/b/h;->q:Z

    .line 166
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/mfluent/asp/b/h;->c:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 255
    iput p1, p0, Lcom/mfluent/asp/b/h;->y:I

    .line 256
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->g:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 267
    iput-boolean p1, p0, Lcom/mfluent/asp/b/h;->r:Z

    .line 268
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->h:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->i:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->n:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->l:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->m:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->o:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/mfluent/asp/b/h;->p:J

    return-wide v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->s:Ljava/lang/String;

    .line 191
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->t:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->u:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->v:Ljava/lang/String;

    .line 215
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/mfluent/asp/b/h;->q:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->w:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/mfluent/asp/b/h;->x:Ljava/lang/String;

    .line 232
    return-void
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StorageProviderInfo [name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/b/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoggedIn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mfluent/asp/b/h;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", localVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/b/h;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/b/h;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/b/h;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final v()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/mfluent/asp/b/h;->j:I

    return v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/mfluent/asp/b/h;->k:I

    return v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/mfluent/asp/b/h;->y:I

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/mfluent/asp/b/h;->r:Z

    return v0
.end method
