.class public final Lcom/mfluent/asp/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/cloudstorage/api/sync/StorageAuthenticationInfo;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/Date;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/mfluent/asp/b/d;->a:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public final a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/mfluent/asp/b/d;->c:Ljava/util/Date;

    .line 45
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/mfluent/asp/b/d;->b:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/mfluent/asp/b/d;->d:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/mfluent/asp/b/d;->e:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public final getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/mfluent/asp/b/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getAccessTokenSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/mfluent/asp/b/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getAccountID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/b/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final getExpiry()Ljava/util/Date;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/b/d;->c:Ljava/util/Date;

    return-object v0
.end method
