.class final Lcom/mfluent/asp/b/c$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/b/c;->a(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

.field final synthetic b:I

.field final synthetic c:Z

.field final synthetic d:Lcom/mfluent/asp/b/c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/b/c;Lcom/mfluent/asp/common/util/prefs/LongPersistedField;IZ)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/mfluent/asp/b/c$1;->d:Lcom/mfluent/asp/b/c;

    iput-object p2, p0, Lcom/mfluent/asp/b/c$1;->a:Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    iput p3, p0, Lcom/mfluent/asp/b/c$1;->b:I

    iput-boolean p4, p0, Lcom/mfluent/asp/b/c$1;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/mfluent/asp/b/c$1;->a:Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {}, Lcom/mfluent/asp/b/c;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/b/c$1;->d:Lcom/mfluent/asp/b/c;

    invoke-static {v0}, Lcom/mfluent/asp/b/c;->a(Lcom/mfluent/asp/b/c;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v3

    .line 106
    invoke-virtual {v3}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v4

    .line 108
    if-nez v4, :cond_1

    .line 109
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Ignoring stats request because no auth info"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_1
    const-string v5, "sup/device/stats.json"

    .line 116
    iget v0, p0, Lcom/mfluent/asp/b/c$1;->b:I

    packed-switch v0, :pswitch_data_0

    .line 127
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Ignoring stats request for unsupported mediaType {}"

    iget v2, p0, Lcom/mfluent/asp/b/c$1;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :pswitch_0
    iget-boolean v0, p0, Lcom/mfluent/asp/b/c$1;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    .line 131
    :goto_1
    iget-boolean v2, p0, Lcom/mfluent/asp/b/c$1;->c:Z

    if-eqz v2, :cond_5

    const-string v2, "4"

    .line 132
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "<pcwDevice><userID>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "</userID><devicePhysicalAddressText>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/pcw/util/c;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "</devicePhysicalAddressText><client>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "</client><type>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "</type><action>1</action></pcwDevice>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 150
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 151
    const-string v1, "app-type"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v7, "Sending stats request: client = {}, type = {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v1, v7, v2, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155
    invoke-virtual {v3, v5, v4, v6}, Lcom/mfluent/asp/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 161
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 162
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Received empty response for stats request"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 118
    goto :goto_1

    .line 121
    :pswitch_1
    iget-boolean v0, p0, Lcom/mfluent/asp/b/c$1;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x3

    goto/16 :goto_1

    .line 124
    :pswitch_2
    iget-boolean v0, p0, Lcom/mfluent/asp/b/c$1;->c:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x2

    goto/16 :goto_1

    .line 131
    :cond_5
    const-string v2, "3"

    goto/16 :goto_2

    .line 156
    :catch_0
    move-exception v0

    .line 157
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Trouble performing stats request"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 167
    :cond_6
    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 168
    const-string v4, "response"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 169
    if-nez v3, :cond_8

    .line 170
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "No \'response\' element in stats response {}"

    invoke-interface {v3, v4, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 180
    :catch_1
    move-exception v3

    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "Trouble parsing stats response {}"

    invoke-interface {v3, v4, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 184
    :cond_7
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v3, "Got success response for stats request: client = {}, type = {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/mfluent/asp/b/c$1;->a:Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;->setValue(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 174
    :cond_8
    :try_start_2
    const-string v4, "result"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 175
    const-string v4, "success"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 176
    invoke-static {}, Lcom/mfluent/asp/b/c;->b()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "Error in stats response {}"

    invoke-interface {v3, v4, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
