.class final Lcom/mfluent/asp/b/i$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/b/i;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/b/i;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/b/i;)V
    .locals 0

    .prologue
    .line 1267
    iput-object p1, p0, Lcom/mfluent/asp/b/i$2;->a:Lcom/mfluent/asp/b/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1271
    invoke-static {}, Lcom/mfluent/asp/b/i;->g()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 1272
    const-string v0, "mfl_UserPortalManager"

    const-string v1, "Sending wakeup push"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/b/i$2;->a:Lcom/mfluent/asp/b/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/i;->e(Lcom/mfluent/asp/datamodel/Device;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1282
    :cond_1
    :goto_0
    return-void

    .line 1277
    :catch_0
    move-exception v0

    .line 1278
    invoke-static {}, Lcom/mfluent/asp/b/i;->g()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 1279
    const-string v1, "mfl_UserPortalManager"

    const-string v2, "Trouble sending wakeup push"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
