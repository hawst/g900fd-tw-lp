.class public final Lcom/mfluent/asp/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->i:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    .line 98
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->a:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/mfluent/asp/b/f;->h:Z

    .line 89
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->b:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->c:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->d:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->e:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->f:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/mfluent/asp/b/f;->g:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getLoginFailureReason()Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->i:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    return-object v0
.end method

.method public final getLoginPeopleID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getLoginUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final getOAuthURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final getTotalAmount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final getUploadLimit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final getUsed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/b/f;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final isLoggedIn()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/mfluent/asp/b/f;->h:Z

    return v0
.end method
