.class public final Lcom/mfluent/asp/b/e;
.super Lcom/mfluent/asp/b/b;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;


# static fields
.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static c:Lcom/mfluent/asp/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_TRANSPORT:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/mfluent/asp/b/b;-><init>(Landroid/content/Context;)V

    .line 88
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/b/e;
    .locals 2

    .prologue
    .line 77
    const-class v1, Lcom/mfluent/asp/b/e;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 78
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 80
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/b/e;->c:Lcom/mfluent/asp/b/e;

    if-nez v0, :cond_1

    .line 81
    new-instance v0, Lcom/mfluent/asp/b/e;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/b/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/b/e;->c:Lcom/mfluent/asp/b/e;

    .line 83
    :cond_1
    sget-object v0, Lcom/mfluent/asp/b/e;->c:Lcom/mfluent/asp/b/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private a(Lorg/json/JSONObject;)Lcom/mfluent/asp/b/h;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v4, 0x0

    .line 344
    iget-object v0, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 345
    const-string v1, "SpName"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 347
    invoke-virtual {v0, v2}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    .line 349
    if-nez v0, :cond_b

    .line 350
    new-instance v0, Lcom/mfluent/asp/b/h;

    invoke-direct {v0}, Lcom/mfluent/asp/b/h;-><init>()V

    .line 351
    invoke-virtual {v0, v2}, Lcom/mfluent/asp/b/h;->b(Ljava/lang/String;)V

    move-object v1, v0

    .line 355
    :goto_0
    const-string v0, "Name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->a(Ljava/lang/String;)V

    .line 357
    const-string v0, "IsOAuth"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const-string v0, "IsOAuth"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 359
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->a(Z)V

    .line 362
    :cond_0
    const-string v0, "LoginStatus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    const-string v0, "LoginStatus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 366
    :cond_1
    const-string v0, "Type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    const-string v0, "Type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->c(Ljava/lang/String;)V

    .line 370
    :cond_2
    const-string v0, "Url"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377
    const-string v0, "http://ndrive.naver.jp"

    const-string v3, "Url"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 378
    const-string v0, "JP"

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->j(Ljava/lang/String;)V

    .line 385
    :cond_3
    :goto_1
    const-string v0, "Applicable"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 386
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 387
    const-string v3, "false"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 388
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->d(Z)V

    .line 396
    :goto_2
    const-string v0, "VersionOnDevice"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 398
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->i(Ljava/lang/String;)V

    .line 401
    :cond_4
    const-string v0, "AccountID"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 402
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 404
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 406
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    .line 408
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 410
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 411
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getWebStorageUserId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getWebStorageUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 414
    :try_start_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v2

    .line 415
    if-eqz v2, :cond_6

    .line 416
    const-string v3, "mfl_StorageGatewayManager"

    const-string v4, "::parseServiceProvider:cloudStorageSync.reset()"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->reset()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    :cond_6
    :goto_3
    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageUserId(Ljava/lang/String;)V

    .line 426
    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/Device;->k(Ljava/lang/String;)V

    .line 429
    :try_start_1
    const-string v2, "mfl_StorageGatewayManager"

    const-string v3, "::parseServiceProvider:device.delete()"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->P()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 444
    :cond_7
    :goto_4
    return-object v1

    .line 379
    :cond_8
    const-string v0, "http://ndrive.naver.com"

    const-string v3, "Url"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 380
    const-string v0, "KR"

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/h;->j(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 390
    :cond_9
    invoke-virtual {v1, v4}, Lcom/mfluent/asp/b/h;->d(Z)V

    goto/16 :goto_2

    .line 393
    :cond_a
    invoke-virtual {v1, v4}, Lcom/mfluent/asp/b/h;->d(Z)V

    goto/16 :goto_2

    .line 419
    :catch_0
    move-exception v2

    .line 420
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v6, :cond_6

    .line 421
    const-string v3, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::parseServiceProvider:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 432
    :catch_1
    move-exception v0

    .line 433
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_7

    .line 434
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::parseServiceProvider:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_b
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private static a(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 221
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    const/4 v1, 0x0

    .line 224
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 228
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 233
    :catch_0
    move-exception v1

    :goto_1
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Reader;)V

    .line 235
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 233
    :cond_0
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Reader;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Reader;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lcom/mfluent/asp/b/e;->a()Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->b()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    .line 943
    if-eqz v0, :cond_0

    .line 944
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 946
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 255
    iget-object v0, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v2

    .line 257
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 270
    const-string v3, "Response"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 272
    if-eqz v3, :cond_5

    .line 273
    const-string v0, "CatalogHost"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 274
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 275
    const-string v4, "CatalogHost"

    invoke-virtual {v2, v4, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 280
    invoke-virtual {v2}, Lcom/mfluent/asp/b/g;->e()Ljava/util/List;

    move-result-object v0

    .line 281
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    .line 283
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 286
    :cond_1
    const-string v0, "ServicProvider"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 287
    if-eqz v5, :cond_3

    move v0, v1

    .line 290
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 291
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 292
    invoke-direct {p0, v3}, Lcom/mfluent/asp/b/e;->a(Lorg/json/JSONObject;)Lcom/mfluent/asp/b/h;

    move-result-object v3

    .line 295
    invoke-virtual {v3, v0}, Lcom/mfluent/asp/b/h;->d(I)V

    .line 298
    invoke-virtual {v3}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    invoke-virtual {v2, v3}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    .line 303
    sget-object v6, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    if-gt v6, v10, :cond_2

    .line 304
    const-string v6, "mfl_StorageGatewayManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::getStorageProviderList: adding sp array item : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 310
    :cond_3
    const-string v0, "ServicProvider"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_4

    .line 312
    invoke-direct {p0, v0}, Lcom/mfluent/asp/b/e;->a(Lorg/json/JSONObject;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    .line 315
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/h;->d(I)V

    .line 318
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    .line 322
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v10, :cond_4

    .line 323
    const-string v3, "mfl_StorageGatewayManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getStorageProviderList: adding sp item 1 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_4
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    .line 332
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 333
    invoke-virtual {v0, v9}, Lcom/mfluent/asp/b/h;->d(Z)V

    .line 334
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    goto :goto_2

    .line 339
    :cond_5
    const-string v0, "ProviderListInitialized"

    invoke-static {v9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 704
    .line 707
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v1

    .line 708
    if-nez v1, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-object v0

    .line 713
    :cond_1
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ssg/websvc/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/account/info/get.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 714
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<Request><UserID>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "</UserID></Request>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 715
    invoke-virtual {p0, v2, v1}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 729
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_0

    .line 730
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::accountInfo: result = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 717
    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 719
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_2

    .line 720
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::accountInfo: network problem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 729
    :cond_2
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_0

    .line 730
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::accountInfo: result = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 723
    :catch_1
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 725
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_3

    .line 726
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::accountInfo:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 729
    :cond_3
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_0

    .line 730
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::accountInfo: result = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 729
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_4

    .line 730
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::accountInfo: result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v1
.end method

.method public final b()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v1, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x6

    .line 94
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v11, :cond_0

    .line 95
    const-string v0, "mfl_StorageGatewayManager"

    const-string v2, "::getStorageProviderList:[START] ##"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v2

    .line 100
    if-nez v2, :cond_2

    .line 101
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_1

    .line 102
    const-string v0, "mfl_StorageGatewayManager"

    const-string v1, "::getStorageProviderList:Abort storage provider check because Samsung account is not logged in yet."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_1
    :goto_0
    return-void

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    .line 109
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v11, :cond_3

    .line 110
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getStorageProviderList: filesDir"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_a

    .line 113
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " does not exist!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_4
    new-instance v4, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "/overrideStorageProvider.json"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    .line 131
    if-eqz v0, :cond_6

    .line 133
    :try_start_0
    sget-object v5, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    if-gt v5, v12, :cond_5

    .line 134
    const-string v5, "mfl_StorageGatewayManager"

    const-string v6, "::getStorageProviderList: OVERRIDE: using LOCAL override file"

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_5
    invoke-static {v4}, Lcom/mfluent/asp/b/e;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    .line 137
    invoke-direct {p0, v4}, Lcom/mfluent/asp/b/e;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    :cond_6
    :goto_1
    if-nez v0, :cond_8

    .line 150
    :try_start_1
    const-string v0, "ssg/websvc/multisp/account/getSPList.json"

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    const-string v4, "<Request><UserID>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-static {v2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string v2, "</UserID><Type>Storage</Type>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string v2, "<IncludeLoginSP>true</IncludeLoginSP>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    const-string v2, "<MCC>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    iget-object v2, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/hybrid/update/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const-string v2, "</MCC>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    const-string v2, "<MNC>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    iget-object v2, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/hybrid/update/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const-string v2, "</MNC>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string v2, "<CSC>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string v2, "</CSC>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string v2, "</Request>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v11, :cond_7

    .line 178
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::getStorageProviderList: response = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_7
    if-eqz v0, :cond_d

    invoke-static {v0}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 183
    invoke-direct {p0, v0}, Lcom/mfluent/asp/b/e;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 198
    :cond_8
    :goto_2
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/overrideCatalogHost.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 201
    if-eqz v1, :cond_1

    .line 203
    :try_start_2
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v12, :cond_9

    .line 204
    const-string v1, "mfl_StorageGatewayManager"

    const-string v2, "::getStorageProviderList: OVERRIDE: using LOCAL CatalogHost file"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_9
    invoke-static {v0}, Lcom/mfluent/asp/b/e;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "Response"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "CatalogHost"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "CatalogHost"

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 208
    :catch_0
    move-exception v0

    .line 210
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v10, :cond_1

    .line 211
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getStorageProviderList: overrideCatalogHostFile failed because "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    :cond_a
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 116
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "files in "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    if-eqz v4, :cond_4

    .line 118
    array-length v5, v4

    move v0, v1

    :goto_3
    if-ge v0, v5, :cond_4

    aget-object v6, v4, v0

    .line 119
    const-string v7, "mfl_StorageGatewayManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "- "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_b

    .line 121
    const-string v6, "mfl_StorageGatewayManager"

    const-string v7, "***** The file does not exist!!!"

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 138
    :catch_1
    move-exception v0

    .line 140
    sget-object v4, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    if-gt v4, v10, :cond_c

    .line 141
    const-string v4, "mfl_StorageGatewayManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getStorageProviderList: override storageProviderList failed because "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move v0, v1

    .line 143
    goto/16 :goto_1

    .line 185
    :cond_d
    :try_start_3
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v10, :cond_8

    .line 186
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::getStorageProviderList:Error getting storage provider list. Response from server was: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2

    .line 190
    :catch_2
    move-exception v0

    .line 191
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 192
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v10, :cond_8

    .line 193
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getStorageProviderList: getting storageProviderList failed because "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method

.method public final createAccount(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x2

    .line 866
    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 867
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_0

    .line 870
    const-string v0, "mfl_StorageGatewayManager"

    const-string v2, "::createAccount:[START] ##"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v0

    .line 875
    if-nez v0, :cond_2

    .line 876
    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_ALREADY_SIGNED_IN:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 936
    :cond_1
    :goto_0
    return-object v1

    .line 879
    :cond_2
    const-string v2, "ssg/websvc/sugarsync/account/create.json"

    .line 883
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<Request><UserID>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</UserID><ID>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</ID><Password>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</Password></Request>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 885
    invoke-virtual {p0, v2, v0}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 887
    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 889
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 890
    const-string v0, "Response"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 892
    if-eqz v0, :cond_b

    .line 893
    const-string v2, "LoginPeopleID"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 895
    if-eqz v0, :cond_b

    .line 896
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 931
    :goto_1
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_a

    .line 932
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::createAccount: response = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 901
    :cond_3
    :try_start_1
    const-string v2, "/error/code/text()"

    invoke-static {v0, v2}, Lcom/mfluent/asp/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 903
    const-string v2, "23210"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 904
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_DUPLICATE_ID:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    goto :goto_1

    .line 905
    :cond_4
    const-string v2, "23211"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 906
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_INVALID_EMAIL:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    goto :goto_1

    .line 908
    :cond_5
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 912
    :catch_0
    move-exception v0

    .line 913
    :try_start_2
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_6

    .line 914
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::createAccount:Failed to parse response. ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] because "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 931
    :cond_6
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_1

    .line 932
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::createAccount: response = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 917
    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 919
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_7

    .line 920
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::createAccount: network problem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    :cond_7
    new-instance v0, Ljava/net/ConnectException;

    const-string v2, "failed to create Storage account due to network problem"

    invoke-direct {v0, v2}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 931
    :catchall_0
    move-exception v0

    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_8

    .line 932
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::createAccount: response = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v0

    .line 925
    :catch_2
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 927
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_9

    .line 928
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::createAccount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 931
    :cond_9
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_1

    .line 932
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::createAccount: response = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    move-object v1, v0

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public final getOAuthURL(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x6

    const/4 v8, 0x2

    .line 952
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_0

    .line 953
    const-string v0, "mfl_StorageGatewayManager"

    const-string v2, "::getOAuthURL:[START]"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v0

    .line 958
    if-nez v0, :cond_1

    .line 1046
    :goto_0
    return-object v1

    .line 962
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ssg/websvc/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/auth/login.json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 963
    const-string v2, ""

    .line 967
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 968
    const-string v5, "<Request><UserID>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 969
    invoke-static {v0}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 970
    const-string v0, "</UserID>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 973
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 975
    const-string v5, "mfl_StorageGatewayManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::getOAuthURL:langCode : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    if-eqz v0, :cond_2

    .line 978
    const-string v5, "<LangCode>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 979
    invoke-static {v0}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 980
    const-string v0, "</LangCode>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    :cond_2
    if-eqz p2, :cond_3

    .line 984
    const-string v0, "<CallbackURL>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 985
    invoke-static {p2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 986
    const-string v0, "</CallbackURL>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 989
    :cond_3
    const-string v0, "</Request>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 991
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 996
    if-eqz v2, :cond_9

    invoke-static {v2}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 998
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 999
    const-string v3, "Response"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 1001
    if-eqz v3, :cond_9

    .line 1003
    new-instance v0, Lcom/mfluent/asp/b/f;

    invoke-direct {v0}, Lcom/mfluent/asp/b/f;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1012
    :try_start_1
    const-string v1, "LoginPeopleID"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->a(Ljava/lang/String;)V

    .line 1013
    const-string v1, "DisplayName"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->b(Ljava/lang/String;)V

    .line 1014
    const-string v1, "LoginUserName"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->c(Ljava/lang/String;)V

    .line 1015
    const-string v1, "TotalAmount"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->d(Ljava/lang/String;)V

    .line 1016
    const-string v1, "Used"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->e(Ljava/lang/String;)V

    .line 1017
    const-string v1, "UploadLimit"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->f(Ljava/lang/String;)V

    .line 1019
    const-string v1, "OAuth"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1021
    if-eqz v1, :cond_4

    .line 1022
    const-string v3, "URL"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->g(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1041
    :cond_4
    :goto_1
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_5

    .line 1042
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getOAuthURL:retrieved the login result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_2
    move-object v1, v0

    .line 1046
    goto/16 :goto_0

    .line 1028
    :catch_0
    move-exception v0

    move-object v1, v2

    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 1030
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v9, :cond_6

    .line 1031
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getOAuthURL: network problem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    :cond_6
    new-instance v0, Ljava/net/ConnectException;

    const-string v2, "failed to get OAuthURL due to network problem"

    invoke-direct {v0, v2}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1041
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_7

    .line 1042
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getOAuthURL:retrieved the login result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v0

    .line 1036
    :catch_1
    move-exception v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    .line 1037
    :goto_4
    :try_start_3
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v9, :cond_8

    .line 1038
    const-string v3, "mfl_StorageGatewayManager"

    const-string v4, "::getOAuthURL: failed to retrieve the token"

    invoke-static {v3, v4, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1041
    :cond_8
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_5

    .line 1042
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getOAuthURL:retrieved the login result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1041
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1036
    :catch_2
    move-exception v1

    goto :goto_4

    :cond_9
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public final getToken(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageAuthenticationInfo;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    .line 450
    const-string v4, ""

    .line 451
    const/4 v3, 0x0

    .line 453
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v5, 0x2

    if-gt v2, v5, :cond_0

    .line 454
    const-string v2, "mfl_StorageGatewayManager"

    const-string v5, "::getStorageGatewayToken:[START] ##"

    invoke-static {v2, v5}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v2

    .line 459
    if-nez v2, :cond_1

    .line 460
    new-instance v2, Ljava/net/ConnectException;

    const-string v3, "failed to get token due to not authorized to access storage gateway"

    invoke-direct {v2, v3}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 463
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ssg/websvc/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/auth/getToken.json"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 467
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "<Request><UserID>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "</UserID></Request>"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 468
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v2}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 470
    if-eqz v4, :cond_b

    invoke-static {v4}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 472
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 473
    const-string v5, "Response"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 475
    if-eqz v2, :cond_b

    .line 477
    const-string v5, "AccessToken"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 478
    const-string v6, "TokenSecret"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 479
    const-string v7, "ExpireTime"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 480
    const-string v8, "LastLoginTime"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 482
    const-string v9, "AccountID"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 484
    new-instance v10, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-direct {v10, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 486
    if-eqz v5, :cond_b

    .line 487
    new-instance v2, Lcom/mfluent/asp/b/d;

    invoke-direct {v2}, Lcom/mfluent/asp/b/d;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    :try_start_1
    invoke-virtual {v2, v5}, Lcom/mfluent/asp/b/d;->a(Ljava/lang/String;)V

    .line 490
    invoke-virtual {v2, v6}, Lcom/mfluent/asp/b/d;->b(Ljava/lang/String;)V

    .line 491
    invoke-virtual {v2, v10}, Lcom/mfluent/asp/b/d;->a(Ljava/util/Date;)V

    .line 493
    invoke-static {v8}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 494
    invoke-virtual {v2, v8}, Lcom/mfluent/asp/b/d;->c(Ljava/lang/String;)V

    .line 497
    :cond_2
    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 498
    invoke-virtual {v2, v9}, Lcom/mfluent/asp/b/d;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 539
    :cond_3
    :goto_0
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_4

    .line 540
    if-eqz v2, :cond_5

    .line 541
    const-string v3, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getStorageGatewayToken: token = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/b/d;->getAccessToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :cond_4
    :goto_1
    return-object v2

    .line 543
    :cond_5
    const-string v3, "mfl_StorageGatewayManager"

    const-string v4, "::getStorageGatewayToken: token = NO TOKEN"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 520
    :catch_0
    move-exception v2

    .line 521
    :goto_2
    :try_start_2
    sget-object v5, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_6

    .line 522
    const-string v5, "mfl_StorageGatewayManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::getToken:Failed to parse response. ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "] because "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524
    :cond_6
    new-instance v2, Ljava/net/ConnectException;

    const-string v4, "failed to get token due to JSON parse problem"

    invoke-direct {v2, v4}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 539
    :catchall_0
    move-exception v2

    :goto_3
    sget-object v4, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_7

    .line 540
    if-eqz v3, :cond_a

    .line 541
    const-string v4, "mfl_StorageGatewayManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getStorageGatewayToken: token = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mfluent/asp/b/d;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_7
    :goto_4
    throw v2

    .line 525
    :catch_1
    move-exception v2

    :goto_5
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 527
    sget-object v4, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x6

    if-gt v4, v5, :cond_8

    .line 528
    const-string v4, "mfl_StorageGatewayManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getStorageGatewayToken: network problem : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :cond_8
    new-instance v4, Ljava/net/ConnectException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to get token due to network problem: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 532
    :catch_2
    move-exception v2

    .line 533
    :goto_6
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 534
    sget-object v5, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_9

    .line 535
    const-string v5, "mfl_StorageGatewayManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::getStorageGatewayToken:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_9
    new-instance v4, Ljava/net/ConnectException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to get token due to exception: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 543
    :cond_a
    const-string v3, "mfl_StorageGatewayManager"

    const-string v4, "::getStorageGatewayToken: token = NO TOKEN"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 539
    :catchall_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto/16 :goto_3

    .line 532
    :catch_3
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto :goto_6

    .line 525
    :catch_4
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto/16 :goto_5

    .line 520
    :catch_5
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto/16 :goto_2

    :cond_b
    move-object v2, v3

    goto/16 :goto_0
.end method

.method public final login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x6

    .line 1053
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_0

    .line 1054
    const-string v1, "mfl_StorageGatewayManager"

    const-string v2, "::login:[START]"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v2

    .line 1059
    if-nez v2, :cond_2

    .line 1172
    :cond_1
    :goto_0
    return-object v0

    .line 1063
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "ssg/websvc/"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/auth/login.json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1064
    const-string v1, ""

    .line 1068
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1069
    const-string v5, "<Request><UserID>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1070
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1071
    const-string v2, "</UserID>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1073
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 1074
    const-string v2, "<UserName>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1075
    invoke-static {p2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1076
    const-string v2, "</UserName>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1077
    const-string v2, "<Password>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1078
    invoke-static {p3}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1079
    const-string v2, "</Password>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1082
    :cond_3
    const-string v2, "</Request>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1084
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, v2}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1089
    if-nez v2, :cond_6

    .line 1090
    :try_start_1
    new-instance v0, Ljava/net/ConnectException;

    const-string v1, "No response from server."

    invoke-direct {v0, v1}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1152
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1153
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 1154
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v6, :cond_4

    .line 1155
    const-string v3, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::login: network problem : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    :cond_4
    new-instance v2, Ljava/net/ConnectException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "failed to get login due to network problem because: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1167
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_5

    .line 1168
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::login:retrieved the login result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0

    .line 1092
    :cond_6
    :try_start_3
    invoke-static {v2}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1094
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1095
    const-string v3, "Response"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1097
    if-eqz v1, :cond_7

    .line 1099
    new-instance v0, Lcom/mfluent/asp/b/f;

    invoke-direct {v0}, Lcom/mfluent/asp/b/f;-><init>()V

    .line 1100
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->a(Z)V

    .line 1109
    const-string v3, "LoginPeopleID"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->a(Ljava/lang/String;)V

    .line 1110
    const-string v3, "DisplayName"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->b(Ljava/lang/String;)V

    .line 1111
    const-string v3, "LoginUserName"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->c(Ljava/lang/String;)V

    .line 1112
    const-string v3, "TotalAmount"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->d(Ljava/lang/String;)V

    .line 1113
    const-string v3, "Used"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->e(Ljava/lang/String;)V

    .line 1114
    const-string v3, "UploadLimit"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/f;->f(Ljava/lang/String;)V

    .line 1116
    const-string v3, "OAuth"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1118
    if-eqz v1, :cond_7

    .line 1119
    const-string v3, "URL"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/f;->g(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1167
    :cond_7
    :goto_3
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_1

    .line 1168
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::login:retrieved the login result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1125
    :cond_8
    :try_start_4
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_9

    .line 1126
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::login:Login Failed. Response: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    :cond_9
    const-string v0, "/error/code/text()"

    invoke-static {v2, v0}, Lcom/mfluent/asp/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1131
    new-instance v1, Lcom/mfluent/asp/b/f;

    invoke-direct {v1}, Lcom/mfluent/asp/b/f;-><init>()V

    .line 1132
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/b/f;->a(Z)V

    .line 1134
    const-string v3, "21113"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1135
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/f;->a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V

    move-object v0, v1

    goto :goto_3

    .line 1136
    :cond_a
    const-string v3, "21111"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1137
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/f;->a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V

    move-object v0, v1

    goto :goto_3

    .line 1138
    :cond_b
    const-string v3, "21112"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1139
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/f;->a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V

    move-object v0, v1

    goto :goto_3

    .line 1140
    :cond_c
    const-string v3, "21114"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1141
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/f;->a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V

    move-object v0, v1

    goto/16 :goto_3

    .line 1142
    :cond_d
    const-string v3, "21120"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1143
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->OTHER_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/f;->a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V

    move-object v0, v1

    goto/16 :goto_3

    .line 1145
    :cond_e
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_f

    .line 1146
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::login:Unknown Error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    :cond_f
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->OTHER_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/f;->a(Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v0, v1

    goto/16 :goto_3

    .line 1160
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 1161
    :goto_4
    :try_start_5
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_10

    .line 1162
    const-string v1, "mfl_StorageGatewayManager"

    const-string v3, "::login: failed to login"

    invoke-static {v1, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1165
    :cond_10
    new-instance v1, Ljava/net/ConnectException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "failed to get login due to network problem because: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1167
    :catchall_1
    move-exception v0

    goto/16 :goto_2

    .line 1160
    :catch_2
    move-exception v0

    goto :goto_4

    .line 1152
    :catch_3
    move-exception v0

    goto/16 :goto_1
.end method

.method public final logout(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x6

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 641
    .line 642
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ssg/websvc/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/auth/logout.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 647
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v2

    .line 648
    if-nez v2, :cond_1

    .line 698
    :cond_0
    :goto_0
    return-object v0

    .line 654
    :cond_1
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<Request><UserID>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</UserID></Request>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 655
    invoke-virtual {p0, v1, v2}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 658
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 660
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 661
    const-string v1, "Response"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 663
    if-eqz v1, :cond_2

    .line 668
    const-string v2, "Logined"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 686
    :cond_2
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_3

    .line 687
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::logout: logoutStatus = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    :cond_3
    iget-object v1, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 691
    invoke-virtual {v1, p1}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v2

    .line 692
    if-eqz v2, :cond_0

    .line 693
    invoke-virtual {v2, v6}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 694
    invoke-virtual {v1, v2}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    goto :goto_0

    .line 672
    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 674
    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_4

    .line 675
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::logout: network problem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    :cond_4
    new-instance v1, Ljava/net/ConnectException;

    const-string v2, "failed to logout due to network problem"

    invoke-direct {v1, v2}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 686
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v7, :cond_5

    .line 687
    const-string v2, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::logout: logoutStatus = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 691
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v2

    .line 692
    if-eqz v2, :cond_6

    .line 693
    invoke-virtual {v2, v6}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 694
    invoke-virtual {v0, v2}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    .line 696
    :cond_6
    throw v1

    .line 680
    :catch_1
    move-exception v1

    .line 681
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 682
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v5, :cond_7

    .line 683
    const-string v3, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::logout:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 686
    :cond_7
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_8

    .line 687
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::logout: logoutStatus = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    :cond_8
    iget-object v1, p0, Lcom/mfluent/asp/b/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 691
    invoke-virtual {v1, p1}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v2

    .line 692
    if-eqz v2, :cond_0

    .line 693
    invoke-virtual {v2, v6}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 694
    invoke-virtual {v1, v2}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    goto/16 :goto_0
.end method

.method public final verifyToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v6, 0x6

    .line 553
    .line 554
    const/4 v2, 0x0

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "ssg/websvc/"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/auth/verifyToken.json"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 559
    invoke-direct {p0}, Lcom/mfluent/asp/b/e;->c()Ljava/lang/String;

    move-result-object v3

    .line 560
    if-nez v3, :cond_1

    .line 627
    :cond_0
    :goto_0
    return v1

    .line 566
    :cond_1
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 568
    const-string v5, "<Request><UserID>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    invoke-static {v3}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    const-string v3, "</UserID>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    if-eqz p2, :cond_2

    .line 573
    const-string v3, "<OauthToken>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    invoke-static {p2}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    const-string v3, "</OauthToken>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    :cond_2
    const-string v3, "<OauthVerifier>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    invoke-static {p3}, Lorg/apache/commons/lang3/StringEscapeUtils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    const-string v3, "</OauthVerifier></Request>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 584
    if-eqz v2, :cond_4

    :try_start_1
    invoke-static {v2}, Lcom/mfluent/asp/b/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 586
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 587
    const-string v3, "Response"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 589
    if-eqz v0, :cond_b

    .line 591
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 622
    :cond_3
    :goto_2
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_0

    .line 623
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::verifyToken: result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 593
    :cond_4
    if-eqz v2, :cond_9

    .line 594
    :try_start_2
    const-string v3, "/error/code/text()"

    const-string v0, ""

    invoke-static {v2, v3}, Lcom/mfluent/asp/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_5

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 595
    :cond_5
    const-string v3, "21114"

    invoke-static {v0, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 596
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException;

    invoke-direct {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException;-><init>()V

    throw v0
    :try_end_2
    .catch Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 606
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 622
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_6

    .line 623
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::verifyToken: result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v0

    .line 598
    :cond_7
    :try_start_4
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_3

    .line 599
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::verifyToken:ERROR: Unable to verify token. Response was: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 608
    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 610
    sget-object v1, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_8

    .line 611
    const-string v1, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::verifyToken: network problem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_8
    new-instance v0, Ljava/net/ConnectException;

    const-string v1, "failed to verify token due to network problem"

    invoke-direct {v0, v1}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 622
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 602
    :cond_9
    :try_start_6
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_3

    .line 603
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::verifyToken:ERROR: Unable to verify token. Response was: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_2

    .line 616
    :catch_2
    move-exception v0

    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 618
    sget-object v3, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v6, :cond_a

    .line 619
    const-string v3, "mfl_StorageGatewayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::verifyToken:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 622
    :cond_a
    sget-object v0, Lcom/mfluent/asp/b/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_0

    .line 623
    const-string v0, "mfl_StorageGatewayManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::verifyToken: result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 606
    :catch_3
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method
