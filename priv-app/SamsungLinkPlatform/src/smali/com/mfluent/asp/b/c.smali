.class public Lcom/mfluent/asp/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final b:J

.field private static c:Lcom/mfluent/asp/b/c;

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/common/util/prefs/LongPersistedField;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    const-class v0, Lcom/mfluent/asp/b/c;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/b/c;->a:Lorg/slf4j/Logger;

    .line 31
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/b/c;->b:J

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mfluent/asp/b/c;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/b/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 43
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/b/c;->h:Ljava/util/concurrent/Executor;

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/b/c;->e:Landroid/content/Context;

    .line 60
    const-string v0, "StatsManager"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/b/c;->g:Landroid/content/SharedPreferences;

    .line 61
    return-void
.end method

.method static synthetic a()J
    .locals 2

    .prologue
    .line 27
    sget-wide v0, Lcom/mfluent/asp/b/c;->b:J

    return-wide v0
.end method

.method static synthetic a(Lcom/mfluent/asp/b/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/mfluent/asp/b/c;->e:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/mfluent/asp/b/c;
    .locals 2

    .prologue
    .line 46
    if-nez p0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "context is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    sget-object v1, Lcom/mfluent/asp/b/c;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/b/c;->c:Lcom/mfluent/asp/b/c;

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Lcom/mfluent/asp/b/c;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/b/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/b/c;->c:Lcom/mfluent/asp/b/c;

    .line 54
    :cond_1
    sget-object v0, Lcom/mfluent/asp/b/c;->c:Lcom/mfluent/asp/b/c;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(IZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    return-object v0
.end method

.method static synthetic b()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/mfluent/asp/b/c;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 6

    .prologue
    .line 79
    invoke-static {p1, p2}, Lcom/mfluent/asp/b/c;->b(IZ)Ljava/lang/String;

    move-result-object v2

    .line 81
    iget-object v0, p0, Lcom/mfluent/asp/b/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    .line 82
    if-nez v0, :cond_2

    .line 83
    new-instance v1, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    iget-object v0, p0, Lcom/mfluent/asp/b/c;->g:Landroid/content/SharedPreferences;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Long;)V

    .line 84
    iget-object v0, p0, Lcom/mfluent/asp/b/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    .line 85
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 90
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/mfluent/asp/b/c;->b:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    .line 188
    :goto_1
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/b/c;->h:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/mfluent/asp/b/c$1;

    invoke-direct {v2, p0, v1, p1, p2}, Lcom/mfluent/asp/b/c$1;-><init>(Lcom/mfluent/asp/b/c;Lcom/mfluent/asp/common/util/prefs/LongPersistedField;IZ)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-static {p1, v2}, Lcom/mfluent/asp/b/c;->b(IZ)Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v3, p0, Lcom/mfluent/asp/b/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;

    .line 67
    if-nez v0, :cond_0

    move v0, v1

    .line 75
    :goto_0
    return v0

    .line 71
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/prefs/LongPersistedField;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/mfluent/asp/b/c;->b:J

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    move v0, v2

    .line 72
    goto :goto_0

    :cond_1
    move v0, v1

    .line 75
    goto :goto_0
.end method
