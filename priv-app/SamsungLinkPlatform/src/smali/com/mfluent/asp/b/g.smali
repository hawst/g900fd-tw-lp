.class public Lcom/mfluent/asp/b/g;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final b:[Ljava/lang/String;

.field private static c:Lcom/mfluent/asp/b/g;


# instance fields
.field private final d:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/mfluent/asp/b/g;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    .line 50
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "nameKey"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "spName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "isOAuth"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "loginStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "main"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "location"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "jarname"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "smallIcon"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "middleIcon"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "largeIcon"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "logoIcon"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "versionOnServer"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "versionOnDevice"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "supportsSignup"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "country"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "md5sum"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "apiOnServer"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "apiOnDevice"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "filesize"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "sort_key"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "smallIconWhiteTheme"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "smallIconBlackTheme"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "is_forbidden"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/b/g;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "sp.db"

    const/4 v2, 0x0

    const/16 v3, 0xf

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 78
    new-instance v0, Lcom/mfluent/asp/b/g$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/b/g$1;-><init>(Lcom/mfluent/asp/b/g;)V

    iput-object v0, p0, Lcom/mfluent/asp/b/g;->d:Landroid/content/BroadcastReceiver;

    .line 101
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/b/g;->d:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 102
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;
    .locals 3

    .prologue
    .line 88
    const-class v1, Lcom/mfluent/asp/b/g;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 89
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 91
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/b/g;->c:Lcom/mfluent/asp/b/g;

    if-nez v0, :cond_1

    .line 92
    new-instance v0, Lcom/mfluent/asp/b/g;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/b/g;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/b/g;->c:Lcom/mfluent/asp/b/g;

    .line 94
    :cond_1
    sget-object v0, Lcom/mfluent/asp/b/g;->c:Lcom/mfluent/asp/b/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 508
    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 511
    :catch_0
    move-exception v0

    .line 512
    sget-object v1, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    const-string v2, "::cursorCloseHelper:failed to close cursor becuase: {}"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;Lcom/mfluent/asp/b/h;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 266
    const-string v0, "id"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->a(I)V

    .line 267
    const-string v0, "nameKey"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->a(Ljava/lang/String;)V

    .line 268
    const-string v0, "spName"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->b(Ljava/lang/String;)V

    .line 270
    const-string v0, "isOAuth"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p1, v3}, Lcom/mfluent/asp/b/h;->a(Z)V

    .line 276
    :goto_0
    const-string v0, "loginStatus"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    invoke-virtual {p1, v3}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 282
    :goto_1
    const-string v0, "smallIcon"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->k(Ljava/lang/String;)V

    .line 283
    const-string v0, "middleIcon"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->n(Ljava/lang/String;)V

    .line 284
    const-string v0, "largeIcon"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->o(Ljava/lang/String;)V

    .line 285
    const-string v0, "logoIcon"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->p(Ljava/lang/String;)V

    .line 287
    const-string v0, "main"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->d(Ljava/lang/String;)V

    .line 288
    const-string v0, "location"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->e(Ljava/lang/String;)V

    .line 289
    const-string v0, "jarname"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->f(Ljava/lang/String;)V

    .line 290
    const-string v0, "md5sum"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->g(Ljava/lang/String;)V

    .line 291
    const-string v0, "filesize"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/mfluent/asp/b/h;->a(J)V

    .line 292
    const-string v0, "versionOnServer"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->h(Ljava/lang/String;)V

    .line 293
    const-string v0, "versionOnDevice"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->i(Ljava/lang/String;)V

    .line 294
    const-string v0, "apiOnDevice"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->b(I)V

    .line 295
    const-string v0, "apiOnServer"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->c(I)V

    .line 296
    const-string v0, "sort_key"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->d(I)V

    .line 297
    const-string v0, "smallIconWhiteTheme"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->l(Ljava/lang/String;)V

    .line 298
    const-string v0, "smallIconBlackTheme"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->m(Ljava/lang/String;)V

    .line 299
    const-string v0, "is_forbidden"

    invoke-static {p0, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->d(Z)V

    .line 301
    const-string v0, "supportsSignup"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 302
    invoke-virtual {p1, v3}, Lcom/mfluent/asp/b/h;->c(Z)V

    .line 307
    :goto_2
    const-string v0, "country"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->j(Ljava/lang/String;)V

    .line 308
    return-void

    .line 273
    :cond_0
    invoke-virtual {p1, v2}, Lcom/mfluent/asp/b/h;->a(Z)V

    goto/16 :goto_0

    .line 279
    :cond_1
    invoke-virtual {p1, v2}, Lcom/mfluent/asp/b/h;->b(Z)V

    goto/16 :goto_1

    .line 304
    :cond_2
    invoke-virtual {p1, v2}, Lcom/mfluent/asp/b/h;->c(Z)V

    goto :goto_2
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ALTER TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ADD COLUMN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method private static a(Lcom/mfluent/asp/b/h;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v0

    .line 235
    const-string v1, "nameKey"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "spName"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "typeKey"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v0, "isOAuth"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 239
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->c()Z

    move-result v0

    .line 240
    const-string v1, "loginStatus"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 242
    const-string v0, "main"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v0, "location"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v0, "jarname"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v0, "md5sum"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v0, "filesize"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->k()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 248
    const-string v0, "smallIcon"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v0, "middleIcon"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "largeIcon"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v0, "logoIcon"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v0, "versionOnServer"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v0, "versionOnDevice"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v0, "apiOnDevice"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->v()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 255
    const-string v0, "apiOnServer"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->w()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 256
    const-string v0, "supportsSignup"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->n()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 257
    const-string v0, "country"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v0, "sort_key"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->x()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 259
    const-string v0, "smallIconWhiteTheme"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v0, "smallIconBlackTheme"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v0, "is_forbidden"

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->y()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 262
    return-void
.end method

.method private declared-synchronized b(Lcom/mfluent/asp/b/h;)V
    .locals 4

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 421
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 422
    invoke-static {p1, v1}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;Landroid/content/ContentValues;)V

    .line 425
    const-string v2, "providers"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 426
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 427
    long-to-int v0, v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/b/h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    :cond_0
    monitor-exit p0

    return-void

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 464
    const-string v1, "meta"

    const-string v2, "key = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    monitor-exit p0

    return-void

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Lcom/mfluent/asp/b/h;)Lcom/mfluent/asp/b/h;
    .locals 7

    .prologue
    .line 433
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 435
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 437
    invoke-static {p1, v1}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    :try_start_1
    const-string v2, "providers"

    const-string v3, "id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444
    :goto_0
    monitor-exit p0

    return-object p1

    .line 440
    :catch_0
    move-exception v0

    .line 441
    :try_start_2
    sget-object v1, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    const-string v2, "Bad things are happening....{}"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic f()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 328
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    :goto_0
    monitor-exit p0

    return-object v9

    .line 331
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 333
    const-string v1, "providers"

    sget-object v2, Lcom/mfluent/asp/b/g;->b:[Ljava/lang/String;

    const-string v3, "spName = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 336
    if-eqz v1, :cond_2

    .line 337
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    new-instance v0, Lcom/mfluent/asp/b/h;

    invoke-direct {v0}, Lcom/mfluent/asp/b/h;-><init>()V

    .line 339
    invoke-static {v1, v0}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;Lcom/mfluent/asp/b/h;)V

    .line 341
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    move-object v9, v0

    .line 344
    goto :goto_0

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v0, v9

    goto :goto_1

    :cond_2
    move-object v0, v9

    goto :goto_2
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 228
    const-string v1, "DELETE FROM providers"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 229
    const-string v1, "DELETE FROM meta"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 230
    return-void
.end method

.method public final declared-synchronized a(Lcom/mfluent/asp/b/h;)V
    .locals 1

    .prologue
    .line 449
    monitor-enter p0

    if-nez p1, :cond_0

    .line 450
    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 453
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 454
    invoke-direct {p0, p1}, Lcom/mfluent/asp/b/g;->b(Lcom/mfluent/asp/b/h;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    :goto_0
    monitor-exit p0

    return-void

    .line 457
    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/mfluent/asp/b/g;->c(Lcom/mfluent/asp/b/h;)Lcom/mfluent/asp/b/h;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 468
    monitor-enter p0

    if-nez p2, :cond_0

    .line 469
    :try_start_0
    invoke-direct {p0, p1}, Lcom/mfluent/asp/b/g;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    :goto_0
    monitor-exit p0

    return-void

    .line 473
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 474
    const-string v1, "meta"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const-string v3, "key = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 476
    :try_start_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 477
    const-string v3, "value"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 481
    :cond_1
    const-string v3, "key"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v3, "meta"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 488
    :goto_1
    :try_start_3
    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 485
    :cond_2
    :try_start_4
    const-string v3, "meta"

    const-string v4, "key = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 488
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 493
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 495
    const-string v1, "meta"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "value"

    aput-object v3, v2, v6

    const-string v3, "key = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 497
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p2

    .line 501
    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;)V

    .line 503
    :goto_0
    return-object p2

    .line 501
    :cond_0
    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 10

    .prologue
    .line 312
    monitor-enter p0

    const/4 v9, 0x0

    .line 313
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 315
    const-string v1, "providers"

    sget-object v2, Lcom/mfluent/asp/b/g;->b:[Ljava/lang/String;

    const-string v3, "is_forbidden = 0"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 317
    if-eqz v1, :cond_1

    .line 318
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    const/4 v0, 0x1

    .line 321
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :goto_1
    monitor-exit p0

    return v0

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v0, v9

    goto :goto_0

    :cond_1
    move v0, v9

    goto :goto_1
.end method

.method public final declared-synchronized c()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/b/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    monitor-enter p0

    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 349
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 351
    const-string v1, "providers"

    sget-object v2, Lcom/mfluent/asp/b/g;->b:[Ljava/lang/String;

    const-string v3, "loginStatus = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_1

    .line 354
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    new-instance v1, Lcom/mfluent/asp/b/h;

    invoke-direct {v1}, Lcom/mfluent/asp/b/h;-><init>()V

    .line 356
    invoke-static {v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;Lcom/mfluent/asp/b/h;)V

    .line 357
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    sget-object v2, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    const-string v3, "::findLoggedInStorageProviders: getting reg storage: {}"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 362
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    :cond_1
    monitor-exit p0

    return-object v9
.end method

.method public final declared-synchronized d()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/b/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 370
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 372
    const-string v1, "providers"

    sget-object v2, Lcom/mfluent/asp/b/g;->b:[Ljava/lang/String;

    const-string v3, "loginStatus = ? AND is_forbidden = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_1

    .line 383
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    new-instance v1, Lcom/mfluent/asp/b/h;

    invoke-direct {v1}, Lcom/mfluent/asp/b/h;-><init>()V

    .line 385
    invoke-static {v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;Lcom/mfluent/asp/b/h;)V

    .line 386
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    sget-object v2, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    const-string v3, "::findLoggedOutStorageProviders: getting reg storage: {}"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 391
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394
    :cond_1
    monitor-exit p0

    return-object v9
.end method

.method public final declared-synchronized e()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/b/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    monitor-enter p0

    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 399
    invoke-virtual {p0}, Lcom/mfluent/asp/b/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 401
    const-string v1, "providers"

    sget-object v2, Lcom/mfluent/asp/b/g;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_1

    .line 404
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 405
    new-instance v1, Lcom/mfluent/asp/b/h;

    invoke-direct {v1}, Lcom/mfluent/asp/b/h;-><init>()V

    .line 406
    invoke-static {v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/Cursor;Lcom/mfluent/asp/b/h;)V

    .line 407
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    sget-object v2, Lcom/mfluent/asp/b/g;->a:Lorg/slf4j/Logger;

    const-string v3, "::findAllStorageProviders: getting all storage: {}"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 411
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    :cond_1
    monitor-exit p0

    return-object v9
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 147
    const-string v0, "CREATE TABLE providers(id INTEGER PRIMARY KEY AUTOINCREMENT,nameKey TEXT,spName TEXT,displayOrder INTEGER, typeKey TEXT,isOAuth INTEGER,loginStatus INTEGER,main TEXT,location TEXT,jarname TEXT,md5sum TEXT,filesize INTEGER,smallIcon TEXT,middleIcon TEXT,largeIcon TEXT,logoIcon TEXT,versionOnServer TEXT,versionOnDevice TEXT,apiOnServer INTEGER,apiOnDevice INTEGER,supportsSignup INTEGER,country TEXT,sort_key INTEGER,smallIconWhiteTheme TEXT,smallIconBlackTheme TEXT,is_forbidden INTEGER, UNIQUE (nameKey))"

    .line 209
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 212
    const-string v0, "CREATE TABLE meta(id INTEGER PRIMARY KEY AUTOINCREMENT,key TEXT,value TEXT)"

    .line 222
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 138
    const-string v0, "DROP TABLE IF EXISTS providers"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 139
    const-string v0, "DROP TABLE IF EXISTS meta"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/b/g;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 141
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 106
    const/16 v0, 0xb

    if-ge p2, v0, :cond_1

    .line 107
    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/b/g;->onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const/16 v0, 0xc

    if-ge p2, v0, :cond_2

    .line 112
    const-string v0, "providers"

    const-string v1, "filesize INTEGER"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "providers"

    const-string v1, "apiOnDevice INTEGER"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v0, "providers"

    const-string v1, "apiOnServer INTEGER"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_2
    const/16 v0, 0xd

    if-ge p2, v0, :cond_3

    .line 118
    const-string v0, "providers"

    const-string v1, "sort_key INTEGER"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_3
    const/16 v0, 0xe

    if-ge p2, v0, :cond_4

    .line 122
    const-string v0, "providers"

    const-string v1, "smallIconWhiteTheme TEXT"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "providers"

    const-string v1, "smallIconBlackTheme TEXT"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_4
    const/16 v0, 0xf

    if-ge p2, v0, :cond_0

    .line 127
    const-string v0, "providers"

    const-string v1, "is_forbidden INTEGER"

    invoke-static {p1, v0, v1}, Lcom/mfluent/asp/b/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
