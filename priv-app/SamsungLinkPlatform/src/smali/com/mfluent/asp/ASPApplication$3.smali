.class final Lcom/mfluent/asp/ASPApplication$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ASPApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/mfluent/asp/datamodel/t;

.field final synthetic c:Lcom/mfluent/asp/ASPApplication;

.field private d:Z


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ASPApplication;Lcom/mfluent/asp/datamodel/t;)V
    .locals 1

    .prologue
    .line 903
    iput-object p1, p0, Lcom/mfluent/asp/ASPApplication$3;->c:Lcom/mfluent/asp/ASPApplication;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ASPApplication$3;->a:Z

    iput-object p2, p0, Lcom/mfluent/asp/ASPApplication$3;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905
    iget-boolean v0, p0, Lcom/mfluent/asp/ASPApplication$3;->a:Z

    iput-boolean v0, p0, Lcom/mfluent/asp/ASPApplication$3;->d:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 909
    iget-boolean v0, p0, Lcom/mfluent/asp/ASPApplication$3;->d:Z

    if-eqz v0, :cond_5

    .line 910
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication$3;->b:Lcom/mfluent/asp/datamodel/t;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Ljava/util/List;

    move-result-object v0

    .line 913
    if-eqz v0, :cond_0

    .line 914
    iget-object v1, p0, Lcom/mfluent/asp/ASPApplication$3;->c:Lcom/mfluent/asp/ASPApplication;

    invoke-static {v1}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v4

    .line 915
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 916
    invoke-interface {v4, v0}, Lcom/mfluent/asp/filetransfer/d;->b(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v3

    :goto_1
    move v1, v0

    .line 919
    goto :goto_0

    :cond_0
    move v1, v2

    .line 922
    :cond_1
    if-eqz v1, :cond_3

    .line 941
    :cond_2
    :goto_2
    return-void

    .line 927
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication$3;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->g()V

    .line 928
    iput-boolean v2, p0, Lcom/mfluent/asp/ASPApplication$3;->d:Z

    .line 937
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication$3;->c:Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/ASPApplication;->a(Lcom/mfluent/asp/ASPApplication;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 938
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication$3;->c:Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/ASPApplication;->a(Lcom/mfluent/asp/ASPApplication;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 939
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication$3;->c:Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/ASPApplication;->b(Lcom/mfluent/asp/ASPApplication;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_2

    .line 930
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication$3;->c:Lcom/mfluent/asp/ASPApplication;

    invoke-static {}, Lcom/mfluent/asp/ASPApplication;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 932
    iput-boolean v3, p0, Lcom/mfluent/asp/ASPApplication$3;->d:Z

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method
