.class public Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants$StorageTypeNames;,
        Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants$Oauth;
    }
.end annotation


# static fields
.field public static final ASP_LOGLEVEL_PREFS:Ljava/lang/String; = "AspLogLevels"

.field public static final DOCUMENTS_FOLDER:Ljava/lang/String; = "Documents"

.field public static final FILES_FOLDER:Ljava/lang/String; = "Files"

.field public static final HTTP_HEADER_USERAGENT_VALUE:Ljava/lang/String; = "SamsungCloud"

.field public static final MUSIC_FOLDER:Ljava/lang/String; = "Music"

.field public static final NDRIVE_SP_NAME:Ljava/lang/String; = "ndrive"

.field public static final OLD_ROOT_FOLDER_PATH:Ljava/lang/String; = "AllShare Play"

.field public static final PHOTO_FOLDER:Ljava/lang/String; = "Photos"

.field public static final ROOT_FOLDER_PATH:Ljava/lang/String; = "Samsung Link"

.field public static final UNKNOWN_ALBUM_FOLDER_NAME:Ljava/lang/String; = "_unknown_"

.field public static final UNKNOWN_ALBUM_TAG:Ljava/lang/String; = "<unknown>"

.field public static final UNKNOWN_ARTIST_FOLDER_NAME:Ljava/lang/String; = "_unknown_"

.field public static final UNKNOWN_ARTIST_TAG:Ljava/lang/String; = "<unknown>"

.field public static final VIDEO_FOLDER:Ljava/lang/String; = "Videos"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    return-void
.end method
