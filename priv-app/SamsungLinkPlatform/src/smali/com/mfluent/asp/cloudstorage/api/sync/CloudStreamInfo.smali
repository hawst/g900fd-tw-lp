.class public interface abstract Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getContentLength()J
.end method

.method public abstract getContentType()Ljava/lang/String;
.end method

.method public abstract getExtraHeaders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getInputStream()Ljava/io/InputStream;
.end method

.method public abstract getResponseCode()I
.end method

.method public abstract getResponseMessage()Ljava/lang/String;
.end method
