.class public interface abstract Lcom/mfluent/asp/cloudstorage/api/sync/StorageProviderInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getId()I
.end method

.method public abstract getJarname()Ljava/lang/String;
.end method

.method public abstract getLargeIcon()Ljava/lang/String;
.end method

.method public abstract getLocation()Ljava/lang/String;
.end method

.method public abstract getMain()Ljava/lang/String;
.end method

.method public abstract getMiddleIcon()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSmallIcon()Ljava/lang/String;
.end method

.method public abstract getSpName()Ljava/lang/String;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract isLoginStatus()Z
.end method

.method public abstract isOAuth()Z
.end method
