.class public interface abstract Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$EmailNotVerifiedException;,
        Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;,
        Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    }
.end annotation


# static fields
.field public static final SIGNUP_RESULT_KEY:Ljava/lang/String; = "SIGNUP_RESULT_KEY"


# virtual methods
.method public abstract createAccount(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation
.end method

.method public abstract getOAuthURL(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;,
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract getToken(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageAuthenticationInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation
.end method

.method public abstract login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation
.end method

.method public abstract logout(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation
.end method

.method public abstract verifyToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation
.end method
