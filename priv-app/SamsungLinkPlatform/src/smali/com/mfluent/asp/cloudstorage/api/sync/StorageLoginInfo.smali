.class public interface abstract Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;
    }
.end annotation


# virtual methods
.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getLoginFailureReason()Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;
.end method

.method public abstract getLoginPeopleID()Ljava/lang/String;
.end method

.method public abstract getLoginUserName()Ljava/lang/String;
.end method

.method public abstract getOAuthURL()Ljava/lang/String;
.end method

.method public abstract getTotalAmount()Ljava/lang/String;
.end method

.method public abstract getUploadLimit()Ljava/lang/String;
.end method

.method public abstract getUsed()Ljava/lang/String;
.end method

.method public abstract isLoggedIn()Z
.end method
