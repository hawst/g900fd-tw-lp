.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LoginFailureType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

.field public static final enum ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

.field public static final enum BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

.field public static final enum OTHER_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    const-string v1, "BAD_USERNAME_OR_PASSWORD"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    .line 23
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    const-string v1, "ACCOUNT_NOT_ACTIVATED"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    .line 25
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    const-string v1, "OTHER_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->OTHER_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->BAD_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->OTHER_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageLoginInfo$LoginFailureType;

    return-object v0
.end method
