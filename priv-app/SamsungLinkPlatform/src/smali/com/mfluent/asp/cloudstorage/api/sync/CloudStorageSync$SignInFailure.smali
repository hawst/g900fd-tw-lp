.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignInFailure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

.field public static final enum ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

.field public static final enum AUTH_LIMIT:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

.field public static final enum INVALID_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

.field public static final enum OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 194
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    const-string v1, "INVALID_USERNAME_OR_PASSWORD"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->INVALID_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    .line 195
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    const-string v1, "AUTH_LIMIT"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->AUTH_LIMIT:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    .line 196
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    const-string v1, "ACCOUNT_NOT_ACTIVATED"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    .line 197
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    const-string v1, "OTHER_FAILURE"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    .line 193
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->INVALID_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->AUTH_LIMIT:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;
    .locals 1

    .prologue
    .line 193
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    return-object v0
.end method
