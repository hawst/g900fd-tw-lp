.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignInResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

.field public static final enum ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

.field public static final enum AUTH_LIMIT:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

.field public static final enum INVALID_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

.field public static final enum OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

.field public static final enum SIGNIN_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    const-string v1, "SIGNIN_SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->SIGNIN_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    .line 59
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    const-string v1, "INVALID_USERNAME_OR_PASSWORD"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->INVALID_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    .line 62
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    const-string v1, "AUTH_LIMIT"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->AUTH_LIMIT:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    .line 64
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    const-string v1, "ACCOUNT_NOT_ACTIVATED"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    .line 68
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    const-string v1, "OTHER_FAILURE"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    .line 50
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->SIGNIN_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->INVALID_USERNAME_OR_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->AUTH_LIMIT:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignInResult;

    return-object v0
.end method
