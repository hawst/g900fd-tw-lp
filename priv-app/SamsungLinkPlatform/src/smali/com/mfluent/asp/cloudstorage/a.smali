.class public final Lcom/mfluent/asp/cloudstorage/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/io/File;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ldalvik/system/DexClassLoader;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/a;->b:Ljava/util/HashMap;

    return-void
.end method

.method private static a(Lcom/mfluent/asp/b/h;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
    .locals 6

    .prologue
    .line 148
    const/4 v1, 0x0

    .line 150
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :try_start_1
    sget-object v1, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 154
    const-string v1, "mfl_DynamicCloudStorageLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loaded ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] from classpath."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 162
    :cond_0
    :goto_0
    return-object v0

    .line 157
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 158
    :goto_1
    sget-object v2, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_0

    .line 159
    const-string v2, "mfl_DynamicCloudStorageLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to load \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] from class path."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 157
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static declared-synchronized a(Ljava/io/File;)Ldalvik/system/DexClassLoader;
    .locals 2

    .prologue
    .line 56
    const-class v1, Lcom/mfluent/asp/cloudstorage/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/cloudstorage/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldalvik/system/DexClassLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Ljava/io/File;Ldalvik/system/DexClassLoader;)V
    .locals 3

    .prologue
    .line 65
    const-class v1, Lcom/mfluent/asp/cloudstorage/a;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 67
    sget-object v2, Lcom/mfluent/asp/cloudstorage/a;->b:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit v1

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 37
    const/4 v0, 0x0

    .line 39
    if-eqz p0, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_0
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 43
    sget-object v0, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x6

    if-gt v0, v2, :cond_1

    .line 44
    const-string v0, "mfl_DynamicCloudStorageLoader"

    const-string v2, "::newInstanceOf: Cannot load plugin becuase jar name is empty"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v0, v1

    .line 52
    :goto_0
    return v0

    .line 49
    :cond_2
    new-instance v2, Ljava/io/File;

    const-string v3, "cloudplugins"

    invoke-virtual {p1, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 51
    invoke-static {v2}, Lcom/mfluent/asp/cloudstorage/a;->a(Ljava/io/File;)Ldalvik/system/DexClassLoader;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static declared-synchronized b(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
    .locals 10

    .prologue
    const/4 v7, 0x6

    const/4 v9, 0x3

    const/4 v1, 0x0

    .line 71
    const-class v2, Lcom/mfluent/asp/cloudstorage/a;

    monitor-enter v2

    if-nez p0, :cond_1

    move-object v0, v1

    .line 137
    :cond_0
    :goto_0
    monitor-exit v2

    return-object v0

    .line 76
    :cond_1
    :try_start_0
    const-string v0, "cloudplugins"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 78
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 82
    new-instance v5, Ljava/io/File;

    const-string v0, "cloudplugins"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-direct {v5, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 84
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->v()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/16 v6, 0x2b

    if-ne v0, v6, :cond_6

    .line 90
    :try_start_1
    invoke-static {v5}, Lcom/mfluent/asp/cloudstorage/a;->a(Ljava/io/File;)Ldalvik/system/DexClassLoader;

    move-result-object v0

    .line 92
    if-nez v0, :cond_2

    .line 93
    new-instance v0, Ldalvik/system/DexClassLoader;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-direct {v0, v6, v3, v7, v8}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 98
    invoke-static {v5, v0}, Lcom/mfluent/asp/cloudstorage/a;->a(Ljava/io/File;Ldalvik/system/DexClassLoader;)V

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 129
    :goto_1
    if-nez v0, :cond_3

    .line 130
    :try_start_2
    invoke-static {p0}, Lcom/mfluent/asp/cloudstorage/a;->a(Lcom/mfluent/asp/b/h;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    .line 133
    :cond_3
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setApplicationContext(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 107
    :catch_0
    move-exception v0

    :try_start_3
    sget-object v0, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_4

    .line 108
    const-string v0, "mfl_DynamicCloudStorageLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Plugin ["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] not available. Will try again."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    .line 115
    goto :goto_1

    .line 110
    :catch_1
    move-exception v0

    .line 111
    sget-object v3, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v9, :cond_5

    .line 112
    const-string v3, "mfl_DynamicCloudStorageLoader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to load ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_5
    move-object v0, v1

    .line 115
    goto :goto_1

    .line 114
    :catchall_1
    move-exception v0

    :try_start_4
    throw v0

    .line 118
    :cond_6
    sget-object v0, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_8

    .line 119
    const-string v0, "mfl_DynamicCloudStorageLoader"

    const-string v3, "::newInstanceOf: Skipped dynamic plugin loading because API version does not match."

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 123
    :cond_7
    sget-object v0, Lcom/mfluent/asp/cloudstorage/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 124
    const-string v0, "mfl_DynamicCloudStorageLoader"

    const-string v3, "::newInstanceOf: Skipped dynamic plugin loading because jar file does not exist."

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_8
    move-object v0, v1

    goto/16 :goto_1
.end method
