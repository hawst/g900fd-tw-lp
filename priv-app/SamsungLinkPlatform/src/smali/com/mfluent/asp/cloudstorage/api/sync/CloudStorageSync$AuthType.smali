.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AuthType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

.field public static final enum NONE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

.field public static final enum OAUTH10:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

.field public static final enum OAUTH20:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

.field public static final enum USER_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 207
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->NONE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    .line 208
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    const-string v1, "USER_PASSWORD"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->USER_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    .line 209
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    const-string v1, "OAUTH10"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->OAUTH10:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    .line 210
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    const-string v1, "OAUTH20"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->OAUTH20:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    .line 206
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->NONE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->USER_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->OAUTH10:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->OAUTH20:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;
    .locals 1

    .prologue
    .line 206
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    return-object v0
.end method
