.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignupFailures"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

.field public static final enum AccountAlreadyExists:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

.field public static final enum AccountIsNotActivated:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

.field public static final enum InvalidID:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

.field public static final enum InvalidPassword:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 181
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    const-string v1, "AccountAlreadyExists"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->AccountAlreadyExists:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    .line 182
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    const-string v1, "AccountIsNotActivated"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->AccountIsNotActivated:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    .line 183
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    const-string v1, "InvalidID"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->InvalidID:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    .line 184
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    const-string v1, "InvalidPassword"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->InvalidPassword:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    .line 180
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->AccountAlreadyExists:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->AccountIsNotActivated:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->InvalidID:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->InvalidPassword:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;
    .locals 1

    .prologue
    .line 180
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;

    return-object v0
.end method
