.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UploadResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum CANT_CREATE_ALBUM_DIRECTORY:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum CANT_CREATE_ARTIST_DIRECTORY:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum NOT_AUTHORIZED:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum OK:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum OTHER:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum OUT_OF_SPACE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

.field public static final enum TOO_LARGE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 499
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "OK"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OK:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 503
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "NOT_AUTHORIZED"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->NOT_AUTHORIZED:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 508
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "OUT_OF_SPACE"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OUT_OF_SPACE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 513
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "TOO_LARGE"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->TOO_LARGE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 517
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "CANT_CREATE_ARTIST_DIRECTORY"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->CANT_CREATE_ARTIST_DIRECTORY:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 521
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "CANT_CREATE_ALBUM_DIRECTORY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->CANT_CREATE_ALBUM_DIRECTORY:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 525
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    const-string v1, "OTHER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OTHER:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    .line 495
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OK:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->NOT_AUTHORIZED:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OUT_OF_SPACE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->TOO_LARGE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->CANT_CREATE_ARTIST_DIRECTORY:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->CANT_CREATE_ALBUM_DIRECTORY:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OTHER:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 495
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;
    .locals 1

    .prologue
    .line 495
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;
    .locals 1

    .prologue
    .line 495
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    return-object v0
.end method
