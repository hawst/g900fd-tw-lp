.class public final Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants$StorageTypeNames;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StorageTypeNames"
.end annotation


# static fields
.field public static final SERVICE_PROVIDER_BAIDU:Ljava/lang/String; = "baidu"

.field public static final SERVICE_PROVIDER_BOX:Ljava/lang/String; = "box"

.field public static final SERVICE_PROVIDER_DEFAULT:Ljava/lang/String; = "defaultstorage"

.field public static final SERVICE_PROVIDER_DROPBOX:Ljava/lang/String; = "dropbox"

.field public static final SERVICE_PROVIDER_DT:Ljava/lang/String; = "Deutsche Telekom"

.field public static final SERVICE_PROVIDER_FACEBOOK:Ljava/lang/String; = "facebook"

.field public static final SERVICE_PROVIDER_FLICKR:Ljava/lang/String; = "flickr"

.field public static final SERVICE_PROVIDER_FMSTORY:Ljava/lang/String; = "family Story"

.field public static final SERVICE_PROVIDER_GDRIVE:Ljava/lang/String; = "google drive"

.field public static final SERVICE_PROVIDER_MULTI:Ljava/lang/String; = "multi"

.field public static final SERVICE_PROVIDER_NDRIVE:Ljava/lang/String; = "ndrive"

.field public static final SERVICE_PROVIDER_PICASA:Ljava/lang/String; = "picasa"

.field public static final SERVICE_PROVIDER_QZONE:Ljava/lang/String; = "qzone"

.field public static final SERVICE_PROVIDER_SKYDRIVE:Ljava/lang/String; = "skydrive"

.field public static final SERVICE_PROVIDER_SUGARSYNC:Ljava/lang/String; = "sugarsync"

.field public static final SERVICE_PROVIDER_SWIFT:Ljava/lang/String; = "swift"

.field public static final SERVICE_PROVIDER_TWITTER:Ljava/lang/String; = "twitter"

.field public static final SERVICE_PROVIDER_VDISK:Ljava/lang/String; = "vdisk"

.field public static final SERVICE_PROVIDER_WEIBO:Ljava/lang/String; = "weibo"

.field public static final SERVICE_PROVIDER_YOUTUBE:Ljava/lang/String; = "youtube"

.field public static final SERVICE_PROVIDER_YSI:Ljava/lang/String; = "YouSendIt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
