.class public final enum Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignupResults"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_ALREADY_SIGNED_IN:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_DUPLICATE_ID:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_INVALID_EMAIL:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_NETWORK_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

.field public static final enum RESULT_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 29
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_DUPLICATE_ID"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_DUPLICATE_ID:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 32
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_ACCOUNT_NOT_ACTIVATED"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 35
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_ALREADY_SIGNED_IN"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_ALREADY_SIGNED_IN:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 37
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_INVALID_EMAIL"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_INVALID_EMAIL:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 39
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_OTHER_FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 41
    new-instance v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    const-string v1, "RESULT_NETWORK_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_NETWORK_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 20
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_SUCCESS:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_DUPLICATE_ID:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_ACCOUNT_NOT_ACTIVATED:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_ALREADY_SIGNED_IN:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_INVALID_EMAIL:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_OTHER_FAILURE:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->RESULT_NETWORK_ERROR:Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->$VALUES:[Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    invoke-virtual {v0}, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    return-object v0
.end method
