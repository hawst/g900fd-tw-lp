.class public interface abstract Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;,
        Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;,
        Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;,
        Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignupFailures;
    }
.end annotation


# static fields
.field public static final ASP_OAUTH_CALLBACK_KEYWORD:Ljava/lang/String; = "aspoauth"

.field public static final CLOUD_AUTHENTICATION_FAILURE:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_FAILURE"

.field public static final CLOUD_AUTHENTICATION_FAILURE_EMAIL_NOT_VERIFIED:Ljava/lang/String; = "EmailNotVerified"

.field public static final CLOUD_AUTHENTICATION_FAILURE_ERROR_CODE:Ljava/lang/String; = "errorCode"

.field public static final CLOUD_AUTHENTICATION_SUCCESS:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_SUCCESS"

.field public static final CLOUD_AUTHENTICATION_UNKNOWN:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_UNKNOWN"

.field public static final CLOUD_INTERFACE_VERSION:I = 0x2b

.field public static final CLOUD_LAUNCH_OAUTH1_BROWSER:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_LAUNCH_OAUTH1_BROWSER"

.field public static final CLOUD_OAUTH1_RESPONSE:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_OAUTH1_RESPONSE"

.field public static final CLOUD_SIGNIN:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_SIGNIN"

.field public static final CLOUD_SIGNUP_FAILED:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_SIGNUP_FAILED"

.field public static final CLOUD_SIGNUP_SUCCESS:Ljava/lang/String; = "com.mfluent.asp.sync.CLOUD_SIGNUP_SUCCESS"

.field public static final OAUTH1_CALLBACK_FILTER:Ljava/lang/String; = "OAUTH1_CALLBACK_FILTER"

.field public static final OAUTH1_CLEAR_CACHE:Ljava/lang/String; = "OAUTH1_CLEAR_CACHE"

.field public static final OAUTH1_RESPONSE_URI:Ljava/lang/String; = "OAUTH1_RESPONSE_URI"

.field public static final OAUTH1_URI:Ljava/lang/String; = "OAUTH1_URI"

.field public static final PROVIDER_CONFIGURATION:Ljava/lang/String; = "com.mfluent.asp.syn.PROVIDER_CONFIGURATION"

.field public static final PROVIDER_CONFIGURATION_COUNTRY_CODE:Ljava/lang/String; = "countryCode"

.field public static final PROVIDER_CONFIGURATION_COUNTRY_CODE_JAPAN:Ljava/lang/String; = "JP"

.field public static final SIGNIN_ERROR_KEY:Ljava/lang/String; = "com.mfluent.asp.sync.SIGNIN_ERROR_KEY"

.field public static final SIGNUP_FAILURE_TYPE:Ljava/lang/String; = "signup_failure_type"

.field public static final UI_STATE:Ljava/lang/String; = "com.fluent.asp.syn.UI_STATE"

.field public static final UI_STATE_CAN_LAUNCH_OAUTH:Ljava/lang/String; = "com.fluent.asp.syn.CAN_LAUNCH_OAUTH"


# virtual methods
.method public abstract deleteFile(ILjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract endUploadBatch()V
.end method

.method public abstract finishedDeleteGroup()Z
.end method

.method public abstract getAuthType()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;
.end method

.method public abstract getCloudStorageFileBrowser()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract getFile(ILjava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getFile(Lcom/mfluent/asp/common/datamodel/ASPFile;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getFile(Lcom/mfluent/asp/common/datamodel/ASPFile;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getFile(Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getFile(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getIntentFilters()[Landroid/content/IntentFilter;
.end method

.method public abstract getMaximumFileSize()J
.end method

.method public abstract getStorageGatewayFileId(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getThumbnail(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getVersion()I
.end method

.method public abstract init()V
.end method

.method public abstract isRangeDownloadSupported()Z
.end method

.method public abstract onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public abstract reset()V
.end method

.method public abstract setApplicationContext(Landroid/content/Context;)V
.end method

.method public abstract setCloudDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V
.end method

.method public abstract setContentProvider(Landroid/content/ContentResolver;)V
.end method

.method public abstract setDataModel(Lcom/mfluent/asp/common/datamodel/CloudDataModel;)V
.end method

.method public abstract setPreferredThumbnailSize(II)V
.end method

.method public abstract setStorageAppDelegate(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageAppDelegate;)V
.end method

.method public abstract setStorageGatewayHelper(Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;)V
.end method

.method public abstract signup(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;
.end method

.method public abstract startUploadBatch()V
.end method

.method public abstract supportSignUp()Z
.end method

.method public abstract sync()V
.end method

.method public abstract upload(Lcom/mfluent/asp/common/datamodel/CloudDevice;Ljava/io/File;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;
.end method
