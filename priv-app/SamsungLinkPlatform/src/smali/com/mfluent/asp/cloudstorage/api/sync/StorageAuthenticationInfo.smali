.class public interface abstract Lcom/mfluent/asp/cloudstorage/api/sync/StorageAuthenticationInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getAccessToken()Ljava/lang/String;
.end method

.method public abstract getAccessTokenSecret()Ljava/lang/String;
.end method

.method public abstract getAccountID()Ljava/lang/String;
.end method

.method public abstract getExpiry()Ljava/util/Date;
.end method
