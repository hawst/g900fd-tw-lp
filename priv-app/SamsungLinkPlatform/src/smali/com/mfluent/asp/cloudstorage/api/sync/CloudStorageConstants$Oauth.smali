.class public final Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants$Oauth;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Oauth"
.end annotation


# static fields
.field public static final CALLBACKURL:Ljava/lang/String; = "CallbacdkURL"

.field public static final HEADER_AUTH:Ljava/lang/String; = "Authorization"

.field public static final HMAC_SHA1:Ljava/lang/String; = "HMAC-SHA1"

.field public static final PARAM_CONSUMER_KEY:Ljava/lang/String; = "oauth_consumer_key="

.field public static final PARAM_NONCE:Ljava/lang/String; = "oauth_nonce="

.field public static final PARAM_REALM:Ljava/lang/String; = "OAuth realm="

.field public static final PARAM_SIGNATRUE:Ljava/lang/String; = "oauth_signature="

.field public static final PARAM_SIGNATURE_METHOD:Ljava/lang/String; = "oauth_signature_method="

.field public static final PARAM_TIMESTAMP:Ljava/lang/String; = "oauth_timestamp="

.field public static final PARAM_TOKEN:Ljava/lang/String; = "oauth_token="

.field public static final TOKEN:Ljava/lang/String; = "Token"

.field public static final TOKENSECRET:Ljava/lang/String; = "TokenSecret"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
