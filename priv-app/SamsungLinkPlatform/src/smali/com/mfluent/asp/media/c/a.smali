.class public final Lcom/mfluent/asp/media/c/a;
.super Lcom/mfluent/asp/media/c/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/c/a$a;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/c/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/c/b;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V

    .line 47
    return-void
.end method


# virtual methods
.method protected final a()Lcom/mfluent/asp/media/c/b$a;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x3

    const/4 v2, 0x1

    const/4 v10, 0x2

    const/4 v1, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/a;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v6

    .line 62
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceId()I

    move-result v3

    int-to-long v8, v3

    invoke-virtual {v0, v8, v9}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    .line 63
    if-nez v7, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find device specified for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v0

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceThumbWidth()I

    move-result v3

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceThumbHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-gt v0, v3, :cond_1

    .line 73
    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbUri()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    .line 79
    :goto_0
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No remote uri specified for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getRemoteUri()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 77
    goto :goto_0

    .line 85
    :cond_2
    :try_start_0
    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v8, "UTF-8"

    invoke-static {v4, v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 90
    :goto_1
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    const-string v8, "/%s?relay=%s&mtime=%s%s&fileName=%s&isThumbnail=%s&rotation=%s&orientation=%s"

    const/16 v9, 0x8

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v3, v9, v1

    const-string v3, "ON"

    aput-object v3, v9, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v10

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&resize="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/a;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/a;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v9, v11

    const/4 v0, 0x4

    aput-object v4, v9, v0

    const/4 v2, 0x5

    sget-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "FALSE"

    :goto_3
    aput-object v0, v9, v2

    const/4 v0, 0x6

    const-string v2, "OFF"

    aput-object v2, v9, v0

    const/4 v0, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v0

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    sget-object v1, Lcom/mfluent/asp/media/c/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v10, :cond_3

    .line 103
    const-string v1, "mfl_ASP10RemoteThumbnailGetter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Requesting thumbnail: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_3
    new-instance v3, Lpcloud/net/a/a/a;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lpcloud/net/a/a/a;-><init>(Ljava/lang/String;)V

    .line 107
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 112
    :try_start_1
    invoke-interface {v3, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/mfluent/asp/media/c/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v10, :cond_4

    .line 114
    const-string v1, "mfl_ASP10RemoteThumbnailGetter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Got response status line: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_4
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_8

    .line 118
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 120
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 121
    :try_start_2
    new-instance v2, Lcom/mfluent/asp/media/c/a$a;

    invoke-direct {v2, v1, v3}, Lcom/mfluent/asp/media/c/a$a;-><init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 122
    :try_start_3
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/media/c/a$a;->a(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v5, v1

    .line 131
    :goto_4
    if-nez v2, :cond_5

    .line 132
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 133
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 137
    :cond_5
    return-object v2

    .line 87
    :catch_0
    move-exception v4

    const-string v4, ""

    goto/16 :goto_1

    .line 90
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    :cond_7
    const-string v0, "TRUE"

    goto/16 :goto_3

    .line 125
    :cond_8
    :try_start_4
    sget-object v1, Lcom/mfluent/asp/media/c/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v11, :cond_9

    .line 126
    const-string v1, "mfl_ASP10RemoteThumbnailGetter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Received non-ok status for thumbnail http request: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_9
    move-object v2, v5

    goto :goto_4

    .line 131
    :catchall_0
    move-exception v0

    move-object v1, v5

    :goto_5
    if-nez v5, :cond_a

    .line 132
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 133
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_a
    throw v0

    .line 131
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v5, v2

    goto :goto_5
.end method
