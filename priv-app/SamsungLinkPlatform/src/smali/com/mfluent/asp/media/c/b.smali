.class public abstract Lcom/mfluent/asp/media/c/b;
.super Lcom/mfluent/asp/media/c/f;
.source "SourceFile"

# interfaces
.implements Landroid/content/ContentProvider$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/c/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/media/c/f;",
        "Landroid/content/ContentProvider$PipeDataWriter",
        "<",
        "Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:I

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;

.field private final e:Lcom/mfluent/asp/media/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/mfluent/asp/media/c/b;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/media/c/b;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/mfluent/asp/media/c/f;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    .line 40
    iput-object p2, p0, Lcom/mfluent/asp/media/c/b;->e:Lcom/mfluent/asp/media/d;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/media/c/b;->b:I

    .line 42
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/b;->d:Ljava/util/concurrent/locks/Condition;

    .line 44
    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v4

    move-object v0, p1

    move-object v2, v1

    move-object v3, v1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 110
    iget-object v2, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 112
    :goto_0
    :try_start_0
    iget v2, p0, Lcom/mfluent/asp/media/c/b;->b:I

    if-nez v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/mfluent/asp/media/c/b;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 116
    :cond_0
    :try_start_1
    iget v2, p0, Lcom/mfluent/asp/media/c/b;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 117
    if-eqz v0, :cond_1

    .line 118
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 126
    if-nez v1, :cond_2

    .line 127
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 130
    :cond_2
    return-object v1

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method private a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 53
    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/media/c/b;->b:I

    if-eq v0, p1, :cond_0

    .line 54
    iput p1, p0, Lcom/mfluent/asp/media/c/b;->b:I

    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 59
    return-void

    .line 58
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/c/b;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;ZZZJ)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x10000000

    .line 71
    .line 73
    if-nez p2, :cond_0

    if-eqz p4, :cond_4

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->e:Lcom/mfluent/asp/media/d;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 76
    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 80
    :goto_0
    if-nez v0, :cond_1

    if-nez p4, :cond_1

    .line 81
    if-nez p3, :cond_2

    .line 82
    invoke-direct {p0, p1}, Lcom/mfluent/asp/media/c/b;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 98
    :cond_1
    :goto_1
    if-nez v0, :cond_3

    .line 99
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to retrieve thumbnail for ImageInfo: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->c()Lcom/mfluent/asp/media/c/d;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 92
    :goto_2
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    invoke-virtual {v1}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 87
    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v2

    goto :goto_2

    .line 102
    :cond_3
    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method protected abstract a()Lcom/mfluent/asp/media/c/b$a;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public final b()Lcom/mfluent/asp/media/c/d;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->e:Lcom/mfluent/asp/media/d;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Lcom/mfluent/asp/media/c/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 172
    const/4 v1, 0x0

    .line 176
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->a()Lcom/mfluent/asp/media/c/b$a;

    move-result-object v1

    .line 177
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->e:Lcom/mfluent/asp/media/d;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-virtual {v1}, Lcom/mfluent/asp/media/c/b$a;->a()J

    move-result-wide v4

    invoke-virtual {v0, v2, v1, v4, v5}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Ljava/io/InputStream;J)Lcom/mfluent/asp/media/c/d;

    .line 178
    iget-object v0, p0, Lcom/mfluent/asp/media/c/b;->e:Lcom/mfluent/asp/media/d;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 180
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 183
    return-object v0

    .line 180
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v0
.end method

.method public synthetic writeDataToPipe(Landroid/os/ParcelFileDescriptor;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 23
    const/16 v1, 0x2000

    new-array v4, v1, [B

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/b;->a()Lcom/mfluent/asp/media/c/b$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    :try_start_2
    invoke-virtual {v3, v4}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/mfluent/asp/media/c/b;->a(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v4, v2, v0}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v3

    :goto_1
    :try_start_3
    sget-object v3, Lcom/mfluent/asp/media/c/b;->a:Lorg/slf4j/Logger;

    const-string v4, "Trouble opening remote thumbnail end of pipe."

    invoke-interface {v3, v4, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const/4 v0, 0x2

    :try_start_4
    invoke-direct {p0, v0}, Lcom/mfluent/asp/media/c/b;->a(I)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :goto_2
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    :goto_3
    return-void

    :cond_1
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_4
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    :catch_1
    move-exception v0

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method
