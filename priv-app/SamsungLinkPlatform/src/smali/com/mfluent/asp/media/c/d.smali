.class public final Lcom/mfluent/asp/media/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

.field private final b:Ljava/io/File;

.field private final c:Ljava/io/File;

.field private final d:J


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    iput-object v0, p0, Lcom/mfluent/asp/media/c/d;->a:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 35
    iput-object p2, p0, Lcom/mfluent/asp/media/c/d;->b:Ljava/io/File;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/d;->c:Ljava/io/File;

    .line 37
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/media/c/d;->d:J

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/mfluent/asp/media/c/d;->b:Ljava/io/File;

    .line 43
    iput-object p1, p0, Lcom/mfluent/asp/media/c/d;->c:Ljava/io/File;

    .line 44
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/media/c/d;->d:J

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lcom/mfluent/asp/media/c/d;->a:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    if-nez v1, :cond_0

    .line 55
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/mfluent/asp/media/c/d;->c:Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :try_start_1
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 57
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    iput-object v0, p0, Lcom/mfluent/asp/media/c/d;->a:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 61
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 62
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 65
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/media/c/d;->a:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    return-object v0

    .line 61
    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 62
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_2
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 62
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v0

    .line 61
    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public final b()Ljava/io/File;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/media/c/d;->b:Ljava/io/File;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/mfluent/asp/media/c/d;->d:J

    return-wide v0
.end method
