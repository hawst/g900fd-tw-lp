.class public final Lcom/mfluent/asp/media/a/c;
.super Lcom/mfluent/asp/media/a/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/media/a/e;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "/api/pCloud/device/media/photo/images/delete"

    return-object v0
.end method

.method protected final a(Ljava/util/ArrayList;I)Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;I)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 49
    const-string v0, "status"

    const-string v2, "INCLUDE"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 50
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 51
    add-int/lit8 v0, p2, 0x32

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 52
    :goto_0
    if-ge p2, v3, :cond_0

    .line 53
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 54
    const-string v4, "source_media_id"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 52
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 56
    :cond_0
    const-string v0, "items"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 58
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/c;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    const-string v0, "inRecycleBin"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 62
    :cond_1
    return-object v1
.end method
