.class public final Lcom/mfluent/asp/media/videotranscoder/d$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/media/videotranscoder/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:Ljava/lang/Long;


# direct methods
.method public constructor <init>(JJJLjava/lang/Long;)V
    .locals 1

    .prologue
    .line 512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    iput-wide p1, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->c:J

    .line 514
    iput-wide p3, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->b:J

    .line 515
    iput-wide p5, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->a:J

    .line 516
    iput-object p7, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->d:Ljava/lang/Long;

    .line 517
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/media/videotranscoder/d$a;)J
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/mfluent/asp/media/videotranscoder/d$a;)J
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/mfluent/asp/media/videotranscoder/d$a;)J
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->a:J

    return-wide v0
.end method

.method static synthetic d(Lcom/mfluent/asp/media/videotranscoder/d$a;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->d:Ljava/lang/Long;

    return-object v0
.end method


# virtual methods
.method public final a(J)J
    .locals 3

    .prologue
    .line 540
    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->c:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->b:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 541
    :cond_0
    const-wide/16 v0, 0x0

    .line 543
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->b:J

    sub-long/2addr v0, p1

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->d:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 527
    iget-wide v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->a:J

    return-wide v0
.end method

.method public final c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$a;->d:Ljava/lang/Long;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
