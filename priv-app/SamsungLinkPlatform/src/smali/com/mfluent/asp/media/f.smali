.class public abstract Lcom/mfluent/asp/media/f;
.super Landroid/os/AsyncTask;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/media/a/o$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Context;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/mfluent/asp/media/a/o$a;"
    }
.end annotation


# instance fields
.field protected a:I

.field protected b:I

.field private c:Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

.field private f:Lcom/mfluent/asp/media/l;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput v0, p0, Lcom/mfluent/asp/media/f;->a:I

    .line 32
    iput v0, p0, Lcom/mfluent/asp/media/f;->b:I

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/media/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/mfluent/asp/media/f;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/media/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/mfluent/asp/media/f;->g:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method protected abstract a(Landroid/content/ContentResolver;Lcom/mfluent/asp/media/f;)I
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/media/f;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected final a(I)V
    .locals 3

    .prologue
    .line 64
    iget v0, p0, Lcom/mfluent/asp/media/f;->a:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 65
    iget v1, p0, Lcom/mfluent/asp/media/f;->b:I

    if-ge v1, v0, :cond_0

    .line 66
    iput v0, p0, Lcom/mfluent/asp/media/f;->b:I

    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    iget v2, p0, Lcom/mfluent/asp/media/f;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/mfluent/asp/media/f;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/media/f;->publishProgress([Ljava/lang/Object;)V

    .line 69
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/media/l;)V
    .locals 0

    .prologue
    .line 50
    iput-object p2, p0, Lcom/mfluent/asp/media/f;->e:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    .line 51
    iput-object p1, p0, Lcom/mfluent/asp/media/f;->c:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    .line 53
    return-void
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/media/l;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/mfluent/asp/media/f;->c:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/mfluent/asp/media/f;->d:[Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    .line 39
    iput-object p3, p0, Lcom/mfluent/asp/media/f;->g:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public final b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/media/f;->d:[Ljava/lang/String;

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 73
    iget v0, p0, Lcom/mfluent/asp/media/f;->b:I

    iget v1, p0, Lcom/mfluent/asp/media/f;->a:I

    if-ge v0, v1, :cond_0

    .line 74
    iget v0, p0, Lcom/mfluent/asp/media/f;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/media/f;->b:I

    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    iget v2, p0, Lcom/mfluent/asp/media/f;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/mfluent/asp/media/f;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/media/f;->publishProgress([Ljava/lang/Object;)V

    .line 77
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 23
    check-cast p1, [Landroid/content/Context;

    aget-object v0, p1, v7

    iget-object v1, p0, Lcom/mfluent/asp/media/f;->e:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v1

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/mfluent/asp/media/f;->c:Ljava/lang/String;

    aput-object v5, v4, v7

    const/4 v5, 0x1

    const-string v6, "device_id"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "_display_name"

    aput-object v6, v4, v5

    new-instance v5, Lcom/mfluent/asp/util/x;

    invoke-direct {v5}, Lcom/mfluent/asp/util/x;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v5, p0, Lcom/mfluent/asp/media/f;->e:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    new-instance v6, Lcom/mfluent/asp/media/f$1;

    invoke-direct {v6, p0, v3, v1, v2}, Lcom/mfluent/asp/media/f$1;-><init>(Lcom/mfluent/asp/media/f;Ljava/util/List;Lcom/mfluent/asp/filetransfer/d;Lcom/mfluent/asp/datamodel/t;)V

    invoke-static {v0, v5, v4, v6}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/media/f;->d:[Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    iget-object v1, p0, Lcom/mfluent/asp/media/f;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/mfluent/asp/media/f;->d:[Ljava/lang/String;

    array-length v2, v2

    invoke-interface {v0, v1, v2}, Lcom/mfluent/asp/media/l;->a(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/media/f;->d:[Ljava/lang/String;

    aget-object v0, p1, v7

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0, p0}, Lcom/mfluent/asp/media/f;->a(Landroid/content/ContentResolver;Lcom/mfluent/asp/media/f;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 23
    check-cast p1, Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mfluent/asp/media/f;->b:I

    iget v1, p0, Lcom/mfluent/asp/media/f;->a:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    invoke-interface {v0}, Lcom/mfluent/asp/media/l;->g_()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/media/l;->a(I)V

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 23
    check-cast p1, Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/media/l;->a(I)V

    :cond_0
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/media/f;->f:Lcom/mfluent/asp/media/l;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/mfluent/asp/media/l;->a(II)V

    :cond_0
    return-void
.end method
