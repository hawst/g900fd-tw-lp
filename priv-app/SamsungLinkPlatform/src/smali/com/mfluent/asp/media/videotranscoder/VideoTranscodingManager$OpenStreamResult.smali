.class public final Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OpenStreamResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;
    }
.end annotation


# instance fields
.field public a:Lcom/mfluent/asp/media/videotranscoder/c;

.field public b:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/c;Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/media/videotranscoder/c;)V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->a:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/c;Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V

    .line 57
    return-void
.end method

.method private constructor <init>(Lcom/mfluent/asp/media/videotranscoder/c;Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->a:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    if-ne p2, v0, :cond_0

    if-nez p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error should not be set to OK without a stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->a:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    if-eq p2, v0, :cond_1

    if-eqz p1, :cond_1

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error should be OK when setting a stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_1
    iput-object p1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;->a:Lcom/mfluent/asp/media/videotranscoder/c;

    .line 52
    iput-object p2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;->b:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    .line 53
    return-void
.end method
