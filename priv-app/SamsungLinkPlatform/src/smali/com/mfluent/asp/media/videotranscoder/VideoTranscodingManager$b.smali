.class public abstract Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ZLjava/lang/Object;JJ)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTV;JJ)TV;"
        }
    .end annotation

    .prologue
    .line 496
    if-nez p1, :cond_4

    const/4 v0, 0x1

    .line 497
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 499
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->a()Ljava/lang/Object;

    move-result-object v1

    .line 500
    if-eqz p1, :cond_1

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 501
    :cond_2
    :try_start_0
    invoke-static {p5, p6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    cmp-long v4, v4, p3

    if-ltz v4, :cond_0

    .line 509
    :cond_3
    return-object v1

    .line 496
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;JJ)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;JJ)TV;"
        }
    .end annotation

    .prologue
    .line 486
    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->a(ZLjava/lang/Object;JJ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;JJ)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;JJ)TV;"
        }
    .end annotation

    .prologue
    .line 490
    const/4 v2, 0x1

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->a(ZLjava/lang/Object;JJ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
