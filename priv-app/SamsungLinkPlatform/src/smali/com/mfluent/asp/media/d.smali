.class public final Lcom/mfluent/asp/media/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:Lcom/mfluent/asp/media/d;


# instance fields
.field private c:Lcom/mfluent/asp/util/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/util/o",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/media/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/io/File;

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/media/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/locks/ReentrantLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/media/d;->b:Lcom/mfluent/asp/media/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    .line 73
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    .line 76
    new-instance v0, Lcom/mfluent/asp/media/d$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/media/d$1;-><init>(Lcom/mfluent/asp/media/d;)V

    iput-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    .line 98
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 60
    const-class v5, Lcom/mfluent/asp/media/d;

    monitor-enter v5

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/media/d;->b:Lcom/mfluent/asp/media/d;

    if-nez v0, :cond_7

    .line 61
    new-instance v6, Lcom/mfluent/asp/media/d;

    invoke-direct {v6}, Lcom/mfluent/asp/media/d;-><init>()V

    .line 62
    sput-object v6, Lcom/mfluent/asp/media/d;->b:Lcom/mfluent/asp/media/d;

    const-string v0, "thumb_cache"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, v6, Lcom/mfluent/asp/media/d;->d:Ljava/io/File;

    iget-object v7, v6, Lcom/mfluent/asp/media/d;->d:Ljava/io/File;

    new-instance v0, Ljava/io/File;

    const-string v2, "prefetch.dat"

    invoke-direct {v0, v7, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :try_start_3
    iget-object v1, v6, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    move v4, v3

    :goto_0
    if-ge v4, v8, :cond_1

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v10

    new-array v11, v10, [Ljava/lang/String;

    move v1, v3

    :goto_1
    if-ge v1, v10, :cond_0

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, v6, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, v9, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_1
    :try_start_4
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    :cond_2
    :goto_2
    new-instance v0, Lcom/mfluent/asp/media/d$2;

    invoke-direct {v0, v6}, Lcom/mfluent/asp/media/d$2;-><init>(Lcom/mfluent/asp/media/d;)V

    invoke-virtual {v7, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    new-instance v0, Lcom/mfluent/asp/media/d$3;

    invoke-direct {v0, v6}, Lcom/mfluent/asp/media/d$3;-><init>(Lcom/mfluent/asp/media/d;)V

    invoke-static {v1, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    iget-object v0, v6, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v3

    :goto_3
    :try_start_5
    array-length v2, v1

    if-ge v0, v2, :cond_6

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "."

    invoke-static {v3, v4}, Lorg/apache/commons/lang3/StringUtils;->substringBeforeLast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Lcom/mfluent/asp/media/d;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v7, Lcom/mfluent/asp/media/c/d;

    invoke-direct {v7, v4, v2}, Lcom/mfluent/asp/media/c/d;-><init>(Ljava/io/File;Ljava/io/File;)V

    iget-object v2, v6, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v2, v3, v7}, Lcom/mfluent/asp/util/o;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {v6, v3}, Lcom/mfluent/asp/media/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v6, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v3, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_5
    :try_start_6
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 62
    :catchall_1
    move-exception v0

    move-object v2, v1

    :goto_6
    :try_start_7
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    :try_start_8
    sget-object v3, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_5

    const-string v3, "mfl_AspThumbnailCache"

    const-string v4, "imageInfo is null!!!"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_4

    :catchall_2
    move-exception v0

    :try_start_9
    iget-object v1, v6, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_6
    iget-object v0, v6, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 65
    :cond_7
    sget-object v0, Lcom/mfluent/asp/media/d;->b:Lcom/mfluent/asp/media/d;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    monitor-exit v5

    return-object v0

    .line 62
    :catchall_3
    move-exception v0

    goto :goto_6

    :catchall_4
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_5

    :catch_2
    move-exception v1

    move-object v1, v2

    goto :goto_5
.end method

.method static synthetic a(Lcom/mfluent/asp/media/d;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/mfluent/asp/media/d;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/media/d;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v2, v1

    .line 174
    :goto_0
    if-ge v2, v3, :cond_1

    .line 175
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 176
    if-ltz v0, :cond_0

    .line 177
    const/4 v0, 0x1

    .line 181
    :goto_1
    return v0

    .line 174
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 181
    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Lcom/mfluent/asp/media/c/d;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 377
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/util/o;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/media/c/d;

    .line 380
    iget-object v3, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 382
    if-nez v0, :cond_0

    .line 383
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/media/c/d;

    .line 384
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 387
    :cond_0
    if-nez v0, :cond_3

    .line 388
    sget-object v0, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_1

    .line 389
    const-string v0, "mfl_AspThumbnailCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::getFile:File cache miss for key:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-object v0, v2

    .line 412
    :cond_2
    :goto_0
    return-object v0

    .line 394
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_5

    .line 395
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/util/o;->d(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-object v0, v2

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 403
    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/io/File;->setLastModified(J)Z

    .line 405
    if-eqz v1, :cond_6

    .line 406
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v1, p1, v0}, Lcom/mfluent/asp/util/o;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    :cond_6
    sget-object v1, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_2

    .line 410
    const-string v1, "mfl_AspThumbnailCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getFile:File cache hit for key"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 400
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method static synthetic b(Lcom/mfluent/asp/media/d;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/mfluent/asp/media/d;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static b(JI)Ljava/lang/String;
    .locals 2

    .prologue
    .line 569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .prologue
    .line 542
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->d:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 565
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getContentId()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/media/d;->b(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 144
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mfluent/asp/media/d;->d:Ljava/io/File;

    const-string v3, "prefetch.dat"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 148
    iget-object v2, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 150
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :try_start_1
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 152
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 153
    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v5, v4

    .line 154
    :goto_0
    if-ge v5, v6, :cond_1

    .line 155
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 156
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 157
    array-length v3, v0

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v3, v4

    .line 158
    :goto_1
    array-length v7, v0

    if-ge v3, v7, :cond_0

    .line 159
    aget-object v7, v0, v3

    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 158
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 154
    :cond_0
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 165
    :cond_1
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 166
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 167
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 168
    :goto_2
    return-void

    .line 165
    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_3
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 166
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 167
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_2

    .line 165
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    :goto_4
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 166
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 167
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 165
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_3
.end method

.method private d(Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .prologue
    .line 550
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->d:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(JI)Lcom/mfluent/asp/media/c/d;
    .locals 1

    .prologue
    .line 371
    invoke-static {p1, p2, p3}, Lcom/mfluent/asp/media/d;->b(JI)Ljava/lang/String;

    move-result-object v0

    .line 373
    invoke-direct {p0, v0}, Lcom/mfluent/asp/media/d;->b(Ljava/lang/String;)Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;
    .locals 1

    .prologue
    .line 365
    invoke-static {p1}, Lcom/mfluent/asp/media/d;->c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/lang/String;

    move-result-object v0

    .line 367
    invoke-direct {p0, v0}, Lcom/mfluent/asp/media/d;->b(Ljava/lang/String;)Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Ljava/io/InputStream;J)Lcom/mfluent/asp/media/c/d;
    .locals 13

    .prologue
    .line 417
    invoke-static {p1}, Lcom/mfluent/asp/media/d;->c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/lang/String;

    move-result-object v5

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "temp"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/media/d;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 420
    invoke-direct {p0, v5}, Lcom/mfluent/asp/media/d;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 421
    const/4 v0, 0x0

    .line 423
    const/4 v2, 0x0

    .line 426
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    .line 428
    const/4 v4, 0x0

    .line 429
    const/16 v2, 0x1000

    :try_start_1
    new-array v8, v2, [B

    .line 430
    const-wide/16 v2, 0x0

    .line 432
    :cond_0
    :goto_0
    if-ltz v4, :cond_b

    cmp-long v4, v2, p3

    if-ltz v4, :cond_1

    const-wide/16 v10, 0x0

    cmp-long v4, p3, v10

    if-gez v4, :cond_b

    .line 433
    :cond_1
    invoke-virtual {p2, v8}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 434
    if-lez v4, :cond_0

    .line 435
    int-to-long v10, v4

    add-long/2addr v2, v10

    .line 436
    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 442
    :catch_0
    move-exception v0

    .line 443
    :goto_1
    const/4 v2, 0x1

    .line 444
    :try_start_2
    sget-object v3, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 445
    const-string v3, "mfl_AspThumbnailCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "::putFile Error reading from stream for "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 448
    :cond_2
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    move v0, v2

    .line 451
    :goto_2
    const/4 v3, 0x0

    .line 453
    if-nez v0, :cond_5

    .line 455
    new-instance v2, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v2, p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    .line 458
    :try_start_3
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 459
    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 461
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 462
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v4, :cond_3

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v4, :cond_d

    .line 463
    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to decode bounds from newly downloaded image."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 467
    :catch_1
    move-exception v0

    .line 468
    const/4 v1, 0x1

    .line 469
    sget-object v4, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v8, 0x6

    invoke-virtual {v4, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 470
    const-string v4, "mfl_AspThumbnailCache"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "::putFile Error for "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    move v0, v1

    move-object p1, v2

    .line 475
    :cond_5
    :goto_3
    if-nez v0, :cond_7

    .line 476
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 478
    :cond_6
    :try_start_4
    invoke-static {v6, v7}, Lorg/apache/commons/io/FileUtils;->moveFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 488
    :cond_7
    :goto_4
    if-eqz v0, :cond_f

    .line 489
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 490
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 492
    :cond_8
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_9

    .line 493
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/util/o;->d(Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 496
    :try_start_5
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 498
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 501
    :cond_9
    const/4 v0, 0x0

    .line 534
    :cond_a
    :goto_5
    return-object v0

    .line 439
    :cond_b
    cmp-long v2, v2, p3

    if-gez v2, :cond_c

    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_c

    .line 440
    :try_start_6
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Invalid length read from input stream."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 448
    :catchall_0
    move-exception v0

    :goto_6
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    :cond_c
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    goto/16 :goto_2

    .line 465
    :cond_d
    :try_start_7
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v2, v4}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setThumbWidth(I)V

    .line 466
    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v2, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setThumbHeight(I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    move-object p1, v2

    .line 472
    goto :goto_3

    .line 479
    :catch_2
    move-exception v0

    .line 480
    const-string v1, "mfl_AspThumbnailCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::putFile Error moving temp file for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 481
    :cond_e
    const/4 v0, 0x1

    goto :goto_4

    .line 498
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 504
    :cond_f
    const/4 v1, 0x0

    .line 506
    :try_start_8
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {p0, v5}, Lcom/mfluent/asp/media/d;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 507
    :try_start_9
    new-instance v0, Ljava/io/ObjectOutputStream;

    invoke-direct {v0, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 508
    :try_start_a
    invoke-virtual {v0, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 511
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 512
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 515
    :goto_7
    const/4 v0, 0x0

    .line 516
    const/4 v1, 0x0

    .line 517
    if-eqz v7, :cond_12

    .line 518
    sget-object v0, Lcom/mfluent/asp/media/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x2

    if-gt v0, v2, :cond_10

    .line 519
    const-string v0, "mfl_AspThumbnailCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::putFile:Adding to fileCache for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :cond_10
    new-instance v2, Lcom/mfluent/asp/media/c/d;

    invoke-direct {v2, p1, v7}, Lcom/mfluent/asp/media/c/d;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Ljava/io/File;)V

    .line 523
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v0, v5, v2}, Lcom/mfluent/asp/util/o;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/media/c/d;

    .line 524
    iget-object v3, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 526
    :try_start_b
    invoke-direct {p0, v5}, Lcom/mfluent/asp/media/d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 527
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    invoke-virtual {v1, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/media/c/d;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 530
    :cond_11
    iget-object v2, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    .line 534
    :goto_8
    if-eqz v1, :cond_a

    move-object v0, v1

    goto/16 :goto_5

    .line 511
    :catch_3
    move-exception v0

    move-object v0, v1

    move-object v1, v3

    :goto_9
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 512
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    goto :goto_7

    .line 511
    :catchall_2
    move-exception v0

    move-object v2, v3

    :goto_a
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 512
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    .line 530
    :catchall_3
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 511
    :catchall_4
    move-exception v0

    goto :goto_a

    :catchall_5
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_a

    :catch_4
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_9

    :catch_5
    move-exception v1

    move-object v1, v2

    goto :goto_9

    .line 448
    :catchall_6
    move-exception v0

    move-object v1, v2

    goto/16 :goto_6

    .line 442
    :catch_6
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    :cond_12
    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_8
.end method

.method public final a()Ljava/io/File;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->d:Ljava/io/File;

    return-object v0
.end method

.method public final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 301
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 304
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v3, v1

    .line 305
    :goto_0
    if-ge v3, v2, :cond_5

    .line 306
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    if-ne v0, p1, :cond_4

    .line 307
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 308
    if-eqz v0, :cond_2

    move v2, v1

    .line 309
    :goto_1
    array-length v1, v0

    if-ge v2, v1, :cond_2

    .line 310
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    aget-object v4, v0, v2

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/media/c/d;

    .line 311
    if-eqz v1, :cond_1

    .line 312
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    aget-object v4, v0, v2

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/util/o;->b(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 313
    aget-object v1, v0, v2

    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/d;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 314
    aget-object v1, v0, v2

    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/d;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 316
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    aget-object v4, v0, v2

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 321
    :cond_2
    const/4 v0, 0x1

    .line 323
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->removeAt(I)V

    .line 328
    :goto_2
    if-eqz v0, :cond_3

    .line 329
    invoke-direct {p0}, Lcom/mfluent/asp/media/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 333
    return-void

    .line 305
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final a(ILjava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 243
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 244
    const/4 v1, 0x0

    .line 245
    if-eqz v0, :cond_2

    .line 246
    array-length v1, v0

    new-array v1, v1, [Z

    move-object v6, v1

    move v4, v2

    .line 250
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v7, v1, [Ljava/lang/String;

    .line 253
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 254
    invoke-static {v1}, Lcom/mfluent/asp/media/d;->c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/lang/String;

    move-result-object v9

    .line 257
    if-eqz v0, :cond_a

    .line 258
    invoke-static {v0, v9}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v10

    .line 259
    if-ltz v10, :cond_3

    move v1, v3

    .line 260
    :goto_2
    if-ltz v10, :cond_0

    .line 261
    aput-boolean v1, v6, v10

    .line 264
    :cond_0
    :goto_3
    if-nez v1, :cond_9

    .line 265
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v1, v9}, Lcom/mfluent/asp/util/o;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/media/c/d;

    .line 266
    if-eqz v1, :cond_1

    .line 267
    iget-object v4, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move v1, v3

    .line 271
    :goto_4
    aput-object v9, v7, v5

    .line 272
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v1

    .line 273
    goto :goto_1

    :cond_2
    move-object v6, v1

    move v4, v3

    .line 248
    goto :goto_0

    :cond_3
    move v1, v2

    .line 259
    goto :goto_2

    .line 274
    :cond_4
    invoke-static {v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 275
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 277
    if-eqz v0, :cond_7

    .line 278
    :goto_5
    array-length v1, v0

    if-ge v2, v1, :cond_7

    .line 279
    aget-boolean v1, v6, v2

    if-nez v1, :cond_6

    .line 280
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/media/c/d;

    .line 281
    if-eqz v1, :cond_6

    .line 282
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/util/o;->b(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 283
    aget-object v1, v0, v2

    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/d;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 284
    aget-object v1, v0, v2

    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/d;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 286
    :cond_5
    iget-object v1, p0, Lcom/mfluent/asp/media/d;->e:Ljava/util/HashMap;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 292
    :cond_7
    if-eqz v4, :cond_8

    .line 293
    invoke-direct {p0}, Lcom/mfluent/asp/media/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 297
    return-void

    .line 296
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/d;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_9
    move v1, v4

    goto :goto_4

    :cond_a
    move v1, v2

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/o;->a()V

    .line 562
    return-void
.end method

.method public final b(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/mfluent/asp/media/d;->c:Lcom/mfluent/asp/util/o;

    invoke-static {p1}, Lcom/mfluent/asp/media/d;->c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/util/o;->d(Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    return-void
.end method
