.class public final Lcom/mfluent/asp/media/i;
.super Lcom/mfluent/asp/media/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/i$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/mfluent/asp/media/j;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/media/a/o;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/mfluent/asp/media/i$1;->a:[I

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 30
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 24
    :pswitch_0
    new-instance v0, Lcom/mfluent/asp/media/a/c;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/media/a/c;-><init>(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 26
    :pswitch_1
    new-instance v0, Lcom/mfluent/asp/media/a/m;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/media/a/m;-><init>(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 28
    :pswitch_2
    new-instance v0, Lcom/mfluent/asp/media/a/h;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, p1, v1}, Lcom/mfluent/asp/media/a/h;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    goto :goto_0

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
