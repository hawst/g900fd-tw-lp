.class public abstract Lcom/mfluent/asp/media/a/e;
.super Lcom/mfluent/asp/media/a/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/a/o;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/media/a/o$a;Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/media/a/o$a;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 40
    move v2, v1

    move v0, v1

    .line 45
    :goto_0
    :try_start_0
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    if-nez v0, :cond_1

    .line 46
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/e;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/e;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p3, v2}, Lcom/mfluent/asp/media/a/e;->a(Ljava/util/ArrayList;I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v3

    .line 51
    const-string v4, "result"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 52
    const-string v4, "FAIL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 54
    const/4 v0, 0x1

    .line 59
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/e;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->u()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/e;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->t()V

    .line 45
    :cond_0
    add-int/lit8 v2, v2, 0x32

    goto :goto_0

    .line 64
    :cond_1
    if-nez v0, :cond_2

    .line 65
    invoke-virtual {p0, p2, p3}, Lcom/mfluent/asp/media/a/e;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 71
    :goto_1
    return v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const-string v2, "mfl_BaseASPDeviceMediaDeleter"

    const-string v3, "Failed to delete media."

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract a(Ljava/util/ArrayList;I)Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;I)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation
.end method
