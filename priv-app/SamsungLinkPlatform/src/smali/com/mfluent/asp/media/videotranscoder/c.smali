.class public final Lcom/mfluent/asp/media/videotranscoder/c;
.super Ljava/io/InputStream;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Ljava/io/FileInputStream;

.field private c:I

.field private final d:Lcom/mfluent/asp/media/videotranscoder/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/videotranscoder/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mfluent/asp/media/videotranscoder/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 24
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->b:Ljava/io/FileInputStream;

    .line 25
    iput-object p2, p0, Lcom/mfluent/asp/media/videotranscoder/c;->d:Lcom/mfluent/asp/media/videotranscoder/d;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->d:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/d;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->d:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-virtual {v0, p0}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/c;)V

    .line 35
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->b:Ljava/io/FileInputStream;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->b:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 38
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 42
    new-array v1, v2, [B

    .line 43
    invoke-virtual {p0, v1, v3, v2}, Lcom/mfluent/asp/media/videotranscoder/c;->read([BII)I

    move-result v2

    .line 45
    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    aget-byte v0, v1, v3

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    const/4 v6, 0x3

    .line 50
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->d:Lcom/mfluent/asp/media/videotranscoder/d;

    iget v4, p0, Lcom/mfluent/asp/media/videotranscoder/c;->c:I

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/media/videotranscoder/d;->a(I)J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_3

    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_0

    const-string v0, "mfl_TranscodedVideoInputStream"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getBytesAvailable: Stream exhausted. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/mfluent/asp/media/videotranscoder/c;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bytes read."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_6

    .line 52
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_1

    .line 53
    const-string v0, "mfl_TranscodedVideoInputStream"

    const-string v2, "::read: bytesAvailable reported EOF. Returning -1."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v0, v1

    .line 76
    :cond_2
    :goto_1
    return v0

    .line 50
    :cond_3
    const-wide/16 v2, -0x64

    cmp-long v0, v4, v2

    if-nez v0, :cond_5

    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_4

    const-string v0, "mfl_TranscodedVideoInputStream"

    const-string v1, "::getBytesAvailable: Read failed because the transcoding session was ended"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Read failed because the transcoding session was ended"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-wide v2, v4

    goto :goto_0

    .line 59
    :cond_6
    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-lez v0, :cond_8

    const v0, 0x7fffffff

    .line 62
    :goto_2
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/c;->b:Ljava/io/FileInputStream;

    invoke-virtual {v0, p1, p2, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    .line 64
    if-gez v0, :cond_9

    .line 66
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x6

    if-gt v0, v2, :cond_7

    .line 67
    const-string v0, "mfl_TranscodedVideoInputStream"

    const-string v2, "::read: Encountered unexpected EOF from transcoded file stream."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v0, v1

    .line 69
    goto :goto_1

    .line 59
    :cond_8
    long-to-int v0, v2

    goto :goto_2

    .line 72
    :cond_9
    iget v1, p0, Lcom/mfluent/asp/media/videotranscoder/c;->c:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/mfluent/asp/media/videotranscoder/c;->c:I

    .line 73
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_2

    .line 74
    const-string v1, "mfl_TranscodedVideoInputStream"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::read: Avail: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Req: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Read: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " TotalRead: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/media/videotranscoder/c;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
