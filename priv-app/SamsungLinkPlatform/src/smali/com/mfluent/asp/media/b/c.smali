.class public final Lcom/mfluent/asp/media/b/c;
.super Lcom/mfluent/asp/media/b/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/ContentProvider$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/media/b/b;",
        "Landroid/content/ContentProvider$PipeDataWriter",
        "<",
        "Lcom/mfluent/asp/media/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/b/b;-><init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    .line 35
    return-void
.end method


# virtual methods
.method public final synthetic writeDataToPipe(Landroid/os/ParcelFileDescriptor;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 27
    const/4 v1, 0x0

    const/16 v0, 0x2000

    new-array v3, v0, [B

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/media/b/c;->c()Lcom/mfluent/asp/media/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/media/b;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "CloudStorageSync instance not found."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    const/4 v0, 0x2

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/media/b/c;->a(I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_4

    :goto_0
    return-void

    :cond_0
    :try_start_2
    invoke-virtual {v0}, Lcom/mfluent/asp/media/b;->c()I

    move-result v5

    invoke-virtual {v0}, Lcom/mfluent/asp/media/b;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/mfluent/asp/media/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v5, v6, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getFile(ILjava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Null streamInfo returned."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x2

    :try_start_3
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/media/b/c;->a(I)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :cond_1
    :try_start_4
    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    if-nez v4, :cond_2

    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Null InputStream returned."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    :try_start_5
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v1, v2

    :cond_3
    :goto_1
    if-ltz v1, :cond_4

    :try_start_6
    invoke-virtual {v4, v3}, Ljava/io/InputStream;->read([B)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v1

    if-lez v1, :cond_3

    const/4 v2, 0x1

    :try_start_7
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/media/b/c;->a(I)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_2
    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {v0, v3, v2, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :catch_3
    move-exception v1

    :goto_3
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_4
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v2

    goto :goto_2

    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4

    :catch_6
    move-exception v0

    move-object v0, v1

    goto :goto_3
.end method
