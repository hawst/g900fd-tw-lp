.class final Lcom/mfluent/asp/media/videotranscoder/d$1;
.super Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/media/videotranscoder/d;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/media/videotranscoder/d;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/media/videotranscoder/d;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-direct {p0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x2

    .line 124
    .line 127
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_0

    .line 128
    const-string v0, "mfl_TranscodingSession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::run: Starting transcoding session "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v4}, Lcom/mfluent/asp/media/videotranscoder/d;->a(Lcom/mfluent/asp/media/videotranscoder/d;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->b:Lcom/tmi/transcode/Transcoder;

    invoke-virtual {v0}, Lcom/tmi/transcode/Transcoder;->getTranscodingPercent()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    .line 135
    :goto_0
    if-nez v0, :cond_5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ltz v4, :cond_5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    const/16 v5, 0x64

    if-gt v4, v5, :cond_5

    .line 138
    const-wide/16 v4, 0x1f4

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    iget-object v4, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->b:Lcom/tmi/transcode/Transcoder;

    invoke-static {v3, v4}, Lcom/mfluent/asp/media/videotranscoder/d;->a(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V

    .line 149
    sget-object v3, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v8, :cond_1

    .line 150
    const-string v3, "mfl_TranscodingSession"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::run: Active stream updated - intial: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v5}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/d;)Lcom/mfluent/asp/media/videotranscoder/d$a;

    move-result-object v5

    invoke-static {v5}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " offset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v5}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/d;)Lcom/mfluent/asp/media/videotranscoder/d$a;

    move-result-object v5

    invoke-static {v5}, Lcom/mfluent/asp/media/videotranscoder/d$a;->b(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " total: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v5}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/d;)Lcom/mfluent/asp/media/videotranscoder/d$a;

    move-result-object v5

    invoke-static {v5}, Lcom/mfluent/asp/media/videotranscoder/d$a;->c(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_1
    iget-object v3, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->b:Lcom/tmi/transcode/Transcoder;

    invoke-virtual {v3}, Lcom/tmi/transcode/Transcoder;->getTranscodingPercent()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 159
    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    move v0, v2

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_3

    .line 141
    const-string v0, "mfl_TranscodingSession"

    const-string v3, "::run: Sleep interrupted while waiting for transcoder."

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move v0, v2

    .line 144
    goto :goto_1

    :cond_4
    move v0, v1

    .line 159
    goto/16 :goto_0

    .line 162
    :cond_5
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_6

    .line 163
    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::run: Doing final datastate update"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_6
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->b:Lcom/tmi/transcode/Transcoder;

    invoke-static {v1, v2}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V

    .line 168
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v9, :cond_7

    .line 169
    const-string v1, "mfl_TranscodingSession"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::run: Transcoding session "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " done - interrupted:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " progress:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " final offset:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v2}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/d;)Lcom/mfluent/asp/media/videotranscoder/d$a;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/media/videotranscoder/d$a;->b(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_7
    return-void

    .line 162
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_8

    .line 163
    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::run: Doing final datastate update"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_8
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d$1;->b:Lcom/tmi/transcode/Transcoder;

    invoke-static {v1, v2}, Lcom/mfluent/asp/media/videotranscoder/d;->b(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V

    throw v0
.end method
