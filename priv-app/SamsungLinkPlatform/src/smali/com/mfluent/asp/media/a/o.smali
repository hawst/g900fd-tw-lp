.class public abstract Lcom/mfluent/asp/media/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/a/o$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/mfluent/asp/datamodel/Device;

.field private final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/mfluent/asp/media/a/o;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 37
    iput-object p2, p0, Lcom/mfluent/asp/media/a/o;->b:Landroid/net/Uri;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/content/ContentResolver;)I
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method protected final a(Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 63
    move v1, v2

    move v3, v2

    .line 65
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 66
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v4, v1, 0x32

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 67
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    sub-int v0, v6, v1

    add-int/lit8 v0, v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/mfluent/asp/media/a/o;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    .line 72
    const/16 v0, 0x28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "device_id=?) AND ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string v0, "source_media_id IN ("

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const/4 v4, 0x1

    move v5, v1

    .line 76
    :goto_1
    if-ge v5, v6, :cond_1

    .line 77
    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v9, "source_media_id"

    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 79
    aput-object v0, v8, v4

    .line 80
    add-int/lit8 v0, v4, 0x1

    .line 81
    if-le v5, v1, :cond_0

    .line 82
    const/16 v4, 0x2c

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    const/16 v4, 0x3f

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 76
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v0

    goto :goto_1

    .line 87
    :cond_1
    const-string v0, "))"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v0, p0, Lcom/mfluent/asp/media/a/o;->b:Landroid/net/Uri;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0, v4, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v3, v0

    .line 65
    add-int/lit8 v0, v1, 0x32

    move v1, v0

    goto :goto_0

    .line 92
    :cond_2
    return v3

    :cond_3
    move v0, v4

    goto :goto_2
.end method

.method public abstract a(Lcom/mfluent/asp/media/a/o$a;Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/media/a/o$a;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method protected final b()Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/mfluent/asp/media/a/o;->a:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method
