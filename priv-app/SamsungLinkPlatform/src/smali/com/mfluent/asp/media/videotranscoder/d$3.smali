.class final Lcom/mfluent/asp/media/videotranscoder/d$3;
.super Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/media/videotranscoder/d;->f()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$c",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/media/videotranscoder/d;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/media/videotranscoder/d;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-direct {p0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$c;-><init>()V

    return-void
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 310
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 325
    :goto_0
    return-void

    .line 313
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "::doStart: Transcoder "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    if-nez p0, :cond_2

    .line 315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " succeeded."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_1
    if-eqz p2, :cond_1

    .line 321
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 324
    :cond_1
    const-string v1, "mfl_TranscodingSession"

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed. Error == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 236
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::call: Attempting to start transcoder"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->b:Lcom/tmi/transcode/Transcoder;

    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/d$3$1;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/media/videotranscoder/d$3$1;-><init>(Lcom/mfluent/asp/media/videotranscoder/d$3;Lcom/tmi/transcode/Transcoder;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0xfa

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->b(Ljava/lang/Object;JJ)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "setParam"

    invoke-static {v0, v1, v7}, Lcom/mfluent/asp/media/videotranscoder/d$3;->a(ILjava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->b:Lcom/tmi/transcode/Transcoder;

    invoke-virtual {v0}, Lcom/tmi/transcode/Transcoder;->start()I

    move-result v0

    const-string v1, "start"

    invoke-static {v0, v1, v7}, Lcom/mfluent/asp/media/videotranscoder/d$3;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v1}, Lcom/mfluent/asp/media/videotranscoder/d;->d(Lcom/mfluent/asp/media/videotranscoder/d;)I

    move-result v7

    if-lez v7, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->b:Lcom/tmi/transcode/Transcoder;

    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/d$3$2;

    invoke-direct {v0, p0, v1, v7}, Lcom/mfluent/asp/media/videotranscoder/d$3$2;-><init>(Lcom/mfluent/asp/media/videotranscoder/d$3;Lcom/tmi/transcode/Transcoder;I)V

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->a(Ljava/lang/Object;JJ)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "seek"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "initialTime="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/media/videotranscoder/d$3;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d$3;->b:Lcom/tmi/transcode/Transcoder;

    invoke-static {v1, v2}, Lcom/mfluent/asp/media/videotranscoder/d;->c(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v6

    goto :goto_0
.end method
