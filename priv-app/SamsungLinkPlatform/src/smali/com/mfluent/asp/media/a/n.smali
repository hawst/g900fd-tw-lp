.class public Lcom/mfluent/asp/media/a/n;
.super Lcom/mfluent/asp/media/a/f;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const-class v0, Lcom/mfluent/asp/media/a/n;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/media/a/n;->a:Lorg/slf4j/Logger;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "caption_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "caption_index_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/media/a/n;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/mfluent/asp/media/a/f;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Landroid/net/Uri;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/mfluent/asp/media/a/n;->a:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mfluent/asp/media/a/n;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 45
    sget-object v1, Lcom/mfluent/asp/media/a/n;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 44
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    return-void
.end method
