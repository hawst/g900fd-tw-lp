.class public abstract Lcom/mfluent/asp/media/j;
.super Lcom/mfluent/asp/media/f;
.source "SourceFile"


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Lcom/mfluent/asp/datamodel/Device;

.field private e:Lcom/mfluent/asp/media/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "full_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/media/j;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Lcom/mfluent/asp/media/f;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/mfluent/asp/media/j;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 32
    iput-object v0, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    return-void
.end method

.method private a(Landroid/content/ContentResolver;Lcom/mfluent/asp/media/f;Ljava/util/ArrayList;I)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/mfluent/asp/media/f;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 39
    const/4 v6, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->b()[Ljava/lang/String;

    move-result-object v1

    .line 42
    add-int/lit8 v0, p4, 0x32

    array-length v2, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 43
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 44
    sub-int v0, v2, p4

    new-array v4, v0, [Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " IN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, p4

    .line 47
    :goto_0
    if-ge v0, v2, :cond_1

    .line 48
    sub-int v5, v0, p4

    aget-object v7, v1, v0

    aput-object v7, v4, v5

    .line 49
    if-le v0, p4, :cond_0

    .line 50
    const/16 v5, 0x2c

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 52
    :cond_0
    const/16 v5, 0x3f

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_1
    const/16 v0, 0x29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->e()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->f()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_5

    move v0, v6

    .line 60
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 61
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/media/j;->a(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 62
    iget-object v2, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    if-eqz v2, :cond_2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 63
    iget v2, p2, Lcom/mfluent/asp/media/f;->b:I

    .line 64
    iget-object v3, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    invoke-virtual {v3, p2, p1, p3}, Lcom/mfluent/asp/media/a/o;->a(Lcom/mfluent/asp/media/a/o$a;Landroid/content/ContentResolver;Ljava/util/ArrayList;)I

    move-result v3

    add-int/2addr v0, v3

    .line 65
    iget-object v3, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    invoke-virtual {v3, p1}, Lcom/mfluent/asp/media/a/o;->a(Landroid/content/ContentResolver;)I

    .line 68
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p2, v2}, Lcom/mfluent/asp/media/f;->a(I)V

    .line 70
    :cond_2
    iget-object v2, p0, Lcom/mfluent/asp/media/j;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/media/j;->a(Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/media/a/o;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    .line 71
    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    .line 74
    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 75
    invoke-static {v1, v2}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 76
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 82
    :catch_0
    move-exception v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 86
    :goto_2
    return v0

    .line 82
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    move v0, v6

    goto :goto_2
.end method


# virtual methods
.method protected final a(Landroid/content/ContentResolver;Lcom/mfluent/asp/media/f;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 104
    .line 106
    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->b()[Ljava/lang/String;

    move-result-object v2

    .line 107
    new-instance v3, Ljava/util/ArrayList;

    const/16 v1, 0x14

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 109
    if-eqz p2, :cond_0

    .line 110
    iput v0, p2, Lcom/mfluent/asp/media/f;->b:I

    .line 111
    array-length v1, v2

    iput v1, p2, Lcom/mfluent/asp/media/f;->a:I

    :cond_0
    move v1, v0

    .line 114
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 115
    invoke-direct {p0, p1, p2, v3, v0}, Lcom/mfluent/asp/media/j;->a(Landroid/content/ContentResolver;Lcom/mfluent/asp/media/f;Ljava/util/ArrayList;I)I

    move-result v4

    add-int/2addr v1, v4

    .line 114
    add-int/lit8 v0, v0, 0x32

    goto :goto_0

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/mfluent/asp/media/j;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 118
    iget v2, p2, Lcom/mfluent/asp/media/f;->b:I

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    invoke-virtual {v0, p2, p1, v3}, Lcom/mfluent/asp/media/a/o;->a(Lcom/mfluent/asp/media/a/o$a;Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/2addr v0, v1

    .line 121
    :try_start_1
    iget-object v1, p0, Lcom/mfluent/asp/media/j;->e:Lcom/mfluent/asp/media/a/o;

    invoke-virtual {v1, p1}, Lcom/mfluent/asp/media/a/o;->a(Landroid/content/ContentResolver;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 127
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p2, v1}, Lcom/mfluent/asp/media/f;->a(I)V

    move v1, v0

    .line 130
    :cond_2
    return v1

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method protected abstract a(Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/media/a/o;
.end method

.method protected a(Landroid/database/Cursor;)Z
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/mfluent/asp/media/j;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/media/j;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    const-string v1, "device_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 95
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    const-string v1, "device_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/j;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 96
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract d()Landroid/net/Uri;
.end method

.method protected e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/mfluent/asp/media/j;->c:[Ljava/lang/String;

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "device_id"

    return-object v0
.end method
