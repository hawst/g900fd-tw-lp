.class public final Lcom/mfluent/asp/media/a/j;
.super Lcom/mfluent/asp/media/a/i;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/a/i;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x3

    return v0
.end method

.method protected final a(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;Landroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 27
    const-string v0, "source_media_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    const-string v1, "caption_uri"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    const/16 v2, 0xd

    invoke-interface {p1, v2, v1, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->deleteFile(ILjava/lang/String;Ljava/lang/String;)Z

    .line 34
    :cond_0
    const-string v1, "caption_index_uri"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    const/16 v2, 0xe

    invoke-interface {p1, v2, v1, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->deleteFile(ILjava/lang/String;Ljava/lang/String;)Z

    .line 38
    :cond_1
    return-void
.end method
