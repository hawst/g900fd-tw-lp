.class public Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/media/videotranscoder/d$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;,
        Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;,
        Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$c;,
        Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$a;,
        Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;
    }
.end annotation


# static fields
.field private static final a:J

.field private static final b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static c:Lcom/tmi/transcode/Transcoder;

.field private static d:Ljava/util/concurrent/ExecutorService;

.field private static final h:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final e:Ljava/util/concurrent/locks/Lock;

.field private f:Lcom/mfluent/asp/media/videotranscoder/b;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a:J

    .line 31
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 129
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->d:Ljava/util/concurrent/ExecutorService;

    .line 138
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x4

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    .line 147
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->c:Lcom/tmi/transcode/Transcoder;

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TMI Mobile Transcoder already instantiated. There should only be one in the system."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e()V

    .line 152
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "Initializing Transcoder"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v7, Lcom/tmi/transcode/Transcoder;

    invoke-direct {v7}, Lcom/tmi/transcode/Transcoder;-><init>()V

    invoke-static {}, Lcom/tmi/transcode/Transcoder;->a()I

    move-result v0

    if-gez v0, :cond_3

    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_2

    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "Initializing Transcoder Failed - load()"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v6

    :goto_0
    sput-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->c:Lcom/tmi/transcode/Transcoder;

    .line 153
    return-void

    .line 152
    :cond_3
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$1;

    invoke-direct {v0, p0, v7}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$1;-><init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;Lcom/tmi/transcode/Transcoder;)V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x2

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->a(Ljava/lang/Object;JJ)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_5

    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_4

    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "Initializing Transcoder Failed - create()"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v6

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_6

    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "Initializing Transcoder Succeeded"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v0, v7

    goto :goto_0
.end method

.method static a()I
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;IIIII)Lcom/mfluent/asp/media/videotranscoder/d;
    .locals 10

    .prologue
    .line 299
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 302
    :try_start_0
    move/from16 v0, p7

    invoke-direct {p0, v0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v1, 0x0

    .line 333
    :goto_0
    return-object v1

    .line 307
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    if-nez v1, :cond_4

    .line 308
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    .line 309
    const-string v1, "mfl_VideoTranscodingManager"

    const-string v2, "::createTranscodingSession: Current session is null - no need to stop it."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_1
    :goto_1
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_2

    .line 319
    const-string v1, "mfl_VideoTranscodingManager"

    const-string v2, "::createTranscodingSession: Creating new TranscodingSession"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_2
    new-instance v1, Lcom/mfluent/asp/media/videotranscoder/d;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v2, p0

    move v5, p3

    move v6, p4

    move v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/mfluent/asp/media/videotranscoder/d;-><init>(Lcom/mfluent/asp/media/videotranscoder/d$b;Ljava/io/File;Ljava/io/File;IIIII)V

    .line 330
    sget-object v2, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_3

    .line 331
    const-string v2, "mfl_VideoTranscodingManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::createTranscodingSession: Created new session: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    :cond_3
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 312
    :cond_4
    :try_start_2
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_5

    .line 313
    const-string v1, "mfl_VideoTranscodingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::createTranscodingSession: Stopping current session: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_5
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a(Lcom/mfluent/asp/media/videotranscoder/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 333
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method

.method private a(Lcom/mfluent/asp/media/videotranscoder/b;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    .line 399
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 401
    if-nez p1, :cond_0

    .line 421
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 422
    :goto_0
    return-void

    .line 405
    :cond_0
    :try_start_0
    invoke-interface {p1}, Lcom/mfluent/asp/media/videotranscoder/b;->d()V

    .line 406
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    if-ne p1, v0, :cond_2

    .line 407
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_1

    .line 408
    const-string v0, "mfl_VideoTranscodingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::interruptSession: Replacing current session with completed session - current session: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_1
    invoke-interface {p1}, Lcom/mfluent/asp/media/videotranscoder/b;->c()Ljava/lang/Long;

    move-result-object v0

    .line 412
    new-instance v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$a;

    invoke-interface {p1}, Lcom/mfluent/asp/media/videotranscoder/b;->a()I

    move-result v2

    invoke-interface {p1}, Lcom/mfluent/asp/media/videotranscoder/b;->b()I

    move-result v3

    if-nez v0, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    :goto_1
    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$a;-><init>(IIJB)V

    iput-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    .line 416
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_2

    .line 417
    const-string v0, "mfl_VideoTranscodingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::interruptSession: Current session replaced with completed session - new current session: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 412
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    goto :goto_1

    .line 421
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private a(I)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v6, 0x3

    .line 340
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 342
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    if-nez v2, :cond_1

    .line 343
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_0

    .line 344
    const-string v1, "mfl_VideoTranscodingManager"

    const-string v2, "::canStartNewTranscodingSession: No current session"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 387
    :goto_0
    return v0

    .line 350
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-interface {v2}, Lcom/mfluent/asp/media/videotranscoder/b;->b()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 351
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_2

    .line 352
    const-string v1, "mfl_VideoTranscodingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::canStartNewTranscodingSession: Current transcoding session will be interrupted by same device - device id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " session: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 360
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-interface {v2}, Lcom/mfluent/asp/media/videotranscoder/b;->c()Ljava/lang/Long;

    move-result-object v2

    .line 361
    if-nez v2, :cond_5

    .line 362
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_4

    .line 363
    const-string v0, "mfl_VideoTranscodingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::canStartNewTranscodingSession: Transcoder is busy - requesting device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " session: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 368
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v1

    goto :goto_0

    .line 371
    :cond_5
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, v4, v2

    .line 372
    sget-wide v4, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a:J

    cmp-long v4, v2, v4

    if-ltz v4, :cond_7

    .line 373
    :goto_1
    if-nez v0, :cond_6

    .line 374
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_6

    .line 375
    const-string v1, "mfl_VideoTranscodingManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::canStartNewTranscodingSession: Transcoder is idle, but not long enough - timeSinceCompletion: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v4, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms requesting device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " session: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 387
    :cond_6
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 372
    goto :goto_1

    .line 387
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->c:Lcom/tmi/transcode/Transcoder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c()Lcom/tmi/transcode/Transcoder;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->c:Lcom/tmi/transcode/Transcoder;

    return-object v0
.end method

.method static synthetic d()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->d:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 3

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    const-string v1, "transcoded_videos"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ASPApplication;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->g:Ljava/lang/String;

    .line 162
    invoke-static {v0}, Lcom/mfluent/asp/util/b;->b(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    monitor-exit p0

    return-void

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IIIILcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;
    .locals 8

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 215
    if-nez p6, :cond_1

    .line 217
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    .line 218
    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "::openStream: Requesting device must not be null"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_0
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;

    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->c:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V

    .line 286
    :goto_0
    return-object v0

    .line 222
    :cond_1
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->c:Lcom/tmi/transcode/Transcoder;

    if-nez v0, :cond_3

    .line 223
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    .line 224
    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "::openStream: Transcoder is null. Transcoding is probably not supported on this device."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_2
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;

    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->c:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V

    goto :goto_0

    .line 230
    :cond_3
    const/4 v0, -0x1

    if-ne p4, v0, :cond_a

    .line 231
    const/16 v0, 0xf0

    if-le p3, v0, :cond_5

    const v0, 0x12bce0

    :goto_1
    move v5, v0

    .line 234
    :goto_2
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 236
    :try_start_0
    invoke-virtual {p6}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a(Ljava/lang/String;Ljava/lang/String;IIIII)Lcom/mfluent/asp/media/videotranscoder/d;

    move-result-object v1

    .line 244
    if-nez v1, :cond_6

    .line 245
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_4

    .line 246
    const-string v0, "mfl_VideoTranscodingManager"

    const-string v1, "::openStream: Not creating a new session, because the transcoder is occupied"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_4
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;

    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->b:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 231
    :cond_5
    const v0, 0x61a80

    goto :goto_1

    .line 251
    :cond_6
    :try_start_1
    iput-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->f:Lcom/mfluent/asp/media/videotranscoder/b;

    .line 253
    invoke-virtual {v1}, Lcom/mfluent/asp/media/videotranscoder/d;->f()Z

    move-result v0

    if-nez v0, :cond_8

    .line 254
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x3

    if-gt v0, v2, :cond_7

    .line 255
    const-string v0, "mfl_VideoTranscodingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::openStream: Clearing newly created session because it never initialized - session: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_7
    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a(Lcom/mfluent/asp/media/videotranscoder/b;)V

    .line 258
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;

    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->c:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 263
    :cond_8
    :try_start_2
    invoke-virtual {v1}, Lcom/mfluent/asp/media/videotranscoder/d;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 268
    :try_start_3
    new-instance v3, Lcom/mfluent/asp/media/videotranscoder/c;

    invoke-direct {v3, v2, v1}, Lcom/mfluent/asp/media/videotranscoder/c;-><init>(Ljava/lang/String;Lcom/mfluent/asp/media/videotranscoder/d;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 280
    :try_start_4
    invoke-virtual {v1, v3}, Lcom/mfluent/asp/media/videotranscoder/d;->a(Lcom/mfluent/asp/media/videotranscoder/c;)V

    .line 282
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;

    invoke-direct {v0, v3}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/c;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 286
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    :try_start_5
    sget-object v3, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_9

    .line 271
    const-string v3, "mfl_VideoTranscodingManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::openStream: Failed to open transcoding output file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for source file: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274
    :cond_9
    invoke-direct {p0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a(Lcom/mfluent/asp/media/videotranscoder/b;)V

    .line 277
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;

    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;->c:Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult;-><init>(Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$OpenStreamResult$Error;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 286
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_a
    move v5, p4

    goto/16 :goto_2
.end method

.method public final a(Lcom/mfluent/asp/media/videotranscoder/d;)V
    .locals 0

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;->a(Lcom/mfluent/asp/media/videotranscoder/b;)V

    .line 428
    return-void
.end method
