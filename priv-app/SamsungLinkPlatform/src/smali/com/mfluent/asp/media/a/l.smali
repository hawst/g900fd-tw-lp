.class public Lcom/mfluent/asp/media/a/l;
.super Lcom/mfluent/asp/media/a/f;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/mfluent/asp/media/a/l;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/media/a/l;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    .line 16
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->CONTENT_URI:Landroid/net/Uri;

    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;-><init>()V

    sget-object v2, Lcom/mfluent/asp/util/e;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/mfluent/asp/media/a/f;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Landroid/net/Uri;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/mfluent/asp/media/a/l;->a:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
