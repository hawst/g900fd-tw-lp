.class public final Lcom/mfluent/asp/media/c/c;
.super Lcom/mfluent/asp/media/c/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/c/b;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected final a()Lcom/mfluent/asp/media/c/b$a;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/c;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v1

    .line 37
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceId()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 38
    if-nez v2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-object v0

    .line 42
    :cond_1
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v2

    .line 43
    if-eqz v2, :cond_0

    .line 47
    const/16 v3, 0x200

    const/16 v4, 0x180

    invoke-interface {v2, v3, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setPreferredThumbnailSize(II)V

    .line 49
    invoke-interface {v2, v1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getThumbnail(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Ljava/io/InputStream;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_0

    .line 53
    new-instance v0, Lcom/mfluent/asp/media/c/b$a;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/media/c/b$a;-><init>(Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/mfluent/asp/media/c/b;->d()V

    .line 59
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/c;->h()Ljava/lang/Thread;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 63
    :cond_0
    return-void
.end method
