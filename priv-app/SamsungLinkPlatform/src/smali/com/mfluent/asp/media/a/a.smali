.class public final Lcom/mfluent/asp/media/a/a;
.super Lcom/mfluent/asp/media/a/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/media/a/e;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/ContentResolver;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 73
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/a;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    .line 75
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/net/Uri;

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Albums;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v2, v3

    .line 82
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 83
    invoke-virtual {p1, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    return v1
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "/api/pCloud/device/media/music/songs/delete"

    return-object v0
.end method

.method protected final a(Ljava/util/ArrayList;I)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;I)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 51
    const-string v0, "view"

    const-string v2, "SONG"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 52
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 54
    const-string v0, "status"

    const-string v3, "INCLUDE"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 55
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 56
    add-int/lit8 v0, p2, 0x32

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 57
    :goto_0
    if-ge p2, v4, :cond_0

    .line 58
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 59
    const-string v5, "source_media_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 57
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 61
    :cond_0
    const-string v0, "items"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    const-string v0, "depth1"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/a;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "inRecycleBin"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 68
    :cond_1
    return-object v1
.end method
