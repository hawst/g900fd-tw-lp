.class public final Lcom/mfluent/asp/media/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:J

.field private final b:Lcom/mfluent/asp/datamodel/Device;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:J

.field private final h:J


# direct methods
.method public constructor <init>(JLcom/mfluent/asp/datamodel/Device;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 30
    const/4 v9, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/mfluent/asp/media/b;-><init>(JLcom/mfluent/asp/datamodel/Device;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;B)V

    .line 31
    return-void
.end method

.method private constructor <init>(JLcom/mfluent/asp/datamodel/Device;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;B)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/mfluent/asp/media/b;->a:J

    .line 35
    iput-object p3, p0, Lcom/mfluent/asp/media/b;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 36
    iput p4, p0, Lcom/mfluent/asp/media/b;->c:I

    .line 37
    iput-object p5, p0, Lcom/mfluent/asp/media/b;->d:Ljava/lang/String;

    .line 38
    iput-object p6, p0, Lcom/mfluent/asp/media/b;->e:Landroid/net/Uri;

    .line 39
    iput-object p7, p0, Lcom/mfluent/asp/media/b;->f:Ljava/lang/String;

    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/media/b;->g:J

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mfluent/asp/media/b;->h:J

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/mfluent/asp/media/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/media/b;->b:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/mfluent/asp/media/b;->c:I

    return v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/mfluent/asp/media/b;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/media/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 92
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    instance-of v2, p1, Lcom/mfluent/asp/media/b;

    if-eqz v2, :cond_3

    .line 97
    check-cast p1, Lcom/mfluent/asp/media/b;

    .line 98
    iget-wide v2, p1, Lcom/mfluent/asp/media/b;->a:J

    iget-wide v4, p0, Lcom/mfluent/asp/media/b;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget v2, p1, Lcom/mfluent/asp/media/b;->c:I

    iget v3, p0, Lcom/mfluent/asp/media/b;->c:I

    if-ne v2, v3, :cond_2

    iget-wide v2, p1, Lcom/mfluent/asp/media/b;->g:J

    iget-wide v4, p0, Lcom/mfluent/asp/media/b;->g:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p1, Lcom/mfluent/asp/media/b;->h:J

    iget-wide v4, p0, Lcom/mfluent/asp/media/b;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 101
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 106
    new-instance v0, Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    invoke-direct {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;-><init>()V

    iget-wide v2, p0, Lcom/mfluent/asp/media/b;->a:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/media/b;->c:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(I)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mfluent/asp/media/b;->g:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mfluent/asp/media/b;->h:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_ID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/mfluent/asp/media/b;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " local id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/media/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uri:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/media/b;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
