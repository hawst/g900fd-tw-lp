.class public abstract Lcom/mfluent/asp/media/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Thread;

.field private b:Z

.field private c:Z

.field private final d:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean v0, p0, Lcom/mfluent/asp/media/c;->b:Z

    .line 14
    iput-boolean v0, p0, Lcom/mfluent/asp/media/c;->c:Z

    .line 18
    iput-object p1, p0, Lcom/mfluent/asp/media/c;->d:Ljava/lang/Object;

    .line 20
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/c;->a:Ljava/lang/Thread;

    .line 21
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/media/c;->b:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/mfluent/asp/media/c;->c:Z

    if-nez v0, :cond_1

    .line 67
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/media/c;->a:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can only finish on the executing thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 70
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/mfluent/asp/media/c;->c:Z

    .line 71
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    monitor-exit p0

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 25
    if-ne p1, p0, :cond_0

    .line 26
    const/4 v0, 0x1

    .line 33
    :goto_0
    return v0

    .line 27
    :cond_0
    instance-of v0, p1, Lcom/mfluent/asp/media/c;

    if-eqz v0, :cond_1

    .line 28
    check-cast p1, Lcom/mfluent/asp/media/c;

    .line 30
    iget-object v0, p0, Lcom/mfluent/asp/media/c;->d:Ljava/lang/Object;

    iget-object v1, p1, Lcom/mfluent/asp/media/c;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 33
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/mfluent/asp/media/c;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
