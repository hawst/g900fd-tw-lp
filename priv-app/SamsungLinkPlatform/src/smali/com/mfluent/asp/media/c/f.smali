.class public abstract Lcom/mfluent/asp/media/c/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;",
            "Lcom/mfluent/asp/media/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

.field private d:Z

.field private e:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/c/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/media/c/f;->d:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/f;->e:Ljava/lang/Thread;

    .line 35
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    iput-object v0, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 36
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;ZZZJ)Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;ZZZJ)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 146
    .line 149
    const/4 v0, 0x1

    move v2, v0

    move-object v3, v1

    .line 156
    :goto_0
    if-eqz v2, :cond_1

    .line 158
    sget-object v3, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v3

    .line 159
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/media/e;

    .line 160
    if-nez v0, :cond_3

    .line 161
    new-instance v2, Lcom/mfluent/asp/media/e;

    iget-object v0, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v2, v0, p0}, Lcom/mfluent/asp/media/e;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/c/f;)V

    .line 162
    sget-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    if-eqz v0, :cond_4

    .line 167
    monitor-enter v2

    .line 169
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 175
    :cond_0
    :goto_2
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v3, v2

    move v2, v0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 170
    :catch_0
    move-exception v3

    .line 171
    :try_start_3
    sget-object v4, Lcom/mfluent/asp/media/c/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x3

    if-gt v4, v5, :cond_0

    .line 172
    const-string v4, "mfl_ThumbnailGetter"

    const-string v5, "::openThumbnail: InterruptedException"

    invoke-static {v4, v5, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 175
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 183
    :cond_1
    :try_start_4
    iget-boolean v0, p0, Lcom/mfluent/asp/media/c/f;->d:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-eqz v0, :cond_2

    .line 184
    sget-object v2, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 189
    :try_start_5
    sget-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 191
    invoke-virtual {v3}, Lcom/mfluent/asp/media/e;->a()V

    move-object v0, v1

    .line 194
    :goto_3
    return-object v0

    .line 190
    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    .line 186
    :cond_2
    :try_start_6
    invoke-virtual/range {p0 .. p6}, Lcom/mfluent/asp/media/c/f;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;ZZZJ)Landroid/os/ParcelFileDescriptor;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result-object v0

    .line 188
    sget-object v1, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 189
    :try_start_7
    sget-object v2, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 191
    invoke-virtual {v3}, Lcom/mfluent/asp/media/e;->a()V

    goto :goto_3

    .line 190
    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    .line 188
    :catchall_4
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 189
    :try_start_8
    sget-object v2, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 191
    invoke-virtual {v3}, Lcom/mfluent/asp/media/e;->a()V

    throw v0

    .line 190
    :catchall_5
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    move v6, v2

    move-object v2, v0

    move v0, v6

    goto :goto_1

    :cond_4
    move-object v3, v2

    move v2, v0

    goto/16 :goto_0
.end method

.method public abstract b()Lcom/mfluent/asp/media/c/d;
.end method

.method protected abstract c()Lcom/mfluent/asp/media/c/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public final e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/mfluent/asp/media/c/f;->d:Z

    if-nez v0, :cond_0

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/media/c/f;->d:Z

    .line 47
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/f;->d()V

    .line 49
    :cond_0
    return-void
.end method

.method public final g()Lcom/mfluent/asp/media/c/d;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 56
    move-object v5, v4

    move v3, v1

    .line 66
    :goto_0
    if-eqz v3, :cond_1

    .line 68
    sget-object v5, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v5

    .line 69
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/media/e;

    .line 70
    if-nez v0, :cond_9

    .line 71
    new-instance v3, Lcom/mfluent/asp/media/e;

    iget-object v0, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v3, v0, p0}, Lcom/mfluent/asp/media/e;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/c/f;)V

    .line 72
    sget-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    .line 75
    :goto_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    if-eqz v0, :cond_a

    .line 77
    monitor-enter v3

    .line 79
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 85
    :cond_0
    :goto_2
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v5, v3

    move v3, v0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 80
    :catch_0
    move-exception v5

    .line 81
    :try_start_3
    sget-object v6, Lcom/mfluent/asp/media/c/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    if-gt v6, v8, :cond_0

    .line 82
    const-string v6, "mfl_ThumbnailGetter"

    const-string v7, "::openThumbnail: InterruptedException"

    invoke-static {v6, v7, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 85
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    .line 94
    :cond_1
    :try_start_4
    iget-boolean v0, p0, Lcom/mfluent/asp/media/c/f;->d:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-eqz v0, :cond_3

    .line 95
    sget-object v1, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 117
    :try_start_5
    sget-object v0, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 119
    invoke-virtual {v5}, Lcom/mfluent/asp/media/e;->a()V

    .line 120
    iput-object v4, p0, Lcom/mfluent/asp/media/c/f;->e:Ljava/lang/Thread;

    move-object v0, v4

    .line 127
    :cond_2
    return-object v0

    .line 118
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 98
    :cond_3
    :try_start_6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/f;->e:Ljava/lang/Thread;

    .line 101
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/f;->b()Lcom/mfluent/asp/media/c/d;

    move-result-object v3

    .line 102
    if-eqz v3, :cond_8

    .line 103
    invoke-virtual {v3}, Lcom/mfluent/asp/media/c/d;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v6

    iget-object v7, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v7}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v7

    if-lt v6, v7, :cond_5

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v0

    iget-object v6, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v6

    if-lt v0, v6, :cond_5

    move v0, v1

    .line 108
    :goto_3
    if-nez v0, :cond_6

    .line 109
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/f;->c()Lcom/mfluent/asp/media/c/d;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result-object v0

    .line 116
    :goto_4
    sget-object v1, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 117
    :try_start_7
    sget-object v2, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 119
    invoke-virtual {v5}, Lcom/mfluent/asp/media/e;->a()V

    .line 120
    iput-object v4, p0, Lcom/mfluent/asp/media/c/f;->e:Ljava/lang/Thread;

    .line 123
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_2

    .line 124
    :cond_4
    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "openThumbnail: result = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move v0, v2

    .line 104
    goto :goto_3

    .line 111
    :cond_6
    :try_start_8
    sget-object v0, Lcom/mfluent/asp/media/c/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_7

    .line 112
    const-string v0, "mfl_ThumbnailGetter"

    const-string v1, "::openThumbnail: skipping far fetch because file cache has acceptable copy"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :cond_7
    move-object v0, v3

    goto :goto_4

    .line 118
    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    .line 116
    :catchall_4
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 117
    :try_start_9
    sget-object v2, Lcom/mfluent/asp/media/c/f;->b:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/mfluent/asp/media/c/f;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 119
    invoke-virtual {v5}, Lcom/mfluent/asp/media/e;->a()V

    .line 120
    iput-object v4, p0, Lcom/mfluent/asp/media/c/f;->e:Ljava/lang/Thread;

    throw v0

    .line 118
    :catchall_5
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move-object v9, v0

    move v0, v3

    move-object v3, v9

    goto/16 :goto_1

    :cond_a
    move-object v5, v3

    move v3, v0

    goto/16 :goto_0
.end method

.method protected final h()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/mfluent/asp/media/c/f;->e:Ljava/lang/Thread;

    return-object v0
.end method
