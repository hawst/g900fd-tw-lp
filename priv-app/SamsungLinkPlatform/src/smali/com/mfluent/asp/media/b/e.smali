.class public abstract Lcom/mfluent/asp/media/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mfluent/asp/media/b;",
            "Lcom/mfluent/asp/media/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/mfluent/asp/media/b;

.field private final d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIA_PROVIDER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/b/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    .line 34
    iput-object p2, p0, Lcom/mfluent/asp/media/b/e;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    .line 35
    return-void
.end method


# virtual methods
.method protected abstract a()Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method public final b()Landroid/os/ParcelFileDescriptor;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 45
    const/4 v1, 0x1

    move v2, v1

    move-object v1, v0

    .line 52
    :goto_0
    if-eqz v2, :cond_1

    .line 54
    sget-object v3, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    monitor-enter v3

    .line 55
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/media/a;

    .line 56
    if-nez v0, :cond_2

    .line 57
    new-instance v1, Lcom/mfluent/asp/media/a;

    iget-object v0, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/media/a;-><init>(Ljava/lang/Object;)V

    .line 58
    sget-object v0, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const/4 v0, 0x0

    .line 61
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    if-eqz v0, :cond_3

    .line 63
    monitor-enter v1

    .line 65
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 71
    :cond_0
    :goto_2
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v2, v0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 66
    :catch_0
    move-exception v2

    .line 67
    :try_start_3
    sget-object v3, Lcom/mfluent/asp/media/b/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_0

    .line 68
    const-string v3, "mfl_MediaGetter"

    const-string v4, "::openMedia: InterruptedException"

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 71
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 79
    :cond_1
    :try_start_4
    invoke-virtual {v1, p0}, Lcom/mfluent/asp/media/a;->a(Lcom/mfluent/asp/media/b/e;)V

    .line 82
    invoke-virtual {p0}, Lcom/mfluent/asp/media/b/e;->a()Landroid/os/ParcelFileDescriptor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result-object v0

    .line 85
    sget-object v2, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 86
    :try_start_5
    sget-object v3, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 88
    invoke-virtual {v1}, Lcom/mfluent/asp/media/a;->a()V

    .line 91
    return-object v0

    .line 87
    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    .line 85
    :catchall_3
    move-exception v0

    sget-object v2, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 86
    :try_start_6
    sget-object v3, Lcom/mfluent/asp/media/b/e;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 88
    invoke-virtual {v1}, Lcom/mfluent/asp/media/a;->a()V

    throw v0

    .line 87
    :catchall_4
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move-object v1, v0

    move v0, v2

    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public final c()Lcom/mfluent/asp/media/b;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/mfluent/asp/media/b/e;->c:Lcom/mfluent/asp/media/b;

    return-object v0
.end method

.method public final d()Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/mfluent/asp/media/b/e;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    return-object v0
.end method
