.class public final Lcom/mfluent/asp/media/b/d;
.super Lcom/mfluent/asp/media/b/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/b/e;-><init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected final a()Landroid/os/ParcelFileDescriptor;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/b/d;->c()Lcom/mfluent/asp/media/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/media/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Local image not found."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    return-object v0
.end method
