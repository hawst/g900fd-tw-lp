.class public abstract Lcom/mfluent/asp/media/b/b;
.super Lcom/mfluent/asp/media/b/e;
.source "SourceFile"

# interfaces
.implements Landroid/content/ContentProvider$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/media/b/e;",
        "Landroid/content/ContentProvider$PipeDataWriter",
        "<",
        "Lcom/mfluent/asp/media/b;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/concurrent/locks/ReentrantLock;

.field private final c:Ljava/util/concurrent/locks/Condition;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/b/e;-><init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    .line 30
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/media/b/b;->a:I

    .line 41
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    .line 42
    iget-object v0, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/b/b;->c:Ljava/util/concurrent/locks/Condition;

    .line 43
    return-void
.end method


# virtual methods
.method protected final a()Landroid/os/ParcelFileDescriptor;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/mfluent/asp/media/b/b;->d()Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/media/b/b;->c()Lcom/mfluent/asp/media/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/media/b;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/media/b/b;->c()Lcom/mfluent/asp/media/b;

    move-result-object v4

    move-object v3, v2

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 57
    :goto_0
    :try_start_0
    iget v1, p0, Lcom/mfluent/asp/media/b/b;->a:I

    if-nez v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/mfluent/asp/media/b/b;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 61
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/mfluent/asp/media/b/b;->a:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    .line 62
    if-eqz v0, :cond_1

    .line 63
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 71
    if-nez v2, :cond_2

    .line 72
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 75
    :cond_2
    return-object v2

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method protected final a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 81
    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/media/b/b;->a:I

    if-eq v0, p1, :cond_0

    .line 82
    iput p1, p0, Lcom/mfluent/asp/media/b/b;->a:I

    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/media/b/b;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 87
    return-void

    .line 86
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/b/b;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
