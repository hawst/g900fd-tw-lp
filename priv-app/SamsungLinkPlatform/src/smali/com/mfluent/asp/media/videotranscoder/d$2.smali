.class final Lcom/mfluent/asp/media/videotranscoder/d$2;
.super Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/media/videotranscoder/d;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/media/videotranscoder/d;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/media/videotranscoder/d;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/mfluent/asp/media/videotranscoder/d$2;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-direct {p0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    .line 188
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_0

    .line 189
    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::run: Stopping transcoder"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d$2;->b:Lcom/tmi/transcode/Transcoder;

    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/d$2$1;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/media/videotranscoder/d$2$1;-><init>(Lcom/mfluent/asp/media/videotranscoder/d$2;Lcom/tmi/transcode/Transcoder;)V

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0xfa

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$b;->a(Ljava/lang/Object;JJ)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_1

    .line 193
    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::run: Transcoder stopped"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d$2;->a:Lcom/mfluent/asp/media/videotranscoder/d;

    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d;->c(Lcom/mfluent/asp/media/videotranscoder/d;)Ljava/io/File;

    move-result-object v0

    .line 197
    if-nez v0, :cond_3

    .line 198
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x7

    if-gt v0, v1, :cond_2

    .line 199
    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::executeSession: session\'s target file is null when finishing session."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_2
    :goto_0
    return-void

    .line 202
    :cond_3
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_4

    .line 203
    const-string v1, "mfl_TranscodingSession"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run: deleting transcoded file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 208
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_2

    .line 209
    const-string v1, "mfl_TranscodingSession"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run: deleted transcoded file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
