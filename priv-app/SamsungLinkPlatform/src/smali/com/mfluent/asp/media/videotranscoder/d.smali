.class final Lcom/mfluent/asp/media/videotranscoder/d;
.super Lcom/mfluent/asp/media/videotranscoder/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/videotranscoder/d$b;,
        Lcom/mfluent/asp/media/videotranscoder/d$a;
    }
.end annotation


# static fields
.field static final b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/io/File;

.field private final e:Ljava/io/File;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private j:Lcom/mfluent/asp/media/videotranscoder/c;

.field private k:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private volatile l:Lcom/mfluent/asp/media/videotranscoder/d$a;

.field private final m:Lcom/mfluent/asp/media/videotranscoder/d$b;

.field private final n:Ljava/util/concurrent/locks/Lock;

.field private final o:Ljava/util/concurrent/locks/Condition;

.field private final p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/media/videotranscoder/d$b;Ljava/io/File;Ljava/io/File;IIIII)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/mfluent/asp/media/videotranscoder/a;-><init>()V

    .line 25
    const-string v0, "mfl_TranscodingSession"

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->c:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->k:Ljava/util/concurrent/Future;

    .line 53
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    .line 54
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->o:Ljava/util/concurrent/locks/Condition;

    .line 67
    iput-object p2, p0, Lcom/mfluent/asp/media/videotranscoder/d;->d:Ljava/io/File;

    .line 68
    iput-object p3, p0, Lcom/mfluent/asp/media/videotranscoder/d;->e:Ljava/io/File;

    .line 69
    iput p4, p0, Lcom/mfluent/asp/media/videotranscoder/d;->f:I

    .line 70
    iput p5, p0, Lcom/mfluent/asp/media/videotranscoder/d;->g:I

    .line 71
    iput p6, p0, Lcom/mfluent/asp/media/videotranscoder/d;->h:I

    .line 72
    iput p7, p0, Lcom/mfluent/asp/media/videotranscoder/d;->i:I

    .line 73
    iput-object p1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->m:Lcom/mfluent/asp/media/videotranscoder/d$b;

    .line 74
    iput p8, p0, Lcom/mfluent/asp/media/videotranscoder/d;->p:I

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/media/videotranscoder/d;)Ljava/io/File;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->d:Ljava/io/File;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, v0, v0}, Lcom/mfluent/asp/media/videotranscoder/d;->a(Lcom/tmi/transcode/Transcoder;Ljava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method private a(Lcom/tmi/transcode/Transcoder;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    .line 460
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 462
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 463
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::setDatatState: updating data state"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 467
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Session already finished"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 470
    :cond_1
    if-nez p2, :cond_3

    .line 471
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    if-nez v0, :cond_2

    .line 472
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Session not started, but no initialOffset was provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    .line 479
    :cond_3
    invoke-virtual {p1}, Lcom/tmi/transcode/Transcoder;->getTranscodingCurOffset()I

    move-result v0

    int-to-long v4, v0

    .line 480
    invoke-virtual {p1}, Lcom/tmi/transcode/Transcoder;->getTranscodingTotalSize()I

    move-result v0

    int-to-long v6, v0

    .line 482
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->d(Lcom/mfluent/asp/media/videotranscoder/d$a;)Ljava/lang/Long;

    move-result-object v0

    if-ne v0, p3, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->b(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->c(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_7

    .line 488
    :cond_4
    new-instance v1, Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/mfluent/asp/media/videotranscoder/d$a;-><init>(JJJLjava/lang/Long;)V

    iput-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    .line 489
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_5

    .line 490
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::setDatatState: data state updated. Signalling lock condition"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->o:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500
    :cond_6
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 501
    return-void

    .line 494
    :cond_7
    :try_start_2
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_6

    .line 495
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::setDatatState: not changing data state. All values are the same"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/media/videotranscoder/d;)Lcom/mfluent/asp/media/videotranscoder/d$a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V
    .locals 4

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/mfluent/asp/media/videotranscoder/d;->a(Lcom/tmi/transcode/Transcoder;Ljava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/media/videotranscoder/d;)Ljava/io/File;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->e:Ljava/io/File;

    return-object v0
.end method

.method static synthetic c(Lcom/mfluent/asp/media/videotranscoder/d;Lcom/tmi/transcode/Transcoder;)V
    .locals 2

    .prologue
    .line 23
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/mfluent/asp/media/videotranscoder/d;->a(Lcom/tmi/transcode/Transcoder;Ljava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method static synthetic d(Lcom/mfluent/asp/media/videotranscoder/d;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->i:I

    return v0
.end method

.method static synthetic e(Lcom/mfluent/asp/media/videotranscoder/d;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->f:I

    return v0
.end method

.method static synthetic f(Lcom/mfluent/asp/media/videotranscoder/d;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->g:I

    return v0
.end method

.method static synthetic g(Lcom/mfluent/asp/media/videotranscoder/d;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->h:I

    return v0
.end method


# virtual methods
.method public final a(I)J
    .locals 9

    .prologue
    const/4 v8, 0x2

    const-wide/16 v6, 0x0

    .line 389
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 393
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    .line 395
    int-to-long v0, p1

    invoke-virtual {v2, v0, v1}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a(J)J

    move-result-wide v0

    .line 396
    :goto_0
    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a(J)J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {v2}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 398
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_0

    .line 399
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::bytesAvailable: awaiting data"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->o:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V

    .line 402
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_1

    .line 403
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "mfl_TranscodingSession"

    const-string v1, "::bytesAvailable: wait finished"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408
    :cond_1
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    .line 409
    int-to-long v0, p1

    invoke-virtual {v2, v0, v1}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 413
    :cond_2
    cmp-long v3, v0, v6

    if-gez v3, :cond_3

    .line 414
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not expecting negative return values from DataState.bytesAvailable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 425
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 415
    :cond_3
    cmp-long v3, v0, v6

    if-lez v3, :cond_4

    .line 428
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-wide v0

    .line 417
    :cond_4
    :try_start_3
    invoke-virtual {v2}, Lcom/mfluent/asp/media/videotranscoder/d$a;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 418
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data to return, but session is not finished. We shouldn\'t have stopped waiting for data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :cond_5
    int-to-long v0, p1

    invoke-static {v2}, Lcom/mfluent/asp/media/videotranscoder/d$a;->c(Lcom/mfluent/asp/media/videotranscoder/d$a;)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-ltz v0, :cond_7

    .line 420
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_6

    .line 421
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "mfl_TranscodingSession"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::read: Stream exhausted. "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/media/videotranscoder/d$a;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes transcoded"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 423
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const-wide/16 v0, -0x1

    goto :goto_2

    .line 425
    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const-wide/16 v0, -0x64

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/mfluent/asp/media/videotranscoder/c;)V
    .locals 2

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->j:Lcom/mfluent/asp/media/videotranscoder/c;

    .line 82
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->p:I

    return v0
.end method

.method public final b(Lcom/mfluent/asp/media/videotranscoder/c;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->j:Lcom/mfluent/asp/media/videotranscoder/c;

    if-eq v0, p1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->j:Lcom/mfluent/asp/media/videotranscoder/c;

    .line 97
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->m:Lcom/mfluent/asp/media/videotranscoder/d$b;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->m:Lcom/mfluent/asp/media/videotranscoder/d$b;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/media/videotranscoder/d$b;->a(Lcom/mfluent/asp/media/videotranscoder/d;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 453
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 455
    :goto_0
    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    .line 453
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->c()Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 455
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 109
    sget-object v0, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "::interrupt: Interrupting Session. mExecutionFuture.isDone()="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->k:Ljava/util/concurrent/Future;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->k:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->k:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 117
    :cond_1
    return-void

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->k:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/d$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/media/videotranscoder/d$1;-><init>(Lcom/mfluent/asp/media/videotranscoder/d;)V

    .line 181
    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;->a()Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->k:Ljava/util/concurrent/Future;

    .line 183
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/d$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/media/videotranscoder/d$2;-><init>(Lcom/mfluent/asp/media/videotranscoder/d;)V

    .line 230
    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$d;->a()Ljava/util/concurrent/Future;

    .line 231
    return-void
.end method

.method public final f()Z
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 236
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/d$3;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/media/videotranscoder/d$3;-><init>(Lcom/mfluent/asp/media/videotranscoder/d;)V

    .line 327
    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager$c;->a()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 332
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    if-nez v0, :cond_1

    .line 333
    const/4 v0, 0x0

    .line 357
    :goto_0
    return v0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_0

    .line 337
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::openStream: Transcoder start task was interrupted"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_0
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_1

    .line 340
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::initSession:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 357
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 342
    :catch_1
    move-exception v0

    .line 343
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_2

    .line 344
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::openStream: Transcoder start task generated an exception"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_2
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_1

    .line 347
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::initSession:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 349
    :catch_2
    move-exception v0

    .line 350
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_3

    .line 351
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::openStream: Transcoder start task was cancelled"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :cond_3
    sget-object v1, Lcom/mfluent/asp/media/videotranscoder/d;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_1

    .line 354
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "mfl_TranscodingSession"

    const-string v2, "::initSession:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final g()J
    .locals 3

    .prologue
    .line 361
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 364
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->l:Lcom/mfluent/asp/media/videotranscoder/d$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 366
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/media/videotranscoder/d;->o:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 369
    :catch_0
    move-exception v0

    goto :goto_0

    .line 372
    :cond_0
    :try_start_2
    invoke-static {v0}, Lcom/mfluent/asp/media/videotranscoder/d$a;->c(Lcom/mfluent/asp/media/videotranscoder/d$a;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    .line 374
    iget-object v2, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/media/videotranscoder/d;->n:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
