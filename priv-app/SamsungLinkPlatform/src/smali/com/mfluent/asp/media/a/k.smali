.class public Lcom/mfluent/asp/media/a/k;
.super Lcom/mfluent/asp/media/a/f;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/mfluent/asp/media/a/k;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/media/a/k;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/mfluent/asp/media/a/f;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Landroid/net/Uri;)V

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/ContentResolver;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/k;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    .line 40
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/net/Uri;

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Albums;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v2, v3

    .line 47
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 48
    invoke-virtual {p1, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return v1
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/mfluent/asp/media/a/k;->a:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
