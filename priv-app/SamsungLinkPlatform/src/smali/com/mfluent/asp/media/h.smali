.class public final Lcom/mfluent/asp/media/h;
.super Lcom/mfluent/asp/media/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/h$1;
    }
.end annotation


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/mfluent/asp/media/j;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/media/h;->c:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/media/a/o;
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/mfluent/asp/media/h$1;->a:[I

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 36
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 30
    :pswitch_0
    new-instance v0, Lcom/mfluent/asp/media/a/b;

    iget-boolean v1, p0, Lcom/mfluent/asp/media/h;->c:Z

    invoke-direct {v0, p1, v1}, Lcom/mfluent/asp/media/a/b;-><init>(Lcom/mfluent/asp/datamodel/Device;Z)V

    goto :goto_0

    .line 32
    :pswitch_1
    new-instance v0, Lcom/mfluent/asp/media/a/l;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/media/a/l;-><init>(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 34
    :pswitch_2
    new-instance v0, Lcom/mfluent/asp/media/a/h;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, p1, v1}, Lcom/mfluent/asp/media/a/h;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/mfluent/asp/media/j;->a(Landroid/database/Cursor;)Z

    move-result v1

    .line 50
    const-string v0, "is_personal"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v2

    .line 51
    iget-boolean v0, p0, Lcom/mfluent/asp/media/h;->c:Z

    if-eq v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    .line 52
    iput-boolean v2, p0, Lcom/mfluent/asp/media/h;->c:Z

    .line 54
    return v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final e()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/mfluent/asp/media/j;->e()[Ljava/lang/String;

    move-result-object v0

    const-string v1, "is_personal"

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ArrayUtils;->add([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/mfluent/asp/media/j;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_personal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
