.class public abstract Lcom/mfluent/asp/media/a/f;
.super Lcom/mfluent/asp/media/a/o;
.source "SourceFile"


# instance fields
.field private final a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

.field private final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/a/o;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    .line 45
    iput-object p3, p0, Lcom/mfluent/asp/media/a/f;->a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    .line 46
    iput-object p4, p0, Lcom/mfluent/asp/media/a/f;->b:Landroid/net/Uri;

    .line 47
    return-void
.end method

.method private a(Landroid/content/ContentResolver;Ljava/util/ArrayList;I)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 50
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, p3, 0x32

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 52
    sub-int v7, v2, p3

    .line 53
    new-instance v8, Lcom/mfluent/asp/util/h;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/mfluent/asp/util/h;-><init>(Ljava/lang/String;)V

    .line 54
    new-instance v9, Lcom/mfluent/asp/util/h;

    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/mfluent/asp/util/h;-><init>(Ljava/lang/String;)V

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Total time for deleting "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " items."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/mfluent/asp/util/h;->a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;

    .line 58
    new-array v4, v7, [Ljava/lang/String;

    .line 59
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 60
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 61
    const-string v0, "_id"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string v0, " IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, p3

    .line 63
    :goto_0
    if-ge v1, v2, :cond_1

    .line 64
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    sub-int v5, v1, p3

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v11, "source_media_id"

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 66
    if-le v1, p3, :cond_0

    .line 67
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 69
    :cond_0
    const/16 v0, 0x3f

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 71
    :cond_1
    const/16 v0, 0x29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Deleted "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from MediaStore."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/mfluent/asp/util/h;->a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;

    .line 74
    iget-object v0, p0, Lcom/mfluent/asp/media/a/f;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 75
    invoke-virtual {v9}, Lcom/mfluent/asp/util/h;->a()Lcom/mfluent/asp/util/h;

    .line 76
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v11, v0, :cond_4

    .line 77
    iget-object v1, p0, Lcom/mfluent/asp/media/a/f;->b:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 78
    if-eqz v1, :cond_4

    .line 80
    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 82
    if-ltz v0, :cond_2

    .line 83
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 87
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 92
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Deleted "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " files."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/mfluent/asp/util/h;->a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;

    .line 93
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 94
    const-string v2, "_data"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 96
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 98
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 100
    :cond_6
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/media/a/f;->a(Landroid/content/ContentValues;)V

    goto :goto_2

    .line 103
    :cond_7
    invoke-virtual {v9}, Lcom/mfluent/asp/util/h;->a()Lcom/mfluent/asp/util/h;

    .line 105
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Deleted "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from ASPMediaStore."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/mfluent/asp/util/h;->a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;

    .line 107
    invoke-virtual {p0, p1, v10}, Lcom/mfluent/asp/media/a/f;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;)I

    .line 108
    invoke-virtual {v9}, Lcom/mfluent/asp/util/h;->a()Lcom/mfluent/asp/util/h;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Journaling "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " deletes."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/mfluent/asp/util/h;->a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;

    .line 111
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v6

    .line 112
    :goto_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 113
    iget-object v3, p0, Lcom/mfluent/asp/media/a/f;->a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v4, "source_media_id"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/mfluent/asp/datamodel/al;->c(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 116
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 120
    :cond_8
    :try_start_1
    const-string v0, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_0

    .line 126
    :goto_4
    invoke-virtual {v9}, Lcom/mfluent/asp/util/h;->a()Lcom/mfluent/asp/util/h;

    .line 189
    const-class v0, Lcom/mfluent/asp/sync/h;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/h;

    .line 190
    invoke-virtual {v0}, Lcom/mfluent/asp/sync/h;->a()V

    .line 193
    :cond_9
    invoke-virtual {v8}, Lcom/mfluent/asp/util/h;->a()Lcom/mfluent/asp/util/h;

    .line 195
    return v11

    :catch_0
    move-exception v0

    goto :goto_4

    .line 125
    :catch_1
    move-exception v0

    goto :goto_4
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/media/a/o$a;Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/media/a/o$a;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 200
    move v1, v0

    .line 202
    :goto_0
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 203
    invoke-direct {p0, p2, p3, v0}, Lcom/mfluent/asp/media/a/f;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;I)I

    move-result v2

    add-int/2addr v1, v2

    .line 202
    add-int/lit8 v0, v0, 0x32

    goto :goto_0

    .line 206
    :cond_0
    return v1
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 236
    return-void
.end method
