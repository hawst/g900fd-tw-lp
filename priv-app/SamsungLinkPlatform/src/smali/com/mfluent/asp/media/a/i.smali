.class public abstract Lcom/mfluent/asp/media/a/i;
.super Lcom/mfluent/asp/media/a/o;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/a/i;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/media/a/o;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public final a(Lcom/mfluent/asp/media/a/o$a;Landroid/content/ContentResolver;Ljava/util/ArrayList;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/media/a/o$a;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/i;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v3

    .line 40
    if-nez v3, :cond_0

    .line 72
    :goto_0
    return v0

    .line 44
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v0

    move v2, v0

    .line 47
    :goto_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {p1}, Lcom/mfluent/asp/media/a/o$a;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 49
    :try_start_0
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v5, "source_media_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 50
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v6, "full_uri"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lcom/mfluent/asp/media/a/i;->a()I

    move-result v6

    invoke-interface {v3, v6, v0, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->deleteFile(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    add-int/lit8 v2, v2, 0x1

    .line 53
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {p0, v3, v0}, Lcom/mfluent/asp/media/a/i;->a(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;Landroid/content/ContentValues;)V

    .line 56
    :cond_1
    invoke-interface {p1}, Lcom/mfluent/asp/media/a/o$a;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 57
    :catch_0
    move-exception v0

    .line 58
    sget-object v5, Lcom/mfluent/asp/media/a/i;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_2

    .line 59
    const-string v5, "mfl_CloudStorageMediaDeleter"

    const-string v6, "::deleteMedia: Error trying to delete cloud file."

    invoke-static {v5, v6, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 64
    :cond_3
    if-lez v2, :cond_4

    invoke-interface {v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->finishedDeleteGroup()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 65
    invoke-virtual {p0, p2, v4}, Lcom/mfluent/asp/media/a/i;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;)I

    .line 68
    :cond_4
    invoke-interface {p1}, Lcom/mfluent/asp/media/a/o$a;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 69
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    .line 72
    goto/16 :goto_0
.end method

.method protected a(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
