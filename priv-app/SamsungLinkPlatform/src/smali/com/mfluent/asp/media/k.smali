.class public final Lcom/mfluent/asp/media/k;
.super Lcom/mfluent/asp/media/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/media/k$1;
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "caption_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "caption_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "caption_index_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/media/k;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/mfluent/asp/media/j;-><init>()V

    .line 44
    return-void
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/media/a/o;
    .locals 2

    .prologue
    .line 36
    sget-object v0, Lcom/mfluent/asp/media/k$1;->a:[I

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 44
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 38
    :pswitch_0
    new-instance v0, Lcom/mfluent/asp/media/a/d;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/media/a/d;-><init>(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 40
    :pswitch_1
    new-instance v0, Lcom/mfluent/asp/media/a/n;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/media/a/n;-><init>(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 42
    :pswitch_2
    new-instance v0, Lcom/mfluent/asp/media/a/j;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, p1, v1}, Lcom/mfluent/asp/media/a/j;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/net/Uri;)V

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final e()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    invoke-super {p0}, Lcom/mfluent/asp/media/j;->e()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/media/k;->c:[Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ArrayUtils;->addAll([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method
