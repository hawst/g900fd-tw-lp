.class public final Lcom/mfluent/asp/media/c/e;
.super Lcom/mfluent/asp/media/c/f;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:[Ljava/lang/String;


# instance fields
.field private c:Landroid/net/Uri;

.field private d:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/media/c/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/media/c/e;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1}, Lcom/mfluent/asp/media/c/f;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    .line 31
    iput-object v0, p0, Lcom/mfluent/asp/media/c/e;->c:Landroid/net/Uri;

    .line 32
    iput-object v0, p0, Lcom/mfluent/asp/media/c/e;->d:Landroid/content/ContentResolver;

    .line 36
    return-void
.end method

.method private a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 143
    const/4 v6, 0x0

    .line 144
    const/4 v7, 0x0

    .line 145
    sget-object v2, Lcom/mfluent/asp/media/c/e;->b:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/e;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_0

    .line 149
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 150
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    .line 155
    :cond_0
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 156
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 157
    const-string v1, "cancel"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 158
    const-string v1, "orig_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/e;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 159
    const-string v1, "group_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 160
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/e;->c:Landroid/net/Uri;

    .line 161
    iput-object p1, p0, Lcom/mfluent/asp/media/c/e;->d:Landroid/content/ContentResolver;

    .line 163
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 164
    const-string v1, "blocking"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 165
    const-string v1, "orig_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/e;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 166
    const-string v1, "group_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 169
    :try_start_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/media/c/e;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_2

    .line 171
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 174
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/e;->c:Landroid/net/Uri;

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/media/c/e;->d:Landroid/content/ContentResolver;

    .line 182
    :cond_3
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 186
    :goto_1
    return-object v6

    .line 177
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/media/c/e;->c:Landroid/net/Uri;

    .line 178
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/media/c/e;->d:Landroid/content/ContentResolver;

    throw v0

    :cond_4
    move-object v6, v7

    goto :goto_1

    :cond_5
    move-object v0, v6

    goto/16 :goto_0
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;ZZZJ)Landroid/os/ParcelFileDescriptor;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    const/4 v8, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/e;->e()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v9

    .line 62
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 64
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move-object v7, v0

    move-object v0, v8

    .line 135
    :goto_0
    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 139
    :cond_0
    return-object v0

    .line 67
    :sswitch_0
    const/16 v0, 0x200

    .line 68
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceThumbWidth()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceThumbHeight()I

    move-result v2

    if-lez v2, :cond_1

    .line 69
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceThumbWidth()I

    move-result v0

    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceThumbHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 72
    :cond_1
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v2

    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-le v2, v0, :cond_2

    .line 74
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getData()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    move-object v0, v8

    goto :goto_0

    .line 76
    :cond_2
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbData()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 78
    sget-object v2, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "image_id"

    move-object v0, p0

    move-wide/from16 v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/media/c/e;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_3
    if-eqz v0, :cond_4

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_6

    .line 82
    :cond_4
    sget-object v1, Lcom/mfluent/asp/media/c/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_6

    .line 83
    const-string v1, "mfl_LocalThumbnailGetter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LocalThumbnailGetter::getThumbnailFileDescriptor: thumbPath is invalid. replace to the original. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getData()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    move-object v0, v8

    goto/16 :goto_0

    .line 91
    :sswitch_1
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbData()Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 94
    sget-object v2, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "video_id"

    move-object v0, p0

    move-wide/from16 v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/media/c/e;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    move-object v0, v8

    goto/16 :goto_0

    .line 100
    :sswitch_2
    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbData()Ljava/lang/String;

    move-result-object v7

    .line 101
    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 102
    sget-object v2, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "album_art"

    aput-object v4, v3, v0

    const-string v4, "_id=?"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceAlbumId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 108
    if-eqz v2, :cond_8

    .line 109
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 110
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 115
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 118
    :goto_2
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 119
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_5

    .line 121
    const/4 v0, 0x0

    .line 124
    :cond_5
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 125
    const-string v2, "content://media/external/audio/albumart"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v9}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceAlbumId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "r"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    move-object v7, v0

    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    move-object v7, v0

    move-object v0, v8

    goto/16 :goto_0

    :cond_7
    move-object v0, v7

    goto :goto_1

    :cond_8
    move-object v0, v7

    goto :goto_2

    :cond_9
    move-object v0, v8

    goto/16 :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch
.end method

.method public final b()Lcom/mfluent/asp/media/c/d;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final c()Lcom/mfluent/asp/media/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final d()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 191
    invoke-super {p0}, Lcom/mfluent/asp/media/c/f;->d()V

    .line 192
    iget-object v0, p0, Lcom/mfluent/asp/media/c/e;->d:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/media/c/e;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/mfluent/asp/media/c/e;->d:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/media/c/e;->c:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_0

    .line 195
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 198
    :cond_0
    return-void
.end method
