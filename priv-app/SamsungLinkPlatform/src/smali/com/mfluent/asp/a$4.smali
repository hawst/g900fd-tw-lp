.class final Lcom/mfluent/asp/a$4;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/a;)V
    .locals 0

    .prologue
    .line 596
    iput-object p1, p0, Lcom/mfluent/asp/a$4;->a:Lcom/mfluent/asp/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 602
    const-string v0, "client_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_0

    .line 605
    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "===> mAuthInfoReceiver - [action] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", [appId] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    :cond_0
    const-string v0, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 610
    const-string v0, "result_code"

    const/16 v1, -0x3e7

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 612
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_1

    .line 613
    const-string v0, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "received com.msc.action.ACCESSTOKEN_V02_RESPONSE with resultCode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_1
    const/4 v0, 0x0

    .line 617
    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 618
    new-instance v0, Lcom/sec/pcw/hybrid/b/b$a;

    invoke-direct {v0}, Lcom/sec/pcw/hybrid/b/b$a;-><init>()V

    .line 619
    const-string v1, "access_token"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->c:Ljava/lang/String;

    .line 622
    const-string v1, "user_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->a:Ljava/lang/String;

    .line 624
    const-string v1, "email_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->b:Ljava/lang/String;

    .line 625
    const-string v1, "mcc"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 628
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->h:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 633
    :goto_0
    const-string v1, "server_url"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->e:Ljava/lang/String;

    .line 639
    const-string v1, "cc"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    .line 643
    const-string v1, "device_physical_address_text"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/pcw/hybrid/b/b$a;->g:Ljava/lang/String;

    .line 645
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v4, :cond_2

    .line 646
    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "received devicePhysicalAddressText : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/sec/pcw/hybrid/b/b$a;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", vs. SlinkDeviceUtils="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/pcw/util/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/mfluent/asp/a$4;->a:Lcom/mfluent/asp/a;

    invoke-static {v1, v0}, Lcom/mfluent/asp/a;->a(Lcom/mfluent/asp/a;Lcom/sec/pcw/hybrid/b/b$a;)V

    .line 694
    :cond_3
    return-void

    .line 651
    :cond_4
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 654
    const-string v1, "error_message"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 660
    const-string v2, "check_list"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 663
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v4, :cond_2

    .line 685
    const-string v2, "mfl_AccessManager"

    const-string v3, "================ NEED TO HANDLE"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "================ SA error : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    const-string v1, "mfl_AccessManager"

    const-string v2, "================ ."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method
