.class final Lcom/mfluent/asp/ContentAggregatorService$b;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ContentAggregatorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ContentAggregatorService;

.field private b:J


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ContentAggregatorService;Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 71
    iput-object p1, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    .line 72
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 69
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->b:J

    .line 73
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->b:J

    .line 140
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ContentAggregatorService$b;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ContentAggregatorService$b;->removeMessages(I)V

    .line 143
    :cond_0
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ContentAggregatorService$b;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 144
    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->b()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/mfluent/asp/ContentAggregatorService$b;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 145
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    .line 81
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 127
    :goto_0
    if-nez v1, :cond_2

    .line 132
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 136
    :cond_0
    :goto_1
    return-void

    .line 83
    :pswitch_0
    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v2}, Lcom/mfluent/asp/ContentAggregatorService;->a(Lcom/mfluent/asp/ContentAggregatorService;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->b:J

    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 86
    invoke-static {}, Lcom/samsung/android/sdk/samsung/a;->d()Z

    move-result v2

    .line 87
    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->d()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "::handleMessage() - DYNAMIC_DEATH_FOR_CONTENTPROVIDER_SPEED:{} isMemLow:{}"

    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->c()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 91
    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v2, :cond_1

    .line 92
    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->d()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Delay activity stop..."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    .line 94
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->d()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "Stopping self due to no activity for {} ms. Bound? {}"

    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v5}, Lcom/mfluent/asp/ContentAggregatorService;->a(Lcom/mfluent/asp/ContentAggregatorService;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 98
    iget-object v2, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {}, Lcom/mfluent/asp/ContentAggregatorService;->e()V

    .line 99
    iget-object v2, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    iget-object v3, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v3}, Lcom/mfluent/asp/ContentAggregatorService;->b(Lcom/mfluent/asp/ContentAggregatorService;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/ContentAggregatorService;->stopSelf(I)V

    move v7, v1

    move v1, v0

    move v0, v7

    .line 102
    goto :goto_0

    :pswitch_1
    move v1, v0

    .line 107
    goto :goto_0

    .line 110
    :pswitch_2
    iget-object v1, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v1}, Lcom/mfluent/asp/b/a;->a(Landroid/content/Context;)V

    move v1, v0

    .line 111
    goto/16 :goto_0

    :pswitch_3
    move v1, v0

    .line 115
    goto/16 :goto_0

    .line 118
    :pswitch_4
    iget-object v1, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v1}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/b/i;->c()Ljava/lang/String;

    move v1, v0

    .line 119
    goto/16 :goto_0

    .line 122
    :pswitch_5
    iget-object v1, p0, Lcom/mfluent/asp/ContentAggregatorService$b;->a:Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v1}, Lcom/mfluent/asp/util/v;->a(Landroid/content/Context;)V

    move v1, v0

    .line 123
    goto/16 :goto_0

    .line 133
    :cond_2
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/mfluent/asp/ContentAggregatorService$b;->a()V

    goto/16 :goto_1

    :cond_3
    move v1, v0

    goto/16 :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
