.class public final Lcom/mfluent/asp/dws/handlers/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/z;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x190

    const/4 v10, 0x3

    .line 41
    instance-of v0, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-nez v0, :cond_1

    .line 42
    sget-object v0, Lcom/mfluent/asp/dws/handlers/z;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_0

    .line 43
    const-string v0, "mfl_GetListMetaHandler"

    const-string v1, "::handle:Expected some payload in the request but got none"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_0
    const-string v0, "No payload in the request"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 46
    invoke-interface {p2, v11}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 85
    :goto_0
    return-void

    .line 50
    :cond_1
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 51
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 64
    const-string v0, "startId"

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 65
    const-string v0, "maxItems"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 66
    const-string v0, "view"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 68
    const-string v0, "depth1"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 69
    const-string v0, "depth2"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 71
    invoke-static {}, Lcom/mfluent/asp/c;->a()Lcom/mfluent/asp/c;

    move-result-object v0

    const-string v6, "sort"

    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/c;->a(Ljava/lang/String;)I

    move-result v6

    .line 72
    const-string v0, "search"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 73
    const-string v0, "backwardMaxItems"

    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 83
    new-instance v0, Lcom/sec/pcw/service/b/c;

    invoke-direct {v0}, Lcom/sec/pcw/service/b/c;-><init>()V

    invoke-virtual/range {v0 .. v8}, Lcom/sec/pcw/service/b/c;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/mfluent/asp/util/n;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/util/n;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    sget-object v1, Lcom/mfluent/asp/dws/handlers/z;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v10, :cond_2

    .line 76
    const-string v1, "mfl_GetListMetaHandler"

    const-string v2, "::handle:Trouble parsing request body"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 79
    invoke-interface {p2, v11}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0
.end method
