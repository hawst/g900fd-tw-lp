.class public final Lcom/mfluent/asp/dws/handlers/m;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lorg/json/JSONObject;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v0, -0x1

    .line 20
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 22
    :try_start_0
    const-string v2, "noOfTotalFiles"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v3

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->g()I

    move-result v5

    add-int/2addr v3, v5

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 23
    const-string v2, "indexOfFile"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->r()I

    move-result v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 24
    const-string v2, "currentFile"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25
    const-string v2, "currentBytesSent"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->t()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 26
    const-string v2, "currentBytes"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->u()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 27
    const-string v2, "currentTab"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 28
    const-string v2, "totalBytesSent"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 30
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 31
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->k()J

    move-result-wide v2

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    .line 32
    const-string v2, "totalBytes"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 40
    :goto_0
    const-string v2, "sourceId"

    const-string v3, "TMP_SOURCE_ID"

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    const-string v2, "targetId"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 42
    const-string v2, "controllerId"

    const-string v3, "TMP_CONTROLLER_ID"

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 43
    const-string v2, "targetId"

    const-string v3, "TMP_TARGET_ID"

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    const-string v2, "status"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    const-string v2, "stopReason"

    const-string v3, "NULL"

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 46
    const-string v2, "stopReasonCode"

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 47
    const-string v2, "role"

    const/4 v3, 0x2

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 48
    const-string v2, "sessionCreationTime"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->b()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 49
    const-string v2, "id"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 50
    const-string v2, "deleteAfterUpload"

    const-string v3, ""

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->b()J

    move-result-wide v6

    sub-long/2addr v2, v6

    .line 54
    cmp-long v5, v2, v8

    if-lez v5, :cond_4

    .line 55
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v6

    div-long v2, v6, v2

    .line 57
    :goto_1
    const-string v5, "transferRate"

    invoke-virtual {v4, v5, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 60
    cmp-long v5, v2, v8

    if-lez v5, :cond_0

    .line 61
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->k()J

    move-result-wide v0

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 62
    div-long/2addr v0, v2

    .line 64
    :cond_0
    const-string v2, "remainingTime"

    invoke-virtual {v4, v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 66
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 69
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->h()Ljava/util/Set;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 71
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_2
    const-string v2, "controllerName"

    invoke-static {v2, v0}, Lcom/mfluent/asp/dws/handlers/m;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 76
    const-string v2, "sourceName"

    invoke-static {v2, v0}, Lcom/mfluent/asp/dws/handlers/m;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 77
    const-string v0, "targetName"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mfluent/asp/dws/handlers/m;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 79
    const-string v0, "displayName"

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    return-object v4

    .line 34
    :cond_1
    const-string v2, "totalBytes"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->k()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble building json response"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 37
    :cond_2
    :try_start_1
    const-string v2, "totalBytes"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->k()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    goto/16 :goto_0

    .line 73
    :cond_3
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    const v2, 0x7f0a00c5

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    :cond_4
    move-wide v2, v0

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 89
    invoke-virtual {v0, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 90
    return-object v0
.end method
