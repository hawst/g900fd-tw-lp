.class public abstract Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;
.super Lcom/mfluent/asp/dws/handlers/b;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException;,
        Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException;,
        Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;,
        Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;,
        Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;
    }
.end annotation


# static fields
.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 178
    const-string v0, "bytes=(\\d*)-(\\d*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/b;-><init>()V

    .line 43
    const/16 v0, 0x4000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a:[B

    .line 512
    return-void
.end method

.method private a(Lorg/apache/http/Header;J)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/Header;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x1

    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 296
    invoke-interface {p1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 297
    sget-object v2, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getContentRange:Range Header: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_0
    sget-object v2, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 302
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_2

    .line 303
    sget-object v0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_1

    .line 304
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "::getContentRange: Failed to parse range header"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_1
    :goto_0
    return-object v1

    .line 313
    :cond_2
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 314
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 315
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v2, v1

    .line 316
    :goto_1
    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_5

    move-object v0, v1

    .line 324
    :goto_2
    if-nez v2, :cond_6

    if-eqz v0, :cond_6

    .line 325
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 326
    sub-long v4, p2, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 331
    :cond_3
    :goto_3
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 315
    :cond_4
    :try_start_1
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_1

    .line 316
    :cond_5
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 318
    :catch_0
    move-exception v2

    sget-object v2, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_1

    .line 319
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getContentRange: Invalid byte position in range: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_6
    if-eqz v2, :cond_3

    if-nez v0, :cond_3

    .line 328
    sub-long v4, p2, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3
.end method

.method private declared-synchronized a(Lorg/apache/http/HttpRequest;Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;)Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 350
    monitor-enter p0

    .line 352
    :try_start_0
    invoke-interface/range {p2 .. p2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 353
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 355
    const-wide/16 v10, 0x0

    .line 356
    const-wide/16 v6, 0x0

    .line 357
    const/4 v2, 0x0

    .line 359
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 361
    const-string v3, "resize"

    invoke-virtual {v5, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 362
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 363
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0x78

    invoke-static {v3, v6}, Lorg/apache/commons/lang3/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v3

    .line 364
    array-length v6, v3

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    .line 365
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid resize parameter."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 368
    :cond_0
    const/4 v6, 0x0

    :try_start_1
    aget-object v6, v3, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-long v10, v6

    .line 369
    const/4 v6, 0x1

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-long v6, v3

    .line 371
    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 373
    const/4 v3, 0x0

    .line 375
    const/4 v12, 0x1

    iput-boolean v12, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 378
    :try_start_2
    invoke-interface/range {p2 .. p2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->c()Ljava/io/InputStream;

    move-result-object v3

    .line 379
    const/4 v12, 0x0

    invoke-static {v3, v12, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 381
    iget v12, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v12, v4}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 383
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 385
    const-wide/16 v14, 0x0

    cmp-long v13, v8, v14

    if-lez v13, :cond_1

    int-to-long v14, v12

    cmp-long v13, v8, v14

    if-gez v13, :cond_1

    .line 386
    const-wide/16 v14, 0x1

    int-to-long v12, v12

    div-long v8, v12, v8

    invoke-static {v14, v15, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    long-to-int v8, v8

    iput v8, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395
    :goto_0
    :try_start_3
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    move-object v3, v4

    .line 399
    :goto_1
    const-string v4, "rotation"

    invoke-virtual {v5, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 400
    const-string v8, "orientation"

    invoke-virtual {v5, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 401
    const-string v8, "ON"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 402
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 403
    rem-int/lit8 v4, v2, 0x5a

    if-eqz v4, :cond_2

    .line 404
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid orientation parameter."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 392
    :cond_1
    const-wide/16 v6, 0x0

    move-wide v10, v6

    goto :goto_0

    .line 395
    :catchall_1
    move-exception v2

    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v2

    .line 406
    :cond_2
    rem-int/lit16 v4, v2, 0x168
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-gez v2, :cond_6

    const/4 v2, -0x1

    :goto_2
    mul-int/2addr v2, v4

    move v14, v2

    .line 409
    :goto_3
    if-nez v14, :cond_3

    const-wide/16 v4, 0x0

    cmp-long v2, v10, v4

    if-gtz v2, :cond_3

    const-wide/16 v4, 0x0

    cmp-long v2, v6, v4

    if-lez v2, :cond_8

    .line 410
    :cond_3
    const/4 v2, 0x0

    .line 411
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a:[B

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 415
    invoke-interface/range {p2 .. p2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->c()Ljava/io/InputStream;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v2

    .line 416
    :try_start_5
    sget-object v4, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x3

    if-gt v4, v5, :cond_4

    .line 417
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Decoding Bitmap with options: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "inSampleSize: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " inScaled:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " inDensity: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " inTargetDensity: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_4
    const/4 v4, 0x0

    invoke-static {v2, v4, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 422
    sget-object v4, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x3

    if-gt v4, v5, :cond_5

    .line 423
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v4, "Bitmap size: "

    invoke-direct {v8, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v3, :cond_7

    const-string v4, "null"

    :goto_4
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 429
    :cond_5
    :try_start_6
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 432
    if-nez v3, :cond_9

    .line 433
    new-instance v2, Ljava/io/FileNotFoundException;

    const-string v3, "Bitmap is null"

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 406
    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_2

    .line 423
    :cond_7
    :try_start_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v9, 0x78

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v4

    goto :goto_4

    .line 427
    :catch_0
    move-exception v3

    .line 429
    :try_start_8
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 493
    :cond_8
    :goto_5
    monitor-exit p0

    return-object p2

    .line 429
    :catchall_2
    move-exception v3

    move-object/from16 v20, v3

    move-object v3, v2

    move-object/from16 v2, v20

    :goto_6
    :try_start_9
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v2

    .line 436
    :cond_9
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-long v8, v2

    .line 437
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-long v4, v2

    .line 439
    const-wide/16 v12, 0x0

    cmp-long v2, v6, v12

    if-gtz v2, :cond_a

    move-wide v6, v4

    .line 442
    :cond_a
    const-wide/16 v12, 0x0

    cmp-long v2, v10, v12

    if-gtz v2, :cond_b

    move-wide v10, v8

    .line 446
    :cond_b
    mul-long v12, v8, v4

    .line 447
    mul-long v16, v4, v12

    div-long v16, v16, v6

    mul-long v18, v8, v12

    div-long v18, v18, v10

    cmp-long v2, v16, v18

    if-lez v2, :cond_e

    .line 448
    mul-long v10, v8, v12

    div-long/2addr v10, v4

    mul-long/2addr v10, v6

    div-long/2addr v10, v12

    .line 453
    :goto_7
    mul-long v12, v8, v4

    mul-long v16, v6, v10

    cmp-long v2, v12, v16

    if-gez v2, :cond_14

    move-wide v10, v4

    move-wide v12, v8

    .line 458
    :goto_8
    cmp-long v2, v10, v4

    if-nez v2, :cond_c

    cmp-long v2, v12, v8

    if-eqz v2, :cond_f

    .line 460
    :cond_c
    long-to-int v2, v12

    long-to-int v4, v10

    const/4 v5, 0x0

    invoke-static {v3, v2, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 461
    if-eq v2, v3, :cond_d

    .line 462
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 463
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 466
    :cond_d
    if-nez v2, :cond_10

    .line 467
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error scaling image."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 450
    :cond_e
    mul-long v6, v4, v12

    div-long/2addr v6, v8

    mul-long/2addr v6, v10

    div-long/2addr v6, v12

    goto :goto_7

    :cond_f
    move-object v2, v3

    .line 471
    :cond_10
    if-eqz v14, :cond_12

    .line 472
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 473
    int-to-float v3, v14

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 476
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 477
    if-eq v3, v2, :cond_11

    .line 478
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 479
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 482
    :cond_11
    if-nez v3, :cond_13

    .line 483
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error rotating image."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_12
    move-object v3, v2

    .line 487
    :cond_13
    new-instance v2, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " scale:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x78

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rotate:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;B)V

    .line 488
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    move-object/from16 p2, v2

    .line 489
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_5

    .line 429
    :catchall_3
    move-exception v3

    move-object/from16 v20, v3

    move-object v3, v2

    move-object/from16 v2, v20

    goto/16 :goto_6

    :cond_14
    move-wide v12, v10

    move-wide v10, v6

    goto/16 :goto_8

    :cond_15
    move v14, v2

    goto/16 :goto_3

    :cond_16
    move-object v3, v4

    goto/16 :goto_1
.end method


# virtual methods
.method protected abstract a(Lorg/apache/http/HttpRequest;Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    const/4 v4, 0x0

    .line 191
    const/4 v3, 0x0

    .line 193
    :try_start_0
    const-string v2, "MFL_ATTR_DEVICE"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/datamodel/Device;

    .line 194
    invoke-virtual {p0, p1, v2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a(Lorg/apache/http/HttpRequest;Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_8
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 197
    :try_start_1
    invoke-direct {p0, p1, v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a(Lorg/apache/http/HttpRequest;Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;)Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 202
    :goto_0
    :try_start_2
    const-string v2, "Range"

    invoke-interface {p1, v2}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v9

    .line 205
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->b()J

    move-result-wide v6

    .line 207
    if-nez v9, :cond_2

    .line 208
    new-instance v2, Landroid/util/Pair;

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-wide/16 v10, 0x1

    sub-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v8, v2

    .line 218
    :goto_1
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->c()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 219
    :try_start_3
    iget-object v2, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 220
    :cond_0
    :goto_2
    const-wide/16 v10, 0x0

    cmp-long v2, v6, v10

    if-lez v2, :cond_5

    .line 221
    invoke-virtual {v5, v6, v7}, Ljava/io/InputStream;->skip(J)J
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-wide v10

    .line 222
    const-wide/16 v12, 0x0

    cmp-long v2, v10, v12

    if-lez v2, :cond_4

    .line 223
    sub-long/2addr v6, v10

    goto :goto_2

    .line 198
    :catch_0
    move-exception v2

    .line 199
    :try_start_4
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Out of memory trying to scale/rotate image."

    invoke-static {v5, v6, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_8
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 267
    :catch_1
    move-exception v2

    move-object v2, v4

    .line 268
    :goto_3
    const/16 v4, 0x1a0

    :try_start_5
    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 282
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    .line 287
    :cond_1
    :goto_4
    return-void

    .line 210
    :cond_2
    :try_start_6
    invoke-direct {p0, v9, v6, v7}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a(Lorg/apache/http/Header;J)Landroid/util/Pair;

    move-result-object v2

    .line 211
    if-nez v2, :cond_3

    .line 212
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v2
    :try_end_6
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_8
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 269
    :catch_2
    move-exception v2

    move-object v5, v4

    .line 270
    :goto_5
    const/16 v2, 0x190

    :try_start_7
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 282
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto :goto_4

    .line 215
    :cond_3
    :try_start_8
    const-string v5, "Content-Range"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "bytes "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "-"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v10, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "/"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-interface {v0, v5, v6}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_8
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_8 .. :try_end_8} :catch_9
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_8 .. :try_end_8} :catch_a
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-object v8, v2

    goto/16 :goto_1

    .line 224
    :cond_4
    const-wide/16 v12, 0x0

    cmp-long v2, v10, v12

    if-gez v2, :cond_0

    .line 225
    :try_start_9
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    .line 267
    :catch_3
    move-exception v2

    move-object v2, v5

    goto :goto_3

    .line 229
    :cond_5
    iget-object v2, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v2, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v6, v10

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    .line 230
    sget-object v2, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v4, 0x3

    if-gt v2, v4, :cond_6

    .line 231
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "::handle:Serving "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, ". Sending bytes "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v10, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "-"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " ("

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ")"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_6
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {p1, v0, v2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Ljava/lang/String;)V

    .line 245
    instance-of v2, v3, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;

    if-eqz v2, :cond_8

    .line 246
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;->g()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v4, "getCaptionInfo.sec"

    invoke-interface {p1, v4}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    if-eqz v4, :cond_8

    const-string v8, "1"

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_f
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_9 .. :try_end_9} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v4

    if-eqz v4, :cond_8

    :try_start_a
    invoke-static {v2}, Lcom/sec/pcw/service/b/k;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const/4 v2, 0x0

    if-eqz v4, :cond_d

    const-string v2, "captionURL"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    :goto_6
    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "Host"

    invoke-interface {p1, v2}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v8

    const/4 v2, 0x0

    if-eqz v8, :cond_9

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    :cond_7
    :goto_7
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "/"

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "http://"

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "CaptionInfo.sec"

    move-object/from16 v0, p2

    invoke-interface {v0, v4, v2}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_10
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_a .. :try_end_a} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 249
    :cond_8
    :goto_8
    :try_start_b
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->f()Ljava/lang/Iterable;
    :try_end_b
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_f
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_b .. :try_end_b} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_b .. :try_end_b} :catch_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v2

    .line 251
    if-eqz v2, :cond_a

    .line 254
    :try_start_c
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 255
    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-interface {v0, v4, v2}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_f
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_c .. :try_end_c} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_c .. :try_end_c} :catch_b
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_9

    .line 258
    :catch_4
    move-exception v2

    :try_start_d
    new-instance v2, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException;-><init>(B)V

    throw v2
    :try_end_d
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_f
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_d .. :try_end_d} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_d .. :try_end_d} :catch_b
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 269
    :catch_5
    move-exception v2

    goto/16 :goto_5

    .line 246
    :cond_9
    :try_start_e
    const-string v8, "MFL_ATTR_CONN"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lorg/apache/http/impl/SocketHttpServerConnection;

    if-eqz v8, :cond_7

    const-string v2, "MFL_ATTR_CONN"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/impl/SocketHttpServerConnection;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lorg/apache/http/impl/SocketHttpServerConnection;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v10

    invoke-virtual {v10}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v10, 0x3a

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lorg/apache/http/impl/SocketHttpServerConnection;->getLocalPort()I

    move-result v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_10
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_f
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_e} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_e .. :try_end_e} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_e .. :try_end_e} :catch_b
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    move-result-object v2

    goto/16 :goto_7

    .line 262
    :cond_a
    :try_start_f
    new-instance v2, Lcom/mfluent/asp/util/m;

    invoke-direct {v2, v5, v6, v7}, Lcom/mfluent/asp/util/m;-><init>(Ljava/io/InputStream;J)V

    .line 263
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mfluent/asp/util/m;->setContentType(Ljava/lang/String;)V

    .line 264
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 265
    if-nez v9, :cond_b

    const/16 v2, 0xc8

    :goto_a
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_f
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_f} :catch_d
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$InvalidHeaderException; {:try_start_f .. :try_end_f} :catch_c
    .catch Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$ServiceUnavailableException; {:try_start_f .. :try_end_f} :catch_b
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 266
    const/4 v2, 0x0

    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto/16 :goto_4

    .line 265
    :cond_b
    const/16 v2, 0xce

    goto :goto_a

    .line 271
    :catch_6
    move-exception v2

    move-object v5, v4

    .line 272
    :goto_b
    const/16 v2, 0x194

    :try_start_10
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 282
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto/16 :goto_4

    .line 273
    :catch_7
    move-exception v2

    move-object v5, v4

    .line 274
    :goto_c
    const/16 v2, 0x1f4

    :try_start_11
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 282
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto/16 :goto_4

    .line 275
    :catch_8
    move-exception v2

    move-object v5, v4

    .line 276
    :goto_d
    const/16 v2, 0x1f4

    :try_start_12
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 282
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto/16 :goto_4

    .line 277
    :catch_9
    move-exception v2

    move-object v5, v4

    .line 278
    :goto_e
    const/16 v2, 0x1f4

    :try_start_13
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 282
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto/16 :goto_4

    .line 279
    :catch_a
    move-exception v2

    move-object v5, v4

    .line 280
    :goto_f
    const/16 v2, 0x1f7

    :try_start_14
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 282
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    goto/16 :goto_4

    .line 282
    :catchall_0
    move-exception v2

    move-object v5, v4

    :goto_10
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 283
    if-eqz v3, :cond_c

    .line 284
    invoke-interface {v3}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;->e()V

    :cond_c
    throw v2

    .line 282
    :catchall_1
    move-exception v2

    goto :goto_10

    :catchall_2
    move-exception v4

    move-object v5, v2

    move-object v2, v4

    goto :goto_10

    .line 279
    :catch_b
    move-exception v2

    goto :goto_f

    .line 277
    :catch_c
    move-exception v2

    goto :goto_e

    .line 275
    :catch_d
    move-exception v2

    goto :goto_d

    .line 273
    :catch_e
    move-exception v2

    goto :goto_c

    .line 271
    :catch_f
    move-exception v2

    goto :goto_b

    :catch_10
    move-exception v2

    goto/16 :goto_8

    :cond_d
    move-object v4, v2

    goto/16 :goto_6
.end method
