.class public final Lcom/mfluent/asp/dws/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/a$b;,
        Lcom/mfluent/asp/dws/a$c;,
        Lcom/mfluent/asp/dws/a$d;,
        Lcom/mfluent/asp/dws/a$a;,
        Lcom/mfluent/asp/dws/a$f;,
        Lcom/mfluent/asp/dws/a$j;,
        Lcom/mfluent/asp/dws/a$g;,
        Lcom/mfluent/asp/dws/a$i;,
        Lcom/mfluent/asp/dws/a$h;,
        Lcom/mfluent/asp/dws/a$e;
    }
.end annotation


# static fields
.field public static final a:[Lcom/mfluent/asp/dws/a$g;

.field public static final b:[Lcom/mfluent/asp/dws/a$g;

.field public static final c:[Lcom/mfluent/asp/dws/a$g;

.field public static final d:[Lcom/mfluent/asp/dws/a$g;

.field private static final e:[Lcom/mfluent/asp/dws/a$g;

.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 498
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/mfluent/asp/dws/a$g;

    new-instance v1, Lcom/mfluent/asp/dws/a$j;

    const-string v2, "id"

    const-string v3, "source_media_id"

    invoke-direct {v1, v2, v3}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/mfluent/asp/dws/a$d;

    const-string v2, "dateAdded"

    const-string v3, "date_added"

    invoke-direct {v1, v2, v3}, Lcom/mfluent/asp/dws/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v9

    new-instance v1, Lcom/mfluent/asp/dws/a$j;

    const-string v2, "uri"

    const-string v3, "full_uri"

    invoke-direct {v1, v2, v3, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v10

    new-instance v1, Lcom/mfluent/asp/dws/a$j;

    const-string v2, "thumbUri"

    const-string v3, "thumbnail_uri"

    invoke-direct {v1, v2, v3, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v11

    const/4 v1, 0x4

    new-instance v2, Lcom/mfluent/asp/dws/a$d;

    const-string v3, "dateModified"

    const-string v4, "date_modified"

    invoke-direct {v2, v3, v4}, Lcom/mfluent/asp/dws/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "fileName"

    const-string v4, "_display_name"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "mimeType"

    const-string v4, "mime_type"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/mfluent/asp/dws/a$f;

    const-string v3, "size"

    const-string v4, "_size"

    invoke-direct {v2, v3, v4}, Lcom/mfluent/asp/dws/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "title"

    const-string v4, "title"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "file_sha1"

    const-string v4, "file_digest"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "bucket"

    const-string v4, "bucket_display_name"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "filePath"

    const-string v4, "_data"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 514
    sput-object v0, Lcom/mfluent/asp/dws/a;->e:[Lcom/mfluent/asp/dws/a$g;

    const/16 v1, 0x15

    new-array v1, v1, [Lcom/mfluent/asp/dws/a$g;

    new-instance v2, Lcom/mfluent/asp/dws/a$c;

    const-string v3, "dateTaken"

    const-string v4, "datetaken"

    invoke-direct {v2, v3, v4}, Lcom/mfluent/asp/dws/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "description"

    const-string v4, "description"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "latitude"

    const-string v4, "latitude"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v10

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "longitude"

    const-string v4, "longitude"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v11

    const/4 v2, 0x4

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_locale"

    const-string v5, "geo_loc_locale"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_country"

    const-string v5, "geo_loc_country"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_province"

    const-string v5, "geo_loc_province"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_sub_province"

    const-string v5, "geo_loc_sub_province"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_locality"

    const-string v5, "geo_loc_locality"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_sub_locality"

    const-string v5, "geo_loc_sub_locality"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_feature"

    const-string v5, "geo_loc_feature"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_thoroughfare"

    const-string v5, "geo_loc_thoroughfare"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_sub_thoroughfare"

    const-string v5, "geo_loc_sub_thoroughfare"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_premises"

    const-string v5, "geo_loc_premises"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "orientation"

    const-string v5, "orientation"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "picasaId"

    const-string v5, "picasa_id"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x10

    new-instance v3, Lcom/mfluent/asp/dws/a$f;

    const-string v4, "width"

    const-string v5, "width"

    invoke-direct {v3, v4, v5}, Lcom/mfluent/asp/dws/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-instance v3, Lcom/mfluent/asp/dws/a$f;

    const-string v4, "height"

    const-string v5, "height"

    invoke-direct {v3, v4, v5}, Lcom/mfluent/asp/dws/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x12

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "groupId"

    const-string v5, "group_id"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x13

    new-instance v3, Lcom/mfluent/asp/dws/a$f;

    const-string v4, "thumb_width"

    const-string v5, "thumb_width"

    invoke-direct {v3, v4, v5}, Lcom/mfluent/asp/dws/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x14

    new-instance v3, Lcom/mfluent/asp/dws/a$f;

    const-string v4, "thumb_height"

    const-string v5, "thumb_height"

    invoke-direct {v3, v4, v5}, Lcom/mfluent/asp/dws/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ArrayUtils;->addAll([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/dws/a$g;

    sput-object v0, Lcom/mfluent/asp/dws/a;->a:[Lcom/mfluent/asp/dws/a$g;

    .line 538
    sget-object v0, Lcom/mfluent/asp/dws/a;->e:[Lcom/mfluent/asp/dws/a$g;

    const/16 v1, 0xd

    new-array v1, v1, [Lcom/mfluent/asp/dws/a$g;

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "album"

    const-string v4, "album"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "albumKey"

    const-string v4, "album_key"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "artist"

    const-string v4, "artist"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v10

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "artistKey"

    const-string v4, "artist_key"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v11

    const/4 v2, 0x4

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "albumArtist"

    const-string v5, "album_artist"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "bookmark"

    const-string v5, "bookmark"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "composer"

    const-string v5, "composer"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "duration"

    const-string v5, "duration"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "isMusic"

    const-string v5, "is_music"

    const-string v6, "0"

    invoke-direct {v3, v4, v5, v6}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "isPodcast"

    const-string v5, "is_podcast"

    const-string v6, "0"

    invoke-direct {v3, v4, v5, v6}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "titleKey"

    const-string v5, "title_key"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "track"

    const-string v5, "track"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "year"

    const-string v5, "year"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ArrayUtils;->addAll([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/dws/a$g;

    sput-object v0, Lcom/mfluent/asp/dws/a;->b:[Lcom/mfluent/asp/dws/a$g;

    .line 557
    sget-object v0, Lcom/mfluent/asp/dws/a;->e:[Lcom/mfluent/asp/dws/a$g;

    const/16 v1, 0x1a

    new-array v1, v1, [Lcom/mfluent/asp/dws/a$g;

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "album"

    const-string v4, "album"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "artist"

    const-string v4, "artist"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "bookmark"

    const-string v4, "bookmark"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v10

    new-instance v2, Lcom/mfluent/asp/dws/a$j;

    const-string v3, "category"

    const-string v4, "category"

    invoke-direct {v2, v3, v4, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v11

    const/4 v2, 0x4

    new-instance v3, Lcom/mfluent/asp/dws/a$c;

    const-string v4, "dateTaken"

    const-string v5, "datetaken"

    invoke-direct {v3, v4, v5}, Lcom/mfluent/asp/dws/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "description"

    const-string v5, "description"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "duration"

    const-string v5, "duration"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "language"

    const-string v5, "language"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "latitude"

    const-string v5, "latitude"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "longitude"

    const-string v5, "longitude"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_locale"

    const-string v5, "geo_loc_locale"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_country"

    const-string v5, "geo_loc_country"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_province"

    const-string v5, "geo_loc_province"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_sub_province"

    const-string v5, "geo_loc_sub_province"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_locality"

    const-string v5, "geo_loc_locality"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_sub_locality"

    const-string v5, "geo_loc_sub_locality"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x10

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_feature"

    const-string v5, "geo_loc_feature"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_thoroughfare"

    const-string v5, "geo_loc_thoroughfare"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x12

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_sub_thoroughfare"

    const-string v5, "geo_loc_sub_thoroughfare"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x13

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "geo_loc_premises"

    const-string v5, "geo_loc_premises"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x14

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "resolution"

    const-string v5, "resolution"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x15

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "width"

    const-string v5, "width"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x16

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "height"

    const-string v5, "height"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x17

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "orientation"

    const-string v5, "orientation"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x18

    new-instance v3, Lcom/mfluent/asp/dws/a$j;

    const-string v4, "tags"

    const-string v5, "tags"

    invoke-direct {v3, v4, v5, v7}, Lcom/mfluent/asp/dws/a$j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/16 v2, 0x19

    new-instance v3, Lcom/mfluent/asp/dws/a$b;

    const-string v4, "captionInfo"

    invoke-direct {v3, v4}, Lcom/mfluent/asp/dws/a$b;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ArrayUtils;->addAll([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/dws/a$g;

    sput-object v0, Lcom/mfluent/asp/dws/a;->c:[Lcom/mfluent/asp/dws/a$g;

    .line 587
    sget-object v0, Lcom/mfluent/asp/dws/a;->e:[Lcom/mfluent/asp/dws/a$g;

    new-instance v1, Lcom/mfluent/asp/dws/a$a;

    const-string v2, "isPersonal"

    const-string v3, "is_personal"

    invoke-direct {v1, v2, v3}, Lcom/mfluent/asp/dws/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ArrayUtils;->add([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/dws/a$g;

    sput-object v0, Lcom/mfluent/asp/dws/a;->d:[Lcom/mfluent/asp/dws/a$g;

    .line 594
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 595
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "Music"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "Photo"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "Video"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "Document"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/a;->f:Ljava/util/Map;

    .line 604
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 605
    const-string v1, "Music"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 606
    const-string v1, "Photo"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    const-string v1, "Video"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    const-string v1, "Document"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/a;->g:Ljava/util/Map;

    .line 610
    return-void
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/mfluent/asp/dws/a;->g:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/mfluent/asp/dws/a;->f:Ljava/util/Map;

    return-object v0
.end method
