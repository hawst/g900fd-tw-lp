.class public final Lcom/mfluent/asp/dws/handlers/g;
.super Lcom/mfluent/asp/dws/handlers/c;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/c;-><init>()V

    return-void
.end method

.method private b(Lorg/json/JSONObject;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 117
    if-eqz p1, :cond_b

    .line 118
    const/4 v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "audio_id"

    aput-object v1, v8, v0

    .line 119
    sget-object v0, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "all"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "members"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 120
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 121
    const-string v0, "search"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    const-string v0, "items"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 123
    invoke-static {p1}, Lcom/mfluent/asp/dws/handlers/g;->a(Lorg/json/JSONObject;)Z

    move-result v0

    .line 124
    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v3, 0x64

    invoke-direct {v11, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 125
    new-instance v12, Ljava/util/ArrayList;

    const/16 v3, 0x14

    invoke-direct {v12, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 127
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 128
    invoke-static {v1}, Lcom/mfluent/asp/dws/handlers/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 129
    const-string v3, "name LIKE \'%\' || ? || \'%\' ESCAPE \'\\\' AND "

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v1, "genre_id"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    if-nez v0, :cond_c

    .line 134
    const-string v0, " NOT"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const/4 v0, 0x1

    move v6, v0

    .line 138
    :goto_0
    const-string v0, " IN ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const/4 v1, 0x0

    .line 140
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 141
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 142
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 143
    if-lez v1, :cond_0

    .line 144
    const/16 v4, 0x2c

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    const/16 v4, 0x3f

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    add-int/lit8 v1, v1, 0x1

    .line 140
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 151
    :cond_2
    const/16 v0, 0x29

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 154
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/g;->a:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_4

    .line 161
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 162
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 163
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    .line 165
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    move-object v0, v7

    .line 172
    :goto_3
    invoke-static {v10}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 173
    const-string v1, "genre_id=? AND "

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    const-string v1, "audio_id"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :goto_4
    if-nez v6, :cond_5

    .line 180
    const-string v1, " NOT"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    :cond_5
    const-string v1, " IN ("

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const/4 v2, 0x0

    .line 184
    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_9

    .line 185
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 186
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 187
    if-lez v2, :cond_6

    .line 188
    const/16 v4, 0x2c

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 190
    :cond_6
    const/16 v4, 0x3f

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    add-int/lit8 v2, v2, 0x1

    .line 184
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 177
    :cond_8
    const-string v1, "genre_id"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 195
    :cond_9
    const/16 v0, 0x29

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/g;->a:Landroid/content/ContentResolver;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, v9

    move-object v2, v8

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 198
    if-eqz v1, :cond_b

    .line 200
    :goto_6
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 201
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_6

    .line 204
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 208
    :cond_b
    return-void

    :cond_c
    move v6, v0

    goto/16 :goto_0

    :cond_d
    move v6, v0

    move-object v0, v2

    goto/16 :goto_3
.end method


# virtual methods
.method protected final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const-string v0, "title"

    return-object v0
.end method

.method protected final b(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/dws/handlers/c$a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 39
    const-string v1, "view"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    const-string v1, "SONG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    const-string v0, "depth1"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 44
    invoke-super {p0, v0}, Lcom/mfluent/asp/dws/handlers/c;->b(Lorg/json/JSONObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 45
    :cond_0
    const-string v1, "ALBUM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "ARTIST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "GENRE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 46
    :cond_1
    const-string v1, "depth1"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 47
    const-string v1, "depth2"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 49
    const-string v1, "GENRE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 50
    new-instance v1, Lcom/mfluent/asp/dws/handlers/c$a;

    invoke-direct {v1}, Lcom/mfluent/asp/dws/handlers/c$a;-><init>()V

    const-string v2, "_id"

    iput-object v2, v1, Lcom/mfluent/asp/dws/handlers/c$a;->g:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v2, v1, Lcom/mfluent/asp/dws/handlers/c$a;->f:Ljava/util/ArrayList;

    invoke-direct {p0, v4, v2}, Lcom/mfluent/asp/dws/handlers/g;->b(Lorg/json/JSONObject;Ljava/util/ArrayList;)V

    :cond_2
    if-eqz v5, :cond_3

    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    iget-object v3, v1, Lcom/mfluent/asp/dws/handlers/c$a;->f:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/mfluent/asp/dws/handlers/g;->b(Lorg/json/JSONObject;Ljava/util/ArrayList;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 54
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0x14

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    const-string v3, "ALBUM"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 56
    const-string v3, "album_id"

    .line 57
    const-string v2, "album"

    .line 63
    :goto_2
    if-eqz v4, :cond_5

    .line 64
    new-instance v6, Lcom/mfluent/asp/dws/handlers/c$a;

    invoke-direct {v6}, Lcom/mfluent/asp/dws/handlers/c$a;-><init>()V

    .line 65
    invoke-static {v4}, Lcom/mfluent/asp/dws/handlers/g;->a(Lorg/json/JSONObject;)Z

    move-result v7

    iput-boolean v7, v6, Lcom/mfluent/asp/dws/handlers/c$a;->e:Z

    .line 66
    iget-object v7, v6, Lcom/mfluent/asp/dws/handlers/c$a;->f:Ljava/util/ArrayList;

    invoke-static {v4, v7}, Lcom/mfluent/asp/dws/handlers/g;->a(Lorg/json/JSONObject;Ljava/util/ArrayList;)V

    .line 67
    iput-object v3, v6, Lcom/mfluent/asp/dws/handlers/c$a;->g:Ljava/lang/String;

    .line 68
    invoke-static {p1, v6, v2}, Lcom/mfluent/asp/dws/handlers/g;->a(Lorg/json/JSONObject;Lcom/mfluent/asp/dws/handlers/c$a;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_5
    if-eqz v5, :cond_7

    .line 73
    :goto_3
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 74
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 76
    new-instance v4, Lcom/mfluent/asp/dws/handlers/c$a;

    invoke-direct {v4}, Lcom/mfluent/asp/dws/handlers/c$a;-><init>()V

    .line 77
    invoke-static {v2}, Lcom/mfluent/asp/dws/handlers/g;->a(Lorg/json/JSONObject;)Z

    move-result v6

    iput-boolean v6, v4, Lcom/mfluent/asp/dws/handlers/c$a;->e:Z

    .line 78
    iput-object v3, v4, Lcom/mfluent/asp/dws/handlers/c$a;->a:Ljava/lang/String;

    .line 79
    const-string v6, "id"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/mfluent/asp/dws/handlers/c$a;->b:Ljava/lang/String;

    .line 80
    const-string v6, "_id"

    iput-object v6, v4, Lcom/mfluent/asp/dws/handlers/c$a;->g:Ljava/lang/String;

    .line 81
    iget-object v6, v4, Lcom/mfluent/asp/dws/handlers/c$a;->f:Ljava/util/ArrayList;

    invoke-static {v2, v6}, Lcom/mfluent/asp/dws/handlers/g;->a(Lorg/json/JSONObject;Ljava/util/ArrayList;)V

    .line 83
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 59
    :cond_6
    const-string v3, "artist_id"

    .line 60
    const-string v2, "artist"

    goto :goto_2

    :cond_7
    move-object v0, v1

    .line 87
    goto/16 :goto_0

    .line 92
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
