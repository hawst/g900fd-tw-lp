.class final Lcom/mfluent/asp/dws/handlers/ao$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/dws/handlers/ao;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/dws/handlers/ao;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/dws/handlers/ao;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ao$1;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 112
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->a()Lorg/slf4j/Logger;

    move-result-object v0

    invoke-interface {v0}, Lorg/slf4j/Logger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::SaveFilesHandler::onReceive: {}"

    invoke-static {p2}, Lcom/mfluent/asp/common/util/IntentHelper;->intentToString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 116
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_1

    .line 118
    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    const-string v2, "isDeleteSessionAfterCancel"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 120
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$1;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v2, v1, v0}, Lcom/mfluent/asp/dws/handlers/ao;->a(Lcom/mfluent/asp/dws/handlers/ao;Ljava/lang/String;Z)V

    .line 122
    :cond_1
    return-void
.end method
