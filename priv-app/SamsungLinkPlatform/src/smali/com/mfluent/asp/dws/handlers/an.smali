.class public Lcom/mfluent/asp/dws/handlers/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/mfluent/asp/dws/handlers/an;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method


# virtual methods
.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x1f4

    .line 29
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 31
    const-string v1, "path"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    const-string v2, "isRecursive"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    sget-object v2, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v3, "Handing rmdir request for {}, isRecursive={}"

    invoke-interface {v2, v3, v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 37
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    sget-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v2, "::handle: request does not have valid input information path={}"

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    const/16 v0, 0x190

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 72
    :goto_0
    return-void

    .line 43
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 46
    sget-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v1, "::handle: path {} is not a directory"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    invoke-interface {p2, v4}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0

    .line 51
    :cond_1
    if-eqz v0, :cond_3

    .line 53
    sget-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v1, "Deleting directory recursively"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 55
    invoke-static {v2}, Lorg/apache/commons/io/FileUtils;->deleteDirectory(Ljava/io/File;)V

    .line 70
    :cond_2
    sget-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v1, "Completed rmdir request."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_3
    sget-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v1, "Deleting only 1 directory"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 61
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v0

    .line 63
    if-nez v0, :cond_2

    .line 64
    sget-object v0, Lcom/mfluent/asp/dws/handlers/an;->a:Lorg/slf4j/Logger;

    const-string v1, "::handle: failed to delete {} "

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    invoke-interface {p2, v4}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0
.end method
