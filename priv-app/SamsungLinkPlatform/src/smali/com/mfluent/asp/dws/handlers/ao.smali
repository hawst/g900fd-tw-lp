.class public Lcom/mfluent/asp/dws/handlers/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/ao$a;,
        Lcom/mfluent/asp/dws/handlers/ao$b;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Future",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private final f:Lcom/mfluent/asp/filetransfer/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/ao;->a:Lorg/slf4j/Logger;

    .line 88
    const-string v0, ".*filename=\"([^\"]*)\".*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/ao;->b:Ljava/util/regex/Pattern;

    .line 93
    const-string v0, "bytes (\\d+)-.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/ao;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->d:Ljava/util/Map;

    .line 102
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ao;->e:Landroid/content/Context;

    .line 103
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    .line 105
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.CANCELED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Lcom/mfluent/asp/dws/handlers/ao$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/dws/handlers/ao$1;-><init>(Lcom/mfluent/asp/dws/handlers/ao;)V

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 125
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 126
    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.RETRY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Lcom/mfluent/asp/dws/handlers/ao$2;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/dws/handlers/ao$2;-><init>(Lcom/mfluent/asp/dws/handlers/ao;)V

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 152
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/dws/handlers/ao;)Lcom/mfluent/asp/filetransfer/d;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    return-object v0
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/mfluent/asp/dws/handlers/ao;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/dws/handlers/ao;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v0, p1}, Lcom/mfluent/asp/filetransfer/d;->c(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    :try_start_0
    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->G()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "api/pCloud/transfer/upload/sessions/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v1, v0, v2}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/dws/handlers/ao;->a:Lorg/slf4j/Logger;

    const-string v2, "Trouble retrying save session {}"

    invoke-interface {v1, v2, p1, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/dws/handlers/ao;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/mfluent/asp/dws/handlers/ao;->a:Lorg/slf4j/Logger;

    const-string v2, "::cancel: Cancelling transfer {}"

    invoke-interface {v1, v2, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v0, p1}, Lcom/mfluent/asp/filetransfer/d;->c(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Lcom/mfluent/asp/dws/handlers/ap;->a(Z)V

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v1, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/dws/handlers/ao;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/mfluent/asp/dws/handlers/ao;->b:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic c()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/mfluent/asp/dws/handlers/ao;->c:Ljava/util/regex/Pattern;

    return-object v0
.end method


# virtual methods
.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-nez v2, :cond_1

    .line 193
    sget-object v2, Lcom/mfluent/asp/dws/handlers/ao;->a:Lorg/slf4j/Logger;

    const-string v3, "::handle: Expected the request to be an HttpEntityEnclosingRequest"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 194
    const/16 v2, 0x190

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 201
    const-string v2, "baseTargetPath"

    invoke-virtual {v4, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 202
    const-string v2, "currentTab"

    invoke-virtual {v4, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 204
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 205
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 207
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-nez v6, :cond_c

    .line 208
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_c

    .line 209
    sget-object v2, Lcom/mfluent/asp/dws/handlers/ao;->a:Lorg/slf4j/Logger;

    const-string v4, "::handle: Couldn\'t create baseTargetPath {}"

    invoke-interface {v2, v4, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 210
    const/16 v2, 0x190

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0

    .line 215
    :cond_2
    invoke-static {v5}, Lcom/mfluent/asp/dws/f;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    move-object v3, v2

    .line 217
    :goto_1
    const/4 v2, 0x0

    .line 219
    const-string v6, "true"

    const-string v7, "isAutoArchive"

    invoke-virtual {v4, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 220
    const/4 v2, 0x1

    move v8, v2

    :goto_2
    move-object/from16 v2, p1

    .line 222
    check-cast v2, Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 223
    invoke-interface {v2}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    .line 225
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v10

    .line 227
    const-string v2, "sessionId"

    invoke-virtual {v4, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 228
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v6, v2}, Lcom/mfluent/asp/filetransfer/d;->c(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v7

    .line 230
    if-eqz v7, :cond_4

    .line 231
    invoke-virtual {v7}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 232
    :cond_3
    const/4 v7, 0x0

    .line 239
    :cond_4
    :goto_3
    if-nez v7, :cond_5

    .line 240
    new-instance v7, Lcom/mfluent/asp/dws/handlers/ap;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/dws/handlers/ao;->e:Landroid/content/Context;

    invoke-direct {v7, v6, v2}, Lcom/mfluent/asp/dws/handlers/ap;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 242
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v2, v7, v8}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;Z)V

    .line 243
    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v7, v2}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 245
    const-string v2, "files"

    invoke-virtual {v4, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v7, v2}, Lcom/mfluent/asp/dws/handlers/ap;->a(I)V

    .line 246
    invoke-virtual {v7, v10, v11}, Lcom/mfluent/asp/dws/handlers/ap;->a(J)V

    .line 247
    invoke-virtual {v7, v5}, Lcom/mfluent/asp/dws/handlers/ap;->a(Ljava/lang/String;)V

    .line 249
    const-string v2, "MFL_ATTR_DEVICE"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/datamodel/Device;

    .line 250
    invoke-virtual {v7, v2}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 254
    :cond_5
    new-instance v6, Lcom/mfluent/asp/dws/handlers/ao$b;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v3, v7}, Lcom/mfluent/asp/dws/handlers/ao$b;-><init>(Lcom/mfluent/asp/dws/handlers/ao;Ljava/io/File;Lcom/mfluent/asp/dws/handlers/ap;)V

    .line 255
    new-instance v2, Lorg/apache/james/mime4j/parser/MimeEntityConfig;

    invoke-direct {v2}, Lorg/apache/james/mime4j/parser/MimeEntityConfig;-><init>()V

    .line 256
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/james/mime4j/parser/MimeEntityConfig;->setStrictParsing(Z)V

    .line 257
    new-instance v4, Lorg/apache/james/mime4j/parser/MimeStreamParser;

    invoke-direct {v4, v2}, Lorg/apache/james/mime4j/parser/MimeStreamParser;-><init>(Lorg/apache/james/mime4j/parser/MimeEntityConfig;)V

    .line 258
    invoke-virtual {v4, v6}, Lorg/apache/james/mime4j/parser/MimeStreamParser;->setContentHandler(Lorg/apache/james/mime4j/parser/ContentHandler;)V

    .line 261
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 262
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    array-length v10, v5

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v10, :cond_7

    aget-object v11, v5, v2

    .line 263
    invoke-interface {v11}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 264
    const-string v12, ": "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 265
    invoke-interface {v11}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 266
    const-string v11, "\n"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 234
    :cond_6
    invoke-virtual {v7}, Lcom/mfluent/asp/dws/handlers/ap;->f()I

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/handlers/ap;->k()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v6, v12, v14

    if-nez v6, :cond_4

    .line 235
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 268
    :cond_7
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 269
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 271
    new-instance v5, Lcom/mfluent/asp/dws/handlers/ao$a;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/io/InputStream;

    const/4 v10, 0x0

    aput-object v2, v3, v10

    const/4 v2, 0x1

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v9

    aput-object v9, v3, v2

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v3}, Lcom/mfluent/asp/dws/handlers/ao$a;-><init>(Lcom/mfluent/asp/dws/handlers/ao;[Ljava/io/InputStream;)V

    .line 274
    invoke-static {}, Lcom/mfluent/asp/util/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v9

    new-instance v2, Lcom/mfluent/asp/dws/handlers/ao$3;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/mfluent/asp/dws/handlers/ao$3;-><init>(Lcom/mfluent/asp/dws/handlers/ao;Lorg/apache/james/mime4j/parser/MimeStreamParser;Ljava/io/InputStream;Lcom/mfluent/asp/dws/handlers/ao$b;Lcom/mfluent/asp/dws/handlers/ap;)V

    invoke-interface {v9, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v2

    .line 296
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/dws/handlers/ao;->d:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    :try_start_0
    invoke-interface {v2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/dws/handlers/ao;->d:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v2, v7}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->c()V

    .line 327
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    .line 328
    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a()V

    .line 331
    if-nez v8, :cond_0

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    goto/16 :goto_0

    .line 302
    :catch_0
    move-exception v2

    .line 303
    :try_start_1
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->b(Lcom/mfluent/asp/dws/handlers/ao$b;)Z

    .line 304
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v3

    sget-object v4, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 305
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/dws/handlers/ap;->F()V

    .line 306
    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->c()V

    .line 307
    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    .line 308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V

    .line 311
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/dws/handlers/ap;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v3

    sget-object v4, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 312
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v3

    sget-object v4, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 314
    :cond_8
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/dws/handlers/ap;->H()Z

    move-result v3

    if-nez v3, :cond_9

    .line 315
    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->c(Lcom/mfluent/asp/dws/handlers/ao$b;)V

    .line 317
    :cond_9
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Trouble saving files"

    invoke-direct {v3, v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 326
    :catchall_0
    move-exception v2

    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->c()V

    .line 327
    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    .line 328
    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a()V

    .line 331
    if-nez v8, :cond_a

    .line 332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/dws/handlers/ao;->f:Lcom/mfluent/asp/filetransfer/d;

    invoke-static {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    :cond_a
    throw v2

    .line 318
    :catch_1
    move-exception v2

    .line 319
    :try_start_2
    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ao$b;->b()V

    .line 324
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Trouble saving files"

    invoke-direct {v3, v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_b
    move v8, v2

    goto/16 :goto_2

    :cond_c
    move-object v3, v2

    goto/16 :goto_1
.end method
