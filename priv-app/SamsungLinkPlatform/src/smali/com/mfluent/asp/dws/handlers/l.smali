.class public final Lcom/mfluent/asp/dws/handlers/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/l$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/l;->a:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/FileTransferSession;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "uuid=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 103
    if-nez v1, :cond_0

    .line 114
    :goto_0
    return-object v2

    .line 108
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 109
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 112
    :cond_1
    :try_start_1
    new-instance v2, Lcom/mfluent/asp/dws/handlers/l$a;

    invoke-direct {v2, v1}, Lcom/mfluent/asp/dws/handlers/l$a;-><init>(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x194

    .line 48
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    const-string v0, "sessionId required"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 53
    const/16 v0, 0x190

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v2

    .line 59
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v3, "GET"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 61
    invoke-interface {v2, v1}, Lcom/mfluent/asp/filetransfer/d;->b(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    .line 62
    if-nez v0, :cond_2

    .line 63
    invoke-interface {v2}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession;

    .line 66
    :cond_2
    if-nez v0, :cond_3

    .line 67
    invoke-direct {p0, v1}, Lcom/mfluent/asp/dws/handlers/l;->a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/FileTransferSession;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_3

    .line 70
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 74
    :cond_3
    if-nez v0, :cond_4

    .line 75
    invoke-interface {p2, v4}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0

    .line 79
    :cond_4
    new-instance v1, Lcom/mfluent/asp/dws/handlers/m;

    invoke-direct {v1}, Lcom/mfluent/asp/dws/handlers/m;-><init>()V

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/m;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lorg/json/JSONObject;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/mfluent/asp/util/n;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/util/n;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 81
    :cond_5
    const-string v3, "POST"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 82
    invoke-interface {v2, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    .line 84
    invoke-interface {p2, v4}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0

    .line 87
    :cond_6
    const-string v3, "DELETE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 88
    const/4 v0, 0x0

    invoke-interface {v2, v1, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 90
    :cond_7
    const/16 v0, 0x1f5

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0
.end method
