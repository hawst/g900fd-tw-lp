.class abstract Lcom/mfluent/asp/dws/handlers/as$e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "e"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 655
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "keyword"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as$e;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/as$e;-><init>()V

    return-void
.end method

.method protected static a([Lcom/mfluent/asp/dws/a$g;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 683
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 685
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 686
    invoke-virtual {v3, v1}, Lcom/mfluent/asp/dws/a$g;->a(Ljava/util/ArrayList;)V

    .line 685
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 689
    :cond_0
    const-string v0, "_id"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 691
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public abstract a(I)Landroid/net/Uri;
.end method

.method public a(Landroid/content/ContentResolver;Lorg/json/JSONObject;Landroid/database/Cursor;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 645
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move v6, v7

    .line 647
    :goto_0
    sget-object v0, Lcom/mfluent/asp/dws/a$i$a$a;->b:[I

    array-length v0, v0

    if-ge v6, v0, :cond_3

    .line 648
    sget-object v0, Lcom/mfluent/asp/dws/a$i$a$a;->b:[I

    aget v0, v0, v6

    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/mfluent/asp/dws/handlers/as$e;->a:[Ljava/lang/String;

    const-string v3, "file_id=? AND keyword_type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v5, "keyword"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 649
    :cond_1
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 650
    sget-object v0, Lcom/mfluent/asp/dws/a$i$a$a;->a:[Ljava/lang/String;

    aget-object v0, v0, v6

    invoke-virtual {p2, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 647
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 653
    :cond_3
    return-void
.end method

.method public abstract a()[Ljava/lang/String;
.end method

.method public abstract b()[Lcom/mfluent/asp/dws/a$g;
.end method

.method public abstract c()Landroid/net/Uri;
.end method

.method public abstract d()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;
.end method
