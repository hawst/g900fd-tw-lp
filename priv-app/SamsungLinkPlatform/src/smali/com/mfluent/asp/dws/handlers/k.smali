.class public Lcom/mfluent/asp/dws/handlers/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/k$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Z

.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/mfluent/asp/dws/handlers/k;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/k;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(ZLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-boolean p1, p0, Lcom/mfluent/asp/dws/handlers/k;->b:Z

    .line 73
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/k;->c:Landroid/content/Context;

    .line 74
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/k$a;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 121
    new-instance v1, Lcom/mfluent/asp/dws/handlers/k$a;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/dws/handlers/k$a;-><init>(B)V

    .line 125
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 127
    const-string v3, "SPName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 128
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/mfluent/asp/datamodel/t;->c(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    iput-object v4, v1, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 129
    iget-object v4, v1, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    if-nez v4, :cond_0

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No device found for SPName "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble processing request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 132
    :cond_0
    :try_start_1
    const-string v3, "CurrentTab"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    .line 134
    const-string v3, "Files"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 135
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 136
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 139
    new-instance v4, Lcom/mfluent/asp/filetransfer/l;

    const-string v5, "ID"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Name"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Size"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {v4, v5, v6, v8, v9}, Lcom/mfluent/asp/filetransfer/l;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 144
    iget-object v3, v1, Lcom/mfluent/asp/dws/handlers/k$a;->a:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_1
    return-object v1
.end method

.method private static a(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    invoke-static {p0, p1, p2, p3}, Lcom/mfluent/asp/dws/handlers/k;->b(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 469
    if-nez v0, :cond_0

    .line 470
    const/4 v0, 0x0

    .line 472
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Set;Lorg/w3c/dom/Element;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/w3c/dom/Element;",
            ")V"
        }
    .end annotation

    .prologue
    .line 459
    const-string v0, "Items"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 460
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 461
    invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 462
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v2

    .line 463
    invoke-interface {p0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 460
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 465
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/String;Lorg/w3c/dom/Element;)Z
    .locals 3

    .prologue
    .line 455
    const-string v0, "INCLUDE"

    const-string v1, "Status"

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/k$a;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 265
    new-instance v7, Lcom/mfluent/asp/dws/handlers/k$a;

    invoke-direct {v7, v1}, Lcom/mfluent/asp/dws/handlers/k$a;-><init>(B)V

    .line 269
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 276
    :try_start_1
    new-instance v3, Ljava/io/ByteArrayInputStream;

    const-string v4, "UTF-8"

    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 284
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 286
    const-string v0, "SPName"

    invoke-static {p0, v3, v0, v10}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 287
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/mfluent/asp/datamodel/t;->c(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    iput-object v4, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 288
    iget-object v4, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    if-nez v4, :cond_0

    .line 289
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No device found for SPName "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble creating DocumentBuilder"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 277
    :catch_1
    move-exception v0

    .line 278
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "WTF"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 279
    :catch_2
    move-exception v0

    .line 280
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble parsing request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 281
    :catch_3
    move-exception v0

    .line 282
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble processing request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 291
    :cond_0
    const-string v0, "ContentType"

    invoke-static {p0, v3, v0, v10}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    .line 293
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v8

    .line 294
    if-nez v8, :cond_1

    .line 296
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    .line 297
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No cloudStorageSync found for device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_1
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 302
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 304
    const-string v0, "Depth1"

    invoke-static {p0, v3, v0, v1}, Lcom/mfluent/asp/dws/handlers/k;->b(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_2

    .line 307
    invoke-static {p0, v0}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 308
    invoke-static {v4, v0}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/util/Set;Lorg/w3c/dom/Element;)V

    .line 315
    :cond_2
    :goto_0
    const-string v0, "Depth2"

    invoke-static {p0, v3, v0, v1}, Lcom/mfluent/asp/dws/handlers/k;->b(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_16

    .line 317
    const-string v5, "Folder"

    invoke-static {p0, v0, v5, v1}, Lcom/mfluent/asp/dws/handlers/k;->b(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 318
    if-eqz v5, :cond_16

    .line 319
    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 320
    const-string v6, "ID"

    invoke-static {p0, v5, v6, v10}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 321
    invoke-static {p0, v0}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;)Z

    move-result v0

    .line 322
    invoke-static {v9, v5}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/util/Set;Lorg/w3c/dom/Element;)V

    move v6, v0

    .line 326
    :goto_1
    const-string v0, "FILES"

    iget-object v5, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 328
    const-string v0, "CurrentID"

    invoke-static {p0, v3, v0, v1}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 329
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 330
    const-string v0, "com.mfluent.ROOT_DIRECTORY_FOLDER_ID"

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 338
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 341
    :try_start_2
    const-string v2, "com.mfluent.ROOT_DIRECTORY_FOLDER_ID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 343
    invoke-interface {v8}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getCloudStorageFileBrowser()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    .line 344
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v10, 0x0

    invoke-interface {v0, v2, v4, v5, v10}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->init(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    move-object v2, v0

    :goto_3
    move v0, v1

    .line 354
    :goto_4
    invoke-interface {v2}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 357
    :try_start_3
    invoke-interface {v2, v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    move-result-object v4

    .line 366
    :try_start_4
    invoke-interface {v8, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_8

    move-result-object v5

    .line 372
    if-nez v6, :cond_5

    invoke-interface {v9, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 374
    :cond_5
    if-eqz v6, :cond_6

    invoke-interface {v9, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 379
    :cond_6
    new-instance v10, Lcom/mfluent/asp/filetransfer/l;

    invoke-interface {v4}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v12

    invoke-direct {v10, v5, v11, v12, v13}, Lcom/mfluent/asp/filetransfer/l;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 389
    iget-object v4, v7, Lcom/mfluent/asp/dws/handlers/k$a;->a:Ljava/util/List;

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 354
    :cond_7
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 310
    :cond_8
    invoke-static {v9, v0}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/util/Set;Lorg/w3c/dom/Element;)V

    goto/16 :goto_0

    .line 332
    :cond_9
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 346
    :cond_a
    :try_start_5
    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v8, v0, v2, v4, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getCloudStorageFileBrowser(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    move-result-object v0

    move-object v2, v0

    .line 352
    goto :goto_3

    .line 348
    :catch_4
    move-exception v0

    .line 349
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Interrupted while getting fileBrowser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 350
    :catch_5
    move-exception v0

    .line 351
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble getting fileBrowser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 358
    :catch_6
    move-exception v0

    .line 359
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Interrupted while traversing fileBrowser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 360
    :catch_7
    move-exception v0

    .line 361
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble traversing fileBrowser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 367
    :catch_8
    move-exception v5

    .line 368
    sget-object v10, Lcom/mfluent/asp/dws/handlers/k;->a:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "FileNotFound for file "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v10, v4, v5}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 395
    :cond_b
    const-string v0, "PHOTO"

    iget-object v1, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 396
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    .line 407
    :goto_6
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 408
    if-eqz v1, :cond_11

    .line 410
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 412
    :cond_c
    const-string v0, "media_type"

    invoke-static {v1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getIntOrThrow(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 413
    const-string v2, "source_media_id"

    invoke-static {v1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 414
    const-string v3, "full_uri"

    invoke-static {v1, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v3

    .line 418
    :try_start_7
    invoke-interface {v8, v0, v3, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getStorageGatewayFileId(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .line 424
    if-nez v6, :cond_d

    :try_start_8
    invoke-interface {v9, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 426
    :cond_d
    if-eqz v6, :cond_e

    invoke-interface {v9, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 431
    :cond_e
    new-instance v2, Lcom/mfluent/asp/filetransfer/l;

    const-string v3, "_display_name"

    invoke-static {v1, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "_size"

    invoke-static {v1, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/mfluent/asp/filetransfer/l;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 441
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443
    :cond_f
    :goto_7
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v0

    if-nez v0, :cond_c

    .line 446
    :cond_10
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 451
    :cond_11
    return-object v7

    .line 397
    :cond_12
    const-string v0, "MUSIC"

    iget-object v1, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 398
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_6

    .line 399
    :cond_13
    const-string v0, "VIDEO"

    iget-object v1, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 400
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_6

    .line 401
    :cond_14
    const-string v0, "DOC"

    iget-object v1, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 402
    iget-object v0, v7, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_6

    .line 404
    :cond_15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized ContentType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :catch_9
    move-exception v0

    .line 420
    :try_start_9
    sget-object v2, Lcom/mfluent/asp/dws/handlers/k;->a:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FileNotFound for media "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/database/DatabaseUtils;->dumpCurrentRowToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_7

    .line 446
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_16
    move v6, v1

    goto/16 :goto_1
.end method

.method private static b(Ljava/lang/String;Lorg/w3c/dom/Element;Ljava/lang/String;Z)Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 476
    invoke-interface {p1, p2}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 477
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 478
    if-eqz p3, :cond_0

    .line 479
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected exactly one \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' node in xml request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 481
    :cond_0
    const/4 v0, 0x0

    .line 484
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    goto :goto_0
.end method


# virtual methods
.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x190

    .line 78
    instance-of v0, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-nez v0, :cond_0

    .line 79
    sget-object v0, Lcom/mfluent/asp/dws/handlers/k;->a:Lorg/slf4j/Logger;

    const-string v1, "Expected some payload in the request but got none"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 80
    const-string v0, "No payload in the request"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 81
    invoke-interface {p2, v3}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 118
    :goto_0
    return-void

    .line 85
    :cond_0
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 86
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    :try_start_0
    iget-boolean v1, p0, Lcom/mfluent/asp/dws/handlers/k;->b:Z

    if-eqz v1, :cond_1

    .line 93
    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/k;->b(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/k$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 104
    :goto_1
    iget-object v1, v0, Lcom/mfluent/asp/dws/handlers/k$a;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 105
    new-instance v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 106
    iget-object v3, v0, Lcom/mfluent/asp/dws/handlers/k$a;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/mfluent/asp/dws/f;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    .line 108
    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/k;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v3

    .line 109
    iget-object v0, v0, Lcom/mfluent/asp/dws/handlers/k$a;->a:Ljava/util/List;

    invoke-interface {v3, v0, v1, v2}, Lcom/mfluent/asp/filetransfer/d;->a(Ljava/util/List;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    .line 111
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 113
    :try_start_1
    const-string v2, "sessionId"

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 117
    new-instance v0, Lcom/mfluent/asp/util/n;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/util/n;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 95
    :cond_1
    :try_start_2
    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/k;->a(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/k$a;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    sget-object v1, Lcom/mfluent/asp/dws/handlers/k;->a:Lorg/slf4j/Logger;

    const-string v2, "Trouble parsing request body"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 100
    invoke-interface {p2, v3}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0

    .line 114
    :catch_1
    move-exception v0

    .line 115
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble building response"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
