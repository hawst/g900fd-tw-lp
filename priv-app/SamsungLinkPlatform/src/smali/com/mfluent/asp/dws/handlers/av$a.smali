.class final Lcom/mfluent/asp/dws/handlers/av$a;
.super Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/av;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".srt"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "application/x-subrip"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ".smi"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "application/smil+xml"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ".sub"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "application/octet-stream"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ".ttxt"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "text/xml"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/dws/handlers/av$a;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;-><init>(Ljava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 37
    invoke-super {p0}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$c;->a()Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    const-string v2, "application/octet-stream"

    .line 41
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/av$a;->g()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 42
    const-string v0, ""

    .line 43
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 44
    if-ltz v3, :cond_0

    .line 45
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    :cond_0
    const/4 v1, 0x0

    :goto_0
    sget-object v3, Lcom/mfluent/asp/dws/handlers/av$a;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 49
    sget-object v3, Lcom/mfluent/asp/dws/handlers/av$a;->a:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 50
    sget-object v0, Lcom/mfluent/asp/dws/handlers/av$a;->a:[Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    .line 56
    :cond_1
    :goto_1
    return-object v0

    .line 48
    :cond_2
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method
