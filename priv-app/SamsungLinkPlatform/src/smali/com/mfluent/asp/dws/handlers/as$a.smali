.class final Lcom/mfluent/asp/dws/handlers/as$a;
.super Lcom/mfluent/asp/dws/handlers/as$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 699
    sget-object v0, Lcom/mfluent/asp/dws/a;->b:[Lcom/mfluent/asp/dws/a$g;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/as$e;->a([Lcom/mfluent/asp/dws/a$g;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as$a;->a:[Ljava/lang/String;

    .line 700
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;-><init>()V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    .line 701
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as$a;->c:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 696
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/dws/handlers/as$e;-><init>(B)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 696
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/as$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 725
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/ContentResolver;Lorg/json/JSONObject;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 730
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 732
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/mfluent/asp/dws/handlers/as$a;->c:[Ljava/lang/String;

    const-string v3, "audio_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v5, "name"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 739
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 740
    if-eqz v1, :cond_2

    .line 742
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 743
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 744
    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 746
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 749
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 753
    :cond_2
    const-string v1, "genre"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 754
    return-void
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 705
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as$a;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Lcom/mfluent/asp/dws/a$g;
    .locals 1

    .prologue
    .line 710
    sget-object v0, Lcom/mfluent/asp/dws/a;->b:[Lcom/mfluent/asp/dws/a$g;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 715
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;
    .locals 1

    .prologue
    .line 720
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    return-object v0
.end method
