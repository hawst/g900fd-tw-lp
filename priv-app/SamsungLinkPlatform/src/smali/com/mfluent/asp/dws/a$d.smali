.class final Lcom/mfluent/asp/dws/a$d;
.super Lcom/mfluent/asp/dws/a$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 341
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/dws/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 346
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 347
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/mfluent/asp/dws/a$d;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lorg/json/JSONObject;Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    .line 352
    iget-object v0, p0, Lcom/mfluent/asp/dws/a$d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 353
    invoke-static {v0}, Lcom/mfluent/asp/dws/a$d;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 354
    iget-object v1, p0, Lcom/mfluent/asp/dws/a$d;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 355
    return-void
.end method
