.class public final Lcom/mfluent/asp/dws/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/HttpRequestInterceptor;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final process(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x6

    const/4 v0, 0x1

    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://ignore"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 40
    :try_start_0
    const-string v1, "http://ignore/diag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "http://ignore/favicon.ico"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    :cond_0
    return-void

    .line 45
    :cond_1
    sget-object v1, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_2

    .line 46
    const-string v1, "mfl_RequestSignatureInterceptor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::process:checking signature for: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_2
    invoke-static {v2}, Lcom/mfluent/asp/dws/a/c;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 50
    sget-object v0, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_3

    .line 51
    const-string v0, "mfl_RequestSignatureInterceptor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::process:Invalid timestamp for Media Proxy. ["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_3
    new-instance v0, Lorg/apache/http/auth/AuthenticationException;

    const-string v1, "Invalid Timestamp"

    invoke-direct {v0, v1}, Lorg/apache/http/auth/AuthenticationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :catch_0
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_4

    .line 102
    const-string v0, "mfl_RequestSignatureInterceptor"

    const-string v1, "::buildCloudStreamUrl:Did not expect URL to be malformed"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_4
    new-instance v0, Lorg/apache/http/auth/AuthenticationException;

    const-string v1, "Invalid Signature"

    invoke-direct {v0, v1}, Lorg/apache/http/auth/AuthenticationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_5
    const/4 v1, 0x0

    .line 59
    :try_start_1
    invoke-static {v2}, Lcom/mfluent/asp/dws/a/c;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    move v1, v0

    .line 70
    :cond_6
    :goto_0
    if-nez v1, :cond_c

    const-string v3, "&xsig="

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 72
    const-string v3, "referer"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    .line 73
    if-eqz v3, :cond_c

    .line 74
    sget-object v4, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    if-gt v4, v5, :cond_7

    .line 75
    const-string v4, "mfl_RequestSignatureInterceptor"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::process:request contains referer header: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_7
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 79
    invoke-static {v3}, Lcom/mfluent/asp/dws/a/c;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 81
    sget-object v1, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v3, 0x4

    if-gt v1, v3, :cond_8

    .line 82
    const-string v1, "mfl_RequestSignatureInterceptor"

    const-string v3, "::process:referer signature is good."

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_8
    :goto_1
    if-nez v0, :cond_0

    .line 94
    sget-object v0, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_9

    .line 95
    const-string v0, "mfl_RequestSignatureInterceptor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::process:Invalid Signature for Media Proxy. ["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_9
    new-instance v0, Lorg/apache/http/auth/AuthenticationException;

    const-string v1, "Invalid Signature"

    invoke-direct {v0, v1}, Lorg/apache/http/auth/AuthenticationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_a
    const-string v3, "&amp;"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 63
    const-string v3, "&amp;"

    const-string v4, "&"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-static {v2}, Lcom/mfluent/asp/dws/a/c;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v1, v0

    .line 65
    goto/16 :goto_0

    .line 85
    :cond_b
    sget-object v0, Lcom/mfluent/asp/dws/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v3, 0x3

    if-gt v0, v3, :cond_c

    .line 86
    const-string v0, "mfl_RequestSignatureInterceptor"

    const-string v3, "::process:referer signature is not valid."

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_c
    move v0, v1

    goto :goto_1
.end method
