.class public final enum Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/GetSongListHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

.field public static final enum b:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

.field public static final enum c:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

.field public static final enum d:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

.field private static final synthetic e:[Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    .line 31
    new-instance v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    const-string v1, "ARTIST"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->b:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    .line 32
    new-instance v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    const-string v1, "GENRE"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->c:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    .line 33
    new-instance v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->d:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    sget-object v1, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->b:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->c:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->d:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->e:[Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->e:[Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    return-object v0
.end method
