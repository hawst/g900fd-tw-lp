.class public Lcom/mfluent/asp/dws/k;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Ljava/net/ServerSocket;

.field private d:Lorg/apache/http/params/HttpParams;

.field private e:Lorg/apache/http/protocol/HttpService;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 87
    const-class v0, Lcom/mfluent/asp/dws/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/k;->f:Z

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/k;->b:Landroid/content/Context;

    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/dws/k;->setDaemon(Z)V

    .line 92
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/k;->f:Z

    .line 96
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/k;->interrupt()V

    .line 97
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x3

    .line 115
    sget-object v0, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "mfl_VideoWebServer"

    const-string v2, "Starting VideoWebServer."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/dws/k;->f:Z

    if-nez v0, :cond_1

    .line 120
    :try_start_0
    new-instance v0, Ljava/net/ServerSocket;

    const/16 v2, 0x4150

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v0, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    if-nez v0, :cond_3

    .line 209
    :cond_2
    :goto_1
    return-void

    .line 120
    :catch_0
    move-exception v0

    const-string v2, "mfl_VideoWebServer"

    const-string v3, "::VideoWebServer:Failed to start VideoWebServer : ServerSocket(16720)"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 129
    :cond_3
    :try_start_1
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/k;->d:Lorg/apache/http/params/HttpParams;

    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->d:Lorg/apache/http/params/HttpParams;

    const-string v2, "http.socket.timeout"

    const/16 v3, 0x1388

    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v2, "http.socket.buffer-size"

    const/16 v3, 0x2000

    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v2, "http.connection.stalecheck"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v2, "http.tcp.nodelay"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v2, "http.origin-server"

    const-string v3, "HttpComponents/1.1"

    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 138
    new-instance v0, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 139
    new-instance v2, Lcom/mfluent/asp/dws/g;

    invoke-direct {v2}, Lcom/mfluent/asp/dws/g;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 140
    new-instance v2, Lorg/apache/http/protocol/ResponseDate;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseDate;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 141
    new-instance v2, Lorg/apache/http/protocol/ResponseServer;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseServer;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 142
    new-instance v2, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 143
    new-instance v2, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 144
    new-instance v2, Lcom/mfluent/asp/dws/j;

    invoke-direct {v2}, Lcom/mfluent/asp/dws/j;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 147
    new-instance v2, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v2}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    .line 148
    const-string v3, "/v1/media/download/*"

    new-instance v4, Lcom/mfluent/asp/dws/handlers/j;

    invoke-direct {v4}, Lcom/mfluent/asp/dws/handlers/j;-><init>()V

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 149
    const-string v3, "/v1/video/caption/*"

    new-instance v4, Lcom/mfluent/asp/dws/handlers/av;

    invoke-direct {v4}, Lcom/mfluent/asp/dws/handlers/av;-><init>()V

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 150
    const-string v3, "/v1/video/thumbnail/*"

    new-instance v4, Lcom/mfluent/asp/dws/handlers/aw;

    invoke-direct {v4}, Lcom/mfluent/asp/dws/handlers/aw;-><init>()V

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 153
    new-instance v3, Lorg/apache/http/protocol/HttpService;

    new-instance v4, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v5, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v5}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v3, v0, v4, v5}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v3, p0, Lcom/mfluent/asp/dws/k;->e:Lorg/apache/http/protocol/HttpService;

    .line 154
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->e:Lorg/apache/http/protocol/HttpService;

    iget-object v3, p0, Lcom/mfluent/asp/dws/k;->d:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v0, v3}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 155
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->e:Lorg/apache/http/protocol/HttpService;

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 157
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    if-eqz v0, :cond_4

    .line 158
    const-string v0, "mfl_VideoWebServer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Listening on port "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/k;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/mfluent/asp/dws/k;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_5

    .line 163
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    .line 164
    if-nez v2, :cond_6

    .line 165
    sget-object v0, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_4

    .line 166
    const-string v0, "mfl_VideoWebServer"

    const-string v2, "::run: Incoming connection, but ignoring, because returned socket is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 197
    :catch_1
    move-exception v0

    .line 200
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/net/ServerSocket;)V

    .line 206
    sget-object v0, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    const-string v0, "mfl_VideoWebServer"

    const-string v1, "Ending VideoWebServer..."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 171
    :cond_6
    :try_start_3
    invoke-virtual {v2}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v3

    if-nez v3, :cond_9

    sget-object v0, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v3, 0x6

    if-gt v0, v3, :cond_7

    const-string v0, "mfl_VideoWebServer"

    const-string v3, "::findDeviceByAddress: socketAddress is null, how did that happen?"

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_7
    move-object v0, v1

    .line 178
    :cond_8
    :goto_4
    if-nez v0, :cond_b

    .line 179
    :try_start_4
    sget-object v0, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_4

    .line 180
    const-string v0, "mfl_VideoWebServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::run: Ignnoring incoming connection from non-whitelist address: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 198
    :catch_2
    move-exception v0

    .line 199
    :try_start_5
    const-string v1, "mfl_VideoWebServer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "I/O error initialising connection thread: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 204
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/k;->c:Ljava/net/ServerSocket;

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/net/ServerSocket;)V

    throw v0

    .line 171
    :cond_9
    :try_start_6
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    new-instance v4, Lcom/mfluent/asp/dws/k$1;

    invoke-direct {v4, p0, v3}, Lcom/mfluent/asp/dws/k$1;-><init>(Lcom/mfluent/asp/dws/k;Ljava/net/SocketAddress;)V

    invoke-static {v0, v4}, Lorg/apache/commons/collections/CollectionUtils;->find(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->e()Ljava/util/Set;

    move-result-object v0

    new-instance v4, Lcom/mfluent/asp/dws/k$2;

    invoke-direct {v4, p0, v3}, Lcom/mfluent/asp/dws/k$2;-><init>(Lcom/mfluent/asp/dws/k;Ljava/net/SocketAddress;)V

    invoke-static {v0, v4}, Lorg/apache/commons/collections/CollectionUtils;->find(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/AllowedDevice;
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 172
    :catch_3
    move-exception v0

    .line 173
    :try_start_7
    sget-object v3, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v7, :cond_a

    .line 174
    const-string v3, "mfl_VideoWebServer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::run: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move-object v0, v1

    goto/16 :goto_4

    .line 184
    :cond_b
    new-instance v3, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v3}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 185
    sget-object v4, Lcom/mfluent/asp/dws/k;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    if-gt v4, v7, :cond_c

    .line 186
    const-string v4, "mfl_VideoWebServer"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::run: Incoming connection from address: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_c
    iget-object v4, p0, Lcom/mfluent/asp/dws/k;->d:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v3, v2, v4}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 191
    new-instance v2, Lcom/mfluent/asp/dws/l;

    iget-object v4, p0, Lcom/mfluent/asp/dws/k;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/mfluent/asp/dws/k;->e:Lorg/apache/http/protocol/HttpService;

    invoke-direct {v2, v4, v5, v3, v0}, Lcom/mfluent/asp/dws/l;-><init>(Landroid/content/Context;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;Lcom/mfluent/asp/datamodel/Device;)V

    .line 192
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 193
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_7
    .catch Ljava/io/InterruptedIOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2
.end method
