.class public final Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;,
        Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$a;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private b:I

.field private c:Landroid/content/ContentResolver;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method private a(Lorg/json/JSONObject;)Lorg/apache/http/HttpEntity;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 107
    :try_start_0
    const-string v0, "Command"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 112
    const/4 v1, 0x0

    .line 114
    const-string v3, "Info"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    new-instance v0, Lcom/mfluent/asp/dws/handlers/ai;

    invoke-direct {v0}, Lcom/mfluent/asp/dws/handlers/ai;-><init>()V

    :goto_0
    move v3, v4

    move-object v5, v1

    move v1, v2

    .line 126
    :goto_1
    if-eqz v3, :cond_2

    const/4 v3, 0x5

    if-ge v1, v3, :cond_2

    .line 129
    :try_start_1
    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->c:Landroid/content/ContentResolver;

    iget v6, p0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->b:I

    iget v7, p0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->d:I

    invoke-interface {v0, p1, v3, v6, v7}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$a;->a(Lorg/json/JSONObject;Landroid/content/ContentResolver;II)Lorg/json/JSONObject;
    :try_end_1
    .catch Landroid/database/StaleDataException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    move-object v5, v3

    move v3, v2

    .line 133
    goto :goto_1

    .line 109
    :catch_0
    move-exception v0

    new-instance v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v1, "The request is missing the \"Command\" field."

    invoke-direct {v0, v1}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    const-string v3, "Sync"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 120
    new-instance v0, Lcom/mfluent/asp/dws/handlers/as;

    invoke-direct {v0}, Lcom/mfluent/asp/dws/handlers/as;-><init>()V

    goto :goto_0

    .line 122
    :cond_1
    new-instance v1, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown command: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :catch_1
    move-exception v3

    .line 132
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    .line 133
    goto :goto_1

    .line 138
    :cond_2
    :try_start_2
    new-instance v0, Lcom/mfluent/asp/util/n;

    invoke-direct {v0, v5}, Lcom/mfluent/asp/util/n;-><init>(Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 139
    return-object v0

    .line 142
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trouble constructing the response entity"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 62
    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    new-instance v1, Lorg/apache/http/MethodNotSupportedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " method not supported"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :cond_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->c:Landroid/content/ContentResolver;

    .line 68
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->b:I

    .line 70
    const-string v0, "MFL_ATTR_DEVICE"

    invoke-interface {p3, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 71
    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->d:I

    .line 75
    :cond_1
    const/4 v0, 0x0

    .line 76
    instance-of v1, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v1, :cond_2

    .line 77
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 78
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 82
    invoke-direct {p0, v2}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->a(Lorg/json/JSONObject;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 83
    invoke-interface {p2, v2}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException; {:try_start_0 .. :try_end_0} :catch_1

    .line 92
    :cond_2
    :goto_0
    if-nez v0, :cond_4

    .line 93
    sget-object v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_3

    .line 94
    const-string v0, "mfl_MetadataSyncHandler"

    const-string v1, "Request processed successfully"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_3
    :goto_1
    return-void

    .line 85
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse request entity as JSON.\nRequest Entity: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :catch_1
    move-exception v0

    .line 88
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to process sync protocol request.\nError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nRequest Entity: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_4
    const-string v1, "mfl_MetadataSyncHandler"

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v1, v0}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 99
    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 100
    const/16 v0, 0x190

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_1
.end method
