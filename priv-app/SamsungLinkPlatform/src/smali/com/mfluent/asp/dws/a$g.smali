.class public abstract Lcom/mfluent/asp/dws/a$g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "g"
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private final d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mfluent/asp/dws/a$g;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/mfluent/asp/dws/a$g;->c:Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v0}, Lcom/mfluent/asp/dws/a$g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/dws/a$g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput-object p1, p0, Lcom/mfluent/asp/dws/a$g;->a:Ljava/lang/String;

    .line 218
    iput-object p2, p0, Lcom/mfluent/asp/dws/a$g;->b:Ljava/lang/String;

    .line 219
    iput-object p3, p0, Lcom/mfluent/asp/dws/a$g;->d:Ljava/lang/Object;

    .line 220
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;B)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1, p2, p3}, Lcom/mfluent/asp/dws/a$g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/mfluent/asp/dws/a$g;->d:Ljava/lang/Object;

    sget-object v1, Lcom/mfluent/asp/dws/a$g;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 245
    const/4 v0, 0x0

    .line 247
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/a$g;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method public abstract a(Landroid/database/Cursor;I)Ljava/lang/Object;
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/mfluent/asp/dws/a$g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    return-void
.end method

.method public abstract a(Lorg/json/JSONObject;Landroid/content/ContentValues;)V
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/mfluent/asp/dws/a$g;->d:Ljava/lang/Object;

    sget-object v1, Lcom/mfluent/asp/dws/a$g;->c:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
