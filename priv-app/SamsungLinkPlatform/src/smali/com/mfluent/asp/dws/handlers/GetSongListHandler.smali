.class public final Lcom/mfluent/asp/dws/handlers/GetSongListHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/GetSongListHandler$1;,
        Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;
    }
.end annotation


# instance fields
.field private final a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;->a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    .line 40
    return-void
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 48
    new-instance v7, Lcom/mfluent/asp/dws/d;

    invoke-direct {v7, p1}, Lcom/mfluent/asp/dws/d;-><init>(Lorg/apache/http/HttpRequest;)V

    .line 50
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    .line 60
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 65
    :goto_0
    sget-object v2, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$1;->a:[I

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;->a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-virtual {v3}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v6, v1

    move v5, v1

    move v4, v1

    .line 74
    :goto_1
    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    new-instance v0, Lcom/sec/pcw/service/b/l;

    invoke-direct {v0}, Lcom/sec/pcw/service/b/l;-><init>()V

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->a()I

    move-result v1

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->b()I

    move-result v2

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->c()I

    move-result v3

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/pcw/service/b/l;->a(IIILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_2
    new-instance v1, Lcom/mfluent/asp/util/n;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/util/n;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 94
    return-void

    :catch_0
    move-exception v0

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_0
    move v6, v1

    move v5, v1

    move v4, v0

    .line 68
    goto :goto_1

    :pswitch_1
    move v6, v1

    move v5, v0

    move v4, v1

    .line 71
    goto :goto_1

    :pswitch_2
    move v6, v0

    move v5, v1

    move v4, v1

    .line 73
    goto :goto_1

    .line 84
    :cond_1
    new-instance v0, Lcom/sec/pcw/service/b/j;

    invoke-direct {v0}, Lcom/sec/pcw/service/b/j;-><init>()V

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->a()I

    move-result v1

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->b()I

    move-result v2

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->c()I

    move-result v3

    invoke-virtual {v7}, Lcom/mfluent/asp/dws/d;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/pcw/service/b/j;->a(IIIIIILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
