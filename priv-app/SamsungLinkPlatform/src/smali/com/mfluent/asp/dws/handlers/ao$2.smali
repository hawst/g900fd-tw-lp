.class final Lcom/mfluent/asp/dws/handlers/ao$2;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/dws/handlers/ao;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/dws/handlers/ao;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/dws/handlers/ao;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ao$2;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 132
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->a()Lorg/slf4j/Logger;

    move-result-object v0

    invoke-interface {v0}, Lorg/slf4j/Logger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::SaveFilesHandler::onReceive: {}"

    invoke-static {p2}, Lcom/mfluent/asp/common/util/IntentHelper;->intentToString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 136
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    .line 138
    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139
    const-string v2, "isAutoUpload"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 140
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/mfluent/asp/dws/handlers/ao$2$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/mfluent/asp/dws/handlers/ao$2$1;-><init>(Lcom/mfluent/asp/dws/handlers/ao$2;ZLjava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 150
    :cond_1
    return-void
.end method
