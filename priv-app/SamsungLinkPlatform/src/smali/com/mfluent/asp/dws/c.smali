.class public Lcom/mfluent/asp/dws/c;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private b:Ljava/net/ServerSocket;

.field private c:Lorg/apache/http/params/HttpParams;

.field private d:Lorg/apache/http/protocol/HttpService;

.field private e:Z

.field private final f:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/mfluent/asp/dws/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/c;->e:Z

    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    .line 128
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/dws/c;->setDaemon(Z)V

    .line 129
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/c;->e:Z

    .line 133
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/c;->interrupt()V

    .line 134
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x1

    .line 139
    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/dws/c;->b:Ljava/net/ServerSocket;

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/mfluent/asp/dws/c;->e:Z

    if-nez v2, :cond_2

    .line 141
    :try_start_0
    new-instance v2, Lpcloud/net/d;

    iget-object v3, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v2, v3}, Lpcloud/net/d;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mfluent/asp/dws/c;->b:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v2

    .line 143
    sget-object v3, Lcom/mfluent/asp/dws/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v9, :cond_0

    .line 144
    const-string v3, "mfl_DeviceWebServer"

    const-string v4, "Trouble creating NtclServerSocket in DeviceWebServer - trying again in 10 seconds"

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 147
    :cond_0
    const-wide/16 v2, 0x2710

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 149
    :catch_1
    move-exception v2

    sget-object v2, Lcom/mfluent/asp/dws/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v9, :cond_1

    .line 150
    const-string v2, "mfl_DeviceWebServer"

    const-string v3, "Interrupted while waiting to attempt to create an NtclServerSocket"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_1
    :goto_1
    return-void

    .line 154
    :cond_2
    iget-boolean v2, p0, Lcom/mfluent/asp/dws/c;->e:Z

    if-nez v2, :cond_1

    .line 161
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v2, p0, Lcom/mfluent/asp/dws/c;->c:Lorg/apache/http/params/HttpParams;

    .line 162
    iget-object v2, p0, Lcom/mfluent/asp/dws/c;->c:Lorg/apache/http/params/HttpParams;

    const-string v3, "http.socket.timeout"

    const/16 v4, 0x1388

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.socket.buffer-size"

    const/16 v4, 0x2000

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.connection.stalecheck"

    invoke-interface {v2, v3, v10}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.tcp.nodelay"

    invoke-interface {v2, v3, v8}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.origin-server"

    const-string v4, "HttpComponents/1.1"

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 170
    new-instance v2, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 171
    new-instance v3, Lcom/mfluent/asp/dws/g;

    invoke-direct {v3}, Lcom/mfluent/asp/dws/g;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 172
    new-instance v3, Lorg/apache/http/protocol/ResponseDate;

    invoke-direct {v3}, Lorg/apache/http/protocol/ResponseDate;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 173
    new-instance v3, Lorg/apache/http/protocol/ResponseServer;

    invoke-direct {v3}, Lorg/apache/http/protocol/ResponseServer;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 174
    new-instance v3, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v3}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 175
    new-instance v3, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v3}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 176
    new-instance v3, Lcom/mfluent/asp/dws/i;

    invoke-direct {v3}, Lcom/mfluent/asp/dws/i;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 177
    new-instance v3, Lcom/mfluent/asp/dws/j;

    invoke-direct {v3}, Lcom/mfluent/asp/dws/j;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 180
    new-instance v3, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v3}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    .line 181
    const-string v4, "/v1/media/sync"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 182
    const-string v4, "/v1/media/download/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/j;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/j;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 183
    const-string v4, "/v1/media/notification/contentChanged"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/d;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/d;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 184
    const-string v4, "/v1/media/notification/deviceListChanged"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ar;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/ar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 185
    const-string v4, "/v1/video/thumbnail/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/aw;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/aw;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 186
    const-string v4, "/v1/images/thumbnail/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ah;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/ah;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 187
    const-string v4, "/v1/video/caption/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/av;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/av;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 188
    const-string v4, "/v1/audio/albumart/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/a;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/a;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 189
    const-string v4, "/api/pCloud/device/info"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/q;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/q;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 192
    const-string v4, "/api/pCloud/device/networkModeChanged"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/al;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/al;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 194
    const-string v4, "/api/pCloud/device/registration"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/aq;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/aq;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 195
    const-string v4, "/api/pCloud/device/uploadCameraRead"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/r;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/r;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 196
    const-string v4, "/api/pCloud/device/connection/keepAlive"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/aj;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/aj;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 198
    const-string v4, "/api/pCloud/device/media"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/aa;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/aa;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 199
    const-string v4, "/api/pCloud/device/media/list.meta"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/z;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/z;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 200
    const-string v4, "/api/pCloud/device/media/list.info"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/y;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/y;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 202
    const-string v4, "/api/pCloud/device/media/photo/images"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ac;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/ac;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 203
    const-string v4, "/api/pCloud/device/media/video/clips"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ag;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/ag;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 204
    const-string v4, "/api/pCloud/device/media/doc/files"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/s;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/s;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 206
    const-string v4, "/api/pCloud/device/media/music/songs"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;

    sget-object v6, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->d:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;-><init>(Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 207
    const-string v4, "/api/pCloud/device/media/music/albums"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/n;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/n;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 208
    const-string v4, "/api/pCloud/device/media/music/albums/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;

    sget-object v6, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->a:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;-><init>(Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 209
    const-string v4, "/api/pCloud/device/media/music/artists"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/p;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/p;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 210
    const-string v4, "/api/pCloud/device/media/music/artists/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;

    sget-object v6, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->b:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;-><init>(Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 211
    const-string v4, "/api/pCloud/device/media/music/genres"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/x;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/x;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 212
    const-string v4, "/api/pCloud/device/media/music/genres/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;

    sget-object v6, Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;->c:Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/GetSongListHandler;-><init>(Lcom/mfluent/asp/dws/handlers/GetSongListHandler$RequestType;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 214
    const-string v4, "/api/pCloud/device/media/photo/images/delete"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/h;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/h;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 215
    const-string v4, "/api/pCloud/device/media/music/songs/delete"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/g;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/g;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 216
    const-string v4, "/api/pCloud/device/media/video/clips/delete"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/i;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/i;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 217
    const-string v4, "/api/pCloud/device/media/video/clips/bookmark"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/af;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/af;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 218
    const-string v4, "/api/pCloud/device/media/video/clips/bookmark/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/au;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/au;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 219
    const-string v4, "/api/pCloud/device/media/doc/files/delete"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/e;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/e;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 221
    new-instance v4, Lcom/mfluent/asp/dws/handlers/ab;

    invoke-direct {v4}, Lcom/mfluent/asp/dws/handlers/ab;-><init>()V

    .line 222
    const-string v5, "/api/pCloud/device/media/photo/images/itemList"

    invoke-virtual {v3, v5, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 223
    const-string v5, "/api/pCloud/device/media/video/clips/itemList"

    invoke-virtual {v3, v5, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 224
    const-string v5, "/api/pCloud/device/media/music/songs/itemList"

    invoke-virtual {v3, v5, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 226
    const-string v4, "/api/pCloud/device/files"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/u;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/u;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 227
    const-string v4, "/api/pCloud/device/files.info"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/v;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/v;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 228
    const-string v4, "/api/pCloud/device/files/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/t;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/t;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 229
    const-string v4, "/api/pCloud/device/files/delete"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/f;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/f;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 230
    const-string v4, "/api/pCloud/device/files/list.meta"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/w;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/w;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 232
    const-string v4, "/api/pCloud/device/files/mkdir"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ak;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/ak;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 233
    const-string v4, "/api/pCloud/device/files/rmdir"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/an;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/an;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 234
    const-string v4, "/api/pCloud/device/files/rename"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/am;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/am;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 236
    const-string v4, "/api/pCloud/device/player/metaInfo"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ad;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/ad;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 238
    const-string v4, "/api/pCloud/transfer/save"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ao;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/ao;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 239
    const-string v4, "/api/pCloud/transfer/save/sessions/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/ae;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/ae;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 241
    const-string v4, "/api/pCloud/transfer/upload"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/at;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/mfluent/asp/dws/handlers/at;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 242
    new-instance v4, Lcom/mfluent/asp/dws/handlers/l;

    iget-object v5, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mfluent/asp/dws/handlers/l;-><init>(Landroid/content/Context;)V

    .line 243
    const-string v5, "/api/pCloud/transfer/upload/sessions/*"

    invoke-virtual {v3, v5, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 245
    const-string v5, "/api/pCloud/externalService/upload"

    new-instance v6, Lcom/mfluent/asp/dws/handlers/at;

    iget-object v7, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v6, v7, v8}, Lcom/mfluent/asp/dws/handlers/at;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v3, v5, v6}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 246
    const-string v5, "/api/pCloud/externalService/sessions/*"

    invoke-virtual {v3, v5, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 247
    const-string v4, "/api/pCloud/externalService/download/include"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/k;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v5, v10, v6}, Lcom/mfluent/asp/dws/handlers/k;-><init>(ZLandroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 248
    const-string v4, "/api/pCloud/externalService/download/exclude"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/k;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    invoke-direct {v5, v8, v6}, Lcom/mfluent/asp/dws/handlers/k;-><init>(ZLandroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 250
    const-string v4, "/api/pCloud/AllShare/DMR/*"

    new-instance v5, Lcom/mfluent/asp/dws/handlers/o;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/handlers/o;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 253
    new-instance v4, Lorg/apache/http/protocol/HttpService;

    new-instance v5, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v5}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v6, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v6}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v4, v2, v5, v6}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v4, p0, Lcom/mfluent/asp/dws/c;->d:Lorg/apache/http/protocol/HttpService;

    .line 254
    iget-object v2, p0, Lcom/mfluent/asp/dws/c;->d:Lorg/apache/http/protocol/HttpService;

    iget-object v4, p0, Lcom/mfluent/asp/dws/c;->c:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v2, v4}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 255
    iget-object v2, p0, Lcom/mfluent/asp/dws/c;->d:Lorg/apache/http/protocol/HttpService;

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 257
    const-string v2, "mfl_DeviceWebServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Listening on port "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/dws/c;->b:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/c;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/mfluent/asp/dws/c;->e:Z

    if-nez v2, :cond_1

    .line 261
    :try_start_2
    iget-object v2, p0, Lcom/mfluent/asp/dws/c;->b:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    .line 262
    if-nez v2, :cond_4

    .line 263
    sget-object v2, Lcom/mfluent/asp/dws/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v9, :cond_3

    .line 264
    const-string v2, "mfl_DeviceWebServer"

    const-string v3, "::run: Incoming connection, but ignoring, because returned socket is null"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 300
    :catch_2
    move-exception v2

    goto/16 :goto_1

    .line 267
    :cond_4
    new-instance v4, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 268
    sget-object v3, Lcom/mfluent/asp/dws/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v9, :cond_5

    .line 269
    const-string v5, "mfl_DeviceWebServer"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v3, "::run: Incoming connection"

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    instance-of v3, v2, Lpcloud/net/e;

    if-eqz v3, :cond_7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v3, " from Peer Id: "

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Lpcloud/net/e;

    move-object v3, v0

    invoke-virtual {v3}, Lpcloud/net/e;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_5
    sget-object v3, Lcom/mfluent/asp/dws/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v5, 0x2

    if-gt v3, v5, :cond_6

    .line 275
    const-string v5, "mfl_DeviceWebServer"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v3, "::run: Incoming connection"

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    instance-of v3, v2, Lpcloud/net/e;

    if-eqz v3, :cond_8

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v3, " from Peer Id: "

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Lpcloud/net/e;

    move-object v3, v0

    invoke-virtual {v3}, Lpcloud/net/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_6
    iget-object v3, p0, Lcom/mfluent/asp/dws/c;->c:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v4, v2, v3}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 281
    const/4 v3, 0x0

    .line 283
    instance-of v5, v2, Lpcloud/net/e;

    if-eqz v5, :cond_9

    .line 284
    check-cast v2, Lpcloud/net/e;

    .line 285
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    invoke-virtual {v2}, Lpcloud/net/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/mfluent/asp/datamodel/t;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 289
    :goto_5
    new-instance v3, Lcom/mfluent/asp/dws/l;

    iget-object v5, p0, Lcom/mfluent/asp/dws/c;->f:Landroid/content/Context;

    iget-object v6, p0, Lcom/mfluent/asp/dws/c;->d:Lorg/apache/http/protocol/HttpService;

    invoke-direct {v3, v5, v6, v4, v2}, Lcom/mfluent/asp/dws/l;-><init>(Landroid/content/Context;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;Lcom/mfluent/asp/datamodel/Device;)V

    .line 290
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 291
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_2

    .line 301
    :catch_3
    move-exception v2

    .line 302
    const-string v3, "mfl_DeviceWebServer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "I/O error initialising connection thread: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 269
    :cond_7
    :try_start_3
    const-string v3, ""

    goto/16 :goto_3

    .line 275
    :cond_8
    const-string v3, ""
    :try_end_3
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_4

    :cond_9
    move-object v2, v3

    goto :goto_5
.end method
