.class final Lcom/mfluent/asp/dws/handlers/at$a;
.super Landroid/database/CursorWrapper;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/dws/handlers/at;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/dws/handlers/at;Landroid/database/Cursor;I)V
    .locals 0

    .prologue
    .line 743
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/at$a;->a:Lcom/mfluent/asp/dws/handlers/at;

    .line 744
    invoke-direct {p0, p2}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 746
    iput p3, p0, Lcom/mfluent/asp/dws/handlers/at$a;->b:I

    .line 747
    return-void
.end method


# virtual methods
.method public final getColumnCount()I
    .locals 1

    .prologue
    .line 751
    invoke-super {p0}, Landroid/database/CursorWrapper;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 756
    const-string v0, "media_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/at$a;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 760
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final getInt(I)I
    .locals 1

    .prologue
    .line 765
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/at$a;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 766
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/at$a;->b:I

    .line 769
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getInt(I)I

    move-result v0

    goto :goto_0
.end method
