.class final Lcom/mfluent/asp/dws/handlers/ao$a;
.super Ljava/io/InputStream;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/ao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/dws/handlers/ao;

.field private b:I

.field private final c:[Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/dws/handlers/ao;[Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 713
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 714
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    .line 715
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    .line 716
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 741
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    iget v1, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 734
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 735
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 734
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 737
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 720
    .line 721
    :goto_0
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 722
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->c:[Ljava/io/InputStream;

    iget v2, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 723
    if-ne v0, v1, :cond_1

    .line 725
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ao$a;->b:I

    goto :goto_0

    :cond_0
    move v0, v1

    .line 729
    :cond_1
    return v0
.end method
