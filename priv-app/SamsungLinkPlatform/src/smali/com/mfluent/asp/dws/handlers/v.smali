.class public Lcom/mfluent/asp/dws/handlers/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/v;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Ljava/util/List;)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 130
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 132
    const-string v0, "count"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 134
    const-wide/16 v0, 0x0

    .line 135
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 136
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 137
    goto :goto_0

    .line 139
    :cond_0
    const-string v0, "size"

    invoke-virtual {v4, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 141
    return-object v4
.end method

.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x190

    const/4 v8, 0x3

    const/4 v1, 0x0

    .line 51
    instance-of v0, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-nez v0, :cond_1

    .line 52
    sget-object v0, Lcom/mfluent/asp/dws/handlers/v;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_0

    .line 53
    const-string v0, "mfl_GetFilesInfoHandler"

    const-string v1, "::handle:Expected some payload in the request but got none"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_0
    const-string v0, "No payload in the request"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 56
    invoke-interface {p2, v9}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 121
    :goto_0
    return-void

    .line 60
    :cond_1
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 61
    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 68
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 70
    const-string v0, "path"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 71
    const-string v0, "status"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 72
    const-string v0, "search"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 73
    const-string v0, "items"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    move v0, v1

    .line 74
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v0, v7, :cond_3

    .line 75
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    sget-object v1, Lcom/mfluent/asp/dws/handlers/v;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_2

    .line 79
    const-string v1, "mfl_GetFilesInfoHandler"

    const-string v2, "::handle:Trouble parsing request body"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 81
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 82
    invoke-interface {p2, v9}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0

    .line 86
    :cond_3
    new-instance v0, Ljava/lang/String;

    const/16 v3, 0xa

    invoke-static {v4, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 87
    new-instance v3, Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-direct {v3}, Lcom/mfluent/asp/datamodel/filebrowser/i;-><init>()V

    .line 89
    :try_start_1
    new-instance v4, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v7}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-virtual {v3, v4, v0, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 95
    const-string v0, "EXCLUDE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 97
    :goto_2
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 99
    :goto_3
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/filebrowser/i;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_7

    .line 100
    invoke-virtual {v3, v1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v5

    .line 101
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_4

    .line 102
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 106
    if-eqz v0, :cond_4

    .line 107
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_4
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 90
    :catch_1
    move-exception v0

    .line 91
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 92
    new-instance v1, Lorg/apache/http/HttpException;

    const-string v2, "Interrupted"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_5
    move v0, v1

    .line 95
    goto :goto_2

    .line 110
    :cond_6
    if-nez v0, :cond_4

    .line 111
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 117
    :cond_7
    :try_start_2
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/dws/handlers/v;->a(Ljava/util/List;)Lorg/json/JSONObject;

    move-result-object v0

    .line 118
    new-instance v1, Lcom/mfluent/asp/util/n;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/util/n;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 119
    :catch_2
    move-exception v0

    .line 120
    new-instance v1, Lorg/apache/http/HttpException;

    const-string v2, "Trouble building json response"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
