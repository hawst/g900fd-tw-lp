.class final Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:[B

.field private final b:I

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    new-instance v0, Lcom/mfluent/asp/util/b/a;

    invoke-direct {v0}, Lcom/mfluent/asp/util/b/a;-><init>()V

    .line 127
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 129
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 131
    invoke-virtual {v0}, Lcom/mfluent/asp/util/b/a;->a()[B

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->a:[B

    .line 132
    invoke-virtual {v0}, Lcom/mfluent/asp/util/b/a;->size()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->b:I

    .line 133
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->c:Ljava/lang/String;

    .line 134
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    const-string v0, "image/jpeg"

    return-object v0
.end method

.method public final b()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->b:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final c()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->a:[B

    const/4 v2, 0x0

    iget v3, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->b:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public final f()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 164
    const/4 v0, 0x0

    return-object v0
.end method
