.class public final Lcom/mfluent/asp/dws/handlers/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ae;->a:Landroid/content/Context;

    .line 36
    return-void
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 48
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ae;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v2

    .line 50
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v3

    .line 51
    const-string v4, "GET"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 53
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    invoke-interface {v2, v1}, Lcom/mfluent/asp/filetransfer/d;->c(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    invoke-interface {v2, v1}, Lcom/mfluent/asp/filetransfer/d;->d(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v0

    .line 62
    :cond_0
    if-nez v0, :cond_2

    .line 63
    const/16 v0, 0x194

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 81
    :cond_1
    :goto_0
    return-void

    .line 67
    :cond_2
    new-instance v1, Lcom/mfluent/asp/dws/handlers/m;

    invoke-direct {v1}, Lcom/mfluent/asp/dws/handlers/m;-><init>()V

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/m;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lorg/json/JSONObject;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/mfluent/asp/util/n;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/util/n;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 70
    :cond_3
    const-string v0, "DELETE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 72
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    const/4 v0, 0x0

    invoke-interface {v2, v1, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 77
    :cond_4
    const/16 v0, 0x1f5

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_0
.end method
