.class final Lcom/mfluent/asp/dws/handlers/ao$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/dws/handlers/ao;->handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lorg/apache/james/mime4j/parser/MimeStreamParser;

.field final synthetic b:Ljava/io/InputStream;

.field final synthetic c:Lcom/mfluent/asp/dws/handlers/ao$b;

.field final synthetic d:Lcom/mfluent/asp/dws/handlers/ap;

.field final synthetic e:Lcom/mfluent/asp/dws/handlers/ao;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/dws/handlers/ao;Lorg/apache/james/mime4j/parser/MimeStreamParser;Ljava/io/InputStream;Lcom/mfluent/asp/dws/handlers/ao$b;Lcom/mfluent/asp/dws/handlers/ap;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->e:Lcom/mfluent/asp/dws/handlers/ao;

    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->a:Lorg/apache/james/mime4j/parser/MimeStreamParser;

    iput-object p3, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->b:Ljava/io/InputStream;

    iput-object p4, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    iput-object p5, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->d:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->a:Lorg/apache/james/mime4j/parser/MimeStreamParser;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->b:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Lorg/apache/james/mime4j/parser/MimeStreamParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ao$b;->c()V

    .line 289
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    .line 292
    const/4 v0, 0x0

    return-object v0

    .line 280
    :catch_0
    move-exception v0

    .line 282
    :try_start_1
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    invoke-static {v1}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/dws/handlers/ap;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    if-eq v1, v2, :cond_0

    .line 283
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    invoke-virtual {v1}, Lcom/mfluent/asp/dws/handlers/ao$b;->b()V

    .line 284
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->d:Lcom/mfluent/asp/dws/handlers/ap;

    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 286
    :cond_0
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    invoke-virtual {v1}, Lcom/mfluent/asp/dws/handlers/ao$b;->c()V

    .line 289
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$3;->c:Lcom/mfluent/asp/dws/handlers/ao$b;

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    throw v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/ao$3;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
