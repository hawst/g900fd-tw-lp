.class public final Lcom/mfluent/asp/dws/a/a;
.super Lcom/mfluent/asp/dws/handlers/b;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 54
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 55
    if-nez v0, :cond_1

    .line 56
    const/16 v0, 0x194

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 58
    const-string v0, "These are not the droids you are looking for."

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 59
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 60
    const-string v0, "mfl_CloudStreamHandler"

    const-string v1, "::handle:ERROR: received an empty request."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    const-string v1, "&amp;"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 66
    sget-object v1, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_2

    .line 67
    const-string v1, "mfl_CloudStreamHandler"

    const-string v2, "::handle:Replacing &amp; to work around a DLNA issue."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_2
    const-string v1, "&amp;"

    const-string v2, "&"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 73
    :cond_3
    sget-object v1, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_4

    .line 74
    const-string v1, "mfl_CloudStreamHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::handle:Request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_4
    const-string v1, "Content-Range"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 78
    if-nez v1, :cond_1e

    .line 79
    const-string v1, "Range"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    move-object v2, v1

    .line 82
    :goto_1
    const/4 v1, 0x0

    .line 83
    if-eqz v2, :cond_5

    .line 84
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 87
    :cond_5
    sget-object v2, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_6

    .line 88
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v3

    .line 89
    array-length v6, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_6

    aget-object v7, v3, v2

    .line 90
    const-string v8, "mfl_CloudStreamHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "::handle:RX Header >> "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 94
    :cond_6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 97
    :try_start_0
    const-string v0, "deviceId"

    invoke-virtual {v6, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 100
    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v2

    .line 106
    :try_start_2
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 107
    if-nez v0, :cond_7

    .line 108
    const/16 v0, 0x190

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 240
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    :catch_0
    move-exception v0

    const/16 v0, 0x190

    :try_start_3
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 240
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 112
    :cond_7
    :try_start_4
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v7

    .line 113
    if-nez v7, :cond_8

    .line 114
    const/16 v0, 0x1f4

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 115
    const-string v0, "Internal Server Error"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 240
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    :cond_8
    :try_start_5
    const-string v0, "sourceUrl"

    invoke-virtual {v6, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 122
    const/16 v2, 0x2b

    const/16 v3, 0x20

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 125
    :goto_3
    const-string v0, "sourceId"

    invoke-virtual {v6, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 128
    const/16 v3, 0x2b

    const/16 v8, 0x20

    invoke-virtual {v0, v3, v8}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 131
    :goto_4
    const-string v0, "mediaType"

    invoke-virtual {v6, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 132
    sget-object v6, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    const/4 v8, 0x4

    if-gt v6, v8, :cond_9

    .line 133
    const-string v6, "mfl_CloudStreamHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "::handle:Requesting: id="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; url="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_9
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 138
    sget-object v6, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    const/4 v8, 0x3

    if-gt v6, v8, :cond_a

    .line 139
    const-string v6, "mfl_CloudStreamHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "::handle:Content Range Request: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_a
    invoke-interface {v7, v0, v2, v3, v1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;

    move-result-object v0

    move-object v2, v0

    .line 149
    :goto_5
    if-nez v2, :cond_d

    .line 150
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_b

    .line 151
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:No stream returned by plugin. ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 240
    :cond_b
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 145
    :cond_c
    :try_start_6
    invoke-interface {v7, v0, v2, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getFile(ILjava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;

    move-result-object v0

    move-object v2, v0

    goto :goto_5

    .line 156
    :cond_d
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 158
    if-nez v0, :cond_f

    .line 159
    const/16 v0, 0x194

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 160
    const-string v0, "These are not the droids you are looking for.."

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 161
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_e

    .line 162
    const-string v0, "mfl_CloudStreamHandler"

    const-string v1, "::handle:ERROR: did not find a media stream"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 240
    :cond_e
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 167
    :cond_f
    :try_start_7
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getContentType()Ljava/lang/String;

    move-result-object v3

    .line 168
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getContentLength()J

    move-result-wide v8

    .line 169
    sget-object v6, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    const/4 v10, 0x3

    if-gt v6, v10, :cond_10

    .line 170
    const-string v6, "mfl_CloudStreamHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "::handle:contentType: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v6, "mfl_CloudStreamHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "::handle:contentLength: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_10
    new-instance v6, Lcom/mfluent/asp/util/m;

    invoke-direct {v6, v0, v8, v9}, Lcom/mfluent/asp/util/m;-><init>(Ljava/io/InputStream;J)V

    .line 175
    invoke-interface {p2, v6}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 178
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getExtraHeaders()Ljava/util/List;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_12

    .line 180
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 181
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->addHeader(Lorg/apache/http/Header;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_6

    .line 231
    :catch_1
    move-exception v0

    .line 232
    :try_start_8
    sget-object v1, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_11

    .line 233
    const-string v1, "mfl_CloudStreamHandler"

    const-string v2, "Trouble with Cloud request"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235
    :cond_11
    const/16 v1, 0x1f4

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 236
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 237
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 238
    const-string v0, "Content-Type"

    const-string v1, "text/html"

    invoke-interface {p2, v0, v1}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 240
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 184
    :cond_12
    :try_start_9
    const-string v0, "Content-Length"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 185
    if-nez v0, :cond_13

    .line 186
    const-string v0, "Content-Length"

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v0, v6}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_13
    invoke-static {p1, p2, v3}, Lcom/mfluent/asp/dws/a/a;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Ljava/lang/String;)V

    .line 193
    const-string v0, "Content-Type"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 194
    if-nez v0, :cond_14

    .line 195
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 196
    const-string v0, "Content-Type"

    invoke-interface {p2, v0, v3}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_14
    const-string v0, "audio/mp3"

    invoke-static {v0, v3}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 201
    const-string v0, "Content-Type"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->removeHeaders(Ljava/lang/String;)V

    .line 202
    const-string v0, "Content-Type"

    const-string v3, "audio/mpeg"

    invoke-interface {p2, v0, v3}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_15
    const-string v0, "Accept-Ranges"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 206
    if-nez v0, :cond_16

    .line 207
    invoke-interface {v7}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->isRangeDownloadSupported()Z

    move-result v0

    if-nez v0, :cond_16

    .line 208
    const-string v0, "Accept-Ranges"

    const-string v3, "none"

    invoke-interface {p2, v0, v3}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_16
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getResponseCode()I

    move-result v0

    .line 213
    if-eqz v0, :cond_19

    .line 214
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 215
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 220
    :cond_17
    :goto_7
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_18

    .line 221
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:sent response; status code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_18
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1b

    .line 225
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v1

    .line 226
    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_1b

    aget-object v3, v1, v0

    .line 227
    const-string v6, "mfl_CloudStreamHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::handle:TX Header >> "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 216
    :cond_19
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v7}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->isRangeDownloadSupported()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 217
    const/16 v0, 0xce

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_7

    .line 240
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_1a

    .line 241
    const-string v1, "mfl_CloudStreamHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::handle:Cloud media response took "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    throw v0

    .line 240
    :cond_1b
    sget-object v0, Lcom/mfluent/asp/dws/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 241
    const-string v0, "mfl_CloudStreamHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::handle:Cloud media response took "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1c
    move-object v3, v0

    goto/16 :goto_4

    :cond_1d
    move-object v2, v0

    goto/16 :goto_3

    :cond_1e
    move-object v2, v1

    goto/16 :goto_1
.end method
