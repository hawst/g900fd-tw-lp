.class final Lcom/mfluent/asp/dws/handlers/j$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/dws/handlers/BaseDownloadHandler$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/mfluent/asp/media/videotranscoder/c;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private final e:[Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/media/videotranscoder/c;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    iput-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->d:Z

    .line 241
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "duration"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->e:[Ljava/lang/String;

    .line 242
    const-string v0, "_data=?"

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->f:Ljava/lang/String;

    .line 245
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/j$a;->a:Lcom/mfluent/asp/media/videotranscoder/c;

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Transcoded file: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->b:Ljava/lang/String;

    .line 247
    invoke-direct {p0, p2}, Lcom/mfluent/asp/dws/handlers/j$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->c:Ljava/lang/String;

    .line 248
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 292
    if-nez p1, :cond_1

    .line 293
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/j;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_0

    .line 294
    const-string v0, "mfl_DownloadHandler"

    const-string v1, "::getDurationForPath: Null sourcePath. Returning null duration"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v6

    .line 333
    :goto_0
    return-object v0

    .line 302
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 303
    new-array v4, v7, [Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 304
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 307
    :try_start_0
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/j$a;->e:[Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "_data=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 309
    if-nez v1, :cond_2

    .line 310
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_0

    .line 316
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 317
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v7, :cond_3

    .line 318
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/j;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_3

    .line 319
    const-string v0, "mfl_DownloadHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getDurationForPath: Found more than 1 file for path, returning first duration. Path="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 330
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 326
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/j;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x5

    if-gt v0, v2, :cond_5

    .line 327
    const-string v0, "mfl_DownloadHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getDurationForPath: Returning null duration. Couldn\'t find file for path="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 330
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    .line 333
    goto/16 :goto_0

    .line 330
    :catchall_0
    move-exception v0

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    const-string v0, "video/mp2t"

    return-object v0
.end method

.method public final b()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->a:Lcom/mfluent/asp/media/videotranscoder/c;

    invoke-virtual {v0}, Lcom/mfluent/asp/media/videotranscoder/c;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->d:Z

    .line 253
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->a:Lcom/mfluent/asp/media/videotranscoder/c;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->a:Lcom/mfluent/asp/media/videotranscoder/c;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 289
    :cond_0
    return-void
.end method

.method public final f()Ljava/lang/Iterable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/j$a;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 276
    const/4 v0, 0x0

    .line 281
    :goto_0
    return-object v0

    .line 279
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 280
    new-instance v1, Landroid/util/Pair;

    const-string v2, "X-ASP-DURATION-TIME"

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/j$a;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
