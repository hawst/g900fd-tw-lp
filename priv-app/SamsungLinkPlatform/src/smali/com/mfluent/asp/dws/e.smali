.class public Lcom/mfluent/asp/dws/e;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static c:I


# instance fields
.field private final d:Landroid/content/Context;

.field private e:Ljava/net/ServerSocket;

.field private f:I

.field private g:Lorg/apache/http/params/HttpParams;

.field private h:Lorg/apache/http/protocol/HttpService;

.field private i:Z

.field private volatile j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mfl_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIAPLAYER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 77
    const/4 v0, 0x0

    sput v0, Lcom/mfluent/asp/dws/e;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    const-class v0, Lcom/mfluent/asp/dws/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 87
    iput-boolean v1, p0, Lcom/mfluent/asp/dws/e;->i:Z

    .line 88
    iput-boolean v1, p0, Lcom/mfluent/asp/dws/e;->j:Z

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/e;->d:Landroid/content/Context;

    .line 100
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/dws/e;->setDaemon(Z)V

    .line 101
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 343
    if-nez p0, :cond_0

    .line 344
    const-string p0, ""

    .line 346
    :cond_0
    const-string v0, "UTF-8"

    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 118
    :try_start_0
    new-instance v0, Ljava/net/ServerSocket;

    sget v1, Lcom/mfluent/asp/dws/e;->c:I

    const/4 v2, 0x0

    const-string v3, "127.0.0.1"

    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v0, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    .line 119
    iget-object v0, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/dws/e;->f:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    sget-object v1, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_0

    .line 123
    sget-object v1, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v2, "::MediaStreamingProxyServer:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    :cond_0
    sput v4, Lcom/mfluent/asp/dws/e;->c:I

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://127.0.0.1:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mfluent/asp/dws/e;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    .line 294
    if-nez p5, :cond_1

    .line 296
    :try_start_0
    const-string v0, "mediastream?%s=%s&%s=%s&%s=%s&%s=%s"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "deviceId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/mfluent/asp/dws/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "mediaType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "sourceId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p4}, Lcom/mfluent/asp/dws/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "sourceUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    invoke-static {p3}, Lcom/mfluent/asp/dws/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 327
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/mfluent/asp/dws/e;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "cloudstream/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 329
    :try_start_1
    invoke-static {v0}, Lcom/mfluent/asp/dws/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330
    sget-object v1, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_0

    .line 331
    sget-object v1, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::buildCloudStreamUrl: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2

    .line 339
    :cond_0
    :goto_1
    return-object v0

    .line 304
    :cond_1
    :try_start_2
    const-string v0, "mediastream.%s?%s=%s&%s=%s&%s=%s&%s=%s"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p5, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "deviceId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p1}, Lcom/mfluent/asp/dws/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "mediaType"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "sourceId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static {p4}, Lcom/mfluent/asp/dws/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "sourceUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    invoke-static {p3}, Lcom/mfluent/asp/dws/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 313
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 encoding isn\'t supported!?!?!?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :catch_1
    move-exception v0

    .line 315
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not encode: deviceId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mediaType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sourceUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sourceId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " because: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 335
    :catch_2
    move-exception v1

    sget-object v1, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_0

    .line 336
    sget-object v1, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v2, "::buildCloudStreamUrl:Did not expect URL to be malformed"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 231
    invoke-static {p1}, Lcom/sec/pcw/service/d/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-static {p2}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/service/d/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/mfluent/asp/dws/e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ntsstream/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    invoke-static {p3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 246
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/mfluent/asp/dws/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    sget-object v1, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    .line 248
    sget-object v1, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::buildNtsStreamUrl:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_1
    :goto_0
    return-object v0

    .line 251
    :catch_0
    move-exception v1

    sget-object v1, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 252
    sget-object v1, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v2, "::buildNtsStreamUrl:Did not expect URL to be malformed"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/mfluent/asp/dws/e;->j:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/e;->i:Z

    .line 109
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/e;->interrupt()V

    .line 110
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 132
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    sget-object v2, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v3, "Starting MediaStreamingProxyServer..."

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/dws/e;->c()V

    .line 140
    :try_start_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v2, p0, Lcom/mfluent/asp/dws/e;->g:Lorg/apache/http/params/HttpParams;

    .line 141
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->g:Lorg/apache/http/params/HttpParams;

    const-string v3, "http.socket.timeout"

    const v4, 0xea60

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.socket.buffer-size"

    const/16 v4, 0x2000

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.connection.stalecheck"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.tcp.nodelay"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.origin-server"

    const-string v4, "HttpComponents/1.1"

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 149
    new-instance v2, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 150
    new-instance v3, Lcom/mfluent/asp/dws/g;

    invoke-direct {v3}, Lcom/mfluent/asp/dws/g;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 151
    new-instance v3, Lcom/mfluent/asp/dws/h;

    invoke-direct {v3}, Lcom/mfluent/asp/dws/h;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 155
    new-instance v3, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v3}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 156
    new-instance v3, Lcom/mfluent/asp/dws/j;

    invoke-direct {v3}, Lcom/mfluent/asp/dws/j;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 159
    new-instance v3, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v3}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    .line 160
    const-string v4, "/ntsstream/*"

    new-instance v5, Lcom/mfluent/asp/dws/a/b;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/a/b;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 161
    const-string v4, "/cloudstream/*"

    new-instance v5, Lcom/mfluent/asp/dws/a/a;

    invoke-direct {v5}, Lcom/mfluent/asp/dws/a/a;-><init>()V

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 164
    new-instance v4, Lorg/apache/http/protocol/HttpService;

    new-instance v5, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v5}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v6, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v6}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v4, v2, v5, v6}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v4, p0, Lcom/mfluent/asp/dws/e;->h:Lorg/apache/http/protocol/HttpService;

    .line 165
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->h:Lorg/apache/http/protocol/HttpService;

    iget-object v4, p0, Lcom/mfluent/asp/dws/e;->g:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v2, v4}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 166
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->h:Lorg/apache/http/protocol/HttpService;

    invoke-virtual {v2, v3}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 168
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_1

    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    if-eqz v2, :cond_1

    .line 169
    sget-object v2, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::run: Listening on "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/mfluent/asp/dws/e;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/e;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/mfluent/asp/dws/e;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    .line 175
    :try_start_1
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_5

    .line 177
    const-wide/16 v2, 0x1388

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 180
    :goto_1
    :try_start_3
    invoke-direct {p0}, Lcom/mfluent/asp/dws/e;->c()V
    :try_end_3
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v2

    .line 213
    :cond_2
    :goto_2
    iput-boolean v9, p0, Lcom/mfluent/asp/dws/e;->j:Z

    .line 218
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v10, :cond_3

    .line 219
    sget-object v2, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v3, "::run MediaStreamingProxyServer is no longer accepting connections"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_3
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/net/ServerSocket;)V

    .line 223
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 224
    sget-object v2, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v3, "Ending MediaStreamingProxyServer..."

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_4
    return-void

    .line 185
    :cond_5
    const/4 v2, 0x1

    :try_start_4
    iput-boolean v2, p0, Lcom/mfluent/asp/dws/e;->j:Z

    .line 186
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v10, :cond_6

    .line 187
    sget-object v2, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v3, "::run MediaStreamingProxyServer is accepting connections"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_6
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 190
    if-nez v3, :cond_8

    .line 191
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v8, :cond_1

    .line 192
    sget-object v2, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v3, "::run: Incoming connection, but ignoring, because returned socket is null"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 209
    :catch_1
    move-exception v2

    .line 210
    :try_start_5
    sget-object v3, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_2

    .line 211
    sget-object v3, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::run: I/O error initialising connection thread: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 217
    :catchall_0
    move-exception v2

    iput-boolean v9, p0, Lcom/mfluent/asp/dws/e;->j:Z

    .line 218
    sget-object v3, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v10, :cond_7

    .line 219
    sget-object v3, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    const-string v4, "::run MediaStreamingProxyServer is no longer accepting connections"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_7
    iget-object v3, p0, Lcom/mfluent/asp/dws/e;->e:Ljava/net/ServerSocket;

    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/net/ServerSocket;)V

    throw v2

    .line 195
    :cond_8
    :try_start_6
    new-instance v4, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 196
    sget-object v2, Lcom/mfluent/asp/dws/e;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v8, :cond_9

    .line 197
    sget-object v5, Lcom/mfluent/asp/dws/e;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v2, "::run: Incoming connection"

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    instance-of v2, v3, Lpcloud/net/e;

    if-eqz v2, :cond_a

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v2, " from Peer Id: "

    invoke-direct {v7, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, v3

    check-cast v0, Lpcloud/net/e;

    move-object v2, v0

    invoke-virtual {v2}, Lpcloud/net/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_9
    iget-object v2, p0, Lcom/mfluent/asp/dws/e;->g:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v4, v3, v2}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 203
    new-instance v2, Lcom/mfluent/asp/dws/l;

    iget-object v3, p0, Lcom/mfluent/asp/dws/e;->d:Landroid/content/Context;

    iget-object v5, p0, Lcom/mfluent/asp/dws/e;->h:Lorg/apache/http/protocol/HttpService;

    invoke-direct {v2, v3, v5, v4}, Lcom/mfluent/asp/dws/l;-><init>(Landroid/content/Context;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;)V

    .line 204
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 205
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 197
    :cond_a
    const-string v2, ""
    :try_end_6
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catch_2
    move-exception v2

    goto/16 :goto_1
.end method
