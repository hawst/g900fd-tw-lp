.class final Lcom/mfluent/asp/dws/handlers/ao$b;
.super Lorg/apache/james/mime4j/parser/AbstractContentHandler;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/ao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/dws/handlers/ao;

.field private final b:Ljava/io/File;

.field private c:Ljava/lang/String;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:Z

.field private final h:Landroid/media/MediaScannerConnection;

.field private final i:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/locks/Lock;

.field private final k:Lcom/mfluent/asp/dws/handlers/ap;

.field private l:J

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/dws/handlers/ao;Ljava/io/File;Lcom/mfluent/asp/dws/handlers/ap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 367
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-direct {p0}, Lorg/apache/james/mime4j/parser/AbstractContentHandler;-><init>()V

    .line 343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->d:Ljava/util/List;

    .line 353
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->i:Ljava/util/Queue;

    .line 355
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    .line 359
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    .line 361
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    .line 363
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    .line 364
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    .line 365
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->p:Ljava/lang/Object;

    .line 368
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->b:Ljava/io/File;

    .line 369
    iput-object p3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    .line 371
    new-instance v1, Landroid/media/MediaScannerConnection;

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->h:Landroid/media/MediaScannerConnection;

    .line 372
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->h:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->connect()V

    .line 373
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    .line 374
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->p:Ljava/lang/Object;

    .line 375
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/dws/handlers/ao$b;)Lcom/mfluent/asp/dws/handlers/ap;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/dws/handlers/ao$b;)Z
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/dws/handlers/ao$b;)V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/ao$b;->d()V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 590
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 592
    iget-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->g:Z

    if-eqz v0, :cond_0

    .line 593
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toContentValues(Landroid/content/ContentValues;)V

    .line 598
    :goto_0
    const-string v0, "uuid"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const-string v0, "endTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 600
    const-string v0, "sourceDeviceId"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->G()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 601
    const-string v0, "targetDeviceId"

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 602
    const-string v0, "transferSize"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->D()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 603
    const-string v0, "firstFileName"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    const-string v0, "numFiles"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->C()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 605
    const-string v0, "transferarchive"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 606
    const-string v0, "currentTransferSize"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 608
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 609
    return-void

    .line 595
    :cond_0
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toContentValues(Landroid/content/ContentValues;)V

    goto/16 :goto_0
.end method

.method private e()I
    .locals 4

    .prologue
    .line 622
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->D()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 623
    const/4 v0, 0x0

    .line 627
    :goto_0
    return v0

    .line 624
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->E()J

    move-result-wide v0

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->D()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 625
    const/16 v0, 0x64

    goto :goto_0

    .line 627
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->E()J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->D()J

    move-result-wide v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)Lcom/mfluent/asp/filetransfer/b;
    .locals 14

    .prologue
    .line 631
    new-instance v1, Lcom/mfluent/asp/filetransfer/b;

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->C()I

    move-result v3

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->D()J

    move-result-wide v4

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->E()J

    move-result-wide v6

    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/ao$b;->e()I

    move-result v8

    iget-boolean v9, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->e:Z

    iget-boolean v10, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->f:Z

    iget-boolean v11, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->g:Z

    iget-object v12, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->d:Ljava/util/List;

    move v13, p1

    invoke-direct/range {v1 .. v13}, Lcom/mfluent/asp/filetransfer/b;-><init>(Ljava/lang/String;IJJIZZZLjava/util/List;Z)V

    .line 649
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/ao;->a(Lcom/mfluent/asp/dws/handlers/ao;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/filetransfer/b;)V

    .line 651
    return-object v1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    .line 379
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->f:Z

    .line 383
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 384
    return-void
.end method

.method public final body(Lorg/apache/james/mime4j/descriptor/BodyDescriptor;Ljava/io/InputStream;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x1

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 406
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    if-ne v0, v8, :cond_0

    .line 407
    iput v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    .line 410
    :cond_0
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_4

    .line 412
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->r()I

    move-result v0

    iget v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    if-eq v0, v1, :cond_1

    .line 413
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/ao;->a(Lcom/mfluent/asp/dws/handlers/ao;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V

    .line 414
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "attempting to resume transfer for fileIndex "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but saved session fileIndex was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->r()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 422
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/ao;->a(Lcom/mfluent/asp/dws/handlers/ao;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V

    .line 423
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "attempting to resume transfer for fileIndex "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but saved session tmpFile does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 429
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v1}, Lcom/mfluent/asp/dws/handlers/ao;->a(Lcom/mfluent/asp/dws/handlers/ao;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-interface {v1, v2}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V

    .line 430
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "attempting to resume transfer for fileIndex "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but saved session tmpFile\'s size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " while contentRangeFirstBytePos is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 441
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    iget v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->b(I)V

    .line 442
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->A()Ljava/io/File;

    .line 444
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 445
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    .line 448
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->b(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/dws/handlers/ap;->c(J)V

    .line 450
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0, v6, v7}, Lcom/mfluent/asp/dws/handlers/ap;->b(J)V

    .line 451
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 452
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->c(Ljava/lang/String;)V

    .line 456
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->u()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-gez v0, :cond_8

    .line 457
    invoke-interface {p1}, Lorg/apache/james/mime4j/descriptor/BodyDescriptor;->getContentLength()J

    move-result-wide v0

    .line 458
    cmp-long v2, v0, v6

    if-gez v2, :cond_7

    .line 459
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Content-Length header required"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :cond_7
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2, v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->c(J)V

    .line 465
    :cond_8
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::body: Saving file {}"

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 469
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 471
    const/16 v0, 0x1000

    :try_start_0
    new-array v0, v0, [B

    .line 472
    :cond_9
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_a

    invoke-virtual {p2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-eq v8, v2, :cond_a

    .line 474
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 475
    int-to-long v2, v2

    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/ao$b;->e()I

    move-result v4

    iget-object v5, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    iget-object v6, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ap;->E()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Lcom/mfluent/asp/dws/handlers/ap;->d(J)V

    iget-object v5, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    iget-object v6, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v6}, Lcom/mfluent/asp/dws/handlers/ap;->t()J

    move-result-wide v6

    add-long/2addr v2, v6

    invoke-virtual {v5, v2, v3}, Lcom/mfluent/asp/dws/handlers/ap;->b(J)V

    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/ao$b;->e()I

    move-result v2

    if-eq v2, v4, :cond_9

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 504
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    .line 478
    :cond_a
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->u()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_b

    .line 479
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 481
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Saved file length is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v3}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but expected length is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v3}, Lcom/mfluent/asp/dws/handlers/ap;->u()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 488
    :cond_b
    :try_start_2
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->b:Ljava/io/File;

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 491
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/mfluent/asp/util/b;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 492
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 493
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 494
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    :cond_c
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lorg/apache/james/mime4j/descriptor/BodyDescriptor;->getMimeType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :try_start_3
    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->h:Landroid/media/MediaScannerConnection;

    invoke-virtual {v3}, Landroid/media/MediaScannerConnection;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->h:Landroid/media/MediaScannerConnection;

    invoke-virtual {v3, v0, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->C()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_d

    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->p:Ljava/lang/Object;

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->p:Ljava/lang/Object;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_1
    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_d
    :goto_2
    :try_start_6
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 500
    :try_start_7
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 504
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 505
    return-void

    .line 496
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v2

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v0

    :try_start_9
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 500
    :catchall_3
    move-exception v0

    :try_start_a
    iget-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v2}, Lcom/mfluent/asp/dws/handlers/ap;->z()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 496
    :cond_e
    :try_start_b
    iget-object v3, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->i:Ljava/util/Queue;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    invoke-interface {v3, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->e:Z

    .line 392
    return-void
.end method

.method public final endBodyPart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 510
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    .line 512
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    .line 513
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    .line 514
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    .line 515
    return-void
.end method

.method public final endMessage()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 578
    invoke-super {p0}, Lorg/apache/james/mime4j/parser/AbstractContentHandler;->endMessage()V

    .line 580
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 581
    iput-boolean v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->e:Z

    .line 582
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    .line 583
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/ao;->a(Lcom/mfluent/asp/dws/handlers/ao;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/dws/handlers/ap;)V

    .line 585
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/ao$b;->d()V

    .line 586
    return-void
.end method

.method public final field(Lorg/apache/james/mime4j/parser/Field;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 519
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Content-Disposition"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 521
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getRaw()Lorg/apache/james/mime4j/util/ByteSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/james/mime4j/util/ContentUtil;->decode(Ljava/nio/charset/Charset;Lorg/apache/james/mime4j/util/ByteSequence;)Ljava/lang/String;

    move-result-object v0

    .line 522
    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 523
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 524
    new-instance v0, Lorg/apache/james/mime4j/MimeException;

    const-string v1, "Expected to find colon when processing Content-Disposition"

    invoke-direct {v0, v1}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :cond_0
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 528
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->b()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 530
    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    .line 557
    :cond_1
    :goto_0
    return-void

    .line 532
    :cond_2
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Content-Range"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 535
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getBody()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Content-Range:"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lorg/apache/commons/lang3/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 537
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->c()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 539
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_3

    .line 540
    new-instance v1, Lorg/apache/james/mime4j/MimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse Content-Range value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 543
    :cond_3
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    goto :goto_0

    .line 552
    :cond_4
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "File-Index"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 553
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    goto :goto_0

    .line 554
    :cond_5
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GroupId"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555
    invoke-interface {p1}, Lorg/apache/james/mime4j/parser/Field;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onMediaScannerConnected()V
    .locals 4

    .prologue
    .line 656
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 660
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 661
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->h:Landroid/media/MediaScannerConnection;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 664
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 665
    return-void
.end method

.method public final onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 670
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/ao;->b(Lcom/mfluent/asp/dws/handlers/ao;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 671
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 672
    const-string v2, "group_id"

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->a:Lcom/mfluent/asp/dws/handlers/ao;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/ao;->b(Lcom/mfluent/asp/dws/handlers/ao;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p2, v1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 675
    invoke-static {}, Lcom/mfluent/asp/dws/handlers/ao;->a()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::onScanCompleted: path : {}, uri : {} updated as groupId {}. updte count is {}"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 677
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->p:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 679
    :cond_0
    return-void
.end method

.method public final startBodyPart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 396
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->l:J

    .line 398
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->m:I

    .line 399
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->c:Ljava/lang/String;

    .line 400
    iput-object v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->n:Ljava/lang/String;

    .line 401
    return-void
.end method

.method public final startMessage()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 565
    invoke-super {p0}, Lorg/apache/james/mime4j/parser/AbstractContentHandler;->startMessage()V

    .line 567
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->k:Lcom/mfluent/asp/dws/handlers/ap;

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->b:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/dws/handlers/ap;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V

    .line 568
    iput-boolean v2, p0, Lcom/mfluent/asp/dws/handlers/ao$b;->e:Z

    .line 569
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/dws/handlers/ao$b;->a(Z)Lcom/mfluent/asp/filetransfer/b;

    .line 570
    return-void
.end method
