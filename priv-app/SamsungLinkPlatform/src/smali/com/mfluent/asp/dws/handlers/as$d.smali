.class final Lcom/mfluent/asp/dws/handlers/as$d;
.super Lcom/mfluent/asp/dws/handlers/as$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 792
    sget-object v0, Lcom/mfluent/asp/dws/a;->a:[Lcom/mfluent/asp/dws/a$g;

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/as$e;->a([Lcom/mfluent/asp/dws/a$g;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as$d;->a:[Ljava/lang/String;

    .line 793
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;-><init>()V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as$d;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 789
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/dws/handlers/as$e;-><init>(B)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 789
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/as$d;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 817
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 797
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as$d;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Lcom/mfluent/asp/dws/a$g;
    .locals 1

    .prologue
    .line 802
    sget-object v0, Lcom/mfluent/asp/dws/a;->a:[Lcom/mfluent/asp/dws/a$g;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 807
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;
    .locals 1

    .prologue
    .line 812
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as$d;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    return-object v0
.end method
