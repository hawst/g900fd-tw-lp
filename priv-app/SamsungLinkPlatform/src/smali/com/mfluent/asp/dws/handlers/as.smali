.class public final Lcom/mfluent/asp/dws/handlers/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/dws/handlers/as$c;,
        Lcom/mfluent/asp/dws/handlers/as$d;,
        Lcom/mfluent/asp/dws/handlers/as$g;,
        Lcom/mfluent/asp/dws/handlers/as$a;,
        Lcom/mfluent/asp/dws/handlers/as$e;,
        Lcom/mfluent/asp/dws/handlers/as$b;,
        Lcom/mfluent/asp/dws/handlers/as$f;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/lang/CharSequence;

.field private static final c:Ljava/lang/CharSequence;

.field private static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/dws/handlers/as$e;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/regex/Pattern;

.field private static final i:Ljava/util/regex/Pattern;


# instance fields
.field private e:Landroid/content/ContentResolver;

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 41
    const-string v0, "0"

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as;->b:Ljava/lang/CharSequence;

    .line 43
    const-string v0, "1"

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as;->c:Ljava/lang/CharSequence;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as;->d:Ljava/util/HashMap;

    .line 531
    const-string v0, ","

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as;->h:Ljava/util/regex/Pattern;

    .line 532
    const-string v0, "="

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/dws/handlers/as;->i:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as;->d:Ljava/util/HashMap;

    const-string v1, "Photo"

    new-instance v2, Lcom/mfluent/asp/dws/handlers/as$d;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/dws/handlers/as$d;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as;->d:Ljava/util/HashMap;

    const-string v1, "Video"

    new-instance v2, Lcom/mfluent/asp/dws/handlers/as$g;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/dws/handlers/as$g;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as;->d:Ljava/util/HashMap;

    const-string v1, "Music"

    new-instance v2, Lcom/mfluent/asp/dws/handlers/as$a;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/dws/handlers/as$a;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/mfluent/asp/dws/handlers/as;->d:Ljava/util/HashMap;

    const-string v1, "Document"

    new-instance v2, Lcom/mfluent/asp/dws/handlers/as$c;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/dws/handlers/as$c;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    return-void
.end method

.method private a(Lcom/mfluent/asp/dws/handlers/as$e;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 627
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->c()Landroid/net/Uri;

    move-result-object v1

    .line 628
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "max(_id)"

    aput-object v0, v2, v4

    .line 630
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 632
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, -0x1

    .line 634
    :goto_0
    if-eqz v1, :cond_1

    .line 635
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return v0

    .line 632
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 634
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 635
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private a(Lcom/mfluent/asp/dws/handlers/as$e;Lorg/json/JSONArray;Landroid/database/Cursor;I)I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 395
    const/4 v4, -0x1

    .line 397
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/dws/handlers/as;->f:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/dws/handlers/as$e;->a(I)Landroid/net/Uri;

    move-result-object v3

    .line 399
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/dws/handlers/as$e;->b()[Lcom/mfluent/asp/dws/a$g;

    move-result-object v12

    .line 400
    const/4 v9, 0x0

    .line 402
    const-string v5, "source_media_id=?"

    .line 403
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .line 405
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 406
    const-string v2, "is_delete"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 407
    const-string v2, "media_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 409
    const/4 v2, 0x0

    move v10, v2

    move v2, v4

    :goto_0
    move/from16 v0, p4

    if-ge v10, v0, :cond_3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 410
    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 412
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 414
    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 416
    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_0

    .line 417
    const-string v2, "Command"

    const-string v4, "Delete"

    invoke-virtual {v8, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 418
    const-string v2, "id"

    move-object/from16 v0, v16

    invoke-virtual {v8, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v2, v9

    .line 448
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 409
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-object v9, v2

    move v2, v11

    goto :goto_0

    .line 422
    :cond_0
    const/4 v2, 0x0

    aput-object v16, v6, v2

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/dws/handlers/as$e;->a()[Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 424
    if-nez v7, :cond_1

    .line 425
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unexpected null cursor returned when trying to get media records"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 430
    :cond_1
    if-nez v9, :cond_4

    .line 431
    :try_start_0
    invoke-static {v12, v7}, Lcom/mfluent/asp/dws/handlers/as;->a([Lcom/mfluent/asp/dws/a$g;Landroid/database/Cursor;)Ljava/util/HashMap;

    move-result-object v4

    .line 435
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 436
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7, v12, v4}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;Landroid/database/Cursor;[Lcom/mfluent/asp/dws/a$g;Ljava/util/HashMap;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 445
    :goto_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v8, v2

    move-object v2, v4

    .line 446
    goto :goto_1

    .line 438
    :cond_2
    :try_start_1
    const-string v2, "mfl_SyncCommandHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v17, "Skipping sending change sync record for a media item (source media ID: "

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, " content uri: "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, ") because no record was found in the media table."

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v8

    goto :goto_3

    .line 445
    :catchall_0
    move-exception v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2

    .line 451
    :cond_3
    return v2

    :cond_4
    move-object v4, v9

    goto :goto_2
.end method

.method private a(Lcom/mfluent/asp/dws/handlers/as$e;II)Lcom/mfluent/asp/dws/handlers/as$b;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 351
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/as;->f:I

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/dws/handlers/as$e;->a(I)Landroid/net/Uri;

    move-result-object v1

    .line 357
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->a()[Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v5, p3, 0x1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    move-object v4, v3

    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "_id DESC LIMIT "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 358
    if-nez v1, :cond_1

    .line 359
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected null cursor returned when trying to get media records"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_0
    const-string v3, "_id<?"

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    goto :goto_0

    .line 364
    :cond_1
    :try_start_0
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 365
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->b()[Lcom/mfluent/asp/dws/a$g;

    move-result-object v3

    .line 368
    invoke-static {v3, v1}, Lcom/mfluent/asp/dws/handlers/as;->a([Lcom/mfluent/asp/dws/a$g;Landroid/database/Cursor;)Ljava/util/HashMap;

    move-result-object v4

    .line 370
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    move v0, v7

    .line 372
    :goto_1
    if-ge v0, p3, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 374
    invoke-direct {p0, p1, v1, v3, v4}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;Landroid/database/Cursor;[Lcom/mfluent/asp/dws/a$g;Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v8

    .line 375
    invoke-virtual {v5, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 378
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result p2

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 381
    :cond_2
    new-instance v2, Lcom/mfluent/asp/dws/handlers/as$b;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, p3, :cond_3

    move v0, v6

    :goto_2
    invoke-direct {v2, p0, p2, v0, v5}, Lcom/mfluent/asp/dws/handlers/as$b;-><init>(Lcom/mfluent/asp/dws/handlers/as;IZLorg/json/JSONArray;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2

    :cond_3
    move v0, v7

    .line 381
    goto :goto_2

    .line 386
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/mfluent/asp/dws/handlers/as$e;Ljava/util/HashMap;I)Lcom/mfluent/asp/dws/handlers/as$f;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/dws/handlers/as$e;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)",
            "Lcom/mfluent/asp/dws/handlers/as$f;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 210
    const-string v0, "j"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 211
    const-string v1, "v"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/String;

    .line 215
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    .line 220
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->d()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/datamodel/al;->b(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;)J

    move-result-wide v0

    .line 221
    cmp-long v0, v0, v10

    if-ltz v0, :cond_0

    move-object v0, v2

    .line 258
    :goto_0
    return-object v0

    .line 218
    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_0

    .line 227
    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->c()Landroid/net/Uri;

    move-result-object v1

    .line 228
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    const-string v0, "media_id"

    aput-object v0, v2, v7

    const/4 v0, 0x2

    const-string v3, "is_delete"

    aput-object v3, v2, v0

    .line 230
    const-string v3, "_id>?"

    .line 231
    new-array v4, v7, [Ljava/lang/String;

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 233
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "_id ASC LIMIT "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v9, p3, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 236
    if-nez v4, :cond_1

    .line 237
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected null cursor returned when trying to get media records"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_1
    :try_start_1
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 242
    invoke-direct {p0, p1, v5, v4, p3}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;Lorg/json/JSONArray;Landroid/database/Cursor;I)I

    move-result v0

    int-to-long v0, v0

    .line 243
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    move-wide v2, v10

    .line 248
    :goto_1
    invoke-direct {p0, p1, v2, v3}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;J)V

    .line 250
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->d()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    iget v9, p0, Lcom/mfluent/asp/dws/handlers/as;->g:I

    invoke-static {v1, v0, v9, v10, v11}, Lcom/mfluent/asp/datamodel/al;->a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;IJ)V

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/mfluent/asp/datamodel/al;->a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;)V

    .line 252
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, p3, :cond_2

    move v1, v7

    .line 255
    :goto_2
    invoke-static {v2, v3, v6}, Lcom/mfluent/asp/dws/handlers/as;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 256
    new-instance v0, Lcom/mfluent/asp/dws/handlers/as$f;

    invoke-direct {v0, v2, v1, v5}, Lcom/mfluent/asp/dws/handlers/as$f;-><init>(Ljava/lang/String;ZLorg/json/JSONArray;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    move v1, v8

    .line 252
    goto :goto_2

    .line 258
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move-wide v2, v0

    goto :goto_1
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    const-string v1, "files_table_validity"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->getValue(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 572
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "c=1,j="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",v="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/as;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    .line 535
    invoke-static {p0}, Lcom/mfluent/asp/dws/handlers/as;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 538
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 552
    :cond_1
    return-object v0

    .line 542
    :cond_2
    sget-object v1, Lcom/mfluent/asp/dws/handlers/as;->h:Ljava/util/regex/Pattern;

    const/4 v3, 0x4

    invoke-virtual {v1, v0, v3}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v3

    .line 544
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 545
    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 546
    sget-object v6, Lcom/mfluent/asp/dws/handlers/as;->i:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v5, v8}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v5

    .line 547
    array-length v6, v5

    if-ne v6, v8, :cond_3

    .line 548
    aget-object v6, v5, v2

    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-virtual {v0, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static a([Lcom/mfluent/asp/dws/a$g;Landroid/database/Cursor;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/mfluent/asp/dws/a$g;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455
    new-instance v1, Ljava/util/HashMap;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 458
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 459
    iget-object v4, v3, Lcom/mfluent/asp/dws/a$g;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/mfluent/asp/dws/a$g;->b:Ljava/lang/String;

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    :cond_0
    return-object v1
.end method

.method private a(Lcom/mfluent/asp/dws/handlers/as$e;Landroid/database/Cursor;[Lcom/mfluent/asp/dws/a$g;Ljava/util/HashMap;)Lorg/json/JSONObject;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/dws/handlers/as$e;",
            "Landroid/database/Cursor;",
            "[",
            "Lcom/mfluent/asp/dws/a$g;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 505
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 506
    const-string v0, "Command"

    const-string v1, "Add"

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 510
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p3

    if-ge v1, v0, :cond_0

    .line 511
    aget-object v3, p3, v1

    .line 515
    iget-object v0, v3, Lcom/mfluent/asp/dws/a$g;->a:Ljava/lang/String;

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 516
    invoke-virtual {v3, p2, v0}, Lcom/mfluent/asp/dws/a$g;->a(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v0

    .line 517
    iget-object v3, v3, Lcom/mfluent/asp/dws/a$g;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 510
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    invoke-virtual {p1, v0, v2, p2}, Lcom/mfluent/asp/dws/handlers/as$e;->a(Landroid/content/ContentResolver;Lorg/json/JSONObject;Landroid/database/Cursor;)V

    .line 521
    return-object v2
.end method

.method private a(Lcom/mfluent/asp/dws/handlers/as$e;J)V
    .locals 2

    .prologue
    .line 263
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/as$e;->d()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-result-object v0

    .line 265
    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    invoke-static {v1, v0, p2, p3}, Lcom/mfluent/asp/datamodel/al;->a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;J)V

    .line 267
    return-void
.end method

.method private b(Lcom/mfluent/asp/dws/handlers/as$e;Ljava/util/HashMap;I)Lcom/mfluent/asp/dws/handlers/as$f;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/dws/handlers/as$e;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)",
            "Lcom/mfluent/asp/dws/handlers/as$f;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    .line 281
    if-nez p2, :cond_0

    .line 284
    invoke-direct {p0, p1}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;)I

    move-result v0

    .line 285
    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/as;->a()Ljava/lang/String;

    move-result-object v1

    .line 291
    int-to-long v6, v0

    invoke-direct {p0, p1, v6, v7}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;J)V

    move-object v3, v1

    move v1, v2

    .line 312
    :goto_0
    invoke-direct {p0, p1, v1, p3}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;II)Lcom/mfluent/asp/dws/handlers/as$b;

    move-result-object v5

    .line 317
    iget-boolean v1, v5, Lcom/mfluent/asp/dws/handlers/as$b;->b:Z

    if-eqz v1, :cond_1

    .line 318
    iget v1, v5, Lcom/mfluent/asp/dws/handlers/as$b;->a:I

    int-to-long v6, v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "c=0,i="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",j="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",v="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/dws/handlers/as;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v4

    .line 330
    :goto_1
    new-instance v2, Lcom/mfluent/asp/dws/handlers/as$f;

    iget-object v3, v5, Lcom/mfluent/asp/dws/handlers/as$b;->c:Lorg/json/JSONArray;

    invoke-direct {v2, v1, v0, v3}, Lcom/mfluent/asp/dws/handlers/as$f;-><init>(Ljava/lang/String;ZLorg/json/JSONArray;)V

    move-object v0, v2

    :goto_2
    return-object v0

    .line 298
    :cond_0
    :try_start_0
    const-string v0, "i"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 299
    const-string v1, "j"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 302
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 303
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 304
    const-string v0, "v"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move v8, v1

    move v1, v3

    move-object v3, v0

    move v0, v8

    .line 308
    goto :goto_0

    .line 307
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_2

    .line 323
    :cond_1
    int-to-long v6, v0

    invoke-static {v6, v7, v3}, Lcom/mfluent/asp/dws/handlers/as;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 326
    invoke-direct {p0, p1}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;)I

    move-result v3

    .line 327
    if-eq v3, v2, :cond_2

    if-ge v0, v3, :cond_2

    move v0, v4

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 588
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    :goto_0
    return-object p0

    .line 593
    :cond_0
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 594
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 596
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trouble encoding sync key"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 606
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    :goto_0
    return-object p0

    .line 613
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 620
    :try_start_1
    new-instance p0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {p0, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 622
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trouble encoding sync key"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 616
    :catch_1
    move-exception v0

    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Landroid/content/ContentResolver;II)Lorg/json/JSONObject;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;
        }
    .end annotation

    .prologue
    .line 72
    sget-object v1, Lcom/mfluent/asp/dws/handlers/as;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 73
    const-string v1, "mfl_SyncCommandHandler"

    const-string v2, "Processing Sync command request"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/as;->e:Landroid/content/ContentResolver;

    .line 77
    move/from16 v0, p3

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/as;->f:I

    .line 78
    move/from16 v0, p4

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/as;->g:I

    .line 81
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 82
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 85
    :try_start_0
    const-string v1, "Command"

    const-string v2, "Sync"

    invoke-virtual {v6, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 86
    const-string v1, "Status"

    const/4 v2, 0x1

    invoke-virtual {v6, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 87
    const-string v1, "Items"

    invoke-virtual {v6, v1, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    const-string v1, "Items"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 93
    if-nez v8, :cond_1

    .line 94
    new-instance v1, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v2, "Invalid Sync command - Items array required."

    invoke-direct {v1, v2}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble building JSON response - couldn\'t add command and items fields"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 97
    :cond_1
    const-string v1, "MaxRecords"

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 102
    const/4 v1, 0x0

    move v4, v1

    move v5, v2

    :goto_0
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v4, v1, :cond_c

    .line 106
    invoke-virtual {v8, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 107
    if-nez v2, :cond_2

    .line 108
    new-instance v1, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v2, "Invalid Sync command - expected an object in the Items array"

    invoke-direct {v1, v2}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :cond_2
    const-string v1, "Class"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 111
    if-nez v9, :cond_3

    .line 112
    new-instance v1, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v2, "Invalid Sync command - each object in the Items array must have a Class field."

    invoke-direct {v1, v2}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 119
    :cond_3
    sget-object v1, Lcom/mfluent/asp/dws/handlers/as;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/dws/handlers/as$e;

    .line 120
    if-nez v1, :cond_4

    .line 121
    new-instance v1, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v2, "Invalid Sync Command - invalid media type in Class field"

    invoke-direct {v1, v2}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 124
    :cond_4
    const-string v3, "SyncKey"

    const/4 v10, 0x0

    invoke-virtual {v2, v3, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 125
    if-nez v2, :cond_5

    .line 126
    new-instance v1, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v2, "Invalid Sync command - each object in the Items array must have a SyncKey field."

    invoke-direct {v1, v2}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_5
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 138
    :try_start_1
    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 139
    invoke-static {v2}, Lcom/mfluent/asp/dws/handlers/as;->a(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v11

    .line 141
    const-string v2, "c"

    invoke-virtual {v11, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 143
    if-eqz v3, :cond_6

    .line 144
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v5}, Lcom/mfluent/asp/dws/handlers/as;->b(Lcom/mfluent/asp/dws/handlers/as$e;Ljava/util/HashMap;I)Lcom/mfluent/asp/dws/handlers/as$f;

    move-result-object v1

    .line 160
    :goto_1
    const-string v2, "Class"

    invoke-virtual {v10, v2, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 161
    if-nez v1, :cond_a

    .line 163
    const-string v1, "Status"

    const/4 v2, 0x4

    invoke-virtual {v10, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 164
    const-string v1, "SyncKey"

    const-string v2, "0"

    invoke-virtual {v10, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move v2, v5

    .line 178
    :goto_2
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 102
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v5, v2

    goto/16 :goto_0

    .line 145
    :cond_6
    :try_start_2
    const-string v3, "v"

    invoke-virtual {v11, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0}, Lcom/mfluent/asp/dws/handlers/as;->a()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_7

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_9

    .line 148
    sget-object v3, Lcom/mfluent/asp/dws/handlers/as;->b:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 150
    invoke-direct {p0, v1, v11, v5}, Lcom/mfluent/asp/dws/handlers/as;->b(Lcom/mfluent/asp/dws/handlers/as$e;Ljava/util/HashMap;I)Lcom/mfluent/asp/dws/handlers/as$f;

    move-result-object v1

    goto :goto_1

    .line 145
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 151
    :cond_8
    sget-object v3, Lcom/mfluent/asp/dws/handlers/as;->c:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 152
    invoke-direct {p0, v1, v11, v5}, Lcom/mfluent/asp/dws/handlers/as;->a(Lcom/mfluent/asp/dws/handlers/as$e;Ljava/util/HashMap;I)Lcom/mfluent/asp/dws/handlers/as$f;

    move-result-object v1

    goto :goto_1

    .line 156
    :cond_9
    const/4 v1, 0x0

    goto :goto_1

    .line 166
    :cond_a
    const-string v2, "Status"

    const/4 v3, 0x1

    invoke-virtual {v10, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 167
    const-string v2, "SyncKey"

    iget-object v3, v1, Lcom/mfluent/asp/dws/handlers/as$f;->a:Ljava/lang/String;

    invoke-virtual {v10, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 168
    const-string v2, "MoreAvailable"

    iget-boolean v3, v1, Lcom/mfluent/asp/dws/handlers/as$f;->b:Z

    invoke-virtual {v10, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 169
    const-string v2, "Commands"

    iget-object v3, v1, Lcom/mfluent/asp/dws/handlers/as$f;->c:Lorg/json/JSONArray;

    invoke-virtual {v10, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 171
    iget-object v2, v1, Lcom/mfluent/asp/dws/handlers/as$f;->c:Lorg/json/JSONArray;

    if-eqz v2, :cond_b

    .line 172
    iget-object v1, v1, Lcom/mfluent/asp/dws/handlers/as$f;->c:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v1

    sub-int/2addr v5, v1

    :cond_b
    move v2, v5

    .line 177
    goto :goto_2

    .line 176
    :catch_1
    move-exception v1

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble building JSON response - couldn\'t add sync data items"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :cond_c
    return-object v6
.end method
