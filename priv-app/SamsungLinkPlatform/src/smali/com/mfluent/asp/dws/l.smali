.class Lcom/mfluent/asp/dws/l;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final f:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:Lorg/apache/http/protocol/HttpService;

.field private final c:Lorg/apache/http/HttpServerConnection;

.field private final d:Lcom/mfluent/asp/datamodel/Device;

.field private final e:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/l;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/mfluent/asp/dws/l;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/dws/l;-><init>(Landroid/content/Context;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;Lcom/mfluent/asp/datamodel/Device;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/dws/l;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/dws/l;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 42
    iput-object p2, p0, Lcom/mfluent/asp/dws/l;->b:Lorg/apache/http/protocol/HttpService;

    .line 43
    iput-object p3, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    .line 44
    iput-object p4, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/l;->e:Landroid/content/Context;

    .line 46
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 50
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    .line 51
    const-string v1, "MFL_ATTR_DEVICE"

    iget-object v2, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    const-string v1, "MFL_ATTR_CONN"

    iget-object v2, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    iget-object v1, p0, Lcom/mfluent/asp/dws/l;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/util/t;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/t;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/util/t;->a(I)Lcom/mfluent/asp/util/t$d;

    move-result-object v1

    .line 55
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 56
    iget-object v2, p0, Lcom/mfluent/asp/dws/l;->b:Lorg/apache/http/protocol/HttpService;

    iget-object v3, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-virtual {v2, v3, v0}, Lorg/apache/http/protocol/HttpService;->handleRequest(Lorg/apache/http/HttpServerConnection;Lorg/apache/http/protocol/HttpContext;)V
    :try_end_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    :try_start_1
    sget-object v0, Lcom/mfluent/asp/dws/l;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    .line 60
    const-string v0, "mfl_WorkerThread"

    const-string v2, "::run:Client closed connection"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_1

    .line 69
    const-string v0, "mfl_WorkerThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Client connection shutting down for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 74
    :goto_1
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V

    .line 75
    :goto_2
    return-void

    .line 68
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_3

    .line 69
    const-string v0, "mfl_WorkerThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Client connection shutting down for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 74
    :goto_3
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V

    goto :goto_2

    .line 62
    :catch_1
    move-exception v0

    .line 63
    :try_start_4
    sget-object v2, Lcom/mfluent/asp/dws/l;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x5

    if-gt v2, v3, :cond_4

    .line 64
    const-string v2, "mfl_WorkerThread"

    const-string v3, "::run:Trouble handling request"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 68
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_5

    .line 69
    const-string v0, "mfl_WorkerThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Client connection shutting down for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 74
    :goto_4
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V

    goto :goto_2

    .line 67
    :catchall_0
    move-exception v0

    .line 68
    :try_start_6
    iget-object v2, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v2, :cond_6

    .line 69
    const-string v2, "mfl_WorkerThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::run:Client connection shutting down for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/dws/l;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_6
    iget-object v2, p0, Lcom/mfluent/asp/dws/l;->c:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 74
    :goto_5
    invoke-virtual {v1}, Lcom/mfluent/asp/util/t$d;->a()V

    throw v0

    :catch_2
    move-exception v2

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto/16 :goto_1

    :catch_5
    move-exception v0

    goto :goto_3
.end method
