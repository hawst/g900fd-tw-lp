.class final Lcom/mfluent/asp/dws/handlers/l$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/filetransfer/FileTransferSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/dws/handlers/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/ContentValues;

.field private b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    invoke-static {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 127
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    .line 128
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    invoke-static {p1, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 129
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "uuid"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    if-ne p1, v0, :cond_0

    .line 277
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 279
    :cond_0
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "createdTime"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "numFiles"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "numFilesSkipped"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final h()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "sourceDeviceId"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 169
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 170
    if-nez v0, :cond_0

    .line 171
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcom/mfluent/asp/datamodel/Device;
    .locals 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "targetDeviceId"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 180
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    return-object v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 185
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "transferSize"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/l$a;->k()J

    move-result-wide v0

    return-wide v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->a:Landroid/content/ContentValues;

    const-string v1, "firstFileName"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/l$a;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return-object v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 235
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 240
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/l$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 248
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    goto :goto_0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    return v0
.end method
