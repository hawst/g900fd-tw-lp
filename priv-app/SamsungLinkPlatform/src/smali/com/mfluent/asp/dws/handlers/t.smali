.class public final Lcom/mfluent/asp/dws/handlers/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/t;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/mfluent/asp/datamodel/filebrowser/i;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/filebrowser/i;",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 450
    move v1, v0

    .line 451
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 452
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v2

    .line 453
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 454
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 455
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    .line 456
    add-int/lit8 v1, v1, 0x1

    .line 451
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    :cond_1
    return v1
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 393
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 394
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    :goto_1
    return v1

    .line 393
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 399
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    :cond_0
    if-eqz p2, :cond_5

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 p1, p2

    .line 95
    :cond_1
    :goto_0
    const/4 v9, 0x0

    .line 97
    const/4 v6, 0x0

    .line 103
    :try_start_0
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 104
    const-string v2, "OK"

    .line 106
    if-eqz p0, :cond_24

    const-string v3, "mycomputer"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 108
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 109
    const-string v4, "mfl_GetFileListHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "converted mycomputer to root path id="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/16 v4, 0xa

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    move-object/from16 v4, p0

    .line 111
    :goto_1
    if-eqz p1, :cond_2

    if-nez p3, :cond_4

    .line 116
    :cond_2
    sget-object v2, Lcom/mfluent/asp/dws/handlers/t;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_3

    .line 117
    const-string v2, "mfl_GetFileListHandler"

    const-string v3, "::infoFiles:view and maxItems are mandatory parameter."

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_3
    const-string v2, "-605341"

    .line 133
    :cond_4
    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 134
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 135
    const-string v4, "result"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 357
    :goto_2
    return-object v2

    .line 89
    :cond_5
    const-string p1, ""

    goto :goto_0

    .line 142
    :cond_6
    :try_start_1
    new-instance v12, Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-direct {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;-><init>()V

    .line 143
    invoke-static {}, Lcom/mfluent/asp/c;->a()Lcom/mfluent/asp/c;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/c;->b(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v5

    .line 146
    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 147
    new-instance v3, Ljava/lang/String;

    const/16 v2, 0xa

    invoke-static {v4, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    .line 149
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_7

    .line 152
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 153
    const-string v3, "result"

    const-string v5, "-605343"

    invoke-virtual {v2, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 159
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 162
    :cond_7
    invoke-static {v4}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 164
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 165
    const-string v3, "result"

    const-string v4, "-405344"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 166
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 169
    :cond_8
    new-instance v2, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-direct {v2, v4}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v18, v2

    move-object v2, v3

    move-object/from16 v3, v18

    .line 175
    :goto_3
    if-eqz p6, :cond_a

    :try_start_2
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    .line 176
    invoke-static/range {p6 .. p6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 177
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    cmp-long v2, v14, v16

    if-nez v2, :cond_a

    .line 179
    const-string v2, "mfl_GetFileListHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "revision is same skip send filelist rev="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 181
    const-string v4, "dirRevision"

    move-object/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 182
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    goto/16 :goto_2

    .line 171
    :cond_9
    const/4 v2, 0x0

    move-object v3, v2

    move-object v2, v4

    goto :goto_3

    .line 185
    :catch_0
    move-exception v2

    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 188
    :cond_a
    const/4 v2, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v12, v3, v5, v0, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V

    .line 190
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v13

    .line 192
    if-nez p4, :cond_b

    .line 193
    const-string p4, ""

    .line 195
    :cond_b
    if-nez p5, :cond_c

    .line 196
    const-string p5, ""

    .line 203
    :cond_c
    const/4 v8, 0x0

    .line 204
    const/4 v3, 0x0

    .line 206
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 207
    invoke-static {v12, v14}, Lcom/mfluent/asp/dws/handlers/t;->a(Lcom/mfluent/asp/datamodel/filebrowser/i;Ljava/util/List;)I

    move-result v4

    .line 208
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    .line 232
    new-instance v15, Lorg/json/JSONArray;

    invoke-direct {v15}, Lorg/json/JSONArray;-><init>()V

    .line 234
    const/4 v7, 0x0

    .line 235
    if-lez p3, :cond_23

    move/from16 v2, p3

    .line 239
    :goto_4
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_11

    .line 240
    if-eqz p5, :cond_d

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 241
    :cond_d
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->c()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v9

    if-eqz v9, :cond_e

    .line 242
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->c()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/dws/handlers/t;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;)Lorg/json/JSONObject;

    move-result-object v9

    .line 243
    if-eqz v9, :cond_e

    .line 244
    invoke-virtual {v15, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 260
    :cond_e
    :goto_5
    if-gez v7, :cond_f

    .line 261
    const/4 v7, 0x0

    .line 264
    :cond_f
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v9

    if-ge v9, v2, :cond_10

    .line 265
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    .line 269
    :cond_10
    if-nez v7, :cond_22

    .line 270
    const/4 v8, 0x1

    move v10, v8

    .line 272
    :goto_6
    add-int v8, v7, p3

    if-lt v8, v5, :cond_21

    .line 273
    const/4 v3, 0x1

    move v9, v3

    .line 275
    :goto_7
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->c()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v3

    if-nez v3, :cond_13

    const/4 v3, 0x1

    move v8, v3

    .line 276
    :goto_8
    const-string v3, ""

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    if-ne v7, v2, :cond_14

    .line 278
    const-string v2, "files"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v11, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 279
    const-string v2, "totalCount"

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 280
    const-string v2, "fileCount"

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 281
    const-string v2, "isFirstpage"

    invoke-virtual {v11, v2, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 282
    const-string v2, "isLastpage"

    invoke-virtual {v11, v2, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 283
    const-string v2, "path"

    invoke-virtual {v11, v2, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 284
    const-string v2, "isRoot"

    invoke-virtual {v11, v2, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 288
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 249
    :cond_11
    move-object/from16 v0, p4

    invoke-static {v14, v0}, Lcom/mfluent/asp/dws/handlers/t;->a(Ljava/util/List;Ljava/lang/String;)I

    move-result v9

    .line 250
    if-ltz v9, :cond_e

    .line 251
    if-lez p3, :cond_12

    .line 252
    add-int/lit8 v7, v9, 0x1

    .line 253
    add-int v2, v7, p3

    goto :goto_5

    .line 254
    :cond_12
    if-gez p3, :cond_e

    .line 255
    add-int v7, v9, p3

    move v2, v9

    .line 256
    goto :goto_5

    .line 275
    :cond_13
    const/4 v3, 0x0

    move v8, v3

    goto :goto_8

    .line 291
    :cond_14
    const/4 v3, 0x1

    if-ne v10, v3, :cond_20

    if-nez v9, :cond_20

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_20

    .line 292
    add-int/lit8 v2, v2, -0x1

    move v3, v2

    .line 294
    :goto_9
    if-ge v7, v3, :cond_16

    .line 295
    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-static {v2, v6}, Lcom/mfluent/asp/dws/handlers/t;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;I)Lorg/json/JSONObject;

    move-result-object v2

    .line 296
    if-eqz v2, :cond_15

    .line 297
    invoke-virtual {v15, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 298
    add-int/lit8 v2, v6, 0x1

    move/from16 v18, v4

    move v4, v5

    move v5, v2

    move/from16 v2, v18

    .line 294
    :goto_a
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto :goto_9

    .line 300
    :cond_15
    if-lez v4, :cond_1f

    .line 301
    add-int/lit8 v2, v4, -0x1

    .line 303
    :goto_b
    if-lez v5, :cond_1e

    .line 304
    add-int/lit8 v4, v5, -0x1

    move v5, v6

    goto :goto_a

    .line 308
    :cond_16
    if-eqz p5, :cond_17

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_17
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-gtz v2, :cond_18

    .line 309
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 310
    const-string v3, "result"

    const-string v4, "-705340"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 313
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 318
    :cond_18
    if-nez v8, :cond_1a

    if-eqz p5, :cond_19

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 319
    :cond_19
    add-int/lit8 v5, v5, 0x1

    .line 322
    :cond_1a
    const-string v2, "totalCount"

    invoke-virtual {v11, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 323
    const-string v2, "fileCount"

    invoke-virtual {v11, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 324
    const-string v2, "isFirstpage"

    invoke-virtual {v11, v2, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 325
    const-string v2, "isLastpage"

    invoke-virtual {v11, v2, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 326
    const-string v2, "path"

    invoke-virtual {v11, v2, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 327
    const-string v2, "isRoot"

    invoke-virtual {v11, v2, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 328
    const-string v2, "files"

    invoke-virtual {v11, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 329
    const-string v2, "dirRevision"

    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v11, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 330
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v2

    if-eqz v2, :cond_1b

    .line 331
    const-string v2, "special"

    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 335
    :cond_1b
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    goto/16 :goto_2

    .line 337
    :catch_1
    move-exception v2

    .line 338
    sget-object v3, Lcom/mfluent/asp/dws/handlers/t;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_1c

    .line 339
    const-string v3, "mfl_GetFileListHandler"

    const-string v4, "::infoFiles:Trouble building files list"

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 342
    :cond_1c
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ManyFileList"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 343
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 344
    const-string v3, "result"

    const-string v4, "-705341"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 349
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 351
    :cond_1d
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 352
    const-string v3, "result"

    const-string v4, "-705340"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 357
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_1e
    move v4, v5

    move v5, v6

    goto/16 :goto_a

    :cond_1f
    move v2, v4

    goto/16 :goto_b

    :cond_20
    move v3, v2

    goto/16 :goto_9

    :cond_21
    move v9, v3

    goto/16 :goto_7

    :cond_22
    move v10, v8

    goto/16 :goto_6

    :cond_23
    move v2, v9

    goto/16 :goto_4

    :cond_24
    move-object/from16 v4, p0

    goto/16 :goto_1
.end method

.method private static a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;)Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 364
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 366
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 369
    :try_start_0
    const-string v2, "isFile"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 370
    const-string v2, "isDirectory"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 371
    const-string v2, "created"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 372
    const-string v2, "modified"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 373
    const-string v2, "name"

    const-string v3, ".."

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 374
    const-string v2, "uri"

    const-string v3, "utf-8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/16 v3, 0xa

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 377
    const-string v1, "fileSize"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 378
    const-string v1, "mtime"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 379
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 380
    const-string v1, "special"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :cond_0
    :goto_0
    return-object v0

    .line 382
    :catch_0
    move-exception v0

    .line 383
    sget-object v1, Lcom/mfluent/asp/dws/handlers/t;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 384
    const-string v1, "mfl_GetFileListHandler"

    const-string v2, "::getReturnFolderInfo:Trouble"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 386
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;I)Lorg/json/JSONObject;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 403
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 406
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v5

    .line 407
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 408
    invoke-virtual {v5}, Ljava/io/File;->isHidden()Z

    move-result v4

    if-nez v4, :cond_5

    .line 409
    const-string v4, "index"

    invoke-virtual {v0, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 410
    const-string v6, "isFile"

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v2

    :goto_0
    invoke-virtual {v0, v6, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 411
    const-string v4, "isDirectory"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    :goto_1
    invoke-virtual {v0, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 418
    const-string v2, "created"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->lastModified()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v0, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 419
    const-string v2, "modified"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->lastModified()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v0, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 420
    const-string v2, "name"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 422
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 423
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 430
    :cond_0
    :goto_2
    const-string v3, "uri"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 433
    const-string v2, "fileSize"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->length()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 434
    const-string v2, "mtime"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->lastModified()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 435
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 446
    :goto_3
    return-object v0

    :cond_1
    move v4, v3

    .line 410
    goto :goto_0

    :cond_2
    move v2, v3

    .line 411
    goto :goto_1

    .line 425
    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "utf-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 426
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 427
    const-string v3, "special"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 444
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 441
    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 446
    goto :goto_3
.end method


# virtual methods
.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    new-instance v5, Lcom/mfluent/asp/dws/d;

    invoke-direct {v5, p1}, Lcom/mfluent/asp/dws/d;-><init>(Lorg/apache/http/HttpRequest;)V

    .line 55
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 57
    invoke-virtual {v6}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 58
    invoke-virtual {v6}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 65
    :goto_0
    :try_start_0
    const-string v1, "sort"

    invoke-virtual {v6, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "view"

    invoke-virtual {v6, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/mfluent/asp/dws/d;->b()I

    move-result v3

    invoke-virtual {v5}, Lcom/mfluent/asp/dws/d;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/mfluent/asp/dws/d;->d()Ljava/lang/String;

    move-result-object v5

    const-string v7, "dir"

    invoke-virtual {v6, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    const-string v7, "revision"

    invoke-virtual {v6, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/mfluent/asp/dws/handlers/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 78
    new-instance v1, Lcom/mfluent/asp/util/n;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/util/n;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 79
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Lorg/apache/http/HttpException;

    const-string v2, "Trouble building json response"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
