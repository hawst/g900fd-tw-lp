.class public final Lcom/mfluent/asp/dws/handlers/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/filetransfer/FileTransferSession;


# instance fields
.field private a:Ljava/io/File;

.field private final b:Ljava/io/File;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:I

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:J

.field private k:J

.field private l:J

.field private m:Ljava/lang/String;

.field private n:Lcom/mfluent/asp/datamodel/Device;

.field private o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

.field private final p:J

.field private q:J

.field private r:Z

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->h:I

    .line 47
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 49
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->p:J

    .line 58
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/mfluent/a/a/b;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "savefiles_cache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->b:Ljava/io/File;

    .line 59
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/ap;->d:Ljava/lang/String;

    .line 60
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->c:I

    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 63
    return-void
.end method


# virtual methods
.method public final A()Ljava/io/File;
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->a:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 71
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ap;->b:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/mfluent/asp/dws/handlers/ap;->c:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/dws/handlers/ap;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->a:Ljava/io/File;

    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->a:Ljava/io/File;

    return-object v0
.end method

.method public final B()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->a:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->a:Ljava/io/File;

    .line 78
    return-void
.end method

.method public final C()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->e:I

    return v0
.end method

.method public final D()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->f:J

    return-wide v0
.end method

.method public final E()J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->l:J

    return-wide v0
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->s:Z

    .line 203
    return-void
.end method

.method public final G()Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->n:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->r:Z

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->e:I

    .line 86
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->f:J

    .line 94
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->n:Lcom/mfluent/asp/datamodel/Device;

    .line 235
    return-void
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V
    .locals 2

    .prologue
    .line 156
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 158
    invoke-virtual {p0}, Lcom/mfluent/asp/dws/handlers/ap;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->q:J

    .line 161
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->g:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 278
    iput-boolean p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->r:Z

    .line 279
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 183
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->p:J

    return-wide v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 111
    iput p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->h:I

    .line 112
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 129
    iput-wide p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->j:J

    .line 130
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->i:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 188
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 189
    const/4 v0, 0x0

    .line 193
    :goto_0
    return v0

    .line 190
    :cond_0
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->l:J

    iget-wide v2, p0, Lcom/mfluent/asp/dws/handlers/ap;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 191
    const/16 v0, 0x64

    goto :goto_0

    .line 193
    :cond_1
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->l:J

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/mfluent/asp/dws/handlers/ap;->f:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 138
    iput-wide p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->k:J

    .line 139
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->m:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public final d(J)V
    .locals 1

    .prologue
    .line 146
    iput-wide p1, p0, Lcom/mfluent/asp/dws/handlers/ap;->l:J

    .line 147
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->s:Z

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 207
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ap;->o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->e:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->n:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_0

    .line 223
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 225
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->n:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    return-object v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 244
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->q:J

    return-wide v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->f:J

    return-wide v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 254
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->l:J

    return-wide v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x1

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 264
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ap;->o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->b:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    iget-object v1, p0, Lcom/mfluent/asp/dws/handlers/ap;->o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x1

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->h:I

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->j:J

    return-wide v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->k:J

    return-wide v0
.end method

.method public final v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->o:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    return v0
.end method

.method public final z()Ljava/io/File;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ap;->a:Ljava/io/File;

    return-object v0
.end method
