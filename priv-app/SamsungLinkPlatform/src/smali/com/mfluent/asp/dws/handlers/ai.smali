.class public final Lcom/mfluent/asp/dws/handlers/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$a;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private b:Landroid/content/ContentResolver;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/dws/handlers/ai;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Landroid/content/ContentResolver;II)Lorg/json/JSONObject;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;
        }
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/mfluent/asp/dws/handlers/ai;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 32
    const-string v0, "mfl_InfoCommandHandler"

    const-string v1, "Processing Info command request"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :cond_0
    iput-object p2, p0, Lcom/mfluent/asp/dws/handlers/ai;->b:Landroid/content/ContentResolver;

    .line 36
    iput p3, p0, Lcom/mfluent/asp/dws/handlers/ai;->c:I

    .line 39
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 40
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 43
    :try_start_0
    const-string v0, "Command"

    const-string v1, "Info"

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    const-string v0, "Items"

    invoke-virtual {v7, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    const-string v0, "Items"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 51
    if-nez v9, :cond_1

    .line 52
    new-instance v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v1, "Invalid Info command - Items array required."

    invoke-direct {v0, v1}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trouble building JSON response - couldn\'t add command and items fields"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v6, v0, :cond_c

    .line 55
    invoke-virtual {v9, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 56
    if-nez v0, :cond_2

    .line 57
    new-instance v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v1, "Invalid Info command - expected an object in the Items array"

    invoke-direct {v0, v1}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_2
    const-string v1, "Class"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    if-nez v0, :cond_3

    .line 62
    new-instance v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v1, "Invalid Info command - each object in the Items array must have a Class field."

    invoke-direct {v0, v1}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_3
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 71
    :try_start_1
    const-string v1, "Class"

    invoke-virtual {v10, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    iget v1, p0, Lcom/mfluent/asp/dws/handlers/ai;->c:I

    const-string v2, "Photo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    int-to-long v0, v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    :goto_1
    if-nez v1, :cond_8

    new-instance v0, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;

    const-string v1, "Invalid media class in Info command"

    invoke-direct {v0, v1}, Lcom/mfluent/asp/dws/handlers/MetadataSyncHandler$SyncProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 74
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trouble building JSON response - couldn\'t add class, count, and sizeBytes fields"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_4
    :try_start_2
    const-string v2, "Video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    int-to-long v0, v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-string v2, "Music"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    int-to-long v0, v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_6
    const-string v2, "Document"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    int-to-long v0, v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    :cond_8
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "total(_size)"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "count(*)"

    aput-object v3, v2, v0

    iget-object v0, p0, Lcom/mfluent/asp/dws/handlers/ai;->b:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_9

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_9
    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    :goto_2
    if-eqz v11, :cond_a

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_a
    const-string v4, "Count"

    invoke-virtual {v10, v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "SizeBytes"

    invoke-virtual {v10, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 76
    invoke-virtual {v8, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 54
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 72
    :cond_b
    const/4 v0, 0x0

    :try_start_3
    aget-object v0, v2, v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x1

    aget-object v0, v2, v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-wide v0

    move-wide v2, v4

    goto :goto_2

    .line 79
    :cond_c
    return-object v7
.end method
