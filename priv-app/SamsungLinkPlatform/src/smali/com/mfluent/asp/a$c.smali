.class final Lcom/mfluent/asp/a$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/a;

.field private b:I


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/a;)V
    .locals 1

    .prologue
    .line 923
    iput-object p1, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 925
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/a$c;->b:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/a;B)V
    .locals 0

    .prologue
    .line 923
    invoke-direct {p0, p1}, Lcom/mfluent/asp/a$c;-><init>(Lcom/mfluent/asp/a;)V

    return-void
.end method

.method private c()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 960
    .line 961
    monitor-enter p0

    .line 962
    :try_start_0
    iget v1, p0, Lcom/mfluent/asp/a$c;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mfluent/asp/a$c;->b:I

    .line 963
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 964
    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(TID:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") SamsungAccountServiceConnection::sendRequest  mRegistrationCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v3, v3, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bindCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/a$c;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 974
    :try_start_1
    iget-object v1, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v1, v1, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 976
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 977
    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "user_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "birthday"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "mcc"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "server_url"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "api_server_url"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "auth_server_url"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "cc"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "device_physical_address_text"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "refresh_token"

    aput-object v4, v2, v3

    .line 988
    iget-object v3, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v3}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 990
    :try_start_2
    iget-object v3, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v3}, Lcom/mfluent/asp/a;->f(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/b/b;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 991
    const-string v3, "mfl_AccessManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "(TID:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") SamsungAccountServiceConnection::sendRequest  [expired_access_token] Reqeust New token : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v5}, Lcom/mfluent/asp/a;->f(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/b/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    const-string v3, "expired_access_token"

    iget-object v4, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v4}, Lcom/mfluent/asp/a;->f(Lcom/mfluent/asp/a;)Lcom/sec/pcw/hybrid/b/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1000
    :goto_0
    :try_start_3
    iget-object v3, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v3}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1003
    const-string v3, "additional"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1005
    iget-object v2, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v2, v2, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    if-eqz v2, :cond_1

    .line 1006
    iget-object v2, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v2, v2, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v4, v4, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lcom/msc/a/a/b;->a(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v0

    .line 1015
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/a$c;->b()V

    .line 1018
    :goto_1
    return v0

    .line 971
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 997
    :cond_2
    :try_start_4
    const-string v3, "expired_access_token"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 1000
    :catchall_1
    move-exception v1

    :try_start_5
    iget-object v2, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v2}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1010
    :catch_0
    move-exception v1

    .line 1011
    :try_start_6
    invoke-static {}, Lcom/mfluent/asp/a;->j()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1012
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "(TID:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") SamsungAccountServiceConnection::sendRequest :: RemoteException occurred"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1015
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/a$c;->b()V

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {p0}, Lcom/mfluent/asp/a$c;->b()V

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1035
    const/4 v0, 0x0

    .line 1036
    monitor-enter p0

    .line 1037
    :try_start_0
    iget v2, p0, Lcom/mfluent/asp/a$c;->b:I

    if-lez v2, :cond_0

    .line 1038
    iget v0, p0, Lcom/mfluent/asp/a$c;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/a$c;->b:I

    .line 1040
    const-string v0, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(TID:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") SamsungAccountServiceConnection::bind Already bound. Sending request. bindCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/a$c;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1045
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1046
    if-eqz v0, :cond_2

    .line 1048
    invoke-direct {p0}, Lcom/mfluent/asp/a$c;->c()Z

    move-result v0

    .line 1049
    if-nez v0, :cond_1

    .line 1050
    invoke-virtual {p0}, Lcom/mfluent/asp/a$c;->b()V

    .line 1059
    :cond_1
    :goto_0
    return v0

    .line 1045
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1054
    :cond_2
    const-string v0, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(TID:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") SamsungAccountServiceConnection::bind Not bound yet. Calling bindService."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1056
    const-string v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1057
    const-string v2, "com.osp.app.signin"

    const-string v3, "com.msc.sa.service.RequestService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1058
    const-string v2, "mfl_AccessManager"

    const-string v3, "set explicit intent to com.osp.app.signin"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    iget-object v2, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v2}, Lcom/mfluent/asp/a;->c(Lcom/mfluent/asp/a;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0, p0, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 1064
    monitor-enter p0

    .line 1065
    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/a$c;->b:I

    if-lez v0, :cond_1

    .line 1066
    iget v0, p0, Lcom/mfluent/asp/a$c;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/a$c;->b:I

    .line 1067
    iget v0, p0, Lcom/mfluent/asp/a$c;->b:I

    if-nez v0, :cond_2

    .line 1068
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") SamsungAccountServiceConnection::unbind bindCount is 0. Unbinding Service."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v0, v0, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v0, v0, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v0, v0, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    iget-object v1, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v1, v1, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/msc/a/a/b;->a(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mfluent/asp/a;->c:Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1070
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->c(Lcom/mfluent/asp/a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1071
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    .line 1072
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mfluent/asp/a;->b:Lcom/mfluent/asp/a$b;

    .line 1077
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 1069
    :catch_0
    move-exception v0

    const-string v1, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(TID:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") SamsungAccountServiceConnection::unregisterCallback Failed to unregister callback."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1077
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1074
    :cond_2
    :try_start_3
    const-string v0, "mfl_AccessManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(TID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") SamsungAccountServiceConnection::unbind bindCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mfluent/asp/a$c;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7

    .prologue
    .line 929
    const/4 v1, 0x0

    .line 931
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 932
    :try_start_1
    iget v0, p0, Lcom/mfluent/asp/a$c;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/a$c;->b:I

    .line 933
    const-string v0, "mfl_AccessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(TID:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") SamsungAccountServiceConnection::onServiceConnected bindCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/a$c;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {p2}, Lcom/msc/a/a/b$a;->a(Landroid/os/IBinder;)Lcom/msc/a/a/b;

    move-result-object v2

    iput-object v2, v0, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    .line 935
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    new-instance v2, Lcom/mfluent/asp/a$b;

    iget-object v3, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/a$b;-><init>(Lcom/mfluent/asp/a;)V

    iput-object v2, v0, Lcom/mfluent/asp/a;->b:Lcom/mfluent/asp/a$b;

    .line 937
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v2, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v2, v2, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    const-string v3, "c7hc8m4900"

    const-string v4, "B5B9B48012665C4F1914C52B4B6DD2F4"

    iget-object v5, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v5}, Lcom/mfluent/asp/a;->c(Lcom/mfluent/asp/a;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    iget-object v6, v6, Lcom/mfluent/asp/a;->b:Lcom/mfluent/asp/a$b;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/msc/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/a/a;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    .line 942
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 943
    :try_start_2
    invoke-direct {p0}, Lcom/mfluent/asp/a$c;->c()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    .line 948
    :goto_0
    if-nez v0, :cond_0

    .line 949
    invoke-virtual {p0}, Lcom/mfluent/asp/a$c;->b()V

    .line 950
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 952
    :try_start_3
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->e(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 954
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 957
    :cond_0
    return-void

    .line 942
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0

    throw v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    .line 944
    :catch_0
    move-exception v0

    .line 945
    const-string v2, "mfl_AccessManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "(TID:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") SamsungAccountServiceConnection::onServiceConnected failed to registerCallback"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 954
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v1}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 1082
    const-string v0, "mfl_AccessManager"

    const-string v1, "onServiceDisconnected :: SA Service is killed unexpectedly"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    monitor-enter p0

    .line 1084
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mfluent/asp/a;->a:Lcom/msc/a/a/b;

    .line 1085
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mfluent/asp/a;->c:Ljava/lang/String;

    .line 1086
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/a$c;->b:I

    .line 1087
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1089
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1091
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->e(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1093
    iget-object v0, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v0}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1094
    return-void

    .line 1087
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1093
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/a$c;->a:Lcom/mfluent/asp/a;

    invoke-static {v1}, Lcom/mfluent/asp/a;->d(Lcom/mfluent/asp/a;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
