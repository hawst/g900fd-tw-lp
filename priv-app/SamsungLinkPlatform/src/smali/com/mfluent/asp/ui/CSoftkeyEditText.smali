.class public Lcom/mfluent/asp/ui/CSoftkeyEditText;
.super Landroid/widget/EditText;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;
    }
.end annotation


# instance fields
.field private a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    .line 80
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;->a()V

    .line 45
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;->b()V

    .line 59
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/widget/EditText;->onWindowFocusChanged(Z)V

    .line 68
    iget-object v0, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;->c()V

    .line 71
    :cond_0
    return-void
.end method
