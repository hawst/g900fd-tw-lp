.class final Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->a()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

.field final synthetic b:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;

    iput-object p2, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 471
    invoke-static {}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Starting delete device task"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 473
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "deviceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 474
    const/4 v0, 0x0

    .line 476
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->deregisterDevice(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 480
    :goto_0
    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {v1, v0}, Ljava/lang/Boolean;-><init>(Z)V

    return-object v1

    .line 477
    :catch_0
    move-exception v1

    .line 478
    invoke-static {}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "Failed to deregisterDevice"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 464
    check-cast p1, Ljava/lang/Boolean;

    invoke-static {}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Delete task complete {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;

    iput-object p1, v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->a:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->c()V

    return-void
.end method
