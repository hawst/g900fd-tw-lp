.class public Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;
.super Lcom/mfluent/asp/ui/LifecycleTrackingActivity;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;,
        Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$b;,
        Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TabHost;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;

.field private d:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->b:Ljava/util/HashMap;

    .line 241
    return-void
.end method

.method private static a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V
    .locals 1

    .prologue
    .line 183
    new-instance v0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$b;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p0, p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$b;-><init>(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    .line 184
    invoke-virtual {p1, p2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 185
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 295
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    instance-of v2, v1, Landroid/widget/ScrollView;

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v2

    iget-object v3, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v3}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v3, 0x15

    if-ne v1, v3, :cond_1

    if-lez v2, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setCurrentTab(I)V

    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->requestFocus()Z

    :cond_0
    move v1, v0

    :goto_0
    if-eqz v1, :cond_4

    .line 299
    :goto_1
    return v0

    .line 295
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v3, 0x16

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_2

    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setCurrentTab(I)V

    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->requestFocus()Z

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 299
    :cond_4
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 89
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const-string v0, "hojun"

    const-string v2, "onCreate"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 94
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->setTheme(I)V

    .line 99
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 101
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 102
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 104
    const v0, 0x7f0a0390

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->setTitle(I)V

    .line 106
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->setContentView(I)V

    .line 107
    const v0, 0x1020012

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    iget-object v2, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    const-string v3, "pc"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0370

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    new-instance v3, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;

    const-string v4, "pc"

    const-class v5, Lcom/mfluent/asp/ui/common/b;

    invoke-direct {v3, p0, v4, v5, p1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;-><init>(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    invoke-static {p0, v0, v2}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->b:Ljava/util/HashMap;

    invoke-static {v3}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;->a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    iget-object v2, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    const-string v3, "mobile"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a036f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    new-instance v3, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;

    const-string v4, "mobile"

    const-class v5, Lcom/mfluent/asp/ui/common/a;

    invoke-direct {v3, p0, v4, v5, p1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;-><init>(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    invoke-static {p0, v0, v2}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->b:Ljava/util/HashMap;

    invoke-static {v3}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;->a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    iget-object v2, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    const-string v3, "tv"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0371

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    new-instance v3, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;

    const-string v4, "tv"

    const-class v5, Lcom/mfluent/asp/ui/common/c;

    invoke-direct {v3, p0, v4, v5, p1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;-><init>(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    invoke-static {p0, v0, v2}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->b:Ljava/util/HashMap;

    invoke-static {v3}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;->a(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    add-int/lit16 v2, v1, 0x456

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 96
    :cond_1
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->setTheme(I)V

    goto/16 :goto_0

    .line 108
    :cond_2
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const-class v1, Lcom/mfluent/asp/ui/common/b;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v1, Lcom/mfluent/asp/ui/common/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v1, Lcom/mfluent/asp/ui/common/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;

    invoke-super {p0}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;-><init>(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;)V

    iput-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->c:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;

    const v0, 0x7f09005b

    invoke-super {p0, v0}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->c:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 127
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 222
    invoke-super {p0}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->onDestroy()V

    .line 223
    const-string v0, "hojun"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 238
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 234
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->finish()V

    .line 235
    const/4 v0, 0x1

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 198
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 215
    invoke-super {p0}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->onResume()V

    .line 217
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->a:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    .line 191
    iget-object v1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 192
    return-void
.end method
