.class public Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;,
        Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;,
        Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$a;,
        Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;
    }
.end annotation


# static fields
.field private static c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static r:Z


# instance fields
.field a:Z

.field private b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/View;

.field private g:Lcom/mfluent/asp/datamodel/Device;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Landroid/os/Bundle;

.field private n:Landroid/support/v4/content/LocalBroadcastManager;

.field private final o:Landroid/content/BroadcastReceiver;

.field private final p:Landroid/os/Handler;

.field private q:Ljava/lang/Runnable;

.field private final s:Landroid/content/BroadcastReceiver;

.field private final t:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 927
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->r:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 75
    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    .line 85
    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    .line 87
    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e:Ljava/util/ArrayList;

    .line 89
    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    .line 91
    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    .line 93
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h:Z

    .line 95
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->j:Z

    .line 98
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->k:Z

    .line 100
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    .line 102
    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->m:Landroid/os/Bundle;

    .line 491
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->o:Landroid/content/BroadcastReceiver;

    .line 601
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->p:Landroid/os/Handler;

    .line 929
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->s:Landroid/content/BroadcastReceiver;

    .line 978
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$3;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$3;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->t:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    .locals 3

    .prologue
    .line 464
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 465
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 466
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    .line 467
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 468
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 469
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 471
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 472
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 476
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Z)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    const-string v1, "storageType"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v1, "isUiAppTheme"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 121
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;-><init>()V

    .line 122
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 123
    return-object v1
.end method

.method public static a(Ljava/lang/String;ZZ)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 139
    const-string v1, "storageType"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v1, "isUiAppTheme"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 141
    const-string v1, "isAutoUpload"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 143
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;-><init>()V

    .line 144
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 145
    return-object v1
.end method

.method public static a(Z)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    const-string v1, "isUiAppTheme"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;-><init>()V

    .line 132
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 133
    return-object v1
.end method

.method public static a(ZZ)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 151
    const-string v1, "isUiAppTheme"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 152
    const-string v1, "isAutoUpload"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 154
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;-><init>()V

    .line 155
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 156
    return-object v1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 288
    const-string v0, "loading_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 289
    const-string v1, "network_dialog_status"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 290
    const-string v2, "auto_upload_status"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 292
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    .line 293
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->k:Z

    .line 294
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    .line 295
    return-void
.end method

.method private a(Lcom/mfluent/asp/b/h;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 695
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 766
    :goto_0
    return-void

    .line 699
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/util/r;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 700
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0a0042

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 704
    :cond_1
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 706
    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 707
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    .line 708
    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    .line 710
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v2, :cond_2

    .line 711
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 712
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    .line 713
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->n:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 740
    :goto_1
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e()V

    goto :goto_0

    .line 715
    :cond_2
    new-instance v1, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    .line 716
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->i(Ljava/lang/String;)V

    .line 717
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    .line 718
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 719
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 721
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$10;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$10;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v3, [Lcom/mfluent/asp/datamodel/Device;

    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$10;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 742
    :cond_3
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    if-eqz v1, :cond_4

    .line 743
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 744
    const-string v2, "target_sign_in_key"

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 745
    const-string v2, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_5

    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 746
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;->a(Landroid/content/Intent;)V

    .line 748
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->dismiss()V

    goto/16 :goto_0

    .line 745
    :cond_5
    const-string v4, "isUiAppTheme"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_2

    .line 752
    :cond_6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 753
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0040

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$11;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$11;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 763
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 764
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;I)V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.sync.CLOUD_SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->n:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Lcom/mfluent/asp/b/h;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/b/h;)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 652
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 656
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->P()V

    .line 660
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->k:Z

    .line 662
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 663
    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 664
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 665
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 666
    const v1, 0x7f0a0026

    new-instance v2, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$8;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$8;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 676
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$9;

    invoke-direct {v2, p0, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$9;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 685
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h:Z

    return v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 60
    sput-boolean p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->r:Z

    return p0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->r:Z

    return v0
.end method

.method private d()Lcom/mfluent/asp/b/h;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 436
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 437
    if-nez v1, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-object v0

    .line 440
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v2

    .line 441
    const-string v3, "storageType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 442
    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->k:Z

    return v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 606
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 607
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    .line 609
    const-string v0, "loading"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, " this.signInButtons.size() : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 611
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 612
    const-string v0, "loading"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setEnabled idx : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 611
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 617
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$a;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 622
    return-void
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->dismiss()V

    return-void
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    return v0
.end method

.method static synthetic h(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    return v0
.end method

.method static synthetic i(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic j(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const v3, 0x7f09002a

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 625
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->j:Z

    if-eqz v0, :cond_0

    .line 626
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->j:Z

    .line 627
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 634
    :goto_0
    return-void

    .line 629
    :cond_0
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->j:Z

    .line 630
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    .line 631
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 638
    invoke-super {p0}, Landroid/app/DialogFragment;->dismiss()V

    .line 640
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    .line 641
    return-void
.end method

.method public dismissAllowingStateLoss()V
    .locals 1

    .prologue
    .line 645
    invoke-super {p0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 648
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    .line 649
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 238
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 239
    instance-of v0, p1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    if-eqz v0, :cond_0

    .line 240
    check-cast p1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    return-void

    .line 242
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "StorageSignInSettingDialogFragment must be attached to activity that implements StorageSignInDialogFragListener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 528
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 531
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 534
    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    if-eqz v2, :cond_0

    .line 535
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    invoke-interface {v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;->a()V

    .line 538
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v2, :cond_1

    .line 539
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2, v1}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 542
    :cond_1
    if-eqz v0, :cond_2

    .line 543
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 544
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$7;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$7;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Landroid/content/Context;)V

    invoke-static {v1}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 553
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 531
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 164
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->n:Landroid/support/v4/content/LocalBroadcastManager;

    .line 165
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    const-string v3, "selected_device_key"

    new-array v4, v1, [Landroid/os/Bundle;

    aput-object p1, v4, v2

    array-length v5, v4

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_3

    aget-object v6, v4, v0

    if-eqz v6, :cond_2

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    if-lez v0, :cond_4

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    :goto_2
    const-string v0, "cloud_launch_broadcast_recd"

    new-array v3, v1, [Landroid/os/Bundle;

    aput-object p1, v3, v2

    invoke-static {v0, v2, v3}, Lcom/mfluent/asp/util/c;->a(Ljava/lang/String;Z[Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h:Z

    const-string v3, "com.mfluent.asp.ui.StorageSignInSettingDialogFragment.SAVED_INSTANCE_KEY_NEED_TO_START_REQUESTED_SIGN_IN"

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d()Lcom/mfluent/asp/b/h;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    new-array v1, v1, [Landroid/os/Bundle;

    aput-object p1, v1, v2

    invoke-static {v3, v0, v1}, Lcom/mfluent/asp/util/c;->a(Ljava/lang/String;Z[Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a:Z

    .line 172
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_7

    .line 173
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/b/g;->d()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d()Lcom/mfluent/asp/b/h;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v3, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$5;

    invoke-direct {v3, p0, v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$5;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Lcom/mfluent/asp/b/h;)V

    invoke-static {v0, v3}, Lorg/apache/commons/collections/CollectionUtils;->exists(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 170
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    iput-object v8, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    .line 173
    :cond_6
    sget-object v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_7

    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    const-string v1, "There are no cloud storage services available to show"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 178
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$1;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 196
    :cond_8
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_c

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    .line 199
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    if-eqz v0, :cond_9

    .line 200
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d:Ljava/util/ArrayList;

    new-instance v1, Lcom/mfluent/asp/b/h;

    invoke-direct {v1}, Lcom/mfluent/asp/b/h;-><init>()V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0350

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/b/h;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_9
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 204
    const v1, 0x7f030005

    invoke-virtual {v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    .line 205
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;

    invoke-direct {v1, p0, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;B)V

    .line 206
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    const v3, 0x7f090029

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 207
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 209
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    if-eqz v0, :cond_a

    .line 210
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 211
    if-eqz v0, :cond_a

    .line 212
    const v1, 0x7f0a0351

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 216
    :cond_a
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v3, 0x5

    invoke-direct {v0, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 217
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$4;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$4;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 226
    if-eqz p1, :cond_b

    .line 227
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Landroid/os/Bundle;)V

    .line 230
    :cond_b
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 231
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 233
    return-object v0

    .line 196
    :cond_c
    const-string v1, "isAutoUpload"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto/16 :goto_5
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 367
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 369
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->o:Landroid/content/BroadcastReceiver;

    invoke-static {v0, v1}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->b(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 371
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->t:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 373
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 374
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 351
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 353
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 362
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 321
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 323
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->o:Landroid/content/BroadcastReceiver;

    invoke-static {v0, v1}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 325
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->t:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_SUCCESS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 329
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->s:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_UNKNOWN"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 333
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    if-eqz v0, :cond_1

    .line 334
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->e()V

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->k:Z

    if-eqz v0, :cond_0

    .line 336
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 337
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 338
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 339
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b(Ljava/lang/String;)V

    .line 340
    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    const-string v1, " Show Progress Dialog Timeout!!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b(Ljava/lang/String;)V

    .line 343
    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    const-string v1, "Unexpected status!! Show Progress Dialog Timeout!!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 248
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 250
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->m:Landroid/os/Bundle;

    .line 253
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_0

    .line 254
    const-string v0, "selected_device_key"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 259
    :goto_0
    const-string v0, "cloud_launch_broadcast_recd"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 261
    const-string v0, "loading_status"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 262
    const-string v0, "network_dialog_status"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 263
    const-string v0, "com.mfluent.asp.ui.StorageSignInSettingDialogFragment.SAVED_INSTANCE_KEY_NEED_TO_START_REQUESTED_SIGN_IN"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 264
    const-string v0, "auto_upload_status"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 265
    return-void

    .line 256
    :cond_0
    const-string v0, "selected_device_key"

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 299
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 301
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->m:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->j:Z

    .line 304
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->m:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Landroid/os/Bundle;)V

    .line 307
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a()V

    .line 309
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a:Z

    if-eqz v0, :cond_1

    .line 310
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->d()Lcom/mfluent/asp/b/h;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_1

    .line 312
    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/b/h;)V

    .line 313
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a:Z

    .line 316
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 378
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 379
    return-void
.end method
