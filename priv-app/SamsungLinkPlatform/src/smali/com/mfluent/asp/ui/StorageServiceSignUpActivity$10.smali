.class final Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 901
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 906
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 907
    const-string v0, "mfl_StorageServiceSignUpActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StorageServiceSignUpActivity RX broadcast: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 953
    :cond_1
    :goto_0
    return-void

    .line 914
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 916
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 918
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 920
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 921
    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 922
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 923
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 924
    const v1, 0x7f0a0026

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10$1;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 950
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Z)Z

    .line 951
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
