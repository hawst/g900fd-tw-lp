.class public Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DeleteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteProgressDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 7

    .prologue
    const v6, 0x7f0a00ae

    const/16 v5, 0x2710

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 171
    check-cast p1, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    .line 172
    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 173
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->dismiss()V

    .line 175
    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->b(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 177
    invoke-virtual {p0, v6}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 178
    const-string v1, "{fileCount}"

    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 179
    const-string v1, "{file_name}"

    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 181
    invoke-static {}, Lcom/mfluent/asp/ui/dialog/b;->a()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Ljava/lang/CharSequence;)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->e(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->b(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I

    move-result v0

    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I

    move-result v1

    if-lt v0, v1, :cond_2

    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 187
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    const v2, 0x7f0a019d

    invoke-static {v0, v3, v2, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 189
    :cond_3
    invoke-virtual {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d()Lcom/mfluent/asp/media/j;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 190
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 191
    const-string v1, "EXTRA_DELETE_LIST"

    invoke-virtual {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d()Lcom/mfluent/asp/media/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/media/j;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 196
    :goto_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 194
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    .line 198
    :cond_5
    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->e(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 199
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->dismiss()V

    .line 200
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    .line 201
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 203
    :cond_6
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    .line 204
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->b(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I

    move-result v1

    .line 209
    invoke-static {p1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->f(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I

    move-result v2

    .line 211
    if-le v1, v5, :cond_7

    .line 212
    div-int v3, v5, v1

    int-to-float v3, v3

    .line 213
    int-to-float v2, v2

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 214
    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 216
    :cond_7
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 217
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto/16 :goto_0
.end method

.method protected final b()Lcom/mfluent/asp/ui/AsyncTaskFragment;
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;-><init>()V

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 165
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 166
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 167
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 142
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->setCancelable(Z)V

    .line 144
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 145
    const v1, 0x7f0a00af

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "indeterminate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 147
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 149
    return-object v0
.end method
