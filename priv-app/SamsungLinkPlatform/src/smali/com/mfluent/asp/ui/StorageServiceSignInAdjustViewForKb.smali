.class public Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->a:Landroid/app/Activity;

    .line 19
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->a:Landroid/app/Activity;

    .line 21
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 30
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 37
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->a:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 39
    const v0, 0x7f090061

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 41
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->a:Landroid/app/Activity;

    instance-of v1, v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    if-eqz v1, :cond_4

    const v1, 0x7f09008e

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 43
    :cond_0
    :goto_0
    if-eqz v0, :cond_b

    if-eqz v1, :cond_b

    .line 45
    invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 57
    :cond_1
    :goto_1
    return-void

    .line 41
    :cond_2
    const v1, 0x7f09008f

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const v1, 0x7f0900a3

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_5
    const v1, 0x7f0900a4

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_6
    const v1, 0x7f0900a5

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_7
    const v1, 0x7f0900a6

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_8
    const v1, 0x7f0900a7

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_9
    const v1, 0x7f0900a8

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_a
    const v1, 0x7f0900a9

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    goto/16 :goto_0

    .line 49
    :cond_b
    const v1, 0x7f090090

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInAdjustViewForKb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 51
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 53
    invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto/16 :goto_1
.end method
