.class public Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StartDocumentViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProgressTaskFragment"
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 264
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;-><init>()V

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->a:Ljava/util/List;

    .line 268
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->b:Ljava/lang/String;

    .line 270
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->c:Z

    .line 272
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->d:Z

    return v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->c:Z

    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->c:Z

    return v0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->d:Z

    return v0
.end method


# virtual methods
.method protected final a()Landroid/os/AsyncTask;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/os/Bundle;",
            "**>;"
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 278
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 280
    new-instance v2, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v2, p0, v3, v0, v1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;-><init>(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;Ljava/util/concurrent/TimeUnit;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/content/ContentResolver;)V

    return-object v2
.end method
