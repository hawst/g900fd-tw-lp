.class final Lcom/mfluent/asp/ui/FileTransferListFragment$c$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment$c;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/FileTransferListFragment$c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$1;->a:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    .line 298
    const v0, 0x7f090048

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 299
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 300
    if-nez v1, :cond_0

    .line 319
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 305
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 307
    const v1, 0x7f090049

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 308
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 309
    sub-int v4, p4, p2

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    sub-int v1, v4, v1

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v1, v4

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    .line 310
    div-int/lit8 v1, v1, 0x2

    .line 312
    if-le v3, v1, :cond_1

    .line 313
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    .line 314
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0

    .line 316
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setWidth(I)V

    .line 317
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0
.end method
