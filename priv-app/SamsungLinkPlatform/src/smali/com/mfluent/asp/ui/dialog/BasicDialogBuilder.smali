.class public Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;,
        Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;,
        Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;,
        Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

.field private final c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$1;-><init>(Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    .line 623
    return-void
.end method

.method static synthetic c()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_CUSTOM_VIEW_ID"

    const v2, 0x7f03000a

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125
    return-object p0
.end method

.method public final a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_MESSAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_MESSAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 136
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_MESSAGE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_MESSAGE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 148
    return-object p0
.end method

.method public final a(Z)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_CANCELABLE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 242
    return-object p0
.end method

.method public final varargs a([Ljava/lang/Object;)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_MESSAGE_ARGS"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 158
    return-object p0
.end method

.method public final a(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;->b()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;

    move-result-object v0

    .line 276
    iget-object v1, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 277
    invoke-virtual {v0, p1, p2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method public final a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 307
    return-void
.end method

.method public final a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 323
    if-eqz p4, :cond_0

    .line 324
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_EXTRAS"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 327
    :cond_0
    instance-of v0, p1, Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;->b()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;

    move-result-object v0

    .line 329
    iget-object v1, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 330
    check-cast p1, Landroid/app/Fragment;

    .line 331
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_SEND_RESULT_TO_FRAGMENT"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 332
    invoke-virtual {v0, p1, p2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 333
    invoke-virtual {p1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 355
    :goto_0
    return-void

    .line 334
    :cond_1
    instance-of v0, p1, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_2

    .line 335
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;->a()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->setArguments(Landroid/os/Bundle;)V

    .line 337
    check-cast p1, Landroid/support/v4/app/Fragment;

    .line 338
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_SEND_RESULT_TO_FRAGMENT"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 339
    invoke-virtual {v0, p1, p2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 340
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_2
    instance-of v0, p1, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_3

    .line 342
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;->a()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;

    move-result-object v0

    .line 343
    iget-object v1, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->setArguments(Landroid/os/Bundle;)V

    .line 344
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 345
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_SEND_RESULT_TO_ACTIVITY"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 346
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_RESULT_ACTIVITY_REQUEST_CODE"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 347
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 348
    :cond_3
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 349
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;

    invoke-interface {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$a;->b()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;

    move-result-object v0

    .line 350
    iget-object v1, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 351
    check-cast p1, Landroid/app/Activity;

    .line 352
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_SEND_RESULT_TO_ACTIVITY"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 353
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_RESULT_ACTIVITY_REQUEST_CODE"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 354
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 356
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must be either a Fragment or an Activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_TITLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 180
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_TITLE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 181
    return-object p0
.end method

.method public final b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_TITLE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 169
    return-object p0
.end method

.method public final c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_POSITIVE_BUTTON_ID"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    return-object p0
.end method

.method public final d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_NEGATIVE_BUTTON_ID"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    return-object p0
.end method

.method public final e(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_FINISH_ACTIVITY_ON_DISMISS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 262
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c:Landroid/os/Bundle;

    const-string v1, "ARG_FINISH_ACTIVITY_ON_DISMISS_RESULT_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 263
    return-object p0
.end method
