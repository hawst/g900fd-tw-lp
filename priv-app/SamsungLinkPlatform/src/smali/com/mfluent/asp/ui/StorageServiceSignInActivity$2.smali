.class final Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/b/h;

.field final synthetic b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/b/h;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    iput-object p2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->a:Lcom/mfluent/asp/b/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 360
    const-string v0, "mfl_StorageServiceSignInActivity"

    const-string v1, "::termsAndConditionsUri.onClick: exiting immediately. shouldIgnoreSignInOrSignUp() returned true."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a()V

    .line 367
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->a:Lcom/mfluent/asp/b/h;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sugarsync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    const-class v2, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    :goto_1
    const-string v1, "target_sign_in_key"

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->a:Lcom/mfluent/asp/b/h;

    invoke-virtual {v2}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->startActivity(Landroid/content/Intent;)V

    .line 374
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->finish()V

    goto :goto_0

    .line 370
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    const-class v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1
.end method
