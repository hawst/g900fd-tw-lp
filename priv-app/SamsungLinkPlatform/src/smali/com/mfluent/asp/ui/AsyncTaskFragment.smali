.class public abstract Lcom/mfluent/asp/ui/AsyncTaskFragment;
.super Landroid/app/Fragment;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/os/Bundle;",
            "**>;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Ljava/util/concurrent/locks/Lock;

.field private final e:Ljava/util/concurrent/locks/Condition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->d:Ljava/util/concurrent/locks/Lock;

    .line 29
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->e:Ljava/util/concurrent/locks/Condition;

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 160
    :try_start_0
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->c:Z

    .line 161
    if-eqz p1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 166
    return-void

    .line 165
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method protected abstract a()Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/os/Bundle;",
            "**>;"
        }
    .end annotation
.end method

.method public final a(Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Z)V

    .line 54
    if-eqz p1, :cond_0

    .line 55
    invoke-virtual {p1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 57
    :cond_0
    return-void
.end method

.method public final a(Lcom/mfluent/asp/ui/AsyncTaskWatcher;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 33
    instance-of v0, p1, Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 34
    check-cast p1, Landroid/app/Fragment;

    .line 35
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 36
    invoke-virtual {p1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 43
    :goto_0
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 44
    invoke-virtual {v0, p2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 49
    :cond_0
    invoke-virtual {v1, p0, p2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 50
    return-void

    .line 37
    :cond_1
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 38
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "watcher must be a Fragment or an Activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 146
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    invoke-virtual {v0, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    .line 151
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "restarting asyncTask {}"

    iget-object v2, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 153
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->c:Z

    if-nez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 188
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_1

    .line 190
    check-cast v0, Lcom/mfluent/asp/ui/AsyncTaskWatcher;

    .line 194
    :goto_1
    invoke-interface {v0, p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcher;->a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/AsyncTaskWatcher;

    goto :goto_1

    .line 196
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/ui/AsyncTaskFragment$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment$1;-><init>(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 81
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onActivityCreated"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 82
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 84
    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Z)V

    .line 86
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    if-nez v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    .line 88
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "starting asyncTask {}"

    iget-object v2, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 91
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onCreate"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 66
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->setRetainInstance(Z)V

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 114
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onDestroy"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Z)V

    .line 117
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "canceling asyncTask {}"

    iget-object v2, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 120
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 121
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onDetach"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Z)V

    .line 105
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 106
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onPause"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Z)V

    .line 128
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 129
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onResume"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 134
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Z)V

    .line 136
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 138
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->c()V

    .line 139
    return-void
.end method
