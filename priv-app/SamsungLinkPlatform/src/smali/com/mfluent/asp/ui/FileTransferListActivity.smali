.class public Lcom/mfluent/asp/ui/FileTransferListActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v13/app/FragmentTabHost;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 143
    new-array v0, v4, [[I

    new-array v1, v5, [I

    const v3, 0x10100a1

    aput v3, v1, v2

    aput-object v1, v0, v2

    new-array v1, v5, [I

    const v3, -0x10100a1

    aput v3, v1, v2

    aput-object v1, v0, v5

    .line 144
    new-array v1, v4, [I

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v1, v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060018

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v1, v5

    .line 145
    new-instance v3, Landroid/content/res/ColorStateList;

    invoke-direct {v3, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 148
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    if-nez v0, :cond_1

    .line 168
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 151
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    invoke-virtual {v0}, Landroid/support/v13/app/FragmentTabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    invoke-virtual {v0}, Landroid/support/v13/app/FragmentTabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f02004c

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 156
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    invoke-virtual {v0}, Landroid/support/v13/app/FragmentTabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 157
    add-int/lit16 v4, v1, 0x1389

    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    .line 158
    const v4, 0x1020016

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 161
    if-eqz v0, :cond_2

    .line 162
    const-string v4, "sans-serif-light"

    invoke-static {v4, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 163
    const/16 v4, 0x11

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 164
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 165
    const v4, 0x106000d

    invoke-virtual {v0, v6, v6, v6, v4}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 151
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 46
    const-string v0, "mfl_FileTransferListActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Intent : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mfluent/asp/common/util/IntentHelper;->intentToString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v0, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 50
    const-string v3, "isAutoUpload"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 53
    if-eqz v2, :cond_0

    move v0, v1

    .line 57
    :cond_0
    if-eqz v0, :cond_3

    .line 60
    sget-object v3, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x100

    invoke-virtual {v3, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 62
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 63
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    sget-object v5, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    xor-int/2addr v4, v5

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 64
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 66
    :cond_1
    const v3, 0x7f0b002d

    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/FileTransferListActivity;->setTheme(I)V

    .line 75
    :goto_0
    const v3, 0x7f0a029a

    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/FileTransferListActivity;->setTitle(I)V

    .line 77
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    .line 78
    invoke-virtual {v3, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 79
    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 80
    invoke-virtual {v3, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 81
    invoke-static {p0}, Lcom/mfluent/asp/util/UiUtils;->b(Landroid/app/Activity;)V

    .line 83
    if-eqz v0, :cond_6

    .line 84
    const v3, 0x7f03002c

    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/FileTransferListActivity;->setContentView(I)V

    .line 85
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 86
    const-string v4, "autoUpload"

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 87
    const-string v4, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 89
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string v5, "autoUpload"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 93
    const v0, 0x1020012

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v13/app/FragmentTabHost;

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    .line 94
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/support/v13/app/FragmentTabHost;->a(Landroid/content/Context;Landroid/app/FragmentManager;)V

    .line 96
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    const-string v5, "0"

    invoke-virtual {v1, v5}, Landroid/support/v13/app/FragmentTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a033a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const-class v5, Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0, v1, v5, v3}, Landroid/support/v13/app/FragmentTabHost;->a(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 100
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    const-string v3, "1"

    invoke-virtual {v1, v3}, Landroid/support/v13/app/FragmentTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0a0186

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const-class v3, Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0, v1, v3, v4}, Landroid/support/v13/app/FragmentTabHost;->a(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 105
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->a()V

    .line 108
    if-eqz v2, :cond_5

    .line 109
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/support/v13/app/FragmentTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 120
    :cond_2
    :goto_1
    return-void

    .line 68
    :cond_3
    sget-boolean v3, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v3, :cond_4

    .line 69
    const v3, 0x7f0b000d

    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/FileTransferListActivity;->setTheme(I)V

    goto/16 :goto_0

    .line 71
    :cond_4
    const v3, 0x7f0b000c

    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/FileTransferListActivity;->setTheme(I)V

    goto/16 :goto_0

    .line 111
    :cond_5
    if-eqz p1, :cond_2

    .line 112
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    const-string v1, "current_tab"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v13/app/FragmentTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    goto :goto_1

    .line 117
    :cond_6
    const v0, 0x7f03002b

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->setContentView(I)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 174
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListActivity;->finish()V

    .line 175
    const/4 v0, 0x1

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 125
    if-eqz p1, :cond_0

    .line 126
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    if-eqz v0, :cond_1

    .line 130
    const-string v0, "current_tab"

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListActivity;->a:Landroid/support/v13/app/FragmentTabHost;

    invoke-virtual {v1}, Landroid/support/v13/app/FragmentTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 139
    return-void
.end method
