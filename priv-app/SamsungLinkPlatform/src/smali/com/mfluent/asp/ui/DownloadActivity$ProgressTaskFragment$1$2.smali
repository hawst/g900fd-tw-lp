.class final Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iput-object p2, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 285
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v0, "media_type"

    aput-object v0, v2, v4

    const-string v3, "_data=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 290
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-object v5

    .line 294
    :cond_1
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 297
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 306
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 311
    :pswitch_0
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 319
    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto :goto_0

    .line 301
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MEDIA_TYPE column for file does not contain an integer value. path="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :pswitch_1
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 314
    :pswitch_2
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 306
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 259
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 261
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 262
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 268
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v1, v6, v4

    if-nez v1, :cond_2

    .line 269
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 270
    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v1, v1, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->b:Landroid/content/ContentResolver;

    invoke-static {v1, v3}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 272
    iget-object v4, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v4, v4, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->d(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    iget-object v3, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v3, v3, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-static {v3}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->c(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 281
    :goto_2
    return v0

    .line 265
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->a:Ljava/io/File;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 274
    goto :goto_1

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Z

    .line 278
    const/4 v0, 0x0

    goto :goto_2
.end method
