.class public Lcom/mfluent/asp/ui/NativePlayerStarter;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/mfluent/asp/ui/NativePlayerStarter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/mfluent/asp/ui/NativePlayerStarter;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/NativePlayerStarter;-><init>()V

    sput-object v0, Lcom/mfluent/asp/ui/NativePlayerStarter;->a:Lcom/mfluent/asp/ui/NativePlayerStarter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static a()Lcom/mfluent/asp/ui/NativePlayerStarter;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/mfluent/asp/ui/NativePlayerStarter;->a:Lcom/mfluent/asp/ui/NativePlayerStarter;

    return-object v0
.end method

.method public static a(Lcom/mfluent/asp/common/content/ContentAdapter;Landroid/app/Activity;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<*>;",
            "Landroid/app/Activity;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-interface {p0}, Lcom/mfluent/asp/common/content/ContentAdapter;->isEditing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Lcom/mfluent/asp/common/content/ContentAdapter;->getSingleSelectedRow()I

    move-result v0

    invoke-interface {p0, v0}, Lcom/mfluent/asp/common/content/ContentAdapter;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    const-string v0, "mfl_sdk_NativePlayerStarter"

    const-string v1, "::launchNativeSingleItemPlayer() return false"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 117
    :goto_0
    return v0

    .line 92
    :cond_0
    const-string v0, "media_type"

    invoke-static {p0, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getIntOrThrow(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 93
    const-string v1, "_id"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 94
    const-string v1, "device_id"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getIntOrThrow(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    .line 97
    packed-switch v0, :pswitch_data_0

    move v0, v2

    .line 108
    goto :goto_0

    .line 99
    :pswitch_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, v0

    .line 111
    :goto_1
    new-instance v0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;

    invoke-direct {v0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;-><init>()V

    .line 112
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->setUri(Landroid/net/Uri;)Z

    .line 114
    const-string v1, "_id = ?"

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->setSelection(Ljava/lang/String;)Z

    .line 115
    new-array v1, v3, [Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->setSelectionArgs([Ljava/lang/String;)Z

    .line 117
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v4, v6

    invoke-virtual {v1, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    const-string v1, "mfl_sdk_NativePlayerStarter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::launchNativeListPlayer() - sourceDevice:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " forResult is not used"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;

    new-instance v5, Landroid/content/Intent;

    const-string v1, "android.intent.action.START_SLINK_PLAYBACK"

    invoke-direct {v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v5, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    move v1, v3

    :goto_2
    if-nez v1, :cond_2

    move v0, v2

    goto :goto_0

    .line 102
    :pswitch_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, v0

    .line 103
    goto :goto_1

    .line 105
    :pswitch_2
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, v0

    .line 106
    goto :goto_1

    :cond_1
    move v1, v2

    .line 117
    goto :goto_2

    :cond_2
    const-string v1, "selection"

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getSelection()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "selectionArgs"

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "sortOrder"

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getSortOrder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "autoplay"

    invoke-virtual {v5, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->d()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "deviceId"

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v5, v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getMediaId()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v1, "selectedId"

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getMediaId()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "selectedIdColumn"

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getIdFieldName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    invoke-virtual {p1, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    move v0, v3

    goto/16 :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
