.class public Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;,
        Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;
    }
.end annotation


# static fields
.field private static c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static r:Z


# instance fields
.field protected a:Landroid/content/BroadcastReceiver;

.field protected b:Landroid/content/BroadcastReceiver;

.field private d:Lcom/mfluent/asp/datamodel/Device;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

.field private i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

.field private m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Landroid/view/inputmethod/InputMethodManager;

.field private final s:Landroid/content/BroadcastReceiver;

.field private final t:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 629
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->r:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 96
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n:Z

    .line 97
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->o:Z

    .line 98
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->p:Z

    .line 556
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a:Landroid/content/BroadcastReceiver;

    .line 580
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b:Landroid/content/BroadcastReceiver;

    .line 630
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->s:Landroid/content/BroadcastReceiver;

    .line 899
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$10;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$10;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->t:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;)Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    return-object p1
.end method

.method private a(ILcom/mfluent/asp/b/h;)V
    .locals 2

    .prologue
    .line 803
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.sync.CLOUD_SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 804
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 805
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 806
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Lcom/mfluent/asp/b/h;Z)V

    .line 807
    return-void
.end method

.method private a(Lcom/mfluent/asp/b/h;Z)V
    .locals 1

    .prologue
    .line 810
    invoke-static {p0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 811
    invoke-virtual {p1, p2}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 812
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    .line 813
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;ILcom/mfluent/asp/b/h;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(ILcom/mfluent/asp/b/h;)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/b/h;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Lcom/mfluent/asp/b/h;Z)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 60
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    const-string v2, "DEVICE_ID_EXTRA_KEY"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Z)Z
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->p:Z

    return p1
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 60
    sput-boolean p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->r:Z

    return p0
.end method

.method static synthetic b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const-string v0, "mfl_StorageServiceSignInActivity"

    const-string v1, "::onSignIn: exiting immediately. shouldIgnoreSignInOrSignUp() returned true."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d()V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "target_sign_in_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v1

    invoke-static {}, Lcom/mfluent/asp/util/r;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0042

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0029

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0a0026

    new-instance v3, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$8;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$8;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    :goto_1
    sget-object v4, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    if-ne v0, v4, :cond_8

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l()V

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4, v2}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageUserId(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(ILcom/mfluent/asp/b/h;)V

    goto/16 :goto_0

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->c:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    goto :goto_1

    :cond_7
    new-instance v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/datamodel/Device;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageUserId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/b/h;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Lcom/mfluent/asp/datamodel/Device;

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d()V

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    if-ne v0, v1, :cond_9

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h()V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->c:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    return-void
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->r:Z

    return v0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 451
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    .line 452
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    .line 453
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 456
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 457
    return-void
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e()V

    return-void
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 460
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0403

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 461
    return-void
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 509
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 513
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 518
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    return-void
.end method

.method static synthetic h(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f()V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 530
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 531
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 533
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 534
    return-void
.end method

.method static synthetic i(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d()V

    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 537
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 538
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 539
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 540
    return-void
.end method

.method static synthetic j(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Z
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->o()Z

    move-result v0

    return v0
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 686
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 688
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j()V

    .line 689
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->finish()V

    .line 718
    :cond_0
    :goto_0
    return-void

    .line 692
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 694
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$6;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$6;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$6;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic k(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m()V

    return-void
.end method

.method static synthetic l(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 822
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 823
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n:Z

    .line 824
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setFinishOnTouchOutside(Z)V

    .line 826
    const v0, 0x7f09009a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 828
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 831
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 832
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n:Z

    .line 833
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setFinishOnTouchOutside(Z)V

    .line 835
    const v0, 0x7f09009a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 837
    :cond_0
    return-void
.end method

.method static synthetic n(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->q:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 840
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 841
    const/4 v0, 0x1

    .line 843
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 925
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 926
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 927
    :goto_0
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->o:Z

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 926
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 935
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->o:Z

    .line 936
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 919
    invoke-static {p1}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 817
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 818
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k()V

    .line 819
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 427
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 428
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v3, 0x7f0a01d4

    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 103
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 106
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setFinishOnTouchOutside(Z)V

    .line 108
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->requestWindowFeature(I)Z

    .line 109
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setContentView(I)V

    .line 127
    :goto_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->q:Landroid/view/inputmethod/InputMethodManager;

    .line 129
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m()V

    .line 132
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    const-string v1, "target_sign_in_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-static {p0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 135
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v1

    .line 137
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 138
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139
    const v0, 0x7f090096

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 140
    const v0, 0x7f090099

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 141
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 144
    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$1;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    const v0, 0x7f09009a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 154
    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$11;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$11;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :goto_1
    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 181
    invoke-static {p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v2

    .line 182
    sget-object v3, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 186
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k:Landroid/view/View;

    if-nez v0, :cond_1

    .line 187
    :cond_0
    const v0, 0x7f090097

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k:Landroid/view/View;

    .line 188
    const v0, 0x7f090098

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j:Landroid/view/View;

    .line 191
    :cond_1
    const v0, 0x7f09008e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e:Landroid/widget/TextView;

    .line 192
    const v0, 0x7f09008f

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    .line 193
    const v0, 0x7f090091

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    .line 194
    const v0, 0x7f090092

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f:Landroid/widget/TextView;

    .line 195
    const v0, 0x7f090094

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    .line 198
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->t:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a(Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;)V

    .line 199
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->t:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a(Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;)V

    .line 201
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i()V

    .line 205
    if-nez p1, :cond_c

    .line 207
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 112
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 113
    if-eqz v0, :cond_3

    .line 114
    const v0, 0x7f0b002d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setTheme(I)V

    .line 124
    :goto_3
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 116
    :cond_3
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_4

    .line 117
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setTheme(I)V

    goto :goto_3

    .line 119
    :cond_4
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setTheme(I)V

    goto :goto_3

    .line 163
    :cond_5
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 166
    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 168
    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 170
    const/16 v0, 0x10

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 171
    const v0, 0x7f030026

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 174
    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0900bc

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j:Landroid/view/View;

    .line 175
    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0900bb

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k:Landroid/view/View;

    goto/16 :goto_1

    .line 207
    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :goto_4
    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 209
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 210
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    .line 216
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_9

    .line 217
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getWebStorageUserId()Ljava/lang/String;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 219
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 221
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getWebStoragePw()Ljava/lang/String;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 223
    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 227
    :cond_9
    iput-boolean v6, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->p:Z

    .line 257
    :goto_5
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$12;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$12;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 275
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$13;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$13;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 288
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$14;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$14;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 301
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$15;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$15;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 316
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->j:Landroid/view/View;

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$16;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$16;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k:Landroid/view/View;

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$17;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$17;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    const v0, 0x7f090095

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 337
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sugarsync"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 339
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a018c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v2

    .line 341
    new-instance v3, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$18;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$18;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v2, v3, v6, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 351
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 354
    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;

    invoke-direct {v2, p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$2;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/b/h;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 379
    :cond_a
    return-void

    .line 207
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 230
    :cond_c
    const-string v0, "credential_state_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 231
    const-string v0, "returned_error_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    .line 233
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d()V

    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    if-eqz v0, :cond_12

    .line 236
    :cond_d
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    if-ne v0, v2, :cond_f

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 243
    :goto_6
    const-string v0, "is_signing_in_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n:Z

    .line 244
    const-string v0, "storage_dev_id_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 245
    if-lez v0, :cond_e

    .line 246
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    .line 248
    :cond_e
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n:Z

    if-eqz v0, :cond_13

    .line 249
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l()V

    goto/16 :goto_5

    .line 236
    :cond_f
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    if-ne v0, v2, :cond_10

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g()V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    goto :goto_6

    :cond_10
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->c:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    if-ne v0, v2, :cond_11

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->h()V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    goto :goto_6

    :cond_11
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->i()V

    goto :goto_6

    .line 238
    :cond_12
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 239
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    goto :goto_6

    .line 251
    :cond_13
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m()V

    goto/16 :goto_5
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 404
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 407
    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->q:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->q:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    :cond_0
    :goto_0
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 415
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 416
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 418
    return-void

    .line 410
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 389
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 391
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_SUCCESS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 394
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_FAILURE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 397
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->s:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_UNKNOWN"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 400
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 432
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 433
    const-string v0, "credential_state_key"

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 434
    const-string v0, "returned_error_key"

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 436
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    .line 437
    const-string v0, "storage_dev_id_key"

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->d:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 438
    const-string v0, "is_signing_in_key"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 443
    :goto_0
    return-void

    .line 440
    :cond_0
    const-string v0, "storage_dev_id_key"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 441
    const-string v0, "is_signing_in_key"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 383
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 384
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 422
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 423
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4

    .prologue
    .line 882
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 884
    if-eqz p1, :cond_0

    .line 885
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->p:Z

    if-eqz v0, :cond_0

    .line 886
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$9;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$9;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 897
    :cond_0
    return-void
.end method
