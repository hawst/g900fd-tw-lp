.class public Lcom/mfluent/asp/ui/StorageTypeHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:Lcom/mfluent/asp/ui/StorageTypeHelper;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper;->c:Landroid/content/Context;

    .line 59
    return-void
.end method

.method private static a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 173
    .line 174
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 175
    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 178
    invoke-static {v0, p0, p1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 183
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :try_start_1
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 196
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 205
    :goto_0
    return-object v0

    .line 186
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 187
    :goto_1
    :try_start_3
    sget-object v3, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_0

    .line 188
    const-string v3, "mfl_StorageTypeHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getImageFromCache:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". Storage icon image not in the cache; will next attempt to retrieve from web."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 196
    :cond_0
    if-eqz v2, :cond_3

    .line 198
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-object v0, v1

    .line 201
    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 191
    :catch_2
    move-exception v0

    move-object v2, v1

    :goto_2
    :try_start_5
    sget-object v0, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v3, 0x6

    if-gt v0, v3, :cond_1

    .line 192
    const-string v0, "mfl_StorageTypeHelper"

    const-string v3, "::getImageFromCache:out of memory."

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 196
    :cond_1
    if-eqz v2, :cond_3

    .line 198
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-object v0, v1

    .line 201
    goto :goto_0

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_2

    .line 198
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 201
    :cond_2
    :goto_4
    throw v0

    :catch_4
    move-exception v1

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_4

    .line 196
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 191
    :catch_6
    move-exception v0

    goto :goto_2

    .line 186
    :catch_7
    move-exception v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x6

    .line 341
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :try_start_1
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 354
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 356
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 357
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 358
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 366
    :goto_0
    return-object v0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v3, :cond_0

    .line 347
    const-string v2, "mfl_StorageTypeHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getIconResourceFromWeb: MalformedURLException["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    move-object v0, v1

    .line 349
    goto :goto_0

    .line 361
    :catch_1
    move-exception v0

    .line 362
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v3, :cond_1

    .line 363
    const-string v2, "mfl_StorageTypeHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getIconResourceFromWeb: IOException: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    .line 366
    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;
    .locals 3

    .prologue
    .line 48
    const-class v1, Lcom/mfluent/asp/ui/StorageTypeHelper;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 49
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 51
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/ui/StorageTypeHelper;->b:Lcom/mfluent/asp/ui/StorageTypeHelper;

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Lcom/mfluent/asp/ui/StorageTypeHelper;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageTypeHelper;->b:Lcom/mfluent/asp/ui/StorageTypeHelper;

    .line 54
    :cond_1
    sget-object v0, Lcom/mfluent/asp/ui/StorageTypeHelper;->b:Lcom/mfluent/asp/ui/StorageTypeHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private static a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 334
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "storage_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".bmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 336
    return-object v0
.end method

.method protected static b()Z
    .locals 3

    .prologue
    .line 99
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 101
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v2, :cond_0

    .line 102
    const/4 v0, 0x1

    .line 105
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x6

    .line 263
    monitor-enter p0

    :try_start_0
    invoke-static {p2, p3}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 265
    if-nez p4, :cond_1

    if-eqz v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 269
    :cond_1
    :try_start_1
    sget-object v1, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_2

    .line 270
    const-string v1, "mfl_StorageTypeHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getWebStorageIcon:get icon: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "imageType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " storage type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageTypeHelper;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 274
    invoke-virtual {v1, p3}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v1

    .line 275
    if-nez v1, :cond_3

    .line 277
    const-string v0, "mfl_StorageTypeHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::getWebStorageIcon: StorageProviderInfo is null! imageType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " storage type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    goto :goto_0

    .line 282
    :cond_3
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->a:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    if-ne p2, v2, :cond_5

    .line 284
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->p()Ljava/lang/String;

    move-result-object v1

    .line 299
    :goto_1
    if-eqz v1, :cond_b

    .line 300
    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 308
    :cond_4
    :goto_2
    if-eqz v0, :cond_0

    .line 311
    invoke-static {p1, p2, p3}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 313
    :try_start_2
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 314
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 315
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 316
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 318
    :catch_0
    move-exception v1

    .line 319
    :try_start_3
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v4, :cond_0

    .line 320
    const-string v2, "mfl_StorageTypeHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getWebStorageIcon:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 285
    :cond_5
    :try_start_4
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->e:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    if-ne p2, v2, :cond_6

    .line 286
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->q()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 287
    :cond_6
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->f:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    if-ne p2, v2, :cond_7

    .line 288
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->r()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 289
    :cond_7
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->b:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    if-ne p2, v2, :cond_8

    .line 290
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->s()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 291
    :cond_8
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->c:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    if-ne p2, v2, :cond_9

    .line 292
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->t()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 293
    :cond_9
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    if-ne p2, v2, :cond_a

    .line 294
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->u()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 296
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown icon type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_b
    sget-object v1, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_4

    .line 304
    const-string v1, "mfl_StorageTypeHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getWebStorageIcon: Storage catalog does not define an icon for storage type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and imageType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 322
    :catch_1
    move-exception v1

    .line 323
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v4, :cond_0

    .line 324
    const-string v2, "mfl_StorageTypeHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getWebStorageIcon:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public final a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 6

    .prologue
    .line 209
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :cond_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getCacheDir()Ljava/io/File;

    move-result-object v4

    new-instance v0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/ui/StorageTypeHelper$1;-><init>(Lcom/mfluent/asp/ui/StorageTypeHelper;Ljava/lang/String;Landroid/widget/ImageView;Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 210
    :cond_1
    return-void
.end method

.method protected final a()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 78
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/mfluent/asp/b/g;->d()Ljava/util/List;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 82
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    .line 83
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 85
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v6, v7, :cond_1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_1
    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 95
    :goto_2
    return v0

    .line 85
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 93
    goto :goto_2

    :cond_6
    move v0, v1

    .line 95
    goto :goto_2
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->o()Ljava/lang/String;

    move-result-object v0

    .line 67
    const-string v1, "JP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/Device;)I
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a020f

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0a020e

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 372
    const-string v1, "ProviderListInitialized"

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/b/g;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
