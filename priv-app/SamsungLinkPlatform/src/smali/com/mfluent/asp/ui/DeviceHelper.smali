.class public Lcom/mfluent/asp/ui/DeviceHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/DeviceHelper$1;
    }
.end annotation


# direct methods
.method public static a(Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;Ljava/util/Set;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 21
    const v0, 0x101009e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 22
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->DARK:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 24
    sget-object v2, Lcom/mfluent/asp/ui/DeviceHelper$1;->a:[I

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 43
    if-eqz v0, :cond_13

    if-eqz v1, :cond_12

    const v0, 0x7f020036

    :goto_0
    return v0

    .line 26
    :pswitch_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    const v0, 0x7f020032

    goto :goto_0

    :cond_0
    const v0, 0x7f020033

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    const v0, 0x7f020030

    goto :goto_0

    :cond_2
    const v0, 0x7f020031

    goto :goto_0

    .line 28
    :pswitch_1
    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    const v0, 0x7f02003a

    goto :goto_0

    :cond_3
    const v0, 0x7f02003b

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_5

    const v0, 0x7f020038

    goto :goto_0

    :cond_5
    const v0, 0x7f020039

    goto :goto_0

    .line 30
    :pswitch_2
    if-eqz v0, :cond_7

    if-eqz v1, :cond_6

    const v0, 0x7f020026

    goto :goto_0

    :cond_6
    const v0, 0x7f020027

    goto :goto_0

    :cond_7
    if-eqz v1, :cond_8

    const v0, 0x7f020024

    goto :goto_0

    :cond_8
    const v0, 0x7f020025

    goto :goto_0

    .line 32
    :pswitch_3
    if-eqz v0, :cond_a

    if-eqz v1, :cond_9

    const v0, 0x7f02002a

    goto :goto_0

    :cond_9
    const v0, 0x7f02002b

    goto :goto_0

    :cond_a
    if-eqz v1, :cond_b

    const v0, 0x7f020028

    goto :goto_0

    :cond_b
    const v0, 0x7f020029

    goto :goto_0

    .line 34
    :pswitch_4
    if-eqz v0, :cond_d

    if-eqz v1, :cond_c

    const v0, 0x7f02002e

    goto :goto_0

    :cond_c
    const v0, 0x7f02002f

    goto :goto_0

    :cond_d
    if-eqz v1, :cond_e

    const v0, 0x7f02002c

    goto :goto_0

    :cond_e
    const v0, 0x7f02002d

    goto :goto_0

    .line 36
    :pswitch_5
    if-eqz v0, :cond_10

    if-eqz v1, :cond_f

    const v0, 0x7f02003e

    goto :goto_0

    :cond_f
    const v0, 0x7f02003f

    goto :goto_0

    :cond_10
    if-eqz v1, :cond_11

    const v0, 0x7f02003c

    goto :goto_0

    :cond_11
    const v0, 0x7f02003d

    goto/16 :goto_0

    .line 43
    :cond_12
    const v0, 0x7f020037

    goto/16 :goto_0

    :cond_13
    if-eqz v1, :cond_14

    const v0, 0x7f020034

    goto/16 :goto_0

    :cond_14
    const v0, 0x7f020035

    goto/16 :goto_0

    .line 24
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;Z)I
    .locals 2

    .prologue
    .line 48
    if-nez p0, :cond_0

    .line 50
    const v0, 0x7f020114

    .line 81
    :goto_0
    return v0

    .line 53
    :cond_0
    sget-object v0, Lcom/mfluent/asp/ui/DeviceHelper$1;->a:[I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 78
    :pswitch_0
    if-eqz p1, :cond_8

    const v0, 0x7f020135

    goto :goto_0

    .line 55
    :pswitch_1
    if-eqz p1, :cond_1

    const v0, 0x7f02012d

    goto :goto_0

    :cond_1
    const v0, 0x7f02012e

    goto :goto_0

    .line 58
    :pswitch_2
    if-eqz p1, :cond_2

    const v0, 0x7f020131

    goto :goto_0

    :cond_2
    const v0, 0x7f020132

    goto :goto_0

    .line 61
    :pswitch_3
    if-eqz p1, :cond_3

    const v0, 0x7f020123

    goto :goto_0

    :cond_3
    const v0, 0x7f020124

    goto :goto_0

    .line 64
    :pswitch_4
    if-eqz p1, :cond_4

    const v0, 0x7f020125

    goto :goto_0

    :cond_4
    const v0, 0x7f020126

    goto :goto_0

    .line 67
    :pswitch_5
    if-eqz p1, :cond_5

    const v0, 0x7f020129

    goto :goto_0

    :cond_5
    const v0, 0x7f02012a

    goto :goto_0

    .line 70
    :pswitch_6
    if-eqz p1, :cond_6

    const v0, 0x7f020133

    goto :goto_0

    :cond_6
    const v0, 0x7f020134

    goto :goto_0

    .line 74
    :pswitch_7
    if-eqz p1, :cond_7

    const v0, 0x7f02012f

    goto :goto_0

    :cond_7
    const v0, 0x7f020130

    goto :goto_0

    .line 78
    :cond_8
    const v0, 0x7f020136

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;ZZZ)I
    .locals 4

    .prologue
    const v1, 0x7f020130

    const v0, 0x7f02012f

    .line 85
    .line 86
    if-nez p2, :cond_0

    if-eqz p3, :cond_1e

    :cond_0
    move v2, v0

    .line 90
    :goto_0
    if-nez p0, :cond_2

    move v0, v2

    .line 148
    :cond_1
    :goto_1
    return v0

    .line 94
    :cond_2
    sget-object v2, Lcom/mfluent/asp/ui/DeviceHelper$1;->a:[I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 141
    if-nez p2, :cond_3

    if-eqz p3, :cond_1c

    .line 142
    :cond_3
    if-nez p1, :cond_1

    move v0, v1

    goto :goto_1

    .line 96
    :pswitch_0
    if-nez p2, :cond_4

    if-eqz p3, :cond_6

    .line 97
    :cond_4
    if-eqz p1, :cond_5

    const v0, 0x7f02012d

    goto :goto_1

    :cond_5
    const v0, 0x7f02012e

    goto :goto_1

    .line 99
    :cond_6
    if-eqz p1, :cond_7

    const v0, 0x7f0200d1

    goto :goto_1

    :cond_7
    const v0, 0x7f0200d0

    goto :goto_1

    .line 103
    :pswitch_1
    if-nez p2, :cond_8

    if-eqz p3, :cond_a

    .line 104
    :cond_8
    if-eqz p1, :cond_9

    const v0, 0x7f020131

    goto :goto_1

    :cond_9
    const v0, 0x7f020132

    goto :goto_1

    .line 106
    :cond_a
    if-eqz p1, :cond_b

    const v0, 0x7f0200d5

    goto :goto_1

    :cond_b
    const v0, 0x7f0200d4

    goto :goto_1

    .line 110
    :pswitch_2
    if-nez p2, :cond_c

    if-eqz p3, :cond_e

    .line 111
    :cond_c
    if-eqz p1, :cond_d

    const v0, 0x7f020123

    goto :goto_1

    :cond_d
    const v0, 0x7f020124

    goto :goto_1

    .line 113
    :cond_e
    if-eqz p1, :cond_f

    const v0, 0x7f0200c9

    goto :goto_1

    :cond_f
    const v0, 0x7f0200c8

    goto :goto_1

    .line 117
    :pswitch_3
    if-nez p2, :cond_10

    if-eqz p3, :cond_12

    .line 118
    :cond_10
    if-eqz p1, :cond_11

    const v0, 0x7f020125

    goto :goto_1

    :cond_11
    const v0, 0x7f020126

    goto :goto_1

    .line 120
    :cond_12
    if-eqz p1, :cond_13

    const v0, 0x7f0200cb

    goto :goto_1

    :cond_13
    const v0, 0x7f0200ca

    goto :goto_1

    .line 124
    :pswitch_4
    if-nez p2, :cond_14

    if-eqz p3, :cond_16

    .line 125
    :cond_14
    if-eqz p1, :cond_15

    const v0, 0x7f020129

    goto :goto_1

    :cond_15
    const v0, 0x7f02012a

    goto/16 :goto_1

    .line 127
    :cond_16
    if-eqz p1, :cond_17

    const v0, 0x7f0200cd

    goto/16 :goto_1

    :cond_17
    const v0, 0x7f0200cc

    goto/16 :goto_1

    .line 131
    :pswitch_5
    if-nez p2, :cond_18

    if-eqz p3, :cond_1a

    .line 132
    :cond_18
    if-eqz p1, :cond_19

    const v0, 0x7f020133

    goto/16 :goto_1

    :cond_19
    const v0, 0x7f020134

    goto/16 :goto_1

    .line 134
    :cond_1a
    if-eqz p1, :cond_1b

    const v0, 0x7f0200d7

    goto/16 :goto_1

    :cond_1b
    const v0, 0x7f0200d6

    goto/16 :goto_1

    .line 144
    :cond_1c
    if-eqz p1, :cond_1d

    const v0, 0x7f0200d3

    goto/16 :goto_1

    :cond_1d
    const v0, 0x7f0200d2

    goto/16 :goto_1

    :cond_1e
    move v2, v1

    goto/16 :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 154
    sget-object v1, Lcom/mfluent/asp/ui/DeviceHelper$1;->b:[I

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 188
    const-string v0, "UNKNOWN"

    .line 190
    :goto_0
    return-object v0

    .line 156
    :pswitch_0
    const-string v1, "slpf_pref_20"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 157
    const-string v1, "chinaCSC"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 158
    const-string v3, "chinaCSC"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    .line 160
    if-nez v2, :cond_2

    .line 161
    invoke-static {}, Lcom/sec/pcw/service/d/b;->b()Ljava/lang/String;

    move-result-object v1

    .line 163
    const-string v2, "cn"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 169
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 170
    const v0, 0x7f0a041a

    .line 190
    :goto_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172
    :cond_1
    const v0, 0x7f0a0419

    .line 174
    goto :goto_2

    .line 176
    :pswitch_1
    const v0, 0x7f0a041b

    .line 177
    goto :goto_2

    .line 179
    :pswitch_2
    const v0, 0x7f0a041c

    .line 180
    goto :goto_2

    .line 182
    :pswitch_3
    const v0, 0x7f0a041d

    .line 183
    goto :goto_2

    .line 185
    :pswitch_4
    const v0, 0x7f0a041e

    .line 186
    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)I
    .locals 4

    .prologue
    const v0, 0x7f020045

    const/4 v1, 0x0

    .line 194
    sget-object v2, Lcom/mfluent/asp/ui/DeviceHelper$1;->b:[I

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 237
    :goto_0
    :pswitch_0
    return v0

    .line 198
    :pswitch_1
    const-string v0, "slpf_pref_20"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 199
    const-string v0, "chinaCSC"

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 200
    const-string v3, "chinaCSC"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    .line 202
    if-nez v2, :cond_2

    .line 203
    invoke-static {}, Lcom/sec/pcw/service/d/b;->b()Ljava/lang/String;

    move-result-object v0

    .line 205
    const-string v2, "cn"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    const/4 v1, 0x1

    .line 211
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 212
    const v0, 0x7f020048

    goto :goto_0

    .line 214
    :cond_1
    const v0, 0x7f020047

    .line 216
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 228
    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1

    .line 194
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
