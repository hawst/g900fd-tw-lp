.class final Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 561
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0, p2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    const-string v0, "mfl_StorageServiceSignInActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignIn authorization success result received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    .line 566
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 569
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 570
    if-eqz v0, :cond_0

    .line 571
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 572
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->setResult(ILandroid/content/Intent;)V

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$3;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->finish()V

    .line 577
    :cond_1
    return-void
.end method
