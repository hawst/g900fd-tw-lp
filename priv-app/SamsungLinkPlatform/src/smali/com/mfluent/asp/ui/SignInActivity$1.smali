.class final Lcom/mfluent/asp/ui/SignInActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/SignInActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SignInActivity;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/mfluent/asp/ui/SignInActivity;->c()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::onReceive: {}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 56
    const-string v1, "com.mfluent.asp.AccessManager.BROADCAST_SIGN_IN_RESULT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SignInActivity;->a()V

    .line 58
    const-string v0, "com.mfluent.asp.AccessManager.EXTRA_SIGN_IN_RESULT"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 59
    packed-switch v0, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected sign in result from AccessManager"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SignInActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SignInActivity;->finish()V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/ui/SignInActivity;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Received Sign-in success broadcast, but we\'re not signed in!?!?"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SignInActivity;->a(Lcom/mfluent/asp/ui/SignInActivity;)V

    goto :goto_0

    .line 71
    :pswitch_1
    invoke-static {}, Lcom/mfluent/asp/ui/SignInActivity;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Received Sign-in EXTRA_SIGN_IN_RESULT_FAILED broadcast.."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SignInActivity;->a(Lcom/mfluent/asp/ui/SignInActivity;)V

    goto :goto_0

    .line 76
    :pswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$1;->a:Lcom/mfluent/asp/ui/SignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SignInActivity;->b(Lcom/mfluent/asp/ui/SignInActivity;)V

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
