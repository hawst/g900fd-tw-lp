.class final Lcom/mfluent/asp/ui/SendToActivity$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic b:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

.field final synthetic c:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

.field final synthetic d:Lcom/mfluent/asp/ui/SendToActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 0

    .prologue
    .line 1325
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->d:Lcom/mfluent/asp/ui/SendToActivity;

    iput-object p2, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->a:Lcom/mfluent/asp/datamodel/Device;

    iput-object p3, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->b:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    iput-object p4, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->c:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1329
    invoke-static {}, Lcom/mfluent/asp/ui/SendToActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::proceedWithSendTo(): targetDevice:{} finalMediaSet:{}"

    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->a:Lcom/mfluent/asp/datamodel/Device;

    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->b:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1330
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->d:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->b:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->a:Lcom/mfluent/asp/datamodel/Device;

    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity$4;->c:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-interface {v0, v1, v2, v3}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1331
    return-void
.end method
