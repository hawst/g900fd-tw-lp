.class public Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskFragment;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/media/l;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DeleteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteTaskFragment"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Lcom/mfluent/asp/media/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/mfluent/asp/media/j;)Lcom/mfluent/asp/media/j;
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->g:Lcom/mfluent/asp/media/j;

    return-object p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d:I

    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->b:I

    return v0
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->f:Z

    return v0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c:I

    return v0
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Lcom/mfluent/asp/media/j;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->g:Lcom/mfluent/asp/media/j;

    return-object v0
.end method


# virtual methods
.method protected final a()Landroid/os/AsyncTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/os/Bundle;",
            "**>;"
        }
    .end annotation

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DeleteActivity;

    .line 246
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DeleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 248
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromIntent(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 250
    new-instance v2, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;-><init>(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/content/Context;)V

    return-object v2
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 381
    iput p1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d:I

    .line 382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->e:Z

    .line 383
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c()V

    .line 384
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 374
    iput p1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c:I

    .line 375
    iput p2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->d:I

    .line 376
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c()V

    .line 377
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a:Ljava/lang/String;

    .line 368
    iput p2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->b:I

    .line 369
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c()V

    .line 370
    return-void
.end method

.method public final d()Lcom/mfluent/asp/media/j;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->g:Lcom/mfluent/asp/media/j;

    return-object v0
.end method

.method public final g_()V
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->f:Z

    .line 389
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->c()V

    .line 390
    return-void
.end method
