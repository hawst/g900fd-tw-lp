.class final Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 73
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    const-string v1, "/api/pCloud/device/account/getPasswordReset"

    invoke-static {v0, v1}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 76
    const-string v1, "mfl_HomeSyncVaultLoadingActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getPasswordReset result : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v1, "passwordReset"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v1, "mfl_HomeSyncVaultLoadingActivity"

    const-string v2, "Trouble checking if a password exists on the homeSync server"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 69
    check-cast p1, Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V

    goto :goto_0
.end method
