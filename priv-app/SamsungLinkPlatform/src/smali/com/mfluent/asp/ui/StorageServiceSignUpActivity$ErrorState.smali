.class final enum Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ErrorState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum b:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum c:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum e:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum f:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum h:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum i:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum j:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum k:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field public static final enum l:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field private static final synthetic m:[Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 83
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "NO_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 84
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "NO_EMAIL_ENTERED"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->b:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 85
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "USER_ALREADY_EXISTS"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->c:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 86
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "GENERAL_SIGN_IN_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 87
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "INVALID_EMAIL"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->e:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 88
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "INVALID_EMAIL_DOMAIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->f:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 89
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "ACCT_NOT_ACTIVATED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 90
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "ACCT_ALREADY_REGISTERED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->h:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 91
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "NO_PASSWORD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->i:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 92
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "INVALID_PW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->j:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 93
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "PW_NOT_MATCHED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->k:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 94
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    const-string v1, "TERMS_NOT_AGREED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->l:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 82
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->b:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->c:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->e:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->f:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->h:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->i:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->j:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->k:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->l:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->m:[Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->m:[Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-virtual {v0}, [Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    return-object v0
.end method
