.class final Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V
    .locals 1

    .prologue
    .line 334
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 325
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->b:I

    .line 326
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->c:I

    .line 327
    const/4 v0, 0x2

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->d:I

    .line 328
    const/4 v0, 0x3

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->e:I

    .line 329
    const/4 v0, 0x4

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->f:I

    .line 330
    const/4 v0, 0x5

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->g:I

    .line 331
    const/4 v0, 0x6

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->h:I

    .line 332
    const/4 v0, 0x7

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->i:I

    .line 335
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->a:Ljava/lang/ref/WeakReference;

    .line 336
    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;B)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x7f0a027f

    const v6, 0x7f0a026b

    const/4 v5, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 340
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received message from SecureStorageManagerClient: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    .line 351
    if-nez v0, :cond_0

    .line 352
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Unable to continue with login procedure because activity is gone"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a()V

    .line 357
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 409
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    .line 410
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v7, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 359
    :pswitch_0
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Password created successfully - logged in"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 361
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    .line 362
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->finish()V

    goto :goto_0

    .line 365
    :pswitch_1
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Failed to process password"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    .line 368
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "P5200"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 369
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v4, v6, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;II[Ljava/lang/Object;)V

    .line 371
    :cond_1
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v7, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 374
    :pswitch_2
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Password recovery complete"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 377
    :pswitch_3
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Password recovery fail"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v4, v6, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 382
    :pswitch_4
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "HomeSync is busy"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    .line 384
    const v1, 0x7f0a0280

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 387
    :pswitch_5
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Password changed successfully - logged in"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 389
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    .line 390
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->finish()V

    goto/16 :goto_0

    .line 393
    :pswitch_6
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Failed to change password"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->d(I)V

    .line 395
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v1

    if-gtz v1, :cond_2

    .line 396
    const v1, 0x7f0a0281

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v4, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;II[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 399
    :cond_2
    const v1, 0x7f0a038e

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 405
    :pswitch_7
    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    .line 406
    const v1, 0x7f0a025f

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
