.class public final Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;
.super Landroid/support/v4/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 623
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 625
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;-><init>(B)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    .line 644
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 646
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getTargetRequestCode()I

    move-result v4

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$c;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;->a(Landroid/content/DialogInterface;Landroid/os/Bundle;Ljava/lang/Object;ILandroid/app/Activity;)V

    .line 647
    return-void
.end method

.method public final show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 629
    invoke-virtual {p1, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 630
    invoke-static {}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Ignoring call to show dialog because a fragment with tag {} already exists"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 635
    :goto_0
    return-void

    .line 634
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
