.class public abstract Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/AsyncTaskWatcher;


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Lcom/mfluent/asp/ui/AsyncTaskFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/mfluent/asp/ui/AsyncTaskFragment;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    return-object v0
.end method

.method protected abstract b()Lcom/mfluent/asp/ui/AsyncTaskFragment;
.end method

.method public dismiss()V
    .locals 3

    .prologue
    .line 49
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "dismiss"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    if-eqz v0, :cond_0

    .line 51
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "removing AsyncTaskFragment {} because the DialogFragment is dismissed"

    iget-object v2, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    .line 56
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->dismiss()V

    .line 57
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 20
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onActivityCreated"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 21
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 26
    const-string v1, "asyncTaskFragment"

    .line 27
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/AsyncTaskFragment;

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    .line 30
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b()Lcom/mfluent/asp/ui/AsyncTaskFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    .line 32
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    const-string v2, "starting AsyncTaskFragment {}"

    iget-object v3, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-interface {v0, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-virtual {v0, p0, v1}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Lcom/mfluent/asp/ui/AsyncTaskWatcher;Ljava/lang/String;)V

    .line 35
    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "onCancel"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->a:Lorg/slf4j/Logger;

    const-string v1, "removing AsyncTaskFragment {} because the Dialog is canceled"

    iget-object v2, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->b:Lcom/mfluent/asp/ui/AsyncTaskFragment;

    .line 45
    :cond_0
    return-void
.end method
