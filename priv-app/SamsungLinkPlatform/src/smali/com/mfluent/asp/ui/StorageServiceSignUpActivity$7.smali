.class final Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 723
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 728
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->n(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 732
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z

    .line 733
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0, v3}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z

    .line 735
    const-string v0, "mfl_StorageServiceSignUpActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUp failure result received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SIGNUP_RESULT_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;

    .line 738
    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$17;->a:[I

    invoke-virtual {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper$SignupResults;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 752
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;)Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 753
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 759
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 760
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 761
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->f(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 762
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->o(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 763
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0, v3}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z

    .line 765
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->p(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 793
    return-void

    .line 740
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->c:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;)Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    goto :goto_0

    .line 744
    :pswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->e:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;)Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    goto :goto_0

    .line 748
    :pswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;)Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    goto :goto_0

    .line 738
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
