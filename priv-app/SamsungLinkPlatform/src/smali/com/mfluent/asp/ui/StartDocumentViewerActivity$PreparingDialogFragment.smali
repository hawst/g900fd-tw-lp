.class public Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StartDocumentViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PreparingDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const v4, 0x7f0a0166

    const/4 v3, 0x0

    .line 220
    check-cast p1, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    .line 222
    invoke-static {p1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->dismiss()V

    .line 225
    invoke-static {p1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->b(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    new-instance v1, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    .line 227
    const v0, 0x1040014

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 228
    invoke-virtual {v1, v4}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 229
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->c(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a([Ljava/lang/Object;)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 230
    const v0, 0x7f0a0026

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 232
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v4, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    invoke-static {p1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->d(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 240
    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->d(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity;Ljava/io/File;)V

    goto :goto_0

    .line 243
    :cond_2
    invoke-static {}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Expected one local path in modal download result"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    .line 245
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected final b()Lcom/mfluent/asp/ui/AsyncTaskFragment;
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;-><init>()V

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 259
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 260
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 261
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 213
    const v1, 0x7f0a03c1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 214
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 215
    return-object v0
.end method
