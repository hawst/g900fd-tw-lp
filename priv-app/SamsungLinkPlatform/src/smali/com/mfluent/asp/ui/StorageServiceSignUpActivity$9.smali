.class final Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 860
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 865
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0, p2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 867
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 868
    const-string v0, "mfl_StorageServiceSignUpActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignIn authorization failure result received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.mfluent.asp.sync.SIGNIN_ERROR_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    .line 873
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 874
    const-string v1, "mfl_StorageServiceSignUpActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::LoginFailed because:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->n(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 884
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 886
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z

    .line 887
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0, v4}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z

    .line 889
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->r(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sugarsync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 890
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 895
    :cond_2
    :goto_0
    return-void

    .line 892
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->finish()V

    goto :goto_0
.end method
