.class public Lcom/mfluent/asp/ui/AddWebStorageActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;
.implements Lcom/mfluent/asp/ui/dialog/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    .line 119
    return-void
.end method

.method public final a(IILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 88
    const v0, 0x7f0a01f1

    if-ne p1, v0, :cond_0

    .line 89
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.DATE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x898

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 128
    const/16 v0, 0x89a

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 129
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    .line 124
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 133
    const/16 v0, 0x899

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 134
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    .line 139
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 101
    packed-switch p1, :pswitch_data_0

    .line 114
    :goto_0
    return-void

    .line 103
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    goto :goto_0

    .line 107
    :pswitch_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    goto :goto_0

    .line 111
    :pswitch_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->finish()V

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x898
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f0a01f1

    const/4 v5, 0x0

    .line 29
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    if-eqz p1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/util/r;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0042

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkStorageUtils.EXTRA_STORAGE_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 44
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkStorageUtils.IS_AUTOUPLOAD_EXTRA_KEY"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 46
    if-eqz v0, :cond_3

    .line 47
    if-eqz v2, :cond_2

    .line 48
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Ljava/lang/String;ZZ)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_2
    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Ljava/lang/String;Z)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_3
    invoke-static {p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v0

    .line 57
    invoke-static {p0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v3

    .line 58
    invoke-virtual {v3}, Lcom/mfluent/asp/b/g;->b()Z

    move-result v3

    .line 60
    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 63
    if-eqz v2, :cond_4

    .line 64
    invoke-static {v1, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(ZZ)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_4
    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Z)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 68
    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->c()Z

    move-result v0

    if-nez v0, :cond_7

    .line 71
    :cond_6
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0072

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 72
    :cond_7
    invoke-static {}, Lcom/mfluent/asp/ui/StorageTypeHelper;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 73
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AddWebStorageActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a01d8

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 75
    :cond_8
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0145

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v6, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    goto/16 :goto_0
.end method
