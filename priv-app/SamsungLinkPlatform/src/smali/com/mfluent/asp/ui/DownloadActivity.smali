.class public Lcom/mfluent/asp/ui/DownloadActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/DownloadActivity$1;,
        Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;,
        Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;,
        Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;,
        Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/mfluent/asp/ui/DownloadActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 516
    return-void
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/mfluent/asp/ui/DownloadActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->finish()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity;Ljava/util/List;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 69
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/os/Parcelable;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    const-string v2, "uris"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const-string v2, "paths"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    iput-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity;->b:Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/DownloadActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->finish()V

    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/DownloadActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromIntent(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mediaSet"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "rowIds"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "broadcastResults"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.DOWNLOAD_RESULT_BROADCAST_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 102
    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity;->b:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 105
    :cond_0
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 108
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 109
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 80
    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    const v0, 0x7f0b0036

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity;->setTheme(I)V

    .line 91
    :goto_0
    if-eqz p1, :cond_2

    .line 96
    :goto_1
    return-void

    .line 84
    :cond_0
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 85
    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity;->setTheme(I)V

    goto :goto_0

    .line 87
    :cond_1
    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity;->setTheme(I)V

    goto :goto_0

    .line 95
    :cond_2
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "preparing"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method
