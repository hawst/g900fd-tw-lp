.class public Lcom/mfluent/asp/ui/DebugLogLevelActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;,
        Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;,
        Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    .line 377
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DebugLogLevelActivity;)Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->notifyDataSetInvalidated()V

    .line 108
    invoke-static {p0}, Lcom/mfluent/asp/common/util/AspLogLevels;->saveLogLevels(Landroid/content/Context;)V

    .line 110
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 111
    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 116
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 117
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 118
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 120
    new-instance v3, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;

    invoke-direct {v3, v1, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    new-instance v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$2;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$2;-><init>(Lcom/mfluent/asp/ui/DebugLogLevelActivity;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 139
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method static synthetic a(Landroid/widget/TextView;I)V
    .locals 2

    .prologue
    .line 40
    packed-switch p1, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_0
    const-string v0, "VERBOSE"

    goto :goto_0

    :pswitch_1
    const-string v0, "DEBUG"

    goto :goto_0

    :pswitch_2
    const-string v0, "INFO"

    goto :goto_0

    :pswitch_3
    const-string v0, "WARN"

    goto :goto_0

    :pswitch_4
    const-string v0, "ERROR"

    goto :goto_0

    :pswitch_5
    const-string v0, "ASSERT"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/DebugLogLevelActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->setContentView(I)V

    .line 58
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->a:Landroid/widget/ListView;

    .line 59
    new-instance v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

    invoke-direct {v0, p0, p0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;-><init>(Lcom/mfluent/asp/ui/DebugLogLevelActivity;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    if-eqz v0, :cond_0

    .line 64
    const-string v1, "-- Log Level --"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    :cond_0
    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    if-eqz v0, :cond_1

    .line 68
    const-string v1, "-- ALL --"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :cond_1
    const v0, 0x7f090025

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 71
    if-eqz v0, :cond_2

    .line 72
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 73
    new-instance v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$1;-><init>(Lcom/mfluent/asp/ui/DebugLogLevelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 101
    :cond_2
    invoke-direct {p0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->a()V

    .line 102
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 157
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 158
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 169
    :goto_0
    return v0

    .line 160
    :pswitch_0
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;

    iget-object v4, v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    instance-of v4, v4, Ljava/lang/Integer;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b()I

    move-result v0

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v4, v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    instance-of v4, v4, Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->c()Z

    move-result v0

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    :cond_2
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "../shared_prefs"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-static {v3, v0}, Lorg/apache/commons/io/FileUtils;->copyFileToDirectory(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-static {p0}, Lcom/mfluent/asp/common/util/AspLogLevels;->loadLogLevels(Landroid/content/Context;)V

    .line 161
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Saved "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " items. NOTE: some settings require restarting app"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    .line 162
    goto/16 :goto_0

    .line 160
    :catch_0
    move-exception v1

    const-string v4, "mfl_DebugLogLevelActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IOException: copyFileToDirectory: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " to "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 164
    :pswitch_1
    invoke-direct {p0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->a()V

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Restored "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " items"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    .line 166
    goto/16 :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 145
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 147
    const-string v0, "Save"

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 149
    const/4 v0, 0x2

    const-string v1, "Restore defaults"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 151
    return v3
.end method
