.class final Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;
.super Landroid/database/ContentObserver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a()Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 677
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onChange(ZLandroid/net/Uri;)V
    .locals 3

    .prologue
    .line 682
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 688
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->e:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    if-eq v0, v1, :cond_0

    .line 693
    :goto_0
    return-void

    .line 683
    :catch_0
    move-exception v0

    .line 684
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Interrupted while waiting for initialization to complete"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 692
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    goto :goto_0
.end method
