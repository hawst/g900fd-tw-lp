.class public Lcom/mfluent/asp/ui/ImageWorkerView;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/bitmap/ImageWorker$a;


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:Landroid/graphics/Point;

.field private e:Ljava/lang/Integer;

.field private f:Lcom/mfluent/asp/util/bitmap/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->a:Z

    .line 24
    iput v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->b:I

    .line 25
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->c:Z

    .line 26
    iput-object v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->d:Landroid/graphics/Point;

    .line 28
    iput-object v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->a:Z

    .line 24
    iput v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->b:I

    .line 25
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->c:Z

    .line 26
    iput-object v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->d:Landroid/graphics/Point;

    .line 28
    iput-object v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    .line 40
    return-void
.end method

.method private a(Lcom/mfluent/asp/util/bitmap/a;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    if-eq p1, v0, :cond_2

    .line 58
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    .line 61
    :cond_0
    if-eqz p1, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->c()V

    .line 64
    :cond_1
    iput-object p1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    .line 66
    :cond_2
    return-void
.end method

.method private c()V
    .locals 10

    .prologue
    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v0, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 87
    if-nez v1, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v3, v0, v0, v2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 91
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 95
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 97
    iget v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->b:I

    if-eqz v1, :cond_2

    .line 98
    iget v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->b:I

    int-to-float v1, v1

    invoke-virtual {v4, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 99
    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 100
    iget v1, v3, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget v2, v3, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    invoke-virtual {v4, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 103
    :cond_2
    iget-boolean v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->a:Z

    if-eqz v1, :cond_3

    .line 104
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 105
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->getPaddingBottom()I

    move-result v5

    sub-int v5, v2, v5

    .line 107
    if-lez v1, :cond_3

    if-lez v5, :cond_3

    .line 111
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v2

    int-to-float v6, v5

    mul-float/2addr v2, v6

    int-to-float v6, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v7

    mul-float/2addr v6, v7

    cmpl-float v2, v2, v6

    if-lez v2, :cond_4

    .line 112
    int-to-float v2, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v2, v5

    .line 113
    int-to-float v1, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, v8

    .line 119
    :goto_1
    invoke-virtual {v4, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 120
    add-float/2addr v1, v8

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v8

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v4, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 124
    :cond_3
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/ImageWorkerView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto/16 :goto_0

    .line 115
    :cond_4
    int-to-float v1, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float v2, v1, v2

    .line 116
    int-to-float v1, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, v8

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->b:I

    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ImageWorkerView;->setImageResource(I)V

    .line 83
    :cond_0
    return-void
.end method

.method public final a(Lcom/mfluent/asp/util/bitmap/a;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/ImageWorkerView;->a(Lcom/mfluent/asp/util/bitmap/a;)V

    .line 49
    iput p2, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->b:I

    .line 51
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ImageWorkerView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 53
    invoke-direct {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->c()V

    .line 54
    return-void
.end method

.method public final b()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->d:Landroid/graphics/Point;

    return-object v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 156
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 157
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/bitmap/a;->e()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    .line 161
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->d:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMaximumBitmapWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMaximumBitmapHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->d:Landroid/graphics/Point;

    .line 75
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->c:Z

    if-eqz v0, :cond_1

    .line 170
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 171
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 172
    :cond_0
    const/high16 v0, -0x80000000

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ImageWorkerView;->setColorFilter(I)V

    .line 177
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 173
    :cond_2
    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 174
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ImageWorkerView;->setColorFilter(I)V

    goto :goto_0
.end method

.method protected setFrame(IIII)Z
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->setFrame(IIII)Z

    move-result v0

    .line 131
    invoke-direct {p0}, Lcom/mfluent/asp/ui/ImageWorkerView;->c()V

    .line 133
    return v0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 144
    const/4 v0, 0x0

    .line 145
    iget-object v1, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    if-eqz v1, :cond_0

    instance-of v1, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 146
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 147
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    invoke-virtual {v2}, Lcom/mfluent/asp/util/bitmap/a;->b()Landroid/graphics/Bitmap;

    move-result-object v2

    if-ne v2, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/mfluent/asp/ui/ImageWorkerView;->f:Lcom/mfluent/asp/util/bitmap/a;

    .line 151
    :cond_0
    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/ImageWorkerView;->a(Lcom/mfluent/asp/util/bitmap/a;)V

    .line 152
    return-void
.end method
