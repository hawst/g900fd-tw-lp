.class final Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    iput-object p2, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v4, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v4, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 277
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->e()V

    .line 279
    :try_start_0
    iget-object v6, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->a:Landroid/content/Context;

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SAMSUNG-"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_f

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    :goto_0
    const-string v0, "connectivity"

    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    const/16 v8, 0x9

    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const-string v4, "505"

    const-string v0, "00"

    :goto_1
    const-string v7, "https://vas.samsungapps.com/stub/stubDownload.as"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "?appId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&encImei="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v6}, Lcom/sec/pcw/hybrid/update/d;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&deviceId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&mcc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&mnc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&csc="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&sdkVer="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&pd="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/pcw/hybrid/update/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v0, "mfl_UpdateCheck"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::makeDowloadApkUrl:: url : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :goto_2
    if-nez v0, :cond_5

    .line 290
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v2, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 291
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    const/16 v2, 0x385

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(I)V

    .line 407
    :cond_2
    :goto_3
    return-object v1

    .line 279
    :cond_3
    if-eqz v4, :cond_4

    :try_start_1
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v6}, Lcom/sec/pcw/hybrid/update/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Lcom/sec/pcw/hybrid/update/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    const-string v0, "mfl_UpdateCheck"

    const-string v4, "Network Error!! Don\'t have to make a URL!"

    invoke-static {v0, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    goto :goto_2

    .line 282
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 284
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v2, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 285
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    const/16 v2, 0x384

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(I)V

    goto :goto_3

    .line 295
    :cond_5
    invoke-static {v0}, Lcom/sec/pcw/hybrid/update/d;->a(Ljava/net/URL;)Lcom/sec/pcw/hybrid/update/a;

    move-result-object v6

    .line 296
    if-nez v6, :cond_6

    .line 297
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v2, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 298
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    const/16 v2, 0x387

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(I)V

    goto :goto_3

    .line 302
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/pcw/hybrid/update/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v6}, Lcom/sec/pcw/hybrid/update/a;->b()I

    move-result v0

    if-ne v0, v2, :cond_7

    move v0, v2

    :goto_4
    if-nez v0, :cond_9

    .line 303
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v2, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 304
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    const/16 v2, 0x388

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(I)V

    goto :goto_3

    .line 302
    :cond_7
    const-string v0, "mfl_UpdateCheck"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::isAvailableDownloadApk::[unavailable] :: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    move v0, v3

    goto :goto_4

    :cond_8
    const-string v0, "mfl_UpdateCheck"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::isAvailableDownloadApk::wrong package name :: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 309
    :cond_9
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v2, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 310
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    const-wide/16 v2, 0x0

    invoke-virtual {v6}, Lcom/sec/pcw/hybrid/update/a;->e()J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(JJ)V

    .line 312
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 315
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 317
    new-instance v3, Lcom/sec/a/a/b;

    iget-object v4, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->a:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "."

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/sec/pcw/hybrid/update/a;->f()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ".slink"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/a/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 318
    invoke-virtual {v3}, Lcom/sec/a/a/b;->b()Z

    .line 319
    invoke-virtual {v3}, Lcom/sec/a/a/b;->a()Ljava/io/File;

    move-result-object v7

    .line 321
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 323
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 328
    :cond_a
    :try_start_2
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 330
    new-instance v8, Ljava/net/URI;

    invoke-virtual {v6}, Lcom/sec/pcw/hybrid/update/a;->d()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 332
    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 333
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 335
    iget-object v2, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->a:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/sec/a/a/b;->c()Ljava/lang/String;

    move-result-object v3

    const v8, 0x8001

    invoke-virtual {v2, v3, v8}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 337
    const v3, 0x8000

    :try_start_3
    new-array v3, v3, [B

    .line 340
    :cond_b
    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v8

    if-lez v8, :cond_c

    .line 341
    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9, v8}, Ljava/io/FileOutputStream;->write([BII)V

    .line 342
    int-to-long v8, v8

    add-long/2addr v4, v8

    .line 343
    iget-object v8, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    invoke-virtual {v6}, Lcom/sec/pcw/hybrid/update/a;->e()J

    move-result-wide v10

    invoke-virtual {v8, v4, v5, v10, v11}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(JJ)V

    .line 344
    iget-object v8, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    invoke-static {v8}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->d(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 345
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v3, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->e:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v3, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 346
    if-eqz v2, :cond_2

    .line 357
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_3

    .line 358
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 355
    :cond_c
    if-eqz v2, :cond_d

    .line 357
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 364
    :cond_d
    :goto_6
    invoke-static {}, Lcom/mfluent/asp/ui/UpgradeActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v2, ":doInBackground::Download Complete::FilePath:{}  FileSize:{}"

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 366
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->c:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v2, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 367
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->e()V

    .line 370
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1$1;

    invoke-direct {v2, p0, v7}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1$1;-><init>(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;Ljava/io/File;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 395
    const-wide/16 v2, 0x64

    :try_start_6
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 396
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 397
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    const-string v2, "com.sec.android.app.launcher"

    const-string v3, "com.android.launcher2.Launcher"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    iget-object v2, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7

    goto/16 :goto_3

    .line 400
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_3

    .line 358
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 349
    :catch_4
    move-exception v0

    move-object v2, v1

    :goto_7
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 351
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    sget-object v3, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iput-object v3, v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 352
    iget-object v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    const/16 v3, 0x3e9

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 353
    if-eqz v2, :cond_2

    .line 357
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_3

    .line 358
    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 355
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_8
    if-eqz v2, :cond_e

    .line 357
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 360
    :cond_e
    :goto_9
    throw v0

    .line 358
    :catch_6
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 403
    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 355
    :catchall_1
    move-exception v0

    goto :goto_8

    .line 349
    :catch_8
    move-exception v0

    goto :goto_7

    :cond_f
    move-object v5, v0

    goto/16 :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
