.class public Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PreparingDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 3

    .prologue
    .line 154
    check-cast p1, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    .line 156
    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->a(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->dismiss()V

    .line 158
    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->b(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "downloadProgress"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DownloadActivity;

    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->c(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->d(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/DownloadActivity;->a(Lcom/mfluent/asp/ui/DownloadActivity;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method protected final b()Lcom/mfluent/asp/ui/AsyncTaskFragment;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;-><init>()V

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DownloadActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity;->a(Lcom/mfluent/asp/ui/DownloadActivity;)V

    .line 174
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 175
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 147
    const v1, 0x7f0a03c1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$PreparingDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 148
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 149
    return-object v0
.end method
