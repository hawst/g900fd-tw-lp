.class public Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "*>;>;"
        }
    .end annotation
.end field

.field private b:Lcom/mfluent/asp/datamodel/Device;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 209
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 210
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    const-string v1, "MODE_EXTRA_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 212
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 213
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(I)V

    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 218
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 220
    const-string v1, "mfl_HomeSyncVaultLoadingActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "homesync exception-msgId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " exeption="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f0a027e

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b(I)V

    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V
    .locals 4

    .prologue
    .line 26
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->G()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 170
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 172
    packed-switch p2, :pswitch_data_0

    .line 201
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->setResult(ILandroid/content/Intent;)V

    .line 202
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->finish()V

    .line 206
    :goto_0
    return-void

    .line 174
    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 176
    :pswitch_1
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->setResult(ILandroid/content/Intent;)V

    .line 177
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->finish()V

    goto :goto_0

    .line 180
    :pswitch_2
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(I)V

    goto :goto_0

    .line 185
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 186
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const-string v1, "account_mode"

    const-string v2, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 189
    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 190
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 191
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 193
    :cond_0
    const v0, 0x7f0a0322

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b(I)V

    goto :goto_0

    .line 197
    :pswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->setResult(ILandroid/content/Intent;)V

    .line 198
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->finish()V

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 174
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const-string v0, "mfl_HomeSyncVaultLoadingActivity"

    const-string v1, "HomeSyncVaultLoadingActivity onCreate"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const v0, 0x7f030017

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->setContentView(I)V

    .line 51
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 53
    const-string v1, "com.samsung.android.sdk.samsunglink.DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 54
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 55
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b:Lcom/mfluent/asp/datamodel/Device;

    if-nez v2, :cond_0

    .line 56
    const-string v0, "mfl_HomeSyncVaultLoadingActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No device found with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->finish()V

    .line 99
    :goto_0
    return-void

    .line 61
    :cond_0
    const-string v1, "mfl_HomeSyncVaultLoadingActivity"

    const-string v2, "Starting homesync login procedure"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "com.samsung.android.sdk.samsunglink.CHANGE_OPTION_EXTRA_KEY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const-string v0, "mfl_HomeSyncVaultLoadingActivity"

    const-string v1, "change"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(I)V

    goto :goto_0

    .line 69
    :cond_1
    new-instance v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$1;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V

    .line 97
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 228
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/AsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 233
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 234
    return-void
.end method
