.class public Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 31
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 34
    invoke-virtual {p0, v7}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 35
    if-nez v0, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No child view found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    instance-of v1, v0, Landroid/widget/TextView;

    if-nez v1, :cond_1

    .line 40
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected the first child view to be a TextView but is a "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43
    :cond_1
    check-cast v0, Landroid/widget/TextView;

    .line 45
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getPaddingRight()I

    move-result v2

    sub-int v3, v1, v2

    .line 46
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AutoShrinkTextViewFontSizeWrapperModified;->getPaddingTop()I

    move-result v2

    sub-int v4, v1, v2

    .line 48
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 50
    const-string v1, "CHS"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "size : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const v1, 0x7fffffff

    :goto_0
    if-lez v4, :cond_2

    if-le v1, v4, :cond_2

    .line 53
    invoke-virtual {v0, v7, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 55
    const-string v1, "CHS"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "textView.getTextSize() : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 58
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 52
    const v5, 0x3dcccccd    # 0.1f

    sub-float/2addr v2, v5

    goto :goto_0

    .line 61
    :cond_2
    return-void
.end method
