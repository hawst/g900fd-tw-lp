.class public final Lcom/mfluent/asp/ui/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/a/c$a;


# instance fields
.field private final a:[Landroid/widget/TextView;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public varargs constructor <init>([Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/b;->b:Ljava/lang/Boolean;

    .line 23
    iput-object p1, p0, Lcom/mfluent/asp/ui/a/b;->a:[Landroid/widget/TextView;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/b;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 29
    iget-object v4, p0, Lcom/mfluent/asp/ui/a/b;->a:[Landroid/widget/TextView;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v4, v3

    .line 30
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v6

    if-lez v6, :cond_2

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0, v6}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 31
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/b;->b:Ljava/lang/Boolean;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/b;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 37
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/b;->b:Ljava/lang/Boolean;

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_2
    move v0, v2

    .line 30
    goto :goto_1

    .line 29
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0
.end method
