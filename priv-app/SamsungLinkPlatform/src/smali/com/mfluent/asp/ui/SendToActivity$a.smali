.class final Lcom/mfluent/asp/ui/SendToActivity$a;
.super Lcom/mfluent/asp/util/WeakReferencingBroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SendToActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/util/WeakReferencingBroadcastReceiver",
        "<",
        "Lcom/mfluent/asp/ui/SendToActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/SendToActivity;)V
    .locals 0

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/mfluent/asp/util/WeakReferencingBroadcastReceiver;-><init>(Ljava/lang/Object;)V

    .line 300
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 290
    check-cast p2, Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {}, Lcom/mfluent/asp/ui/SendToActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HomeFragment RX broadcast: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "com.mfluent.asp.sync.CLOUD_LAUNCH_OAUTH1_BROWSER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "OAUTH1_URI"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mfluent/asp/ui/SendToActivity;->b()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/mfluent/asp/ui/OAuthWebView;->a(Landroid/content/Intent;)V

    :cond_0
    invoke-static {p2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
