.class final Lcom/mfluent/asp/ui/SendToActivity$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic b:Lcom/mfluent/asp/ui/SendToActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 790
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    iput-object p2, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 795
    invoke-static {}, Lcom/mfluent/asp/ui/SendToActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Enter ::updateStorage()::OnClickListener()::onClick()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 797
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 799
    new-instance v1, Lcom/mfluent/asp/ui/SendToActivity$1$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/SendToActivity$1$1;-><init>(Lcom/mfluent/asp/ui/SendToActivity$1;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 821
    :goto_0
    return-void

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;Z)Z

    .line 810
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 811
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->a:Lcom/mfluent/asp/datamodel/Device;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    goto :goto_0

    .line 814
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;I)I

    .line 816
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v0

    .line 817
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 818
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    .line 819
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$1;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/b/h;)V

    goto :goto_0
.end method
