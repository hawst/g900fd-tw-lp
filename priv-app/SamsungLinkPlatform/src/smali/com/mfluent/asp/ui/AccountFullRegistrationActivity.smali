.class public Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/mfluent/asp/ui/AsyncTaskWatcher;
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:I

.field private f:Z

.field private g:Landroid/widget/TextView;

.field private final h:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    .line 58
    iput v1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    .line 59
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->f:Z

    .line 62
    new-instance v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$1;-><init>(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->h:Landroid/content/BroadcastReceiver;

    .line 519
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;I)I
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    return p1
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 263
    .line 264
    const/4 v0, -0x1

    move v2, v3

    .line 266
    :goto_0
    const/4 v1, 0x6

    if-ge v2, v1, :cond_1

    .line 267
    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 287
    :goto_1
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f090014

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 289
    if-ne p1, v2, :cond_0

    .line 290
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 266
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 269
    :pswitch_0
    const v0, 0x7f090054

    move v1, v0

    .line 270
    goto :goto_1

    .line 272
    :pswitch_1
    const v0, 0x7f090055

    move v1, v0

    .line 273
    goto :goto_1

    .line 275
    :pswitch_2
    const v0, 0x7f090056

    move v1, v0

    .line 276
    goto :goto_1

    .line 278
    :pswitch_3
    const v0, 0x7f090057

    move v1, v0

    .line 279
    goto :goto_1

    .line 281
    :pswitch_4
    const v0, 0x7f090058

    move v1, v0

    .line 282
    goto :goto_1

    .line 284
    :pswitch_5
    const v0, 0x7f090059

    move v1, v0

    goto :goto_1

    .line 292
    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 296
    :cond_1
    return-void

    .line 267
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/widget/TextView;Z)V
    .locals 1

    .prologue
    .line 492
    if-eqz p1, :cond_0

    .line 493
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 497
    :goto_0
    return-void

    .line 495
    :cond_0
    const v0, 0x3e4ccccd    # 0.2f

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b()V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 181
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 183
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 184
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 185
    iget-object v2, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 188
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->c()V

    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 228
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v6, v5

    move v7, v5

    .line 233
    :goto_0
    if-ge v6, v8, :cond_4

    .line 234
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 235
    const/4 v1, 0x0

    packed-switch v7, :pswitch_data_0

    move-object v4, v1

    :goto_1
    if-eqz v4, :cond_3

    const v1, 0x7f090012

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090013

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f090011

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v9, 0x7f090014

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v9, v10, :cond_1

    sget-object v9, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    const-string v10, "::update:SlinkDevicePhysicalType.SPC cannot be deregistered"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0259

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setEnabled(Z)V

    :goto_3
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v10

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    iget-boolean v12, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->f:Z

    sget-boolean v13, Lcom/mfluent/asp/ASPApplication;->k:Z

    invoke-static {v10, v11, v12, v13}, Lcom/mfluent/asp/ui/DeviceHelper;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;ZZZ)I

    move-result v10

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v1, v3}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a(Landroid/widget/TextView;Z)V

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v2, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a(Landroid/widget/TextView;Z)V

    new-instance v1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$2;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$2;-><init>(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-virtual {v4, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    :goto_4
    add-int/lit8 v1, v7, 0x1

    .line 233
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto/16 :goto_0

    .line 235
    :pswitch_0
    const v1, 0x7f090054

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :pswitch_1
    const v1, 0x7f090055

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :pswitch_2
    const v1, 0x7f090056

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :pswitch_3
    const v1, 0x7f090057

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :pswitch_4
    const v1, 0x7f090058

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :pswitch_5
    const v1, 0x7f090059

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :cond_0
    sget-object v9, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    const-string v10, "::update:Device did nto have a display name: {}"

    invoke-interface {v9, v10, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_1
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v9, v10, :cond_2

    sget-object v9, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    const-string v10, "::update:SlinkDevicePhysicalType.BLURAY cannot be deregistered"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0285

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setEnabled(Z)V

    goto/16 :goto_3

    :cond_2
    const/16 v9, 0x8

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_3
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::update:TOO MANY DEVICES {}"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 240
    :cond_4
    iget v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    if-nez v0, :cond_7

    .line 242
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e()V

    .line 243
    :goto_5
    if-ge v5, v8, :cond_5

    .line 244
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 245
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v1

    if-nez v1, :cond_6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 246
    invoke-direct {p0, v5}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a(I)V

    .line 247
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    .line 260
    :cond_5
    :goto_6
    return-void

    .line 243
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_7
    move v1, v5

    .line 252
    :goto_7
    if-ge v1, v8, :cond_5

    .line 253
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 254
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    iget v2, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    if-ne v0, v2, :cond_8

    .line 255
    invoke-direct {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a(I)V

    goto :goto_6

    .line 252
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 235
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e()V

    return-void
.end method

.method private d()Lcom/mfluent/asp/datamodel/Device;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 383
    const/4 v1, 0x0

    move v3, v0

    .line 387
    :goto_0
    const/4 v2, 0x6

    if-ge v3, v2, :cond_0

    .line 388
    packed-switch v3, :pswitch_data_0

    move v2, v0

    .line 408
    :goto_1
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f090014

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 410
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 387
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    move v0, v2

    goto :goto_0

    .line 390
    :pswitch_0
    const v0, 0x7f090054

    move v2, v0

    .line 391
    goto :goto_1

    .line 393
    :pswitch_1
    const v0, 0x7f090055

    move v2, v0

    .line 394
    goto :goto_1

    .line 396
    :pswitch_2
    const v0, 0x7f090056

    move v2, v0

    .line 397
    goto :goto_1

    .line 399
    :pswitch_3
    const v0, 0x7f090057

    move v2, v0

    .line 400
    goto :goto_1

    .line 402
    :pswitch_4
    const v0, 0x7f090058

    move v2, v0

    .line 403
    goto :goto_1

    .line 405
    :pswitch_5
    const v0, 0x7f090059

    move v2, v0

    goto :goto_1

    .line 415
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 388
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 420
    const/4 v0, -0x1

    move v2, v3

    .line 423
    :goto_0
    const/4 v1, 0x6

    if-ge v2, v1, :cond_0

    .line 424
    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 444
    :goto_1
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f090014

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 445
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 423
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 426
    :pswitch_0
    const v0, 0x7f090054

    move v1, v0

    .line 427
    goto :goto_1

    .line 429
    :pswitch_1
    const v0, 0x7f090055

    move v1, v0

    .line 430
    goto :goto_1

    .line 432
    :pswitch_2
    const v0, 0x7f090056

    move v1, v0

    .line 433
    goto :goto_1

    .line 435
    :pswitch_3
    const v0, 0x7f090057

    move v1, v0

    .line 436
    goto :goto_1

    .line 438
    :pswitch_4
    const v0, 0x7f090058

    move v1, v0

    .line 439
    goto :goto_1

    .line 441
    :pswitch_5
    const v0, 0x7f090059

    move v1, v0

    goto :goto_1

    .line 448
    :cond_0
    return-void

    .line 424
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 501
    const v0, 0x7f0a00bd

    if-ne p1, v0, :cond_0

    .line 502
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 503
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 504
    if-eqz v0, :cond_0

    .line 505
    new-instance v1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "deviceId"

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "deleteTaskFragment"

    invoke-virtual {v1, p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->a(Lcom/mfluent/asp/ui/AsyncTaskWatcher;Ljava/lang/String;)V

    .line 509
    :cond_0
    return-void
.end method

.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 3

    .prologue
    const v2, 0x7f09005a

    const/4 v1, 0x0

    .line 564
    check-cast p1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;

    .line 566
    iget-object v0, p1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 568
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 586
    :goto_0
    return-void

    .line 571
    :cond_0
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 573
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Removing {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 575
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 577
    iget-object v0, p1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    const v0, 0x7f0a0348

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 582
    :catch_0
    move-exception v0

    .line 583
    sget-object v1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a:Lorg/slf4j/Logger;

    const-string v2, "Failed to Removing DeleteDeviceTaskFragment "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 580
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0111

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f0a0026

    const/4 v5, 0x0

    const v4, 0x7f0a0285

    const v3, 0x7f0a0259

    const v2, 0x7f0a00bd

    .line 590
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 598
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 593
    :pswitch_1
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Z)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v3, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Z)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v4, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    goto :goto_0

    .line 597
    :pswitch_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->finish()V

    goto :goto_0

    .line 590
    nop

    :pswitch_data_0
    .packed-switch 0x7f090015
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->requestWindowFeature(I)Z

    .line 80
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 82
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 83
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    xor-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 84
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 89
    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->f:Z

    .line 90
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->f:Z

    if-eqz v0, :cond_4

    .line 91
    const v0, 0x7f0b002d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->setTheme(I)V

    .line 100
    :goto_0
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->setContentView(I)V

    .line 102
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 103
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 104
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 106
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    :cond_1
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    const v1, 0x7f030001

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f090017

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    if-eqz p1, :cond_2

    .line 109
    const-string v0, "select_device"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    .line 112
    :cond_2
    const-string v0, "VZW"

    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113
    const v0, 0x7f090053

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->g:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->g:Landroid/widget/TextView;

    const v1, 0x7f0a03df

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 116
    :cond_3
    return-void

    .line 93
    :cond_4
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_5

    .line 94
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->setTheme(I)V

    goto :goto_0

    .line 96
    :cond_5
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->setTheme(I)V

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 154
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 155
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 136
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 138
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 139
    sget-object v1, Lcom/mfluent/asp/nts/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 142
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 145
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->b()V

    .line 146
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->c()V

    .line 147
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 160
    const-string v0, "select_device"

    iget v1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 161
    return-void
.end method
