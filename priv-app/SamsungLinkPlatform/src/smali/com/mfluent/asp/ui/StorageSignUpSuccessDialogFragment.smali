.class public Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$SignUpSuccessWebViewClient;
    }
.end annotation


# instance fields
.field a:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    .line 126
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 156
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->dismiss()V

    .line 157
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 158
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 42
    const-string v1, "cloud_storage_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 44
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v2

    .line 46
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f030027

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    .line 48
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 50
    const v0, 0x7f0a0126

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    const v1, 0x7f0900c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 56
    new-instance v1, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$SignUpSuccessWebViewClient;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$SignUpSuccessWebViewClient;-><init>(Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;)V

    .line 57
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 59
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 62
    const-string v4, "es"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "de"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "jp"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "zh"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "fr"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "it"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ko"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ru"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "en"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 72
    :cond_0
    const-string v4, "en"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 73
    const-string v4, "http://www.sugarsync.com/offers/samsungmobile/index_LANG.html"

    const-string v5, "LANG"

    invoke-virtual {v4, v5, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    const v1, 0x7f0900c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 86
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;->a:Landroid/view/View;

    const v4, 0x7f0900c4

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 88
    new-instance v4, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$1;-><init>(Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$2;

    invoke-direct {v0, p0, v2}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment$2;-><init>(Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;Lcom/mfluent/asp/b/h;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 80
    :cond_1
    const-string v1, "http://www.sugarsync.com/offers/samsungmobile/index_LANG.html"

    const-string v4, "_LANG"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
