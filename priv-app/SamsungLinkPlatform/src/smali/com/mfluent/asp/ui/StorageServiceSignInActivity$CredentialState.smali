.class final enum Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CredentialState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

.field public static final enum b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

.field public static final enum c:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

.field private static final synthetic d:[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    const-string v1, "CREDENTIALS_ARE_VALID"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 85
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    const-string v1, "EMAIL_ADDRESS_INVALID"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 86
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    const-string v1, "PW_REQUIRED"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->c:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    .line 83
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->c:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->d:[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->d:[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    invoke-virtual {v0}, [Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/ui/StorageServiceSignInActivity$CredentialState;

    return-object v0
.end method
