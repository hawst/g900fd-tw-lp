.class public Lcom/mfluent/asp/ui/a/a;
.super Lcom/mfluent/asp/ui/a/c;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Z

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/mfluent/asp/ui/a/a;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/a/a;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/ui/a/c$a;I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/ui/a/c;-><init>(Landroid/content/Context;Lcom/mfluent/asp/ui/a/c$a;)V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/a/a;->d:Z

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/ui/a/a;->e:I

    .line 32
    iput p3, p0, Lcom/mfluent/asp/ui/a/a;->e:I

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/a;->b:Landroid/graphics/drawable/Drawable;

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/a;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/mfluent/asp/ui/a/a;->e:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/a;->c:Landroid/graphics/drawable/Drawable;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/a;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 57
    sget-object v0, Lcom/mfluent/asp/ui/a/a;->a:Lorg/slf4j/Logger;

    const-string v1, "::onHoverEnter View id: {} swapping BG from: {} to: {}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/mfluent/asp/ui/a/a;->b:Landroid/graphics/drawable/Drawable;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/mfluent/asp/ui/a/a;->c:Landroid/graphics/drawable/Drawable;

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method protected final a()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/a/a;->d:Z

    return v0
.end method

.method protected final b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 64
    sget-object v0, Lcom/mfluent/asp/ui/a/a;->a:Lorg/slf4j/Logger;

    const-string v1, "::onHoverExit View id: {} restoring BG: {}"

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/ui/a/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 65
    return-void
.end method
