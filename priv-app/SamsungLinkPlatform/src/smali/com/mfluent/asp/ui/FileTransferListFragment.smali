.class public Lcom/mfluent/asp/ui/FileTransferListFragment;
.super Landroid/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/FileTransferListFragment$6;,
        Lcom/mfluent/asp/ui/FileTransferListFragment$a;,
        Lcom/mfluent/asp/ui/FileTransferListFragment$b;,
        Lcom/mfluent/asp/ui/FileTransferListFragment$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Fragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/mfluent/asp/ui/dialog/a;"
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static h:Ljava/lang/String;


# instance fields
.field private b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

.field private c:Landroid/view/View;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/FileTransferListFragment;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 103
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->d:Z

    .line 104
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->e:Z

    .line 107
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->f:Z

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->g:Landroid/os/Handler;

    .line 931
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/FileTransferListFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/ui/FileTransferListFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/ui/FileTransferListFragment;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c()Z

    move-result v0

    return v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "autoUpload"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 115
    :cond_0
    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->d:Z

    return v0
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 830
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "transferarchive=?"

    new-array v4, v7, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "1"

    :goto_0
    aput-object v5, v4, v6

    const-string v5, "endTime DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 839
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 840
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 841
    iput-boolean v7, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->f:Z

    .line 848
    :goto_1
    return-void

    .line 830
    :cond_0
    const-string v5, "0"

    goto :goto_0

    .line 843
    :cond_1
    if-eqz v0, :cond_2

    .line 844
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_2
    iput-boolean v6, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->f:Z

    goto :goto_1
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->e:Z

    return v0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 1296
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->g:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1317
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1299
    :cond_1
    :try_start_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->g:Landroid/os/Handler;

    .line 1300
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->g:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1301
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->g:Landroid/os/Handler;

    new-instance v1, Lcom/mfluent/asp/ui/FileTransferListFragment$5;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$5;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/FileTransferListFragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d()V

    return-void
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/FileTransferListFragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->e()V

    return-void
.end method

.method static synthetic h(Lcom/mfluent/asp/ui/FileTransferListFragment;)Lcom/mfluent/asp/ui/FileTransferListFragment$c;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    return-object v0
.end method

.method static synthetic i(Lcom/mfluent/asp/ui/FileTransferListFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->g:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1197
    const v0, 0x7f0a02bb

    if-ne v0, p1, :cond_4

    .line 1198
    if-ne p2, v1, :cond_1

    .line 1206
    if-eqz p3, :cond_0

    const-string v0, "FROM_ERROR"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1207
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    .line 1208
    invoke-interface {v0, v2}, Lcom/mfluent/asp/filetransfer/d;->a(Z)V

    .line 1209
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 1211
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->e()V

    .line 1292
    :cond_1
    :goto_0
    return-void

    .line 1213
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    .line 1215
    invoke-interface {v0, v2}, Lcom/mfluent/asp/filetransfer/d;->a(Z)V

    .line 1216
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/LruCache;->evictAll()V

    .line 1217
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/d;->h()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 1218
    const-string v0, "isAutoupload"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1219
    const-string v4, "transferarchive=? AND state=?"

    .line 1223
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "1"

    :goto_1
    aput-object v0, v5, v6

    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    .line 1233
    new-instance v0, Lcom/mfluent/asp/ui/FileTransferListFragment$3;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ui/FileTransferListFragment$3;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment;Landroid/content/ContentResolver;)V

    .line 1247
    const/16 v1, 0x4e20

    const/4 v2, 0x0

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 1223
    :cond_3
    const-string v0, "0"

    goto :goto_1

    .line 1254
    :cond_4
    const v0, 0x7f0a034e

    if-ne v0, p1, :cond_1

    .line 1255
    if-ne p2, v1, :cond_1

    .line 1256
    const-string v0, "sessionId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1257
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    const-string v2, "sourceDeviceId"

    invoke-virtual {p3, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 1258
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    const-string v3, "targetDeviceId"

    invoke-virtual {p3, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 1259
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/mfluent/asp/ui/FileTransferListFragment$4;

    invoke-direct {v4, p0, v1, v0, v2}, Lcom/mfluent/asp/ui/FileTransferListFragment$4;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 1285
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.filetransfer.FileTransferManager.CANCELED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1286
    const-string v2, "sessionId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1287
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1289
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->e()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/FileTransferListFragment;->setHasOptionsMenu(Z)V

    .line 124
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->d:Z

    .line 133
    :goto_0
    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/FileTransferListFragment;->h:Ljava/lang/String;

    .line 134
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 135
    return-void

    .line 127
    :cond_0
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 128
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->e:Z

    goto :goto_0

    .line 130
    :cond_1
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->e:Z

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v0, Lcom/mfluent/asp/ui/FileTransferListFragment$b;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/FileTransferListFragment$b;-><init>(Landroid/content/Context;Z)V

    .line 182
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->setUpdateThrottle(J)V

    .line 183
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 139
    const v0, 0x7f03000b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 141
    new-instance v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    .line 143
    const v0, 0x7f090039

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 144
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 145
    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 146
    const v2, 0x7f09003a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 148
    const v0, 0x7f09002f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->c:Landroid/view/View;

    .line 149
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 151
    const v0, 0x7f090033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 152
    const v2, 0x7f0a0358

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 154
    const v0, 0x7f090031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 155
    new-instance v2, Lcom/mfluent/asp/ui/FileTransferListFragment$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$1;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    const v0, 0x7f090032

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 164
    new-instance v2, Lcom/mfluent/asp/ui/FileTransferListFragment$2;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$2;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    invoke-direct {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d()V

    .line 174
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const v2, 0x7f09000b

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 176
    return-object v1
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 82
    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    invoke-virtual {v0, p2}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 194
    return-void
.end method
