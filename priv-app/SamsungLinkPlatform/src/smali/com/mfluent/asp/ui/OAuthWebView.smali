.class public Lcom/mfluent/asp/ui/OAuthWebView;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static e:Z


# instance fields
.field private b:Landroid/webkit/WebView;

.field private c:I

.field private d:Landroid/content/IntentFilter;

.field private f:Landroid/widget/FrameLayout;

.field private g:Z

.field private h:Z

.field private final i:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/OAuthWebView;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/ui/OAuthWebView;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 61
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->g:Z

    .line 63
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->h:Z

    .line 440
    new-instance v0, Lcom/mfluent/asp/ui/OAuthWebView$3;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/OAuthWebView$3;-><init>(Lcom/mfluent/asp/ui/OAuthWebView;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->i:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static final a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 93
    sget-object v0, Lcom/mfluent/asp/ui/OAuthWebView;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 94
    const-string v0, "mfl_OAuthWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::startOauthWebView:OAuthWebPage: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "OAUTH1_URI"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 98
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 99
    return-object v0
.end method

.method public static final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 67
    sget-boolean v0, Lcom/mfluent/asp/ui/OAuthWebView;->e:Z

    if-nez v0, :cond_1

    .line 68
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/ui/OAuthWebView;->e:Z

    .line 69
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 70
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 73
    const-string v3, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 74
    const-string v4, "OAUTH1_URI"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 75
    const-string v5, "OAUTH1_CLEAR_CACHE"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 76
    sget-object v5, Lcom/mfluent/asp/ui/OAuthWebView;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x4

    if-gt v5, v6, :cond_0

    .line 77
    const-string v5, "mfl_OAuthWebView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::startOauthWebView:OAuthWebPage: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    const-string v5, "OAUTH1_CALLBACK_FILTER"

    invoke-virtual {p0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    .line 82
    const-string v6, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v1, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 83
    const-string v3, "OAUTH1_URI"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string v3, "OAUTH1_CLEAR_CACHE"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    const-string v2, "OAUTH1_CALLBACK_FILTER"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 86
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 87
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ASPApplication;->startActivity(Landroid/content/Intent;)V

    .line 89
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/OAuthWebView;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->e()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/OAuthWebView;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->g:Z

    return p1
.end method

.method static synthetic b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/ui/OAuthWebView;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/OAuthWebView;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->d()V

    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/OAuthWebView;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->h:Z

    return p1
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/OAuthWebView;)Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->d:Landroid/content/IntentFilter;

    return-object v0
.end method

.method private c()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 125
    const v0, 0x7f09005d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->f:Landroid/widget/FrameLayout;

    .line 127
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    if-nez v0, :cond_4

    .line 128
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    .line 129
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 130
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 134
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 135
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 136
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 137
    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 141
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-le v1, v2, :cond_0

    .line 142
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/mfluent/asp/ui/OAuthWebView$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/OAuthWebView$1;-><init>(Lcom/mfluent/asp/ui/OAuthWebView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 270
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->c:I

    const-string v0, "OAUTH1_URI"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "OAUTH1_CALLBACK_FILTER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    iput-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->d:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->d:Landroid/content/IntentFilter;

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->d:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->d:Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->d:Landroid/content/IntentFilter;

    const-string v3, "aspoauth"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    :cond_1
    const-string v0, "OAUTH1_CLEAR_CACHE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    invoke-static {p0}, Landroid/webkit/WebViewDatabase;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearFormData()V

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearHttpAuthUsernamePassword()V

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearUsernamePassword()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    sget-object v0, Lcom/mfluent/asp/ui/OAuthWebView;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_3

    const-string v0, "mfl_OAuthWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::onCreate:LoadURI: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 272
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 273
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 277
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 279
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->g:Z

    if-eqz v0, :cond_5

    .line 280
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->e()V

    .line 284
    :goto_1
    return-void

    .line 270
    :catch_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/ui/OAuthWebView;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v3, 0x3

    if-gt v1, v3, :cond_2

    const-string v1, "mfl_OAuthWebView"

    const-string v3, "Trouble clearing webview persistent data"

    invoke-static {v1, v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 282
    :cond_5
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->d()V

    goto :goto_1
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/OAuthWebView;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->c:I

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 399
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 400
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 403
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 404
    return-void
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/OAuthWebView;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->h:Z

    return v0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/OAuthWebView;)Z
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->c:I

    return v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 408
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 438
    :goto_0
    return-void

    .line 412
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->d()V

    .line 414
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 416
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 418
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 421
    new-instance v0, Lcom/mfluent/asp/ui/OAuthWebView$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/OAuthWebView$2;-><init>(Lcom/mfluent/asp/ui/OAuthWebView;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 435
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 363
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 366
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->setContentView(I)V

    .line 369
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->c()V

    .line 370
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->setRequestedOrientation(I)V

    .line 111
    :goto_0
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->setContentView(I)V

    .line 113
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->d()V

    .line 115
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->c()V

    .line 117
    return-void

    .line 109
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/OAuthWebView;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 324
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 325
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 329
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 330
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 335
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 385
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 388
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 390
    const-string v0, "aborting_signin"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 391
    const-string v0, "save_instance_device_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    .line 392
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 339
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 340
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 374
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 377
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 379
    const-string v0, "aborting_signin"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 380
    const-string v0, "save_instance_device_id"

    iget v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 381
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 344
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 345
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->i:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 346
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 350
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 351
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 352
    invoke-direct {p0}, Lcom/mfluent/asp/ui/OAuthWebView;->e()V

    .line 353
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/ui/OAuthWebView;->e:Z

    .line 354
    return-void
.end method
