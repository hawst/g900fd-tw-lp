.class public Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;,
        Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;,
        Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/mfluent/asp/datamodel/t;

.field private c:Lcom/mfluent/asp/datamodel/Device;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/EditText;

.field private k:Landroid/widget/EditText;

.field private l:Landroid/widget/EditText;

.field private m:Landroid/widget/EditText;

.field private n:Lcom/sec/android/safe/SecureStorageManagerClient;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 90
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->o:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    .line 76
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f:Z

    .line 77
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    .line 79
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    .line 80
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    .line 81
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    .line 83
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->k:Landroid/widget/EditText;

    .line 84
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->l:Landroid/widget/EditText;

    .line 85
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->m:Landroid/widget/EditText;

    .line 88
    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    .line 656
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    return-object v0
.end method

.method private varargs a(IIZ[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 828
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 829
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 831
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 832
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 835
    :cond_2
    if-eqz p3, :cond_3

    .line 836
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1, p2, p4}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    .line 840
    :goto_0
    return-void

    .line 838
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p2, p4}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private varargs a(I[Ljava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 843
    invoke-direct {p0, v0, p1, v0, p2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(IIZ[Ljava/lang/Object;)V

    .line 844
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;II[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(IIZ[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 273
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "promptToCreatePassword start"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    if-eqz p1, :cond_0

    .line 275
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "requestEnableBt"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-direct {p0, p1, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "requestEnableBt fail"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-void

    .line 282
    :cond_0
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Bluetooth is enabled.  Prompting user to create a new password"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const v0, 0x7f090070

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    .line 285
    const v0, 0x7f090071

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    .line 286
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a([Landroid/widget/EditText;)V

    .line 288
    const v0, 0x7f090072

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 289
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$1;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method private a([Landroid/widget/EditText;)V
    .locals 4

    .prologue
    .line 1042
    if-eqz p1, :cond_0

    .line 1043
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1044
    new-instance v3, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;

    invoke-direct {v3, p0, p1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;[Landroid/widget/EditText;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1043
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1076
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x5

    const/4 v6, 0x0

    .line 502
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 505
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    const-string v1, "/api/pCloud/device/account/getLoginCount"

    invoke-static {v0, v1}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 506
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    const-string v2, "loginCount"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->d(I)V

    .line 508
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Login count left : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v0

    if-le v0, v3, :cond_1

    .line 510
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->d(I)V

    .line 516
    :cond_0
    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->getWifiPkeInstance()Lcom/sec/android/safe/wifiPke/WifiPke;

    move-result-object v0

    .line 517
    if-nez v0, :cond_2

    move v0, v6

    .line 535
    :goto_0
    return v0

    .line 511
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v0

    if-gtz v0, :cond_0

    .line 512
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Login count less than 0"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 513
    goto :goto_0

    .line 520
    :cond_2
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->e:Ljava/lang/String;

    const-string v3, "not used"

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v4

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/safe/wifiPke/WifiPke;->verifyDSAPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 526
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "verify path : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->d(I)V

    .line 530
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V

    move v0, v7

    .line 531
    goto :goto_0

    .line 533
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->d(I)V

    .line 534
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->n(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    .line 535
    goto :goto_0

    .line 537
    :catch_0
    move-exception v0

    .line 538
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    throw v0
.end method

.method private a(Ljava/lang/String;Z)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 544
    new-instance v2, Lpcloud/net/a/a/a;

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lpcloud/net/a/a/a;-><init>(Ljava/lang/String;)V

    .line 545
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 546
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    .line 548
    :try_start_0
    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->getWifiPkeInstance()Lcom/sec/android/safe/wifiPke/WifiPke;

    move-result-object v3

    .line 549
    if-nez v3, :cond_0

    .line 550
    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 598
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lpcloud/net/a/a/a;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    throw v0

    .line 553
    :cond_0
    :try_start_2
    invoke-static {p1}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 554
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 555
    iget-object v4, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v4

    .line 556
    if-nez v4, :cond_1

    .line 557
    throw v1

    .line 560
    :cond_1
    invoke-virtual {v2, v5}, Lpcloud/net/a/a/a;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 561
    sget-object v6, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    const/4 v7, 0x3

    if-gt v6, v7, :cond_2

    .line 562
    const-string v6, "mfl_HomeSyncVaultLoginActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Got response status line from "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for request "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpGet;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ": "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :cond_2
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    iget-object v5, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v5

    invoke-virtual {v3, v1, v4, v5}, Lcom/sec/android/safe/wifiPke/WifiPke;->processResponse(Ljava/io/InputStream;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 568
    const-string v5, "mfl_HomeSyncVaultLoginActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "wifiPkeInstance.processResponse() - result : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v5, "SUCCESS"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    if-eqz v5, :cond_4

    .line 572
    :try_start_3
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->e:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/sec/android/safe/wifiPke/WifiPke;->SpcIsTokenExist(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 573
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/service/account/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/safe/wifiPke/WifiPke;->SPCGeneratorTokenReq(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 574
    invoke-static {v1}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 575
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 576
    invoke-virtual {v2, v5}, Lpcloud/net/a/a/a;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 577
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/safe/wifiPke/WifiPke;->SPCStoreToken(Ljava/io/InputStream;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 582
    :cond_3
    :goto_0
    invoke-virtual {v2}, Lpcloud/net/a/a/a;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 601
    :goto_1
    return v0

    .line 579
    :catch_1
    move-exception v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 583
    :cond_4
    const-string v4, "FAILURE"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    if-eqz v4, :cond_5

    .line 584
    invoke-virtual {v2}, Lpcloud/net/a/a/a;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    const/4 v0, 0x0

    goto :goto_1

    .line 585
    :cond_5
    if-eqz p2, :cond_6

    .line 587
    :try_start_5
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v3, "Resended parsed url but still got encrypted response. Hack to force login for test"

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 588
    invoke-virtual {v2}, Lpcloud/net/a/a/a;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_1

    .line 590
    :cond_6
    :try_start_6
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v4

    invoke-virtual {v3, v1, v0, v4}, Lcom/sec/android/safe/wifiPke/WifiPke;->parseDSAPassword(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 594
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "wifiPkeInstance.parseDSAPassword() - result : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Ljava/lang/String;Z)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    .line 601
    invoke-virtual {v2}, Lpcloud/net/a/a/a;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_1
.end method

.method private a(ZI)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 246
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    .line 248
    if-nez v2, :cond_0

    .line 250
    const-string v2, "mfl_HomeSyncVaultLoginActivity"

    const-string v3, "Failed to create password because bluetooth not supported on this device."

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const v2, 0x7f0a027d

    new-array v3, v0, [Ljava/lang/Object;

    invoke-direct {p0, v0, v2, v1, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(IIZ[Ljava/lang/Object;)V

    .line 269
    :goto_0
    return v0

    .line 255
    :cond_0
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 256
    if-eqz p1, :cond_1

    .line 257
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Bluetooth is not enabled. Asking the user to enable it"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.bluetooth.adapter.action.REQUEST_ENABLE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    invoke-static {p0}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/app/Activity;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setRequestedOrientation(I)V

    .line 261
    invoke-virtual {p0, v1, p2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 263
    :cond_1
    const-string v2, "mfl_HomeSyncVaultLoginActivity"

    const-string v3, "Failed to create password because bluetooth is still not enabled - giving up"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const v2, 0x7f0a025e

    new-array v3, v0, [Ljava/lang/Object;

    invoke-direct {p0, v0, v2, v1, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(IIZ[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 269
    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 417
    const v0, 0x7f09006d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    .line 418
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a([Landroid/widget/EditText;)V

    .line 420
    const v0, 0x7f09006f

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 421
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$2;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$2;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 436
    const v0, 0x7f09006e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 437
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$3;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$3;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 455
    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 606
    if-eqz p1, :cond_0

    .line 607
    invoke-direct {p0, p1, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 654
    :goto_0
    return-void

    .line 612
    :cond_0
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Bluetooth is enabled.  Prompting user to change a password"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const v0, 0x7f090065

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    .line 615
    const v0, 0x7f090067

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    .line 616
    const v0, 0x7f090069

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    .line 617
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a([Landroid/widget/EditText;)V

    .line 619
    const v0, 0x7f09006a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 620
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$4;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$4;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 809
    const-string v0, "^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{6,16}$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 810
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 811
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 850
    const v0, 0x7f09006c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 851
    const v1, 0x7f09006b

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 852
    if-eqz v1, :cond_0

    .line 853
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 854
    iput-boolean v3, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    .line 855
    if-eqz v0, :cond_0

    .line 856
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 857
    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 860
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 704
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "promptToResetPassword start"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    if-eqz p1, :cond_0

    .line 706
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "requestEnableBt"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 708
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "requestEnableBt fail"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    :goto_0
    return-void

    .line 713
    :cond_0
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Bluetooth is enabled.  Prompting user to create a new password"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    invoke-virtual {v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->recoveryPasswd()V

    .line 716
    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    .line 721
    const v0, 0x7f090070

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->k:Landroid/widget/EditText;

    .line 722
    const v0, 0x7f090071

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->l:Landroid/widget/EditText;

    .line 723
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->k:Landroid/widget/EditText;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->l:Landroid/widget/EditText;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a([Landroid/widget/EditText;)V

    .line 725
    const v0, 0x7f090072

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 726
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 756
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "promptToRecoverPassword start"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    if-eqz p1, :cond_0

    .line 758
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "requestEnableBt"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 760
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "requestEnableBt fail"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    :goto_0
    return-void

    .line 765
    :cond_0
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Bluetooth is enabled.  Prompting user to recover password"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const v0, 0x7f09006d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->m:Landroid/widget/EditText;

    .line 769
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->m:Landroid/widget/EditText;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a([Landroid/widget/EditText;)V

    .line 771
    const v0, 0x7f09006f

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 772
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$6;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$6;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 787
    const v0, 0x7f09006e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 788
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$7;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$7;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/sec/android/safe/SecureStorageManagerClient;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    return-object v0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->k:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->l:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic h(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->m:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic i(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->p:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 866
    const v0, 0x7f09006c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 867
    const v1, 0x7f09006b

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 868
    if-eqz v1, :cond_0

    .line 869
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 870
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    .line 871
    if-eqz v0, :cond_0

    .line 872
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 873
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 876
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 203
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 205
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 206
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", res: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_0
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setRequestedOrientation(I)V

    .line 221
    if-ne p2, v4, :cond_1

    .line 223
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "User enabled bluetooth"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    packed-switch p1, :pswitch_data_0

    .line 243
    :goto_0
    return-void

    .line 227
    :pswitch_0
    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Z)V

    goto :goto_0

    .line 230
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Z)V

    goto :goto_0

    .line 233
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Z)V

    goto :goto_0

    .line 236
    :pswitch_3
    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d(Z)V

    goto :goto_0

    .line 240
    :cond_1
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "User did not enable bluetooth"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const v0, 0x7f0a025e

    const/4 v1, 0x1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(IIZ[Ljava/lang/Object;)V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 931
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    if-nez v0, :cond_0

    .line 932
    const/16 v0, 0x63

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 933
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->finish()V

    .line 935
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const v8, 0x7f0a0265

    const/4 v7, 0x2

    const/4 v6, 0x1

    const v5, 0x7f0a027b

    const/4 v4, 0x0

    .line 909
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 927
    :cond_0
    :goto_0
    return-void

    .line 912
    :pswitch_0
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    if-nez v0, :cond_0

    .line 913
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v5, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v8, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    invoke-virtual {v1, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->initPasswd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    if-ne v0, v6, :cond_5

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v5, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;

    invoke-direct {v1, p0, v4}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;B)V

    new-array v2, v6, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    if-ne v0, v7, :cond_9

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v5, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v8, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const v0, 0x7f0a027c

    new-array v1, v4, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_8
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;

    invoke-direct {v2, p0, v4}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;B)V

    new-array v3, v7, [Ljava/lang/String;

    aput-object v1, v3, v4

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    iput-boolean v6, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f:Z

    goto/16 :goto_0

    :cond_9
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v5, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v8, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    invoke-virtual {v1, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->initPasswd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    goto/16 :goto_0

    :cond_c
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v5, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_d
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    invoke-virtual {v1, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->deviceChanged(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    new-array v0, v4, [Ljava/lang/Object;

    invoke-direct {p0, v5, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_f
    new-instance v1, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;

    invoke-direct {v1, p0, v4}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;B)V

    new-array v2, v6, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c()V

    goto/16 :goto_0

    .line 918
    :pswitch_1
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    if-nez v0, :cond_0

    .line 919
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    if-ne v0, v7, :cond_10

    iget-boolean v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->g()V

    :cond_10
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->finish()V

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->onBackPressed()V

    goto/16 :goto_0

    .line 909
    :pswitch_data_0
    .packed-switch 0x7f0900bb
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const v10, 0x7f030018

    const v9, 0x7f030016

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 103
    const-string v0, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v1, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 104
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    .line 105
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    .line 106
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    if-nez v2, :cond_0

    .line 107
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No device found with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->finish()V

    .line 176
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    .line 114
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    .line 115
    if-eqz v0, :cond_1

    const v0, 0x7f020108

    .line 116
    :goto_1
    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setIcon(I)V

    .line 117
    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 119
    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 121
    const v0, 0x7f0a0277

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setTitle(I)V

    .line 122
    const-string v0, "MODE_EXTRA_KEY"

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    .line 123
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {p0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 126
    if-nez v0, :cond_2

    .line 127
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    const v2, 0x7f0a0322

    invoke-static {v0, v7, v2, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_1
    const v0, 0x7f020109

    goto :goto_1

    .line 130
    :cond_2
    invoke-static {p0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->f()Landroid/accounts/Account;

    move-result-object v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->e:Ljava/lang/String;

    .line 134
    :try_start_0
    new-instance v0, Lcom/sec/android/safe/SecureStorageManagerClient;

    invoke-direct {v0, p0}, Lcom/sec/android/safe/SecureStorageManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    .line 135
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->n:Lcom/sec/android/safe/SecureStorageManagerClient;

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->e:Ljava/lang/String;

    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$c;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;B)V

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/safe/SecureStorageManagerClient;->initValues(Ljava/lang/String;Landroid/os/Handler;ILjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    iget v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d:I

    packed-switch v0, :pswitch_data_0

    .line 169
    invoke-virtual {p0, v9}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setContentView(I)V

    .line 170
    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b()V

    .line 175
    :goto_2
    if-nez v6, :cond_3

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    :goto_3
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    const v1, 0x7f030026

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0900bc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0900bb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->p:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0900be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0177

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loading SecureStorageManagerClient error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const v0, 0x7f0a027e

    new-array v1, v7, [Ljava/lang/Object;

    invoke-direct {p0, v7, v0, v8, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(IIZ[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 149
    :pswitch_0
    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setContentView(I)V

    .line 150
    invoke-direct {p0, v8}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Z)V

    goto :goto_2

    .line 153
    :pswitch_1
    invoke-virtual {p0, v9}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setContentView(I)V

    .line 154
    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b()V

    goto :goto_2

    .line 157
    :pswitch_2
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setContentView(I)V

    .line 158
    invoke-direct {p0, v8}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->b(Z)V

    goto/16 :goto_2

    .line 161
    :pswitch_3
    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setContentView(I)V

    .line 162
    invoke-direct {p0, v8}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Z)V

    goto/16 :goto_2

    .line 165
    :pswitch_4
    invoke-virtual {p0, v9}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->setContentView(I)V

    .line 166
    invoke-direct {p0, v8}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->d(Z)V

    goto/16 :goto_2

    :cond_3
    move-object v0, v6

    goto/16 :goto_3

    .line 147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 881
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 939
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 940
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 941
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/AsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 944
    :cond_0
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 946
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 947
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 886
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g:Z

    if-nez v0, :cond_0

    .line 887
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 903
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 889
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->finish()V

    .line 890
    const/4 v0, 0x1

    goto :goto_0

    .line 887
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
