.class final Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment$c;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic b:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Landroid/content/Context;

.field final synthetic f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 536
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iput-object p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->a:Lcom/mfluent/asp/datamodel/Device;

    iput-object p3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->b:Lcom/mfluent/asp/datamodel/Device;

    iput-object p4, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->d:Z

    iput-object p6, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->e:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f0a00b4

    const v5, 0x7f0a03cf

    const/4 v4, 0x0

    .line 540
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->a:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->b:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_2

    .line 541
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->b()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "targetDevice == null || sourceDevice == null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 542
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v5, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    .line 582
    :cond_1
    :goto_0
    return-void

    .line 546
    :cond_2
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 549
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v5, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 552
    :cond_4
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-nez v0, :cond_5

    .line 553
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v6, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 558
    :cond_5
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 559
    if-eqz v0, :cond_6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 560
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v5, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 563
    :cond_7
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-nez v0, :cond_8

    .line 564
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v6, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 568
    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 569
    const-string v1, "sessionId"

    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 571
    iget-boolean v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->d:Z

    if-eqz v1, :cond_9

    .line 572
    const-string v1, "isAutoUpload"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 575
    :cond_9
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->e:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 576
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_1

    .line 579
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;->f:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->g(Lcom/mfluent/asp/ui/FileTransferListFragment;)V

    goto/16 :goto_0
.end method
