.class public Lcom/mfluent/asp/ui/StartDocumentViewerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;,
        Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b:Z

    .line 264
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a(Ljava/io/File;)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 6

    .prologue
    const v5, 0x7f0a0167

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 112
    invoke-static {p1}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 113
    if-nez v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v3, v5, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    .line 133
    :goto_0
    return-void

    .line 118
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setDataAndTypeAndNormalize(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    :goto_1
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b:Z

    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    iput-boolean v4, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b:Z

    .line 131
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v3, v5, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private b()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 197
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromIntent(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 198
    if-nez v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "rowId"

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 200
    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 201
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    aput-wide v2, v0, v1

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 205
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 167
    packed-switch p1, :pswitch_data_0

    .line 192
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected requestCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 171
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 172
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_HOMESYNC_IS_PRIVATE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncSecureTransfer:Z

    .line 174
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v0

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0a01d2

    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v0

    .line 181
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 182
    const-string v2, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 183
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->setResult(I)V

    .line 186
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->finish()V

    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x7f0a0166
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    packed-switch p1, :pswitch_data_0

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 139
    :pswitch_0
    invoke-virtual {p0, p2, p3}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->setResult(ILandroid/content/Intent;)V

    .line 140
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b:Z

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->finish()V

    goto :goto_0

    .line 146
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 147
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getLocalPathsFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 148
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    sget-object v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Expected one local path in modal download result"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 157
    :cond_1
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->setResult(I)V

    .line 158
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->finish()V

    goto :goto_0

    .line 153
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a(Ljava/io/File;)V

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    if-eqz p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->b()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 64
    if-nez v0, :cond_1

    .line 65
    sget-object v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Intent missing required extras to make {}"

    const-class v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->setResult(I)V

    .line 67
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->finish()V

    goto :goto_0

    .line 71
    :cond_1
    new-instance v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "preparing"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$PreparingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
