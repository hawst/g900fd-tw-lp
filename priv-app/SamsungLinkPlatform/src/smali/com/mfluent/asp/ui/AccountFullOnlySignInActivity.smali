.class public Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/dialog/a;


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    const/16 v1, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 85
    return-void
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 72
    const v0, 0x7f0a03de

    if-ne p1, v0, :cond_0

    .line 73
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 74
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a()V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->finish()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 54
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::onActivityResult: requestCode={} resultCode={}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 55
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 57
    const/16 v0, 0x12c

    if-ne p1, v0, :cond_0

    .line 60
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 61
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::onActivityResult: device removed. Calling background sign-in"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 62
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->signIn()V

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->finish()V

    .line 68
    :cond_0
    return-void

    .line 64
    :cond_1
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::onActivityResult: device not removed."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f0a03de

    .line 28
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::onCreate"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 32
    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->setTheme(I)V

    .line 37
    :goto_0
    if-nez p1, :cond_0

    .line 38
    sget-object v0, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::onCreate: launching AccountFullRegistrationActivity"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 39
    const-string v0, "VZW"

    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a03e1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0026

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    .line 50
    :cond_0
    :goto_1
    return-void

    .line 34
    :cond_1
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->setTheme(I)V

    goto :goto_0

    .line 47
    :cond_2
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullOnlySignInActivity;->a()V

    goto :goto_1
.end method
