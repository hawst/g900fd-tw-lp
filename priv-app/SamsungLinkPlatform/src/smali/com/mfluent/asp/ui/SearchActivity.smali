.class public Lcom/mfluent/asp/ui/SearchActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/mfluent/asp/ui/DeleteActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/SearchActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 38
    .line 39
    sget-object v1, Lcom/mfluent/asp/ui/SearchActivity;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setSearchStateFromIntent : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 40
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_1

    .line 43
    new-instance v2, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;

    invoke-direct {v2}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;-><init>()V

    .line 44
    invoke-virtual {v2, p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->initContext(Landroid/content/Context;)V

    .line 45
    invoke-virtual {v2, v1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->setUri(Landroid/net/Uri;)Z

    .line 46
    invoke-virtual {v2}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->loadDataSynchronously()V

    .line 47
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->setSingleSelectedRow(I)V

    .line 48
    invoke-static {}, Lcom/mfluent/asp/ui/NativePlayerStarter;->a()Lcom/mfluent/asp/ui/NativePlayerStarter;

    invoke-static {v2, p0}, Lcom/mfluent/asp/ui/NativePlayerStarter;->a(Lcom/mfluent/asp/common/content/ContentAdapter;Landroid/app/Activity;)Z

    move-result v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SearchActivity;->finish()V

    .line 53
    :cond_0
    invoke-virtual {v2}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->destroy()V

    .line 56
    :cond_1
    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/SearchActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/mfluent/asp/ui/SearchActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "onNewIntent"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/SearchActivity;->setIntent(Landroid/content/Intent;)V

    .line 34
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/SearchActivity;->a(Landroid/content/Intent;)Z

    .line 35
    return-void
.end method
