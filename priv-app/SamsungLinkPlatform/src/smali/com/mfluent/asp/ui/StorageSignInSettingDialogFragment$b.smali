.class final Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V
    .locals 1

    .prologue
    .line 770
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 771
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1, v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 772
    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;B)V
    .locals 0

    .prologue
    .line 768
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 782
    const/4 v0, 0x0

    .line 785
    :goto_0
    return v0

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->f(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 795
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 800
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const v8, 0x7f09009f

    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 806
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    .line 809
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->g(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v4}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0350

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v5, v2

    .line 820
    :goto_0
    if-nez p2, :cond_1

    .line 823
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 824
    const v4, 0x7f030022

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 826
    new-instance v4, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;

    invoke-direct {v4, v3}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;-><init>(B)V

    .line 827
    const v1, 0x7f09009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->a:Landroid/widget/TextView;

    .line 828
    const v1, 0x7f0900a0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->b:Landroid/widget/TextView;

    .line 829
    const v1, 0x7f09009b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v4, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->c:Landroid/widget/ImageView;

    .line 830
    const v1, 0x7f09009d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->d:Landroid/widget/TextView;

    .line 831
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v4

    .line 839
    :goto_1
    iget-object v4, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 841
    iget-object v4, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->b:Landroid/widget/TextView;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 844
    iget-object v4, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 846
    iget-object v4, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 848
    if-nez v5, :cond_5

    .line 850
    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v4}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v4

    .line 851
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->c:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v1, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->c:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v6, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 854
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 855
    const v4, 0x7f0a0045

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 857
    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v4}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-nez v4, :cond_3

    .line 911
    :cond_0
    :goto_2
    return-object p2

    .line 832
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 833
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;

    move v2, v3

    goto :goto_1

    .line 835
    :cond_2
    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    const-string v1, "convertView.getTag() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 861
    :cond_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 862
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b$1;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 872
    if-eqz v2, :cond_0

    .line 873
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 874
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 876
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    .line 879
    :cond_5
    iget-object v4, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 880
    iget-object v1, v1, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$c;->c:Landroid/widget/ImageView;

    const v4, 0x7f020032

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 882
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 883
    const v4, 0x7f0a02dd

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 885
    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v4}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 889
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 890
    new-instance v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b$2;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 903
    if-eqz v2, :cond_0

    .line 904
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->h(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 905
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 907
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$b;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->i(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_7
    move v5, v3

    goto/16 :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 776
    const/4 v0, 0x0

    return v0
.end method
