.class public Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/LicenseInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LicenseWebViewClient"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/LicenseInfoActivity;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/LicenseInfoActivity;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;->a:Lcom/mfluent/asp/ui/LicenseInfoActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;->a:Lcom/mfluent/asp/ui/LicenseInfoActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->b(Lcom/mfluent/asp/ui/LicenseInfoActivity;)V

    .line 135
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 127
    iget-object v0, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;->a:Lcom/mfluent/asp/ui/LicenseInfoActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->a(Lcom/mfluent/asp/ui/LicenseInfoActivity;)V

    .line 129
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 106
    const-string v0, "mailto:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 108
    iget-object v1, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;->a:Lcom/mfluent/asp/ui/LicenseInfoActivity;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 109
    invoke-virtual {p1}, Landroid/webkit/WebView;->reload()V

    .line 114
    :goto_0
    return v3

    .line 113
    :cond_0
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
