.class public final enum Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/UpgradeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field public static final enum b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field public static final enum c:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field public static final enum d:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field public static final enum e:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field public static final enum f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field private static final synthetic g:[Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    const-string v1, "CHECKING_DOWNLOAD_AVAILABLE"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 57
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    const-string v1, "DOWNLOADING_APK"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 59
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    const-string v1, "INSTALLING_APK"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->c:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 61
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->d:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 63
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->e:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 65
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    const-string v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    .line 53
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->c:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->d:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->e:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->g:[Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->g:[Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    invoke-virtual {v0}, [Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    return-object v0
.end method
