.class final Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->a()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

.field final synthetic b:Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;

    iput-object p2, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 535
    invoke-static {}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Starting delete device task"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 537
    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "deviceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 538
    const/4 v0, 0x0

    .line 540
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->deregisterDevice(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 544
    :goto_0
    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {v1, v0}, Ljava/lang/Boolean;-><init>(Z)V

    return-object v1

    .line 541
    :catch_0
    move-exception v1

    .line 542
    invoke-static {}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "Failed to deregisterDevice"

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 528
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 528
    check-cast p1, Ljava/lang/Boolean;

    invoke-static {}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Delete task complete {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;

    iput-object p1, v0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->a:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment$1;->b:Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity$DeleteDeviceTaskFragment;->c()V

    return-void
.end method
