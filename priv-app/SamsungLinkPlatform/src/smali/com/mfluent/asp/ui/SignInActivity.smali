.class public Lcom/mfluent/asp/ui/SignInActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/SignInActivity$a;
    }
.end annotation


# static fields
.field private static final b:Lorg/slf4j/Logger;


# instance fields
.field protected a:Landroid/content/BroadcastReceiver;

.field private c:Landroid/view/View;

.field private d:Z

.field private e:Z

.field private f:Z

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/mfluent/asp/ui/SignInActivity$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/mfluent/asp/ui/SignInActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/SignInActivity;->b:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->f:Z

    .line 50
    new-instance v0, Lcom/mfluent/asp/ui/SignInActivity$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/SignInActivity$1;-><init>(Lcom/mfluent/asp/ui/SignInActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->a:Landroid/content/BroadcastReceiver;

    .line 176
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->g:Landroid/os/Handler;

    .line 178
    new-instance v0, Lcom/mfluent/asp/ui/SignInActivity$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/SignInActivity$a;-><init>(Lcom/mfluent/asp/ui/SignInActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->h:Lcom/mfluent/asp/ui/SignInActivity$a;

    .line 180
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->a()V

    .line 212
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->f:Z

    if-nez v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v2, p1, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    .line 215
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SignInActivity;)V
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f0a0054

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/SignInActivity;)V
    .locals 3

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->d:Z

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mfluent/asp/ui/AccountFullRegistrationActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/16 v0, 0x66

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/ui/SignInActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method static synthetic c()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/mfluent/asp/ui/SignInActivity;->b:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 158
    invoke-static {}, Lcom/mfluent/asp/util/r;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const v0, 0x7f0a0042

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->a(I)V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->h:Lcom/mfluent/asp/ui/SignInActivity$a;

    const-wide/32 v2, 0x15f90

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 167
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->signIn()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->h:Lcom/mfluent/asp/ui/SignInActivity$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 208
    return-void
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 253
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 219
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 222
    packed-switch p1, :pswitch_data_0

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 224
    :pswitch_0
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->e:Z

    .line 231
    sget-object v0, Lcom/mfluent/asp/ui/SignInActivity;->b:Lorg/slf4j/Logger;

    const-string v1, "::onActivityResult: Samsung Account Sign-in complete"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 232
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->samsungAccountExists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->finish()V

    goto :goto_0

    .line 238
    :pswitch_1
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->d:Z

    .line 241
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 242
    invoke-direct {p0}, Lcom/mfluent/asp/ui/SignInActivity;->d()V

    goto :goto_0

    .line 245
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->setResult(I)V

    .line 246
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->finish()V

    goto :goto_0

    .line 222
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    sget-object v0, Lcom/mfluent/asp/ui/SignInActivity;->b:Lorg/slf4j/Logger;

    const-string v1, "::onCreate"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_0

    .line 92
    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->setTheme(I)V

    .line 97
    :goto_0
    const v0, 0x7f030025

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->setContentView(I)V

    .line 98
    const v0, 0x7f09006b

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->c:Landroid/view/View;

    .line 100
    const-string v0, "com.mfluent.asp.ui.SignInActivity.SAVED_INSTANCE_ARG_SAMSUNG_ACCOUNT_ACTIVITY_ACTIVE"

    new-array v1, v3, [Landroid/os/Bundle;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/mfluent/asp/util/c;->a(Ljava/lang/String;[Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->e:Z

    .line 101
    const-string v0, "com.mfluent.asp.ui.SignInActivity.SAVED_INSTANCE_ARG_ACCOUNT_FULL_ACTIVITY_ACTIVE"

    new-array v1, v3, [Landroid/os/Bundle;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/mfluent/asp/util/c;->a(Ljava/lang/String;[Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->d:Z

    .line 102
    return-void

    .line 94
    :cond_0
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SignInActivity;->setTheme(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lcom/mfluent/asp/ui/SignInActivity;->b:Lorg/slf4j/Logger;

    const-string v1, "::onPause"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 144
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 145
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->a()V

    .line 146
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 147
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 106
    sget-object v0, Lcom/mfluent/asp/ui/SignInActivity;->b:Lorg/slf4j/Logger;

    const-string v1, "::onResume"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->f:Z

    .line 108
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 109
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 110
    const-string v1, "com.mfluent.asp.AccessManager.BROADCAST_SIGN_IN_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 111
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/SignInActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 113
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    .line 114
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->finish()V

    .line 118
    const-string v0, "INFO"

    const-string v1, "isSignIn OK"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->samsungAccountExists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    invoke-direct {p0}, Lcom/mfluent/asp/ui/SignInActivity;->d()V

    .line 122
    const-string v0, "INFO"

    const-string v1, "doSignIn OK"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_2
    iget-boolean v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->e:Z

    if-nez v1, :cond_0

    .line 126
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getSamsungAccountSignInIntent()Landroid/content/Intent;

    move-result-object v0

    .line 128
    const/16 v1, 0x65

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/SignInActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_1
    iput-boolean v3, p0, Lcom/mfluent/asp/ui/SignInActivity;->e:Z

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 131
    const v0, 0x7f0a0322

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 132
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SignInActivity;->finish()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SignInActivity;->f:Z

    .line 152
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 153
    const-string v0, "com.mfluent.asp.ui.SignInActivity.SAVED_INSTANCE_ARG_SAMSUNG_ACCOUNT_ACTIVITY_ACTIVE"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 154
    const-string v0, "com.mfluent.asp.ui.SignInActivity.SAVED_INSTANCE_ARG_ACCOUNT_FULL_ACTIVITY_ACTIVE"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/SignInActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 155
    return-void
.end method
