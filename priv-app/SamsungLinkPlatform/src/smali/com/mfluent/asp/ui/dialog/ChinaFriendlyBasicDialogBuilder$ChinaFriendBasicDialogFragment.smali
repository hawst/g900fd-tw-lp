.class public Lcom/mfluent/asp/ui/dialog/ChinaFriendlyBasicDialogBuilder$ChinaFriendBasicDialogFragment;
.super Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/dialog/ChinaFriendlyBasicDialogBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChinaFriendBasicDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/ChinaFriendlyBasicDialogBuilder$ChinaFriendBasicDialogFragment;->getResources()Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/ChinaFriendlyBasicDialogBuilder$ChinaFriendBasicDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v0, "ARG_MESSAGE_ID"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v0, "ARG_MESSAGE_ARGS"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    if-eqz v1, :cond_0

    const-string v2, "CHINA_ARG_MESSAGE_ID"

    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "CHINA_ARG_MESSAGE_ARGS"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v2, "ARG_MESSAGE_ID"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v2, "ARG_MESSAGE_ARGS"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    move v2, v1

    move-object v1, v0

    :goto_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0, v2, v1}, Lcom/mfluent/asp/ASPApplication;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ARG_MESSAGE"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    .line 48
    :cond_0
    const-string v0, "CHINA_ARG_MESSAGE_ID"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v0, "CHINA_ARG_MESSAGE_ARGS"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    move v2, v1

    move-object v1, v0

    goto :goto_0
.end method
