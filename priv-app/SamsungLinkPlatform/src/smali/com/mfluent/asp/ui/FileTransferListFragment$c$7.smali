.class final Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment$c;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

.field final synthetic b:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic c:J

.field final synthetic d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;Lcom/mfluent/asp/datamodel/Device;J)V
    .locals 0

    .prologue
    .line 626
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iput-object p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    iput-object p3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->b:Lcom/mfluent/asp/datamodel/Device;

    iput-wide p4, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 630
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    if-eq v0, v1, :cond_1

    .line 651
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->b:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 635
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a019c

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 639
    :cond_3
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 640
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->b(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->c:J

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils;->createLaunchToDeviceIntent(JJ)Landroid/content/Intent;

    move-result-object v0

    .line 646
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v1, v1, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 650
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 648
    :catch_0
    move-exception v1

    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->b()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Activity not found when attempting to launch to device with intent {}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 650
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v1, v1, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    throw v0
.end method
