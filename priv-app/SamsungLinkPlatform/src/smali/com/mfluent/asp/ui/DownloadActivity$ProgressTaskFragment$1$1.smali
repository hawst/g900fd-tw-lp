.class final Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$1;->a:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 212
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 213
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$1;->a:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v1, v1, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Z

    .line 242
    :goto_0
    return v0

    .line 219
    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 220
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 222
    packed-switch v0, :pswitch_data_0

    .line 234
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 237
    :goto_1
    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 239
    iget-object v3, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$1;->a:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v3, v3, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-static {v3}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->d(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    iget-object v2, p0, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1$1;->a:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;

    iget-object v2, v2, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;

    invoke-static {v2}, Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;->c(Lcom/mfluent/asp/ui/DownloadActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 242
    goto :goto_0

    .line 225
    :pswitch_0
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 228
    :pswitch_1
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 231
    :pswitch_2
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 222
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
