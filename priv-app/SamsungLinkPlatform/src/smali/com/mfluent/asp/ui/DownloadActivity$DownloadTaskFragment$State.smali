.class final enum Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field public static final enum b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field public static final enum c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field public static final enum d:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field public static final enum e:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field public static final enum f:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field public static final enum g:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field private static final synthetic h:[Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 519
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 520
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "ERROR_DEVICE_FULL"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 521
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "ERROR_RETRYABLE"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 522
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "ERROR_FATAL"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->d:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 523
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "SCANNING"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->e:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 524
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "COMPLETE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->f:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 525
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    const-string v1, "DOWNLOADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->g:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 518
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    sget-object v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->d:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->e:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->f:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->g:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->h:[Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;
    .locals 1

    .prologue
    .line 518
    const-class v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;
    .locals 1

    .prologue
    .line 518
    sget-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->h:[Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    invoke-virtual {v0}, [Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    return-object v0
.end method
