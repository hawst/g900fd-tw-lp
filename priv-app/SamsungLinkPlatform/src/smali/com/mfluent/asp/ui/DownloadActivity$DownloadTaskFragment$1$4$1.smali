.class final Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;->onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 720
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    iput-object p2, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 725
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 731
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "onScanCompleted {}, {}"

    iget-object v2, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->b:Landroid/net/Uri;

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 732
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 733
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scanning failed for file "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 734
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Runnable;

    const/4 v2, 0x0

    new-instance v3, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1$1;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1$1;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;[Ljava/lang/Object;)V

    .line 746
    :goto_0
    return-void

    .line 726
    :catch_0
    move-exception v0

    .line 727
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Interrupted while waiting for initialization to complete"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 743
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->c(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 744
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4$1;->c:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    goto :goto_0
.end method
