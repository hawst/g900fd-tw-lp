.class final Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;

    iput-object p2, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 322
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 324
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 325
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 331
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    .line 332
    iget-object v2, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;

    iget-object v2, v2, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    invoke-static {v2}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->d(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 340
    :goto_1
    return v0

    .line 328
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;->a:Ljava/io/File;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;

    iget-object v0, v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->e(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z

    .line 336
    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;->b:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;

    iget-object v0, v0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->a(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 337
    const/4 v0, 0x0

    goto :goto_1
.end method
