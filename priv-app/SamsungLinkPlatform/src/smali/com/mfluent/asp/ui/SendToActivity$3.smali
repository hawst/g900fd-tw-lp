.class final Lcom/mfluent/asp/ui/SendToActivity$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic b:Lcom/mfluent/asp/ui/SendToActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 1094
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    iput-object p2, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1099
    invoke-static {}, Lcom/mfluent/asp/ui/SendToActivity;->a()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "Enter ::updateDevice()::OnClickListener()::onClick()"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1101
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1102
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1103
    new-instance v1, Lcom/mfluent/asp/ui/SendToActivity$3$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/SendToActivity$3$1;-><init>(Lcom/mfluent/asp/ui/SendToActivity$3;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1134
    :goto_0
    return-void

    .line 1112
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v2, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;Z)Z

    .line 1114
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->a:Lcom/mfluent/asp/datamodel/Device;

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1116
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;

    invoke-direct {v2}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;-><init>()V

    .line 1117
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1118
    const-string v4, "DEVICE_ID_EXTRA_KEY"

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1119
    const-string v4, "HOMESYNC_HIDEPRIVATE"

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v5}, Lcom/mfluent/asp/ui/SendToActivity;->b(Lcom/mfluent/asp/ui/SendToActivity;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1122
    const-string v4, "HOMESYNC_OWNTRANSFER"

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v5}, Lcom/mfluent/asp/ui/SendToActivity;->c(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v6

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_1

    :goto_1
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1125
    const-string v0, "HOMESYNC_SOURCEPERSONAL"

    iget-object v4, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v4}, Lcom/mfluent/asp/ui/SendToActivity;->d(Lcom/mfluent/asp/ui/SendToActivity;)Z

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1126
    const-string v0, "HOMESYNC_SOURCEPRIVATE"

    iget-object v4, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v4}, Lcom/mfluent/asp/ui/SendToActivity;->e(Lcom/mfluent/asp/ui/SendToActivity;)Z

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1127
    const-string v0, "HOMESYNC_FROMFILETAB"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1129
    invoke-virtual {v2, v3}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1130
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "HomeSyncDialogFragment"

    invoke-virtual {v2, v0, v1}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1122
    goto :goto_1

    .line 1132
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->a:Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$3;->b:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->e(Lcom/mfluent/asp/ui/SendToActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    :goto_2
    invoke-static {v1, v2, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
