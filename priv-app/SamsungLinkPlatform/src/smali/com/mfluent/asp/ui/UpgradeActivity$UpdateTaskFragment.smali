.class public Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/UpgradeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateTaskFragment"
.end annotation


# instance fields
.field a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

.field private b:J

.field private c:J

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)J
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)J
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->e:I

    return v0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->d:Z

    return v0
.end method


# virtual methods
.method protected final a()Landroid/os/AsyncTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/os/Bundle;",
            "**>;"
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/UpgradeActivity;

    .line 270
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/UpgradeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 272
    new-instance v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment$1;-><init>(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;Landroid/content/Context;)V

    return-object v1
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 426
    iput p1, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->e:I

    .line 427
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->c()V

    .line 428
    return-void
.end method

.method public final a(JJ)V
    .locals 1

    .prologue
    .line 419
    iput-wide p1, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->b:J

    .line 420
    iput-wide p3, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->c:J

    .line 421
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->c()V

    .line 422
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->d:Z

    .line 265
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->c()V

    .line 415
    return-void
.end method
