.class final Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/mfluent/asp/datamodel/Device;",
        "Ljava/lang/Void;",
        "Lcom/mfluent/asp/datamodel/Device;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/b/h;

.field final synthetic b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/b/h;)V
    .locals 0

    .prologue
    .line 761
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    iput-object p2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;->a:Lcom/mfluent/asp/b/h;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 761
    check-cast p1, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_0

    const-string v1, "mfl_StorageServiceSignInActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doInBackground: adding: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 761
    check-cast p1, Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$7;->a:Lcom/mfluent/asp/b/h;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;ILcom/mfluent/asp/b/h;)V

    return-void
.end method
