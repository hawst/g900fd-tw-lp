.class final enum Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ReturnedError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

.field public static final enum b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

.field private static final synthetic c:[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    const-string v1, "NO_ERROR_RETURNED"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    .line 91
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    const-string v1, "EMAIL_PW_INVALID"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    .line 89
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->c:[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->c:[Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    invoke-virtual {v0}, [Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    return-object v0
.end method
