.class public Lcom/mfluent/asp/ui/SendToActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/SendToActivity$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static d:I


# instance fields
.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/mfluent/asp/datamodel/Device;

.field private f:Lcom/mfluent/asp/datamodel/Device;

.field private g:I

.field private h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

.field private i:J

.field private j:J

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Landroid/content/Intent;

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:I

.field private x:Z

.field private final y:Lcom/mfluent/asp/ui/SendToActivity$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    .line 80
    const/4 v0, -0x1

    sput v0, Lcom/mfluent/asp/ui/SendToActivity;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    .line 85
    iput v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->g:I

    .line 89
    iput-wide v4, p0, Lcom/mfluent/asp/ui/SendToActivity;->i:J

    .line 91
    iput-wide v4, p0, Lcom/mfluent/asp/ui/SendToActivity;->j:J

    .line 93
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->k:Z

    .line 114
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->l:Z

    .line 116
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->m:Z

    .line 118
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->n:Z

    .line 122
    iput v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->p:I

    .line 124
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->q:Z

    .line 126
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->r:Z

    .line 136
    iput v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->w:I

    .line 138
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->x:Z

    .line 325
    new-instance v0, Lcom/mfluent/asp/ui/SendToActivity$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/SendToActivity$a;-><init>(Lcom/mfluent/asp/ui/SendToActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->y:Lcom/mfluent/asp/ui/SendToActivity$a;

    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 71
    sput p0, Lcom/mfluent/asp/ui/SendToActivity;->d:I

    return p0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SendToActivity;I)I
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/mfluent/asp/ui/SendToActivity;->g:I

    return p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SendToActivity;J)J
    .locals 1

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/mfluent/asp/ui/SendToActivity;->j:J

    return-wide p1
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/mfluent/asp/datamodel/Device;"
        }
    .end annotation

    .prologue
    .line 947
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 948
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 949
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 950
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v2, v3, :cond_0

    .line 951
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 954
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 955
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 959
    :goto_1
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private a(ILcom/mfluent/asp/datamodel/Device;)V
    .locals 8

    .prologue
    const v5, 0x7f0a031d

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 731
    packed-switch p1, :pswitch_data_0

    .line 744
    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 749
    :goto_0
    invoke-virtual {v1, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 751
    const v0, 0x7f090012

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 752
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 753
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 755
    const v0, 0x7f090011

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 756
    invoke-static {p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v2

    .line 757
    sget-object v3, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->f:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 766
    const v0, 0x7f09008a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 768
    invoke-direct {p0, p2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 769
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 771
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    sget v3, Lcom/mfluent/asp/ui/SendToActivity;->d:I

    if-ne v2, v3, :cond_0

    .line 772
    const/4 v2, -0x1

    sput v2, Lcom/mfluent/asp/ui/SendToActivity;->d:I

    .line 775
    :cond_0
    invoke-direct {p0, p2}, Lcom/mfluent/asp/ui/SendToActivity;->b(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 776
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 777
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getUsedCapacityInBytes()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 778
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 779
    const-string v4, "%s/%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v7

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 780
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 790
    :cond_1
    :goto_1
    new-instance v0, Lcom/mfluent/asp/ui/SendToActivity$1;

    invoke-direct {v0, p0, p2}, Lcom/mfluent/asp/ui/SendToActivity$1;-><init>(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 824
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_2

    .line 825
    const v0, 0x7f02010b

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 856
    :cond_2
    :goto_2
    invoke-virtual {v1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 857
    return-void

    .line 737
    :pswitch_0
    const v0, 0x7f090084

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 738
    goto/16 :goto_0

    .line 740
    :pswitch_1
    const v0, 0x7f090085

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 741
    goto/16 :goto_0

    .line 784
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 829
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 834
    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 836
    const v3, 0x7f0a0321

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 838
    :cond_5
    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 839
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v3, v4, :cond_6

    .line 840
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 841
    :cond_6
    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v3, v4, :cond_8

    .line 842
    const v3, 0x7f0a031b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 846
    :cond_7
    invoke-direct {p0, p2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 847
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v3, v4, :cond_8

    .line 848
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 850
    :cond_8
    const v3, 0x7f0a031c

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 731
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ILcom/mfluent/asp/datamodel/Device;Z)V
    .locals 10

    .prologue
    .line 1007
    packed-switch p1, :pswitch_data_0

    .line 1028
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Bad device index:{}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1032
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {p2, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1033
    invoke-static {}, Lcom/mfluent/asp/datamodel/au;->a()Lcom/mfluent/asp/datamodel/au;

    move-result-object v0

    .line 1034
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mfluent/asp/datamodel/au;->a:Z

    .line 1191
    :cond_0
    :goto_0
    return-void

    .line 1010
    :pswitch_0
    const v0, 0x7f09007e

    move v1, v0

    .line 1039
    :goto_1
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1041
    if-eqz p3, :cond_d

    .line 1042
    const v0, 0x7f090011

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1043
    const v0, 0x7f090012

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1045
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1046
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1048
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v4

    .line 1049
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/mfluent/asp/ui/DeviceHelper;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;Z)I

    move-result v2

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Landroid/view/View;->setFocusable(Z)V

    const v1, 0x7f090011

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    const v1, 0x7f090089

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 1051
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1053
    const v1, 0x7f09008a

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1054
    if-nez v4, :cond_3

    .line 1055
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1169
    :cond_1
    :goto_2
    if-eqz v4, :cond_c

    .line 1170
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0200e5

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1171
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1173
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 1174
    const v0, 0x7f02010b

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1184
    :cond_2
    :goto_3
    invoke-virtual {v3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1013
    :pswitch_1
    const v0, 0x7f09007f

    move v1, v0

    .line 1014
    goto/16 :goto_1

    .line 1016
    :pswitch_2
    const v0, 0x7f090080

    move v1, v0

    .line 1017
    goto/16 :goto_1

    .line 1019
    :pswitch_3
    const v0, 0x7f090081

    move v1, v0

    .line 1020
    goto/16 :goto_1

    .line 1022
    :pswitch_4
    const v0, 0x7f090082

    move v1, v0

    .line 1023
    goto/16 :goto_1

    .line 1025
    :pswitch_5
    const v0, 0x7f090083

    move v1, v0

    .line 1026
    goto/16 :goto_1

    .line 1060
    :cond_3
    invoke-direct {p0, p2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1061
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-nez v2, :cond_5

    .line 1062
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1082
    :goto_4
    const v2, 0x7f090075

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1083
    invoke-static {p0, p2}, Lcom/mfluent/asp/ui/DeviceHelper;->a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)Ljava/lang/String;

    move-result-object v5

    .line 1084
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1087
    :cond_4
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1094
    :goto_5
    new-instance v2, Lcom/mfluent/asp/ui/SendToActivity$3;

    invoke-direct {v2, p0, p2}, Lcom/mfluent/asp/ui/SendToActivity$3;-><init>(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 1064
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1065
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getUsedCapacityInBytes()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 1066
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 1067
    const-string v6, "%s/%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    aput-object v5, v7, v2

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1068
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 1089
    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1090
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 1143
    :cond_7
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1149
    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v5}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 1150
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_8

    .line 1151
    const v5, 0x7f0a031d

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1152
    :cond_8
    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_9

    .line 1153
    const v5, 0x7f0a031e

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/mfluent/asp/ui/SendToActivity;->f:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1155
    :cond_9
    const v5, 0x7f0a031b

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1157
    :cond_a
    invoke-direct {p0, p2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1158
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_b

    .line 1159
    const v5, 0x7f0a031d

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1161
    :cond_b
    const v5, 0x7f0a031c

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1178
    :cond_c
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    .line 1186
    :cond_d
    const v0, 0x7f090011

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1187
    const v0, 0x7f090012

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1007
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1267
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v3, "Enter ::proceedWithSendTo(targetDevice:{}, homeSyncDestination:{})"

    invoke-interface {v0, v3, p1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1269
    invoke-static {p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v0

    .line 1271
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p1, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ndrive"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1275
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v2, 0x7f0a041f

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    .line 1349
    :goto_0
    return-void

    .line 1279
    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getUsedCapacityInBytes()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_1

    iget-wide v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->i:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    :cond_1
    move v0, v2

    :goto_1
    if-nez v0, :cond_3

    .line 1280
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    const v3, 0x7f0a0174

    invoke-static {v0, v1, v3, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1279
    goto :goto_1

    .line 1284
    :cond_3
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1285
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    .line 1286
    if-eqz v0, :cond_4

    .line 1287
    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getMaximumFileSize()J

    move-result-wide v4

    .line 1288
    cmp-long v0, v4, v10

    if-lez v0, :cond_4

    iget-wide v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->j:J

    cmp-long v0, v6, v4

    if-lez v0, :cond_4

    .line 1289
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    const v3, 0x7f0a0212

    invoke-static {v0, v1, v3, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 1295
    :cond_4
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v4, v0

    .line 1297
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->t:Z

    if-eqz v0, :cond_6

    .line 1298
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1299
    const-string v2, "deviceId"

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1300
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 1301
    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->s:Landroid/content/Intent;

    .line 1302
    invoke-virtual {p0, v8, v0}, Lcom/mfluent/asp/ui/SendToActivity;->setResult(ILandroid/content/Intent;)V

    .line 1336
    :goto_2
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->v:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "targetDeviceId"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1337
    :cond_5
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    goto/16 :goto_0

    .line 1304
    :cond_6
    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    .line 1305
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "transferOptions"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .line 1306
    sget-object v6, Lcom/mfluent/asp/ui/HomeSyncDestination;->b:Lcom/mfluent/asp/ui/HomeSyncDestination;

    if-ne p2, v6, :cond_8

    .line 1307
    iput-boolean v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncPersonalTransfer:Z

    .line 1313
    :cond_7
    :goto_3
    iget-boolean v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncSecureTransfer:Z

    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->l:Z

    if-nez v2, :cond_9

    .line 1314
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->G()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1315
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1316
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    long-to-int v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1317
    sget-object v1, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/HomeSyncDestination;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1308
    :cond_8
    sget-object v6, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    if-ne p2, v6, :cond_7

    .line 1309
    iput-boolean v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncSecureTransfer:Z

    goto :goto_3

    .line 1322
    :cond_9
    iget-boolean v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->v:Z

    iput-boolean v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->isUiAppTheme:Z

    .line 1323
    const-string v2, "mfl_SendToActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isUiAppTheme : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->isUiAppTheme:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    invoke-static {}, Lcom/mfluent/asp/util/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    new-instance v4, Lcom/mfluent/asp/ui/SendToActivity$4;

    invoke-direct {v4, p0, p1, v3, v0}, Lcom/mfluent/asp/ui/SendToActivity$4;-><init>(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    invoke-interface {v2, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 1333
    invoke-virtual {p0, v8}, Lcom/mfluent/asp/ui/SendToActivity;->setResult(I)V

    goto :goto_2

    .line 1339
    :cond_a
    const v0, 0x7f09005e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1340
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1341
    new-instance v1, Lcom/mfluent/asp/ui/SendToActivity$5;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/SendToActivity$5;-><init>(Lcom/mfluent/asp/ui/SendToActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/b/h;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 71
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {p1, v0}, Lcom/mfluent/asp/cloudstorage/a;->b(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getAuthType()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->USER_PASSWORD:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "target_sign_in_key"

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->OAUTH10:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;->OAUTH20:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$AuthType;

    if-ne v0, v1, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->y:Lcom/mfluent/asp/ui/SendToActivity$a;

    invoke-static {p0, v0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    sput v2, Lcom/mfluent/asp/ui/SendToActivity;->d:I

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "needs_reauth"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->i(Ljava/lang/String;)V

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    new-instance v1, Lcom/mfluent/asp/ui/SendToActivity$2;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/SendToActivity$2;-><init>(Lcom/mfluent/asp/ui/SendToActivity;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v3, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/ui/SendToActivity$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned an unknown authentication type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SendToActivity;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 668
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 718
    :cond_0
    :goto_0
    return v0

    .line 672
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/Device;->j()Z

    .line 674
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p1, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v2

    if-nez v2, :cond_2

    .line 678
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->r:Z

    .line 681
    :cond_2
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v2

    if-nez v2, :cond_0

    .line 684
    iget-boolean v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->l:Z

    if-eqz v2, :cond_3

    move v0, v1

    .line 686
    goto :goto_0

    .line 689
    :cond_3
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 690
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p1, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 692
    goto :goto_0

    .line 695
    :cond_4
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->B()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    .line 697
    goto :goto_0

    .line 700
    :cond_6
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 702
    goto :goto_0

    .line 705
    :cond_7
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p1, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 706
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->B()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 708
    goto :goto_0

    .line 711
    :cond_8
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 712
    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SendToActivity;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->x:Z

    return v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/SendToActivity;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/SendToActivity;->x:Z

    return p1
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 71
    sget v0, Lcom/mfluent/asp/ui/SendToActivity;->d:I

    return v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/SendToActivity;J)J
    .locals 1

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/mfluent/asp/ui/SendToActivity;->i:J

    return-wide p1
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/SendToActivity;I)V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.sync.CLOUD_SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method

.method private b(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 6

    .prologue
    .line 984
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 985
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 986
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 987
    const/4 v0, 0x0

    .line 991
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/SendToActivity;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->k:Z

    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 566
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 568
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 569
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 570
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 572
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 573
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->q:Z

    .line 576
    :cond_1
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 578
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 582
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 586
    :cond_2
    const-string v2, "CQTEST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    .line 594
    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 596
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v2

    .line 600
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->u:Z

    if-nez v2, :cond_0

    .line 608
    :cond_3
    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 612
    :cond_4
    return-void
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/SendToActivity;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->m:Z

    return v0
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/SendToActivity;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->l:Z

    return v0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    return-object v0
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/SendToActivity;)J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->j:J

    return-wide v0
.end method

.method static synthetic h(Lcom/mfluent/asp/ui/SendToActivity;)V
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0215

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1593
    packed-switch p1, :pswitch_data_0

    .line 1602
    :goto_0
    return-void

    .line 1595
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1596
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->createHowToUseViewIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1597
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->startActivity(Landroid/content/Intent;)V

    .line 1599
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    goto :goto_0

    .line 1593
    :pswitch_data_0
    .packed-switch 0x7f0a037a
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/mfluent/asp/ui/HomeSyncDestination;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1221
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::onHomeSyncDestinationSelected()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1223
    const-string v0, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1224
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 1225
    if-nez v1, :cond_0

    .line 1227
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a019c

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    .line 1238
    :goto_0
    return-void

    .line 1231
    :cond_0
    sget-object v2, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    invoke-virtual {v2, p1}, Lcom/mfluent/asp/ui/HomeSyncDestination;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1232
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1233
    const-string v2, "com.samsung.android.sdk.samsunglink.DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1234
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/ui/SendToActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1236
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    .line 1559
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1560
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const/16 v0, 0x9

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_2

    const v0, 0x7f090011

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 1563
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 1560
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :array_0
    .array-data 4
        0x7f09007e
        0x7f09007f
        0x7f090080
        0x7f090081
        0x7f090082
        0x7f090083
        0x7f090084
        0x7f090085
        0x7f090086
    .end array-data
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "broadcastResults"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.DEVICE_CHOOSER_RESULT_BROADCAST_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->s:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->s:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 278
    :cond_0
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 281
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 282
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1243
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::onActivityResult() : requestCode:{} resultCode:{} Intent:{}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1245
    packed-switch p1, :pswitch_data_0

    .line 1263
    :goto_0
    return-void

    .line 1247
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1248
    const-string v0, "com.samsung.android.sdk.samsunglink.DEVICE_ID_EXTRA_KEY"

    const/high16 v1, -0x80000000

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1249
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 1250
    if-nez v0, :cond_0

    .line 1252
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a019c

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V

    goto :goto_0

    .line 1256
    :cond_0
    sget-object v1, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    goto :goto_0

    .line 1258
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    goto :goto_0

    .line 1245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1550
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1551
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->setResult(I)V

    .line 1552
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    .line 1553
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v6, 0x7f09007b

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 144
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v3, "Enter ::onCreate()"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 147
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->requestWindowFeature(I)Z

    .line 148
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x100

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 150
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 151
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    sget-object v4, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    xor-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 152
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 156
    const-string v0, "com.samsung.android.sdk.samsunglink.APPLICATION_ID"

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->NONE:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->w:I

    .line 157
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v4, "::onCreate() : clientAppId:{}"

    iget v5, p0, Lcom/mfluent/asp/ui/SendToActivity;->w:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    const-string v0, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->v:Z

    .line 160
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->v:Z

    if-eqz v0, :cond_4

    .line 161
    const v0, 0x7f0b002e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->setTheme(I)V

    .line 170
    :goto_0
    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->setContentView(I)V

    .line 171
    const v0, 0x7f0a017b

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->setTitle(I)V

    .line 173
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 178
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 180
    invoke-static {p0}, Lcom/mfluent/asp/util/UiUtils;->b(Landroid/app/Activity;)V

    .line 183
    :cond_1
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "com.samsung.android.sdk.samsunglink.filetransfer.ChooseDevice"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->t:Z

    .line 185
    invoke-static {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromIntent(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    .line 186
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    if-nez v0, :cond_2

    .line 187
    const-string v0, "rowIds"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    .line 188
    if-eqz v0, :cond_2

    .line 189
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    .line 193
    :cond_2
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v4, "::onCreate() mediaSet:{}"

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 195
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->t:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    if-nez v0, :cond_5

    .line 197
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "mediaSet required for send to mode"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    .line 269
    :cond_3
    :goto_1
    return-void

    .line 164
    :cond_4
    const v0, 0x7f0b0011

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->setTheme(I)V

    goto :goto_0

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    if-eqz v0, :cond_7

    .line 203
    invoke-virtual {p0, v6}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 208
    :goto_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "transferOptions"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .line 209
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    if-nez v0, :cond_6

    .line 210
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .line 213
    :cond_6
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    .line 215
    const-string v0, "isFilesOrDocumentsTabSource"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->u:Z

    .line 216
    const-string v0, "isSourceSecure"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->l:Z

    .line 217
    const-string v0, "SOURCE_IS_PERSONAL"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->m:Z

    .line 218
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->t:Z

    if-eqz v0, :cond_8

    .line 219
    const-string v0, "choose_storage"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->n:Z

    .line 224
    :goto_3
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->t:Z

    if-eqz v0, :cond_a

    .line 225
    const-string v0, "deviceId"

    const-wide/16 v6, -0x1

    invoke-virtual {v3, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    long-to-int v0, v6

    .line 226
    const/4 v3, -0x1

    if-ne v0, v3, :cond_9

    .line 227
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Expected a value for {} in intent"

    const-string v2, "deviceId"

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 228
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    goto :goto_1

    .line 205
    :cond_7
    invoke-virtual {p0, v6}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 221
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->allowCloudStorageTargetDevice:Z

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->n:Z

    goto :goto_3

    .line 232
    :cond_9
    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    .line 237
    :goto_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_b

    .line 238
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Could not find source device"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 239
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    goto/16 :goto_1

    .line 234
    :cond_a
    new-instance v0, Lcom/mfluent/asp/util/x;

    invoke-direct {v0}, Lcom/mfluent/asp/util/x;-><init>()V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v0, v3, v5, v4}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    goto :goto_4

    .line 243
    :cond_b
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->k:Z

    .line 245
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "targetDeviceId"

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    .line 247
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v4, "::onCreate() isFilesOrDocumentsTabSource:{} sourceDevice:{} targetDevice:{}"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-boolean v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->u:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v2, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    aput-object v2, v5, v1

    const/4 v1, 0x2

    aput-object v3, v5, v1

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 253
    if-eqz v3, :cond_f

    .line 254
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncPersonalTransfer:Z

    if-eqz v0, :cond_d

    .line 256
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncDestination;->b:Lcom/mfluent/asp/ui/HomeSyncDestination;

    .line 262
    :goto_6
    invoke-direct {p0, v3, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 243
    goto :goto_5

    .line 257
    :cond_d
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->o:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncSecureTransfer:Z

    if-eqz v0, :cond_e

    .line 258
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    goto :goto_6

    .line 260
    :cond_e
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncDestination;->a:Lcom/mfluent/asp/ui/HomeSyncDestination;

    goto :goto_6

    .line 266
    :cond_f
    if-eqz p1, :cond_3

    .line 267
    const-string v0, "select_device"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->g:I

    goto/16 :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 381
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 389
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 384
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->finish()V

    .line 385
    const/4 v0, 0x1

    goto :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 353
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 355
    const-string v0, "select_device"

    iget v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 356
    return-void
.end method

.method protected onStart()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/16 v9, 0x8

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 329
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 330
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->f:Lcom/mfluent/asp/datamodel/Device;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v3

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v3, v5, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->E()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v3}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    sget-object v1, Lcom/mfluent/asp/ui/DeviceSorter;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    invoke-direct {p0}, Lcom/mfluent/asp/ui/SendToActivity;->c()V

    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09007d

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v3, p0, Lcom/mfluent/asp/ui/SendToActivity;->p:I

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v6, v7}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v6

    if-eqz v6, :cond_5

    const v6, 0x7f0a0321

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v4

    :goto_1
    if-nez v3, :cond_b

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iput v2, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_c

    const v0, 0x7f090078

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    const v1, 0x7f0a037a

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    const v1, 0x7f0a0026

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    const v1, 0x7f0a037a

    const v2, 0x7f0a037a

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    .line 332
    :cond_3
    :goto_3
    iget v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 333
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 334
    iget v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_18

    .line 337
    sget-object v1, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onStart selectedDevice getAliasName : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 338
    sget-object v1, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onStart selectedDevice isWebStorageSignedIn : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 339
    sget-object v1, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onStart thisSignedInStorageHasCompletedLoading : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->b(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 341
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->b(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 342
    invoke-direct {p0, v0, v10}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/ui/HomeSyncDestination;)V

    .line 348
    :cond_4
    :goto_4
    return-void

    .line 330
    :cond_5
    iget-object v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v6, v7, :cond_6

    const v6, 0x7f0a031e

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/mfluent/asp/ui/SendToActivity;->f:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v4

    goto/16 :goto_1

    :cond_6
    iget-object v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v6

    if-nez v6, :cond_7

    const v6, 0x7f0a031c

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/mfluent/asp/ui/SendToActivity;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v4

    goto/16 :goto_1

    :cond_7
    iget-boolean v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->q:Z

    if-eqz v6, :cond_8

    const v6, 0x7f0a031d

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v4

    goto/16 :goto_1

    :cond_8
    iget-boolean v6, p0, Lcom/mfluent/asp/ui/SendToActivity;->r:Z

    if-eqz v6, :cond_9

    const v6, 0x7f0a031b

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v4

    goto/16 :goto_1

    :cond_9
    move v3, v2

    goto/16 :goto_1

    :cond_a
    move v3, v2

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->p:I

    iput v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_e

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_5
    if-ge v1, v3, :cond_d

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-nez v0, :cond_d

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_d
    if-ne v1, v3, :cond_e

    const v0, 0x7f090078

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SendToActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    const v3, 0x7f0a037a

    invoke-static {v0, v2, v3, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_e
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->h:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    if-eqz v0, :cond_f

    new-instance v0, Lcom/mfluent/asp/ui/SendToActivity$6;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/SendToActivity$6;-><init>(Lcom/mfluent/asp/ui/SendToActivity;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Void;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput-object v6, v3, v5

    invoke-virtual {v0, v1, v3}, Lcom/mfluent/asp/ui/SendToActivity$6;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_f
    :goto_6
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const v1, 0x7f090084

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090085

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090086

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x3

    if-le v0, v1, :cond_10

    const/4 v0, 0x3

    :cond_10
    iget-boolean v1, p0, Lcom/mfluent/asp/ui/SendToActivity;->n:Z

    if-eqz v1, :cond_11

    packed-switch v0, :pswitch_data_0

    :cond_11
    :goto_7
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const v1, 0x7f09007e

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f09007f

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090080

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090081

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090082

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090083

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    if-lez v0, :cond_12

    const v1, 0x7f09007e

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_12
    if-lt v0, v11, :cond_13

    const v1, 0x7f09007f

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_13
    const/4 v1, 0x3

    if-lt v0, v1, :cond_14

    const v1, 0x7f090080

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_14
    const/4 v1, 0x4

    if-lt v0, v1, :cond_15

    const v1, 0x7f090081

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_15
    const/4 v1, 0x5

    if-lt v0, v1, :cond_16

    const v1, 0x7f090082

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_16
    const/4 v1, 0x6

    if-lt v0, v1, :cond_17

    const v0, 0x7f090083

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_17
    invoke-direct {p0, v2, v10, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    invoke-direct {p0, v4, v10, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    invoke-direct {p0, v11, v10, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    const/4 v0, 0x3

    invoke-direct {p0, v0, v10, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    const/4 v0, 0x4

    invoke-direct {p0, v0, v10, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    const/4 v0, 0x5

    invoke-direct {p0, v0, v10, v2}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    sget-object v1, Lcom/mfluent/asp/ui/DeviceSorter;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_8
    if-ltz v1, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v1, v0, v4}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;Z)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_8

    :catch_0
    move-exception v0

    const v1, 0x7f09007b

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_6

    :pswitch_0
    const v0, 0x7f090084

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v4, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    goto/16 :goto_7

    :pswitch_1
    const v0, 0x7f090084

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v4, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    const v0, 0x7f090085

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v11, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    goto/16 :goto_7

    :pswitch_2
    const v0, 0x7f090084

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v4, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    const v0, 0x7f090085

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v11, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, v1, v0}, Lcom/mfluent/asp/ui/SendToActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    goto/16 :goto_7

    .line 345
    :cond_18
    sget-object v0, Lcom/mfluent/asp/ui/SendToActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "onStart selectedDevice is NULL"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity;->y:Lcom/mfluent/asp/ui/SendToActivity$a;

    invoke-static {p0, v0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->b(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 366
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 367
    return-void
.end method
