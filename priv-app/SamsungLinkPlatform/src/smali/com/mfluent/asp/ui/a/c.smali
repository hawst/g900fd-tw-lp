.class public abstract Lcom/mfluent/asp/ui/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/a/c$b;,
        Lcom/mfluent/asp/ui/a/c$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:I

.field private static l:Ljava/lang/Boolean;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/mfluent/asp/ui/a/c$a;

.field private d:Z

.field private final e:Landroid/media/AudioManager;

.field private m:J

.field private n:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/mfluent/asp/ui/a/c;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->a:Lorg/slf4j/Logger;

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->l:Ljava/lang/Boolean;

    .line 64
    const-class v0, Landroid/provider/Settings$System;

    const-string v1, "FINGER_AIR_VIEW_INFORMATION_PREVIEW"

    const-string v2, "finger_air_view_information_preview"

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->f:Ljava/lang/String;

    .line 69
    const-class v0, Landroid/provider/Settings$System;

    const-string v1, "FINGER_AIR_VIEW_SOUND_AND_HAPTIC_FEEDBACK"

    const-string v2, "finger_air_view_sound_and_haptic_feedback"

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->g:Ljava/lang/String;

    .line 74
    const-class v0, Landroid/provider/Settings$System;

    const-string v1, "PEN_HOVERING"

    const-string v2, "pen_hovering"

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->h:Ljava/lang/String;

    .line 76
    const-class v0, Landroid/provider/Settings$System;

    const-string v1, "PEN_HOVERING_INFORMATION_PREVIEW"

    const-string v2, "pen_hovering_information_preview"

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->i:Ljava/lang/String;

    .line 81
    const-class v0, Landroid/provider/Settings$System;

    const-string v1, "PEN_HOVERING_SOUND"

    const-string v2, "pen_hovering_sound"

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->j:Ljava/lang/String;

    .line 83
    const-class v0, Landroid/media/AudioManager;

    const-string v1, "SOUND_TW_HIGHLIGHT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/mfluent/asp/ui/a/c;->k:I

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/ui/a/c$a;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/c;->b:Ljava/lang/ref/WeakReference;

    .line 88
    iput-object p2, p0, Lcom/mfluent/asp/ui/a/c;->c:Lcom/mfluent/asp/ui/a/c$a;

    .line 89
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/c;->e:Landroid/media/AudioManager;

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/a/c;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/mfluent/asp/ui/a/c;->m:J

    return-wide v0
.end method

.method private static b()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x1

    const/4 v1, -0x1

    :try_start_0
    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ui/a/c;->a:Lorg/slf4j/Logger;

    const-string v1, "Trouble setting SPen hovering icon"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->n:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->n:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 225
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/view/View;)V
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract b(Landroid/view/View;)V
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/ui/a/c;->m:J

    .line 97
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 98
    if-nez v0, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v3

    .line 102
    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_7

    move v1, v2

    .line 104
    :goto_1
    if-eqz v1, :cond_8

    .line 105
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/mfluent/asp/ui/a/c;->h:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 109
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/mfluent/asp/ui/a/c;->i:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v4

    if-eqz v4, :cond_0

    .line 120
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/mfluent/asp/ui/a/c;->c:Lcom/mfluent/asp/ui/a/c$a;

    invoke-interface {v4}, Lcom/mfluent/asp/ui/a/c$a;->a()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 121
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 135
    iget-boolean v3, p0, Lcom/mfluent/asp/ui/a/c;->d:Z

    if-nez v3, :cond_5

    .line 138
    if-eqz v1, :cond_a

    .line 139
    :try_start_1
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Lcom/mfluent/asp/ui/a/c;->j:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 144
    :goto_3
    if-ne v0, v2, :cond_3

    .line 145
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 147
    sget-object v0, Lcom/mfluent/asp/ui/a/c;->l:Ljava/lang/Boolean;
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_b

    .line 150
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->e:Landroid/media/AudioManager;

    sget v3, Lcom/mfluent/asp/ui/a/c;->k:I

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 151
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/a/c;->l:Ljava/lang/Boolean;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 165
    :cond_3
    :goto_4
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/a/c;->a(Landroid/view/View;)V

    .line 166
    iput-boolean v2, p0, Lcom/mfluent/asp/ui/a/c;->d:Z

    .line 168
    invoke-direct {p0}, Lcom/mfluent/asp/ui/a/c;->c()V

    .line 170
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->n:Landroid/os/Handler;

    if-nez v0, :cond_4

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/c;->n:Landroid/os/Handler;

    .line 173
    :cond_4
    new-instance v0, Lcom/mfluent/asp/ui/a/c$b;

    iget-object v2, p0, Lcom/mfluent/asp/ui/a/c;->n:Landroid/os/Handler;

    invoke-direct {v0, p0, p1, p2, v2}, Lcom/mfluent/asp/ui/a/c$b;-><init>(Lcom/mfluent/asp/ui/a/c;Landroid/view/View;Landroid/view/MotionEvent;Landroid/os/Handler;)V

    .line 174
    iget-object v2, p0, Lcom/mfluent/asp/ui/a/c;->n:Landroid/os/Handler;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 177
    :cond_5
    if-eqz v1, :cond_6

    .line 178
    const/16 v0, 0xa

    const/4 v1, -0x1

    :try_start_3
    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    .line 184
    :cond_6
    :goto_5
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/a/c;->a()Z

    move-result v3

    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 102
    goto/16 :goto_1

    .line 113
    :cond_8
    :try_start_4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/mfluent/asp/ui/a/c;->f:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v4

    if-nez v4, :cond_2

    goto/16 :goto_0

    .line 123
    :pswitch_0
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/a/c;->d:Z

    if-eqz v0, :cond_6

    .line 124
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/a/c;->b(Landroid/view/View;)V

    .line 125
    iput-boolean v3, p0, Lcom/mfluent/asp/ui/a/c;->d:Z

    .line 127
    if-eqz v1, :cond_9

    .line 128
    invoke-static {}, Lcom/mfluent/asp/ui/a/c;->b()V

    .line 131
    :cond_9
    invoke-direct {p0}, Lcom/mfluent/asp/ui/a/c;->c()V

    goto :goto_5

    .line 141
    :cond_a
    :try_start_5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Lcom/mfluent/asp/ui/a/c;->g:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sput-object v3, Lcom/mfluent/asp/ui/a/c;->l:Ljava/lang/Boolean;

    .line 154
    sget-object v3, Lcom/mfluent/asp/ui/a/c;->a:Lorg/slf4j/Logger;

    const-string v4, "Trouble playing sound effect {}"

    sget v5, Lcom/mfluent/asp/ui/a/c;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_4

    .line 156
    :cond_b
    sget-object v0, Lcom/mfluent/asp/ui/a/c;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 157
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->e:Landroid/media/AudioManager;

    sget v3, Lcom/mfluent/asp/ui/a/c;->k:I

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->playSoundEffect(I)V

    goto/16 :goto_4

    .line 159
    :cond_c
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c;->e:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->playSoundEffect(I)V
    :try_end_5
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_4

    .line 178
    :catch_2
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ui/a/c;->a:Lorg/slf4j/Logger;

    const-string v1, "Trouble setting SPen hovering icon"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_5

    .line 186
    :cond_d
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/a/c;->d:Z

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 190
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/a/c;->b(Landroid/view/View;)V

    .line 191
    iput-boolean v3, p0, Lcom/mfluent/asp/ui/a/c;->d:Z

    .line 192
    invoke-direct {p0}, Lcom/mfluent/asp/ui/a/c;->c()V

    .line 194
    if-eqz v1, :cond_e

    .line 195
    invoke-static {}, Lcom/mfluent/asp/ui/a/c;->b()V

    .line 198
    :cond_e
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/a/c;->a()Z

    move-result v3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    goto/16 :goto_2

    .line 121
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
