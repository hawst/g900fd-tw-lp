.class public abstract Lcom/mfluent/asp/ui/LifecycleTrackingActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z

.field private c:Landroid/content/res/Configuration;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 18
    iput v1, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 19
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->b:Z

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->c:Landroid/content/res/Configuration;

    .line 21
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->d:Z

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 115
    invoke-static {p1}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 94
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->c:Landroid/content/res/Configuration;

    .line 95
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 28
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->c:Landroid/content/res/Configuration;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->d:Z

    .line 30
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 36
    iget v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    if-lez v0, :cond_0

    .line 37
    iput v1, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 39
    :cond_0
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->b:Z

    .line 40
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 71
    iget v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 72
    const/4 v0, 0x2

    iput v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 74
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->d:Z

    .line 64
    const/4 v0, 0x3

    iput v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 65
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->d:Z

    .line 106
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 46
    const/4 v0, 0x2

    iput v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->d:Z

    .line 48
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 54
    iget v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->a:I

    .line 57
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onWindowFocusChanged(Z)V

    .line 79
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/LifecycleTrackingActivity;->b:Z

    .line 80
    return-void
.end method
