.class final Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V
    .locals 1

    .prologue
    .line 578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$a;->a:Ljava/lang/ref/WeakReference;

    .line 580
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    .line 585
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 586
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a()V

    .line 588
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    .line 589
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 590
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 591
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Ljava/lang/String;)V

    .line 598
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;Ljava/lang/String;)V

    .line 594
    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    const-string v1, "Unexpected status!! Show Progress Dialog Timeout!!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
