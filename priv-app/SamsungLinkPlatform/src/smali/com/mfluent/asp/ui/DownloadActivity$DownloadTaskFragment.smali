.class public Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadTaskFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;,
        Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;
    }
.end annotation


# instance fields
.field final a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;

.field private b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

.field private c:Ljava/lang/String;

.field private d:I

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/mfluent/asp/filetransfer/f;

.field private h:Lcom/mfluent/asp/filetransfer/d;

.field private i:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;-><init>()V

    .line 528
    sget-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 534
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e:Ljava/util/List;

    .line 536
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f:Ljava/util/List;

    .line 593
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;B)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;I)I
    .locals 0

    .prologue
    .line 516
    iput p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->d:I

    return p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->i:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Lcom/mfluent/asp/filetransfer/f;)Lcom/mfluent/asp/filetransfer/f;
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->g:Lcom/mfluent/asp/filetransfer/f;

    return-object p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;)Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    return-object p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->i:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/d;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->h:Lcom/mfluent/asp/filetransfer/d;

    return-object v0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->g:Lcom/mfluent/asp/filetransfer/f;

    return-object v0
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()Landroid/os/AsyncTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/os/Bundle;",
            "**>;"
        }
    .end annotation

    .prologue
    .line 597
    sget-object v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    .line 599
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 601
    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->h:Lcom/mfluent/asp/filetransfer/d;

    if-nez v1, :cond_0

    .line 604
    new-instance v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->h:Lcom/mfluent/asp/filetransfer/d;

    .line 607
    :cond_0
    new-instance v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Landroid/content/Context;)V

    return-object v1
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 549
    iget v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->d:I

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 843
    invoke-super {p0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->onDestroy()V

    .line 844
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 845
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->i:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 847
    :cond_0
    return-void
.end method
