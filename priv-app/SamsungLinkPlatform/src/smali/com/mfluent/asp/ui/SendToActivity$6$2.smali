.class final Lcom/mfluent/asp/ui/SendToActivity$6$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SendToActivity$6;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Lcom/mfluent/asp/ui/SendToActivity$6;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity$6;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1465
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->b:Lcom/mfluent/asp/ui/SendToActivity$6;

    iput-object p2, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Z
    .locals 8

    .prologue
    .line 1469
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->a:Ljava/util/Map;

    const-string v2, "result_file_count"

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->a:Ljava/util/Map;

    const-string v3, "result_file_count"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1471
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1474
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->a:Ljava/util/Map;

    const-string v4, "result_file_size"

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->a:Ljava/util/Map;

    const-string v5, "result_file_size"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1478
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->b:Lcom/mfluent/asp/ui/SendToActivity$6;

    iget-object v0, v0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    const v1, 0x7f0a01f4

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1479
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->b:Lcom/mfluent/asp/ui/SendToActivity$6;

    iget-object v1, v1, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/SendToActivity;->g(Lcom/mfluent/asp/ui/SendToActivity;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1480
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->b:Lcom/mfluent/asp/ui/SendToActivity$6;

    iget-object v1, v1, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;J)J

    .line 1483
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$2;->a:Ljava/util/Map;

    const-string v2, "result_file_info"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1484
    const/4 v0, 0x1

    return v0
.end method
