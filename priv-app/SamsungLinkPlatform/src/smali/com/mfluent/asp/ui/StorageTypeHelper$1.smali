.class final Lcom/mfluent/asp/ui/StorageTypeHelper$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageTypeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/widget/ImageView;

.field final synthetic c:Ljava/io/File;

.field final synthetic d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

.field final synthetic e:Ljava/util/Map;

.field final synthetic f:Lcom/mfluent/asp/ui/StorageTypeHelper;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageTypeHelper;Ljava/lang/String;Landroid/widget/ImageView;Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;)V
    .locals 1

    .prologue
    .line 232
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->f:Lcom/mfluent/asp/ui/StorageTypeHelper;

    iput-object p2, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->b:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->c:Ljava/io/File;

    iput-object p5, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->e:Ljava/util/Map;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 232
    invoke-static {}, Lcom/mfluent/asp/ui/StorageTypeHelper;->d()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const-string v0, "mfl_StorageTypeHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::setStorageImg:doInBackground webtype: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " imageView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->f:Lcom/mfluent/asp/ui/StorageTypeHelper;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->c:Ljava/io/File;

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 232
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/mfluent/asp/ui/StorageTypeHelper;->d()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const-string v0, "mfl_StorageTypeHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::setStorageImg:onPostExecute webtype: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " imageView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->e:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->e:Ljava/util/Map;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageTypeHelper$1;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method
