.class public final Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    iput-object p1, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->a:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;

    .line 246
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 247
    iput-object p3, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->b:Ljava/util/List;

    .line 248
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const v1, 0x7f0a0370

    .line 277
    packed-switch p1, :pswitch_data_0

    .line 287
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->a:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 281
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->a:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 283
    :pswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->a:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 285
    :pswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity$a;->a:Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/common/GuideAddOthersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
