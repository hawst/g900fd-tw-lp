.class final Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DebugLogLevelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    iput-object p1, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    .line 341
    iput-object p2, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    .line 342
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    const-string v1, "LOGLEVEL_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 350
    :goto_0
    return-object v0

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    const-string v1, "_LogLevel.LOGLEVEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x12

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 364
    const/4 v0, -0x1

    .line 366
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 371
    const/4 v0, 0x0

    .line 373
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
