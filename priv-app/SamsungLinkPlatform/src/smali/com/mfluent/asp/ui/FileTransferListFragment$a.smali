.class final Lcom/mfluent/asp/ui/FileTransferListFragment$a;
.super Landroid/database/AbstractCursor;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/filetransfer/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/FileTransferSession;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/database/Cursor;

.field private final c:Lcom/mfluent/asp/filetransfer/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 958
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "uuid"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "state"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "createdTime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "endTime"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sourceDeviceId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "targetDeviceId"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "transferSize"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "firstFileName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "numFiles"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "numFilesSkipped"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "currentTransferSize"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TRANSFERRED_BYTES"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "PROGRESS"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "TYPE"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "IS_FIRST_COMPLETE"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "IS_DOWNLOAD"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "IS_PAUSED"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroid/database/Cursor;Lcom/mfluent/asp/filetransfer/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/FileTransferSession;",
            ">;",
            "Landroid/database/Cursor;",
            "Lcom/mfluent/asp/filetransfer/d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 978
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 979
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    .line 980
    iput-object p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    .line 981
    iput-object p3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->c:Lcom/mfluent/asp/filetransfer/d;

    .line 982
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->c:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/filetransfer/c;)V

    .line 983
    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/filetransfer/b;)V
    .locals 1

    .prologue
    .line 1191
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->onChange(Z)V

    .line 1192
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1184
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1185
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->c:Lcom/mfluent/asp/filetransfer/d;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/filetransfer/d;->b(Lcom/mfluent/asp/filetransfer/c;)V

    .line 1186
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 1187
    return-void
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1000
    sget-object v0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->d:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 1016
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getFloat(I)F
    .locals 1

    .prologue
    .line 1021
    const/4 v0, 0x0

    return v0
.end method

.method public final getInt(I)I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1026
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->getPosition()I

    move-result v0

    .line 1028
    iget-object v4, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 1029
    iget-object v4, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession;

    .line 1031
    packed-switch p1, :pswitch_data_0

    .line 1056
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1033
    :pswitch_1
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1071
    :goto_0
    return v0

    .line 1035
    :pswitch_2
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->c()I

    move-result v0

    goto :goto_0

    .line 1037
    :pswitch_3
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    .line 1038
    goto :goto_0

    .line 1039
    :cond_0
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 1040
    goto :goto_0

    :cond_1
    move v0, v3

    .line 1042
    goto :goto_0

    .line 1045
    :pswitch_4
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v0

    goto :goto_0

    .line 1047
    :pswitch_5
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->g()I

    move-result v0

    goto :goto_0

    :pswitch_6
    move v0, v3

    .line 1049
    goto :goto_0

    .line 1051
    :pswitch_7
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_0

    .line 1054
    :pswitch_8
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_0

    .line 1061
    :cond_4
    packed-switch p1, :pswitch_data_1

    .line 1071
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    sget-object v1, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->d:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_9
    move v0, v1

    .line 1063
    goto :goto_0

    .line 1065
    :pswitch_a
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_5

    move v0, v2

    .line 1066
    goto :goto_0

    :cond_5
    move v0, v3

    .line 1068
    goto :goto_0

    .line 1031
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1061
    :pswitch_data_1
    .packed-switch 0xe
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final getLong(I)J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 1079
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->getPosition()I

    move-result v0

    .line 1081
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1082
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession;

    .line 1084
    packed-switch p1, :pswitch_data_0

    .line 1122
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1086
    :pswitch_1
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 1126
    :goto_0
    return-wide v0

    .line 1088
    :pswitch_2
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->h()Ljava/util/Set;

    move-result-object v0

    .line 1089
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move-wide v0, v2

    .line 1090
    goto :goto_0

    .line 1092
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 1093
    const-wide/16 v0, -0x62

    goto :goto_0

    .line 1095
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 1098
    :pswitch_3
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    if-nez v1, :cond_2

    move-wide v0, v2

    .line 1099
    goto :goto_0

    .line 1101
    :cond_2
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 1103
    :pswitch_4
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->j()J

    move-result-wide v0

    goto :goto_0

    .line 1105
    :pswitch_5
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->b()J

    move-result-wide v0

    goto :goto_0

    .line 1107
    :pswitch_6
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->k()J

    move-result-wide v4

    .line 1108
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v2

    .line 1112
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    cmp-long v0, v4, v2

    if-lez v0, :cond_4

    move-wide v0, v2

    .line 1113
    goto :goto_0

    .line 1118
    :pswitch_7
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->l()J

    move-result-wide v0

    goto :goto_0

    .line 1120
    :pswitch_8
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->t()J

    move-result-wide v0

    goto :goto_0

    .line 1126
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    sget-object v1, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->d:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :cond_4
    move-wide v0, v4

    goto :goto_0

    .line 1084
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public final getShort(I)S
    .locals 1

    .prologue
    .line 1133
    const/4 v0, 0x0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->getPosition()I

    move-result v0

    .line 1140
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1141
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession;

    .line 1143
    sparse-switch p1, :sswitch_data_0

    .line 1169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1146
    :sswitch_0
    :try_start_0
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->m()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1173
    :goto_0
    return-object v0

    .line 1150
    :catch_0
    move-exception v0

    .line 1151
    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->b()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "getString FIRST_FILE_NAME IndexOutOfBoundsException"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1152
    const-string v0, "Waiting.file"

    goto :goto_0

    .line 1157
    :sswitch_1
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->n()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1158
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1159
    :cond_0
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1160
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1161
    :cond_1
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1162
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1164
    :cond_2
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->IN_PROGRESS:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1167
    :sswitch_2
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1173
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    sget-object v1, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->d:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1143
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method public final isNull(I)Z
    .locals 1

    .prologue
    .line 1179
    const/4 v0, 0x0

    return v0
.end method

.method public final onMove(II)Z
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->b:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p2, v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1011
    invoke-super {p0, p1, p2}, Landroid/database/AbstractCursor;->onMove(II)Z

    move-result v0

    return v0
.end method
