.class public Lcom/mfluent/asp/ui/UpgradeActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;,
        Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;,
        Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/mfluent/asp/ui/UpgradeActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/UpgradeActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 251
    return-void
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/ui/UpgradeActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const/4 v0, 0x0

    .line 74
    sget-boolean v1, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v1, :cond_2

    .line 75
    const v1, 0x7f0b001d

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/UpgradeActivity;->setTheme(I)V

    .line 80
    :goto_0
    if-nez p1, :cond_0

    .line 81
    const/4 v0, 0x1

    .line 96
    invoke-static {p0}, Lcom/mfluent/asp/util/y;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/y;

    move-result-object v1

    .line 97
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 98
    const-string v3, "com.mfluent.asp.ui.UpgradeActivity.ACTION_DO_UPGRADE_FROM_NOTIFICATION_CLICK"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 99
    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->f()V

    .line 100
    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v1, p0}, Lcom/mfluent/asp/util/y;->b(Landroid/content/Context;)V

    .line 122
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity;->finish()V

    .line 125
    :cond_1
    return-void

    .line 77
    :cond_2
    const v1, 0x7f0b001c

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/UpgradeActivity;->setTheme(I)V

    goto :goto_0

    .line 111
    :cond_3
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->e()V

    .line 114
    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->g()V

    goto :goto_1
.end method
