.class final Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Runnable;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

.field private c:Landroid/database/ContentObserver;

.field private final d:Ljava/util/concurrent/CountDownLatch;

.field private final e:Ljava/util/concurrent/CountDownLatch;

.field private f:Lcom/mfluent/asp/filetransfer/c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 607
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    iput-object p2, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 611
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->d:Ljava/util/concurrent/CountDownLatch;

    .line 613
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->e:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 619
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->d(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    .line 621
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {}, Lcom/mfluent/asp/util/i;->a()Lcom/mfluent/asp/util/i;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/util/i;->a(Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Landroid/os/Handler;)Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    :cond_0
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$2;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->f:Lcom/mfluent/asp/filetransfer/c;

    .line 675
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->f:Lcom/mfluent/asp/filetransfer/c;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/filetransfer/c;)V

    .line 677
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->d(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$3;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->c:Landroid/database/ContentObserver;

    .line 696
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, ""

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 699
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 700
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "retrying download"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 701
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;

    .line 704
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    if-nez v0, :cond_2

    .line 705
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "starting new download"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 707
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DownloadActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity;->b(Lcom/mfluent/asp/ui/DownloadActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    .line 709
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "transferOptions"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .line 710
    iget-object v2, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    iget-object v3, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v3}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    iget-object v4, v4, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;

    invoke-interface {v3, v1, v4, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Lcom/mfluent/asp/filetransfer/f;)Lcom/mfluent/asp/filetransfer/f;

    .line 716
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;

    new-instance v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$4;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->a(Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 751
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 752
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Runnable;

    const/4 v1, 0x0

    new-instance v2, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$5;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$5;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 773
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 777
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 781
    :goto_0
    return-object v5

    .line 622
    :catch_0
    move-exception v0

    .line 623
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Interrupted while trying to create a handler"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 624
    new-array v0, v3, [Ljava/lang/Runnable;

    new-instance v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$1;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0

    .line 761
    :catch_1
    move-exception v0

    .line 762
    :try_start_3
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "transferFiles: Exception: "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 763
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Runnable;

    const/4 v1, 0x0

    new-instance v2, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$6;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$6;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->publishProgress([Ljava/lang/Object;)V

    .line 770
    invoke-direct {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 771
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->d:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 607
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 824
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "DownloadTaskFragment cleaning up"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 826
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->c:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 830
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->f:Lcom/mfluent/asp/filetransfer/c;

    if-eqz v0, :cond_1

    .line 831
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->f:Lcom/mfluent/asp/filetransfer/c;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->b(Lcom/mfluent/asp/filetransfer/c;)V

    .line 834
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    iget-object v0, v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->a(Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 836
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 837
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 607
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_0
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DownloadTaskFragment: null error scannedUris-null="

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " fileTransferTask-null="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v4

    if-nez v4, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v3, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v3}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/filetransfer/f;->af()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v3}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->f(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/filetransfer/f;->f()I

    move-result v3

    if-lt v0, v3, :cond_1

    invoke-direct {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b()V

    new-array v0, v1, [Ljava/lang/Runnable;

    new-instance v1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$7;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1$7;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;)V

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->publishProgress([Ljava/lang/Object;)V

    goto :goto_2
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 607
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 607
    invoke-direct {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onCancelled(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 607
    invoke-direct {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->e(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->g(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/mfluent/asp/filetransfer/d;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method protected final synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 607
    check-cast p1, [Ljava/lang/Runnable;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$1;->b:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->c()V

    return-void
.end method
