.class final Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V
    .locals 0

    .prologue
    .line 929
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 934
    invoke-static {}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 935
    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StorageSignInSettingDialogFragment RX broadcast: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->j(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 973
    :cond_1
    :goto_0
    return-void

    .line 942
    :cond_2
    invoke-static {}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 946
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 948
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 951
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 953
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 954
    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 955
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 956
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 957
    const v1, 0x7f0a0026

    new-instance v2, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2$1;-><init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 969
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b(Z)Z

    .line 971
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
