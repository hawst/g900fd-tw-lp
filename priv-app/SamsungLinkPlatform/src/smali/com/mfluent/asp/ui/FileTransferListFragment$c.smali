.class final Lcom/mfluent/asp/ui/FileTransferListFragment$c;
.super Landroid/widget/CursorAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/FileTransferListFragment;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment;)V
    .locals 3

    .prologue
    .line 198
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    .line 199
    invoke-virtual {p1}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 200
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 27

    .prologue
    .line 250
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v19

    .line 251
    const-class v4, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v4}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mfluent/asp/ASPApplication;

    .line 252
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->getItemViewType(I)I

    move-result v20

    .line 253
    const-string v5, "uuid"

    move-object/from16 v0, p3

    invoke-static {v0, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 254
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v5}, Lcom/mfluent/asp/ui/FileTransferListFragment;->b(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v9

    .line 256
    const v5, 0x7f090041

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .line 257
    const v5, 0x7f09001a

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v16, v5

    check-cast v16, Landroid/widget/TextView;

    .line 258
    const v5, 0x7f090045

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 259
    const v6, 0x7f090048

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object v10, v6

    check-cast v10, Landroid/widget/TextView;

    .line 260
    const v6, 0x7f09004a

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object v11, v6

    check-cast v11, Landroid/widget/TextView;

    .line 261
    const v6, 0x7f090046

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object/from16 v17, v6

    check-cast v17, Landroid/widget/TextView;

    .line 262
    const v6, 0x7f090036

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object v12, v6

    check-cast v12, Landroid/widget/ProgressBar;

    .line 263
    const v6, 0x7f09003c

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object/from16 v18, v6

    check-cast v18, Landroid/widget/ImageView;

    .line 264
    const v6, 0x7f090042

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object v13, v6

    check-cast v13, Landroid/widget/ImageView;

    .line 265
    const v6, 0x7f09003f

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    .line 268
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_0

    .line 269
    const v6, 0x7f020160

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 272
    :cond_0
    const-string v6, "sourceDeviceId"

    move-object/from16 v0, p3

    invoke-static {v0, v6}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v14

    .line 273
    long-to-int v6, v14

    int-to-long v6, v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    .line 276
    if-eqz v7, :cond_9

    .line 277
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->b()Ljava/lang/String;

    move-result-object v6

    move-object v14, v6

    .line 286
    :goto_0
    invoke-virtual {v10}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v6

    .line 288
    invoke-virtual {v6, v14}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v6

    float-to-int v15, v6

    .line 289
    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    const v6, 0x7f090047

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 292
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 293
    sget v15, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v23, 0x12

    move/from16 v0, v23

    if-lt v15, v0, :cond_b

    .line 294
    new-instance v15, Lcom/mfluent/asp/ui/FileTransferListFragment$c$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$1;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;)V

    invoke-virtual {v6, v15}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 327
    :goto_1
    const-string v6, "firstFileName"

    move-object/from16 v0, p3

    invoke-static {v0, v6}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 328
    if-nez v6, :cond_31

    .line 329
    const-string v6, "Waiting.file"

    move-object v15, v6

    .line 332
    :goto_2
    const-string v6, "numFiles"

    move-object/from16 v0, p3

    invoke-static {v0, v6}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v23

    .line 333
    move/from16 v0, v23

    invoke-static {v15, v0}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 334
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    const/4 v6, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 337
    const-string v6, "targetDeviceId"

    move-object/from16 v0, p3

    invoke-static {v0, v6}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v24

    move-wide/from16 v0, v24

    long-to-int v6, v0

    int-to-long v0, v6

    move-wide/from16 v24, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    .line 339
    if-eqz v6, :cond_c

    .line 340
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v19

    .line 345
    :goto_3
    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    const-string v10, "FILE"

    invoke-static {v15, v10}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 349
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 351
    const-string v11, "image"

    invoke-static {v10, v11}, Lorg/apache/commons/lang3/StringUtils;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 352
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-nez v10, :cond_1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-eqz v10, :cond_d

    :cond_1
    const v10, 0x7f02014e

    :goto_4
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 354
    const-wide/16 v14, 0x1

    .line 375
    :goto_5
    const-string v10, "slpf_pref_20"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 377
    const-string v10, "ATT"

    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "TMB"

    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "VZW"

    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "SPR"

    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "USC"

    invoke-static {}, Lcom/mfluent/asp/ui/FileTransferListFragment;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 383
    :cond_2
    const-string v10, "closed_tooltip"

    const/4 v13, 0x0

    invoke-interface {v11, v10, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 384
    if-eqz v20, :cond_3

    const/4 v13, 0x1

    move/from16 v0, v20

    if-ne v0, v13, :cond_4

    :cond_3
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v13

    if-eqz v13, :cond_5

    :cond_4
    const/4 v13, 0x2

    move/from16 v0, v20

    if-ne v0, v13, :cond_1a

    const-string v13, "IS_FIRST_COMPLETE"

    move-object/from16 v0, p3

    invoke-static {v0, v13}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v13

    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v13, v0, :cond_1a

    .line 386
    :cond_5
    if-nez v10, :cond_1a

    .line 387
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->e(Lcom/mfluent/asp/ui/FileTransferListFragment;)V

    .line 390
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->f(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    const/4 v13, 0x1

    if-ne v10, v13, :cond_7

    if-eqz v20, :cond_6

    const/4 v10, 0x1

    move/from16 v0, v20

    if-ne v0, v10, :cond_7

    :cond_6
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    if-eqz v10, :cond_1a

    .line 392
    :cond_7
    const v10, 0x7f09004b

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/view/View;->setVisibility(I)V

    .line 396
    const v10, 0x7f090031

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 397
    new-instance v13, Lcom/mfluent/asp/ui/FileTransferListFragment$c$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v13, v0, v1, v11}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$2;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Landroid/view/View;Landroid/content/SharedPreferences;)V

    invoke-virtual {v10, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 417
    :goto_6
    packed-switch v20, :pswitch_data_0

    .line 794
    :cond_8
    :goto_7
    new-instance v5, Lcom/mfluent/asp/ui/a/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v4}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    new-instance v7, Lcom/mfluent/asp/ui/a/b;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/widget/TextView;

    const/4 v8, 0x0

    aput-object v16, v4, v8

    invoke-direct {v7, v4}, Lcom/mfluent/asp/ui/a/b;-><init>([Landroid/widget/TextView;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v4

    if-eqz v4, :cond_2f

    const v4, 0x59d0dcff

    :goto_8
    invoke-direct {v5, v6, v7, v4}, Lcom/mfluent/asp/ui/a/a;-><init>(Landroid/content/Context;Lcom/mfluent/asp/ui/a/c$a;I)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 826
    return-void

    .line 279
    :cond_9
    const-wide/16 v24, -0x62

    cmp-long v6, v14, v24

    if-nez v6, :cond_a

    .line 280
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v6}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v14, 0x7f0a03dc

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v14, v6

    goto/16 :goto_0

    .line 282
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v6}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v14, 0x7f0a00c5

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v14, v6

    goto/16 :goto_0

    .line 323
    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v6}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v15, "window"

    invoke-virtual {v6, v15}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Display;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x3

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto/16 :goto_1

    .line 342
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v24, 0x7f0a00c5

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_3

    .line 352
    :cond_d
    const v10, 0x7f02014f

    goto/16 :goto_4

    .line 355
    :cond_e
    const-string v11, "video"

    invoke-static {v10, v11}, Lorg/apache/commons/lang3/StringUtils;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 356
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-nez v10, :cond_f

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-eqz v10, :cond_10

    :cond_f
    const v10, 0x7f020150

    :goto_9
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 358
    const-wide/16 v14, 0x3

    goto/16 :goto_5

    .line 356
    :cond_10
    const v10, 0x7f020151

    goto :goto_9

    .line 359
    :cond_11
    const-string v11, "audio"

    invoke-static {v10, v11}, Lorg/apache/commons/lang3/StringUtils;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 360
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-nez v10, :cond_12

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-eqz v10, :cond_13

    :cond_12
    const v10, 0x7f02014c

    :goto_a
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 362
    const-wide/16 v14, 0x2

    goto/16 :goto_5

    .line 360
    :cond_13
    const v10, 0x7f02014d

    goto :goto_a

    .line 363
    :cond_14
    invoke-static {v15}, Lcom/mfluent/asp/common/util/FileTypeHelper;->isAspDocument(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_17

    .line 364
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-nez v10, :cond_15

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-eqz v10, :cond_16

    :cond_15
    const v10, 0x7f020148

    :goto_b
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 366
    const-wide/16 v14, 0x4

    goto/16 :goto_5

    .line 364
    :cond_16
    const v10, 0x7f020149

    goto :goto_b

    .line 368
    :cond_17
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-nez v10, :cond_18

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v10}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v10

    if-eqz v10, :cond_19

    :cond_18
    const v10, 0x7f02014a

    :goto_c
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 370
    const-wide/16 v14, 0x5

    goto/16 :goto_5

    .line 368
    :cond_19
    const v10, 0x7f02014b

    goto :goto_c

    .line 411
    :cond_1a
    const v10, 0x7f09004b

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 414
    :cond_1b
    const v10, 0x7f09004b

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 421
    :pswitch_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    if-nez v9, :cond_1e

    .line 422
    const v9, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 429
    :goto_d
    if-eqz v12, :cond_1c

    .line 430
    const-string v9, "PROGRESS"

    move-object/from16 v0, p3

    invoke-static {v0, v9}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v12, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 433
    :cond_1c
    const-string v9, "state"

    move-object/from16 v0, p3

    invoke-static {v0, v9}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 434
    sget-object v10, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v10}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1f

    .line 435
    const v9, 0x7f0a00eb

    invoke-virtual {v4, v9}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450
    :goto_e
    const-string v4, "transferSize"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 453
    const-string v5, "TRANSFERRED_BYTES"

    move-object/from16 v0, p3

    invoke-static {v0, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    move-object/from16 v0, p2

    invoke-static {v0, v10, v11}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 454
    const-string v9, "%s/%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v5, 0x1

    aput-object v4, v10, v5

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 455
    if-eqz v17, :cond_1d

    .line 456
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v4

    if-eqz v4, :cond_20

    const v4, 0x7f02015e

    .line 460
    :goto_f
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 461
    new-instance v4, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v8, v7, v6}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    new-instance v4, Lcom/mfluent/asp/ui/FileTransferListFragment$c$4;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$4;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_7

    .line 425
    :cond_1e
    const v9, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_d

    .line 439
    :cond_1f
    const-string v9, "IS_PAUSED"

    move-object/from16 v0, p3

    invoke-static {v0, v9}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    .line 440
    const v9, 0x7f0a02b8

    invoke-virtual {v4, v9}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_e

    .line 459
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v4

    if-eqz v4, :cond_21

    const v4, 0x7f02015f

    goto :goto_f

    :cond_21
    const v4, 0x7f02015d

    goto :goto_f

    .line 512
    :pswitch_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    if-nez v10, :cond_22

    .line 513
    const v10, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 520
    :goto_10
    const-string v10, "endTime"

    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 521
    new-instance v11, Ljava/util/Date;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 525
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v10

    if-eqz v10, :cond_23

    .line 526
    const v10, 0x7f0a02b9

    invoke-virtual {v4, v10}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static/range {p2 .. p2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v4, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 530
    :goto_11
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v4

    if-eqz v4, :cond_24

    const v4, 0x7f020165

    .line 534
    :goto_12
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 536
    new-instance v4, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;

    move-object/from16 v5, p0

    move-object/from16 v10, p2

    invoke-direct/range {v4 .. v10}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$5;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;ZLandroid/content/Context;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584
    new-instance v4, Lcom/mfluent/asp/ui/FileTransferListFragment$c$6;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v9}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$6;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Z)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 609
    const-string v4, "transferSize"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 612
    const-string v5, "currentTransferSize"

    move-object/from16 v0, p3

    invoke-static {v0, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p2

    invoke-static {v0, v6, v7}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 615
    const-string v6, "%s/%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    const/4 v5, 0x1

    aput-object v4, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 616
    if-eqz v17, :cond_8

    .line 617
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 516
    :cond_22
    const v10, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_10

    .line 528
    :cond_23
    const v10, 0x7f0a02b9

    invoke-virtual {v4, v10}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static/range {p2 .. p2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v4, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_11

    .line 533
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v4}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v4

    if-eqz v4, :cond_25

    const v4, 0x7f020166

    goto/16 :goto_12

    :cond_25
    const v4, 0x7f020164

    goto/16 :goto_12

    .line 624
    :pswitch_2
    invoke-static/range {p3 .. p3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v12

    .line 626
    new-instance v10, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;

    move-object/from16 v11, p0

    move-object v13, v6

    invoke-direct/range {v10 .. v15}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$7;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;Lcom/mfluent/asp/datamodel/Device;J)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 655
    const-string v7, "IS_FIRST_COMPLETE"

    move-object/from16 v0, p3

    invoke-static {v0, v7}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v7

    const/4 v10, 0x1

    if-ne v7, v10, :cond_26

    .line 656
    const v7, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 663
    :goto_13
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v7}, Lcom/mfluent/asp/ui/FileTransferListFragment;->c(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v7

    if-eqz v7, :cond_27

    const v7, 0x7f020162

    .line 664
    :goto_14
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 665
    new-instance v7, Lcom/mfluent/asp/ui/FileTransferListFragment$c$8;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v9, v8}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$8;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;ZLjava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 692
    new-instance v7, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v9}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Z)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 726
    const-string v7, "endTime"

    move-object/from16 v0, p3

    invoke-static {v0, v7}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 727
    new-instance v8, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 728
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v7

    if-eqz v7, :cond_29

    .line 730
    invoke-static/range {p2 .. p2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 736
    :goto_15
    const/4 v9, 0x0

    .line 738
    const/4 v8, 0x0

    .line 739
    sget-object v10, Lcom/mfluent/asp/ui/FileTransferListFragment$6;->a:[I

    invoke-virtual {v12}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_1

    .line 754
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Complete tasks should not be in state "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 659
    :cond_26
    const v7, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_13

    .line 663
    :cond_27
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v7}, Lcom/mfluent/asp/ui/FileTransferListFragment;->d(Lcom/mfluent/asp/ui/FileTransferListFragment;)Z

    move-result v7

    if-eqz v7, :cond_28

    const v7, 0x7f020163

    goto/16 :goto_14

    :cond_28
    const v7, 0x7f020161

    goto/16 :goto_14

    .line 732
    :cond_29
    invoke-static/range {p2 .. p2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    goto :goto_15

    .line 741
    :pswitch_3
    const-string v8, "numFilesSkipped"

    move-object/from16 v0, p3

    invoke-static {v0, v8}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v8

    .line 742
    move/from16 v0, v23

    if-ne v8, v0, :cond_2d

    .line 743
    const v10, 0x7f0a0383

    move v11, v9

    move/from16 v26, v8

    move v8, v10

    move/from16 v10, v26

    .line 756
    :goto_16
    if-eqz v7, :cond_2a

    .line 757
    invoke-virtual {v4, v8}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v7, v8, v9

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 763
    :cond_2a
    const-string v4, "transferSize"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v8

    .line 764
    const-string v4, "currentTransferSize"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 765
    const-wide/16 v14, 0x0

    cmp-long v7, v4, v14

    if-nez v7, :cond_2b

    if-nez v11, :cond_2b

    move-wide v4, v8

    .line 769
    :cond_2b
    sget-object v7, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    if-ne v12, v7, :cond_2e

    cmp-long v7, v8, v4

    if-lez v7, :cond_2e

    move-wide v8, v4

    .line 778
    :cond_2c
    :goto_17
    move-object/from16 v0, p2

    invoke-static {v0, v8, v9}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 779
    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 782
    move/from16 v0, v23

    if-ne v10, v0, :cond_30

    move-object v4, v5

    .line 786
    :goto_18
    const-string v6, "%s/%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    const/4 v5, 0x1

    aput-object v4, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 787
    if-eqz v17, :cond_8

    .line 788
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 745
    :cond_2d
    const v10, 0x7f0a03a9

    move v11, v9

    move/from16 v26, v8

    move v8, v10

    move/from16 v10, v26

    .line 748
    goto :goto_16

    .line 750
    :pswitch_4
    const/4 v9, 0x1

    .line 751
    const v10, 0x7f0a02f8

    move v11, v9

    move/from16 v26, v8

    move v8, v10

    move/from16 v10, v26

    .line 752
    goto :goto_16

    .line 771
    :cond_2e
    sget-object v7, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    if-ne v12, v7, :cond_2c

    cmp-long v7, v8, v4

    if-gez v7, :cond_2c

    if-eqz v6, :cond_2c

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v6, v7}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v6

    if-eqz v6, :cond_2c

    move-wide v4, v8

    .line 775
    goto :goto_17

    .line 794
    :cond_2f
    const v4, 0x106000d

    goto/16 :goto_8

    :cond_30
    move-object v4, v6

    goto :goto_18

    :cond_31
    move-object v15, v6

    goto/16 :goto_2

    .line 417
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 739
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 240
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    const-string v1, "TYPE"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x3

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 215
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 216
    const/4 v0, 0x0

    .line 217
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->getItemViewType(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 229
    :goto_0
    return-object v0

    .line 219
    :pswitch_0
    const v0, 0x7f03000c

    invoke-virtual {v1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 222
    :pswitch_1
    const v0, 0x7f03000e

    invoke-virtual {v1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 225
    :pswitch_2
    const v0, 0x7f03000d

    invoke-virtual {v1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 204
    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/FileTransferListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 210
    :cond_0
    return-object v0
.end method
