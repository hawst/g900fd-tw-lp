.class public Lcom/mfluent/asp/ui/DeleteActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;,
        Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/mfluent/asp/ui/DeleteActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/DeleteActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity;->b:Z

    .line 223
    return-void
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/mfluent/asp/ui/DeleteActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    packed-switch p1, :pswitch_data_0

    .line 136
    :goto_0
    return-void

    .line 123
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 124
    new-instance v0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;-><init>()V

    .line 125
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 126
    const-string v2, "indeterminate"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 127
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "progressDialog"

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/DeleteActivity;->setResult(I)V

    .line 131
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity;->finish()V

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a00ac
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 62
    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    const v0, 0x7f0b0036

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DeleteActivity;->setTheme(I)V

    .line 72
    :goto_0
    return-void

    .line 66
    :cond_0
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 67
    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DeleteActivity;->setTheme(I)V

    goto :goto_0

    .line 69
    :cond_1
    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/DeleteActivity;->setTheme(I)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 85
    const-string v0, "showedConfirmationDialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity;->b:Z

    .line 86
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "showedConfirmationDialog"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/DeleteActivity;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 78
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 79
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const v1, 0x7f0a00ac

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 92
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/mfluent/asp/ui/DeleteActivity;->b:Z

    if-nez v0, :cond_0

    .line 93
    iput-boolean v5, p0, Lcom/mfluent/asp/ui/DeleteActivity;->b:Z

    .line 96
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.samsung.android.sdk.samsunglink.DEVICE_ID_EXTRA_KEY"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    .line 98
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "com.samsung.android.sdk.samsunglink.RECYCLE_BIN_EXTRA_KEY"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 99
    const-wide/16 v4, 0x0

    cmp-long v4, v4, v2

    if-gez v4, :cond_2

    .line 100
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 101
    if-eqz v2, :cond_1

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 102
    const v0, 0x7f0a00ad

    .line 109
    :goto_0
    new-instance v2, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    .line 110
    const v3, 0x7f0a0029

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 111
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 112
    const v0, 0x7f0a0027

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 113
    const v0, 0x7f0a0104

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 115
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p0, v1, v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    .line 117
    :cond_0
    return-void

    .line 103
    :cond_1
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v0, :cond_2

    .line 104
    const v0, 0x7f0a034b

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
