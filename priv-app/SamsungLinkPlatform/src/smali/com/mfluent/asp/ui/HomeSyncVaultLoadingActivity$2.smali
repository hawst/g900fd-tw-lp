.class final Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 105
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->b:I

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->c:I

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->d:I

    return-void
.end method

.method private varargs a()Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 111
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    const-string v1, "/api/pCloud/device/passwordInfo"

    invoke-static {v0, v1}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 115
    const-string v1, "mfl_HomeSyncVaultLoadingActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "passwordInfo response : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "uidPWPresent"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 117
    const-string v2, "uidMKPresent"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 118
    if-nez v1, :cond_1

    .line 120
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, -0x1

    .line 128
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 131
    :goto_1
    return-object v0

    .line 123
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    const-string v1, "mfl_HomeSyncVaultLoadingActivity"

    const-string v2, "Trouble checking if a password exists on the homeSync server"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 103
    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->b(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "mfl_HomeSyncVaultLoadingActivity"

    const-string v1, "Password does not exist on the server but MFEK exist - prompting user to recover"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "mfl_HomeSyncVaultLoadingActivity"

    const-string v1, "Password exists on the server - prompting user to enter their password"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "mfl_HomeSyncVaultLoadingActivity"

    const-string v1, "Password does not exist on the server - prompting user to create one"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity$2;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoadingActivity;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
