.class final Lcom/mfluent/asp/ui/SendToActivity$6$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SendToActivity$6;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Ljava/util/ArrayList;

.field final synthetic c:Lcom/mfluent/asp/ui/SendToActivity$6;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity$6;Ljava/util/Map;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1407
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->c:Lcom/mfluent/asp/ui/SendToActivity$6;

    iput-object p2, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->a:Ljava/util/Map;

    iput-object p3, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1412
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->a:Ljava/util/Map;

    const-string v3, "result_file_count"

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->a:Ljava/util/Map;

    const-string v4, "result_file_count"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .line 1414
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1415
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1416
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 1417
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1418
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1414
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1421
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->b:Ljava/util/ArrayList;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1422
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1424
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->a:Ljava/util/Map;

    const-string v4, "result_file_size"

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->a:Ljava/util/Map;

    const-string v5, "result_file_size"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1428
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->c:Lcom/mfluent/asp/ui/SendToActivity$6;

    iget-object v0, v0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    const v1, 0x7f0a01f4

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1450
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->c:Lcom/mfluent/asp/ui/SendToActivity$6;

    iget-object v1, v1, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/SendToActivity;->g(Lcom/mfluent/asp/ui/SendToActivity;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 1451
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->c:Lcom/mfluent/asp/ui/SendToActivity$6;

    iget-object v1, v1, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/ui/SendToActivity;->a(Lcom/mfluent/asp/ui/SendToActivity;J)J

    .line 1454
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6$1;->a:Ljava/util/Map;

    const-string v2, "result_file_info"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456
    return v8
.end method
