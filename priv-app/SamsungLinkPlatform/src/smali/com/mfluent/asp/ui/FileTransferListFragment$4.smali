.class final Lcom/mfluent/asp/ui/FileTransferListFragment$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment;->a(IILandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic d:Lcom/mfluent/asp/ui/FileTransferListFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 1259
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->d:Lcom/mfluent/asp/ui/FileTransferListFragment;

    iput-object p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->a:Lcom/mfluent/asp/datamodel/Device;

    iput-object p3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1267
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->a:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->a:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1270
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->a:Lcom/mfluent/asp/datamodel/Device;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "api/pCloud/transfer/upload/sessions/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/nts/b;->c(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1275
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->c:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->c:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1278
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->c:Lcom/mfluent/asp/datamodel/Device;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "api/pCloud/transfer/upload/sessions/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$4;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/nts/b;->c(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1283
    :cond_1
    :goto_1
    return-void

    .line 1280
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
