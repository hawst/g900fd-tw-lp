.class final Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 496
    invoke-static {}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 497
    const-string v0, "mfl_StorageSignInSettingDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StorageSignInSettingDialogFragment RX broadcast: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 502
    const-string v1, "com.mfluent.asp.sync.CLOUD_LAUNCH_OAUTH1_BROWSER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 506
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->a()V

    .line 508
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->b(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Z

    .line 510
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OAUTH1_URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 511
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 512
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;->b()V

    .line 514
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->dismissAllowingStateLoss()V

    .line 522
    :cond_2
    :goto_0
    return-void

    .line 516
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 517
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->c(Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;)Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/mfluent/asp/ui/OAuthWebView;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$d;->b(Landroid/content/Intent;)V

    .line 519
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment$6;->a:Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageSignInSettingDialogFragment;->dismissAllowingStateLoss()V

    goto :goto_0
.end method
