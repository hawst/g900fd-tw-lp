.class public Lcom/mfluent/asp/ui/RegisteredDevicesActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/AsyncTaskWatcher;
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;,
        Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;,
        Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;,
        Lcom/mfluent/asp/ui/RegisteredDevicesActivity$c;,
        Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/mfluent/asp/datamodel/Device;

.field private d:Z

.field private e:Landroid/widget/TextView;

.field private final f:Z

.field private final g:Landroid/content/BroadcastReceiver;

.field private final h:Landroid/os/Handler;

.field private i:Ljava/lang/Runnable;

.field private final j:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;

.field private final k:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;

.field private final l:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    .line 67
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d:Z

    .line 70
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->f:Z

    .line 72
    new-instance v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$1;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->g:Landroid/content/BroadcastReceiver;

    .line 552
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->h:Landroid/os/Handler;

    .line 612
    new-instance v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->j:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;

    .line 644
    new-instance v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->k:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;

    .line 661
    new-instance v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->l:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/datamodel/Device;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    return-object p1
.end method

.method static synthetic a()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private a(ILcom/mfluent/asp/datamodel/Device;)V
    .locals 13

    .prologue
    const v12, 0x7f090075

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 296
    const/4 v0, 0x0

    .line 302
    packed-switch p1, :pswitch_data_0

    move-object v4, v0

    .line 323
    :goto_0
    if-eqz v4, :cond_0

    .line 325
    if-nez p2, :cond_1

    .line 326
    sget-object v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::device == null {}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 327
    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 401
    :cond_0
    :goto_1
    return-void

    .line 304
    :pswitch_0
    const v0, 0x7f090054

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 305
    goto :goto_0

    .line 307
    :pswitch_1
    const v0, 0x7f090055

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 308
    goto :goto_0

    .line 310
    :pswitch_2
    const v0, 0x7f090056

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 311
    goto :goto_0

    .line 313
    :pswitch_3
    const v0, 0x7f090057

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 314
    goto :goto_0

    .line 316
    :pswitch_4
    const v0, 0x7f090058

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 317
    goto :goto_0

    .line 319
    :pswitch_5
    const v0, 0x7f090059

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    .line 331
    :cond_1
    const v0, 0x7f090012

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 332
    const v1, 0x7f090013

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 333
    const v2, 0x7f090011

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 334
    const v3, 0x7f090076

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 336
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v5

    .line 337
    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 338
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    :goto_2
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_4

    .line 344
    sget-object v5, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v6, "::update:SlinkDevicePhysicalType.SPC cannot be deregistered"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 345
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0259

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 357
    :goto_3
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 359
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-boolean v8, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d:Z

    sget-boolean v9, Lcom/mfluent/asp/ASPApplication;->k:Z

    invoke-static {v6, v7, v8, v9}, Lcom/mfluent/asp/ui/DeviceHelper;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;ZZZ)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 365
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(Landroid/widget/TextView;Z)V

    .line 366
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(Landroid/widget/TextView;Z)V

    .line 368
    new-instance v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$4;

    invoke-direct {v0, p0, p2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$4;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 382
    invoke-virtual {v4, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 383
    invoke-virtual {v4, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    .line 384
    invoke-static {p0, p2}, Lcom/mfluent/asp/ui/DeviceHelper;->b(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)I

    move-result v1

    .line 386
    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 391
    :cond_2
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 398
    :goto_4
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 340
    :cond_3
    sget-object v5, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v6, "::update:Device did nto have a display name: {}"

    invoke-interface {v5, v6, p2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 348
    :cond_4
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_5

    .line 349
    sget-object v5, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v6, "::update:SlinkDevicePhysicalType.BLURAY cannot be deregistered"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0285

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 352
    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_3

    .line 354
    :cond_5
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 355
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 393
    :cond_6
    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 395
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    .line 302
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/widget/TextView;Z)V
    .locals 1

    .prologue
    .line 429
    if-eqz p1, :cond_0

    .line 430
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 434
    :goto_0
    return-void

    .line 432
    :cond_0
    const v0, 0x3e4ccccd    # 0.2f

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b()V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 173
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 175
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 176
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d()V

    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const v5, 0x7f090074

    const v4, 0x7f090073

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 197
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    .line 198
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 199
    :cond_0
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 200
    invoke-virtual {p0, v5}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 206
    :goto_0
    return-void

    .line 202
    :cond_1
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 203
    invoke-virtual {p0, v5}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V
    .locals 4

    .prologue
    .line 54
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->e()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->i:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$c;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$c;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->i:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d()V

    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sendWakeupPushInBackground()V

    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->requestRefresh()V

    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v4, 0x0

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 210
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c()V

    .line 212
    const v1, 0x7f090054

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 213
    const v1, 0x7f090055

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 214
    const v1, 0x7f090056

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 215
    const v1, 0x7f090057

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 216
    const v1, 0x7f090058

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 217
    const v1, 0x7f090059

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 220
    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 222
    if-lez v1, :cond_0

    .line 223
    const v2, 0x7f090054

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 225
    :cond_0
    if-lt v1, v5, :cond_1

    .line 226
    const v2, 0x7f090055

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 228
    :cond_1
    const/4 v2, 0x3

    if-lt v1, v2, :cond_2

    .line 229
    const v2, 0x7f090056

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 231
    :cond_2
    const/4 v2, 0x4

    if-lt v1, v2, :cond_3

    .line 232
    const v2, 0x7f090057

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 234
    :cond_3
    const/4 v2, 0x5

    if-lt v1, v2, :cond_4

    .line 235
    const v2, 0x7f090058

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 237
    :cond_4
    const/4 v2, 0x6

    if-lt v1, v2, :cond_5

    .line 238
    const v1, 0x7f090059

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 242
    :cond_5
    invoke-direct {p0, v0, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    .line 243
    invoke-direct {p0, v5, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    .line 244
    const/4 v1, 0x3

    invoke-direct {p0, v1, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    .line 245
    const/4 v1, 0x4

    invoke-direct {p0, v1, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    .line 246
    const/4 v1, 0x5

    invoke-direct {p0, v1, v4}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    .line 257
    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    new-instance v2, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$3;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$3;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v1, v0

    .line 275
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v1, v0, :cond_6

    .line 276
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b:Ljava/util/List;

    add-int/lit8 v2, v1, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 277
    add-int/lit8 v2, v1, -0x1

    invoke-direct {p0, v2, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a(ILcom/mfluent/asp/datamodel/Device;)V

    .line 275
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 293
    :cond_6
    return-void
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V
    .locals 4

    .prologue
    const v3, 0x7f0a010c

    const v2, 0x7f0a00bd

    .line 54
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 421
    const v0, 0x7f09005a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 422
    return-void
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->f()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 425
    const v0, 0x7f09005a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 426
    return-void
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c()V

    return-void
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 438
    const v0, 0x7f0a00bd

    if-ne p1, v0, :cond_0

    .line 439
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    new-instance v1, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;

    invoke-direct {v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "deviceId"

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "deleteTaskFragment"

    invoke-virtual {v1, p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->a(Lcom/mfluent/asp/ui/AsyncTaskWatcher;Ljava/lang/String;)V

    .line 445
    :cond_0
    return-void
.end method

.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 3

    .prologue
    .line 500
    check-cast p1, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;

    .line 502
    iget-object v0, p1, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 504
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->e()V

    .line 522
    :goto_0
    return-void

    .line 507
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->f()V

    .line 509
    sget-object v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "Removing {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 511
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 513
    iget-object v0, p1, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$DeleteDeviceTaskFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    const v0, 0x7f0a0348

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->setResult(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 518
    :catch_0
    move-exception v0

    .line 519
    sget-object v1, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v2, "Failed to Removing DeleteDeviceTaskFragment "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 516
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0111

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;I[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->requestWindowFeature(I)Z

    .line 90
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 92
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 93
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    xor-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 94
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 99
    const-string v1, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d:Z

    .line 100
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d:Z

    if-eqz v0, :cond_2

    .line 101
    const v0, 0x7f0b002d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->setTheme(I)V

    .line 110
    :goto_0
    const v0, 0x7f0a02c5

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->setTitle(I)V

    .line 112
    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->setContentView(I)V

    .line 114
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 116
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 117
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 118
    invoke-static {p0}, Lcom/mfluent/asp/util/UiUtils;->b(Landroid/app/Activity;)V

    .line 120
    const-string v0, "VZW"

    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const v0, 0x7f090053

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->e:Landroid/widget/TextView;

    .line 122
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->e:Landroid/widget/TextView;

    const v1, 0x7f0a03df

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 125
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 126
    sget-object v1, Lcom/mfluent/asp/nts/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 127
    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    const-string v1, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 130
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 131
    return-void

    .line 103
    :cond_2
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_3

    .line 104
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->setTheme(I)V

    goto :goto_0

    .line 106
    :cond_3
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->setTheme(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 160
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 162
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 164
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 526
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 531
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 528
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->finish()V

    .line 529
    const/4 v0, 0x1

    goto :goto_0

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 135
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    .line 139
    const v0, 0x7f090074

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 141
    if-eqz v0, :cond_0

    .line 143
    new-instance v1, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$2;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity$2;-><init>(Lcom/mfluent/asp/ui/RegisteredDevicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->b()V

    .line 154
    invoke-direct {p0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->d()V

    .line 155
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 170
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 665
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 667
    sget-object v0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->a:Lorg/slf4j/Logger;

    const-string v1, "::onStart {}"

    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->isInitialized()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    .line 669
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->j:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 670
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->k:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 672
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->l:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 673
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 677
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 679
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->j:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$a;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 680
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->k:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$b;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 682
    iget-object v0, p0, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->l:Lcom/mfluent/asp/ui/RegisteredDevicesActivity$d;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/RegisteredDevicesActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 683
    return-void
.end method
