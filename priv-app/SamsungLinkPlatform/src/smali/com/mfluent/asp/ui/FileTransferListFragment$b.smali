.class final Lcom/mfluent/asp/ui/FileTransferListFragment$b;
.super Landroid/content/CursorLoader;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 855
    invoke-direct {p0, p1}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 856
    iput-boolean p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->a:Z

    .line 857
    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 861
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v6

    .line 864
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 866
    monitor-enter p0

    .line 867
    :try_start_0
    invoke-interface {v6}, Lcom/mfluent/asp/filetransfer/d;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 868
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    iget-boolean v3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->a:Z

    if-ne v2, v3, :cond_0

    .line 869
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 884
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 873
    :cond_1
    :try_start_1
    invoke-interface {v6}, Lcom/mfluent/asp/filetransfer/d;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession;

    .line 874
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->o()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_3
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v2

    iget-boolean v3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->a:Z

    if-ne v2, v3, :cond_2

    .line 875
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 879
    :cond_4
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->a:Z

    if-nez v0, :cond_5

    .line 880
    invoke-interface {v6}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 884
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 886
    new-instance v0, Lcom/mfluent/asp/ui/FileTransferListFragment$b$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$b$1;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$b;)V

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 908
    const-string v3, "transferarchive=?"

    .line 909
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-boolean v5, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->a:Z

    if-eqz v5, :cond_6

    const-string v5, "1"

    :goto_3
    aput-object v5, v4, v8

    const-string v5, "endTime DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 918
    new-instance v1, Lcom/mfluent/asp/ui/FileTransferListFragment$a;

    invoke-direct {v1, v7, v0, v6}, Lcom/mfluent/asp/ui/FileTransferListFragment$a;-><init>(Ljava/util/List;Landroid/database/Cursor;Lcom/mfluent/asp/filetransfer/d;)V

    .line 919
    new-instance v0, Lcom/mfluent/asp/ui/FileTransferListFragment$b$2;

    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v2}, Lcom/mfluent/asp/ui/FileTransferListFragment$b$2;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$b;Landroid/os/Handler;)V

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/FileTransferListFragment$a;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 927
    return-object v1

    .line 882
    :cond_5
    :try_start_2
    invoke-interface {v6}, Lcom/mfluent/asp/filetransfer/d;->h()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 909
    :cond_6
    const-string v5, "0"

    goto :goto_3
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/FileTransferListFragment$b;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
