.class final Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 811
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 816
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0, p2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 818
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z

    .line 819
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0, v3}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z

    .line 821
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 822
    const-string v0, "mfl_StorageServiceSignUpActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignIn authorization success result received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 826
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 829
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->n(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 831
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)V

    .line 834
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 835
    if-eqz v0, :cond_2

    .line 836
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 837
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setResult(ILandroid/content/Intent;)V

    .line 840
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->r(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sugarsync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 843
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->s(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 844
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    .line 845
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->u(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z

    .line 851
    :cond_3
    :goto_0
    return-void

    .line 848
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->finish()V

    goto :goto_0
.end method
