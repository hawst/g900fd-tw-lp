.class final Lcom/mfluent/asp/ui/SendToActivity$6;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SendToActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/SendToActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity;)V
    .locals 0

    .prologue
    .line 1369
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1369
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v0, "result_file_count"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "result_file_size"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isLocalFilePathsMediaSet()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getLocalFilePaths()[Ljava/lang/String;

    move-result-object v3

    const-string v0, "result_file_count"

    array-length v4, v3

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_1

    aget-object v0, v3, v1

    if-eqz v0, :cond_0

    new-instance v4, Ljava/io/File;

    aget-object v0, v3, v1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v5, "result_file_size"

    const-string v0, "result_file_size"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    const v1, 0x7f0a01f4

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "result_file_info"

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_1
    return-object v2

    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIdColumnName()Ljava/lang/String;

    move-result-object v0

    const-string v5, "_id"

    invoke-static {v0, v5}, Lorg/apache/commons/lang3/StringUtils;->defaultIfEmpty(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v1

    const-string v0, "_size"

    aput-object v0, v4, v8

    new-instance v0, Lcom/mfluent/asp/util/x;

    invoke-direct {v0}, Lcom/mfluent/asp/util/x;-><init>()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SendToActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v5, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v5}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v5

    new-instance v6, Lcom/mfluent/asp/ui/SendToActivity$6$1;

    invoke-direct {v6, p0, v2, v3}, Lcom/mfluent/asp/ui/SendToActivity$6$1;-><init>(Lcom/mfluent/asp/ui/SendToActivity$6;Ljava/util/Map;Ljava/util/ArrayList;)V

    invoke-static {v0, v5, v4, v6}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    const-string v0, "result_file_count"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->c(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->f(Lcom/mfluent/asp/ui/SendToActivity;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/util/x;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-array v4, v8, [Ljava/lang/String;

    const-string v0, "_size"

    aput-object v0, v4, v1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/SendToActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v3, Lcom/mfluent/asp/ui/SendToActivity$6$2;

    invoke-direct {v3, p0, v2}, Lcom/mfluent/asp/ui/SendToActivity$6$2;-><init>(Lcom/mfluent/asp/ui/SendToActivity$6;Ljava/util/Map;)V

    invoke-static {v1, v0, v4, v3}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;[Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    goto/16 :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1369
    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_0

    const-string v0, "result_file_count"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/SendToActivity;->h(Lcom/mfluent/asp/ui/SendToActivity;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "result_file_count"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "result_file_size"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v0, "result_file_info"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v1, v4, v5}, Lcom/mfluent/asp/ui/SendToActivity;->b(Lcom/mfluent/asp/ui/SendToActivity;J)J

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-static {v3, v4, v5}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$6;->a:Lcom/mfluent/asp/ui/SendToActivity;

    const v2, 0x7f09007c

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/SendToActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
