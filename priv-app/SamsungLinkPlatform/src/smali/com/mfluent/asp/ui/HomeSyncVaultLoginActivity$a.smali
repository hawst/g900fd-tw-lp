.class final Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;B)V
    .locals 0

    .prologue
    .line 656
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;-><init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V

    return-void
.end method

.method private varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 663
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->b:Ljava/lang/String;

    .line 664
    const/4 v0, 0x1

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->c:Ljava/lang/String;

    .line 667
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 672
    :goto_0
    return-object v0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    const-string v1, "mfl_HomeSyncVaultLoginActivity"

    const-string v2, "Trouble executing verify password request"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 672
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 656
    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 656
    check-cast p1, Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    const v1, 0x7f0a027e

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Password was rejected by the server"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    const v1, 0x7f0a0391

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;II[Ljava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    const v1, 0x7f0a038e

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v3}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->H()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;I[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    const-string v1, "Password verified"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->e(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Lcom/sec/android/safe/SecureStorageManagerClient;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/safe/SecureStorageManagerClient;->changePasswd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
