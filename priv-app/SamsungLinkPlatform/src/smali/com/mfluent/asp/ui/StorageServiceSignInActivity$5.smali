.class final Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 635
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 636
    const-string v0, "mfl_StorageServiceSignInActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StorageServiceSignInActivity RX broadcast: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 682
    :cond_1
    :goto_0
    return-void

    .line 643
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_4

    .line 644
    :cond_3
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_1

    .line 645
    const-string v0, "mfl_StorageServiceSignInActivity"

    const-string v1, "intent or intent.getExtras() is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 649
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 651
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 653
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 655
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 656
    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 657
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 658
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 659
    const v1, 0x7f0a0026

    new-instance v2, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5$1;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity$5;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 679
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Z)Z

    .line 680
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
