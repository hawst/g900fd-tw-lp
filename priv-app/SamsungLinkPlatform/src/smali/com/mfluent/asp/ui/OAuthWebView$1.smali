.class final Lcom/mfluent/asp/ui/OAuthWebView$1;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/OAuthWebView;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/OAuthWebView;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/OAuthWebView;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v0}, Lcom/mfluent/asp/ui/OAuthWebView;->a(Lcom/mfluent/asp/ui/OAuthWebView;)V

    .line 152
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/OAuthWebView;->a(Lcom/mfluent/asp/ui/OAuthWebView;Z)Z

    .line 154
    invoke-static {}, Lcom/mfluent/asp/ui/OAuthWebView;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 155
    const-string v0, "mfl_OAuthWebView"

    const-string v1, "::onPageFinished:oauth load finished."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 164
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 166
    invoke-static {}, Lcom/mfluent/asp/ui/OAuthWebView;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 167
    const-string v0, "mfl_OAuthWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onPageStarted:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v0}, Lcom/mfluent/asp/ui/OAuthWebView;->b(Lcom/mfluent/asp/ui/OAuthWebView;)V

    .line 172
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/OAuthWebView;->a(Lcom/mfluent/asp/ui/OAuthWebView;Z)Z

    .line 174
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 179
    invoke-static {}, Lcom/mfluent/asp/ui/OAuthWebView;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    .line 180
    const-string v0, "mfl_OAuthWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onReceivedError:oauth page load error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 207
    invoke-static {}, Lcom/mfluent/asp/ui/OAuthWebView;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    .line 208
    const-string v0, "mfl_OAuthWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::shouldOverrideUrlLoading:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p2, v0}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v1}, Lcom/mfluent/asp/ui/OAuthWebView;->c(Lcom/mfluent/asp/ui/OAuthWebView;)Landroid/content/IntentFilter;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-virtual {v2}, Lcom/mfluent/asp/ui/OAuthWebView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x1

    const-string v4, "mfl_OAuthWebView"

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/IntentFilter;->match(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I

    move-result v0

    .line 214
    if-ltz v0, :cond_5

    .line 216
    const-string v0, "not_approved=true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "error=access_denied"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/OAuthWebView;->finish()V

    .line 265
    :goto_0
    return v5

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 222
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.CLOUD_OAUTH1_RESPONSE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    const-string v2, "DEVICE_ID_EXTRA_KEY"

    iget-object v3, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v3}, Lcom/mfluent/asp/ui/OAuthWebView;->d(Lcom/mfluent/asp/ui/OAuthWebView;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 224
    const-string v2, "OAUTH1_RESPONSE_URI"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 227
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/OAuthWebView;->a()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_3

    .line 230
    iget-object v1, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 231
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    .line 232
    const-string v2, "loading"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "selectedStorageService.getJarName() : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/b/h;->b(Z)V

    .line 234
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    .line 238
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/OAuthWebView;->finish()V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    const-string v1, "mfl_OAuthWebView"

    const-string v2, "exception parsing url"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 264
    :cond_4
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_5
    :try_start_1
    const-string v0, "allshareplay.com/storage/oAuthPcLogin.do"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "&oauth_token="

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "&oauth_verifier="

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 247
    iget-object v0, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 248
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.CLOUD_OAUTH1_RESPONSE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 249
    const-string v2, "DEVICE_ID_EXTRA_KEY"

    iget-object v3, p0, Lcom/mfluent/asp/ui/OAuthWebView$1;->a:Lcom/mfluent/asp/ui/OAuthWebView;

    invoke-static {v3}, Lcom/mfluent/asp/ui/OAuthWebView;->d(Lcom/mfluent/asp/ui/OAuthWebView;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 250
    const-string v2, "OAUTH1_RESPONSE_URI"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 256
    :cond_6
    const-string v0, "market://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 257
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
