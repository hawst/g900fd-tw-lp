.class public Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BasicDialogFragment"
.end annotation


# instance fields
.field private final a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 593
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 595
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;-><init>(B)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 612
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    .line 617
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 619
    iget-object v0, p0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->a:Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getTargetRequestCode()I

    move-result v4

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$BasicDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder$b;->a(Landroid/content/DialogInterface;Landroid/os/Bundle;Ljava/lang/Object;ILandroid/app/Activity;)V

    .line 620
    return-void
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 599
    invoke-virtual {p1, p2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600
    invoke-static {}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Ignoring call to show dialog because a fragment with tag {} already exists"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 608
    :goto_0
    return-void

    .line 604
    :cond_0
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "IllegalStateException error in support library"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method
