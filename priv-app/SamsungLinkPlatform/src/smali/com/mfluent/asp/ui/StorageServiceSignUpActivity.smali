.class public Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageAppDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$17;,
        Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;
    }
.end annotation


# static fields
.field private static K:Z

.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Landroid/view/inputmethod/InputMethodManager;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private final G:Landroid/content/BroadcastReceiver;

.field private final H:Landroid/content/BroadcastReceiver;

.field private final I:Landroid/content/BroadcastReceiver;

.field private final J:Landroid/content/BroadcastReceiver;

.field private final L:Landroid/content/BroadcastReceiver;

.field private final M:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

.field private b:Lcom/mfluent/asp/b/h;

.field private c:Lcom/mfluent/asp/datamodel/Device;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

.field private h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

.field private i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

.field private j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/CheckBox;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 900
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->K:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 66
    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    .line 67
    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e:Ljava/lang/String;

    .line 116
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->x:Z

    .line 125
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->B:Z

    .line 127
    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->C:Z

    .line 709
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$6;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$6;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->G:Landroid/content/BroadcastReceiver;

    .line 723
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$7;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->H:Landroid/content/BroadcastReceiver;

    .line 811
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$8;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->I:Landroid/content/BroadcastReceiver;

    .line 860
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$9;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->J:Landroid/content/BroadcastReceiver;

    .line 901
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$10;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->L:Landroid/content/BroadcastReceiver;

    .line 1131
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$16;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$16;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->M:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    return-void
.end method

.method static synthetic a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;)Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    return-object p1
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 61
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    const-string v2, "DEVICE_ID_EXTRA_KEY"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->C:Z

    return p1
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 61
    sput-boolean p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->K:Z

    return p0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v1, 0x0

    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g()V

    sget-object v5, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v5, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c()V

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e()V

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e:Ljava/lang/String;

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->b:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-static {v3, v0}, Lcom/mfluent/asp/cloudstorage/a;->b(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->f()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v5, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v5, Lcom/mfluent/asp/ui/ValidateEmailAddressHelper;

    invoke-direct {v5}, Lcom/mfluent/asp/ui/ValidateEmailAddressHelper;-><init>()V

    const-string v6, "@"

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/mfluent/asp/ui/ValidateEmailAddressHelper;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->f:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v5, v3}, Lcom/mfluent/asp/ui/ValidateEmailAddressHelper;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->e:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->i:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v5, 0x6

    if-lt v3, v5, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v5, 0x2d

    if-le v3, v5, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->j:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->k:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->x:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->l:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v1

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    move v0, v2

    goto/16 :goto_0

    :cond_9
    invoke-interface {v0, p0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setStorageAppDelegate(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageAppDelegate;)V

    invoke-static {p0}, Lcom/mfluent/asp/b/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/e;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setStorageGatewayHelper(Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;)V

    iput-boolean v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->z:Z

    iput-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->y:Z

    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i()V

    new-instance v3, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$13;

    invoke-direct {v3, p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$13;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    aput-object v8, v2, v1

    invoke-virtual {v3, v0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$13;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->K:Z

    return v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->x:Z

    return p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 517
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 518
    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->y:Z

    return p1
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 521
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0403

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 522
    return-void
.end method

.method static synthetic d(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Z)V
    .locals 2

    .prologue
    .line 61
    invoke-static {p0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v1, p1}, Lcom/mfluent/asp/b/h;->b(Z)V

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 525
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 526
    return-void
.end method

.method static synthetic e(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 541
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->b:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_1

    .line 542
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->c:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_2

    .line 545
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 547
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->d:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_3

    .line 548
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 550
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->e:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_4

    .line 551
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 553
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->f:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_5

    .line 554
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 556
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_6

    .line 557
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 559
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->h:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_7

    .line 560
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 562
    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->i:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_8

    .line 563
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 565
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->j:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_9

    .line 566
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 568
    :cond_9
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->k:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_a

    .line 569
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 571
    :cond_a
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->l:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-ne v0, v1, :cond_0

    .line 572
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e()V

    return-void
.end method

.method static synthetic g(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 658
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 659
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 660
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 661
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 663
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 664
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 665
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 666
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 667
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 669
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 672
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 673
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 674
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 675
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 676
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    .line 677
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    .line 678
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    .line 679
    return-void
.end method

.method static synthetic h(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d()V

    return-void
.end method

.method static synthetic i(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 682
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 683
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->A:Z

    .line 684
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 685
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setFinishOnTouchOutside(Z)V

    .line 686
    const v0, 0x7f0900ba

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 688
    :cond_0
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 691
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 692
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->A:Z

    .line 693
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 694
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setFinishOnTouchOutside(Z)V

    .line 695
    const v0, 0x7f0900ba

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 697
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setCursorVisible(Z)V

    return-void
.end method

.method static synthetic k(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method private k()V
    .locals 5

    .prologue
    .line 1066
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->y:Z

    if-nez v0, :cond_1

    .line 1068
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h()V

    .line 1069
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->finish()V

    .line 1110
    :cond_0
    :goto_0
    return-void

    .line 1072
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v0, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    .line 1074
    if-eqz v0, :cond_2

    .line 1075
    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->reset()V

    .line 1078
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    .line 1080
    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$14;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$14;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;Lcom/mfluent/asp/datamodel/Device;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$14;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic l(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->z:Z

    return v0
.end method

.method static synthetic m(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 61
    new-instance v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageUserId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$11;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$11;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$11;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic n(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j()V

    return-void
.end method

.method static synthetic o(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->w:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic p(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->f()V

    return-void
.end method

.method static synthetic q(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->c:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method static synthetic r(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Lcom/mfluent/asp/b/h;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    return-object v0
.end method

.method static synthetic s(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->f:Z

    return v0
.end method

.method static synthetic t(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V
    .locals 4

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->B:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/StorageSignUpSuccessDialogFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "cloud_storage_type"

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v3}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic u(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->f:Z

    return v0
.end method

.method static synthetic v(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->D:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1151
    invoke-static {p1}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 501
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 502
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v3, 0x7f0a0046

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 136
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 137
    iput-boolean v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->B:Z

    .line 139
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 140
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    invoke-virtual {p0, v5}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setFinishOnTouchOutside(Z)V

    .line 142
    invoke-virtual {p0, v5}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->requestWindowFeature(I)Z

    .line 143
    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setContentView(I)V

    .line 153
    :goto_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->D:Landroid/view/inputmethod/InputMethodManager;

    .line 155
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j()V

    .line 158
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 159
    const-string v1, "target_sign_in_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-static {p0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v1

    .line 161
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    .line 163
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 164
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 165
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 166
    const v0, 0x7f090099

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v2}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 170
    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$1;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    const v0, 0x7f0900ba

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 180
    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$12;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$12;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    :goto_1
    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 212
    invoke-static {p0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v1

    .line 213
    sget-object v2, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    iget-object v3, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v3}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 215
    const v0, 0x7f0900a3

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->l:Landroid/widget/TextView;

    .line 216
    const v0, 0x7f0900a4

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->m:Landroid/widget/TextView;

    .line 217
    const v0, 0x7f0900a5

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->n:Landroid/widget/TextView;

    .line 218
    const v0, 0x7f0900a6

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->o:Landroid/widget/TextView;

    .line 219
    const v0, 0x7f0900a7

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->p:Landroid/widget/TextView;

    .line 220
    const v0, 0x7f0900a8

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->q:Landroid/widget/TextView;

    .line 221
    const v0, 0x7f0900a9

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->r:Landroid/widget/TextView;

    .line 222
    const v0, 0x7f0900ab

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->s:Landroid/widget/TextView;

    .line 223
    const v0, 0x7f0900ac

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->t:Landroid/widget/TextView;

    .line 224
    const v0, 0x7f0900ad

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->u:Landroid/widget/TextView;

    .line 225
    const v0, 0x7f0900b2

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->v:Landroid/widget/TextView;

    .line 227
    const v0, 0x7f0900b4

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->w:Landroid/widget/CheckBox;

    .line 231
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->E:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->F:Landroid/view/View;

    if-nez v0, :cond_1

    .line 232
    :cond_0
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->E:Landroid/view/View;

    .line 233
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->F:Landroid/view/View;

    .line 236
    :cond_1
    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    .line 237
    const v0, 0x7f0900af

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    .line 238
    const v0, 0x7f0900b1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    .line 241
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->M:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a(Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;)V

    .line 242
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->M:Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->a(Lcom/mfluent/asp/ui/CSoftkeyEditText$OnEditKeyClickListener;)V

    .line 244
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g()V

    .line 248
    if-eqz p1, :cond_8

    .line 250
    const-string v0, "checkbox_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->x:Z

    .line 251
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->x:Z

    if-eqz v0, :cond_5

    .line 252
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 257
    :goto_2
    const-string v0, "error_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 258
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    if-eqz v0, :cond_6

    .line 259
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->f()V

    .line 264
    :goto_3
    const-string v0, "signing_in"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->y:Z

    .line 265
    const-string v0, "signing_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->z:Z

    .line 267
    const-string v0, "spinner_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->A:Z

    .line 268
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->A:Z

    if-eqz v0, :cond_7

    .line 269
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i()V

    .line 295
    :goto_4
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$18;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$18;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 307
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$19;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$19;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 319
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$20;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$20;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 331
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->h:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$21;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$21;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 345
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->i:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$22;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$22;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 359
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j:Lcom/mfluent/asp/ui/CSoftkeyEditText;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$23;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$23;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 375
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->F:Landroid/view/View;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$24;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$24;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 384
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->E:Landroid/view/View;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$2;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$2;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->w:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$3;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$3;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 416
    const v0, 0x7f0900b6

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k:Landroid/widget/TextView;

    .line 417
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0189

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 418
    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$4;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$4;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-interface {v0, v1, v4, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 428
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 429
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 431
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k:Landroid/widget/TextView;

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$5;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$5;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    return-void

    .line 145
    :cond_2
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_3

    .line 146
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setTheme(I)V

    .line 150
    :goto_5
    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 148
    :cond_3
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setTheme(I)V

    goto :goto_5

    .line 190
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->b:Lcom/mfluent/asp/b/h;

    invoke-virtual {v2}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 193
    const/16 v0, 0x10

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 194
    const v0, 0x7f030026

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 197
    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0900bc

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->F:Landroid/view/View;

    .line 198
    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0900bb

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->E:Landroid/view/View;

    .line 201
    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0900be

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a018e

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 204
    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 205
    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    goto/16 :goto_1

    .line 254
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_2

    .line 261
    :cond_6
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    goto/16 :goto_3

    .line 271
    :cond_7
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->j()V

    goto/16 :goto_4

    .line 276
    :cond_8
    iput-boolean v5, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->C:Z

    .line 278
    sget-object v0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;->a:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    iput-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    .line 279
    iput-boolean v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->y:Z

    .line 280
    iput-boolean v4, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->z:Z

    goto/16 :goto_4
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 958
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 960
    invoke-direct {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->k()V

    .line 962
    const/4 v0, 0x1

    .line 964
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 477
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 480
    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->D:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->D:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    :cond_0
    :goto_0
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->G:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 488
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->H:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 489
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->I:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 490
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->J:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 491
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->L:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 492
    return-void

    .line 483
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 530
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 531
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->B:Z

    .line 532
    const-string v0, "error_state"

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->g:Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$ErrorState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 533
    const-string v0, "checkbox_state"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 534
    const-string v0, "signing_in"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 535
    const-string v0, "signing_up"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 536
    const-string v0, "spinner_state"

    iget-boolean v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 537
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 452
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 455
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->G:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_SIGNUP_SUCCESS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 458
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->H:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_SIGNUP_FAILED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 463
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->I:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_SUCCESS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 466
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->J:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_FAILURE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 470
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->L:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.sync.CLOUD_AUTHENTICATION_UNKNOWN"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 473
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 496
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 497
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4

    .prologue
    .line 1114
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 1116
    if-eqz p1, :cond_0

    .line 1117
    iget-boolean v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;->C:Z

    if-eqz v0, :cond_0

    .line 1118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$15;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity$15;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1129
    :cond_0
    return-void
.end method

.method public requestSync()V
    .locals 0

    .prologue
    .line 1008
    return-void
.end method

.method public sendBroadcastMessage(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 702
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 703
    if-eqz v0, :cond_0

    .line 704
    const-string v1, "mfl_StorageServiceSignUpActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CloudStroage SendBroadcast: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 707
    :cond_0
    return-void
.end method
