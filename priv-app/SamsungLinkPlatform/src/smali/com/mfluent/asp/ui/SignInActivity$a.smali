.class final Lcom/mfluent/asp/ui/SignInActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mfluent/asp/ui/SignInActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/SignInActivity;)V
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$a;->a:Ljava/lang/ref/WeakReference;

    .line 186
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/mfluent/asp/ui/SignInActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/SignInActivity;

    .line 191
    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SignInActivity;->a()V

    .line 195
    invoke-static {}, Lcom/mfluent/asp/ui/SignInActivity;->c()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::SignInTimeout.run: sign in progress dialog timed out."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SignInActivity;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    invoke-static {}, Lcom/mfluent/asp/ui/SignInActivity;->c()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::SignInTimeout.run: Sign-in process timed out, but we are signed in anyway."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/SignInActivity;->finish()V

    goto :goto_0

    .line 200
    :cond_1
    invoke-static {v0}, Lcom/mfluent/asp/ui/SignInActivity;->a(Lcom/mfluent/asp/ui/SignInActivity;)V

    goto :goto_0
.end method
