.class final Lcom/mfluent/asp/ui/SendToActivity$2;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/SendToActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/mfluent/asp/datamodel/Device;",
        "Ljava/lang/Void;",
        "Lcom/mfluent/asp/datamodel/Device;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/SendToActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/SendToActivity;)V
    .locals 0

    .prologue
    .line 901
    iput-object p1, p0, Lcom/mfluent/asp/ui/SendToActivity$2;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 901
    check-cast p1, [Lcom/mfluent/asp/datamodel/Device;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {}, Lcom/mfluent/asp/ui/SendToActivity;->a()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::signInToStorageService: adding: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/mfluent/asp/ui/SendToActivity;->a(I)I

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 901
    check-cast p1, Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, p0, Lcom/mfluent/asp/ui/SendToActivity$2;->a:Lcom/mfluent/asp/ui/SendToActivity;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/SendToActivity;->b(Lcom/mfluent/asp/ui/SendToActivity;I)V

    return-void
.end method
