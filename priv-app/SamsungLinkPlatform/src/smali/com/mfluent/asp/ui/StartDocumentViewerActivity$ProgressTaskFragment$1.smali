.class final Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;
.super Lcom/mfluent/asp/util/q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->a()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/util/q",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

.field final synthetic b:Landroid/content/ContentResolver;

.field final synthetic c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;Ljava/util/concurrent/TimeUnit;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    iput-object p3, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    iput-object p4, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->b:Landroid/content/ContentResolver;

    invoke-direct {p0, p2}, Lcom/mfluent/asp/util/q;-><init>(Ljava/util/concurrent/TimeUnit;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 280
    new-instance v0, Lcom/mfluent/asp/util/x;

    invoke-direct {v0}, Lcom/mfluent/asp/util/x;-><init>()V

    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->isFileListUri(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "local_data"

    aput-object v1, v0, v3

    const-string v1, "_display_name"

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    new-instance v3, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$1;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$1;-><init>(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;)V

    invoke-static {v1, v2, v0, v3}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-static {}, Lcom/mfluent/a/a/b;->g()Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "document_id"

    aput-object v2, v1, v3

    const-string v2, "_display_name"

    aput-object v2, v1, v4

    const-string v2, "home_sync_flags"

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string v3, "_size"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "local_data"

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->b:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    new-instance v4, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;

    invoke-direct {v4, p0, v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1$2;-><init>(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;Ljava/io/File;)V

    invoke-static {v2, v3, v1, v4}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->f(Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;)Z

    iget-object v0, p0, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment$1;->c:Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/StartDocumentViewerActivity$ProgressTaskFragment;->c()V

    return-void
.end method
