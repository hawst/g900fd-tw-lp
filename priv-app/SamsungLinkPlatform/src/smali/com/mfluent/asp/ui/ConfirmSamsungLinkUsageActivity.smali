.class public Lcom/mfluent/asp/ui/ConfirmSamsungLinkUsageActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/dialog/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 36
    const v1, 0x7f0a035c

    if-ne p1, v1, :cond_0

    .line 37
    if-ne p2, v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ConfirmSamsungLinkUsageActivity;->setResult(I)V

    .line 38
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/ConfirmSamsungLinkUsageActivity;->finish()V

    .line 41
    :cond_0
    return-void

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f0a035c

    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 24
    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ConfirmSamsungLinkUsageActivity;->setTheme(I)V

    .line 29
    :goto_0
    if-nez p1, :cond_0

    .line 30
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0026

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    .line 32
    :cond_0
    return-void

    .line 26
    :cond_1
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/ConfirmSamsungLinkUsageActivity;->setTheme(I)V

    goto :goto_0
.end method
