.class final Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaScannerConnection$OnScanCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

.field private b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

.field private final c:Ljava/util/concurrent/locks/Lock;

.field private final d:Ljava/util/concurrent/locks/Condition;


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)V
    .locals 1

    .prologue
    .line 555
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->a:Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    .line 561
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->d:Ljava/util/concurrent/locks/Condition;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;B)V
    .locals 0

    .prologue
    .line 555
    invoke-direct {p0, p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 585
    :try_start_0
    iput-object p1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    .line 586
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 588
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 589
    return-void

    .line 588
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 567
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 569
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 578
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 579
    :goto_1
    return-void

    .line 576
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    invoke-interface {v0, p1, p2}, Landroid/media/MediaScannerConnection$OnScanCompletedListener;->onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 578
    iget-object v0, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$a;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
