.class final Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment$c;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Z)V
    .locals 0

    .prologue
    .line 692
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iput-boolean p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 696
    iget-object v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;->b:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v0, v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/FileTransferListFragment;->h(Lcom/mfluent/asp/ui/FileTransferListFragment;)Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 698
    const-string v4, "transferarchive=? AND state IN (?,?)"

    .line 702
    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    aput-object v0, v5, v1

    const/4 v0, 0x1

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    .line 707
    new-instance v0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9$1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ui/FileTransferListFragment$c$9$1;-><init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c$9;Landroid/content/ContentResolver;)V

    .line 721
    const/16 v1, 0x2710

    const/4 v2, 0x0

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 723
    :cond_0
    return-void

    .line 702
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method
