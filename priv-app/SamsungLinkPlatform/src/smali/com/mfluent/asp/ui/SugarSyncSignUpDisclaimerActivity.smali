.class public Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 84
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f0a01d6

    const/4 v3, 0x1

    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 29
    const-string v1, "target_sign_in_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->setFinishOnTouchOutside(Z)V

    .line 33
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->requestWindowFeature(I)Z

    .line 42
    :goto_0
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->setContentView(I)V

    .line 44
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 45
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    const v0, 0x7f090099

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 57
    :goto_1
    const v0, 0x7f0900c6

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 59
    new-instance v2, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity$1;

    invoke-direct {v2, p0, v1}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity$1;-><init>(Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void

    .line 35
    :cond_0
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->k:Z

    if-eqz v0, :cond_1

    .line 36
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->setTheme(I)V

    goto :goto_0

    .line 38
    :cond_1
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->setTheme(I)V

    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->setTitle(I)V

    .line 51
    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 53
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 73
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 75
    const v1, 0x7f0c0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 76
    const/4 v0, 0x1

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 90
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 92
    :sswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->finish()V

    goto :goto_0

    .line 95
    :sswitch_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 96
    const-string v2, "target_sign_in_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    const-string v3, "target_sign_in_key"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->startActivity(Landroid/content/Intent;)V

    .line 100
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/SugarSyncSignUpDisclaimerActivity;->finish()V

    goto :goto_0

    .line 90
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0900d1 -> :sswitch_1
    .end sparse-switch
.end method
