.class public Lcom/mfluent/asp/ui/StorageSignInOutHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ui/StorageSignInOutHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_BROADCAST_SIGNOUT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->c:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x6

    .line 43
    sget-object v0, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_0

    .line 44
    const-string v0, "mfl_StorageSignInOutHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::signOutOfStorageService:Signing out of: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_0
    const-class v0, Lcom/mfluent/asp/sync/e;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/e;

    .line 50
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/e;->g(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :try_start_1
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_1

    .line 55
    invoke-interface {v1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->reset()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    :cond_1
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/e;

    move-result-object v1

    .line 66
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/b/e;->logout(Ljava/lang/String;)Ljava/lang/String;

    .line 68
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageUserId(Ljava/lang/String;)V

    .line 69
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/mfluent/asp/datamodel/Device;->k(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 79
    :cond_2
    :goto_1
    :try_start_3
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->P()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    :cond_3
    :goto_2
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/e;->f(Landroid/content/Intent;)V

    .line 90
    sget-object v0, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_4

    .line 91
    const-string v0, "mfl_StorageSignInOutHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::signOutOfStorageService:--- Signed out of: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_4
    return-void

    .line 57
    :catch_0
    move-exception v1

    .line 58
    :try_start_4
    sget-object v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_1

    .line 59
    const-string v2, "mfl_StorageSignInOutHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::signOutOfStorageService:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v1

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/sync/e;->f(Landroid/content/Intent;)V

    throw v1

    .line 71
    :catch_1
    move-exception v1

    .line 72
    :try_start_5
    sget-object v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_2

    .line 73
    const-string v2, "mfl_StorageSignInOutHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::run:Failed to logout of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 80
    :catch_2
    move-exception v1

    .line 81
    sget-object v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v5, :cond_3

    .line 82
    const-string v2, "mfl_StorageSignInOutHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::signOutOfStorageService:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2
.end method
