.class public Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/ui/dialog/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadProgressDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 501
    packed-switch p1, :pswitch_data_0

    .line 513
    :goto_0
    return-void

    .line 503
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 504
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->a()Lcom/mfluent/asp/ui/AsyncTaskFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/AsyncTaskFragment;->b()V

    goto :goto_0

    .line 506
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->dismiss()V

    .line 507
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 508
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 501
    :pswitch_data_0
    .packed-switch 0x7f0a015e
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 4

    .prologue
    const v2, 0x7f0a015e

    const/4 v3, 0x0

    .line 437
    check-cast p1, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    .line 439
    sget-object v0, Lcom/mfluent/asp/ui/DownloadActivity$1;->a:[I

    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 492
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 444
    :pswitch_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 445
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->dismiss()V

    .line 446
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    const v2, 0x7f0a00fe

    invoke-static {v0, v3, v2, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 450
    :pswitch_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 451
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->dismiss()V

    .line 452
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v3, v2, v1}, Lcom/mfluent/asp/ui/dialog/b;->a(Landroid/app/FragmentManager;II[Ljava/lang/Object;)V

    goto :goto_0

    .line 456
    :pswitch_3
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    const v1, 0x7f0a0178

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Z)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a002c

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;)V

    .line 465
    :pswitch_4
    invoke-virtual {p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->d()I

    move-result v1

    .line 467
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v2, 0x7f090036

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 468
    if-eqz v0, :cond_1

    .line 469
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 471
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v2, 0x7f090035

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 472
    if-eqz v0, :cond_0

    .line 473
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 478
    :pswitch_5
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 479
    if-eqz v0, :cond_0

    .line 480
    const v1, 0x7f0a015f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 485
    :pswitch_6
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->a(Landroid/app/FragmentManager;)V

    .line 486
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->dismiss()V

    .line 488
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DownloadActivity;

    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->b(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;->c(Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/ui/DownloadActivity;->a(Lcom/mfluent/asp/ui/DownloadActivity;Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    .line 439
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected final synthetic b()Lcom/mfluent/asp/ui/AsyncTaskFragment;
    .locals 1

    .prologue
    .line 337
    new-instance v0, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadTaskFragment;-><init>()V

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 428
    invoke-static {}, Lcom/mfluent/asp/ui/DownloadActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "onCancel"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 429
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 431
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 432
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 433
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f090034

    const v8, 0x7f090019

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 341
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 342
    new-array v1, v7, [I

    const/high16 v2, 0x7f010000

    aput v2, v1, v6

    .line 343
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v2, v0, v1}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 344
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 345
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 347
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 348
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "downloadingText"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 350
    if-eqz v1, :cond_3

    .line 351
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v4, 0x7f030009

    invoke-virtual {v0, v4, v10, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f09001a

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f090031

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment$2;

    invoke-direct {v2, p0, v1}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment$2;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;Landroid/app/Dialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 385
    :cond_1
    :goto_1
    return-object v0

    .line 351
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 354
    :cond_3
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 356
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v5, "layout_inflater"

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 357
    const v5, 0x7f030008

    invoke-virtual {v0, v5, v10, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 358
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 359
    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 361
    const v5, 0x7f0a0027

    new-instance v6, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment$1;

    invoke-direct {v6, p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment$1;-><init>(Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 370
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 371
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 374
    :cond_4
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 375
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 376
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    :cond_5
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 380
    if-eqz v1, :cond_1

    .line 381
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/DownloadActivity$DownloadProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/util/UiUtils;->a(Landroid/app/Dialog;Landroid/content/res/Resources;)V

    goto :goto_1
.end method
