.class final Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/StorageServiceSignInActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 585
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->l(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 586
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0, p2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 588
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->m(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/datamodel/Device;

    .line 589
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v3, 0x4

    if-gt v0, v3, :cond_0

    .line 590
    const-string v0, "mfl_StorageServiceSignInActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::mSignInAuthFailure::onReceive:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/mfluent/asp/common/util/IntentHelper;->intentToString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 598
    if-eqz v0, :cond_3

    .line 599
    const-string v3, "com.mfluent.asp.sync.SIGNIN_ERROR_KEY"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$SignInFailure;

    .line 602
    :goto_0
    invoke-static {}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->b()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_1

    .line 603
    const-string v3, "mfl_StorageServiceSignInActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::LoginFailed because:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->k(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    .line 608
    invoke-virtual {v2, v6}, Lcom/mfluent/asp/datamodel/Device;->k(Z)V

    .line 609
    new-instance v0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4$1;

    invoke-direct {v0, p0, v2}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4$1;-><init>(Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 618
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->g(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Lcom/mfluent/asp/ui/CSoftkeyEditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/CSoftkeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 619
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->e(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)V

    .line 620
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    sget-object v1, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;->b:Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->a(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;)Lcom/mfluent/asp/ui/StorageServiceSignInActivity$ReturnedError;

    .line 621
    iget-object v0, p0, Lcom/mfluent/asp/ui/StorageServiceSignInActivity$4;->a:Lcom/mfluent/asp/ui/StorageServiceSignInActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/StorageServiceSignInActivity;->f(Lcom/mfluent/asp/ui/StorageServiceSignInActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 624
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
