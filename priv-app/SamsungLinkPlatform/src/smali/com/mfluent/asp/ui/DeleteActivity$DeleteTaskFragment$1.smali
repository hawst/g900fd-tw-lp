.class final Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    iput-object p2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    iput-object p3, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->b:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 254
    new-instance v0, Lcom/mfluent/asp/util/x;

    invoke-direct {v0}, Lcom/mfluent/asp/util/x;-><init>()V

    .line 256
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isSlinkUri()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->isFileListUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    invoke-static {}, Lcom/mfluent/asp/ui/DeleteActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Starting file browser delete task."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDeviceIdFromUri(Landroid/net/Uri;)J

    move-result-wide v0

    .line 262
    iget-object v2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->b:Landroid/content/Context;

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mfluent/asp/datamodel/filebrowser/e;->a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    move-result-object v2

    .line 266
    new-array v1, v8, [Ljava/lang/String;

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 269
    iget-object v3, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "document_id"

    aput-object v6, v5, v7

    const-string v6, "_display_name"

    aput-object v6, v5, v8

    new-instance v6, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1$1;

    invoke-direct {v6, p0, v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1$1;-><init>(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;Ljava/util/ArrayList;[Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    .line 283
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 285
    iget-object v3, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    aget-object v1, v1, v7

    array-length v4, v0

    invoke-virtual {v3, v1, v4}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Ljava/lang/String;I)V

    .line 286
    iget-object v1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    array-length v3, v0

    invoke-virtual {v1, v7, v3}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(II)V

    .line 287
    iget-object v1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSortOrder()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->getSortTypeFromCursorSortOrder(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v1

    .line 288
    if-nez v1, :cond_1

    .line 289
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->getDefaultSortType()Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v1

    .line 292
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDirectoryIdFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->deleteFiles(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;[Ljava/lang/String;)I

    move-result v0

    .line 296
    iget-object v1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 360
    :goto_0
    return-object v9

    .line 298
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(I)V

    goto :goto_0

    .line 300
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(I)V

    goto :goto_0

    .line 304
    :cond_2
    invoke-static {}, Lcom/mfluent/asp/ui/DeleteActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Starting media delete task"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 306
    new-array v0, v8, [Ljava/lang/Integer;

    .line 307
    iget-object v1, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "media_type"

    aput-object v4, v3, v7

    new-instance v4, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1$2;

    invoke-direct {v4, p0, v0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1$2;-><init>(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;[Ljava/lang/Integer;)V

    invoke-static {v1, v2, v3, v4}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    .line 320
    aget-object v1, v0, v7

    if-nez v1, :cond_3

    .line 321
    invoke-static {}, Lcom/mfluent/asp/ui/DeleteActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "empty mediaSet"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-virtual {v0, v9, v8}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Ljava/lang/String;I)V

    .line 324
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(I)V

    goto :goto_0

    .line 328
    :cond_3
    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 329
    sparse-switch v0, :sswitch_data_0

    .line 343
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No delete task for media type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 331
    :sswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    new-instance v1, Lcom/mfluent/asp/media/i;

    invoke-direct {v1}, Lcom/mfluent/asp/media/i;-><init>()V

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/mfluent/asp/media/j;)Lcom/mfluent/asp/media/j;

    .line 346
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->g(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Lcom/mfluent/asp/media/j;

    move-result-object v0

    const-string v1, "_id"

    iget-object v2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    iget-object v3, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/mfluent/asp/media/j;->a(Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/media/l;)V

    .line 348
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->g(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;)Lcom/mfluent/asp/media/j;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1$3;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1$3;-><init>(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;)V

    new-array v2, v8, [Landroid/content/Context;

    iget-object v3, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->b:Landroid/content/Context;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/media/j;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 334
    :sswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    new-instance v1, Lcom/mfluent/asp/media/k;

    invoke-direct {v1}, Lcom/mfluent/asp/media/k;-><init>()V

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/mfluent/asp/media/j;)Lcom/mfluent/asp/media/j;

    goto :goto_1

    .line 337
    :sswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    new-instance v1, Lcom/mfluent/asp/media/g;

    invoke-direct {v1}, Lcom/mfluent/asp/media/g;-><init>()V

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/mfluent/asp/media/j;)Lcom/mfluent/asp/media/j;

    goto :goto_1

    .line 340
    :sswitch_3
    iget-object v0, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->c:Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;

    new-instance v1, Lcom/mfluent/asp/media/h;

    invoke-direct {v1}, Lcom/mfluent/asp/media/h;-><init>()V

    invoke-static {v0, v1}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;->a(Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment;Lcom/mfluent/asp/media/j;)Lcom/mfluent/asp/media/j;

    goto :goto_1

    .line 357
    :cond_4
    invoke-static {}, Lcom/mfluent/asp/ui/DeleteActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Unable perform delete. Unsupported mediaSet: {}"

    iget-object v2, p0, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a:Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 329
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/mfluent/asp/ui/DeleteActivity$DeleteTaskFragment$1;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
