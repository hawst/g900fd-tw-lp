.class final Lcom/mfluent/asp/ui/DeviceSorter$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DeviceSorter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/mfluent/asp/datamodel/Device;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 38
    check-cast p1, Lcom/mfluent/asp/datamodel/Device;

    check-cast p2, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v4

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {p1, v5}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v1

    :cond_3
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {p2, v5}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v4, v1

    :cond_4
    if-eqz v0, :cond_5

    if-nez v4, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    if-nez v0, :cond_6

    if-eqz v4, :cond_6

    move v1, v3

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->ordinal()I

    move-result v4

    sub-int/2addr v0, v4

    if-eqz v0, :cond_7

    move v1, v0

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v0, :cond_8

    if-eqz v4, :cond_0

    move v1, v3

    goto :goto_0

    :cond_8
    if-nez v4, :cond_9

    move v1, v2

    goto :goto_0

    :cond_9
    invoke-virtual {v0, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method
