.class public Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;
.super Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/UpgradeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateProgressDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/ui/AsyncTaskFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 212
    check-cast p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    .line 213
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    .line 214
    if-nez v0, :cond_0

    .line 215
    invoke-static {}, Lcom/mfluent/asp/ui/UpgradeActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::onAsyncTaskStateChanged::progressDialog is null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 248
    :goto_0
    return-void

    .line 219
    :cond_0
    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iget-object v2, p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    if-ne v1, v2, :cond_1

    .line 220
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_0

    .line 221
    :cond_1
    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->b:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iget-object v2, p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    if-ne v1, v2, :cond_3

    .line 222
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 223
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 225
    :cond_2
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 226
    invoke-static {p1}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 227
    invoke-static {p1}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->b(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0

    .line 228
    :cond_3
    sget-object v1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->c:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iget-object v2, p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    if-ne v1, v2, :cond_4

    .line 229
    invoke-static {p1}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 230
    invoke-static {p1}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 231
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_0

    .line 232
    :cond_4
    sget-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->d:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iget-object v1, p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    if-ne v0, v1, :cond_5

    .line 233
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->dismiss()V

    goto :goto_0

    .line 234
    :cond_5
    sget-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->e:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iget-object v1, p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    if-ne v0, v1, :cond_6

    .line 235
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->dismiss()V

    .line 236
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    .line 237
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 238
    :cond_6
    sget-object v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;->f:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    iget-object v1, p1, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->a:Lcom/mfluent/asp/ui/UpgradeActivity$UpdateStatus;

    if-ne v0, v1, :cond_7

    .line 239
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->dismiss()V

    .line 240
    invoke-static {}, Lcom/mfluent/asp/ui/dialog/b;->a()Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Update Error Occurred [ErrorCode]:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->c(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Ljava/lang/CharSequence;)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->e(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0a00ae

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 246
    :cond_7
    invoke-static {}, Lcom/mfluent/asp/ui/UpgradeActivity;->a()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::onAsyncTaskStateChanged::UNKNOWN Update Status"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final b()Lcom/mfluent/asp/ui/AsyncTaskFragment;
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;-><init>()V

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->a()Lcom/mfluent/asp/ui/AsyncTaskFragment;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;

    .line 203
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateTaskFragment;->d()V

    .line 204
    invoke-super {p0, p1}, Lcom/mfluent/asp/ui/AsyncTaskWatcherDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 206
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 207
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 208
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 174
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->setCancelable(Z)V

    .line 175
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 177
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 178
    const v2, 0x7f0a03c9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 179
    const v2, 0x7f0a03cb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 180
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 181
    const/4 v2, -0x2

    const v3, 0x7f0a0027

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment$1;-><init>(Lcom/mfluent/asp/ui/UpgradeActivity$UpdateProgressDialogFragment;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 188
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 189
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 190
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 192
    return-object v1
.end method
