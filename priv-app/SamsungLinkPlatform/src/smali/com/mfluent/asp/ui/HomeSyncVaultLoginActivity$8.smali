.class final Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->a([Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:[Landroid/widget/EditText;

.field final synthetic b:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;[Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1044
    iput-object p1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;->b:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    iput-object p2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;->a:[Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1056
    .line 1058
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;->a:[Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 1059
    iget-object v3, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;->a:[Landroid/widget/EditText;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 1060
    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->trimToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    .line 1067
    :goto_1
    if-nez v0, :cond_1

    .line 1068
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;->b:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1072
    :goto_2
    return-void

    .line 1059
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1070
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$8;->b:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->i(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1052
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1048
    return-void
.end method
