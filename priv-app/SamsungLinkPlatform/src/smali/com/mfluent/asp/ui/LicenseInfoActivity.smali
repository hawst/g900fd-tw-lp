.class public Lcom/mfluent/asp/ui/LicenseInfoActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;
    }
.end annotation


# instance fields
.field private a:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 100
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/LicenseInfoActivity;)V
    .locals 2

    .prologue
    .line 20
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/ui/LicenseInfoActivity;)V
    .locals 2

    .prologue
    .line 20
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 69
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f030011

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->setContentView(I)V

    .line 35
    const v0, 0x7f0a00b9

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->setTitle(I)V

    .line 36
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 37
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 39
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 41
    const v0, 0x7f09005c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity;->a:Landroid/webkit/WebView;

    .line 43
    new-instance v0, Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ui/LicenseInfoActivity$LicenseWebViewClient;-><init>(Lcom/mfluent/asp/ui/LicenseInfoActivity;)V

    .line 44
    iget-object v1, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 46
    iget-object v0, p0, Lcom/mfluent/asp/ui/LicenseInfoActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 48
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 49
    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 51
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    const-class v1, Lcom/mfluent/asp/ui/StorageServiceSignUpActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "https://sugarsync.com/terms.html"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 58
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 59
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 60
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->finish()V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v0, "mfl_LicenseInfoActivity"

    const-string v1, "Calling activity not found"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 82
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 88
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 84
    :pswitch_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/LicenseInfoActivity;->finish()V

    .line 85
    const/4 v0, 0x1

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
