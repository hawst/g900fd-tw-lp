.class final Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/FileTransferListFragment$c;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic c:Lcom/mfluent/asp/datamodel/Device;

.field final synthetic d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/FileTransferListFragment$c;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 461
    iput-object p1, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iput-object p2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->b:Lcom/mfluent/asp/datamodel/Device;

    iput-object p4, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x7f0a034e

    .line 465
    new-instance v0, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    invoke-direct {v0}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;-><init>()V

    .line 466
    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->b(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 469
    invoke-virtual {v0, v4}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 470
    const v1, 0x7f0a002a

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->c(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 471
    const v1, 0x7f0a002b

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->d(I)Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;

    .line 473
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 474
    const-string v2, "sessionId"

    iget-object v3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->b:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v2, :cond_0

    .line 476
    const-string v2, "sourceDeviceId"

    iget-object v3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 478
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->c:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v2, :cond_1

    .line 479
    const-string v2, "targetDeviceId"

    iget-object v3, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 481
    :cond_1
    iget-object v2, p0, Lcom/mfluent/asp/ui/FileTransferListFragment$c$3;->d:Lcom/mfluent/asp/ui/FileTransferListFragment$c;

    iget-object v2, v2, Lcom/mfluent/asp/ui/FileTransferListFragment$c;->a:Lcom/mfluent/asp/ui/FileTransferListFragment;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3, v1}, Lcom/mfluent/asp/ui/dialog/BasicDialogBuilder;->a(Lcom/mfluent/asp/ui/dialog/a;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 486
    return-void
.end method
