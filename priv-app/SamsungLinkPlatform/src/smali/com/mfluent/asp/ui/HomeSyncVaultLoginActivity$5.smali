.class final Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->c(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)V
    .locals 0

    .prologue
    .line 726
    iput-object p1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x81

    const/4 v3, 0x1

    .line 730
    const-string v0, "mfl_HomeSyncVaultLoginActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCheckedChanged : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    if-eqz p2, :cond_0

    .line 732
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 733
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 734
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 735
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 736
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 737
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 738
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 739
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 750
    :goto_0
    return-void

    .line 741
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 742
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 743
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 744
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->f(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 745
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 746
    iget-object v1, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v1}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 747
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 748
    iget-object v2, p0, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity$5;->a:Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;

    invoke-static {v2}, Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;->g(Lcom/mfluent/asp/ui/HomeSyncVaultLoginActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0
.end method
