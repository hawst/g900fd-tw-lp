.class public Lcom/mfluent/asp/ui/HomeSyncDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;
    }
.end annotation


# static fields
.field private static final a:[Lcom/mfluent/asp/ui/HomeSyncDestination;


# instance fields
.field private b:Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mfluent/asp/ui/HomeSyncDestination;

    const/4 v1, 0x0

    sget-object v2, Lcom/mfluent/asp/ui/HomeSyncDestination;->a:Lcom/mfluent/asp/ui/HomeSyncDestination;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/mfluent/asp/ui/HomeSyncDestination;->b:Lcom/mfluent/asp/ui/HomeSyncDestination;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/mfluent/asp/ui/HomeSyncDestination;->c:Lcom/mfluent/asp/ui/HomeSyncDestination;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->a:[Lcom/mfluent/asp/ui/HomeSyncDestination;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 20
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/ui/HomeSyncDialogFragment;)Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->b:Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;

    return-object v0
.end method

.method static synthetic a()[Lcom/mfluent/asp/ui/HomeSyncDestination;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->a:[Lcom/mfluent/asp/ui/HomeSyncDestination;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 39
    check-cast p1, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;

    iput-object p1, p0, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->b:Lcom/mfluent/asp/ui/HomeSyncDialogFragment$HomeSyncDialogFragmentListener;

    .line 40
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12

    .prologue
    const v11, 0x7f0a0279

    const v10, 0x7f0a0278

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 44
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 47
    const-string v2, "HOMESYNC_HIDEPRIVATE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 50
    const-string v3, "HOMESYNC_SOURCEPERSONAL"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 51
    const-string v4, "HOMESYNC_SOURCEPRIVATE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 52
    const-string v5, "HOMESYNC_FROMFILETAB"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 53
    const-string v6, "HOMESYNC_OWNTRANSFER"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 57
    const v6, 0x7f0a0271

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 60
    if-eqz v0, :cond_3

    .line 61
    if-nez v5, :cond_0

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v11}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    const v2, 0x7f0a0277

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    .line 67
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$1;-><init>(Lcom/mfluent/asp/ui/HomeSyncDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 125
    :goto_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 74
    :cond_0
    if-eqz v4, :cond_1

    .line 75
    new-array v0, v9, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v11}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 76
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$2;-><init>(Lcom/mfluent/asp/ui/HomeSyncDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 83
    :cond_1
    if-eqz v3, :cond_2

    .line 84
    new-array v0, v9, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v11}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    const v2, 0x7f0a0277

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 85
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$3;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$3;-><init>(Lcom/mfluent/asp/ui/HomeSyncDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 97
    :cond_2
    new-array v0, v9, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    const v2, 0x7f0a0277

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 98
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$4;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$4;-><init>(Lcom/mfluent/asp/ui/HomeSyncDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 107
    :cond_3
    if-eqz v2, :cond_4

    .line 108
    new-array v0, v9, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v11}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 117
    :goto_1
    new-instance v2, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$5;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment$5;-><init>(Lcom/mfluent/asp/ui/HomeSyncDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 110
    :cond_4
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v11}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {p0, v10}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    const v2, 0x7f0a0277

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/ui/HomeSyncDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    goto :goto_1
.end method
