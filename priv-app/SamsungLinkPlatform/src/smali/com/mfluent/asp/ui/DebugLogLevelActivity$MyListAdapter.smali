.class public Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/DebugLogLevelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyListAdapter"
.end annotation


# instance fields
.field protected a:Landroid/view/LayoutInflater;

.field final synthetic b:Lcom/mfluent/asp/ui/DebugLogLevelActivity;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/DebugLogLevelActivity;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 210
    iput-object p1, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 211
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->a:Landroid/view/LayoutInflater;

    .line 212
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b(Lcom/mfluent/asp/ui/DebugLogLevelActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 221
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 226
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 233
    if-nez p2, :cond_1

    .line 234
    new-instance v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;

    invoke-direct {v1, v4}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;-><init>(B)V

    .line 236
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030003

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 238
    const v0, 0x7f090023

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->a:Landroid/widget/TextView;

    .line 239
    const v0, 0x7f090024

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->b:Landroid/widget/TextView;

    .line 240
    const v0, 0x7f090025

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->c:Landroid/widget/SeekBar;

    .line 241
    const v0, 0x7f090026

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->d:Landroid/widget/CheckBox;

    .line 242
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 248
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity;

    invoke-static {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->b(Lcom/mfluent/asp/ui/DebugLogLevelActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;

    .line 250
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v2, v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 253
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 254
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->c:Landroid/widget/SeekBar;

    invoke-virtual {v2, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 256
    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b()I

    move-result v2

    .line 261
    iget-object v3, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->b:Landroid/widget/TextView;

    .line 262
    iget-object v4, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->c:Landroid/widget/SeekBar;

    new-instance v5, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter$1;

    invoke-direct {v5, p0, v3, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter$1;-><init>(Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;Landroid/widget/TextView;Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;)V

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 281
    iget-object v0, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->c:Landroid/widget/SeekBar;

    add-int/lit8 v3, v2, -0x2

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 284
    iget-object v0, p0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;->b:Lcom/mfluent/asp/ui/DebugLogLevelActivity;

    iget-object v0, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->b:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/mfluent/asp/ui/DebugLogLevelActivity;->a(Landroid/widget/TextView;I)V

    .line 302
    :cond_0
    :goto_1
    return-object p2

    .line 245
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;

    move-object v1, v0

    goto :goto_0

    .line 285
    :cond_2
    iget-object v2, v0, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->b:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 286
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->c:Landroid/widget/SeekBar;

    invoke-virtual {v2, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 288
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 289
    iget-object v2, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 291
    iget-object v1, v1, Lcom/mfluent/asp/ui/DebugLogLevelActivity$b;->d:Landroid/widget/CheckBox;

    new-instance v2, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter$2;

    invoke-direct {v2, p0, v0}, Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter$2;-><init>(Lcom/mfluent/asp/ui/DebugLogLevelActivity$MyListAdapter;Lcom/mfluent/asp/ui/DebugLogLevelActivity$a;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_1
.end method
