.class final Lcom/mfluent/asp/ui/a/c$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/ui/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mfluent/asp/ui/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/MotionEvent;

.field private final d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/ui/a/c;Landroid/view/View;Landroid/view/MotionEvent;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/c$b;->a:Ljava/lang/ref/WeakReference;

    .line 247
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/ui/a/c$b;->b:Ljava/lang/ref/WeakReference;

    .line 248
    iput-object p3, p0, Lcom/mfluent/asp/ui/a/c$b;->c:Landroid/view/MotionEvent;

    .line 249
    iput-object p4, p0, Lcom/mfluent/asp/ui/a/c$b;->d:Landroid/os/Handler;

    .line 251
    const v0, 0x7f09000a

    invoke-virtual {p2, v0, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 252
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 256
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ui/a/c;

    .line 257
    if-nez v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v1, p0, Lcom/mfluent/asp/ui/a/c$b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 262
    if-eqz v1, :cond_0

    .line 266
    const v2, 0x7f09000a

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    .line 267
    if-ne p0, v2, :cond_0

    .line 271
    const-wide/16 v2, 0x7d0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v0}, Lcom/mfluent/asp/ui/a/c;->a(Lcom/mfluent/asp/ui/a/c;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    .line 272
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_2

    .line 273
    iget-object v2, p0, Lcom/mfluent/asp/ui/a/c$b;->c:Landroid/view/MotionEvent;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 274
    iget-object v2, p0, Lcom/mfluent/asp/ui/a/c$b;->c:Landroid/view/MotionEvent;

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ui/a/c;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ui/a/c$b;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
