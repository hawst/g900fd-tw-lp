.class public Lcom/mfluent/asp/datamodel/am;
.super Lcom/mfluent/asp/datamodel/ap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/am$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "local_caption_path"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "local_caption_index_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/am;->a:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ap;-><init>()V

    .line 41
    return-void
.end method

.method public static g()Lcom/mfluent/asp/datamodel/am;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/mfluent/asp/datamodel/am$a;->a()Lcom/mfluent/asp/datamodel/am;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 3

    .prologue
    .line 89
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 91
    const-string v1, "keyword"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const-string v1, "keyword"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-static {}, Lcom/mfluent/asp/datamodel/an$a;->a()Lcom/mfluent/asp/datamodel/an;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/mfluent/asp/datamodel/an;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 95
    const-string v2, "keyword_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 98
    :cond_0
    const-string v1, "keyword_type"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing required field:keyword_type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    const-string v1, "file_id"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing required field:file_id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_2
    const-string v1, "keyword_type"

    const-string v2, "keyword_type"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 104
    const-string v1, "file_id"

    const-string v2, "file_id"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 105
    const-string v1, "keyword_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing required field:keyword_id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_3
    invoke-super {p0, p1, v0, p3}, Lcom/mfluent/asp/datamodel/ap;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/mfluent/asp/datamodel/am;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 50
    const-string v0, "file_keywords"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string v0, "FILE_KEYWORD_DETAIL"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, "KEYWORD_MAP"

    return-object v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method
