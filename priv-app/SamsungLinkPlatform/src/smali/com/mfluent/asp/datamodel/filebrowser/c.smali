.class public Lcom/mfluent/asp/datamodel/filebrowser/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/filebrowser/c$4;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
        "<",
        "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Z

.field public static b:J

.field private static c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/mfluent/asp/datamodel/Device;

.field private h:Landroid/content/Context;

.field private i:I

.field private j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

.field private k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

.field private l:Ljava/lang/String;

.field private final m:Ljava/util/concurrent/locks/Lock;

.field private n:Z

.field private o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:J

.field private s:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Ljava/text/Collator;

.field private w:I

.field private x:J

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 71
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/c$1;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->d:Ljava/util/Comparator;

    .line 79
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/c$2;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c$2;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->e:Ljava/util/Comparator;

    .line 161
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    .line 162
    const-wide/16 v0, 0x1e

    sput-wide v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->b:J

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    .line 137
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    .line 143
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->p:Ljava/util/Set;

    .line 145
    iput-boolean v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->q:Z

    .line 157
    iput v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    .line 163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    .line 164
    iput v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    .line 165
    iput-boolean v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    .line 166
    iput-boolean v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->A:Z

    .line 174
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    .line 175
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    .line 177
    sget-boolean v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/SL_FILE"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 179
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    .line 183
    :cond_0
    return-void
.end method

.method private a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lorg/json/JSONObject;)I
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 1006
    const/4 v7, -0x1

    .line 1009
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1010
    const-string v2, "vnd.android.document/directory"

    move-object v5, v2

    .line 1014
    :goto_0
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1015
    const-string v2, "video/"

    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string v2, "image/"

    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string v2, "music/"

    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string v2, "audio/"

    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    :cond_0
    move v0, v1

    .line 1019
    :cond_1
    if-eqz v0, :cond_7

    .line 1022
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1023
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v9

    .line 1024
    const-string v0, "INFO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "guessMetaDataLocally strDisplayName="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",strSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    if-eqz v0, :cond_6

    .line 1026
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "device_id=? AND mime_type=? AND _size=? AND _display_name=?"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v10

    const/4 v10, 0x1

    aput-object v5, v4, v10

    const/4 v5, 0x2

    aput-object v8, v4, v5

    const/4 v5, 0x3

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1032
    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 1033
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1034
    const-string v0, "_id"

    invoke-static {v1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    long-to-int v2, v2

    .line 1035
    :try_start_2
    const-string v0, "INFO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fnd and set nMediaId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    const-string v0, "media_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1037
    const-string v3, "orientation"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1038
    const-string v4, "width"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1039
    const-string v5, "height"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 1040
    const-string v6, "album_id"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1041
    const-string v8, "device_id"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    int-to-long v8, v8

    .line 1042
    const-string v10, "asp_mediaType"

    invoke-virtual {p2, v10, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1043
    const-string v0, "asp_orientation"

    invoke-virtual {p2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1044
    const-string v0, "asp_width"

    invoke-virtual {p2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1045
    const-string v0, "asp_height"

    invoke-virtual {p2, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1046
    const-string v0, "asp_albumId"

    invoke-virtual {p2, v0, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1047
    const-string v0, "asp_deviceId"

    invoke-virtual {p2, v0, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1053
    :goto_1
    if-eqz v1, :cond_2

    .line 1054
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1059
    :cond_2
    :goto_2
    :try_start_3
    const-string v0, "mediaId"

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 1064
    :goto_3
    return v2

    .line 1012
    :cond_3
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    goto/16 :goto_0

    .line 1050
    :catch_0
    move-exception v0

    move-object v1, v6

    move v2, v7

    .line 1051
    :goto_4
    :try_start_4
    const-string v3, "Err"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FileBrowser.MEDIA_ID Exception="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1053
    if-eqz v1, :cond_2

    .line 1054
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1053
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_5
    if-eqz v1, :cond_4

    .line 1054
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1060
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 1053
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 1050
    :catch_2
    move-exception v0

    move v2, v7

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_4

    :cond_5
    move v2, v7

    goto :goto_1

    :cond_6
    move-object v1, v6

    move v2, v7

    goto :goto_1

    :cond_7
    move v2, v7

    goto :goto_2
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 66
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v3

    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez v2, :cond_2

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v3

    if-nez v2, :cond_0

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a()Ljava/text/CollationKey;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a()Ljava/text/CollationKey;

    move-result-object v3

    if-nez v2, :cond_4

    if-eqz v3, :cond_0

    :cond_4
    if-eqz v2, :cond_0

    if-nez v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {v2, v3}, Ljava/text/CollationKey;->compareTo(Ljava/text/CollationKey;)I

    move-result v0

    goto :goto_0
.end method

.method private a(ZZ)I
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 388
    const/4 v8, 0x0

    .line 393
    const/4 v10, 0x0

    .line 394
    const/4 v7, 0x0

    .line 395
    const-wide/16 v14, 0x0

    .line 396
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-nez v4, :cond_0

    .line 397
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "This file browser was never initialized"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 400
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v4, v5, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v4

    sget-object v5, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v4, v5, :cond_1

    .line 402
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->i()V

    .line 403
    const/4 v4, 0x2

    .line 729
    :goto_0
    return v4

    .line 406
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 408
    invoke-static {}, Lcom/mfluent/asp/c;->a()Lcom/mfluent/asp/c;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->s:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-virtual {v4, v5}, Lcom/mfluent/asp/c;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSortType;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 409
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->t:Ljava/lang/String;

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 410
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 413
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_a

    move-result-object v4

    .line 418
    :cond_2
    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/mfluent/asp/datamodel/filebrowser/c$4;->a:[I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->ordinal()I

    move-result v11

    aget v5, v5, v11

    packed-switch v5, :pswitch_data_0

    const-string v5, "/api/pCloud/device/files/"

    :goto_2
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 419
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "&sort="

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v4, ""

    :goto_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "&maxItems=100&isPersonal="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v4

    sget-object v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v4, v9, :cond_5

    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 427
    const/4 v4, 0x0

    .line 428
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_30

    if-nez p1, :cond_30

    .line 429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v17, v4

    .line 432
    :goto_5
    if-eqz v17, :cond_3

    .line 434
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "&fileName="

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v11, "UTF-8"

    invoke-static {v9, v11}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_9

    move-result-object v6

    .line 439
    :cond_3
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "?_="

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 440
    const-string v9, "INFO"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "file20 request = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :try_start_2
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    invoke-static {v4}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 444
    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v9, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 446
    sget-boolean v4, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    if-eqz v4, :cond_6

    if-nez p1, :cond_6

    .line 447
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 448
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/b;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v12

    invoke-virtual {v4, v5, v6, v12, v11}, Lcom/samsung/android/sdk/samsung/b;->a(Ljava/lang/String;Ljava/lang/String;ILjava/io/ByteArrayOutputStream;)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    .line 449
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    const-wide/16 v20, 0x0

    cmp-long v4, v12, v20

    if-lez v4, :cond_2f

    .line 450
    const/4 v7, 0x1

    .line 451
    new-instance v4, Lorg/json/JSONObject;

    new-instance v8, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v11

    invoke-direct {v8, v11}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v4, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 452
    const-string v8, "INFO"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "ASP20FileBrowser Cache Hit! directory="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :goto_7
    move v12, v7

    .line 463
    :goto_8
    const/4 v7, 0x1

    if-eq v12, v7, :cond_2d

    .line 464
    if-eqz p2, :cond_7

    move v4, v12

    .line 465
    goto/16 :goto_0

    .line 418
    :pswitch_0
    const-string v5, "/api/pCloud/device/secured/"

    goto/16 :goto_2

    .line 419
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "&search="

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 454
    :cond_6
    :try_start_3
    sget-boolean v4, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    if-eqz v4, :cond_2e

    const/4 v4, 0x1

    move/from16 v0, p1

    if-ne v0, v4, :cond_2e

    .line 455
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/b;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v11

    invoke-virtual {v4, v5, v6, v11}, Lcom/samsung/android/sdk/samsung/b;->a(Ljava/lang/String;Ljava/lang/String;I)J

    move-result-wide v12

    .line 456
    sget-wide v20, Lcom/mfluent/asp/datamodel/filebrowser/c;->b:J

    cmp-long v4, v12, v20

    if-gez v4, :cond_2e

    .line 457
    const-string v4, "INFO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ASP20FileBrowser no need check LMT updated before "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "sec"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 467
    :cond_7
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    const/4 v7, 0x0

    const/16 v8, 0x14

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v11, 0x1

    invoke-static {v9, v4, v7, v8, v11}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;Lorg/apache/http/client/HttpRequestRetryHandler;Ljava/lang/Integer;Z)Lorg/json/JSONObject;

    move-result-object v4

    move-object/from16 v16, v4

    .line 470
    :goto_9
    sget-object v4, Lcom/mfluent/asp/datamodel/filebrowser/c;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v7, 0x2

    if-gt v4, v7, :cond_8

    .line 471
    const-string v4, "mfl_ASP20FileBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Got json response: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_8
    const-string v4, "totalCount"

    const/4 v7, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 475
    if-gez v4, :cond_9

    .line 477
    invoke-static/range {v16 .. v16}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lorg/json/JSONObject;)V

    .line 478
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getNextWindow: result: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "result"

    const-string v7, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 702
    :catch_0
    move-exception v4

    .line 703
    move-object/from16 v0, p0

    iget v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    .line 704
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 705
    new-instance v5, Ljava/lang/InterruptedException;

    invoke-virtual {v4}, Ljava/io/InterruptedIOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 481
    :cond_9
    :try_start_4
    sget-boolean v7, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    if-nez v7, :cond_11

    .line 482
    const-string v7, "dirRevision"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v8, v9}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 483
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->q:Z

    if-eqz v7, :cond_b

    .line 484
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->r:J

    move-wide v8, v14

    .line 515
    :cond_a
    :goto_a
    if-eqz p1, :cond_12

    .line 516
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/b;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/samsung/b;->b(Ljava/lang/String;Ljava/lang/String;I)V

    .line 517
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 486
    :cond_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->r:J

    move-wide/from16 v20, v0

    cmp-long v7, v8, v20

    if-eqz v7, :cond_10

    .line 487
    sget-object v4, Lcom/mfluent/asp/datamodel/filebrowser/c;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x3

    if-gt v4, v5, :cond_c

    .line 488
    const-string v4, "mfl_ASP20FileBrowser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getNextWindow:Got json response: dirRevision: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " != "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->r:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->destroy()V

    .line 493
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->p:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/DataSetObserver;

    .line 494
    invoke-virtual {v4}, Landroid/database/DataSetObserver;->onInvalidated()V
    :try_end_4
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_b

    .line 706
    :catch_1
    move-exception v4

    .line 707
    move-object/from16 v0, p0

    iget v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    .line 708
    invoke-virtual {v4}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->a()I

    move-result v5

    const/16 v6, 0x190

    if-eq v5, v6, :cond_d

    invoke-virtual {v4}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->a()I

    move-result v5

    const/16 v6, 0x194

    if-ne v5, v6, :cond_e

    .line 710
    :cond_d
    :try_start_5
    new-instance v5, Lorg/json/JSONObject;

    invoke-virtual {v4}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 711
    invoke-static {v5}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lorg/json/JSONObject;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_6

    .line 717
    :cond_e
    throw v4

    .line 496
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_10
    move-wide v8, v14

    .line 499
    goto/16 :goto_a

    .line 500
    :cond_11
    :try_start_6
    const-string v7, "dirRevision"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v8, v9}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 501
    const-wide/16 v14, 0x0

    cmp-long v7, v8, v14

    if-lez v7, :cond_a

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    const-wide/16 v20, 0x0

    cmp-long v7, v14, v20

    if-lez v7, :cond_a

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    cmp-long v7, v8, v14

    if-eqz v7, :cond_a

    .line 503
    const-string v4, "INFO"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "dirty cache detected dir="

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", old lmt="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", new lmt="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/b;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-virtual {v4, v5, v7}, Lcom/samsung/android/sdk/samsung/b;->a(Ljava/lang/String;I)V

    .line 505
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    .line 506
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->destroy()V

    .line 508
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 509
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 510
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/b;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    const-string v11, "application/json"

    invoke-virtual/range {v4 .. v11}, Lcom/samsung/android/sdk/samsung/b;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/io/ByteArrayOutputStream;Ljava/lang/String;)Z

    .line 511
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 520
    :cond_12
    if-ltz v4, :cond_13

    .line 521
    move-object/from16 v0, p0

    iput v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    .line 524
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-nez v4, :cond_14

    .line 525
    new-instance v4, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    invoke-direct {v4, v7}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    .line 528
    :cond_14
    const-string v4, "path"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 529
    const-string v7, "mycomputer"

    invoke-static {v7, v4}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 530
    const-string v7, ""

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    .line 535
    :goto_c
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 536
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    invoke-static {v7}, Lorg/apache/commons/io/FilenameUtils;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 537
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 539
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    invoke-static {v7}, Lorg/apache/commons/io/FilenameUtils;->getFullPathNoEndSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 540
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v11, "\\"

    invoke-static {v7, v11}, Lorg/apache/commons/lang3/StringUtils;->removeEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 543
    :cond_15
    const/4 v4, 0x0

    .line 544
    const-string v7, "special"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 545
    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z
    :try_end_6
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-result v11

    if-eqz v11, :cond_16

    .line 547
    :try_start_7
    invoke-static {v7}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_7 .. :try_end_7} :catch_2

    move-result-object v4

    .line 552
    :cond_16
    :goto_d
    :try_start_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 554
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v4

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v4, v7, :cond_1c

    .line 555
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/c;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "\\"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    .line 557
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 558
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/c;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 571
    :cond_17
    :goto_e
    const-string v4, "files"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 572
    if-nez v4, :cond_2c

    .line 574
    const-string v4, "file"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    move-object v14, v4

    .line 577
    :goto_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    .line 579
    const/4 v4, 0x0

    move v13, v4

    move v4, v10

    :goto_10
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v13, v7, :cond_26

    .line 580
    invoke-virtual {v14, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    .line 581
    const-string v7, "name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 582
    const-string v7, "uri"

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 583
    const/4 v7, 0x0

    .line 584
    const-string v20, "special"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 585
    invoke-static/range {v20 .. v20}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z
    :try_end_8
    .catch Ljava/io/InterruptedIOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    move-result v21

    if-eqz v21, :cond_18

    .line 587
    :try_start_9
    invoke-static/range {v20 .. v20}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/io/InterruptedIOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_9 .. :try_end_9} :catch_2

    move-result-object v7

    .line 594
    :cond_18
    :goto_11
    if-nez v13, :cond_1e

    :try_start_a
    const-string v20, ".."

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1e

    .line 596
    new-instance v19, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct/range {v19 .. v19}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 600
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v10, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 602
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v7

    sget-object v10, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v7, v10, :cond_19

    .line 604
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_19

    .line 605
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    const-string v10, "mycomputer"

    invoke-virtual {v7, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 606
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/c;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 609
    :cond_19
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 610
    move-object/from16 v0, p0

    iget v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, p0

    iput v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    .line 611
    new-instance v7, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    invoke-direct {v7, v10}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    .line 579
    :cond_1a
    :goto_12
    add-int/lit8 v7, v13, 0x1

    move v13, v7

    goto/16 :goto_10

    .line 532
    :cond_1b
    new-instance v7, Ljava/lang/String;

    const/16 v11, 0x8

    invoke-static {v4, v11}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v11

    const-string v13, "UTF-8"

    invoke-direct {v7, v11, v13}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;
    :try_end_a
    .catch Ljava/io/InterruptedIOException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_c

    .line 718
    :catch_2
    move-exception v4

    .line 719
    move-object/from16 v0, p0

    iget v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    .line 720
    throw v4

    .line 560
    :cond_1c
    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v4

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v4, v7, :cond_17

    .line 561
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v4

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)Ljava/lang/String;

    move-result-object v7

    .line 562
    const-string v4, "/"

    .line 563
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    invoke-static {v11, v4}, Lorg/apache/commons/lang3/StringUtils;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1d

    .line 564
    const-string v4, ""

    .line 566
    :cond_1d
    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 567
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;
    :try_end_b
    .catch Ljava/io/InterruptedIOException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_e

    .line 721
    :catch_3
    move-exception v4

    .line 722
    move-object/from16 v0, p0

    iget v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    .line 723
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Trouble fetching next ASP10 window"

    invoke-direct {v5, v6, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 613
    :cond_1e
    if-nez v13, :cond_1f

    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->t:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1f

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1f

    sget-object v20, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1f

    .line 617
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    .line 618
    const-string v20, "INFO"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "ASP10 server will only return .. "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", count="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_1f
    new-instance v20, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct/range {v20 .. v20}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 623
    const-string v21, "isDirectory"

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v21

    .line 624
    if-eqz v21, :cond_20

    .line 625
    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 628
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V

    .line 629
    const-string v21, "fileSize"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v22

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(J)V

    .line 630
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->v:Ljava/text/Collator;

    move-object/from16 v21, v0

    if-eqz v21, :cond_21

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->v:Ljava/text/Collator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/text/Collator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/text/CollationKey;)V

    .line 634
    :cond_21
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 635
    const-string v10, "modified"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v10, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v22, 0x3e8

    mul-long v10, v10, v22

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(J)V

    .line 636
    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 637
    const-string v7, "mediaId"

    const-wide/16 v10, -0xa

    move-object/from16 v0, v19

    invoke-virtual {v0, v7, v10, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
    :try_end_c
    .catch Ljava/io/InterruptedIOException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_c .. :try_end_c} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3

    move-result-wide v10

    .line 638
    const-wide/16 v22, -0x1

    cmp-long v7, v10, v22

    if-nez v7, :cond_23

    .line 641
    :try_start_d
    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 642
    if-eqz v7, :cond_23

    .line 643
    const-string v21, "image/"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-gez v21, :cond_22

    const-string v21, "video/"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-gez v21, :cond_22

    const-string v21, "audio/"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-ltz v7, :cond_23

    .line 644
    :cond_22
    const-wide/16 v10, -0xa

    .line 645
    const-string v7, "INFO"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "check asp_id again for "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-static {v7, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/io/InterruptedIOException; {:try_start_d .. :try_end_d} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_d .. :try_end_d} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_d .. :try_end_d} :catch_2

    .line 652
    :cond_23
    :goto_13
    const-wide/16 v22, -0xa

    cmp-long v7, v10, v22

    if-nez v7, :cond_24

    .line 653
    :try_start_e
    const-string v4, "INFO"

    const-string v7, "meidiaID is not decided!"

    invoke-static {v4, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lorg/json/JSONObject;)I
    :try_end_e
    .catch Ljava/io/InterruptedIOException; {:try_start_e .. :try_end_e} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_e .. :try_end_e} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    move-result v4

    int-to-long v10, v4

    .line 655
    const/4 v4, 0x1

    .line 657
    :cond_24
    long-to-int v7, v10

    :try_start_f
    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(I)V

    const-wide/16 v22, 0x0

    cmp-long v7, v10, v22

    if-lez v7, :cond_25

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v10, "asp_mediaType"

    const/4 v11, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    const-string v11, "asp_orientation"

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v11, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v11

    const-string v21, "asp_width"

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v21

    const-string v22, "asp_height"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v22

    const-string v23, "asp_albumId"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-wide/from16 v2, v24

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v24

    const-string v23, "asp_deviceId"

    const/16 v26, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v19

    const-string v23, "asp_mediaType"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v10, "asp_orientation"

    invoke-virtual {v7, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v10, "asp_width"

    move/from16 v0, v21

    invoke-virtual {v7, v10, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v10, "asp_height"

    move/from16 v0, v22

    invoke-virtual {v7, v10, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v10, "asp_albumId"

    move-wide/from16 v0, v24

    invoke-virtual {v7, v10, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v10, "asp_deviceId"

    move/from16 v0, v19

    invoke-virtual {v7, v10, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Landroid/os/Bundle;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/io/InterruptedIOException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_f .. :try_end_f} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_f .. :try_end_f} :catch_2

    .line 658
    :cond_25
    :goto_14
    :try_start_10
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    move-object/from16 v0, v20

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 660
    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 661
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v7, v10}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setIsDirectory(I)V

    goto/16 :goto_12

    .line 648
    :catch_4
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_13

    .line 657
    :catch_5
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_14

    .line 666
    :cond_26
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v7

    sget-object v10, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v7, v10, :cond_27

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-nez v7, :cond_27

    .line 669
    new-instance v7, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 670
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    sget-object v10, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    invoke-virtual {v7, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 672
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 675
    :cond_27
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V

    .line 677
    sget-object v7, Lcom/mfluent/asp/datamodel/filebrowser/c;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v7}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v7

    const/4 v10, 0x3

    if-gt v7, v10, :cond_28

    .line 678
    const-string v10, "mfl_ASP20FileBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v11, "getNextWindow: "

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "-"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, " of "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", dir: "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", parent: "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", sort: "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-eqz v17, :cond_2b

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v13, ", from: "

    invoke-direct {v7, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_15
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    :cond_28
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->x:J

    .line 695
    sget-boolean v7, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    if-eqz v7, :cond_2a

    const/4 v7, 0x1

    if-ne v12, v7, :cond_29

    if-eqz v4, :cond_2a

    .line 697
    :cond_29
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 698
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 699
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->h:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsung/b;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    const-string v11, "application/json"

    invoke-virtual/range {v4 .. v11}, Lcom/samsung/android/sdk/samsung/b;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/io/ByteArrayOutputStream;Ljava/lang/String;)Z
    :try_end_10
    .catch Ljava/io/InterruptedIOException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3

    .line 725
    :cond_2a
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    .line 728
    invoke-static {}, Lcom/mfluent/asp/datamodel/a/e;->b()V

    move v4, v12

    .line 729
    goto/16 :goto_0

    .line 678
    :cond_2b
    :try_start_11
    const-string v7, ""
    :try_end_11
    .catch Ljava/io/InterruptedIOException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3

    goto :goto_15

    .line 713
    :catch_6
    move-exception v5

    throw v4

    :catch_7
    move-exception v7

    goto/16 :goto_d

    :catch_8
    move-exception v20

    goto/16 :goto_11

    :catch_9
    move-exception v4

    goto/16 :goto_6

    :catch_a
    move-exception v5

    goto/16 :goto_1

    :cond_2c
    move-object v14, v4

    goto/16 :goto_f

    :cond_2d
    move-object/from16 v16, v4

    goto/16 :goto_9

    :cond_2e
    move-object v4, v7

    move v12, v8

    goto/16 :goto_8

    :cond_2f
    move-object v4, v7

    move v7, v8

    goto/16 :goto_7

    :cond_30
    move-object/from16 v17, v4

    goto/16 :goto_5

    .line 418
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private a(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 829
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_0

    .line 866
    :goto_0
    return-object v0

    .line 833
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_1

    .line 834
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    goto :goto_0

    .line 837
    :cond_1
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 840
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v2, p1, v2

    div-int/lit8 v2, v2, 0x64

    add-int/lit8 v2, v2, 0x1

    .line 842
    sget-object v3, Lcom/mfluent/asp/datamodel/filebrowser/c;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v6, :cond_2

    .line 843
    const-string v3, "mfl_ASP20FileBrowser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getFile: not in cache: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", windows: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt p1, v3, :cond_5

    .line 849
    iget-boolean v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->n:Z

    if-eqz v3, :cond_3

    .line 850
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "ASP10FileBrowser destroyed"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 866
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 853
    :cond_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_1
    invoke-direct {p0, v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I

    .line 855
    add-int/lit8 v1, v1, 0x1

    .line 856
    if-le v1, v2, :cond_2

    .line 857
    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/c;->c:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_4

    .line 858
    const-string v1, "mfl_ASP20FileBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getFile: file doesn\'t exist! "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 860
    :cond_4
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 864
    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 866
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0
.end method

.method private static a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 762
    if-nez p0, :cond_0

    .line 763
    const-string v0, ""

    .line 774
    :goto_0
    return-object v0

    .line 766
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/c$4;->a:[I

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 774
    const-string v0, ""

    goto :goto_0

    .line 768
    :pswitch_0
    const-string v0, "personal"

    goto :goto_0

    .line 770
    :pswitch_1
    const-string v0, "secured"

    goto :goto_0

    .line 772
    :pswitch_2
    const-string v0, "shared"

    goto :goto_0

    .line 766
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lorg/json/JSONObject;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mfluent/asp/common/datamodel/ASPFileException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 745
    const-string v0, "result"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 746
    const-string v1, "resultDetail"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 747
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "-705343"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 748
    invoke-static {v5}, Lcom/mfluent/asp/datamodel/a/e;->a(I)V

    .line 749
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextWindow: result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/common/datamodel/ASPFileException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 751
    :cond_0
    const-string v1, "-705342"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "-405343"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "-405344"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 752
    :cond_1
    invoke-static {v4}, Lcom/mfluent/asp/datamodel/a/e;->a(I)V

    .line 753
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPFileException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextWindow: result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 755
    :cond_2
    return-void
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v3, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 281
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 285
    new-array v1, v3, [Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    sget-object v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v2, v1, v0

    sget-object v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v2, v1, v5

    .line 289
    new-array v2, v3, [Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_SHARED:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v3, v2, v0

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PERSONAL:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v3, v2, v4

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PRIVATE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v3, v2, v5

    .line 291
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 292
    new-instance v3, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v3}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 293
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 294
    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 295
    aget-object v4, v1, v0

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 296
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V

    .line 297
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 298
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 302
    array-length v0, v1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    .line 303
    return-void
.end method

.method private static j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 758
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 871
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->s:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->s:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->u:Z

    .line 187
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 197
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 199
    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    .line 200
    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    .line 201
    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->A:Z

    .line 203
    if-nez p1, :cond_3

    .line 204
    :try_start_0
    new-instance p1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 205
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 206
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 207
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v3, :cond_0

    .line 208
    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    invoke-virtual {p1, v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 215
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 216
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->s:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 217
    iput-object p3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->t:Ljava/lang/String;

    .line 219
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 221
    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->u:Z

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->k()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 222
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->v:Ljava/text/Collator;

    .line 223
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->v:Ljava/text/Collator;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/text/Collator;->setStrength(I)V

    .line 227
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    .line 228
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->n:Z

    .line 229
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    .line 230
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->r:J

    .line 232
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->q:Z

    .line 233
    const-string v1, "INFO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "directory.getUri()="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->g:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v3, :cond_1

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ROOT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    const-string v1, "INFO"

    const-string v3, "its\' PC!!!"

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_1
    const-string v1, "mycomputer"

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_6

    .line 247
    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-direct {p0, v1, v3}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 268
    :cond_2
    :goto_2
    if-ne v0, v2, :cond_7

    :try_start_2
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCount()I

    move-result v1

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v3

    if-le v1, v3, :cond_7

    .line 269
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I

    move-result v0

    goto :goto_2

    .line 212
    :cond_3
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->d()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 274
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 225
    :cond_4
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->v:Ljava/text/Collator;

    goto/16 :goto_1

    .line 249
    :catch_0
    move-exception v1

    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 250
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 251
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move v1, v0

    .line 253
    :goto_3
    if-nez v1, :cond_2

    .line 254
    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I

    move-result v0

    .line 255
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-nez v3, :cond_5

    move v1, v2

    .line 256
    goto :goto_3

    .line 258
    :cond_5
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    iput-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 259
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 260
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    goto :goto_3

    .line 265
    :cond_6
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I

    move-result v0

    goto :goto_2

    .line 271
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->q:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 274
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 276
    const-string v1, "INFO"

    const-string v2, "ASP20FileBrowser Init"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    .line 278
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->w:I

    return v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->u:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->s:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->d:Ljava/util/Comparator;

    :goto_0
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 313
    :cond_0
    return-void

    .line 312
    :cond_1
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/c;->e:Ljava/util/Comparator;

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 320
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deselectAll()V
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-eqz v0, :cond_0

    .line 935
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->deselectAll()V

    .line 937
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 784
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->n:Z

    .line 785
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    .line 326
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onInvalidated()V

    goto :goto_0

    .line 328
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 332
    .line 333
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 334
    invoke-direct {p0, v0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I

    move-result v2

    iput v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    .line 335
    const-string v2, "URLINFO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "checkNextWindow getCount() > getCurrentSize() cache="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    if-eqz v2, :cond_1

    .line 348
    const-string v0, "URLINFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkNextWindow isRemoteDirChanged="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 351
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v3

    if-gt v2, v3, :cond_3

    .line 352
    const-string v0, "URLINFO"

    const-string v2, "checkNextWindow full true"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :goto_1
    return v1

    .line 336
    :cond_2
    iget v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    if-ne v2, v1, :cond_0

    .line 338
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->A:Z

    .line 339
    invoke-direct {p0, v1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(ZZ)I

    .line 340
    const-string v2, "URLINFO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "checkNextWindow isDuringLMTCheck=true isRemoteDirChanged="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",getCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",getCurrentSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 359
    const/4 v1, 0x0

    .line 360
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 361
    const-string v1, "URLINFO"

    const-string v2, "checkNeedBackgroundJobInCursor currentsize not enough"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :goto_0
    return v0

    .line 363
    :cond_0
    iget v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->y:I

    if-ne v2, v0, :cond_1

    .line 364
    const-string v1, "URLINFO"

    const-string v2, "checkNeedBackgroundJobInCursor nLastGetWindowResult == RET_CACHE_HIT"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 367
    :cond_1
    const-string v0, "URLINFO"

    const-string v2, "checkNeedBackgroundJobInCursor bRet = false"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 820
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->i:I

    return v0
.end method

.method public bridge synthetic getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    return-object v0
.end method

.method public getCurrentDirectoryAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->l:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    return-object v0
.end method

.method public getSelectedFileIndexes()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 979
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-nez v0, :cond_0

    .line 980
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 981
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 986
    :goto_0
    return-object v0

    .line 984
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->getSelectedFileIndexes()Ljava/util/Iterator;

    move-result-object v1

    .line 986
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/c$3;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/c$3;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/c;Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 373
    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->A:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->z:Z

    if-nez v1, :cond_0

    .line 374
    const-string v1, "URLINFO"

    const-string v2, "checkNeedUpdateCursor false"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->A:Z

    .line 379
    :goto_0
    return v0

    .line 378
    :cond_0
    const-string v0, "URLINFO"

    const-string v1, "checkNeedUpdateCursor true"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic init(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    return-void
.end method

.method public isAllSelected()Z
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isAllSelected()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected(I)Z
    .locals 1

    .prologue
    .line 914
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isSelected(I)Z

    move-result v0

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 954
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 957
    :cond_0
    return-void
.end method

.method public selectAll()V
    .locals 1

    .prologue
    .line 923
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectAll()V

    .line 926
    :cond_0
    return-void
.end method

.method public setSelected(IZ)V
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->o:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 906
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 970
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ASP10FileBrowser: curDir: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->j:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->k:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", full path: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/c;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 966
    return-void
.end method
