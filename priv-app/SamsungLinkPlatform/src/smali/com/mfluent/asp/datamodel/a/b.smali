.class public final Lcom/mfluent/asp/datamodel/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 31
    :try_start_0
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 37
    if-nez v1, :cond_0

    .line 100
    :goto_0
    return-object v2

    .line 32
    :catch_0
    move-exception v0

    .line 33
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid rowId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 43
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 45
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 49
    :cond_1
    :try_start_2
    const-string v0, "media_type"

    invoke-static {v1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getIntOrThrow(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 50
    sparse-switch v0, :sswitch_data_0

    .line 64
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No delete task for media type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 52
    :sswitch_0
    :try_start_3
    new-instance v0, Lcom/mfluent/asp/media/i;

    invoke-direct {v0}, Lcom/mfluent/asp/media/i;-><init>()V

    .line 67
    :goto_1
    new-instance v3, Lcom/mfluent/asp/datamodel/a/b$1;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/datamodel/a/b$1;-><init>(Lcom/mfluent/asp/datamodel/a/b;)V

    .line 86
    const-string v4, "_id"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const-string v6, "filename"

    invoke-virtual {v0, v4, v5, v6, v3}, Lcom/mfluent/asp/media/j;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/media/l;)V

    .line 88
    new-instance v3, Lcom/mfluent/asp/datamodel/a/b$2;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/datamodel/a/b$2;-><init>(Lcom/mfluent/asp/datamodel/a/b;)V

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/content/Context;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/mfluent/asp/media/j;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 97
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 55
    :sswitch_1
    :try_start_4
    new-instance v0, Lcom/mfluent/asp/media/k;

    invoke-direct {v0}, Lcom/mfluent/asp/media/k;-><init>()V

    goto :goto_1

    .line 58
    :sswitch_2
    new-instance v0, Lcom/mfluent/asp/media/g;

    invoke-direct {v0}, Lcom/mfluent/asp/media/g;-><init>()V

    goto :goto_1

    .line 61
    :sswitch_3
    new-instance v0, Lcom/mfluent/asp/media/h;

    invoke-direct {v0}, Lcom/mfluent/asp/media/h;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method
