.class final Lcom/mfluent/asp/datamodel/filebrowser/d$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/filebrowser/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field public a:I

.field public final b:Ljava/util/concurrent/locks/Condition;

.field public c:I

.field public d:Z

.field private e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

.field private final f:Landroid/database/DataSetObserver;

.field private final g:Landroid/database/ContentObserver;

.field private final h:Landroid/content/ContentResolver;

.field private i:[Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/util/concurrent/locks/Condition;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->i:[Landroid/net/Uri;

    .line 121
    iput v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    .line 122
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->d:Z

    .line 125
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    .line 126
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->h:Landroid/content/ContentResolver;

    .line 128
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d$1;

    invoke-direct {v0, p0, p3}, Lcom/mfluent/asp/datamodel/filebrowser/d$d$1;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/d$d;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->g:Landroid/database/ContentObserver;

    .line 136
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/filebrowser/d$d$2;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/d$d;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->f:Landroid/database/DataSetObserver;

    .line 148
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/filebrowser/d$d;)V
    .locals 6

    .prologue
    .line 112
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->i:[Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->i:[Landroid/net/Uri;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->h:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/mfluent/asp/datamodel/filebrowser/d$d;)Lcom/mfluent/asp/datamodel/filebrowser/d$a;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 175
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_0

    .line 177
    iput-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    .line 178
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->h:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->g:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 180
    :cond_0
    iput-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->i:[Landroid/net/Uri;

    .line 181
    iput v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    .line 182
    iput v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I

    .line 183
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;[Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 159
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eq p1, v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a()V

    .line 163
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    .line 164
    if-eqz p1, :cond_1

    .line 165
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->e:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->f:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 166
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->i:[Landroid/net/Uri;

    .line 167
    if-eqz p2, :cond_1

    .line 168
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->h:Landroid/content/ContentResolver;

    aget-object v1, p2, v3

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->g:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 172
    :cond_1
    return-void
.end method
