.class public final Lcom/mfluent/asp/datamodel/an;
.super Lcom/mfluent/asp/datamodel/ao;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/an$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ao;-><init>()V

    .line 26
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x2ee

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/an;->a:Landroid/util/LruCache;

    .line 39
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/an;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;I)I
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 160
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 162
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/an;->a:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 163
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "KEYWORD_ORPHANS"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "keyword"

    aput-object v3, v2, v9

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_1

    .line 166
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 168
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/an;->a:Landroid/util/LruCache;

    invoke-virtual {v2, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 171
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 175
    :cond_1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-virtual {v8, v9, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 176
    const-string v0, "_id IN ("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string v0, "SELECT _id"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const-string v0, " FROM KEYWORD_ORPHANS)"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180
    const-string v0, " AND ("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183
    :cond_2
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "KEYWORDS"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 43
    const-string v0, "genre"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 83
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    :goto_0
    return-object v5

    .line 87
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 92
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v0, :cond_8

    .line 93
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    move-object v8, v0

    .line 95
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/an;->a:Landroid/util/LruCache;

    invoke-virtual {v0, v10}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 96
    if-nez v0, :cond_7

    if-eqz v8, :cond_7

    .line 97
    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    move-object v9, v0

    .line 99
    :goto_2
    if-nez v9, :cond_4

    .line 101
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "KEYWORDS"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "keyword=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v10, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_1

    .line 112
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 113
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 116
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v9, v0

    .line 120
    :cond_1
    if-nez v9, :cond_2

    .line 121
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 122
    const-string v1, "keyword"

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "KEYWORDS"

    invoke-virtual {v1, v2, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 125
    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 128
    :cond_2
    if-nez v8, :cond_3

    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v0, :cond_3

    .line 129
    new-instance v8, Ljava/util/HashMap;

    const/16 v0, 0x32

    invoke-direct {v8, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 130
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0, p0, v8}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;Ljava/lang/Object;)V

    .line 133
    :cond_3
    if-eqz v8, :cond_5

    .line 134
    invoke-virtual {v8, v10, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    :goto_4
    move-object v5, v9

    .line 140
    goto/16 :goto_0

    .line 116
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 136
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/an;->a:Landroid/util/LruCache;

    invoke-virtual {v0, v10, v9}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_6
    move-object v0, v9

    goto :goto_3

    :cond_7
    move-object v9, v0

    goto :goto_2

    :cond_8
    move-object v8, v5

    goto/16 :goto_1
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 146
    check-cast p3, Ljava/util/HashMap;

    .line 148
    invoke-virtual {p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 149
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 150
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/an;->a:Landroid/util/LruCache;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 152
    :cond_0
    return-void
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 48
    const-string v0, "genre"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "KEYWORDS"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "KEYWORDS"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$KeywordList;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$KeywordList;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
