.class public final Lcom/mfluent/asp/datamodel/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 21
    invoke-static {p1}, Lcom/samsung/android/sdk/samsung/c;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/c;

    move-result-object v1

    .line 24
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 25
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.VIDEO_OPTIMIZATION"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsung/c;->a()Z

    move-result v1

    .line 27
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSetting.VALUE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 29
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 30
    const-string v1, "method_result_str"

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->BOOLEAN:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.MARKETING_PUSH"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 32
    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsung/c;->b()Z

    move-result v1

    .line 33
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSetting.VALUE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 35
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 36
    const-string v1, "method_result_str"

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->BOOLEAN:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return v0
.end method
