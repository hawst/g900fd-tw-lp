.class public final Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;
.super Lcom/mfluent/asp/datamodel/k;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$1;,
        Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;,
        Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;
    }
.end annotation


# static fields
.field private static final i:[Ljava/lang/String;

.field private static j:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final k:[Ljava/lang/String;

.field private final l:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field private final m:Ljava/lang/String;

.field private n:I

.field private o:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private volatile p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "asp_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "document_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "last_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "home_sync_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "device_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->i:[Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/k;-><init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->n:I

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->o:Ljava/util/concurrent/Future;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->p:Z

    .line 144
    if-nez p5, :cond_0

    .line 145
    sget-object p5, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->i:[Ljava/lang/String;

    .line 147
    :cond_0
    iput-object p5, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->k:[Ljava/lang/String;

    .line 149
    array-length v0, p5

    new-array v1, v0, [Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p5

    if-ge v0, v2, :cond_11

    aget-object v2, p5, v0

    const-string v3, "_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v3, "asp_media_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_2
    const-string v3, "document_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_3
    const-string v3, "local_data"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_4
    const-string v3, "_display_name"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_5
    const-string v3, "home_sync_flags"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->i:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_6
    const-string v3, "icon"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->j:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_7
    const-string v3, "last_modified"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_8
    const-string v3, "mime_type"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_9
    const-string v3, "media_type"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->k:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto :goto_1

    :cond_a
    const-string v3, "orientation"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->l:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto/16 :goto_1

    :cond_b
    const-string v3, "width"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->m:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto/16 :goto_1

    :cond_c
    const-string v3, "height"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->n:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto/16 :goto_1

    :cond_d
    const-string v3, "album_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->o:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto/16 :goto_1

    :cond_e
    const-string v3, "device_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->p:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto/16 :goto_1

    :cond_f
    const-string v3, "_size"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v1, v0

    goto/16 :goto_1

    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported column: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->l:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file_browser_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->m:Ljava/lang/String;

    .line 152
    const-string v0, "INFO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ASP20FileBrowserCursor created remoteFolderId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sortType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_15

    .line 155
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v1

    invoke-interface {v1}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 159
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_14

    .line 161
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v1

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v4

    if-eqz v1, :cond_13

    :try_start_0
    const-string v5, "document_id"

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "_display_name"

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "home_sync_flags"

    invoke-static {v1}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->c(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "file_count"

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "path"

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_12

    const-string v5, "parent_display_name"

    invoke-virtual {p0, v4}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->d(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "parent_id"

    invoke-virtual {p0, v4}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "home_sync_flags"

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->c(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "parent_icon"

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->e(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_12
    const-string v4, "icon"

    invoke-static {v1}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->e(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->setExtras(Landroid/os/Bundle;)V

    .line 162
    :cond_13
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 163
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/mfluent/asp/datamodel/filebrowser/c;

    if-ne v1, v2, :cond_14

    .line 164
    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/c;

    .line 165
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->g()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->p:Z

    .line 167
    invoke-static {}, Lcom/mfluent/asp/util/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;-><init>(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->o:Ljava/util/concurrent/Future;

    .line 174
    :cond_14
    :goto_3
    return-void

    .line 161
    :catch_0
    move-exception v1

    const-string v4, "INFO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error during addDirectoryInfo, maybe ICON_ID, "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 172
    :cond_15
    const-string v0, "INFO"

    const-string v1, "ASP20FileBrowserCursor fileRef is NULL!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->p:Z

    return v0
.end method


# virtual methods
.method public final fillWindow(ILandroid/database/CursorWindow;)V
    .locals 14

    .prologue
    .line 246
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 247
    const-string v11, "NULL"

    .line 248
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->m:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->clearOrCreateWindow(Ljava/lang/String;)V

    .line 250
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->l:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    array-length v3, v3

    invoke-virtual {v2, v3}, Landroid/database/CursorWindow;->setNumColumns(I)Z

    .line 251
    iget v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->n:I

    invoke-static {p1, v2}, Landroid/database/DatabaseUtils;->cursorPickFillWindowStartPosition(II)I

    move-result v6

    .line 252
    iget v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->n:I

    add-int v3, v6, v2

    .line 253
    const/4 v2, 0x0

    .line 254
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v4, :cond_17

    .line 255
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v2

    .line 256
    if-ne v3, v6, :cond_16

    .line 257
    invoke-interface {v2}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v3

    move-object v4, v2

    move v2, v3

    .line 260
    :goto_0
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v3, v6}, Landroid/database/CursorWindow;->setStartPosition(I)V

    .line 262
    if-nez v4, :cond_15

    .line 435
    :cond_0
    :goto_1
    return-void

    .line 271
    :cond_1
    invoke-interface {v4, v6}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v3

    .line 273
    const/4 v2, 0x0

    move v5, v2

    :goto_2
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->l:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    array-length v2, v2

    if-ge v5, v2, :cond_14

    .line 274
    const/4 v2, 0x0

    .line 276
    :try_start_0
    invoke-static {v3}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v8

    .line 277
    sget-object v9, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$1;->a:[I

    iget-object v12, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->l:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aget-object v12, v12, v5

    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->ordinal()I

    move-result v12

    aget v9, v9, v12
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v9, :pswitch_data_0

    .line 427
    :goto_3
    if-nez v2, :cond_e

    .line 428
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->freeLastRow()V

    .line 429
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v2

    iput v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->n:I

    move v2, v6

    .line 266
    :goto_4
    add-int/lit8 v6, v6, 0x1

    move v7, v2

    :goto_5
    if-ge v6, v7, :cond_0

    .line 267
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 268
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v2

    iput v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->n:I

    goto :goto_1

    .line 279
    :pswitch_0
    if-eqz v3, :cond_2

    .line 280
    :try_start_1
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 282
    :cond_2
    invoke-virtual {p0, v11, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 286
    :pswitch_1
    if-eqz v3, :cond_3

    instance-of v2, v3, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-eqz v2, :cond_3

    .line 287
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 289
    :cond_3
    invoke-virtual {p0, v11, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 293
    :pswitch_2
    if-eqz v3, :cond_4

    .line 294
    invoke-virtual {p0, v3, v8}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 296
    :cond_4
    invoke-virtual {p0, v11, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 300
    :pswitch_3
    if-eqz v3, :cond_7

    .line 301
    iget-object v12, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isSecure()Z

    move-result v2

    if-eqz v2, :cond_5

    const-wide/16 v8, 0x1

    :goto_6
    invoke-virtual {v12, v8, v9, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto :goto_3

    :cond_5
    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isPersonal()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x2

    :goto_7
    int-to-long v8, v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    goto :goto_7

    .line 307
    :cond_7
    invoke-virtual {p0, v10, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto/16 :goto_3

    .line 311
    :pswitch_4
    if-eqz v3, :cond_8

    .line 312
    invoke-static {v3, v8}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0, v2, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto/16 :goto_3

    .line 314
    :cond_8
    invoke-virtual {p0, v10, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto/16 :goto_3

    .line 318
    :pswitch_5
    if-eqz v3, :cond_9

    .line 319
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->lastModified()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 321
    :cond_9
    invoke-virtual {p0, v10, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto/16 :goto_3

    .line 325
    :pswitch_6
    if-eqz v3, :cond_a

    .line 326
    invoke-static {v3}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->f(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto/16 :goto_3

    .line 328
    :cond_a
    invoke-virtual {p0, v11, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto/16 :goto_3

    .line 332
    :pswitch_7
    if-eqz v3, :cond_b

    .line 333
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 335
    :cond_b
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v6, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto/16 :goto_3

    .line 339
    :pswitch_8
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v8, v6

    invoke-virtual {v2, v8, v9, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 342
    :pswitch_9
    const/4 v2, -0x1

    .line 343
    if-eqz v3, :cond_c

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-class v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v8, v9, :cond_c

    .line 344
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 345
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->g()I

    move-result v2

    .line 347
    :cond_c
    iget-object v8, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v12, v2

    invoke-virtual {v8, v12, v13, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 351
    :pswitch_a
    const/4 v8, 0x0

    .line 352
    if-eqz v3, :cond_13

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v2, v9, :cond_13

    .line 353
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 354
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 355
    if-eqz v2, :cond_13

    .line 356
    const-string v8, "asp_mediaType"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 359
    :goto_8
    iget-object v8, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v12, v2

    invoke-virtual {v8, v12, v13, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 363
    :pswitch_b
    const/4 v8, 0x0

    .line 364
    if-eqz v3, :cond_12

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v2, v9, :cond_12

    .line 365
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 366
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 367
    if-eqz v2, :cond_12

    .line 368
    const-string v8, "asp_orientation"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 371
    :goto_9
    iget-object v8, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v12, v2

    invoke-virtual {v8, v12, v13, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 375
    :pswitch_c
    const/4 v8, 0x0

    .line 376
    if-eqz v3, :cond_11

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v2, v9, :cond_11

    .line 377
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 378
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 379
    if-eqz v2, :cond_11

    .line 380
    const-string v8, "asp_width"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 383
    :goto_a
    iget-object v8, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v12, v2

    invoke-virtual {v8, v12, v13, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 387
    :pswitch_d
    const/4 v8, 0x0

    .line 388
    if-eqz v3, :cond_10

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v2, v9, :cond_10

    .line 389
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 390
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 391
    if-eqz v2, :cond_10

    .line 392
    const-string v8, "asp_height"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 395
    :goto_b
    iget-object v8, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v12, v2

    invoke-virtual {v8, v12, v13, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 399
    :pswitch_e
    const-wide/16 v8, 0x0

    .line 400
    if-eqz v3, :cond_d

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v12, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v2, v12, :cond_d

    .line 401
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 402
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 403
    if-eqz v2, :cond_d

    .line 404
    const-string v8, "asp_albumId"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 407
    :cond_d
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2, v8, v9, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 411
    :pswitch_f
    const/4 v8, 0x0

    .line 412
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v9, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-ne v2, v9, :cond_f

    .line 413
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v2, v0

    .line 414
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 415
    if-eqz v2, :cond_f

    .line 416
    const-string v8, "asp_deviceId"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 419
    :goto_c
    iget-object v8, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    int-to-long v12, v2

    invoke-virtual {v8, v12, v13, v6, v5}, Landroid/database/CursorWindow;->putLong(JII)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto/16 :goto_3

    .line 423
    :catch_0
    move-exception v2

    .line 424
    new-instance v3, Landroid/database/StaleDataException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/database/StaleDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 273
    :cond_e
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_2

    :cond_f
    move v2, v8

    goto :goto_c

    :cond_10
    move v2, v8

    goto :goto_b

    :cond_11
    move v2, v8

    goto/16 :goto_a

    :cond_12
    move v2, v8

    goto/16 :goto_9

    :cond_13
    move v2, v8

    goto/16 :goto_8

    :cond_14
    move v2, v7

    goto/16 :goto_4

    :cond_15
    move v7, v2

    goto/16 :goto_5

    :cond_16
    move-object v4, v2

    move v2, v3

    goto/16 :goto_0

    :cond_17
    move-object v4, v2

    move v2, v3

    goto/16 :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/mfluent/asp/datamodel/filebrowser/c;

    if-ne v1, v2, :cond_0

    .line 230
    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/c;

    .line 231
    const-string v1, "INFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getCurrentSize()="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v0

    .line 241
    :goto_0
    return v0

    .line 234
    :cond_0
    const-string v1, "INFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getCount()="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",class="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v0

    goto :goto_0

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDeactivateOrClose()V
    .locals 7

    .prologue
    .line 451
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/k;->onDeactivateOrClose()V

    .line 452
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->g:Z

    if-eqz v0, :cond_1

    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->g:Z

    .line 455
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_0

    .line 456
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->c:J

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->f:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->e:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->o:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->p:Z

    if-nez v0, :cond_1

    .line 466
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->o:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    :cond_1
    :goto_0
    return-void

    .line 467
    :catch_0
    move-exception v0

    .line 468
    const-string v1, "ERR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error when closing cursor: exeption="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onMove(II)Z
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v0

    if-lt p2, v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v1}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v1

    add-int/2addr v0, v1

    if-lt p2, v0, :cond_1

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {p0, p2, v0}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 446
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
