.class final Lcom/mfluent/asp/datamodel/at$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public final a:J

.field public final b:J

.field public final c:I


# direct methods
.method public constructor <init>(JIJ)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/at$a;->b:J

    .line 44
    iput p3, p0, Lcom/mfluent/asp/datamodel/at$a;->c:I

    .line 45
    iput-wide p4, p0, Lcom/mfluent/asp/datamodel/at$a;->a:J

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;J)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide p2, p0, Lcom/mfluent/asp/datamodel/at$a;->a:J

    .line 38
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getContentId()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/mfluent/asp/datamodel/at$a;->b:J

    .line 39
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/datamodel/at$a;->c:I

    .line 40
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 55
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    check-cast p1, Lcom/mfluent/asp/datamodel/at$a;

    .line 59
    iget-wide v2, p1, Lcom/mfluent/asp/datamodel/at$a;->b:J

    iget-wide v4, p0, Lcom/mfluent/asp/datamodel/at$a;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-wide v2, p1, Lcom/mfluent/asp/datamodel/at$a;->a:J

    iget-wide v4, p0, Lcom/mfluent/asp/datamodel/at$a;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget v1, p1, Lcom/mfluent/asp/datamodel/at$a;->c:I

    iget v2, p0, Lcom/mfluent/asp/datamodel/at$a;->c:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    invoke-direct {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;-><init>()V

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/at$a;->b:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/at$a;->b:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/at$a;->a:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->toHashCode()I

    move-result v0

    return v0
.end method
