.class public Lcom/mfluent/asp/datamodel/ax;
.super Lcom/mfluent/asp/datamodel/l;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ax$a;
    }
.end annotation


# static fields
.field private static b:Lorg/slf4j/Logger;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/mfluent/asp/datamodel/ax;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/ax;->b:Lorg/slf4j/Logger;

    .line 42
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_caption_path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "local_caption_index_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ax;->c:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/l;-><init>()V

    .line 61
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ax;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/ax;
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/mfluent/asp/datamodel/ax$a;->a()Lcom/mfluent/asp/datamodel/ax;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 4

    .prologue
    .line 120
    const-string v0, "isPlayed"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    if-eq p2, p3, :cond_0

    .line 122
    new-instance p2, Landroid/content/ContentValues;

    invoke-direct {p2, p3}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 123
    const-string v0, "isPlayed"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 127
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/l;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 65
    const-string v0, "video"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V
    .locals 7

    .prologue
    .line 133
    const-string v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "_display_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 136
    const-string v0, "_display_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_0
    const-string v1, "title"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 142
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    invoke-static {v1}, Lorg/apache/commons/io/FilenameUtils;->removeExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 153
    :cond_1
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/ax;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;IJZ)V

    .line 156
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected final a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/mfluent/asp/datamodel/ax;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 70
    const-string v0, "video"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "VIDEOS"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "FILES"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected final g()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/mfluent/asp/datamodel/ax;->b:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final j()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x3

    return v0
.end method
