.class public final Lcom/mfluent/asp/datamodel/ay;
.super Lcom/mfluent/asp/datamodel/at;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ay$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "thumb_width"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thumb_height"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "full_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "thumbnail_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "transport_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "height"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ay;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/at;-><init>()V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ay;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/ay;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/mfluent/asp/datamodel/ay$a;->a()Lcom/mfluent/asp/datamodel/ay;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/mfluent/asp/datamodel/ay;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "VIDEOS"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x3

    return v0
.end method
