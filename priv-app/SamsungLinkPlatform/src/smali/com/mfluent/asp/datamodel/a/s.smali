.class public final Lcom/mfluent/asp/datamodel/a/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 26
    if-nez p3, :cond_1

    move-wide v2, v4

    .line 28
    :goto_0
    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 29
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 54
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 26
    :cond_1
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.RequestNetworkRefresh.INTENT_ARG_DEVICE_ID"

    invoke-virtual {p3, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    goto :goto_0

    .line 31
    :cond_2
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 32
    if-eqz v1, :cond_0

    .line 33
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.RequestNetworkRefresh.INTENT_ARG_MEDIA_TYPE"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 34
    sparse-switch v2, :sswitch_data_0

    .line 51
    :goto_2
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->a(I)V

    goto :goto_1

    .line 36
    :sswitch_0
    const/4 v0, 0x1

    .line 37
    goto :goto_2

    .line 39
    :sswitch_1
    const/4 v0, 0x2

    .line 40
    goto :goto_2

    .line 42
    :sswitch_2
    const/16 v0, 0x9

    .line 43
    goto :goto_2

    .line 45
    :sswitch_3
    const/4 v0, 0x3

    .line 46
    goto :goto_2

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0xf -> :sswitch_2
    .end sparse-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method
