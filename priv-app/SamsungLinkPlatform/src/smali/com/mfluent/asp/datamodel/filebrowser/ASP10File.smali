.class public Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
.super Lcom/mfluent/asp/datamodel/filebrowser/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:J

.field private d:Ljava/lang/String;

.field private e:J

.field private f:Z

.field private g:Ljava/text/CollationKey;

.field private h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

.field private i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

.field private j:I

.field private k:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/f;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->d:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f:Z

    .line 39
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->g:Ljava/text/CollationKey;

    .line 43
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->j:I

    .line 50
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->k:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final a()Ljava/text/CollationKey;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->g:Ljava/text/CollationKey;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 210
    iput p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->j:I

    .line 211
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 108
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c:J

    .line 109
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->k:Landroid/os/Bundle;

    .line 220
    return-void
.end method

.method public final bridge synthetic a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/f;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    return-void
.end method

.method final a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 194
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 145
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public final a(Ljava/text/CollationKey;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->g:Ljava/text/CollationKey;

    .line 67
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b:Z

    .line 101
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 140
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->e:J

    .line 141
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 123
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->d:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f:Z

    return v0
.end method

.method public final f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->j:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getSpecialDirectoryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/filebrowser/f;->getSpecialDirectoryType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->k:Landroid/os/Bundle;

    return-object v0
.end method

.method public final bridge synthetic i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/filebrowser/f;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v0

    return-object v0
.end method

.method public isDirectory()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b:Z

    return v0
.end method

.method public isPersonal()Z
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecure()Z
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastModified()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->e:J

    return-wide v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", len: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
