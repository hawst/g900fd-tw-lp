.class public final Lcom/mfluent/asp/datamodel/filebrowser/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileProvider;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/mfluent/asp/datamodel/Device;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/j;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/j;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 32
    return-void
.end method


# virtual methods
.method public final varargs deleteFiles(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;[Ljava/lang/String;)I
    .locals 12

    .prologue
    .line 74
    const/4 v6, 0x0

    .line 75
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/j;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 78
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "_data"

    aput-object v3, v2, v1

    .line 79
    const-string v3, "_display_name=?"

    .line 80
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    .line 82
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v7

    .line 86
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 87
    array-length v6, p3

    .line 161
    :cond_0
    return v6

    .line 90
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    array-length v9, p3

    const/4 v1, 0x0

    move v5, v1

    move v1, v6

    :goto_0
    if-ge v5, v9, :cond_5

    aget-object v6, p3, v5

    .line 92
    invoke-static {v6}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v6

    .line 93
    if-eqz v6, :cond_3

    .line 94
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v6

    .line 95
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-static {v10, v7}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "File to delete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a child of directory:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_2
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 102
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 104
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 109
    :cond_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v1

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 111
    const/4 v7, 0x0

    .line 113
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v10

    .line 114
    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 115
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 116
    const/4 v1, -0x1

    .line 117
    if-eqz v5, :cond_8

    .line 118
    :cond_6
    :goto_3
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 119
    const/4 v7, 0x1

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 120
    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v11

    if-nez v11, :cond_6

    .line 121
    :try_start_2
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v7

    .line 125
    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 126
    const/4 v7, 0x0

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    .line 133
    :cond_7
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    const/4 v10, 0x1

    if-ne v7, v10, :cond_8

    .line 134
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 135
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 139
    :cond_8
    if-lez v1, :cond_d

    .line 140
    const-string v7, "external"

    int-to-long v10, v1

    invoke-static {v7, v10, v11}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0, v1, v7, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    add-int/2addr v1, v6

    .line 145
    :goto_4
    if-eqz v5, :cond_b

    .line 146
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move v6, v1

    goto :goto_2

    .line 142
    :catch_0
    move-exception v1

    move-object v1, v5

    .line 145
    :goto_5
    if-eqz v1, :cond_a

    .line 146
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 145
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v5, :cond_9

    .line 146
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    :cond_a
    move v1, v6

    :cond_b
    move v6, v1

    .line 149
    goto/16 :goto_2

    .line 150
    :cond_c
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 151
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 152
    if-eqz v0, :cond_0

    .line 153
    add-int/lit8 v6, v6, 0x1

    .line 157
    goto :goto_7

    .line 145
    :catchall_1
    move-exception v0

    move-object v5, v7

    goto :goto_6

    .line 142
    :catch_1
    move-exception v1

    move-object v1, v7

    goto :goto_5

    .line 132
    :catch_2
    move-exception v7

    goto :goto_3

    :cond_d
    move v1, v6

    goto :goto_4
.end method

.method public final getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/j;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/j;->b:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public final getCloudStorageFileBrowser(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/common/datamodel/ASPFileSortType;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File does not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File is not a directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-direct {v1}, Lcom/mfluent/asp/datamodel/filebrowser/i;-><init>()V

    .line 67
    invoke-virtual {v1, v0, p2, p3}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 69
    return-object v1
.end method

.method public final getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 46
    instance-of v0, p1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Parameter must be of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-static {p1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
