.class public Lcom/mfluent/asp/datamodel/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/CloudDataModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/t$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static b:Ljava/lang/String;

.field private static c:Lcom/mfluent/asp/datamodel/t;


# instance fields
.field private final d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

.field private final e:Landroid/support/v4/content/LocalBroadcastManager;

.field private final f:Ljava/util/concurrent/locks/ReentrantLock;

.field private final g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/AllowedDevice;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/mfluent/asp/datamodel/Device;

.field private j:Lcom/mfluent/asp/datamodel/Device;

.field private final k:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/mfluent/asp/datamodel/t;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/datamodel/t;->c:Lcom/mfluent/asp/datamodel/t;

    return-void
.end method

.method private constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 4

    .prologue
    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    .line 81
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    .line 87
    new-instance v0, Lcom/mfluent/asp/datamodel/t$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/t$1;-><init>(Lcom/mfluent/asp/datamodel/t;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/t;->k:Landroid/content/BroadcastReceiver;

    .line 307
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    .line 308
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/t;->e:Landroid/support/v4/content/LocalBroadcastManager;

    .line 309
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->e:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->k:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 310
    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/t;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->c:Lcom/mfluent/asp/datamodel/t;

    return-object v0
.end method

.method public static a(Ljava/util/Set;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 370
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 371
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 372
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 374
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 134
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->c:Lcom/mfluent/asp/datamodel/t;

    if-nez v0, :cond_7

    .line 135
    new-instance v6, Lcom/mfluent/asp/datamodel/t;

    invoke-direct {v6, p0}, Lcom/mfluent/asp/datamodel/t;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    .line 136
    sput-object v6, Lcom/mfluent/asp/datamodel/t;->c:Lcom/mfluent/asp/datamodel/t;

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lcom/mfluent/asp/datamodel/Device;

    iget-object v3, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    add-int/lit8 v0, v0, 0x1

    const/4 v3, 0x6

    if-le v0, v3, :cond_1

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v3, v4, :cond_0

    :cond_1
    sget-object v3, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v4, "::initializeFromDB:caching device: {}"

    invoke-interface {v3, v4, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    iput-object v2, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    :cond_2
    iget-object v3, v6, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, v6, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_4
    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_6

    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v1, "::initializeFromDB:creating local device"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    new-instance v0, Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    iput-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/util/c;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->d(Ljava/lang/String;)V

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/service/account/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v0}, Lcom/sec/pcw/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->c(Ljava/lang/String;)V

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/mfluent/asp/datamodel/Device;->a:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Ljava/util/Set;)V

    :cond_6
    new-instance v0, Lcom/mfluent/asp/datamodel/AllDevicesDevice;

    iget-object v1, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mfluent/asp/datamodel/AllDevicesDevice;-><init>(Landroid/content/Context;)V

    iput-object v0, v6, Lcom/mfluent/asp/datamodel/t;->j:Lcom/mfluent/asp/datamodel/Device;

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    iget-object v1, v6, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/a/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/a/a;->e()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->X()Z

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, v6, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 137
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0402

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/t;->b:Ljava/lang/String;

    .line 139
    :cond_7
    return-void
.end method

.method public static d(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    .locals 1

    .prologue
    .line 558
    invoke-static {p0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 583
    :goto_0
    return-object v0

    .line 562
    :cond_0
    const-string v0, "PC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 564
    :cond_1
    const-string v0, "PHONE DEVICE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 565
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 566
    :cond_2
    const-string v0, "TAB DEVICE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 567
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 568
    :cond_3
    const-string v0, "BD"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 569
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 570
    :cond_4
    const-string v0, "CAMERA"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 571
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 572
    :cond_5
    const-string v0, "SPC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 574
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 575
    :cond_6
    const-string v0, "TV"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 576
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 577
    :cond_7
    const-string v0, "PHONE DEVICE WINDOWS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 578
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 579
    :cond_8
    const-string v0, "WEARABLE DEVICE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 580
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0

    .line 583
    :cond_9
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    goto :goto_0
.end method

.method static synthetic h()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a(J)Lcom/mfluent/asp/datamodel/Device;
    .locals 3

    .prologue
    .line 249
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->j:Lcom/mfluent/asp/datamodel/Device;

    .line 253
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    .locals 2

    .prologue
    .line 467
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 470
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/datamodel/t$3;

    invoke-direct {v1, p0, p1}, Lcom/mfluent/asp/datamodel/t$3;-><init>(Lcom/mfluent/asp/datamodel/t;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lorg/apache/commons/collections/CollectionUtils;->find(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 480
    return-object v0

    .line 478
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 216
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 218
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 220
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 221
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 228
    return-object v2

    .line 225
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 166
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 168
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 170
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 178
    return-object v2

    .line 175
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->e:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 258
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/AllowedDevice;)V
    .locals 3

    .prologue
    .line 433
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v1, "addAllowedDevice invoked with device: {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 434
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    monitor-enter v1

    .line 435
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v2, "addAllowedDLNADevice added device: {}"

    invoke-interface {v0, v2, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 438
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    .line 384
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v1, "::addDevice:add device [{}]"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 386
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 389
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 394
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/t$a;->a(Landroid/content/Context;)Lcom/mfluent/asp/datamodel/t$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t$a;->c(Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->e:Landroid/support/v4/content/LocalBroadcastManager;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 397
    :cond_0
    return-void

    .line 391
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    .locals 2

    .prologue
    .line 502
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 505
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/datamodel/t$4;

    invoke-direct {v1, p0, p1}, Lcom/mfluent/asp/datamodel/t$4;-><init>(Lcom/mfluent/asp/datamodel/t;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lorg/apache/commons/collections/CollectionUtils;->find(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 515
    return-object v0

    .line 513
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 153
    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 154
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 156
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 4

    .prologue
    .line 406
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 408
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 413
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->e:Landroid/support/v4/content/LocalBroadcastManager;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 415
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.device.DeviceDeleted"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 416
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->d:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 418
    return-void

    .line 410
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final c()Lcom/mfluent/asp/datamodel/Device;
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_0

    .line 238
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no local device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->i:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    .locals 2

    .prologue
    .line 519
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 522
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/datamodel/t$5;

    invoke-direct {v1, p0, p1}, Lcom/mfluent/asp/datamodel/t$5;-><init>(Lcom/mfluent/asp/datamodel/t;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lorg/apache/commons/collections/CollectionUtils;->find(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 532
    return-object v0

    .line 530
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 451
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v1, "clear AllowedDeviceList"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 452
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    monitor-enter v1

    .line 453
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 454
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/AllowedDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 461
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    monitor-enter v1

    .line 462
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/t;->h:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 540
    sget-object v0, Lcom/mfluent/asp/datamodel/t;->a:Lorg/slf4j/Logger;

    const-string v1, "::setAllDevicesOffline: setting all devices to offline"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 541
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 542
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v2

    if-nez v2, :cond_0

    .line 543
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v2, v3, :cond_0

    .line 547
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 551
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 552
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    goto :goto_0

    .line 555
    :cond_1
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 631
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/t;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 633
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->I()V

    .line 634
    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCTerminateSecureSession(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 631
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 636
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 639
    :cond_0
    return-void
.end method

.method public updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V
    .locals 2

    .prologue
    .line 423
    invoke-static {}, Lcom/mfluent/asp/util/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/datamodel/t$2;

    invoke-direct {v1, p0, p1}, Lcom/mfluent/asp/datamodel/t$2;-><init>(Lcom/mfluent/asp/datamodel/t;Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 430
    return-void
.end method
