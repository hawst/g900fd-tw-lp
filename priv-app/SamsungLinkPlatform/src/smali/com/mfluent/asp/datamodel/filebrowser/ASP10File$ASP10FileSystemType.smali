.class public final enum Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ASP10FileSystemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

.field public static final enum b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

.field public static final enum c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

.field public static final enum d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

.field private static final synthetic e:[Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    const-string v1, "ASP_DEVICE"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 22
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    const-string v1, "HOMESYNC_FAKE_ROOT"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 23
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    const-string v1, "HOMESYNC_DEVICE_SECURED"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 24
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    const-string v1, "HOMESYNC_DEVICE_PERSONAL"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->e:[Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->e:[Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    return-object v0
.end method
