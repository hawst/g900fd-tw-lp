.class public final Lcom/mfluent/asp/datamodel/ao$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

.field public final b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

.field public final c:Landroid/database/sqlite/SQLiteDatabase;

.field public final d:Landroid/net/Uri;

.field public final e:I

.field public final f:Lcom/mfluent/asp/util/a/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/util/a/a/a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/net/Uri;",
            "I",
            "Lcom/mfluent/asp/util/a/a/a",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    .line 42
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 43
    iput-object p3, p0, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    .line 44
    iput p4, p0, Lcom/mfluent/asp/datamodel/ao$a;->e:I

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    .line 46
    iput-object p5, p0, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;Lcom/mfluent/asp/util/a/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/net/Uri;",
            "Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;",
            "Lcom/mfluent/asp/util/a/a/a",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    .line 57
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 58
    iput-object p3, p0, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/datamodel/ao$a;->e:I

    .line 60
    iput-object p4, p0, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    .line 61
    iput-object p5, p0, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    .line 62
    return-void
.end method
