.class final Lcom/mfluent/asp/datamodel/filebrowser/i$3;
.super Landroid/os/FileObserver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/datamodel/filebrowser/i;->registerDataSetObserver(Landroid/database/DataSetObserver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/filebrowser/i;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/datamodel/filebrowser/i;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 573
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i$3;->a:Lcom/mfluent/asp/datamodel/filebrowser/i;

    const/16 v0, 0xfc6

    invoke-direct {p0, p2, v0}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final onEvent(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 578
    const/16 v0, 0x800

    if-le p1, v0, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 581
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i$3;->a:Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/i;)Ljava/io/FileFilter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i$3;->a:Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b(Lcom/mfluent/asp/datamodel/filebrowser/i;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i$3;->a:Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/i;)Ljava/io/FileFilter;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i$3;->a:Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b(Lcom/mfluent/asp/datamodel/filebrowser/i;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/io/FileFilter;->accept(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/i;->d()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    const-string v0, "mfl_LocalFileBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FileObserver: event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i$3;->a:Lcom/mfluent/asp/datamodel/filebrowser/i;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->c(Lcom/mfluent/asp/datamodel/filebrowser/i;)V

    goto :goto_0
.end method
