.class public final Lcom/mfluent/asp/datamodel/b;
.super Lcom/mfluent/asp/datamodel/at;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/b$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "12 AS media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thumbnail_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "thumb_width"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "thumb_height"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "physical_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "transport_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "source_album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/b;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/at;-><init>()V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/b;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/b;
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/mfluent/asp/datamodel/b$a;->a()Lcom/mfluent/asp/datamodel/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;J)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 48
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/Long;

    .line 50
    :try_start_0
    sget-object v2, Lcom/mfluent/asp/datamodel/b;->a:[Ljava/lang/String;

    .line 51
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALBUM_DETAIL"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "device_priority"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_4

    .line 61
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    const-string v0, "thumbnail_uri"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 64
    const-string v2, "thumb_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 67
    :cond_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 68
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 69
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 70
    :cond_1
    invoke-static {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v9

    move-object v0, v9

    .line 75
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :goto_1
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v8}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    return-object v0

    .line 73
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move-object v0, v9

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v8}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    throw v0

    :cond_4
    move-object v0, v9

    goto :goto_1
.end method

.method protected final b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/mfluent/asp/datamodel/b;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "ALBUM_DETAIL"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 110
    const/16 v0, 0xc

    return v0
.end method
