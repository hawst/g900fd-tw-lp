.class public Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
.super Lcom/mfluent/asp/datamodel/filebrowser/f;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/f;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a:Ljava/io/File;

    .line 18
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/File;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a:Ljava/io/File;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/f;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSpecialDirectoryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/filebrowser/f;->getSpecialDirectoryType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/filebrowser/f;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v0

    return-object v0
.end method

.method public isDirectory()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    return v0
.end method

.method public isPersonal()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public isSecure()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public lastModified()J
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "LocalASPFile: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "DIR: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "FILE: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", size: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
