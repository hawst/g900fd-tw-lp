.class public final Lcom/mfluent/asp/datamodel/filebrowser/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/filebrowser/d$b;,
        Lcom/mfluent/asp/datamodel/filebrowser/d$d;,
        Lcom/mfluent/asp/datamodel/filebrowser/d$c;,
        Lcom/mfluent/asp/datamodel/filebrowser/d$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/d$c;",
            "Lcom/mfluent/asp/datamodel/filebrowser/d$d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/locks/ReentrantLock;

.field private final c:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->a:Ljava/util/HashMap;

    .line 201
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    .line 202
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->c:Landroid/os/Handler;

    .line 203
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/d;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/filebrowser/d;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/d$b;->a:Lcom/mfluent/asp/datamodel/filebrowser/d;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFileProvider;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;)Lcom/mfluent/asp/datamodel/filebrowser/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/common/datamodel/ASPFileProvider;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Z)Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFileProvider;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Z)Lcom/mfluent/asp/datamodel/filebrowser/d$a;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    const/4 v7, 0x0

    .line 219
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;

    move-result-object v0

    if-nez v0, :cond_0

    .line 220
    const/4 v0, 0x0

    .line 301
    :goto_0
    return-object v0

    .line 222
    :cond_0
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/CloudDevice;->getId()I

    move-result v0

    int-to-long v8, v0

    .line 223
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/CloudDevice;->getId()I

    move-result v0

    int-to-long v2, v0

    const/4 v6, 0x0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d$c;-><init>(JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 224
    if-eqz p4, :cond_1

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->e:Z

    .line 228
    :cond_1
    const/4 v2, 0x0

    .line 229
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;

    .line 232
    if-eqz v0, :cond_9

    .line 234
    iget v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    .line 235
    :goto_1
    iget v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 236
    if-eqz p4, :cond_2

    .line 239
    iget-object v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    const-wide/32 v4, 0x7270e00

    invoke-interface {v1, v4, v5}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 256
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 241
    :cond_2
    :try_start_1
    iget-object v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V

    goto :goto_1

    .line 244
    :cond_3
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b(Lcom/mfluent/asp/datamodel/filebrowser/d$d;)Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    move-result-object v1

    .line 245
    if-eqz v1, :cond_4

    iget-boolean v3, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->d:Z

    if-eqz v3, :cond_13

    :cond_4
    move-object v3, v0

    .line 252
    :goto_2
    if-eqz v3, :cond_5

    .line 253
    const/4 v0, 0x1

    iput v0, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 258
    if-eqz v3, :cond_11

    .line 260
    const/4 v0, 0x0

    if-eqz p1, :cond_7

    const/4 v2, 0x0

    const/4 v4, 0x1

    :try_start_2
    invoke-interface {p1, p2, p3, v2, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getCloudStorageFileBrowser(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v2

    if-eqz v2, :cond_7

    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-direct {v0, v2}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;-><init>(Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;)V

    sget-boolean v4, Lcom/mfluent/asp/datamodel/filebrowser/c;->a:Z

    if-eqz v4, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/mfluent/asp/datamodel/filebrowser/c;

    if-eq v4, v5, :cond_7

    :cond_6
    invoke-interface {v2}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v4

    if-lez v4, :cond_7

    invoke-interface {v2}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 264
    :cond_7
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 266
    const/4 v1, 0x0

    .line 267
    if-eqz v0, :cond_8

    .line 268
    :try_start_3
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 269
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/net/Uri;

    .line 270
    const/4 v2, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDefaultFileListUri(J)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v1, v2

    .line 271
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v4

    invoke-interface {v4}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v9, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v1, v2

    .line 286
    :cond_8
    :goto_3
    invoke-virtual {v3, v0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;[Landroid/net/Uri;)V

    .line 288
    if-eqz v0, :cond_c

    const/4 v1, 0x2

    :goto_4
    iput v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 293
    iget-object v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 294
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 249
    :cond_9
    :try_start_4
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->c:Landroid/os/Handler;

    invoke-direct {v0, v2, v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/d$d;-><init>(Landroid/content/ContentResolver;Ljava/util/concurrent/locks/Condition;Landroid/os/Handler;)V

    .line 250
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v3, v0

    move-object v1, v7

    goto/16 :goto_2

    .line 274
    :cond_a
    :try_start_5
    const-string v1, "ROOT"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 275
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/net/Uri;

    .line 276
    const/4 v2, 0x1

    invoke-static {v8, v9, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v1, v2

    .line 277
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v4

    invoke-interface {v4}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v9, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v1, v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    .line 289
    :catch_0
    move-exception v1

    .line 290
    :try_start_6
    const-string v2, "INFO"

    const-string v4, "error during setURI in filebrowser"

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 293
    iget-object v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 294
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 281
    :cond_b
    const/4 v1, 0x1

    :try_start_7
    new-array v1, v1, [Landroid/net/Uri;

    .line 282
    const/4 v2, 0x0

    invoke-static {v8, v9, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v1, v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 293
    :catchall_1
    move-exception v0

    iget-object v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 294
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 288
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 296
    :catchall_2
    move-exception v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 266
    const/4 v2, 0x0

    .line 267
    if-eqz v1, :cond_d

    .line 268
    :try_start_8
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 269
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/net/Uri;

    .line 270
    const/4 v4, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDefaultFileListUri(J)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v2, v4

    .line 271
    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v5

    invoke-interface {v5}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v9, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v2, v4

    .line 286
    :cond_d
    :goto_5
    invoke-virtual {v3, v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;[Landroid/net/Uri;)V

    .line 288
    if-eqz v1, :cond_10

    const/4 v1, 0x2

    :goto_6
    iput v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 293
    iget-object v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 294
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_7
    throw v0

    .line 274
    :cond_e
    :try_start_9
    const-string v2, "ROOT"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 275
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/net/Uri;

    .line 276
    const/4 v4, 0x1

    invoke-static {v8, v9, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v2, v4

    .line 277
    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v5

    invoke-interface {v5}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v9, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v2, v4
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_5

    .line 289
    :catch_1
    move-exception v1

    .line 290
    :try_start_a
    const-string v2, "INFO"

    const-string v4, "error during setURI in filebrowser"

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 293
    iget-object v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 294
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_7

    .line 281
    :cond_f
    const/4 v2, 0x1

    :try_start_b
    new-array v2, v2, [Landroid/net/Uri;

    .line 282
    const/4 v4, 0x0

    invoke-static {v8, v9, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v2, v4
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_5

    .line 293
    :catchall_3
    move-exception v0

    iget-object v1, v3, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 294
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 288
    :cond_10
    const/4 v1, 0x0

    goto :goto_6

    .line 297
    :cond_11
    if-eqz v1, :cond_12

    .line 298
    invoke-static {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;)V

    :cond_12
    move-object v0, v1

    goto/16 :goto_0

    :cond_13
    move-object v3, v2

    goto/16 :goto_2
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 305
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d$c;-><init>(JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 306
    if-eqz p1, :cond_1

    .line 307
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lcom/mfluent/asp/datamodel/filebrowser/c;

    if-ne v0, v2, :cond_0

    .line 309
    iput-boolean v7, v1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->e:Z

    .line 311
    :cond_0
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b(Lcom/mfluent/asp/datamodel/filebrowser/d$a;)V

    .line 312
    const-string v0, "INFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fileBrowserRef.mRefCount="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->c(Lcom/mfluent/asp/datamodel/filebrowser/d$a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;

    .line 319
    if-eqz v0, :cond_3

    .line 320
    iget v2, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    if-le v2, v7, :cond_4

    .line 321
    iget v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    .line 329
    :cond_2
    :goto_0
    const-string v1, "INFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mFileBrowserMap.check refCount="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", frbowserSize="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 333
    return-void

    .line 322
    :cond_4
    :try_start_1
    iget v2, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    if-ne v2, v7, :cond_2

    .line 323
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a()V

    .line 324
    const/4 v2, 0x0

    iput v2, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->c:I

    .line 325
    const/4 v2, 0x0

    iput v2, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->a:I

    .line 326
    iget-object v2, v0, Lcom/mfluent/asp/datamodel/filebrowser/d$d;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 327
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
