.class final enum Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FieldValues"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum b:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum c:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum d:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum e:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum f:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum g:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field public static final enum h:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field private static final synthetic i:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "FILE_ID"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 35
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "LOCAL_DATA"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 36
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "DISPLAY_NAME"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 37
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "MIME_TYPE"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 38
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "LAST_MODIFIED"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 39
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "SIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 40
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "HOME_SYNC_FLAGS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 41
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    const-string v1, "ICON_ID"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 33
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->i:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->i:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    invoke-virtual {v0}, [Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    return-object v0
.end method
