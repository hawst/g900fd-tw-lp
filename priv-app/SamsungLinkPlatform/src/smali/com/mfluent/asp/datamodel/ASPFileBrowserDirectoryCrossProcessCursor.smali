.class public final Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;
.super Lcom/mfluent/asp/datamodel/k;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$1;,
        Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;
    }
.end annotation


# static fields
.field private static final i:[Ljava/lang/String;

.field private static j:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final k:[Ljava/lang/String;

.field private final l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "document_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "home_sync_flags"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "file_count"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "parent_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "parent_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "home_sync_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "parent_icon"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->i:[Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 52
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/k;-><init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;B)V

    .line 53
    if-nez p5, :cond_0

    .line 54
    sget-object p5, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->i:[Ljava/lang/String;

    .line 56
    :cond_0
    iput-object p5, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->k:[Ljava/lang/String;

    .line 58
    array-length v0, p5

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    :goto_0
    array-length v1, p5

    if-ge v6, v1, :cond_b

    aget-object v1, p5, v6

    const-string v2, "document_id"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "_display_name"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_2
    const-string v2, "home_sync_flags"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_3
    const-string v2, "icon"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_4
    const-string v2, "file_count"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_5
    const-string v2, "path"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_6
    const-string v2, "parent_id"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_7
    const-string v2, "parent_display_name"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_8
    const-string v2, "home_sync_flags"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->i:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_9
    const-string v2, "parent_icon"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->j:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported column: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file_browser_dir"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->m:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_c

    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v1

    invoke-interface {v1}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$DirectoryInfo;->getDirectoryInfoUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 68
    :cond_c
    return-void
.end method


# virtual methods
.method public final fillWindow(ILandroid/database/CursorWindow;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->clearOrCreateWindow(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    array-length v2, v2

    invoke-virtual {v0, v2}, Landroid/database/CursorWindow;->setNumColumns(I)Z

    .line 122
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0, v1}, Landroid/database/CursorWindow;->setStartPosition(I)V

    .line 124
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-nez v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v3

    .line 133
    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v4

    .line 134
    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v5

    move v0, v1

    .line 136
    :goto_1
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 140
    :try_start_0
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$1;->a:[I

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->ordinal()I

    move-result v6

    aget v2, v2, v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v2, :pswitch_data_0

    move v2, v1

    .line 179
    :goto_2
    if-nez v2, :cond_2

    .line 180
    invoke-virtual {p2}, Landroid/database/CursorWindow;->freeLastRow()V

    goto :goto_0

    .line 142
    :pswitch_0
    :try_start_1
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_2

    .line 145
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->d(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_2

    .line 148
    :pswitch_2
    invoke-static {v4}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->c(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto :goto_2

    .line 151
    :pswitch_3
    invoke-static {v4}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->e(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto :goto_2

    .line 154
    :pswitch_4
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v6

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-virtual {v2, v6, v7, v8, v0}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto :goto_2

    .line 157
    :pswitch_5
    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_2

    .line 160
    :pswitch_6
    invoke-virtual {p0, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->d(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_2

    .line 163
    :pswitch_7
    invoke-virtual {p0, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_2

    .line 166
    :pswitch_8
    invoke-static {v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->c(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto :goto_2

    .line 169
    :pswitch_9
    invoke-static {v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->e(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {p0, v2, v6, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->a(Ljava/lang/Long;II)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto :goto_2

    .line 175
    :catch_0
    move-exception v0

    .line 176
    new-instance v1, Landroid/database/StaleDataException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/database/StaleDataException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 140
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    .line 112
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic onDeactivateOrClose()V
    .locals 0

    .prologue
    .line 17
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/k;->onDeactivateOrClose()V

    return-void
.end method

.method public final bridge synthetic onMove(II)Z
    .locals 1

    .prologue
    .line 17
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/datamodel/k;->onMove(II)Z

    move-result v0

    return v0
.end method
