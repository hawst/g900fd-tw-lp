.class public final Lcom/mfluent/asp/datamodel/ac;
.super Lcom/mfluent/asp/datamodel/n;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ac$a;
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "local_caption_path"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "local_caption_index_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ac;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/n;-><init>()V

    .line 56
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ac;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/ac;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/mfluent/asp/datamodel/ac$a;->a()Lcom/mfluent/asp/datamodel/ac;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 60
    const-string v0, "files"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/mfluent/asp/datamodel/ac;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 70
    const-string v0, "files"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, "FILES_DEVICES"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string v0, "FILES"

    return-object v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
